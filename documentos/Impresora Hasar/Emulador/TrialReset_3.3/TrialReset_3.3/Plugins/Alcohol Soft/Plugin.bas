Attribute VB_Name = "Plugin"

Sub Main()

    If LCase(Command) = "\info" Then
        Open VB.App.Path & "\info.tmp" For Output As #1
        Print #1, "This plugins support Alcohol 120%/52% 1.x"
        Close

        End

    End If

    Erase AllKeys()
    ReDim AllKeys(0) As String
    TrialName = "Alcohol 1.x"

    EnumAllRegKeys HKEY_CURRENT_USER, ""

    For I = 1 To UBound(AllKeys)

        TheValue = Trim(ReadValue(HKEY_CURRENT_USER, AllKeys(I), ""))
        NOK = EnumValues(HKEY_CURRENT_USER, AllKeys(I))
 
        If NOK = 3 And TheValue = "" Then
            TheValue1 = Trim(ReadValue(HKEY_CURRENT_USER, AllKeys(I), ValueKeys(1)))
            TheValue2 = Trim(ReadValue(HKEY_CURRENT_USER, AllKeys(I), ValueKeys(2)))
            TheValue3 = Trim(ReadValue(HKEY_CURRENT_USER, AllKeys(I), ValueKeys(3)))

            If Left(ValueKeys(1), 2) = Left(ValueKeys(2), 2) And Left(ValueKeys(2), 2) = Left(ValueKeys(3), 2) And IsNumeric(TheValue1) = True And IsNumeric(TheValue2) = True And IsNumeric(TheValue3) = True Then
                If TheValue1 < 40 And TheValue2 < 40 And TheValue3 < 40 Then
                    AddToList "HKEY_CURRENT_USER\" & AllKeys(I)
                End If
            End If
        End If

    Next I

    Open VB.App.Path & "\data.tmp" For Output As #1
    Print #1, Result
    Close

End Sub

