Probado en un Windows 7 Profesional

Instalar el archivo:

Emulador52_std_setup.exe

Es una versi�n vieja, la �ltima a fecha de que armo esto es 5.3.

De todas formas anda bien, y posiblemente la actualizaci�n debe ser porque le encontraron forma de reiniciar el trial.
Tanto la 5.2 como la 5.3 en alg�n momento me dieron pantalla azul de la muerte, as� que no se molesten en actualizar.

Una vez instalado el emulador, descomprimir el TrialReset.
En la carpeta "setup" est� el "Trial-Reset.exe", ejecutarlo con permisos de administrador.
De la columna de botones que ven a la izquierda, el segundo empezando de arriba es "Armadillo"
Darle primero "Scan" y desp�s "Clean" y listo, se reinician los 15 d�as de prueba del emulador.

Hago esto porque llam� al tipo para comprarle el emulador y no solo que cobra car�simo, sino que me atendi� como el orto.

Disfruten

NOTA: posiblemente para que el emulador pueda emular un puerto COM tengan que iniciar el emulador con permisos de administrador.