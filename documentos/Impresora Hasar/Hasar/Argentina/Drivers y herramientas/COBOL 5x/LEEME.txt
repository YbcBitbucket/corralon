================================================================================
Dto. Software de Base                      Compa��a HASAR SAIC -                                              Grupo HASAR
Impresoras Fiscales                          Lenguaje Cobol                                                                         Argentina
================================================================================

putcmd.exe	Permite enviar un comando a la impresora fiscal.
		Para uso en MS-DOS o sesiones MS-DOS de Windows '98, o anteriores.

getans.exe	Permite obtener la respuesta a un comando enviado a la impresora fiscal.
		Para uso en MS-DOS o sesiones MS-DOS de Windows '98, o anteriores.

fistest.cbl		Ejemplo cobol de uso de estos ejecutables.		

leeme.txt              	Este archivo.