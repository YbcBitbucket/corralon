﻿//=========================================================================================================================================
// Ejemplo VC# , OCX Fiscal HASAR , Dto. Software de Base , Compañía HASAR SAIC , Grupo HASAR                                    ARGENTINA
// VS 2010 , OCX v051122 , Impresora Fiscal SMH/P-715F v04.03                                                                     NOV 2012
// Ejemplo fácilmente adaptable a cualquier otro modelo de impresora fiscal HASAR.
// Consultar:   Manual del OCX - Manual de comandos de la impresora fiscal
//=========================================================================================================================================
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CSH_OCX_HASAR
{
    public partial class Form1 : Form
    {
        // Se instancia el objeto OCX Fiscal HASAR (ofh)
        //----------------------------------------------
        FiscalPrinterLib.HASAR ofh = new FiscalPrinterLib.HASAR();
       
        public Form1()
        {
            InitializeComponent();
        }

        //===============================================================================================================================
        // Se dispara la impresión de los documentos de ejemplo.
        //===============================================================================================================================
        private void button1_Click(object sender, EventArgs e)
        {
            Int32 n;                    // Para mostrar números de comprobantes
            Int32 monto;                // Para mostrar montos

            object nrocomp;             // Argumento de salida CerrarComprobanteFiscal() y CerrarDNFH()

            object items;               // Argumento de salida Subtotal()
            object ventas;              // Argumento de salida Subtotal(), ReporteX(), ReporteZ()
            object iva;                 // Argumento de salida Subtotal()
            object pagado;              // Argumento de salida Subtotal()
            object ivanoi;              // Argumento de salida Subtotal()
            object impint;              // Argumento de salida Subtotal()

            object nrox;                // Argumento de salida ReporteX() / ReporteZ()
            object dfcancel;            // Argumento de salida ReporteX() / ReporteZ()
            object dnfhemit;            // Argumento de salida ReporteX() / ReporteZ()
            object dnfemit;             // Argumento de salida ReporteX() / ReporteZ()
            object dfemit;              // Argumento de salida ReporteX() / ReporteZ()
            object ultdfbc;             // Argumento de salida ReporteX() / ReporteZ()
            object ultdfa;              // Argumento de salida ReporteX() / ReporteZ()
            object ivadf;               // Argumento de salida ReporteX() / ReporteZ()
            object impintdf;            // Argumento de salida ReporteX() / ReporteZ()
            object percepdf;            // Argumento de salida ReporteX() / ReporteZ()
            object ivanoidf;            // Argumento de salida ReporteX() / ReporteZ()
            object ultncbc;             // Argumento de salida ReporteX() / ReporteZ()
            object ultnca;              // Argumento de salida ReporteX() / ReporteZ()
            object ventasnc;            // Argumento de salida ReporteX() / ReporteZ()
            object ivanc;               // Argumento de salida ReporteX() / ReporteZ()
            object impintnc;            // Argumento de salida ReporteX() / ReporteZ()
            object percepnc;            // Argumento de salida ReporteX() / ReporteZ()
            object ivanoinc;            // Argumento de salida ReporteX() / ReporteZ()
            object ultrto;              // Argumento de salida ReporteX() / ReporteZ()
            object cantnccancel;        // Argumento de salida ReporteX() / ReporteZ()
            object cantdfbcemit;        // Argumento de salida ReporteX() / ReporteZ()
            object cantdfaemit;         // Argumento de salida ReporteX() / ReporteZ()
            object cantncbcemit;        // Argumento de salida ReporteX() / ReporteZ()
            object cantncaemit;         // Argumento de salida ReporteX() / ReporteZ()

            // Se declaran todos los eventos posibles de ser disparados por el OCX Fiscal HASAR
            //---------------------------------------------------------------------------------
            ofh.ErrorFiscal += new FiscalPrinterLib._FiscalEvents_ErrorFiscalEventHandler(ofh_ErrorFiscal);
            ofh.ErrorImpresora += new FiscalPrinterLib._FiscalEvents_ErrorImpresoraEventHandler(ofh_ErrorImpresora);
            // ofh.EventoCajon += new FiscalPrinterLib._FiscalEvents_EventoCajonEventHandler(ofh_EventoCajon);
            // ofh.EventoFiscal += new FiscalPrinterLib._FiscalEvents_EventoFiscalEventHandler(ofh_EventoFiscal);
            // ofh.EventoImpresora += new FiscalPrinterLib._FiscalEvents_EventoImpresoraEventHandler(ofh_EventoImpresora);
            ofh.FaltaPapel += new FiscalPrinterLib._FiscalEvents_FaltaPapelEventHandler(ofh_FaltaPapel);
            ofh.ImpresoraNoResponde += new FiscalPrinterLib._FiscalEvents_ImpresoraNoRespondeEventHandler(ofh_ImpresoraNoResponde);
            // ofh.ImpresoraOcupada += new FiscalPrinterLib._FiscalEvents_ImpresoraOcupadaEventHandler(ofh_ImpresoraOcupada);
            // ofh.ImpresoraOK += new FiscalPrinterLib._FiscalEvents_ImpresoraOKEventHandler(ofh_ImpresoraOK);
            ofh.ProgresoDeteccion += new FiscalPrinterLib._FiscalEvents_ProgresoDeteccionEventHandler(ofh_ProgresoDeteccion);

            try  // Porción de código para atrapar excepciones...
            {
                ofh.Modelo = FiscalPrinterLib.ModelosDeImpresoras.MODELO_715_403;
                ofh.Puerto = 4;
                ofh.Comenzar();
                ofh.TratarDeCancelarTodo();

                //-----------------------------------------------
                // Ejemplo de impresión de un tique factura "A"
                //-----------------------------------------------     
                ofh.DatosCliente("Razón social cliente...","99999999995",FiscalPrinterLib.TiposDeDocumento.TIPO_CUIT,FiscalPrinterLib.TiposDeResponsabilidades.RESPONSABLE_INSCRIPTO,"Domiclio Cliente...");
                ofh.AbrirComprobanteFiscal(FiscalPrinterLib.DocumentosFiscales.TICKET_FACTURA_A);
                ofh.ImprimirItem("Art. Vendido...", 1.0, 10.0, 21.0, 0.0);
                ofh.Subtotal(false,out items,out ventas,out iva,out pagado,out ivanoi,out impint);

                monto = System.Int32.Parse(ventas.ToString());
                MessageBox.Show("Subtotal Comprobante ::: $ " +  monto.ToString());
                
                ofh.CerrarComprobanteFiscal(1, out nrocomp );

                n = System.Int32.Parse(nrocomp.ToString());
                MessageBox.Show("Nro. de Comprobante ::: TFA" + " ::: " + n.ToString());

                //-----------------------------------------------------
                // Ejemplo de impresión de un tique nota de crédito "B"
                //-----------------------------------------------------
                ofh.DatosCliente("Razón social cliente...", "12345678", FiscalPrinterLib.TiposDeDocumento.TIPO_DNI , FiscalPrinterLib.TiposDeResponsabilidades.CONSUMIDOR_FINAL , "Domiclio Cliente...");
                ofh.set_DocumentoDeReferencia(1,"TFA 0001-00000876");
                ofh.AbrirDNFH(FiscalPrinterLib.DocumentosNoFiscales.TICKET_NOTA_CREDITO_B);
                ofh.ImprimirItem("Art. Vendido...", 1.0, 10.0, 21.0, 0.0);
                ofh.Subtotal(false, out items, out ventas, out iva, out pagado, out ivanoi, out impint);

                monto = System.Int32.Parse(ventas.ToString());
                MessageBox.Show("Subtotal Comprobante ::: $ " + monto.ToString());

                ofh.CerrarDNFH(1, out nrocomp);

                n = System.Int32.Parse(nrocomp.ToString());
                MessageBox.Show("Nro. de Comprobante ::: TNCB" + " ::: " + n.ToString());

                //-----------------------------------------------
                // Ejemplo de impresión de un documento no fiscal
                //-----------------------------------------------
                ofh.AbrirComprobanteNoFiscal();
                ofh.ImprimirTextoNoFiscal("Línea de texto no fiscal...");
                ofh.CerrarComprobanteNoFiscal();

                //------------------------------------------------
                // Ejemplo de impresión de un reporte "X"
                //------------------------------------------------
                ofh.ReporteX(out nrox,out dfcancel,out dnfhemit,out dnfemit,out dfemit,out ultdfbc,out ultdfa,out ventas, out ivadf,out impintdf,out percepdf,out ivanoidf,out ultncbc,out ultnca, out ventasnc,out ivanc, out impintnc,out percepnc,out ivanoinc,out ultrto,out cantnccancel,out cantdfbcemit,out cantdfaemit,out cantncbcemit,out cantncaemit);
                
                n = System.Int32.Parse(ventas.ToString());
                MessageBox.Show("Parcial Ventas ::: Reporte X ::: $ " + n.ToString());

                // Se cierra el puerto serie abierto con 'Comenzar()'...
                ofh.Finalizar();
            }
            catch(SystemException exc)  // Tratamiento de las excepciones disparadas...
            {
                MessageBox.Show(exc.Source + "  :::  " + exc.Message);     
            }
        }

        //·······························································································································
        // Los siguientes son todos los eventos que puede dispara el OCX Fiscal HASAR.
        // En la mayoría no se hace tratamiento (corresponde al desarrollador implementar el código adecuado).
        // Tampoco se procesan todas las posibilidades. Se trata de ejemplos orientativos.
        //·······························································································································

        //===============================================================================================================================
        // Este evento se dispara cuando el OCX está intentando comunicarse con la impresora fiscal, probando a diferentes baudios.
        //===============================================================================================================================
        void ofh_ProgresoDeteccion(int Puerto, int Velocidad)
        {
            // throw new NotImplementedException();
            MessageBox.Show("Evento: ProgesoDetección");
        }

        //===============================================================================================================================
        // Este evento se dispara cuando la impresora fiscal recibe la respuesta a un comando, que estaba demorada.
        //===============================================================================================================================
        //void ofh_ImpresoraOK()
        //{
        //    MessageBox.Show("Evento: ImpresoraOK");
        //}

        //===============================================================================================================================
        // Este evento se dispara cuando la impresora fiscal solicita que no se envíe un nuevo comando.
        // La respuesta al comando enviado está demorada.
        //===============================================================================================================================
        //void ofh_ImpresoraOcupada()
        //{
        //    MessageBox.Show("Evento: ImpresoraOcupada");
        //}

        //===============================================================================================================================
        // Este evento se dispara cuando la impresora (como estándar) reporta su estado.
        //===============================================================================================================================
        //void ofh_EventoImpresora(int Flags)
        //{
        //    MessageBox.Show("Evento: EventoImpresora");
        //}

        //===============================================================================================================================
        // Este evento se dispara cuando la impresora fiscal reporta su estado.
        //===============================================================================================================================
        //void ofh_EventoFiscal(int Flags)
        //{
        //    MessageBox.Show("Evento: EventoFiscal");
        //}

        //===============================================================================================================================
        // Este evento se dispara cuando el cajó de dinero conectado a la impreosra fiscal (si ésta lo soporta) cambia de estado.
        //===============================================================================================================================
        //void ofh_EventoCajon(int Opened)
        //{
        //    MessageBox.Show("Evento: EventoCajon");
        //}

        //===============================================================================================================================
        // Este evento se dispara cuando la impresora (como estándar) reporta su estado.
        //===============================================================================================================================
        void ofh_ErrorImpresora(int Flags)
        {
            MessageBox.Show("Evento: ErrorImpresora");
        }

        //===============================================================================================================================
        // Este evento se dispara cuando la impresora fiscal rechaza comandos.
        //===============================================================================================================================
        void ofh_ErrorFiscal(int Flags)
        {
            string msj = "";      // Mensaje asociado al evento

            switch (Flags)
            {
                case (int) FiscalPrinterLib.FiscalBits.F_INVALID_COMMAND:
                    msj = "OCX Fiscal HASAR ::: Comando Inválido para el estado fiscal actual";
                    break;
                case (int) FiscalPrinterLib .FiscalBits .F_INVALID_FIELD_DATA:
                    msj = "OCX Fiscal HASAR ::: Campo de datos inválido";
                    break;
                case (int) FiscalPrinterLib.FiscalBits.F_TOTAL_OVERFLOW:
                    msj = "OCX Fiscal HASAR ::: Se excede monto límite comprobante";
                    break;
                case (int)FiscalPrinterLib.FiscalBits.F_UNRECOGNIZED_COMMAND:
                    msj = "OCX Fiscal HASAR ::: Comando desconocido";
                    break;
                default:
                    msj = "OCX Fiscal HASAR ::: Comando rechazado - Evento: ErrorFiscal( )";
                    break;
            };

            MessageBox.Show(msj);
        }

        //===============================================================================================================================
        // Este evento se dispara cuando la impresora fiscal reporta falta de papel.
        //===============================================================================================================================
        void ofh_FaltaPapel()
        {
            MessageBox.Show("Evento: FaltaPapel");
        }

        //===============================================================================================================================
        // Este evento se dispara ante inconvenientes en la comunicación con la impresora fiscal.
        //===============================================================================================================================
        void ofh_ImpresoraNoResponde(int CantidadReintentos)
        {
            MessageBox.Show("Evento: ImpresoraNoResponde");
        }

        //================================================================================================================================
        // Se da por terminada la ejecución del ejemplo.
        //================================================================================================================================
        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
