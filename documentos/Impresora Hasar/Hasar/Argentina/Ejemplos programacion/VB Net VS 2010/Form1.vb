Imports System
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.String

Public Class Form1
    '// Declaraci�n de algunas de las funciones disponibles en la librer�a fiscal "winfis32.dll" provista por HASAR
    '//------------------------------------------------------------------------------------------------------------

    '// Obtener la versi�n de la librer�a
    '//----------------------------------
    <DllImport("winfis32.dll", EntryPoint:="VersionDLLFiscal", CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function VersionDLLFiscal() As System.Int32
    End Function

    '// Sincronizaci�n con la impreosra fiscal - Env�a dos comandos de consulta de estado
    '//----------------------------------------------------------------------------------
    <DllImport("winfis32.dll", EntryPoint:="InitFiscal", CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function InitFiscal(ByVal handler As System.Int32) As System.Int32
    End Function

    '// Permite seleccionar el protocolo de intercambio de paquetes de informaci�n
    '//---------------------------------------------------------------------------
    <DllImport("winfis32.dll", EntryPoint:="SetModoEpson", CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function SetModoEpson(ByVal epson As Boolean) As System.Int32
    End Function

    '// Permite abrir el puerto serie RS-232
    '//-------------------------------------
    <DllImport("winfis32.dll", EntryPoint:="OpenComFiscal", CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function OpenComFiscal(ByVal puerto As System.Int32, ByVal mode As System.Int32) As System.Int32
    End Function

    '// Permite enviar el string de comando constru�do a la impresora fiscal a trav�s del puerto serie
    '//-----------------------------------------------------------------------------------------------
    <DllImport("winfis32.dll", EntryPoint:="MandaPaqueteFiscal", CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function MandaPaqueteFiscal(ByVal handler As System.Int32, ByVal Buffer As String) As System.Int32
    End Function

    '// Permite cerrar el puerto serie RS-232 utilizado
    '//------------------------------------------------
    <DllImport("winfis32.dll", EntryPoint:="CloseComFiscal", CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Sub CloseComFiscal(ByVal handler As System.Int32)
    End Sub

    '// Permite obtener el string de respuesta proveniente de la impresora fiscal, a trav�s del puerto serie
    '//-----------------------------------------------------------------------------------------------------
    <DllImport("winfis32.dll", EntryPoint:="UltimaRespuesta", CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function UltimaRespuesta(ByVal handler As System.Int32, ByVal Buffer As StringBuilder) As System.Int32
    End Function

    '//=====================================================================================================================================
    '// Rutina que imprime el reporte 'X' y consulta fecha y hora
    '//=====================================================================================================================================
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim ret As System.Int32                         '// Valor retornado por la funci�n de la DLL
        Dim handler As System.Int32                     '// Identificador del COM a trav�s del cual se intercambian comandos y respuesta
        Dim comando As String = ""                      '// String de comando a enviar a la impresora fiscal
        Dim puerto As System.Int32 = 4                 '// Identificaci�n del COM
        Dim modo As System.Int32 = 1                    '// Modo ANSI
        Dim respuesta As New StringBuilder(500)         '// Buffer donde alojar la respuesta de la impreosra fiscal

        handler = OpenComFiscal(puerto, modo)           '// Abre el puerto serie
        ret = InitFiscal(handler)                       '// Env�a dos pedidos de status

        '// Reporte "X"
        '//-------------
        comando = "9" & Chr(28) & "X"                                    '// Comando DailyClose
        Console.WriteLine("DailyCLose (X) : " & comando)
        ret = MandaPaqueteFiscal(handler, comando)                       '// Se env�a a la impresora fiscal

        ret = UltimaRespuesta(handler, respuesta)                        '// Se obtiene la respuesta de la impresora fiscal
        Console.WriteLine("Respuesta      : " & respuesta.ToString)      '// Se muestra en la ventan MS-DOS
        Console.WriteLine()

        '// Pedido de fecha y hora
        '//-----------------------
        comando = Chr(89)                                                '// Comando SetDateTime
        Console.WriteLine("GetDateTime : " & comando)
        ret = MandaPaqueteFiscal(handler, comando)                       '// Se env�a a la impresora fiscal

        ret = UltimaRespuesta(handler, respuesta)                        '// Se obtiene la respuesta de la impresora fiscal
        Console.WriteLine("Respuesta   : " & respuesta.ToString)         '// Se muestra en la ventan MS-DOS
        Console.WriteLine()

        CloseComFiscal(handler)                                           '// Se cierra el COM utilizado

    End Sub

End Class