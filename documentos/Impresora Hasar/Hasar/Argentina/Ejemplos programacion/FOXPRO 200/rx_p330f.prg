* ==========================================================================
* CIA. HASAR SAIC     Grupo HASAR - Dto. Software de Base
*                     Por Ricardo D. Cardenes
*
* Ejemplo:            Foxpro 2.0 
* Requiere:           Cargar fiscal.sys via archivo config.sys de la PC
*                     Disparar lptfis.exe en la sesion DOS donde correra su
*                     su aplicacion
*
* Valido para:        Impresoras fiscales HASAR
* Modelos:            SMH/P-330F 
* Consultar:          publfact.pdf - drivers.pdf
* 
* Emision de:         Recibo "X" ( Documento No Fiscal Homologado )
* ==========================================================================
set talk on
store space(500) to respuesta
Se = CHR(28)	                        
Fin = CHR(10)                           

* Se abre archivo para lectura / escritura
* ----------------------------------------
fp = fopen("c:\fisprn",2)

	* Si hay un documento abierto se cancela. Si no se pudo cancelar,
	* se intenta su cierre
	* Genera comandos: Cancel y CloseFiscalReceipt
	* ------------------------------------------------------------------	
	s = CHR(152) + Fin                                  
	=Enviar (s)
	
	s = "E" + Fin                                       
	=Enviar (s)
	
	* Genera comando: SetCustomerData
	* ---------------------------------------------------------------------
	s = "b" + Se + "Razon Social..." + Se + "99999999995" + Se + "I" + Se + "C" + Se + "Domicilio..." + Fin
	=Enviar (s)

	* Genera comando: SetEmbarkNumber
	* ---------------------------------------------------------------------
	s = CHR(147) + Se + "1" + Se + "9998-00000123" + Fin
	=Enviar (s)

	* Genera comando: OpenDNFH
	* ------------------------
	s = CHR(128) + Se + "x" + Se + "S" + Se + "1"	+ Fin
	=Enviar (s)

	* Genera comando: PrintLineitem
	* ------------------------------
	s = "B" + Se + "No se imprime..." + Se + "1.0" + Se + "1500.0" + Se + "0.0" + Se + "M" + Se + "0.0" + Se + "0" + Se + "T" + Fin
	=Enviar (s)

	* Genera comando: ReceiptText
	* ---------------------------
	s = CHR(151) + Se + "Texto 1 detalle en Recibo..." + Fin
	=Enviar(s)

	s = CHR(151) + Se + "Texto 2 detalle en Recibo..." + Fin
	=Enviar(s)

	* Genera comando: CloseDNFH
	* -------------------------
	s = CHR(129) + Fin
	=Enviar (s)

* Se cierra el archivo 
* --------------------
=fclose(fp)

* ==========================================================================
*  Funcion que envia un comando a la impresora fiscal
* ==========================================================================
function Enviar
parameters string

if fp < 0 
	Wait Window "No se puede abrir el puerto"
	quit
endif

* Se envia el comando
* -------------------
n = fwrite (fp, string)

if n <= 0
	? "Error enviando el comando"
	return -1
endif

* Respuesta de la impresora fiscal
* --------------------------------
respuesta = fread (fp, 500)

return 0

