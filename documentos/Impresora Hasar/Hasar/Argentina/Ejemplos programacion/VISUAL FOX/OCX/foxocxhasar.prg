************************************************************************
* Visual Fox ejemplo utilizando OCX fiscal010724.ocx 
* Modelo: SMH/P-615F, SMH/P-PR4F, SMH/PT-262F, SMH/P-715F, SMH/P-PR5F
*	 	  SMH/PT-272F, SMH/P-320F, SMH/PL-8F, SMH/P-321F, SMH/P-322F
*	   	  SMH/P-330F, SMH/PL-9F, SMH/P-425F, SMH/P-435F
*
* Depto. Software de Base
* Cia. Hasar SAIC
************************************************************************
LPARAMETERS ComFiscal

#define OCUPADO_CMD			"Un comando se ejecutando, espere por favor un momento !!!"
#define ESTACION_TICKET 	"T"
#define ESTACION_SLIP		"S"
#define TICKET				1
#define TICKET_FACTURA		2
#define TICKET_FACTURAPLUS	3
#define FACTURA				4

*****************************
* Modelos de Impresoras
*****************************
#define MODELO_614			1
#define MODELO_615			2
#define MODELO_PR4			3
#define MODELO_950			4
#define MODELO_951			5
#define MODELO_262			6
#define MODELO_PJ20			7
#define MODELO_P320			8

****************************
* Eventos de Impresora
****************************
#define P_PRINTER_ERROR			4
#define P_OFFLINE				8
#define P_JOURNAL_PAPER_LOW		16
#define P_RECEIPT_PAPER_LOW		32
#define P_SLIP_PLATEN_OPEN		256
#define P_DRAWER_CLOSE			16384

****************************
* Error Fiscal
****************************
#define F_FISCAL_MEMORY_FAIL	1
#define F_WORKING_MEMORY_FAIL	2
#define F_UNRECOGNIZED_COMMAND	8
#define F_INVALID_FIELD_DATA	16
#define F_INVALID_COMMAND		32
#define F_TOTAL_OVERFLOW		64
#define F_FISCAL_MEMORY_FULL	128
#define F_DATE_SET_FAIL			2048

ErrorComenzar = .F. && flag para detectar errores de comunicaci�n 
FS = chr(28) && SEPARADOR DE CAMPOS
CLEAR
IF VARTYPE(ComFiscal) # "N"
 ComFiscal= 1
ENDIF


oForm = CREATEOBJECT('frmComprobantes',ComFiscal)
oModelos = CREATEOBJECT('Modelos')
READ EVENTS
RELEASE oForm
RELEASE oModelos
RETURN

DEFINE CLASS frmComprobantes AS FORM
	
	AutoCenter      = .T.
	Caption         = 'Emisi�n de comprobantes'
	Width	        = 570
  	Height	        = 400
  	AlwaysOnTop 	= .T.
  	nPort	        = 0 
  	lConnected      = .F. 
  	ProcesandoCMD   = .F.
  	GrupoImpresoras = FACTURA
  	ModeloCF 		= MODELO_P320	
  
  	PROCEDURE Load
  		SYS(2333,0)
   		_VFP.AutoYield = .F. && para detectar los eventos de la OCX debe ser false
   	
   		RETURN
  	ENDPROC

  	PROCEDURE Unload
  		_VFP.AutoYield = .T.
   		RETURN
  	ENDPROC

	PROCEDURE QueryUnload
  		CLEAR EVENTS
	  	RETURN
  	ENDPROC

  	PROCEDURE Init
  		LPARAMETERS ComFiscal
  	
   		ThisForm.nPort = ComFiscal
   		This.AddObject('EditOut', 'bEditBox')
   		WITH This.EditOut
    	.Top      = 215
	    .Left     = 10
    	.Width    = This.Width - 20
	    .Height   = This.Height - (.Top+5)
    	.Readonly = .T.
	    .Value    = ''
    	.Visible  = .T.
   		ENDWITH
	
		This.AddObject('lblResp', 'label')
   		WITH This.lblResp
   		.Top      = 198
    	.Left     = 10
    	.Autosize = .T.
    	.Caption  = 'Respuesta del Controlador Fiscal'
    	.Visible  = .T.
    	.ForeColor = RGB(0,0,250)
   		ENDWITH
  
   		This.AddObject('txtRemotePort', 'rTextBox')
   		WITH This.txtRemotePort
    	.Top      = 30
    	.Left     = 10
    	.Width    = 50
    	.Enabled  = .T.
    	.ControlSource = 'ThisForm.nPort'
    	.Visible  = .T.
  	 	ENDWITH
   
   		This.AddObject('lblComFiscal', 'label')
   		WITH This.lblComFiscal
    	.Top      = 10
    	.Left     = 10
    	.AutoSize = .T.
    	.Caption  = 'Puerto'
    	.Visible  = .T.
   		ENDWITH
   
   		This.AddObject('cmdConnect', 'MyOpenPort')
   		WITH This.cmdConnect
    	.Top      = 30
    	.Left     = 420
    	.Height   = 24
    	.Width    = 100
    	.Visible  = .T.
    	.ForeColor = RGB(0,0,250)
   		ENDWITH
   
   		This.AddObject('cmdDisconnect', 'MyClosePort')
   		WITH this.cmdDisconnect
    	.Top      = 60
    	.Left     = 420
    	.Height   = 24
    	.Width    = 100
    	.Visible  = .T.
    	.ForeColor = RGB(250,0,0)
   		ENDWITH
  
   		This.AddObject('cmdReporteX', 'ReporteX')
   		WITH This.cmdReporteX
    	.Top     = 90
    	.Left    = 115
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.ForeColor = RGB(05,05,255)
   		ENDWITH

   		This.AddObject('cmdReporteZ', 'ReporteZ')
   		WITH This.cmdReporteZ
    	.Top     = 90
    	.Left    = 220
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.ForeColor = RGB(05,05,255)
   		ENDWITH
   
   		This.AddObject('cmdFacturaB', 'FacturaB')
   		WITH This.cmdFacturaB
    	.Top     = 145
    	.Left    = 115
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.ForeColor = RGB(05,05,255)
   		ENDWITH

   		This.AddObject('cmdFacturaA', 'FacturaA')
   		WITH This.cmdFacturaA
    	.Top     = 145
    	.Left    = 220
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.ForeColor = RGB(05,05,255)
   		ENDWITH

   		This.AddObject('cmdEstado', 'ConsultaEstado')
   		WITH This.cmdEstado
    	.Top     = 90
    	.Left    = 10
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.ForeColor = RGB(05,05,255)
   		ENDWITH

   		This.AddObject('cmdConsFinal', 'TckConsumidorFinal')
   		WITH This.cmdConsFinal
    	.Top     = 145
    	.Left    = 10
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.Wordwrap = .T.
    	.ForeColor = RGB(05,05,255)
   		ENDWITH
   		
   		This.AddObject('oFiscal', 'frmFiscal' )
   
   		This.AddObject('lblModelos', 'label')
   		WITH This.lblModelos
    	.Top      = 10
    	.Left     = 80
    	.AutoSize = .T.
    	.Caption  = 'Modelos'
    	.Visible  = .T.
   		ENDWITH
   
   		This.AddObject('LstModelos', 'Modelos')
   
   		This.AddObject('cbobox', 'rComboBox')
   		WITH This.cbobox
    	.Top     = 30
    	.Left    = 80
    	.Width   = 120
    	.Enabled = .T.
    	.Visible = .T.
    	FOR I = 1 TO This.LstModelos.CantidadDeModelos()
    		.AddItem (This.LstModelos.ObtenerDescripcionModelo(I) )
 	    ENDFOR
 	    .Value = .List(11)
 	    This.ModeloCF = MODELO_P320
   		ENDWITH
	
   		This.Refresh()
   		This.Visible = .T.
   		RETURN
   
  	ENDPROC
 
  	PROCEDURE MostrarRespuesta
  		LPARAMETER Resp
  
  		WITH Thisform.EditOut
		.Value = .Value + CHR(13) + CHR(10) + Resp + CHR(13) + CHR(10)
    	.Refresh()
  		EndWith
  	
  	ENDPROC
	
ENDDEFINE

DEFINE CLASS frmFiscal AS OleControl
	
  	OleClass = "hasar.fiscal.1"

  	PROCEDURE ImpresoraOcupada
  		WITH Thisform.EditOut
		.Value = .Value + CHR(13) + CHR(10) + "Impresora Ocupada "  + CHR(13) + CHR(10)
    	.Refresh()
   		EndWith
  	ENDPROC
  	
  	PROCEDURE EventoImpresora
  		LPARAMETER Flags
  	
  		Texto = "Evento Impresora: No especificado"
  		DO CASE
	  		CASE Flags = P_PRINTER_ERROR
	  		Texto = "Evento Impresora: Error mec�nico de impresora"
	  		CASE Flags = P_OFFLINE
	  		Texto = "Evento Impresora: Impresora fuera de l�nea"
	  		CASE Flags = P_SLIP_PLATEN_OPEN
	  		Texto = "Evento Impresora: Tapa abierta de impresora"
  		ENDCASE
  		  		
  		ThisForm.MostrarRespuesta(Texto)

  	ENDPROC

  	PROCEDURE ErrorFiscal
  		LPARAMETER Flags

  		Texto = "Error Fiscal: No especificado"
  		DO CASE
	  		CASE Flags = F_FISCAL_MEMORY_FAIL
	  		Texto = "Error Fiscal: Error chequeo de memoria fiscal"
	  		CASE Flags = F_WORKING_MEMORY_FAIL
	  		Texto = "Error Fiscal: Error chequeo de la memoria de trabajo"
	  		CASE Flags = F_UNRECOGNIZED_COMMAND
	  		Texto = "Error Fiscal: Tapa abierta de impresora"
  	 		CASE Flags = F_INVALID_FIELD_DATA
	  		Texto = "Error Fiscal: Campos de datos inv�lido"
			CASE Flags = F_INVALID_COMMAND
	  		Texto = "Error Fiscal: Comando inv�lido para el estado actual"
	 		CASE Flags = F_TOTAL_OVERFLOW
	  		Texto = "Error Fiscal: Desborde de total"
  	 		CASE Flags = F_FISCAL_MEMORY_FULL
	  		Texto = "Error Fiscal: Memoria fiscal llena"	
  	 		CASE Flags = F_DATE_SET_FAIL
	  		Texto = "Error Fiscal: Error ingreso de fecha"	
  		ENDCASE
		ThisForm.MostrarRespuesta(Texto)
  	ENDPROC
  	
  	PROCEDURE Error
		LPARAMETERS nIndex, nError, cMethod, nLine
	
		texto = 'Error number: ' + LTRIM(STR(merror))
		ThisForm.MostrarRespuesta(texto)
		texto = 'Error message: ' + mess
		ThisForm.MostrarRespuesta(texto)
		texto = 'Line of code with error: ' + mess1
		ThisForm.MostrarRespuesta(texto)
		texto = 'Line number of error: ' + LTRIM(STR(mlineno))
		ThisForm.MostrarRespuesta(texto)
		texto = 'Program with error: ' + mprog
		ThisForm.MostrarRespuesta(texto)
	
 	ENDPROC
 
ENDDEFINE 

DEFINE CLASS FacturaB AS CommandButton
	Caption = 'Factura B'

  	PROCEDURE Refresh
   		This.Enabled = ThisForm.lConnected
		IF ThisForm.GrupoImpresoras = FACTURA
   			This.Caption = 'Factura B'
   		ELSE
	   		This.Caption = 'Ticket Factura B'
   		ENDIF
   		IF ThisForm.GrupoImpresoras = TICKET
	  		This.Enabled = .F.
		ENDIF
  	ENDPROC
	
  	PROCEDURE Click

		Resp = SPACE(500)
  		WITH ThisForm.oFiscal
  	
		IF (ThisForm.ProcesandoCMD = .T.)
			MessageBox(OCUPADO_CMD)
			RETURN
		ENDIF
   		
		ThisForm.ProcesandoCMD = .T.
		* Cargar datos del cliente	
		DO CASE
			CASE ThisForm.GrupoImpresoras = TICKET_FACTURA
				.DatosCliente('Cliente', '20101101', 50, 67)
			CASE ThisForm.GrupoImpresoras = TICKET_FACTURAPLUS
				cmd = "b" + FS + "Nombre" + FS + "20101101" + FS + "C" + FS + "2" + FS + "Domicilio"
				.Enviar(cmd)
			CASE ThisForm.GrupoImpresoras = FACTURA	
				.DatosCliente('Cliente', '20101101', 50, 67, 'Domicilio')
			OTHERWISE
				RETURN
		ENDCASE

		Resp = .Respuesta(0)
		ThisForm.MostrarRespuesta(Resp)
		
		IF ThisForm.GrupoImpresoras = FACTURA
			.AbrirComprobanteFiscal(49) && FACTURA
		ELSE
			.AbrirComprobanteFiscal(66) && TICKET FACTURA
		ENDIF
		
		Resp = .Respuesta(0)
		ThisForm.MostrarRespuesta(Resp)
		
		.ImprimirItem('Art. Nro. 1', 1, 2, 21, 0)
		Resp = .Respuesta(0)
		ThisForm.MostrarRespuesta(Resp)
		
		.CerrarComprobanteFiscal
		Resp = .Respuesta(0)
		ThisForm.MostrarRespuesta(Resp)
	    ThisForm.ProcesandoCMD = .F.
		ENDWITH
	
   		ThisForm.Refresh()
	ENDPROC
	
ENDDEFINE

DEFINE CLASS FacturaA AS CommandButton
	Caption = 'Factura A'

  	PROCEDURE Refresh
  		This.Enabled = ThisForm.lConnected
   		IF ThisForm.GrupoImpresoras = FACTURA
   			This.Caption = 'Factura A'
   		ELSE
	   		This.Caption = 'Ticket Factura A'
   		ENDIF
   	
   		IF ThisForm.GrupoImpresoras = TICKET
	  		This.Enabled = .F.
		ENDIF
  	ENDPROC
	
  	PROCEDURE Click

  		WITH ThisForm.oFiscal
  	
		IF (ThisForm.ProcesandoCMD = .T.)
			MessageBox(OCUPADO_CMD)
			RETURN
		ENDIF
		
		ThisForm.ProcesandoCMD = .T.
		* Cargar datos del cliente	
		DO CASE
			CASE ThisForm.GrupoImpresoras = TICKET_FACTURA
				.DatosCliente('Cliente', '99999999995', 67, 73)
			CASE ThisForm.GrupoImpresoras = TICKET_FACTURAPLUS
				cmd = "b" + FS + "Nombre" + FS + "99999999995" + FS + "I" + FS + "C" + FS + "Domicilio"
				.Enviar(cmd)
			CASE ThisForm.GrupoImpresoras = FACTURA	
				.DatosCliente('Cliente', '99999999995', 67, 73, 'Domicilio')
			OTHERWISE
				RETURN
		ENDCASE

		Resp = .Respuesta(0)
		ThisForm.MostrarRespuesta(Resp)
		
		IF ThisForm.GrupoImpresoras = FACTURA
			.AbrirComprobanteFiscal(48) && FACTURA
		ELSE
			.AbrirComprobanteFiscal(65) && TICKET FACTURA
		ENDIF
		
		Resp = .Respuesta(0)
		ThisForm.MostrarRespuesta(Resp)
	
		FOR I = 1 TO 3
		
			.ImprimirItem('Art. Nro.' + ALLTRIM(STR(I)), 1, 2, 21, 0)
			Resp = .Respuesta(0)
			ThisForm.MostrarRespuesta(Resp)	
		
		ENDFOR
		
		.CerrarComprobanteFiscal
		Resp = .Respuesta(0)
		ThisForm.MostrarRespuesta(Resp)
		ThisForm.ProcesandoCMD = .F.
		
		ENDWITH
	
   		ThisForm.Refresh()
	ENDPROC
	
ENDDEFINE

DEFINE CLASS ConsultaEstado AS CommandButton
	Caption = 'Estado'
  
  	PROCEDURE Refresh
  		This.Enabled = ThisForm.lConnected
   	ENDPROC

	PROCEDURE Click

		Resp = SPACE(500)
		IF (ThisForm.ProcesandoCMD = .T.)
			MessageBox(OCUPADO_CMD)
			RETURN
		ENDIF
		
		ThisForm.ProcesandoCMD = .T.
		WITH ThisForm.ofiscal
			.PedidoDeStatus
			Resp = .Respuesta(0) && respuesta completa 
			ThisForm.MostrarRespuesta(Resp)
		ENDWITH
		ThisForm.ProcesandoCMD = .F.
		
   		ThisForm.Refresh()
   		
  	ENDPROC  
  	
ENDDEFINE


DEFINE CLASS TckConsumidorFinal AS CommandButton

	Caption = 'Ticket' + chr(13) + 'A Consumidor Final'
  
  	PROCEDURE Refresh
		IF NOT ThisForm.GrupoImpresoras = FACTURA
	  	  	This.Enabled = .T.
	  	ELSE
		  	This.Enabled = .F.
	   	ENDIF
   		This.Enabled = ThisForm.lConnected	   		
  	ENDPROC

	PROCEDURE Click
	
   		WITH ThisForm.oFiscal
		IF (ThisForm.ProcesandoCMD = .T.)
			MessageBox(OCUPADO_CMD)
			RETURN
		ENDIF
		
		ThisForm.ProcesandoCMD = .T.
		.AbrirComprobanteFiscal(84)	
		Resp = .Respuesta(0)
		ThisForm.MostrarRespuesta(Resp)

		FOR I = 1 TO 3
			.ImprimirItem('Art. Nro.' + ALLTRIM(STR(I)), 1, 2, 21, 0)
			Resp = .Respuesta(0)
			ThisForm.MostrarRespuesta(Resp)	
		ENDFOR

		.CerrarComprobanteFiscal
		Resp = .Respuesta(0)
		ThisForm.MostrarRespuesta(Resp)
	    ThisForm.ProcesandoCMD = .F.
   		ENDWITH
   	
   		ThisForm.Refresh()
  	ENDPROC  
  	
ENDDEFINE


DEFINE CLASS ReporteX AS CommandButton
	Caption = 'Reporte X'

  	PROCEDURE Refresh
   		This.Enabled = ThisForm.lConnected
   	ENDPROC
	
  	PROCEDURE Click
  	
  		Resp = SPACE(500)
   		WITH ThisForm.oFiscal
		IF (ThisForm.ProcesandoCMD = .T.)
			MessageBox(OCUPADO_CMD)
			RETURN
		ENDIF
		ThisForm.ProcesandoCMD = .T.
		.ReporteX
		Resp = .Respuesta(0)
		ThisForm.MostrarRespuesta(Resp)
		ThisForm.ProcesandoCMD = .F.
   		ENDWITH
   	
   		ThisForm.Refresh()
  	ENDPROC
  	
ENDDEFINE

DEFINE CLASS ReporteZ AS CommandButton
	Caption = 'Reporte Z'

  	PROCEDURE Refresh
   		This.Enabled = ThisForm.lConnected
   	ENDPROC
	
  	PROCEDURE Click
  	
  		Resp = SPACE(500)
   		WITH ThisForm.oFiscal
   	
		IF (ThisForm.ProcesandoCMD = .T.)
			MessageBox(OCUPADO_CMD)
			RETURN
		ENDIF
		ThisForm.ProcesandoCMD = .T.
		.ReporteZ
		Resp = .Respuesta(0)
		ThisForm.MostrarRespuesta(Resp)
        ThisForm.ProcesandoCMD = .F.
   		ENDWITH
   	
   		ThisForm.Refresh()
  	ENDPROC
  	
ENDDEFINE

DEFINE CLASS MyOpenPort AS CommandButton
	Caption = 'Abrir puerto'

	PROCEDURE Refresh
   		This.Enabled = Not ThisForm.lConnected
  	ENDPROC
	
  	PROCEDURE Click
  	
  		ErrorComenzar = .F.
  		ON ERROR DO AtraparError WITH ERROR( ), MESSAGE( ), MESSAGE(1), PROGRAM( ), LINENO( )
   		WITH ThisForm.oFiscal
	    .Puerto = ThisForm.nPort
	    * Los modelos 715F, PR5F, 321F, 322F, 330F, PL9F, 425F, 435F no est�n soportados por la
	    * OCX fiscal010722.ocx entonces debemos seleccionar un modelo de similar caracter�stica 
	    .Modelo = ThisForm.ModeloCF
	    .Comenzar
	    .TratarDeCancelarTodo 	&& cancelar cualquier comprobante abierto

   		ENDWITH
   	
   		IF ErrorComenzar = .T.
   	   		ThisForm.lConnected = .F. 
		ELSE   	   	
			ThisForm.lConnected = .T.
		ENDIF
  		ThisForm.Refresh()

   	ENDPROC
  	
ENDDEFINE

DEFINE CLASS MyClosePort AS CommandButton
	Caption = 'Cerrar puerto'

  	PROCEDURE Refresh
   		This.Enabled = ThisForm.lConnected
  	ENDPROC
	
  	PROCEDURE Click
   		Thisform.oFiscal.Finalizar()
 		ThisForm.lConnected = .F.
   		ThisForm.Refresh()
  	ENDPROC
 	
ENDDEFINE

DEFINE CLASS rTextBox AS TextBox

	PROCEDURE Refresh
   		This.Enabled = NOT ThisForm.lConnected
  	ENDPROC
  	
ENDDEFINE

DEFINE CLASS r2TextBox AS TextBox

	PROCEDURE Refresh
   		This.Enabled = ThisForm.lConnected
  	ENDPROC
  	
ENDDEFINE

DEFINE CLASS bEditBox AS EditBox

	PROCEDURE refresh
   		WITH This
    	.SelStart = len(.text)
   		ENDWITH
  	ENDPROC
  	
ENDDEFINE 

DEFINE CLASS rComboBox AS ComboBox

	PROCEDURE Refresh
   		This.Enabled = NOT ThisForm.lConnected
  	ENDPROC
  	
  	PROCEDURE Click
  	
  		ThisForm.GrupoImpresoras = ThisForm.LstModelos.ObtenerGrupoModelo(This.ListIndex)
  		ThisForm.ModeloCF = ThisForm.LstModelos.ObtenerIDModelo(This.ListIndex)
  		ThisForm.cmdFacturaB.Refresh()
 		ThisForm.cmdFacturaA.Refresh()
  	ENDPROC
  	
ENDDEFINE

DEFINE CLASS Modelos AS Custom

	#DEFINE CANT_MODELOS	14
	#DEFINE CANT_COLUMNAS	2
	
	PROTECTED DIMENSION aModelos(CANT_MODELOS, CANT_COLUMNAS) 	
	
	PROCEDURE Init

		STORE "SMH/P-615F" TO This.aModelos(1,1) 
		STORE TICKET_FACTURA TO This.aModelos(1,2) 
	 	STORE "SMH/P-PR4F" TO This.aModelos(2,1) 
 		STORE TICKET_FACTURA TO This.aModelos(2,2) 
	   	STORE "SMH/PT-262F" TO This.aModelos(3,1) 
   		STORE TICKET TO This.aModelos(3,2) 
	   	STORE "SMH/P-715F" TO This.aModelos(4,1)
   		STORE TICKET_FACTURAPLUS TO This.aModelos(4,2)  
	    STORE "SMH/P-PR5F" TO This.aModelos(5,1)
    	STORE TICKET_FACTURAPLUS TO This.aModelos(5,2) 
	 	STORE "SMH/PT-272F" TO This.aModelos(6,1) 
 		STORE TICKET TO This.aModelos(6,2) 
	   	STORE "SMH/P-320F" TO This.aModelos(7,1)
   		STORE FACTURA TO This.aModelos(7,2) 
	   	STORE "SMH/PL-8F" TO This.aModelos(8,1) 
   		STORE FACTURA TO This.aModelos(8,2)
	   	STORE "SMH/P-321F" TO This.aModelos(9,1) 
   		STORE FACTURA TO This.aModelos(9,2)
	 	STORE "SMH/P-322F" TO This.aModelos(10,1) 
 		STORE FACTURA TO This.aModelos(10,2)
	   	STORE "SMH/P-330F" TO This.aModelos(11,1) 
   		STORE FACTURA TO This.aModelos(11,2)
	   	STORE "SMH/PL-9F" TO This.aModelos(12,1) 
   		STORE FACTURA TO This.aModelos(12,2)
	 	STORE "SMH/P-425F" TO This.aModelos(13,1) 
 		STORE TICKET_FACTURAPLUS TO This.aModelos(13,2)
		STORE "SMH/P-435F" TO This.aModelos(14,1) 
	 	STORE TICKET_FACTURAPLUS TO This.aModelos(14,2)
 	ENDPROC
 	
	FUNCTION ObtenerDescripcionModelo
		LPARAMETERS nIndex
	
		RETURN this.aModelos(nIndex,1)
	ENDFUNC
	
	FUNCTION ObtenerGrupoModelo
		LPARAMETERS nIndex
	
		RETURN this.aModelos(nIndex,2)
	ENDFUNC
 	
 	FUNCTION CantidadDeModelos
	 	RETURN CANT_MODELOS
 	ENDFUNC
 	
 	FUNCTION ObtenerIDModelo
 		LPARAMETERS nIndex
 		
	 	m_Modelo = MODELO_615	
 		DO CASE
 			CASE nIndex = 1
 				m_Modelo = MODELO_615
 			CASE nIndex = 2
 				m_Modelo = MODELO_PR4
 			CASE nIndex = 3
	 			m_Modelo = MODELO_262
 			CASE nIndex = 4
				m_Modelo = MODELO_615
 			CASE nIndex = 5
	 			m_Modelo = MODELO_PR4
 			CASE nIndex = 6
 				m_Modelo = MODELO_262
 			CASE nIndex = 7
 				m_Modelo = MODELO_P320
 			CASE nIndex = 8
 				m_Modelo = MODELO_P320
 			CASE nIndex = 9
 				m_Modelo = MODELO_P320
 			CASE nIndex = 10
 				m_Modelo = MODELO_P320
 			CASE nIndex = 11
 				m_Modelo = MODELO_P320
 			CASE nIndex = 12
 				m_Modelo = MODELO_P320
 			CASE nIndex = 13
 				m_Modelo = MODELO_P320
 			CASE nIndex = 14
 				m_Modelo = MODELO_P320
 		ENDCASE
 		RETURN m_Modelo
 	ENDFUNC
ENDDEFINE

PROCEDURE AtraparError
  	PARAMETER merror, mess, mess1, mprog, mlineno

	ErrorComenzar = .T.
	MessageBox('Error number: ' + LTRIM(STR(merror)) + chr(13) + ;
	'Error message: ' + mess + chr(13) + ;
	'Line of code with error: ' + mess1 + chr(13) + ;
	'Line number of error: ' + LTRIM(STR(mlineno)) + chr(13) + ;
	'Program with error: ' + mprog)

ENDPROC
