************************************************************************
* Visual Fox ejemplo utilizando Winsock (TCP/IP)
* Modelo: SMH/P-615F, SMH/P-PR4F, SMH/PT-262F, SMH/P-715F, SMH/P-PR5F
*	 	  SMH/PT-272F, SMH/P-320F, SMH/PL-8F, SMH/P-321F, SMH/P-322F
*	   	  SMH/P-330F, SMH/PL-9F, SMH/P-425F, SMH/P-435F
*
* Depto. Software de Base
* Cia. Hasar SAIC
************************************************************************
LPARAMETERS tcpPort, strRemoteHost

#define OCUPADO_CMD			"Un comando se ejecutando, espere por favor un momento !!!"
#define ESTACION_TICKET 	"T"
#define ESTACION_SLIP		"S"
#define TICKET				1
#define TICKET_FACTURA		2
#define TICKET_FACTURAPLUS	3
#define FACTURA				4

FS = chr(28)
CLEAR
IF VARTYPE(tcpPort) # "N"
 tcpPort = 1600
ENDIF
IF VARTYPE(strRemoteHost) # "C"
 strRemoteHost = "localhost"
ENDIF

oForm = CREATEOBJECT('frmTCPClient',tcpPort,strRemoteHost)
oModelos = CREATEOBJECT('Modelos')
READ EVENTS
RELEASE oForm
RELEASE oModelos
RETURN

DEFINE CLASS frmTCPClient AS FORM
	
  AutoCenter = .T.
  Caption    = 'TCP-Cliente'
  Width	     = 570
  Height	 = 400
  AlwaysOnTop = .T.

  nProtocol       = 0
  nPort	          = 0 
  RemoteHost      = ""
  nStat           = 0
  lConnected      = .F. 
  ProcesandoCMD   = .F.
  GrupoImpresoras = FACTURA
  
  PROCEDURE Load
  	SYS(2333,0)
   	_VFP.AutoYield = .F.
   	RETURN
  ENDPROC

  PROCEDURE Unload
  	_VFP.AutoYield = .T.
   	RETURN
  ENDPROC

  PROCEDURE QueryUnload
  	CLEAR EVENTS
  	RETURN
  ENDPROC

  PROCEDURE Init
  	LPARAMETERS tcpPort, strRemoteHost
  	
   	ThisForm.nPort      = tcpPort
   	ThisForm.RemoteHost = strRemoteHost
   	This.AddObject('EditOut', 'bEditBox')
   	WITH This.EditOut
    	.Top      = 215
	    .Left     = 10
    	.Width    = This.Width - 20
	    .Height   = This.Height - (.Top+5)
    	.Readonly = .T.
	    .Value    = ''
    	.Visible  = .T.
   	ENDWITH
	
   This.AddObject('lblResp', 'label')
   WITH This.lblResp
   		.Top      = 198
    	.Left     = 10
    	.Autosize = .T.
    	.Caption  = 'Respuesta del Controlador Fiscal'
    	.Visible  = .T.
    	.ForeColor = RGB(0,0,250)
   ENDWITH
   This.AddObject('txtRMHost', 'rTextBox')
   WITH This.txtRMHost
    	.Top      = 30
    	.Left     = 10
    	.Width    = 100
    	.Enabled  = .T.
    	.ControlSource = 'ThisForm.RemoteHost'
    	.Visible  = .T.
   ENDWITH
   
   This.AddObject('lblRemoteHost', 'label')
   WITH This.lblRemoteHost
   		.Top      = 10
    	.Left     = 10
    	.Autosize = .T.
    	.Caption  = 'RemoteHost'
    	.Visible  = .T.
   ENDWITH
   
   This.AddObject('txtRemotePort', 'rTextBox')
   WITH This.txtRemotePort
    	.Top      = 30
    	.Left     = 130
    	.Width    = 50
    	.Enabled  = .T.
    	.ControlSource = 'ThisForm.nPort'
    	.Visible  = .T.
   ENDWITH
   
   This.AddObject('lblTCPPort', 'label')
   WITH This.lblTCPPort
    	.Top      = 10
    	.Left     = 130
    	.AutoSize = .T.
    	.Caption  = 'TCP-Port'
    	.Visible  = .T.
   ENDWITH
   
   This.AddObject('cmdConnect', 'MyConn')
   WITH This.cmdConnect
    	.Top      = 30
    	.Left     = 420
    	.Height   = 24
    	.Width    = 100
    	.Visible  = .T.
    	.ForeColor = RGB(0,0,250)
   ENDWITH
   
   This.AddObject('cmdDisconnect', 'MyDisconn')
   WITH this.cmdDisconnect
    	.Top      = 60
    	.Left     = 420
    	.Height   = 24
    	.Width    = 100
    	.Visible  = .T.
    	.ForeColor = RGB(250,0,0)
   ENDWITH
  
   This.AddObject('cmdReporteX', 'ReporteX')
   WITH This.cmdReporteX
    	.Top     = 90
    	.Left    = 115
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.ForeColor = RGB(05,05,255)
   ENDWITH

	This.AddObject('cmdReporteZ', 'ReporteZ')
   WITH This.cmdReporteZ
    	.Top     = 90
    	.Left    = 220
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.ForeColor = RGB(05,05,255)
   ENDWITH
   
   This.AddObject('cmdFacturaB', 'FacturaB')
   WITH This.cmdFacturaB
    	.Top     = 145
    	.Left    = 115
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.ForeColor = RGB(05,05,255)
   ENDWITH

   This.AddObject('cmdFacturaA', 'FacturaA')
   WITH This.cmdFacturaA
    	.Top     = 145
    	.Left    = 220
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.ForeColor = RGB(05,05,255)
   ENDWITH

   This.AddObject('cmdEstado', 'ConsultaEstado')
   WITH This.cmdEstado
    	.Top     = 90
    	.Left    = 10
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.ForeColor = RGB(05,05,255)
   ENDWITH

   This.AddObject('cmdConsFinal', 'TckConsumidorFinal')
   WITH This.cmdConsFinal
    	.Top     = 145
    	.Left    = 10
    	.Height  = 50
    	.Width   = 100
    	.Visible = .T.
    	.Wordwrap = .T.
    	.ForeColor = RGB(05,05,255)
   ENDWITH
   
   This.AddObject('oSock', 'frmSock' )
   This.AddObject('txtStat', 'TextBox')
   WITH This.txtStat
    	.Top     = 30
    	.Left    = 200
    	.Width   = 20
    	.Enabled = .F.
    	.ControlSource = 'Thisform.oSock.State'
    	.Visible = .T.
   ENDWITH

   This.AddObject('lblModelos', 'label')
   WITH This.lblModelos
    	.Top      = 10
    	.Left     = 240
    	.AutoSize = .T.
    	.Caption  = 'Modelos'
    	.Visible  = .T.
   ENDWITH
   This.AddObject('LstModelos', 'Modelos')
   This.AddObject('cbobox', 'rComboBox')
   WITH This.cbobox
    	.Top     = 30
    	.Left    = 240
    	.Width   = 120
    	.Enabled = .T.
    	.Visible = .T.
    	FOR I = 1 TO This.LstModelos.CantidadDeModelos()
    		.AddItem (This.LstModelos.ObtenerDescripcionModelo(I) )
 	    ENDFOR
 	    .Value = .List(11) 
   ENDWITH
   This.Refresh()
   This.Visible = .T.
   RETURN
   
  ENDPROC
  
  PROCEDURE EsperarRespuesta

  	* demora 500 ms porque la impresora fiscal devuelve cada 500 ms aprox.
  	* el estado del comando.
  	=inkey(.5)
	* espero la respuesta completa de la impresora fiscal
	DO WHILE ThisForm.ProcesandoCMD = .T.
		=inkey(.25)
	ENDDO
	
  ENDPROC
	
ENDDEFINE

DEFINE CLASS  frmSock AS OleControl
  	OleClass = "MSWinsock.Winsock"
 
  	cReceiveBuffer = ''					
  	PROCEDURE Error
  	LPARAMETERS Number, Description, Scode, Source, Helpfile, Helpcontext, Canceldisplay

   	WITH Thisform.EditOut
   		.Value = .Value + CHR(13) + CHR(10) +;
    	"Error : " + STR(Number) +'  - ' + Description + CHR(13) + CHR(10)
    	.Refresh()
   	EndWith
	ENDPROC

	PROCEDURE Close
   		This.Object.Close()
	ENDPROC

	PROCEDURE Destroy
   		This.Object.Close()
	ENDPROC

	PROCEDURE DataArrival
   		LPARAMETERS nByteCount
	   	LOCAL lcBuffer
	   	
   		ThisForm.ProcesandoCMD = .T.
   		lcBuffer = SPACE(nByteCount)
	   	This.GetData( @lcBuffer, , nByteCount )
   		This.cReceiveBuffer = This.cReceiveBuffer + lcBuffer
	   	With ThisForm.EditOut
    		.Value = .Value+This.cReceiveBuffer+ chr(13)+ chr(10)
     		ThisForm.refresh()
    	Endwith
    	
    	This.cReceiveBuffer = ''
    	IF (lcBuffer = "DC2") OR (lcBuffer = "DC4")
	    	ThisForm.ProcesandoCMD = .T.
	    ELSE
	    	ThisForm.ProcesandoCMD = .F.
	    ENDIF
    	RETURN
	ENDPROC
	
ENDDEFINE

DEFINE CLASS FacturaB AS CommandButton
  Caption = 'Factura B'

  PROCEDURE Refresh
   This.Enabled = ThisForm.lConnected
	IF ThisForm.GrupoImpresoras = FACTURA
   		This.Caption = 'Factura B'
   	ELSE
	   	This.Caption = 'Ticket Factura B'
   	ENDIF
   	IF ThisForm.GrupoImpresoras = TICKET
	  	This.Enabled = .F.
	ENDIF
  ENDPROC
	
  PROCEDURE Click

  	WITH ThisForm.oSock
  	
	IF (ThisForm.ProcesandoCMD = .T.)
		MessageBox(OCUPADO_CMD)
		RETURN
	ENDIF
  	IF .state = 7	
		
		ThisForm.ProcesandoCMD = .F.
		DO CASE
		CASE ThisForm.GrupoImpresoras = TICKET_FACTURA
			cmd = "b" + FS + "Nombre" + FS + "2010101" + FS + "C" + FS + "2" 
		CASE ThisForm.GrupoImpresoras = FACTURA OR ThisForm.GrupoImpresoras = TICKET_FACTURAPLUS
			cmd = "b" + FS + "Nombre" + FS + "2010101" + FS + "C" + FS + "2" + FS + "Domicilio"
		OTHERWISE
			RETURN
		ENDCASE
		.SendData(cmd)
		ThisForm.EsperarRespuesta()
		
		IF ThisForm.GrupoImpresoras = TICKET_FACTURA
			cmd = "@" + FS + "B" + FS + ESTACION_TICKET 
		ELSE
			cmd = "@" + FS + "B" + FS + ESTACION_SLIP 
		ENDIF
		.SendData(cmd)
		ThisForm.EsperarRespuesta()
		
		cmd = "B" + FS + "Art. 1" + FS + "1.0" + FS + "0.50" + FS + "21.0" + ;
		FS + "M" + FS + "0.0" + FS + "0" + FS + "T" 
		.SendData(cmd)
		ThisForm.EsperarRespuesta()
	
		cmd = "E" 
		.SendData(cmd)
		ThisForm.EsperarRespuesta()

    ELSE
    	.Error( -1, 'Connection lost', , , , , .T. )
     	.Close()
     	ThisForm.lConnected = .F.
    ENDIF
	ENDWITH
	
   	ThisForm.Refresh()
	ENDPROC
	
ENDDEFINE

DEFINE CLASS FacturaA AS CommandButton
  Caption = 'Factura A'

  PROCEDURE Refresh
  	This.Enabled = ThisForm.lConnected
   	IF ThisForm.GrupoImpresoras = FACTURA
   		This.Caption = 'Factura A'
   	ELSE
	   	This.Caption = 'Ticket Factura A'
   	ENDIF
   	
   	IF ThisForm.GrupoImpresoras = TICKET
	  	This.Enabled = .F.
	ENDIF
  ENDPROC
	
  PROCEDURE Click

  	WITH ThisForm.oSock
  	
	IF (ThisForm.ProcesandoCMD = .T.)
		MessageBox(OCUPADO_CMD)
		RETURN
	ENDIF
  	IF .state = 7	
		
		ThisForm.ProcesandoCMD = .F.
		DO CASE
		CASE ThisForm.GrupoImpresoras = TICKET_FACTURA
			cmd = "b" + FS + "Nombre" + FS + "99999999995" + FS + "I" + FS + "C" 
		CASE ThisForm.GrupoImpresoras = FACTURA OR ThisForm.GrupoImpresoras = TICKET_FACTURAPLUS
			cmd = "b" + FS + "Nombre" + FS + "99999999995" + FS + "I" + FS + "C" + FS + "Domicilio"
		OTHERWISE
			RETURN
		ENDCASE
		.SendData(cmd)
		ThisForm.EsperarRespuesta()

		IF ThisForm.GrupoImpresoras = TICKET_FACTURA OR ThisForm.GrupoImpresoras = TICKET_FACTURAPLUS
			cmd = "@" + FS + "A" + FS + ESTACION_TICKET 
		ELSE
			cmd = "@" + FS + "A" + FS + ESTACION_SLIP 
		ENDIF
		.SendData(cmd)
		ThisForm.EsperarRespuesta()
	
		FOR I = 1 TO 3
		
			cmd = "B" + FS + "Art. " + ALLTRIM(STR(I)) + FS + "1.0" + FS + "0.50" + FS + "21.0" + ;
			FS + "M" + FS + "0.0" + FS + "0" + FS + "T" 
			.SendData(cmd)
			ThisForm.EsperarRespuesta()		
	
		ENDFOR
		
		cmd = "E" 
		.SendData(cmd)
		ThisForm.EsperarRespuesta()

    ELSE
    	.Error( -1, 'Connection lost', , , , , .T. )
     	.Close()
     	ThisForm.lConnected = .F.
    ENDIF
	ENDWITH
	
   	ThisForm.Refresh()
	ENDPROC
	
ENDDEFINE

DEFINE CLASS ConsultaEstado AS CommandButton
  Caption = 'Estado'
  
  PROCEDURE Refresh
   		This.Enabled = ThisForm.lConnected
   		
  ENDPROC

	PROCEDURE Click
   	WITH ThisForm.oSock
	IF (ThisForm.ProcesandoCMD = .T.)
		MessageBox(OCUPADO_CMD)
		RETURN
	ENDIF
    IF (.state = 7) 

		cmd = "*"
		.SendData(cmd)
		ThisForm.EsperarRespuesta()
    ELSE
    	.Error( -1, 'Connection lost', , , , , .T. )
     	.Close()
    	ThisForm.lConnected = .F.
    ENDIF
    
   	ENDWITH
   	
   	ThisForm.Refresh()
  	ENDPROC  
ENDDEFINE


DEFINE CLASS TckConsumidorFinal AS CommandButton

  Caption = 'Ticket' + chr(13) + 'A Consumidor Final'
  
  PROCEDURE Refresh

   		This.Enabled = ThisForm.lConnected
	  	IF NOT ThisForm.GrupoImpresoras = TICKET
	  	This.Enabled = .F.
	   	ENDIF	
  ENDPROC

	PROCEDURE Click
   	WITH ThisForm.oSock
	IF (ThisForm.ProcesandoCMD = .T.)
		MessageBox(OCUPADO_CMD)
		RETURN
	ENDIF
    IF (.state = 7) 

		cmd = "@" + FS + "T" + FS + ESTACION_TICKET  
		
		.SendData(cmd)
		ThisForm.EsperarRespuesta()
	
		FOR I = 1 TO 3
		
			cmd = "B" + FS + "Art. " + ALLTRIM(STR(I)) + FS + "1.0" + FS + "0.50" + FS + "21.0" + ;
			FS + "M" + FS + "0.0" + FS + "0" + FS + "T" 
			.SendData(cmd)
			ThisForm.EsperarRespuesta()		
	
		ENDFOR
		
		cmd = "E" 
		.SendData(cmd)
		ThisForm.EsperarRespuesta()
    ELSE
    	.Error( -1, 'Connection lost', , , , , .T. )
     	.Close()
    	ThisForm.lConnected = .F.
    ENDIF
    
   	ENDWITH
   	
   	ThisForm.Refresh()
  	ENDPROC  
ENDDEFINE


DEFINE CLASS  ReporteX AS CommandButton
	Caption = 'Reporte X'

  	PROCEDURE Refresh
   		This.Enabled = ThisForm.lConnected
   		
	ENDPROC
	
  	PROCEDURE Click
   	WITH ThisForm.oSock
	IF (ThisForm.ProcesandoCMD = .T.)
		MessageBox(OCUPADO_CMD)
		RETURN
	ENDIF
    IF (.state = 7) 

		cmd = "9" + FS + "X"
		.SendData(cmd)
		ThisForm.EsperarRespuesta()
    ELSE
    	.Error( -1, 'Connection lost', , , , , .T. )
     	.Close()
    	ThisForm.lConnected = .F.
    ENDIF
    
   	ENDWITH
   	
   	ThisForm.Refresh()
  	ENDPROC
  	
ENDDEFINE

DEFINE CLASS ReporteZ AS CommandButton
	Caption = 'Reporte Z'

  	PROCEDURE Refresh
   		This.Enabled = ThisForm.lConnected
   		
	ENDPROC
	
  	PROCEDURE Click
   	WITH ThisForm.oSock
	IF (ThisForm.ProcesandoCMD = .T.)
		MessageBox(OCUPADO_CMD)
		RETURN
	ENDIF
    IF (.state = 7) 

		cmd = "9" + FS + "Z"
		.SendData(cmd)
		ThisForm.EsperarRespuesta()
    ELSE
    	.Error( -1, 'Connection lost', , , , , .T. )
     	.Close()
    	ThisForm.lConnected = .F.
    ENDIF
    
   	ENDWITH
   	
   	ThisForm.Refresh()
  	ENDPROC
  	
ENDDEFINE

DEFINE CLASS  MyConn AS CommandButton
	Caption = 'Connect'

	PROCEDURE Refresh
   		This.Enabled = Not ThisForm.lConnected
  	ENDPROC
	
  	PROCEDURE Click
   	WITH ThisForm.oSock
   		IF .state != 0  							
    		.Object.Close()
   		ENDIF
   		.protocol   = 0 
   		.remoteHost = ALLTRIM(ThisForm.RemoteHost) 
	    .remotePort = ThisForm.nPort	  
   		.localPort  = 0   
   		.Object.Connect()
   		
   		DO WHILE .Object.state < 7
    		DOEVENTS	 
    		ThisForm.txtStat.Refresh() 
		    IF .state = 9
     			EXIT
    		ENDIF
   		ENDDO
    	IF .object.state = 7
     		ThisForm.lConnected = .T.
    	ELSE       
     		ThisForm.lConnected = .F.
    	ENDIF
   	ENDWITH
   
   	ThisForm.Refresh()
   	ENDPROC
  	
ENDDEFINE


DEFINE CLASS  MyDisconn AS CommandButton
	Caption = 'Disconnect'

  	PROCEDURE Refresh
   		This.Enabled = ThisForm.lConnected
  	ENDPROC
	
  	PROCEDURE Click
   		Thisform.oSock.Object.Close()
   		DO WHILE Thisform.oSock.state > 0
    		DOEVENTS
    		ThisForm.txtStat.Refresh()
   		ENDDO
   		ThisForm.lConnected = .F.
   		ThisForm.Refresh()
  	ENDPROC
  	
ENDDEFINE

DEFINE CLASS  rTextBox AS TextBox

	PROCEDURE Refresh
   		This.Enabled = NOT ThisForm.lConnected
  	ENDPROC
  	
ENDDEFINE

DEFINE CLASS  r2TextBox AS TextBox

	PROCEDURE Refresh
   		This.Enabled = ThisForm.lConnected
  	ENDPROC
  	
ENDDEFINE

DEFINE CLASS  bEditBox AS EditBox

	PROCEDURE refresh
   	WITH This
    	.SelStart = len(.text)
   	ENDWITH
  	ENDPROC
  	
ENDDEFINE 

DEFINE CLASS rComboBox AS ComboBox

	PROCEDURE Refresh
   		This.Enabled = NOT ThisForm.lConnected
  	ENDPROC
  	
  	PROCEDURE Click
  	
  		ThisForm.GrupoImpresoras = ThisForm.LstModelos.ObtenerGrupoModelo(This.ListIndex)
  		ThisForm.cmdFacturaB.Refresh()
 		ThisForm.cmdFacturaA.Refresh()
  	ENDPROC
  	
ENDDEFINE

DEFINE CLASS Modelos AS Custom

	#DEFINE CANT_MODELOS	14
	#DEFINE CANT_COLUMNAS	2
	
	PROTECTED DIMENSION aModelos(CANT_MODELOS, CANT_COLUMNAS) 	
	
	PROCEDURE Init

		STORE "SMH/P-615F" TO This.aModelos(1,1) 
		STORE TICKET_FACTURA TO This.aModelos(1,2) 
	 	STORE "SMH/P-PR4F" TO This.aModelos(2,1) 
 		STORE TICKET_FACTURA TO This.aModelos(2,2) 
	   	STORE "SMH/PT-262F" TO This.aModelos(3,1) 
   		STORE TICKET TO This.aModelos(3,2) 
	   	STORE "SMH/P-715F" TO This.aModelos(4,1)
   		STORE TICKET_FACTURAPLUS TO This.aModelos(4,2)  
	    STORE "SMH/P-PR5F" TO This.aModelos(5,1)
    	STORE TICKET_FACTURAPLUS TO This.aModelos(5,2) 
	 	STORE "SMH/PT-272F" TO This.aModelos(6,1) 
 		STORE TICKET TO This.aModelos(6,2) 
	   	STORE "SMH/P-320F" TO This.aModelos(7,1)
   		STORE FACTURA TO This.aModelos(7,2) 
	   	STORE "SMH/PL-8F" TO This.aModelos(8,1) 
   		STORE FACTURA TO This.aModelos(8,2)
	   	STORE "SMH/P-321F" TO This.aModelos(9,1) 
   		STORE FACTURA TO This.aModelos(9,2)
	 	STORE "SMH/P-322F" TO This.aModelos(10,1) 
 		STORE FACTURA TO This.aModelos(10,2)
	   	STORE "SMH/P-330F" TO This.aModelos(11,1) 
   		STORE FACTURA TO This.aModelos(11,2)
	   	STORE "SMH/PL-9F" TO This.aModelos(12,1) 
   		STORE FACTURA TO This.aModelos(12,2)
	 	STORE "SMH/P-425F" TO This.aModelos(13,1) 
 		STORE TICKET_FACTURAPLUS TO This.aModelos(13,2)
		STORE "SMH/P-435F" TO This.aModelos(14,1) 
	 	STORE TICKET_FACTURAPLUS TO This.aModelos(14,2)
 	ENDPROC
 	
	FUNCTION ObtenerDescripcionModelo
		LPARAMETERS nIndex
	
		RETURN this.aModelos(nIndex,1)
	ENDFUNC
	
	FUNCTION ObtenerGrupoModelo
		LPARAMETERS nIndex
	
		RETURN this.aModelos(nIndex,2)
	ENDFUNC
 	
 	FUNCTION CantidadDeModelos
	 	RETURN CANT_MODELOS
 	ENDFUNC
ENDDEFINE
