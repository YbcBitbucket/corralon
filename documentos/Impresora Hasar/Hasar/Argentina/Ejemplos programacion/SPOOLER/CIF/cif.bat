@echo off
rem ########################################################################
rem ##                                                                    ##
rem ##         Cia. HASAR saic - Depto. Software de Base                  ##
rem ##         por Ricardo D. Cardenes                                    ##
rem ##                                                                    ##
rem ##   Ejemplo de uso del programa SPOOLER.EXE en dos de sus formas:    ##
rem ##                                                                    ##
rem ##   a) Leyendo comandos desde archivo ( solamente en comercios au-   ##
rem ##      torizados por AFIP para impresion diferida )                  ##
rem ##                                                                    ##
rem ##   b) Invocacion comando a comando ( forma valida por Resolucion    ##
rem ##      AFIP )  -- IMPRESION CONCOMITANTE --                          ##
rem ##                                                                    ##
rem ##                                              Marzo 04, 2004        ##
rem ##                                                                    ##
rem ########################################################################

rem #----------------------------------------------------------------------#
rem # Este ejemplo requiere que el ejecutable SPOOLER.EXE este presente en #
rem # el mismo directorio que el BAT a ejecutar.                           #
rem # SPOOLER.EXE se encuentra en zip de drivers fiscales Hasar            #
rem # Para mayor informacion ver SPOOLER.DOC y PUBLICIF.PDF                #
rem #----------------------------------------------------------------------# 

cls
if "%1"=="" goto noparam
if exist spooler.log del spooler.log

rem ########################################################################
rem      EJEMPLOS DE EMISION DE DOCUMENTOS CON IMPRESORAS FISCALES
rem      MODELO:         SMH/P-425F 
rem      VERSIONES:      1.00 / 2.01
rem ########################################################################

:inicio

cls
echo .
echo .
echo .++++++++++++++++++++++ DOCUMENTO A EMITIR ++++++++++++++++++++++++++++
echo .                       ==================
echo .
echo .           	# En Form. Cont. / Hoja Suelta
echo .           	# ----------------------------
echo .
echo .           	1      Factura "A"
echo .           	2      Nota de Cr�dito "A"
echo .           	3      Recibo "X"
echo .           	4      Cotizaci�n o Presupuesto
echo .           	5      Remito / Orden de Salida
echo .           	6      Resumen de Cuenta / Cargo Habitaci�n
echo .           	7      Documento No Fiscal
echo .
echo .           	8      Otros Documentos
echo .
echo .           	9      -- ABORTAR -- Ejecuci�n del Programa
echo .
echo .++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo .
echo .

choice /c:123456789 ......Su opcion es...
if errorlevel 9 goto final
if errorlevel 8 goto masdoc
if errorlevel 7 goto gendnf
if errorlevel 6 goto gencta
if errorlevel 5 goto genrto
if errorlevel 4 goto gencot
if errorlevel 3 goto genrx
if errorlevel 2 goto gennc
if errorlevel 1 goto genfa

:masdoc

cls
echo .
echo .
echo .++++++++++++++++++++++ DOCUMENTO A EMITIR ++++++++++++++++++++++++++++
echo .                       ==================
echo .
echo .           	# En Rollo ( ticket / testigo )
echo .           	# -----------------------------
echo .
echo .           	1      Ticket Factura "A"
echo .           	2      Ticket Nota de Cr�dito "A"
echo .           	3      Ticket Recibo "X"
echo .           	4      Ticket Documento No Fiscal
echo .
echo .           	# En estaci�n default
echo .           	# -------------------
echo .
echo .           	5      Reporte X
echo .           	6      Reporte Auditor�a por N�meros Z
echo .           	7      Cierre Z
echo .
echo .           	8      -- VOLVER -- Men� anterior
echo .
echo .++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo .
echo .

choice /c:12345678 ......Su opcion es...
if errorlevel 8 goto inicio
if errorlevel 7 goto genz
if errorlevel 6 goto genaudz
if errorlevel 5 goto genx
if errorlevel 4 goto gentdnf
if errorlevel 3 goto gentrx
if errorlevel 2 goto gentnca
if errorlevel 1 goto gentfa

rem ########################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Reporte de Auditoria por Nro de Z                ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:genaudz

cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f repaud.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo repaud.425 con los comandos ejecutados
echo .
type repaud.425
echo .
pause

cls
echo .
echo ......Archivo repaud.ans respuestas a los comandos ejecutados
echo .
type repaud.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Reporte de Auditoria por Nro de Z                ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se pide imprimir el reporte ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . ;13800T
echo .
spooler -p%1  -e -c ";13800T"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto masdoc

rem ########################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Cierre Z                                         ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:genz

cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f repz.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo repz.425 con los comandos ejecutados
echo .
type repz.425
echo .
pause

cls
echo .
echo ......Archivo repz.ans respuestas a los comandos ejecutados
echo .
type repz.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Cierre Z                                         ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se pide imprimir el reporte ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . 9Z
echo .
spooler -p%1  -e -c "9Z"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto masdoc

rem ########################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Reporte X                                        ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:genx

cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f repx.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo repx.425 con los comandos ejecutados
echo .
type repx.425
echo .
pause

cls
echo .
echo ......Archivo repx.ans respuestas a los comandos ejecutados
echo .
type repx.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Reporte X                                        ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se pide imprimir el reporte ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . 9X
echo .
spooler -p%1  -e -c "9X"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto masdoc

rem ########################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket Documento No Fiscal                       ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:gentdnf
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f dnft.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo dnft.425 con los comandos ejecutados
echo .
type dnft.425
echo .
pause

cls
echo .
echo ......Archivo dnft.ans respuestas a los comandos ejecutados
echo .
type dnft.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket Documento No Fiscal                       ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se abre el Documento No Fiscal ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . H
echo .
spooler -p%1  -e -c "H"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . I�Esta es una linea de texto no fiscal0
echo .
spooler -p%1  -e -c "I�Esta es una linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . I�Esta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "I�Esta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Cierre del Documento No Fiscal ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . J
echo .
spooler -p%1  -e -c "J"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto masdoc

rem ########################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket Recibo "X"                                ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:gentrx
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f trx.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo trx.425 con los comandos ejecutados
echo .
type trx.425
echo .
pause

cls
echo .
echo ......Archivo trx.ans respuestas a los comandos ejecutados
echo .
type trx.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket Recibo "X"                                ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se envian los datos del cliente ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . bEmpresa Equis99999999995ICDomicilio: De la Empresa Equis
echo .
spooler -p%1  -e -c "bEmpresa Equis99999999995ICDomicilio: De la Empresa Equis"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se envia el numero de Comprobante Original ( Factura ) ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �19998-00000452
echo .
spooler -p%1  -e -c "�19998-00000452"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide abrir Recibo X ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �xT9998-00000001
echo .
spooler -p%1  -e -c "�xT9998-00000001"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BNo se imprime110000M00T
echo .
spooler -p%1  -e -c "BNo se imprime110000M00T"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Linea de texto en el Recibo ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Las facturas a Cuenta Corriente del mes de Noviembre del 2000 y seg�n el siguiente detalle:
echo .
spooler -p%1  -e -c "�Las facturas a Cuenta Corriente del mes de Noviembre del 2000 y seg�n el siguiente detalle:"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Linea de texto en el Recibo ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Desde la 9998-00000100 hasta la 9998-00000323 ( ambas inclusive )
echo .
spooler -p%1  -e -c "�Desde la 9998-00000100 hasta la 9998-00000323 ( ambas inclusive )"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Cierre del Recibo ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �
echo .
spooler -p%1  -e -c "�"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto masdoc

rem ######################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket Nota de Credito "A"                       ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:gentnca
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos de archivo a la impresora fiscal
echo .
echo . Por Favor...Espere !!
echo .
spooler -p%1  -f tnca.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo tnca.425 con los comandos ejecutados
echo .
type tnca.425
echo .
pause

cls
echo .
echo ......Archivo tnca.ans respuestas a los comandos ejecutados
echo .
type tnca.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket Nota de Credito "A"                       ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se envian los datos del cliente ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . bEl Cliente de Siempre99999999995ICDomicilio: El mismo de toda la vida
echo .
spooler -p%1  -e -c "bEl Cliente de Siempre99999999995ICDomicilio: El mismo de toda la vida"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se envia el numero de Comprobante Original ( Factura ) ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �19998-00000123
echo .
spooler -p%1  -e -c "�19998-00000123"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide abrir la Nota de Credito A ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �RT1
echo .
spooler -p%1  -e -c "�RT1"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BProducto Uno101021M00T
echo .
spooler -p%1  -e -c "BProducto Uno101021M00T"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BProducto Dos22021M%100T
echo .
spooler -p%1  -e -c "BProducto Dos22021M%%100T"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BProducto Tres33010.5M00T
echo .
spooler -p%1  -e -c "BProducto Tres33010.5M00T"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Cierre de la Nota de Credito ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �
echo .
spooler -p%1  -e -c "�"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto masdoc

rem ######################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket Factura "A"                               ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:gentfa
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f tfa.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo tfa.425 con los comandos ejecutados
echo .
type tfa.425
echo .
pause

cls
echo .
echo ......Archivo tfa.ans respuestas a los comandos ejecutados
echo .
type tfa.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket Factura "A"                               ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

rem ... Se le indica al spooler, con el parametro -m ...
rem ... que no muestre mensajes por pantalla         ...
rem ....................................................
cls
echo .
echo . *** Se envian los datos del cliente ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . bEmpresa Equis30702383923ICDomicilio Desconocido
echo .
spooler -m -p%1  -e -c "bEmpresa Equis30702383923ICDomicilio Desconocido"
if errorlevel 6 goto problem6
if errorlevel 5 goto problem5
if errorlevel 4 goto problem4
if errorlevel 3 goto problem3
if errorlevel 2 goto problem2
if errorlevel 1 goto problem1
goto sigo1

:problem6
echo .
echo . *********************************
echo . * Impresora Fiscal              *
echo . * ERROR Fatal en Comando Fiscal *
echo . *********************************
echo .
goto final

:problem5
echo .
echo . ****************************************
echo . * SPOOLER                              *
echo . * ERROR tratando de abrir puerto serie *
echo . ****************************************
echo .
goto final

:problem4
echo .
echo . ***********************************************
echo . * SPOOLER                                     *
echo . * ERROR tratando de abrir archivo de comandos *
echo . ***********************************************
echo .
goto final

:problem3
echo .
echo . *************************************************
echo . * SPOOLER                                       *
echo . * ERROR tratando de abrir archivo de respuestas *
echo . *************************************************
echo .
goto final

:problem2
echo .
echo . ******************************************** 
echo . * SPOOLER                                  *
echo . * ERROR de comunicaciones con la impresora *
echo . ********************************************
echo .
goto final

:sigo1
echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se pide abrir una Factura "A" ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . @AT
echo .
spooler -p%1  -e -c "@AT"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se realiza la venta de un item ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BItem Uno30.01.021.0M0.00b
echo .
spooler -p%1  -e -c "BItem Uno30.01.021.0M0.00b"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se realiza la venta de un item ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BItem Dos1.020.8110.0M0.8950b
echo .
spooler -p%1  -e -c "BItem Dos1.020.8110.0M0.8950b"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se pide el subtotal de lo facturado ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . CPSubtotal0
echo .
spooler -p%1  -e -c "CPSubtotal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se registra un pago ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . DEfectivo100.00T0
echo .
spooler -p%1  -e -c "DEfectivo100.00T0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se pide el cierre de la factura ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . E
echo .
spooler -p%1  -e -c "E"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

goto masdoc

rem ######################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de una Factura "A"                                     ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:genfa
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f fa.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo fa.425 con los comandos ejecutados
echo .
type fa.425
echo .
pause

cls
echo .
echo ......Archivo fa.ans respuestas a los comandos ejecutados
echo .
type fa.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de una Factura "A"                                     ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

rem ... Se le indica al spooler, con el parametro -m ...
rem ... que no muestre mensajes por pantalla         ...
rem ....................................................
cls
echo .
echo . *** Se envian los datos del cliente ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . bEmpresa Equis30702383923ICDomicilio Desconocido
echo .
spooler -m -p%1  -e -c "bEmpresa Equis30702383923ICDomicilio Desconocido"
if errorlevel 6 goto problem6
if errorlevel 5 goto problem5
if errorlevel 4 goto problem4
if errorlevel 3 goto problem3
if errorlevel 2 goto problem2
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se pide abrir una Factura "A" ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . @AS
echo .
spooler -p%1  -e -c "@AS"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se realiza la venta de un item ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BItem Uno30.01.021.0M0.00b
echo .
spooler -p%1  -e -c "BItem Uno30.01.021.0M0.00b"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se realiza la venta de un item ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BItem Dos1.020.8110.0M0.8950b
echo .
spooler -p%1  -e -c "BItem Dos1.020.8110.0M0.8950b"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se pide el subtotal de lo facturado ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . CPSubtotal0
echo .
spooler -p%1  -e -c "CPSubtotal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se registra un pago ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . DEfectivo100.00T0
echo .
spooler -p%1  -e -c "DEfectivo100.00T0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

cls
echo .
echo . *** Se pide el cierre de la factura ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . E
echo .
spooler -p%1  -e -c "E"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
echo ..........................................................
echo . La respuesta recibida debe ser analizada por el programa
echo . Si todo anduvo bien, entonces ...
echo . Enviar siguiente comando fiscal
echo ..........................................................
echo .
pause

goto inicio

rem ######################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de una Nota de Credito "A"                             ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:gennc
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos de archivo a la impresora fiscal
echo .
echo . Por Favor...Espere !!
echo .
spooler -p%1  -f nca.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo nca.425 con los comandos ejecutados
echo .
type nca.425
echo .
pause

cls
echo .
echo ......Archivo nca.ans respuestas a los comandos ejecutados
echo .
type nca.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de una Nota de Credito "A"                             ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se envian los datos del cliente ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . bEl Cliente de Siempre99999999995ICDomicilio: El mismo de toda la vida
echo .
spooler -p%1  -e -c "bEl Cliente de Siempre99999999995ICDomicilio: El mismo de toda la vida"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se envia el numero de Comprobante Original ( Factura ) ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �19998-00000123
echo .
spooler -p%1  -e -c "�19998-00000123"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide abrir la Nota de Credito A ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �RS1
echo .
spooler -p%1  -e -c "�RS1"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BProducto Uno101021M00T
echo .
spooler -p%1  -e -c "BProducto Uno101021M00T"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BProducto Dos22021M%100T
echo .
spooler -p%1  -e -c "BProducto Dos22021M%%100T"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BProducto Tres33010.5M00T
echo .
spooler -p%1  -e -c "BProducto Tres33010.5M00T"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Cierre de la Nota de Credito ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �
echo .
spooler -p%1  -e -c "�"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto inicio

rem ######################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Recibo "X"                                       ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:genrx
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f rx.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo rx.425 con los comandos ejecutados
echo .
type rx.425
echo .
pause

cls
echo .
echo ......Archivo rx.ans respuestas a los comandos ejecutados
echo .
type rx.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Recibo "X"                                       ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se envian los datos del cliente ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . bEmpresa Equis99999999995ICDomicilio: De la Empresa Equis
echo .
spooler -p%1  -e -c "bEmpresa Equis99999999995ICDomicilio: De la Empresa Equis"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se envia el numero de Comprobante Original ( Factura ) ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �19998-00000452
echo .
spooler -p%1  -e -c "�19998-00000452"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide abrir Recibo X ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �xS9998-00000001
echo .
spooler -p%1  -e -c "�xS9998-00000001"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BNo se imprime110000M00T
echo .
spooler -p%1  -e -c "BNo se imprime110000M00T"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Linea de texto en el Recibo ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Las facturas a Cuenta Corriente del mes de Noviembre del 2000 y seg�n el siguiente detalle:
echo .
spooler -p%1  -e -c "�Las facturas a Cuenta Corriente del mes de Noviembre del 2000 y seg�n el siguiente detalle:"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Linea de texto en el Recibo ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Desde la 9998-00000100 hasta la 9998-00000323 ( ambas inclusive )
echo .
spooler -p%1  -e -c "�Desde la 9998-00000100 hasta la 9998-00000323 ( ambas inclusive )"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Cierre del Recibo ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �
echo .
spooler -p%1  -e -c "�"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto inicio

rem ######################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de una Cotizacion o Presupuesto                        ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:gencot
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f cot.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo cot.425 con los comandos ejecutados
echo .
type cot.425
echo .
pause

cls
echo .
echo ......Archivo cot.ans respuestas a los comandos ejecutados
echo .
type cot.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de una Cotizacion o Presupuesto                        ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se envian los datos del cliente ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . bEl Cliente Potencial99999999995MCDomicilio: Del Cliente Potencial
echo .
spooler -p%1  -e -c "bEl Cliente Potencial99999999995MCDomicilio: Del Cliente Potencial"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide abrir la Cotizacion ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �uS9998-00000145
echo .
spooler -p%1  -e -c "�uS9998-00000145"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item de cotizacion ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �.0
echo .
spooler -p%1  -e -c "�.0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item de cotizacion ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �.0
echo .
spooler -p%1  -e -c "�.0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item de cotizacion ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Las autoridades de la Escuela Normal Nro 270
echo .
spooler -p%1  -e -c "�Las autoridades de la Escuela Normal Nro 270"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item de cotizacion ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �.0
echo .
spooler -p%1  -e -c "�.0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item de cotizacion ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �.0
echo .
spooler -p%1  -e -c "�.0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item de cotizacion ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Servicios de mantenimiento mensual .....................................$ 50.00 .-0
echo .
spooler -p%1  -e -c "�Servicios de mantenimiento mensual .....................................$ 50.00 .-0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item de cotizacion ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �.0
echo .
spooler -p%1  -e -c "�.0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item de cotizacion ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Para detalle de los servicios presupuestados, ver....................... ANEXO I.0
echo .
spooler -p%1  -e -c "�Para detalle de los servicios presupuestados, ver....................... ANEXO I.0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Cierre de la Cotizacion ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �
echo .
spooler -p%1  -e -c "�"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto inicio

rem ######################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Remito / Orden de Salida                         ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:genrto
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f rto.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo rto.425 con los comandos ejecutados
echo .
type rto.425
echo .
pause

cls
echo .
echo ......Archivo rto.ans respuestas a los comandos ejecutados
echo .
type rto.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Remito / Orden de Salida                         ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se envian los datos del cliente ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . bEl Cliente Nuestro99999999995NCDomicilio: Ver Base de Datos
echo .
spooler -p%1  -e -c "bEl Cliente Nuestro99999999995NCDomicilio: Ver Base de Datos"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Referencia a una factura ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �19998-00000897
echo .
spooler -p%1  -e -c "�19998-00000897"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Apertura del Remito ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �rS9998-00000432
echo .
spooler -p%1  -e -c "�rS9998-00000432"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el remito ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Bultos x 12 unidades - Jab�n en povlo x 400 gr120
echo .
spooler -p%1  -e -c "�Bultos x 12 unidades - Jab�n en povlo x 400 gr120"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el remito ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Bultos x 48 unidades - Cuadernos Tapa Dura x 96 Hojas60
echo .
spooler -p%1  -e -c "�Bultos x 48 unidades - Cuadernos Tapa Dura x 96 Hojas60"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el remito ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Bultos x 60 unidades - Bol�grafos tipo roller100
echo .
spooler -p%1  -e -c "�Bultos x 60 unidades - Bol�grafos tipo roller100"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el remito ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Bultos x 1000 unidades - Etiquetas autoadhesivas10
echo .
spooler -p%1  -e -c "�Bultos x 1000 unidades - Etiquetas autoadhesivas10"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el remito ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Bultos x 6 unidades - Canoplas pl�sticas nuevo modelo550
echo .
spooler -p%1  -e -c "�Bultos x 6 unidades - Canoplas pl�sticas nuevo modelo550"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el remito ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Bultos x 30 unidades - Gomas l�piz/Tinta10
echo .
spooler -p%1  -e -c "�Bultos x 30 unidades - Gomas l�piz/Tinta10"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el remito ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �Bultos x 12 unidades - Detergente por 1 Lts40
echo .
spooler -p%1  -e -c "�Bultos x 12 unidades - Detergente por 1 Lts40"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Cierre del Remito ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �
echo .
spooler -p%1  -e -c "�"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto inicio

rem ######################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Resumen de Cuenta / Cargo a la Habitacion        ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:gencta
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f rcta.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo rcta.425 con los comandos ejecutados
echo .
type rcta.425
echo .
pause

cls
echo .
echo ......Archivo rcta.ans respuestas a los comandos ejecutados
echo .
type rcta.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Resumen de Cuenta / Cargo a la Habitacion        ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se envian los datos del cliente ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . bEl Cliente Golondrina99999999995MCDomicilio: No Especificado
echo .
spooler -p%1  -e -c "bEl Cliente Golondrina99999999995MCDomicilio: No Especificado"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se abre el resumen de Cuenta ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �tS9998-00000895
echo .
spooler -p%1  -e -c "�tS9998-00000895"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el resumen ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �0011039998-00000123Servicio de Abono Mantenimiento50.0000
echo .
spooler -p%1  -e -c "�0011039998-00000123Servicio de Abono Mantenimiento50.0000"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el resumen ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �0011039998-00000135Reparaciones Sucursal 1150.0000
echo .
spooler -p%1  -e -c "�0011039998-00000135Reparaciones Sucursal 1150.0000"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el resumen ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �0011039998-00000165Repuestos para Sucursal 23330.0000
echo .
spooler -p%1  -e -c "�0011039998-00000165Repuestos para Sucursal 23330.0000"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el resumen ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �0011039998-00000198Vi�ticos y estad�a T�cnicos1000.0000
echo .
spooler -p%1  -e -c "�0011039998-00000198Vi�ticos y estad�a T�cnicos1000.0000"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de un item en el resumen ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �0011039998-00000213Cobertura fuera de abono100.0000
echo .
spooler -p%1  -e -c "�0011039998-00000213Cobertura fuera de abono100.0000"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Cierre del Resumen ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . �
echo .
spooler -p%1  -e -c "�"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto inicio

rem ######################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Documento No Fiscal                              ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:gendnf
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f dnf.425
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo dnf.425 con los comandos ejecutados
echo .
type dnf.425
echo .
pause

cls
echo .
echo ......Archivo dnf.ans respuestas a los comandos ejecutados
echo .
type dnf.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Documento No Fiscal                              ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se abre el Documento No Fiscal ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . G
echo .
spooler -p%1  -e -c "G"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . I�Esta es una linea de texto no fiscal0
echo .
spooler -p%1  -e -c "I�Esta es una linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . I�Esta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "I�Esta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Cierre del Documento No Fiscal ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . J
echo .
spooler -p%1  -e -c "J"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto inicio

rem ########################################################################
rem      EJEMPLOS DE EMISION DE DOCUMENTOS CON IMPRESORAS FISCALES
rem      MODELOS:       SMH / P-PR4F / P-615F 
rem ########################################################################

:rollo
cls
echo .
echo .
echo .++++++++++++++++++++++ DOCUMENTO A EMITIR ++++++++++++++++++++++++++++
echo .                       ==================
echo .
echo .           1      Emitir Ticket-Factura "A" + Voucher Tarjeta
echo .           2      Emitir Ticket-Factura "B" + Reparto
echo .           3      Emitir Ticket + Farmacia
echo .           4      Emitir Documento No Fiscal
echo .           5      -- ABORTAR -- Ejecuci�n del Programa
echo .
echo .++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
echo .
echo .

choice /c:12345 ......Su opcion es...
if errorlevel 5 goto final
if errorlevel 4 goto dnf615
if errorlevel 3 goto gentcf
if errorlevel 2 goto gentfb
if errorlevel 1 goto gentfa

rem ########################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket-Factura "A" + Voucher Tarjeta             ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:gentfa
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Se le indica al spooler, con el parametro -m ...
rem ... que no muestre mensajes por pantalla         ...
rem ....................................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -m  -p%1  -f tfa.615
if errorlevel 6 goto problem6
if errorlevel 5 goto problem5
if errorlevel 4 goto problem4
if errorlevel 3 goto problem3
if errorlevel 2 goto problem2
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo tfa.615 con los comandos a ejecutar
echo .
type tfa.615
echo .
pause

cls
echo .
echo ......Archivo tfa.ans respuestas a los comandos ejecutados
echo .
type tfa.ans
echo .
pause
goto tfapr4

:problem6
echo .
echo . *********************************
echo . * Impresora Fiscal              *
echo . * ERROR Fatal en Comando Fiscal *
echo . *********************************
echo .
goto final

:problem5
echo .
echo . ****************************************
echo . * SPOOLER                              *
echo . * ERROR tratando de abrir puerto serie *
echo . ****************************************
echo .
goto final

:problem4
echo .
echo . ***********************************************
echo . * SPOOLER                                     *
echo . * ERROR tratando de abrir archivo de comandos *
echo . ***********************************************
echo .
goto final

:problem3
echo .
echo . *************************************************
echo . * SPOOLER                                       *
echo . * ERROR tratando de abrir archivo de respuestas *
echo . *************************************************
echo .
goto final

:problem2
echo .
echo . ******************************************** 
echo . * SPOOLER                                  *
echo . * ERROR de comunicaciones con la impresora *
echo . ********************************************
echo .
goto final

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket-Factura "A" + Voucher Tarjeta             ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

:tfapr4
cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
cls
echo .
echo . *** Se envian los datos del cliente ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . bEmpresa Equis30702383923IC
echo .
spooler -p%1  -e -c "bEmpresa Equis30702383923IC"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
rem  En este ejemplo simplemente se muestra que vino desde la impresora
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide abrir un Ticket-Factura "A" ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . @AT
echo .
spooler -p%1  -e -c "@AT"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se realiza la venta de un item ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BItem Uno30.01.021.0M0.00b
echo .
spooler -p%1  -e -c "BItem Uno30.01.021.0M0.00b"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se realiza la venta de un item ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BItem Dos1.020.8110.0M0.8950b
echo .
spooler -p%1  -e -c "BItem Dos1.020.8110.0M0.8950b"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide el subtotal de lo facturado ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . CPSubtotal0
echo .
spooler -p%1  -e -c "CPSubtotal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se registra un pago ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . DEfectivo100.00T0
echo .
spooler -p%1  -e -c "DEfectivo100.00T0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide el cierre del Ticket-Factura ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . E
echo .
spooler -p%1  -e -c "E"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Carga 1 de datos del Voucher ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . jEmpresa EquisVisaCardC12345678901234560208C3
echo .
spooler -p%1  -e -c "jEmpresa EquisVisaCardC12345678901234560208C3"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Carga 2 de datos del Voucher ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . k12345543219875678 N786961.63123
echo .
spooler -p%1  -e -c "k12345543219875678 N786961.63123"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion del Voucher ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . l2
echo .
spooler -p%1  -e -c "l2"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

goto final

rem ########################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket-Factura "B" + Reparto                     ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:gentfb
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Se deja que spooler muestre por pantalla ...
rem ... sus propios mensajes                     ...
rem ................................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -e -p%1  -f tfb.615
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo tfb.615 con los comandos a ejecutar
echo .
type tfb.615
echo .
pause

cls
echo .
echo ......Archivo tfb.ans respuestas a los comandos ejecutados
echo .
type tfb.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket-Factura "B" + Reparto                     ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
cls
echo .
echo . *** Se envian los datos del cliente ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . bEmpresa Equis30702383923CC
echo .
spooler -p%1  -e -c "bEmpresa Equis30702383923CC"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
rem  En este ejemplo simplemente se muestra que vino desde la impresora
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide abrir un Ticket-Factura "B" ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . @BT
echo .
spooler -p%1  -e -c "@BT"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se realiza la venta de un item ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BItem Uno30.01.021.0M0.00b
echo .
spooler -p%1  -e -c "BItem Uno30.01.021.0M0.00b"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se realiza la venta de un item ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BItem Dos1.020.8110.0M0.8950b
echo .
spooler -p%1  -e -c "BItem Dos1.020.8110.0M0.8950b"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide el subtotal de lo facturado ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . CPSubtotal0
echo .
spooler -p%1  -e -c "CPSubtotal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se registra un pago ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . DEfectivo100.00T0
echo .
spooler -p%1  -e -c "DEfectivo100.00T0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide el cierre del Ticket-Factura ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . E
echo .
spooler -p%1  -e -c "E"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se emite el documento de Reparto ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . i3
echo .
spooler -p%1  -e -c "i3"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

goto final

rem ########################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket + Farmacia                                ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:gentcf
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Se deja que spooler muestre por pantalla ...
rem ... sus propios mensajes                     ...
rem ................................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -e -p%1  -f tcf.615
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo tcf.615 con los comandos a ejecutar
echo .
type tcf.615
echo .
pause

cls
echo .
echo ......Archivo tcf.ans respuestas a los comandos ejecutados
echo .
type tcf.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Ticket + Farmacia                                ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
cls
echo .
echo . *** Se pide abrir un Ticket ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . @TT
echo .
spooler -p%1  -e -c "@TT"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se realiza la venta de un item ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BItem Uno30.01.021.0M0.00b
echo .
spooler -p%1  -e -c "BItem Uno30.01.021.0M0.00b"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se realiza la venta de un item ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . BItem Dos1.020.8110.0M0.8950b
echo .
spooler -p%1  -e -c "BItem Dos1.020.8110.0M0.8950b"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide el subtotal de lo facturado ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . CPSubtotal0
echo .
spooler -p%1  -e -c "CPSubtotal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se registra un pago ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . DEfectivo100.00T0
echo .
spooler -p%1  -e -c "DEfectivo100.00T0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se pide el cierre del Ticket-Factura ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . E
echo .
spooler -p%1  -e -c "E"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

cls
echo .
echo . *** Se emite el documento de Farmacia ***
del respuest.ans
echo .
echo ......Comando Fiscal......
echo . h2
echo .
spooler -p%1  -e -c "h2"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
rem  Aqui el programa debe revisar la respuesta y actuar en consecuencia
type respuest.ans
echo .
pause

goto final

rem ########################################################################

rem ###----------------------------------------------------------------###
rem ### Emision de un Documento No Fiscal                              ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo de empleo solo en comercios autorizados por AFIP para     ###
rem ### emplear impresion diferida.                                    ###
rem ###----------------------------------------------------------------###

:dnf615
cls
echo .
echo . ********************************************************
echo . ***   El Spooler leyendo comandos desde un archivo   ***
echo . *** IMPRESION DIFERIDA -- Solo con autorizaci�n AFIP ***
echo . ********************************************************
echo .
pause
cls

rem ... Aqui se deja que el spooler muestre ...
rem ... sus mensajes por pantalla           ...
rem ...........................................
echo .
echo . Enviando comandos desde archivo a la impresora fiscal
echo .
echo . Por Favor.....Espere !!
echo .
spooler -p%1  -f dnf.615
if errorlevel 1 goto problem1

cls
echo .
echo ......Archivo dnf.615 con los comandos a ejecutar
echo .
type dnf.615
echo .
pause

cls
echo .
echo ......Archivo dnf.ans respuestas a los comandos ejecutados
echo .
type dnf.ans
echo .
pause

rem ###----------------------------------------------------------------###
rem ### Emision de un Documento No Fiscal                              ###
rem ### Es obligacion de la aplicacion realizar el analisis de la res- ###
rem ### puesta generada por el programa spooler.exe                    ###
rem ###                                                                ###
rem ### Modo exigido por AFIP - Concomitante                           ###
rem ### El comando "pause" indica que alli va la captura de datos      ###
rem ###----------------------------------------------------------------###

cls
echo .
echo . ********************************************************
echo . ***   El Spooler enviando comandos de a uno por vez  ***
echo . ***    IMPRESION CONCOMITANTE -- Exigida por AFIP    ***
echo . ********************************************************
echo .
pause

cls
echo .
echo . *** Se abre el Documento No Fiscal ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . H
echo .
spooler -p%1  -e -c "H"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . I�Texto no fiscal0
echo .
spooler -p%1  -e -c "I�Texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . I�Texto no fiscal0
echo .
spooler -p%1  -e -c "I�Texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Impresion de una linea ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . IEsta es otra linea de texto no fiscal0
echo .
spooler -p%1  -e -c "IEsta es otra linea de texto no fiscal0"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

cls
echo .
echo . *** Cierre del Documento No Fiscal ***
if exist respuest.ans del respuest.ans
echo .
echo ......Comando Fiscal......
echo . J
echo .
spooler -p%1  -e -c "J"
if errorlevel 1 goto problem1

echo .
echo ......Respuesta recibida......
type respuest.ans
echo .
pause

goto final

rem #########################################################################
rem                     MANEJO DE ALGUNOS ERRORES
rem #########################################################################

:problem1
echo .
echo . ****************************
echo . * SPOOLER                  *
echo . * Se ha producido un ERROR *
echo . ****************************
echo .
goto final

:noparam
echo .
echo .
echo . ***************************************
echo . *** RUNSP                           ***
echo . *** ERROR: Falta indicar puerto COM ***
echo . ***************************************
echo .
echo .

:final
echo .
echo .
echo . ***************************************
echo . ***       Fin de ejecuci�n          ***
echo . ***************************************
echo .
echo .

