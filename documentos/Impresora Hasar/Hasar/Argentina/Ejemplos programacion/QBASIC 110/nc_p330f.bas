'//==========================================================================
'// CIA. HASAR SAIC     Grupo HASAR - Dto. Software de Base
'//                     por Ricardo D. Cardenes
'//
'// Ejemplo:            Qbasic 1.10
'// Requiere:           Cargar fiscal.sys via archivo config.sys de la PC
'//                     Disparar lptfis.exe en la sesion DOS donde correra
'//                     la aplicacion
'//
'// Consultar:          publtick.pdf - drivers.pdf
'//
'// Valido para:        Impresoras fiscales HASAR
'// Modelos:            SMH/P-330F v02.01
'// 
'// Emision de:         Notas de Credito "B" ( Docum. No Fiscal Homologado )
'// =========================================================================
DIM Comando AS STRING, Se AS STRING, Fin AS STRING
DIM Respuesta AS STRING * 500
cls

Se = CHR$(28)                          '// Separador de campos en el comando
Fin = CHR$(10)                         '// Terminador requerido por el driver

'// Abrir archivo lectura / escritura
'// ---------------------------------
ON ERROR GOTO ErrorOpen
OPEN "c:\fisprn" FOR BINARY AS #1

	'// Si hay un documento abierto se cancela. Si no se pudo cancelar,
	'// se intenta su cierre
	'// Genera comandos: Cancel y CloseFiscalRceipt
	'// ------------------------------------------------------------------
	ON ERROR GOTO ErrorPut
    Comando = CHR$(152)+Fin                          '// Cancel     
	PRINT "Comando: "; Comando
	PUT #1, , Comando

	'// Ver respuesta de la impresora fiscal
	'// ------------------------------------
	ON ERROR GOTO ErrorGet
	GET #1, , Respuesta
	PRINT "Respuesta: "; Respuesta

	ON ERROR GOTO ErrorPut
    Comando = "E"+Fin                                 '// CloseFiscalReceipt
	PRINT "Comando: "; Comando
	PUT #1, , Comando

	'// Ver respuesta de la impresora fiscal
	'// ------------------------------------
	ON ERROR GOTO ErrorGet
	GET #1, , Respuesta
	PRINT "Respuesta: "; Respuesta

	'// Genera comando: SetCustomerData
	'// -------------------------------
	ON ERROR GOTO ErrorPut
    Comando = "b"+Se+"Razon Social..."+Se+"99999999995"+Se+"M"+Se+"C"+Se+"Domicilio..."+Fin
	PRINT "Comando: "; Comando
	PUT #1, , Comando

	'// Ver respuesta de la impresora fiscal
	'// ------------------------------------
	ON ERROR GOTO ErrorGet
	GET #1, , Respuesta
	PRINT "Respuesta: "; Respuesta

	'// Genera comando: SetEmbarkNumber
	'// -------------------------------
	ON ERROR GOTO ErrorPut
    Comando = CHR$(147)+Se+"1"+Se+"9998-00000123"+Fin
	PRINT "Comando: "; Comando
	PUT #1, , Comando

	'// Ver respuesta de la impresora fiscal
	'// ------------------------------------
	ON ERROR GOTO ErrorGet
	GET #1, , Respuesta
	PRINT "Respuesta: "; Respuesta

	'// Genera comando: OpenDNFH
	'// ------------------------
	ON ERROR GOTO ErrorPut
	Comando = CHR$(128)+Se+"S"+Se+"S"+Se+"1"+Fin	
	PRINT "Comando: "; Comando
	PUT #1, , Comando

	'// Ver respuesta de la impresora fiscal
	'// ------------------------------------
	ON ERROR GOTO ErrorGet
	GET #1, , Respuesta
	PRINT "Respuesta: "; Respuesta

	'// Genera comando: PrintFiscalText
	'// -------------------------------
	ON ERROR GOTO ErrorPut
    Comando = "A"+Se+"Texto Fiscal..."+Se+"0"+Fin
	PRINT "Comando: "; Comando
	PUT #1, , Comando

	'// Ver respuesta de la impresora fiscal
	'// ------------------------------------
	ON ERROR GOTO ErrorGet
	GET #1, , Respuesta
	PRINT "Respuesta: "; Respuesta

	'// Genera comando: PrintLineItem
	'// -----------------------------
	ON ERROR GOTO ErrorPut
    Comando = "B"+Se+"Articulo 1"+Se+"2.0"+Se+"10.0"+Se+"21.0"+Se+"M"+Se+"0.0"+Se+"0"+Se+"T"+Fin
	PRINT "Comando: "; Comando
	PUT #1, , Comando

	'// Ver respuesta de la impresora fiscal
	'// ------------------------------------
	ON ERROR GOTO ErrorGet
	GET #1, , Respuesta
	PRINT "Respuesta: "; Respuesta

	'// Genera comando: LastItemDiscount
	'// --------------------------------
	ON ERROR GOTO ErrorPut
    Comando = "U"+Se+"Oferta Ult. Venta..."+Se+"1.0"+Se+"m"+Se+"0"+Se+"T"+Fin
	PRINT "Comando: "; Comando
	PUT #1, , Comando

	'// Ver respuesta de la impresora fiscal
	'// ------------------------------------
	ON ERROR GOTO ErrorGet
	GET #1, , Respuesta
	PRINT "Respuesta: "; Respuesta

	'// Genera comando: GeneralDiscount
	'// -------------------------------
	ON ERROR GOTO ErrorPut
    Comando = "T"+Se+"Financiero..."+Se+"10.0"+Se+"M"+Se+"0"+Se+"T"+Fin
	PRINT "Comando: "; Comando
	PUT #1, , Comando

	'// Ver respuesta de la impresora fiscal
	'// ------------------------------------
	ON ERROR GOTO ErrorGet
	GET #1, , Respuesta
	PRINT "Respuesta: "; Respuesta

	'// Genera comando: CloseDNFH
	'// -------------------------
	ON ERROR GOTO ErrorPut
	Comando = CHR$(129)+Fin
	PRINT "Comando: "; Comando
	PUT #1, , Comando

	'// Ver respuesta de la impresora fiscal
	'// ------------------------------------
	ON ERROR GOTO ErrorGet
	GET #1, , Respuesta
	PRINT "Respuesta: "; Respuesta
	END

'// Tratamiento de errores muy elemental
'// Falta analisis campos de status
'// ------------------------------------
ErrorOpen:
	PRINT "Error abriendo driver"
	END

ErrorPut:
	PRINT "Error escribiendo comando"
	END

ErrorGet:
	PRINT "Error obteniendo respuesta"
	END

