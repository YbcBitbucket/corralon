/*
* Manejo de Controlador Fiscal Hasar 715
* Gustavo Carlos Asborno - Octubre 2005
* laher@speedy.com.ar / asborno@infovia.com.ar
*
* Harbour MiniGUI R.62a Copyright 2002-2006 Roberto Lopez,
* MINIGUI - Harbour Win32 GUI library 
* Copyright 2002 Roberto Lopez <roblez@ciudad.com.ar>
* http://www.geocities.com/harbourminigui/
*/
*--------------------------------------------------------------------------*
#include "minigui.ch"

Function Main
  PUBLIC TOTAL,Se,Handler
  TOTAL=0
  Se=''
  DEFINE WINDOW Win_1            ;
    AT 0,0                       ;
    WIDTH 600                    ;
    HEIGHT 200                   ;
    TITLE 'Demo Uso I. F.l 320'  ;
    MAIN                         ;
    ON INIT  Apagoboton()

    @ 010,050 BUTTON ABR_PUERTO  ;
         CAPTION '&Abrir Puerto' ;
         ACTION  AbrirPuerto()   ;
         TOOLTIP 'Abre el Puerto'

    @ 010,160 BUTTON CER_PUERTO   ;
         CAPTION '&Cerrar Puerto' ;
         ACTION  CerrarPuerto()   ;
         TOOLTIP 'Cierra el Puerto'

    @ 050,050 BUTTON FACTURA      ;
         CAPTION '&Factura B'     ;
         ACTION  AbroFac()        ;
         TOOLTIP 'Cierra el Puerto'
	
    @ 050,160 BUTTON CIERREZ      ;
         CAPTION '&Cierre Z'      ;
         ACTION  Cierrez()        ;
         TOOLTIP 'Vende un item'

    @ 050,270 BUTTON CIERREX      ;
         CAPTION '&Cierre X'      ;
         ACTION  CierreX()        ;
         TOOLTIP 'Vende un item'

    @ 090,050 BUTTON VENTA        ;
         CAPTION '&Vender Item'   ;
         ACTION  VenderItem()     ;
         TOOLTIP 'Vende un item'

    @ 090,160 BUTTON TERMVENTA    ;
         CAPTION '&Terminar Fac'  ;
         ACTION  FinVenta()       ;
         TOOLTIP 'Finalizar Factura'

    @ 140,010 LABEL Lmen VALUE "Controlador Hasar 715" FONT "ARIAL" SIZE 10 Bold Autosize

	END WINDOW

  CENTER WINDOW Win_1
  ACTIVATE WINDOW Win_1
Return
*-------------------------------*
Function AbrirPuerto
handler=CallDll32 ( "OpenComFiscal" , "WINFIS32.DLL" ,1,0 )
CallDll32 ( "InitFiscal" , "WINFIS32.DLL" ,handler, 0 )
alerror(handler)
If handler=0
  Win_1.ABR_PUERTO.enabled:=.F.
  Win_1.CER_PUERTO.enabled:=.T.
  Win_1.FACTURA.enabled:=.T.
  Win_1.CIERREZ.enabled:=.T.
  Win_1.CIERREX.enabled:=.T.
  Win_1.VENTA.enabled:=.T.
  Win_1.TERMVENTA.enabled:=.T.
  MODIFY CONTROL Lmen OF Win_1 VALUE 'Puerto Abierto'
endif
return
*-------------------------------*
Function CerrarPuerto
CallDll32 ( "CloseComFiscal" , "WINFIS32.DLL" ,handler)
Apagoboton()
MODIFY CONTROL Lmen OF Win_1 VALUE 'Puerto Cerrado'
Return
*-------------------------------*
FUNCTION Cierrez
alerror(handler)
MODIFY CONTROL Lmen OF Win_1 VALUE 'Realizando Cierre Z'
CallDll32 ( "MandaPaqueteFiscal" , "WINFIS32.DLL" ,handler, '9Z' )
CerrarPuerto()
Apagoboton()
MODIFY CONTROL Lmen OF Win_1 VALUE "Controlador Hasar 320"
Return               
*-------------------------------*
FUNCTION Cierrex
MODIFY CONTROL Lmen OF Win_1 VALUE 'Realizando Cierre X'
CallDll32 ( "MandaPaqueteFiscal" , "WINFIS32.DLL" ,handler, '9X' )
CerrarPuerto()
Apagoboton()
MODIFY CONTROL Lmen OF Win_1 VALUE "Controlador Hasar 320"
Return                         
*-------------------------------*
Function AbroFac
* Actualizar Pantalla
    Win_1.ABR_PUERTO.enabled:=.F.
    Win_1.CER_PUERTO.enabled:=.F.
    Win_1.FACTURA.enabled:=.T.
    Win_1.CIERREZ.enabled:=.F.
    Win_1.CIERREX.enabled:=.F.
    Win_1.VENTA.enabled:=.T.
    Win_1.TERMVENTA.enabled:=.T.

* Configuracion SetHeaderTrailer
for i=1 to 10
	linea = alltrim(str(i))
	* s = "]" + Se + linea + Se + "Linea " + linea + " de Header"
	* Borra la linea 
	s = "]" + Se + linea + Se + CHR(127)
  CallDll32 ( "MandaPaqueteFiscal" , "WINFIS32.DLL" ,handler, s )
next

* Datos del comprador
  s = "b" + Se + "Emilio Soft" + Se + "18369730" + Se + "C" + Se + "2" + Se + "Tapalqu� 761"
  CallDll32 ( "MandaPaqueteFiscal" , "WINFIS32.DLL" ,handler, s )

* Abre un comprobante fiscal 
  s = "@" + Se + "B" + Se + "T"
  CallDll32 ( "MandaPaqueteFiscal" , "WINFIS32.DLL" ,handler, s )
MODIFY CONTROL Lmen OF Win_1 VALUE "Factura tipo B Abierta"
Return
*-------------------------------*
Function VenderItem
total=total + 10
s = "B" + Se + "Articulo 1" + Se + "1.0" + Se + "10.00" + Se + "21.00" + Se + "M" + Se + "0" + Se + "" + Se + "T"
CallDll32 ( "MandaPaqueteFiscal" , "WINFIS32.DLL" ,handler, s )
mensaje:="Articulo Vendido   Total: "+tran(total,'9999.99')
MODIFY CONTROL Lmen OF Win_1 VALUE mensaje
Return
*-------------------------------*
Function FinVenta

MODIFY CONTROL Lmen OF Win_1 VALUE "Enviando SubTotal"
s='D'+ SE +'Su Pago: '+ SE + LTRIM(STR(TOTAL,9,2)) + SE +'T'+SE+'1'
CallDll32 ( "MandaPaqueteFiscal" , "WINFIS32.DLL" ,handler, s )
MODIFY CONTROL Lmen OF Win_1 VALUE "Cerrando Documento Fiscal"
s = "E"
CallDll32 ( "MandaPaqueteFiscal" , "WINFIS32.DLL" ,handler, s )
CerrarPuerto()
Apagoboton()
MODIFY CONTROL Lmen OF Win_1 VALUE "Controlador Hasar 320"
Return
*-------------------------------*
function Apagoboton
    Win_1.ABR_PUERTO.enabled:=.T.
    Win_1.CER_PUERTO.enabled:=.F.
    Win_1.FACTURA.enabled:=.F.
    Win_1.CIERREZ.enabled:=.F.
    Win_1.CIERREX.enabled:=.F.
    Win_1.VENTA.enabled:=.F.
    Win_1.TERMVENTA.enabled:=.F.
Return
*-------------------------------*
function alerror(handler)
if handler =-1
  msgstop('Error General','Error')
elseif handler =-2
  msgstop('Handler Invalido','Error')
elseif handler =-3
  msgstop('Intento de Enviar un Comando'+chr(13)+ 'Cuando se Estaba Procesando','Error')
elseif handler =-4
  msgstop('Error de Comunicacion','Error')
elseif handler =-5
  msgstop('Puerto ya Abierto','Error')
elseif handler =-6
  msgstop('No Hay Memoria','Error')
elseif handler =-7
  msgstop('El Puerto Ya Esta Abierto','Error')
elseif handler =-8
  msgstop('Direccion del Buffer de Respuestas es Invalida','Error')
elseif handler =-9
  msgstop('El comando no finalizo'+chr(13)+'Volvio una respuesta tipo STR_PRN','Error')
elseif handler =-10
  msgstop('Proceso Cancelado por Usuario','Error')
endif
return
