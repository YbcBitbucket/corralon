package WebServices;

import WSFEV.CbteTipoResponse;
import WSFEV.ConceptoTipoResponse;
import WSFEV.DocTipoResponse;
import WSFEV.DummyResponse;
import WSFEV.FECAEAGetResponse;
import WSFEV.FECAEAResponse;
import WSFEV.FECAEASinMovConsResponse;
import WSFEV.FECAEASinMovResponse;
import WSFEV.FECAEResponse;
import WSFEV.FECompConsultaResponse;
import WSFEV.FECotizacionResponse;
import WSFEV.FEPaisResponse;
import WSFEV.FEPtoVentaResponse;
import WSFEV.FERecuperaLastCbteResponse;
import WSFEV.FERegXReqResponse;
import WSFEV.FETributoResponse;
import WSFEV.IvaTipoResponse;
import WSFEV.MonedaResponse;
import WSFEV.OpcionalTipoResponse;


public class ServidorWsfev {

    public static FECAEAGetResponse fecaeaConsultar(WSFEV.FEAuthRequest auth, int periodo, short orden) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.fecaeaConsultar(auth, periodo, orden);
    }

    public static FECAEAResponse fecaeaRegInformativo(WSFEV.FEAuthRequest auth, WSFEV.FECAEARequest feCAEARegInfReq) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.fecaeaRegInformativo(auth, feCAEARegInfReq);
    }

    public static FECAEASinMovConsResponse fecaeaSinMovimientoConsultar(WSFEV.FEAuthRequest auth, java.lang.String caea, int ptoVta) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.fecaeaSinMovimientoConsultar(auth, caea, ptoVta);
    }

    public static FECAEASinMovResponse fecaeaSinMovimientoInformar(WSFEV.FEAuthRequest auth, int ptoVta, java.lang.String caea) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.fecaeaSinMovimientoInformar(auth, ptoVta, caea);
    }

    public static FECAEAGetResponse fecaeaSolicitar(WSFEV.FEAuthRequest auth, int periodo, short orden) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.fecaeaSolicitar(auth, periodo, orden);
    }

    public static FECAEResponse fecaeSolicitar(WSFEV.FEAuthRequest auth, WSFEV.FECAERequest feCAEReq) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.fecaeSolicitar(auth, feCAEReq);
    }

    public static FECompConsultaResponse feCompConsultar(WSFEV.FEAuthRequest auth, WSFEV.FECompConsultaReq feCompConsReq) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feCompConsultar(auth, feCompConsReq);
    }

    public static FERegXReqResponse feCompTotXRequest(WSFEV.FEAuthRequest auth) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feCompTotXRequest(auth);
    }

    public static FERecuperaLastCbteResponse feCompUltimoAutorizado(WSFEV.FEAuthRequest auth, int ptoVta, int cbteTipo) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feCompUltimoAutorizado(auth, ptoVta, cbteTipo);
    }

    public static DummyResponse feDummy() {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feDummy();
    }

    public static FECotizacionResponse feParamGetCotizacion(WSFEV.FEAuthRequest auth, java.lang.String monId) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetCotizacion(auth, monId);
    }

    public static FEPtoVentaResponse feParamGetPtosVenta(WSFEV.FEAuthRequest auth) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetPtosVenta(auth);
    }

    public static CbteTipoResponse feParamGetTiposCbte(WSFEV.FEAuthRequest auth) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposCbte(auth);
    }

    public static ConceptoTipoResponse feParamGetTiposConcepto(WSFEV.FEAuthRequest auth) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposConcepto(auth);
    }

    public static DocTipoResponse feParamGetTiposDoc(WSFEV.FEAuthRequest auth) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposDoc(auth);
    }

    public static IvaTipoResponse feParamGetTiposIva(WSFEV.FEAuthRequest auth) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposIva(auth);
    }

    public static MonedaResponse feParamGetTiposMonedas(WSFEV.FEAuthRequest auth) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposMonedas(auth);
    }

    public static OpcionalTipoResponse feParamGetTiposOpcional(WSFEV.FEAuthRequest auth) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposOpcional(auth);
    }

    public static FEPaisResponse feParamGetTiposPaises(WSFEV.FEAuthRequest auth) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposPaises(auth);
    }

    public static FETributoResponse feParamGetTiposTributos(WSFEV.FEAuthRequest auth) {
        WSFEV.Service service = new WSFEV.Service();
        WSFEV.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposTributos(auth);
    }
    
}
