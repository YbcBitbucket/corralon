VERSION 5.00
Object = "{4DDBCF44-22D2-46E9-B4C2-A99AEF1AF6CC}#15.0#0"; "HasarArgentina.ocx"
Begin VB.Form FormPrincipal 
   BackColor       =   &H00008080&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "OCX NG - Argentina"
   ClientHeight    =   10920
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14280
   Icon            =   "FormPrincipal.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   10920
   ScaleWidth      =   14280
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrameDescargas 
      BackColor       =   &H00E0E0E0&
      Caption         =   "DESCARGAR"
      ForeColor       =   &H00FF0000&
      Height          =   1695
      Left            =   9360
      TabIndex        =   73
      Top             =   9120
      Width           =   4815
      Begin VB.CommandButton CommandObtRepLog 
         BackColor       =   &H0000FFFF&
         Caption         =   "LOG INTERNO"
         Height          =   615
         Left            =   2520
         Style           =   1  'Graphical
         TabIndex        =   77
         Top             =   960
         Width           =   2175
      End
      Begin VB.CommandButton CommandObtRepDoc 
         BackColor       =   &H0000FFFF&
         Caption         =   "REPORTE DOCUMENTOS"
         Height          =   615
         Left            =   2520
         Style           =   1  'Graphical
         TabIndex        =   76
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton CommandObtRepAudit 
         BackColor       =   &H0000FFFF&
         Caption         =   "CTD (Cinta testigo digital)"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   75
         Top             =   960
         Width           =   2295
      End
      Begin VB.CommandButton CommandObtRepElectr 
         BackColor       =   &H0000FFFF&
         Caption         =   "REPORTES ELECTRONICOS"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   74
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Frame FrameMiscel 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Miscel�neas"
      ForeColor       =   &H00FF0000&
      Height          =   1695
      Left            =   120
      TabIndex        =   63
      Top             =   9120
      Width           =   9135
      Begin VB.CommandButton CommandReimpr 
         BackColor       =   &H0080C0FF&
         Caption         =   "REIMPRESION COMPROBANTE"
         Height          =   615
         Left            =   6960
         Style           =   1  'Graphical
         TabIndex        =   70
         Top             =   240
         Width           =   2055
      End
      Begin VB.CommandButton CommandDocxMail 
         BackColor       =   &H0080C0FF&
         Caption         =   "ENVIAR COMPROBANTE POR CORREO ELECTR."
         Height          =   615
         Left            =   4680
         Style           =   1  'Graphical
         TabIndex        =   69
         Top             =   960
         Width           =   2175
      End
      Begin VB.CommandButton CommandBorrarLogo 
         BackColor       =   &H0080C0FF&
         Caption         =   "ELIMINAR LOGO EMISOR"
         Height          =   615
         Left            =   2400
         Style           =   1  'Graphical
         TabIndex        =   68
         Top             =   960
         Width           =   2175
      End
      Begin VB.CommandButton CommandCopiarDoc 
         BackColor       =   &H0080C0FF&
         Caption         =   "COPIAR COMPROBANTE"
         Height          =   615
         Left            =   4680
         Style           =   1  'Graphical
         TabIndex        =   67
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton CommandCargaLogo 
         BackColor       =   &H0080C0FF&
         Caption         =   "CARGAR LOGO EMISOR"
         Height          =   615
         Left            =   2400
         Style           =   1  'Graphical
         TabIndex        =   66
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton CommandAvanzPap 
         BackColor       =   &H0080C0FF&
         Caption         =   "AVANZAR PAPEL"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   65
         Top             =   960
         Width           =   2175
      End
      Begin VB.CommandButton CommandAbrirCaj 
         BackColor       =   &H0080C0FF&
         Caption         =   "ABRIR CAJON DE DINERO"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   64
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.Frame FrameEmisor 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Modificar datos emisor"
      ForeColor       =   &H00FF0000&
      Height          =   1695
      Left            =   120
      TabIndex        =   59
      Top             =   7320
      Width           =   4815
      Begin VB.CommandButton CommandModifIIBB 
         BackColor       =   &H008080FF&
         Caption         =   "INSCRIP. INGRESOS BRUTOS"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   62
         Top             =   960
         Width           =   2295
      End
      Begin VB.CommandButton CommandModifIniActiv 
         BackColor       =   &H008080FF&
         Caption         =   "FECHA INICIO ACTIVIDADES"
         Height          =   615
         Left            =   2520
         Style           =   1  'Graphical
         TabIndex        =   61
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton CommandModifCateg 
         BackColor       =   &H008080FF&
         Caption         =   "RESPONSABILIDAD IVA"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   60
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Frame FrameCFG 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Configurar"
      ForeColor       =   &H00FF0000&
      Height          =   1695
      Left            =   5040
      TabIndex        =   51
      Top             =   7320
      Width           =   9135
      Begin VB.CommandButton CommandCfgBaudios 
         BackColor       =   &H00FFFFC0&
         Caption         =   "BAUDIOS SERIE RS-232"
         Height          =   615
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   58
         Top             =   960
         Width           =   2175
      End
      Begin VB.CommandButton CommandCFgZona 
         BackColor       =   &H00FFFFC0&
         Caption         =   "ZONAS DE LINEAS DE USUARIO"
         Height          =   615
         Left            =   6840
         Style           =   1  'Graphical
         TabIndex        =   57
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton CommandCfgZBorr 
         BackColor       =   &H00FFFFC0&
         Caption         =   "LIMITE BORRADO DE 'Z'"
         Height          =   615
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   56
         Top             =   240
         Width           =   2295
      End
      Begin VB.CommandButton CommandCfgServCorreo 
         BackColor       =   &H00FFFFC0&
         Caption         =   "SERVIDOR CORREO ELECTRONICO"
         Height          =   615
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   960
         Width           =   2295
      End
      Begin VB.CommandButton CommandCfgRed 
         BackColor       =   &H00FFFFC0&
         Caption         =   "CONECTIVIDAD EN RED"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   54
         Top             =   960
         Width           =   1935
      End
      Begin VB.CommandButton CommandCfgImprFis 
         BackColor       =   &H00FFFFC0&
         Caption         =   "IMPRESORA FISCAL COMPORTAMIENTO"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   53
         Top             =   240
         Width           =   1935
      End
      Begin VB.CommandButton CommandCfgFyh 
         BackColor       =   &H00FFFFC0&
         Caption         =   "FECHA Y HORA"
         Height          =   615
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   52
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.CommandButton CommandCancel 
      BackColor       =   &H000000FF&
      Caption         =   "CANCELAR"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   2640
      MaskColor       =   &H8000000E&
      Style           =   1  'Graphical
      TabIndex        =   39
      Top             =   4800
      UseMaskColor    =   -1  'True
      Width           =   2295
   End
   Begin VB.Frame FrameDNFH 
      BackColor       =   &H00E0E0E0&
      Caption         =   "DNFH"
      ForeColor       =   &H00FF0000&
      Height          =   4575
      Left            =   2640
      TabIndex        =   25
      Top             =   120
      Width           =   2295
      Begin VB.CommandButton CommandGenerico 
         BackColor       =   &H00C0E0FF&
         Caption         =   "DOCUMENTO GENERICO"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   3120
         Width           =   2055
      End
      Begin VB.CommandButton CommandPRESUP 
         BackColor       =   &H00C0E0FF&
         Caption         =   "PRESUPUESTO 'X'"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   34
         Top             =   1680
         Width           =   2055
      End
      Begin VB.CommandButton CommandDONA 
         BackColor       =   &H00C0E0FF&
         Caption         =   "DONACION"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   33
         Top             =   3840
         Width           =   2055
      End
      Begin VB.CommandButton CommandRBOX 
         BackColor       =   &H00C0E0FF&
         Caption         =   "RECIBO 'X'"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   2400
         Width           =   2055
      End
      Begin VB.CommandButton CommandRTOX 
         BackColor       =   &H00C0E0FF&
         Caption         =   "REMITO 'X'"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   960
         Width           =   2055
      End
      Begin VB.CommandButton CommandRTOR 
         BackColor       =   &H00C0E0FF&
         Caption         =   "REMITO 'R'"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.Frame FrameDF 
      BackColor       =   &H00E0E0E0&
      Caption         =   "DF (Documentos Fiscales)"
      ForeColor       =   &H00FF0000&
      Height          =   3855
      Left            =   5040
      TabIndex        =   12
      Top             =   120
      Width           =   9135
      Begin HasarArgentina.ImpresoraFiscalRG3561 HASARNG 
         Left            =   1800
         Top             =   3000
         _ExtentX        =   1720
         _ExtentY        =   1085
      End
      Begin VB.CommandButton CommandPatron 
         BackColor       =   &H00C0FFFF&
         Caption         =   "--- DOCUMENTO   PATRON ---"
         Height          =   615
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   79
         Top             =   3120
         Width           =   4575
      End
      Begin VB.CommandButton CommandTiqueNC 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE NC CONS. FINAL"
         Height          =   615
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   37
         Top             =   3120
         Width           =   2175
      End
      Begin VB.CommandButton CommandTique 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE CONS. FINAL"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   3120
         Width           =   1935
      End
      Begin VB.CommandButton CommandTRM 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE RECIBO 'M'"
         Height          =   615
         Left            =   6840
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   2400
         Width           =   2175
      End
      Begin VB.CommandButton CommandTRC 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE RECIBO 'C'"
         Height          =   615
         Left            =   6840
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   1680
         Width           =   2175
      End
      Begin VB.CommandButton CommandTRB 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE RECIBO 'B'"
         Height          =   615
         Left            =   6840
         Style           =   1  'Graphical
         TabIndex        =   30
         Top             =   960
         Width           =   2175
      End
      Begin VB.CommandButton CommandTRA 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE RECIBO 'A'"
         Height          =   615
         Left            =   6840
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton CommandTNCM 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE NOTA DE CREDITO 'M'"
         Height          =   615
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   24
         Top             =   2400
         Width           =   2295
      End
      Begin VB.CommandButton CommandTNCC 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE NOTA DE CREDITO 'C'"
         Height          =   615
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   1680
         Width           =   2295
      End
      Begin VB.CommandButton CommandTNCB 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE NOTA DE CREDITO 'B'"
         Height          =   615
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   960
         Width           =   2295
      End
      Begin VB.CommandButton CommandTNCA 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE NOTA DE CREDITO 'A'"
         Height          =   615
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   240
         Width           =   2295
      End
      Begin VB.CommandButton CommandTNDM 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE NOTA DE DEBITO 'M'"
         Height          =   615
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   2400
         Width           =   2175
      End
      Begin VB.CommandButton CommandTNDC 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE NOTA DE DEBITO 'C'"
         Height          =   615
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   1680
         Width           =   2175
      End
      Begin VB.CommandButton CommandTNDB 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE NOTA DE DEBITO 'B'"
         Height          =   615
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   960
         Width           =   2175
      End
      Begin VB.CommandButton CommandTNDA 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE NOTA DE DEBITO 'A'"
         Height          =   615
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton CommandTFM 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE FACTURA 'M'"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   2400
         Width           =   1935
      End
      Begin VB.CommandButton CommandTFC 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE FACTURA 'C'"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   1680
         Width           =   1935
      End
      Begin VB.CommandButton CommandTFB 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE FACTURA 'B'"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   960
         Width           =   1935
      End
      Begin VB.CommandButton CommandTFA 
         BackColor       =   &H00C0FFFF&
         Caption         =   "TIQUE FACTURA 'A'"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame FrameAcumCierres 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Acumulados y Cierres"
      ForeColor       =   &H00FF0000&
      Height          =   5295
      Left            =   120
      TabIndex        =   6
      Top             =   120
      Width           =   2415
      Begin VB.CommandButton CommandAudZeta 
         BackColor       =   &H00C0C0FF&
         Caption         =   "AUDITORIA POR ZETAS"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   80
         Top             =   4560
         Width           =   2175
      End
      Begin VB.CommandButton CommandAudit 
         BackColor       =   &H00C0C0FF&
         Caption         =   "AUDITORIA POR FECHAS"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   38
         Top             =   3840
         Width           =   2175
      End
      Begin VB.CommandButton CommandCierreZ 
         BackColor       =   &H00C0C0FF&
         Caption         =   "INFORME DIARIO DE CIERRE 'Z'"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   3120
         Width           =   2175
      End
      Begin VB.CommandButton CommandEquis 
         BackColor       =   &H00C0C0FF&
         Caption         =   "DETALLE DE VENTAS 'X'"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   2400
         Width           =   2175
      End
      Begin VB.CommandButton CommandConsCierreZ 
         BackColor       =   &H00C0C0FF&
         Caption         =   "CONSULTA ACUMULADOS CIERRE 'Z'"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   1680
         Width           =   2175
      End
      Begin VB.CommandButton CommandConsAcumMemoria 
         BackColor       =   &H00C0C0FF&
         Caption         =   "CONSULTA ACUMULADOS MEMORIA DE TRABAJO"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   960
         Width           =   2175
      End
      Begin VB.CommandButton CommandConsAcumComprob 
         BackColor       =   &H00C0C0FF&
         Caption         =   "CONSULTA ACUMULADOS COMPROBANTE"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.Frame FrameConsultas 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Consultar"
      ForeColor       =   &H00FF0000&
      Height          =   3135
      Left            =   5040
      TabIndex        =   3
      Top             =   4080
      Width           =   9135
      Begin VB.CommandButton CommandRangoZ 
         BackColor       =   &H00C0FFC0&
         Caption         =   "RANGO  CIERRES 'Z'"
         Height          =   615
         Left            =   6840
         Style           =   1  'Graphical
         TabIndex        =   78
         Top             =   2400
         Width           =   2175
      End
      Begin VB.CommandButton CommandObtStImpr 
         BackColor       =   &H00C0FFC0&
         Caption         =   "ULTIMO ESTADO IMPRESORA"
         Height          =   615
         Left            =   6840
         Style           =   1  'Graphical
         TabIndex        =   72
         Top             =   1680
         Width           =   2175
      End
      Begin VB.CommandButton CommandObtStFiscal 
         BackColor       =   &H00C0FFC0&
         Caption         =   "ULTIMO ESTADO FISCAL"
         Height          =   615
         Left            =   6840
         Style           =   1  'Graphical
         TabIndex        =   71
         Top             =   960
         Width           =   2175
      End
      Begin VB.CommandButton CommandConsZona 
         BackColor       =   &H00C0FFC0&
         Caption         =   "ZONAS DE LINEAS DE USUARIO"
         Height          =   615
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   50
         Top             =   960
         Width           =   2295
      End
      Begin VB.CommandButton CommandZBorr 
         BackColor       =   &H00C0FFC0&
         Caption         =   "LIMITE BORRADO DE 'Z'"
         Height          =   615
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   49
         Top             =   2400
         Width           =   2295
      End
      Begin VB.CommandButton CommandConsModvers 
         BackColor       =   &H00C0FFC0&
         Caption         =   "MODELO Y VERSION"
         Height          =   615
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   48
         Top             =   1680
         Width           =   2295
      End
      Begin VB.CommandButton CommandConsError 
         BackColor       =   &H00C0FFC0&
         Caption         =   "ULTIMO ERROR"
         Height          =   615
         Left            =   6840
         Style           =   1  'Graphical
         TabIndex        =   47
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton CommandSubtot 
         BackColor       =   &H00C0FFC0&
         Caption         =   "SUBTOTAL"
         Height          =   615
         Left            =   4440
         Style           =   1  'Graphical
         TabIndex        =   46
         Top             =   240
         Width           =   2295
      End
      Begin VB.CommandButton CommandFechaHora 
         BackColor       =   &H00C0FFC0&
         Caption         =   "FECHA Y HORA"
         Height          =   615
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   45
         Top             =   2400
         Width           =   2175
      End
      Begin VB.CommandButton CommandConsEstado 
         BackColor       =   &H00C0FFC0&
         Caption         =   "ESTADO GENERAL"
         Height          =   615
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   44
         Top             =   1680
         Width           =   2175
      End
      Begin VB.CommandButton CommandConsDocAsoc 
         BackColor       =   &H00C0FFC0&
         Caption         =   "DOCUMENTOS ASOCIADOS"
         Height          =   615
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   43
         Top             =   960
         Width           =   2175
      End
      Begin VB.CommandButton CommandConsCorreo 
         BackColor       =   &H00C0FFC0&
         Caption         =   "CONFIGURACION SERV. CORREO ELECTR."
         Height          =   615
         Left            =   2160
         Style           =   1  'Graphical
         TabIndex        =   42
         Top             =   240
         Width           =   2175
      End
      Begin VB.CommandButton CommandConsZetas 
         BackColor       =   &H00C0FFC0&
         Caption         =   "CAPACIDAD ZETAS"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   41
         Top             =   2400
         Width           =   1935
      End
      Begin VB.CommandButton CommandCapac 
         BackColor       =   &H00C0FFC0&
         Caption         =   "CAPACIDADES IMPRESORA FISCAL"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   40
         Top             =   1680
         Width           =   1935
      End
      Begin VB.CommandButton CommandDatosIni 
         BackColor       =   &H00C0FFC0&
         Caption         =   "DATOS INICIALIZACION"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   960
         Width           =   1935
      End
      Begin VB.CommandButton CommandConsCfgRed 
         BackColor       =   &H00C0FFC0&
         Caption         =   "CONFIGURACION RED"
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame FrameProtocolo 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Vinculaci�n"
      ForeColor       =   &H00FF0000&
      Height          =   1695
      Left            =   120
      TabIndex        =   0
      Top             =   5520
      Width           =   4815
      Begin VB.CommandButton CommandSalir 
         BackColor       =   &H00008080&
         Caption         =   "TERMINAR"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   2760
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   240
         Width           =   1935
      End
      Begin VB.CommandButton CommandProtocolo 
         BackColor       =   &H00008000&
         Caption         =   "CONECTARSE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   240
         MaskColor       =   &H00808080&
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   240
         Width           =   2535
      End
   End
End
Attribute VB_Name = "FormPrincipal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'//###################################################################################################################
'// Impresoras Fiscales HASAR - 2da. Generaci�n                                     OCX Fiscal HASAR - 2da. Generaci�n
'// RG AFIP N� 3561/13 - Controladores Fiscales Nueva tecnolog�a                    Ejemplo de uso
'// Dto. Software de Base - Compa��a HASAR SAIC - Grupo HASAR                       Marzo 2015
'//###################################################################################################################

Option Explicit

'//====================================================================================================================
'// Comando de apertura del caj�n de dinero.
'//====================================================================================================================
Private Sub CommandAbrirCaj_Click()

    Debug.Print
    Debug.Print "ABRIENDO CAJON DE DINERO..."
    Debug.Print
    
On Error GoTo keka
    HASARNG.AbrirCajonDinero
    
    Debug.Print
    Debug.Print "CAJON DE DINERO ABIERTO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un informe de aditor�a por fechas.
'//====================================================================================================================
Private Sub CommandAudit_Click()

    Debug.Print
    Debug.Print "IMPRIMIENDO REPORTE DE AUDITORIA POR FECHAS..."
    Debug.Print
    
On Error GoTo keka
    HASARNG.ReportarZetasPorFecha "23/02/2015", "25/02/2015", ReporteAuditoriaGlobal
    
    Debug.Print
    Debug.Print "REPORTE IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un informe de aditor�a por fechas.
'//====================================================================================================================
Private Sub CommandAudZeta_Click()

    Debug.Print
    Debug.Print "IMPRIMIENDO REPORTE DE AUDITORIA POR ZETAS..."
    Debug.Print
    
On Error GoTo keka
    HASARNG.ReportarZetasPorNumeroZeta 1, 5, ReporteAuditoriaDiscriminado
    
    Debug.Print
    Debug.Print "REPORTE IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Comandos de avance de papel.
'//====================================================================================================================
Private Sub CommandAvanzPap_Click()

    Debug.Print
    Debug.Print "AVANZANDO PAPEL..."
    Debug.Print
    
On Error GoTo keka
    HASARNG.AvanzarPapelAmbasEstaciones 3
    Debug.Print "3 l�neas en tique/rollo testigo."
    
    HASARNG.AvanzarPapelEstacionAuditoria 3
    Debug.Print "3 l�neas en rollo testigo, solamente."
    
    HASARNG.AvanzarPapelEstacionTique 3
    Debug.Print "3 l�neas en tique, solamente."
    
    Debug.Print
    Debug.Print "FIN DE AVANCE DE PAPEL..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Se elimina el logo del emisor de la memoria de trabajo de la impresora fiscal.
'//====================================================================================================================
Private Sub CommandBorrarLogo_Click()

    Debug.Print
    Debug.Print "ELIMINANDO LOGO DEL EMISOR..."
    Debug.Print
    
On Error GoTo keka
    HASARNG.EliminarLogoEmisor
    
    Debug.Print
    Debug.Print "LOGO ELIMINADO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Se cancela el comprobante en curso de emisi�n.
'//====================================================================================================================
Private Sub CommandCancel_Click()

    Debug.Print
    Debug.Print "CANCELANDO COMPROBANTE ABIERTO..."
    Debug.Print
    
On Error GoTo keka
    HASARNG.Cancelar
    
    Debug.Print
    Debug.Print "CANCELADO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consulta de capacidades de la impresora fiscal
'//====================================================================================================================
Private Sub CommandCapac_Click()
Dim respcapac As RespuestaConsultarCapacidadesImpresoraFiscal

    Debug.Print
    Debug.Print "CONSULTANDO CAPACIDADES DE LA IMPRESORA FISCAL..."
    Debug.Print
    
On Error GoTo keka
    respcapac = HASARNG.ConsultarCapacidadesImpresoraFiscal(AltoLogo, EstacionTicket, TiqueFacturaA)
    Debug.Print "Alto logotipo             = [" & respcapac.Valor & "]"
    Debug.Print
    respcapac = HASARNG.ConsultarCapacidadesImpresoraFiscal(AnchoLogo, EstacionTicket, TiqueFacturaA)
    Debug.Print "Ancho logotipo            = [" & respcapac.Valor & "]"
    Debug.Print
    respcapac = HASARNG.ConsultarCapacidadesImpresoraFiscal(AnchoRazonSocial, EstacionTicket, TiqueFacturaA)
    Debug.Print "Ancho raz�n social        = [" & respcapac.Valor & "]"
    Debug.Print
    respcapac = HASARNG.ConsultarCapacidadesImpresoraFiscal(AnchoTextoFiscal, EstacionTicket, TiqueFacturaA)
    Debug.Print "Ancho texto fiscal        = [" & respcapac.Valor & "]"
    Debug.Print
    respcapac = HASARNG.ConsultarCapacidadesImpresoraFiscal(AnchoTextoLineasUsuario, EstacionTicket, TiqueFacturaA)
    Debug.Print "Ancho l�neas usuario      = [" & respcapac.Valor & "]"
    Debug.Print
    respcapac = HASARNG.ConsultarCapacidadesImpresoraFiscal(AnchoTextoNoFiscal, EstacionTicket, TiqueFacturaA)
    Debug.Print "Ancho texto no fiscal     = [" & respcapac.Valor & "]"
    Debug.Print
    respcapac = HASARNG.ConsultarCapacidadesImpresoraFiscal(AnchoTextoVenta, EstacionTicket, TiqueFacturaA)
    Debug.Print "Ancho texto venta         = [" & respcapac.Valor & "]"
    Debug.Print
    respcapac = HASARNG.ConsultarCapacidadesImpresoraFiscal(AnchoTotalImpresion, EstacionTicket, TiqueFacturaA)
    Debug.Print "Ancho total impresi�n     = [" & respcapac.Valor & "]"
    Debug.Print
    respcapac = HASARNG.ConsultarCapacidadesImpresoraFiscal(SoportaCajon, EstacionPorDefecto, NoDocumento)
    Debug.Print "Soporta caj�n dinero      = [" & respcapac.Valor & "]"
    Debug.Print
    respcapac = HASARNG.ConsultarCapacidadesImpresoraFiscal(SoportaEstacion, EstacionTicket, TiqueFacturaA)
    Debug.Print "Soporta estaci�n tique    = [" & respcapac.Valor & "]"
    Debug.Print
    respcapac = HASARNG.ConsultarCapacidadesImpresoraFiscal(SoportaComprobante, EstacionTicket, TiqueFacturaA)
    Debug.Print "Soporta tique factura 'A' = [" & respcapac.Valor & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Cargar imagen de logotipo del emisor en la memoria de trabajo de la impresora fiscal.
'//====================================================================================================================
Private Sub CommandCargaLogo_Click()

    Debug.Print
    Debug.Print "CARGANDO LOGO EMISOR..."
    Debug.Print

On Error GoTo keka
    HASARNG.CargarLogoEmisor ComienzoCargaLogo, "424D9E180000000000003E00000028000000DC010000680000000100010000000000601800000000000000000000000000000000000000000000FFFFFF00FFFF"
    
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFF00003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFF0000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF800000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF800000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFF0FFFFFFC000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFF0FFFFFF00000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFF0FFFFFE00000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FE00000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF8000000"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0000000000001FF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFC0000000000000FFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF800000000000007FFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF800000000000007FFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF8000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF8000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF00000000000003C07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF00000000000007F07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFE0000000000000FF81FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFF0FFE0000000000000FF81FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFF0FFC0000000000003FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFF0FFC0000000000007FE07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF80"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "000000000087FC7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF8000018000"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "000FFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF8000018000000FFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF0000780000001FFFFE1FFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF0003000000003FFFFE1FFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE0000007FFFFFFFFFFE0FFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE0000007FFFFFFFFFFE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE004007FFFFFFFFFFFE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE007CFFFFFFFFFFFFF80FFFFFFCFFFF9FBFFFFC7FFC0FFFFDFFFF80FFFFF873FFFC"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "7FFF1FFFEFFFE00FFFFE00FFE1FE7FC1FEFFFFC07FFFFFFFFFF0FE003FFFFFFFFFFFFFF80FFFFFFCFFFF9FBFFFFC7FFC0FFFFDFFFF80FFFFF873FFFC7FFF1FFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "EFFFE00FFFFE00FFE1FE7FC1FEFFFFC07FFFFFFFFFF0FC003FFFFFFFFFFFFFF807FFFFFCFFFF3FBFFFFCFF80007FFDFFFF0EFFFFF8F3FFF87FFF1FFFEFFE0001"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFE0001FE1FE7FC0FEFFF80007FFFFFFFFF0FC003FFFFFFFFFFFFFF807FFFFFCFFFF3FBFFFFCFF80007FFDFFFF0EFFFFF8F3FFF87FFF1FFFEFFE0001FFE0001F"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "E1FE7FC0FEFFF80007FFFFFFFFF0FC003FFFFFFFFFFF003007FFFFFCFFFF3FBFFFF0FE0FFE1FFDFFFC7E3FFFE0F3FFF87FFF1FFFEFF81FF87F83FF0FE1FE7F80"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FEFFE07FF1FFFFFFFFF0FC000FFFFFFFFFF8000007FFFFFCFFFF3FBFFFF0FE0FFE1FFDFFFC7E3FFFE0F3FFF87FFF1FFFEFF81FF87F83FF0FE1FE7F80FEFFE07F"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "F1FFFFFFFFF0FC000FFFFFFFFFFF000007FFFFFCFFF07FBFFFE3FE7FFF9FFDFFF0FE3FFFE7F3FFE07FFF1FFFEFF8FFFE3F1FFFCFE1FE7F8EFEFFE7FFF9FFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFF0FC000FFFFFFFFFFF000007FFFFFCFFF07FBFFFE3FE7FFF9FFDFFF0FE3FFFE7F3FFE07FFF1FFFEFF8FFFE3F1FFFCFE1FE7F8EFEFFE7FFF9FFFFFFFFF0FC00"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "07FFFFFFFFFFF00007FFFFFCFFE0FFBFFFEFFCFFFFC7FDFFF0FF07FFC7F3FF047FFF1FFFEFF1FFFE3F1FFFEFE1FE7FBE7EFFCFFFFC3FFFFFFFF0FC0000FFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFC007FFFFFCFFE0FFBFFFEFFCFFFFC7FDFFF0FF07FFC7F3FF047FFF1FFFEFF1FFFE3F1FFFEFE1FE7FBE7EFFCFFFFC3FFFFFFFF0F800001FFFFFFFFFFFE0"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "03FFFFFCFFEFFFBFFF8FFDFFFFE7FDFFE3FF000007F3FF1C7FFF1FFFEFF1FFFE3F7FFFEFE1FE7FBE7EFFDFFFFE3FFFFFFFF0F800001FFFFFFFFFFFE003FFFFFC"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFEFFFBFFF8FFDFFFFE7FDFFE3FF000007F3FF1C7FFF1FFFEFF1FFFE3F7FFFEFE1FE7FBE7EFFDFFFFE3FFFFFFFF0F8000001FFFFFFFFFFF003FFFFFCFFCFFFBF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FC1FF1FFFFE0FC3F0FFFC0001FF3FE1C7FFF1FFFEFFFFFFE3FFFFFCFE1FE7E3F7EFF9FFFFE0FFFFFFFF0F80000001FFFFFFFFFF003FFFFFCFFCFFFBFFC1FF1FF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFE0FC3F0FFFC0001FF3FE1C7FFF1FFFEFFFFFFE3FFFFFCFE1FE7E3F7EFF9FFFFE0FFFFFFFF0F800000007FFFFFFFFF063FFFFFC7E1FFF87F07FF1FFFFE0FC0F"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "1FFFC3FF9FF3F8FC7FFF1FFFEFFFFFF07FFFFF0FE1FE7E7F7EFF9FFFFE0FFFFFFFF0FC00000003FFFFFFFFF067FFFFFC7E1FFF87F07FF1FFFFE0FC0F1FFFC3FF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "9FF3F8FC7FFF1FFFEFFFFFF07FFFFF0FE1FE7E7F7EFF9FFFFE0FFFFFFFF0FC00000003FFFFFFFFF067FFFFFC0C3FFF80000FF1FFFFE0FC041FFFC3FFBFF3F8FC"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "7FFF1FFFEFFFFC01FFFF003FE1FE7E7F3EFF9FFFFE0FFFFFFFF0FC00000001FFFFFFFFF867FFFFFC0C3FFF80000FF1FFFFE0FC041FFFC3FFBFF3F8FC7FFF1FFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "EFFFFC01FFFF003FE1FE7E7F3EFF9FFFFE0FFFFFFFF0FC00000000FFFFFFF00007FFFFFC0C3FFF80000FF1FFFFE0FC041FFFC3FFBFF3F8FC7FFF1FFFEFFFFC01"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFF003FE1FE7E7F3EFF9FFFFE0FFFFFFFF0FC000000007FFFFFFC0007FFFFFC803FFF87FF83F1FFFFE0FDF0FFFFF8FE3FF3F1FC7FFF1FFFEFFF000FFFE007FF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "E1FE787F3EFF9FFFFE0FFFFFFFF0FC000000007FFFFFFC0007FFFFFC803FFF87FF83F1FFFFE0FDF0FFFFF8FE3FF3F1FC7FFF1FFFEFFF000FFFE007FFE1FE787F"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "3EFF9FFFFE0FFFFFFFF0FC000000001FFFFFFF0007FFFFFCF1FFFFBFFFE3F1FFFFE0FDF8FFFFF8FE7FF3F7FC7FFF1FFFEFF81FFFFF81FFFFE1FE787FBEFF9FFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FE0FFFFFFFF0FC0000000007FFFFFF8007FFFFFCF1FFFFBFFFE3F1FFFFE0FDF8FFFFF8FE7FF3F7FC7FFF1FFFEFF81FFFFF81FFFFE1FE787FBEFF9FFFFE0FFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFF0FC0000000003FFFFFFC007FFFFFCFC3FFFBFFFF0FDFFFFE7FDFC1FFFFCF07FF3C7FC7FFF1FFFEFF8FFFFFF1FFFFFE1FE79FFBEFFDFFFFE3FFFFFFFF0FC00"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "00000003FFFFFFC007FFFFFCFC3FFFBFFFF0FDFFFFE7FDFC1FFFFCF07FF3C7FC7FFF1FFFEFF8FFFFFF1FFFFFE1FE79FFBEFFDFFFFE3FFFFFFFF0FE0000000001"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFF00FFFFFFCFE1FFFBFFFF0FCFFFFC7FDFC0FFFFC707FF38FFC7FFF1FFFEFF9FFFFFF1FFFFFE1FE71FF82FFCFFFFC3FFFFFFFF0FE0000000000FFFFFFF8"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "0FFFFFFCFE1FFFBFFFF0FCFFFFC7FDFC0FFFFC707FF38FFC7FFF1FFFEFF9FFFFFF1FFFFFE1FE71FF82FFCFFFFC3FFFFFFFF0FE00000000003FFFFFFF0FFFFFFC"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFCFFFBFFFF0FCFFFFC7FDFF03FFFC71FFF00FFC7FFF1FFFEFF9FFF87F1FFFCFE1FE71FFC0FFCFFFFC3FFFFFFFF0FF00000000001FFFFFFF9FFFFFFCFFCFFFBF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFF0FCFFFFC7FDFF03FFFC71FFF00FFC7FFF1FFFEFF9FFF87F1FFFCFE1FE71FFC0FFCFFFFC3FFFFFFFF0FF00000000001FFFFFFF9FFFFFFCFFE0FFBFFFE3FE7F"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FF9FFDFFE0FFFF61FFF03FFC7FFF1FFFEFF8FFF87F9FFF1FE1FE07FFC0FFE7FFF9FFFFFFFFF0FF00000000000FFFFFFFFFFFFFFCFFE0FFBFFFE3FE7FFF9FFDFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "E0FFFF61FFF03FFC7FFF1FFFEFF8FFF87F9FFF1FE1FE07FFC0FFE7FFF9FFFFFFFFF0FF800000000007FFFFFFFFFFFFFCFFF07F800003FF03F03FFDFFF07FFF0F"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFF07FFC7E00000FEFFE0781FF81F81FE1FE07FFC0FFF03F83FFFFFFFFF0FF800000000003FFFFFFFFFFFFFCFFF07F800003FF03F03FFDFFF07FFF0FFFF07FFC"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "7E00000FEFFE0781FF81F81FE1FE07FFC0FFF03F83FFFFFFFFF0FF800000000003FFFFFFFFFFFFFCFFFF3F80001FFF80007FFDFFFC7FFF8FFFF07FFC7E00000F"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "EFFF0007FFE000FFE1FE07FFC0FFF80007FFFFFFFFF0FF800000000000FFFFDFFFFFFFFCFFFF3F80001FFF80007FFDFFFC7FFF8FFFF07FFC7E00000FEFFF0007"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFE000FFE1FE07FFC0FFF80007FFFFFFFFF0FFC000000000007FFFE1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFE000000000001FFFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFF0FFE000000000000FFFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFF0FFE000000000000FFFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFF0FFF0000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF8"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC00000000"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC000000000000FFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC0000000000007FFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF0000000000003FFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF8000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF8000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF8000000000303FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFC000000000801FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF01000000603EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF01000000603EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF80100007007F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFF0FFFFFFE001FFE003FF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFF0FFFFFFFE0000001FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFF0FFFFFFFE0000001FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFE00000FFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFF00"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    HASARNG.CargarLogoEmisor CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"
    
    HASARNG.CargarLogoEmisor FinCargaLogo, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0"
    
    Debug.Print
    Debug.Print "LOGO DEL EMISOR CARGADO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Configurar velocidad de transferencia de informaci�n (baudios) del puerto serie RS-232.
'//====================================================================================================================
Private Sub CommandCfgBaudios_Click()

    Debug.Print
    Debug.Print "CONFIGURANDO BAUDIOS RS-232..."
    Debug.Print

On Error GoTo keka
    HASARNG.CambiarVelocidadPuerto Baudrate9600
    Debug.Print "Baudios = 9600"
    
    Debug.Print
    Debug.Print "FIN DE LA CONFIGURACION !..."
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Configurar fecha y hora en la impresora fiscal.
'//====================================================================================================================
Private Sub CommandCfgFyh_Click()

    Debug.Print
    Debug.Print "CONFIGURANDO FECHA Y HORA..."
    Debug.Print

On Error GoTo keka
    HASARNG.ConfigurarFechaHora "06/03/2015", "10:49:45"
    
    Debug.Print
    Debug.Print "FIN DE LA CONFIGURACION !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Configurar par�metros de comportamiento de la impresora fiscal.
'//====================================================================================================================
Private Sub CommandCfgImprFis_Click()

    Debug.Print
    Debug.Print "CONFIGURANDO PARAMETROS IMPRESORA FISCAL..."
    Debug.Print

On Error GoTo keka
    Debug.Print "Borrado autom�tico CTD        = P"
    HASARNG.ConfigurarImpresoraFiscal BorradoAutomaticoAuditoria, "P"
    Debug.Print
    
    Debug.Print "Verificar supera monto m�ximo = N"
    HASARNG.ConfigurarImpresoraFiscal ChequeoDesborde, "N"
    Debug.Print
    
    Debug.Print "Corte de papel                = P"
    HASARNG.ConfigurarImpresoraFiscal CortePapel, "P"
    Debug.Print
    
    Debug.Print "Imprimir CAMBIO $0.00         = P"
    HASARNG.ConfigurarImpresoraFiscal ImpresionCambio, "P"
    Debug.Print
    
    Debug.Print "Imprimir leyendas opcionales  = P"
    HASARNG.ConfigurarImpresoraFiscal ImpresionLeyendas, "P"
    Debug.Print
    
    Debug.Print "Texto saldo  pendientes       = Pendiente de pago"
    HASARNG.ConfigurarImpresoraFiscal PagoSaldo, "Pendiente de pago"
    Debug.Print
    
    Debug.Print "Aviso sonoro falta papel      = P"
    HASARNG.ConfigurarImpresoraFiscal SonidoAviso, "P"
    Debug.Print
    
    'Modificar estos par�metros requiere Cierre 'Z' previo.
    '------------------------------------------------------
    'Debug.Print "Monto limite 'B/C'            = 0.00"
    'HASARNG.ConfigurarImpresoraFiscal LimiteBC, "0.00"
    ' Debug.Print
    
'    Debug.Print "Tipo de habilitaci�n          = A"
'    HASARNG.ConfigurarImpresoraFiscal TipoHabilitacion, "A"
    'Debug.Print

    Debug.Print
    Debug.Print "FIN DE LA CONFIGURACION !..."
    Debug.Print
        
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Configurar par�metros de conectividad en red.
'//====================================================================================================================
Private Sub CommandCfgRed_Click()

    Debug.Print
    Debug.Print "CONFIGURANDO PARAMETROS CONECTIVIDAD EN RED..."
    Debug.Print

On Error GoTo keka
    Debug.Print "Direcci�n IP  = 10.0.7.69"
    Debug.Print "M�scara       = 255.255.255.0"
    Debug.Print "Puerta enlace = 10.0.7.69"
    HASARNG.ConfigurarRed "10.0.7.69", "255.255.255.0", "10.0.7.69"
    
    Debug.Print
    Debug.Print "FIN DE LA CONFIGURACION !..."
    Debug.Print
        
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Configurar servidor SMHTP para correo electr�nico.
'//====================================================================================================================
Private Sub CommandCfgServCorreo_Click()

    Debug.Print
    Debug.Print "CONFIGURANDO SERVIDOR SMTP..."
    Debug.Print

On Error GoTo keka
    Debug.Print "IP SMTP     = 200.80.204.3"
    Debug.Print "Puerto      = 465"
    Debug.Print "Responder a = hasarventas@hasar.com"
    HASARNG.ConfigurarServidorCorreo "200.80.204.3", 465, "hasarventas@hasar.com"
    
    Debug.Print
    Debug.Print "FIN DE LA CONFIGURACION !..."
    Debug.Print
        
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Configurar Cierre 'Z' l�mite para borrado de la CTD.
'//====================================================================================================================
Private Sub CommandCfgZBorr_Click()

    Debug.Print
    Debug.Print "CONFIGURANDO LIMITE BORRADO CTD..."
    Debug.Print

On Error GoTo keka
    Debug.Print "L�mite, Cierre 'Z' Nro. = 1"
    HASARNG.ConfigurarZetaBorrable 1
    
    Debug.Print
    Debug.Print "FIN DE LA CONFIGURACION !..."
    Debug.Print
        
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n del Informe Diario de Cierre 'Z'.
'//====================================================================================================================
Private Sub CommandCFgZona_Click()
Dim estilo As AtributosDeTexto

    Debug.Print
    Debug.Print "CONFIGURANDO ZONAS LINEAS DE USUARIO..."
    Debug.Print
    
    estilo.Centrado = True
    estilo.DobleAncho = False
    estilo.Negrita = True

On Error GoTo keka
    HASARNG.ConfigurarZona 1, estilo, "= Golosinas y Afines =", EstacionTicket, ZonaFantasia
    HASARNG.ConfigurarZona 2, estilo, "(Atendemos al Gremio)", EstacionTicket, ZonaFantasia
    
    estilo.Negrita = False
    
    HASARNG.ConfigurarZona 1, estilo, "Los cambios se efect�an en los mismos d�as y horarios en los", EstacionTicket, Zona1Encabezado
    HASARNG.ConfigurarZona 2, estilo, "que el comercio atienda al p�blico para ventas.", EstacionTicket, Zona1Encabezado
    HASARNG.ConfigurarZona 3, estilo, "= LU a VI: 09 a 13 hs y 15 a 20 hs - SA: 09 a 14 hs", EstacionTicket, Zona1Encabezado
    
    HASARNG.ConfigurarZona 1, estilo, "Texto l�nea 1 - Zona 2 Encabezado", EstacionTicket, Zona2Encabezado
    HASARNG.ConfigurarZona 2, estilo, "Texto l�nea 2 - Zona 2 Encabezado", EstacionTicket, Zona2Encabezado
    HASARNG.ConfigurarZona 3, estilo, "Texto l�nea 3 - Zona 2 Encabezado", EstacionTicket, Zona2Encabezado
    
    HASARNG.ConfigurarZona 1, estilo, "CAJA: 03 - CAJERO/A: Zacar�as Flores", EstacionTicket, Zona1Cola
    estilo.Negrita = True
    HASARNG.ConfigurarZona 2, estilo, "MUCHAS GRACIAS POR SU COMPRA", EstacionTicket, Zona1Cola
    HASARNG.ConfigurarZona 3, estilo, "Orientaci�n al Consumidor Provincia de Buenos Aires", EstacionTicket, Zona1Cola
    HASARNG.ConfigurarZona 4, estilo, "0800-222-9042", EstacionTicket, Zona1Cola
    
    estilo.Centrado = False
    estilo.DobleAncho = False
    estilo.Negrita = False
    
    HASARNG.ConfigurarZona 1, estilo, "Compras por montos inferiores (o iguales) a $ 100.00 s�lo se", EstacionTicket, Zona2Cola
    HASARNG.ConfigurarZona 2, estilo, "podr�n abonar en efectivo.", EstacionTicket, Zona2Cola
    HASARNG.ConfigurarZona 3, estilo, "Aceptamos todas las tarjetas de cr�dito / d�bito.", EstacionTicket, Zona2Cola
    HASARNG.ConfigurarZona 4, estilo, "Consulte nuestro sitio web oficial para enterarse de todas", EstacionTicket, Zona2Cola
    HASARNG.ConfigurarZona 5, estilo, "las promociones vigentes.", EstacionTicket, Zona2Cola
    HASARNG.ConfigurarZona 6, estilo, "SOLO X HOY: Cereal en caja X 300g $ 35.99", EstacionTicket, Zona2Cola
    
    HASARNG.ConfigurarZona 1, estilo, "Pasaje Ignoto 777 - (1408) Villa Real", EstacionTicket, ZonaDomicilioEmisor
    HASARNG.ConfigurarZona 2, estilo, "Ciudad Aut�noma de Buenos Aires", EstacionTicket, ZonaDomicilioEmisor
    HASARNG.ConfigurarZona 3, estilo, "Rep�blica Argentina", EstacionTicket, ZonaDomicilioEmisor
    HASARNG.ConfigurarZona 4, estilo, "Am�rica del Sur", EstacionTicket, ZonaDomicilioEmisor
    
    Debug.Print
    Debug.Print "FIN de la CONFIGURACION...."
    Debug.Print
        
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n del Informe Diario de Cierre 'Z'.
'//====================================================================================================================
Private Sub CommandCierreZ_Click()
Dim cierre As RespuestaCerrarJornadaFiscal
Dim zeta As CerrarJornadaFiscalZ
Dim rep As TipoReporte

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO INFORME DIARIO DE CIERRE 'Z':"
    Debug.Print
    
    cierre = HASARNG.CerrarJornadaFiscal(ReporteZ)
    zeta = cierre.Z
    rep = cierre.Reporte
    
    Debug.Print
    Debug.Print "Reporte...           = [" & FromTipoReporteToString(rep) & "]"
    Debug.Print
    Debug.Print "Cierre Nro.          = [" & zeta.Numero & "]"
    Debug.Print "Fecha cierre         = [" & zeta.Fecha & "]"
    Debug.Print
    Debug.Print "DF cancelados        = [" & zeta.DF_CantidadCancelados & "]"
    Debug.Print "DF emitidos          = [" & zeta.DF_CantidadEmitidos & "]"
    Debug.Print
    Debug.Print "DF total ventas      = [" & zeta.DF_Total & "]"
    Debug.Print "DF total gravado     = [" & zeta.DF_TotalGravado & "]"
    Debug.Print "DF total IVA         = [" & zeta.DF_TotalIVA & "]"
    Debug.Print "DF total no gravado  = [" & zeta.DF_TotalNoGravado & "]"
    Debug.Print "DF total exento      = [" & zeta.DF_TotalExento & "]"
    Debug.Print "DF total tributos    = [" & zeta.DF_TotalTributos & "]"
    Debug.Print
    Debug.Print "DNFH emitidos        = [" & zeta.DNFH_CantidadEmitidos & "]"
    Debug.Print "DNFH total acumulado = [" & zeta.DNFH_Total & "]"
    Debug.Print
    Debug.Print "NC canceladas        = [" & zeta.NC_CantidadCancelados & "]"
    Debug.Print "NC emitidas          = [" & zeta.NC_CantidadEmitidos & "]"
    Debug.Print "NC total cr�dito     = [" & zeta.NC_Total & "]"
    Debug.Print "NC total gravado     = [" & zeta.NC_TotalGravado & "]"
    Debug.Print "NC total IVA         = [" & zeta.NC_TotalIVA & "]"
    Debug.Print "NC total no gravado  = [" & zeta.NC_TotalNoGravado & "]"
    Debug.Print "NC total exento      = [" & zeta.NC_TotalExento & "]"
    Debug.Print "NC total tributos    = [" & zeta.NC_TotalTributos & "]"
    
    Debug.Print
    Debug.Print "INFORME DIARIO DE CIERRE 'Z'... IMPRESO !..."
    Debug.Print

    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consulta de acumulados para determnado comprobante.
'//====================================================================================================================
Private Sub CommandConsAcumComprob_Click()
Dim acomprob As RespuestaConsultarAcumuladosComprobante
Dim acomprob2 As RespuestaContinuarConsultaAcumulados

    Debug.Print
    Debug.Print "CONSULTANDO ACUMULADOS COMPROBANTE:"
    Debug.Print

On Error GoTo keka
    acomprob = HASARNG.ConsultarAcumuladosComprobante(TiqueFacturaC, 5)
    
    If (acomprob.Registro = HasarArgentina.TiposDeRegistroInforme.RegistroFinal) Then
        Debug.Print "NO HAY INFORMACION DISPONIBLE !..."
        Debug.Print
        
        Exit Sub
    End If
    
    Select Case acomprob.Registro
        Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & acomprob.Registro & "]"
            Debug.Print
            Debug.Print "DF (Documentos Fiscales):"
            Debug.Print "-------------------------"
            Debug.Print "DF Tipo comprobante        = [" & FromTiposComprobanteToString(acomprob.RegDF.CodigoComprobante) & "]"
            Debug.Print "DF Nro. inicial            = [" & acomprob.RegDF.NumeroInicial & "]"
            Debug.Print "DF Nro. final              = [" & acomprob.RegDF.NumeroFinal & "]"
            Debug.Print "DF cancelados              = [" & acomprob.RegDF.CantidadCancelados & "]"
            Debug.Print
            Debug.Print "DF total ventas            = [" & acomprob.RegDF.Total & "]"
            Debug.Print "DF total gravado           = [" & acomprob.RegDF.TotalGravado & "]"
            Debug.Print "DF total no gravado        = [" & acomprob.RegDF.TotalNoGravado & "]"
            Debug.Print "DF total exento            = [" & acomprob.RegDF.TotalExento & "]"
            Debug.Print
            Debug.Print "DF IVA 1                   = [" & acomprob.RegDF.AlicuotaIVA_1 & "]"
            Debug.Print "DF monto IVA 1             = [" & acomprob.RegDF.MontoIVA_1 & "]"
            Debug.Print "DF base imponible 1        = [" & acomprob.RegDF.MontoNetoSinIVA_1 & "]"
            Debug.Print "DF IVA 2                   = [" & acomprob.RegDF.AlicuotaIVA_2 & "]"
            Debug.Print "DF monto IVA 2             = [" & acomprob.RegDF.MontoIVA_2 & "]"
            Debug.Print "DF base imponible 2        = [" & acomprob.RegDF.MontoNetoSinIVA_2 & "]"
            Debug.Print "DF IVA 3                   = [" & acomprob.RegDF.AlicuotaIVA_3 & "]"
            Debug.Print "DF monto IVA 3             = [" & acomprob.RegDF.MontoIVA_3 & "]"
            Debug.Print "DF base imponible 3        = [" & acomprob.RegDF.MontoNetoSinIVA_3 & "]"
            Debug.Print "DF IVA 4                   = [" & acomprob.RegDF.AlicuotaIVA_4 & "]"
            Debug.Print "DF monto IVA 4             = [" & acomprob.RegDF.MontoIVA_4 & "]"
            Debug.Print "DF base imponible 4        = [" & acomprob.RegDF.MontoNetoSinIVA_4 & "]"
            Debug.Print "DF IVA 5                   = [" & acomprob.RegDF.AlicuotaIVA_5 & "]"
            Debug.Print "DF monto IVA 5             = [" & acomprob.RegDF.MontoIVA_5 & "]"
            Debug.Print "DF base imponible 5        = [" & acomprob.RegDF.MontoNetoSinIVA_5 & "]"
            Debug.Print "DF IVA 6                   = [" & acomprob.RegDF.AlicuotaIVA_6 & "]"
            Debug.Print "DF monto IVA 6             = [" & acomprob.RegDF.MontoIVA_6 & "]"
            Debug.Print "DF base imponible 6        = [" & acomprob.RegDF.MontoNetoSinIVA_6 & "]"
            Debug.Print "DF IVA 7                   = [" & acomprob.RegDF.AlicuotaIVA_7 & "]"
            Debug.Print "DF monto IVA 7             = [" & acomprob.RegDF.MontoIVA_7 & "]"
            Debug.Print "DF base imponible 7        = [" & acomprob.RegDF.MontoNetoSinIVA_7 & "]"
            Debug.Print "DF IVA 8                   = [" & acomprob.RegDF.AlicuotaIVA_8 & "]"
            Debug.Print "DF monto IVA 8             = [" & acomprob.RegDF.MontoIVA_8 & "]"
            Debug.Print "DF base imponible 8        = [" & acomprob.RegDF.MontoNetoSinIVA_8 & "]"
            Debug.Print "DF IVA 9                   = [" & acomprob.RegDF.AlicuotaIVA_9 & "]"
            Debug.Print "DF monto IVA 9             = [" & acomprob.RegDF.MontoIVA_9 & "]"
            Debug.Print "DF base imponible 9        = [" & acomprob.RegDF.MontoNetoSinIVA_9 & "]"
            Debug.Print "DF IVA 10                  = [" & acomprob.RegDF.AlicuotaIVA_10 & "]"
            Debug.Print "DF monto IVA 10            = [" & acomprob.RegDF.MontoIVA_10 & "]"
            Debug.Print "DF base imponible 10       = [" & acomprob.RegDF.MontoNetoSinIVA_10 & "]"
            Debug.Print
            Debug.Print "DF total IVA               = [" & acomprob.RegDF.TotalIVA & "]"
            Debug.Print
            Debug.Print "DF tributo 1               = [" & acomprob.RegDF.CodigoTributo1 & "]"
            Debug.Print "DF monto tributo 1         = [" & acomprob.RegDF.ImporteTributo1 & "]"
            Debug.Print "DF tributo 2               = [" & acomprob.RegDF.CodigoTributo2 & "]"
            Debug.Print "DF monto tributo 2         = [" & acomprob.RegDF.ImporteTributo2 & "]"
            Debug.Print "DF tributo 3               = [" & acomprob.RegDF.CodigoTributo3 & "]"
            Debug.Print "DF monto tributo 3         = [" & acomprob.RegDF.ImporteTributo3 & "]"
            Debug.Print "DF tributo 4               = [" & acomprob.RegDF.CodigoTributo4 & "]"
            Debug.Print "DF monto tributo 4         = [" & acomprob.RegDF.ImporteTributo4 & "]"
            Debug.Print "DF tributo 5               = [" & acomprob.RegDF.CodigoTributo5 & "]"
            Debug.Print "DF monto tributo 5         = [" & acomprob.RegDF.ImporteTributo5 & "]"
            Debug.Print "DF tributo 6               = [" & acomprob.RegDF.CodigoTributo6 & "]"
            Debug.Print "DF monto tributo 6         = [" & acomprob.RegDF.ImporteTributo6 & "]"
            Debug.Print "DF tributo 7               = [" & acomprob.RegDF.CodigoTributo7 & "]"
            Debug.Print "DF monto tributo 7         = [" & acomprob.RegDF.ImporteTributo7 & "]"
            Debug.Print "DF tributo 8               = [" & acomprob.RegDF.CodigoTributo8 & "]"
            Debug.Print "DF monto tributo 8         = [" & acomprob.RegDF.ImporteTributo8 & "]"
            Debug.Print "DF tributo 9               = [" & acomprob.RegDF.CodigoTributo9 & "]"
            Debug.Print "DF monto tributo 9         = [" & acomprob.RegDF.ImporteTributo9 & "]"
            Debug.Print "DF tributo 10              = [" & acomprob.RegDF.CodigoTributo10 & "]"
            Debug.Print "DF monto tributo 10        = [" & acomprob.RegDF.ImporteTributo10 & "]"
            Debug.Print "DF tributo 11              = [" & acomprob.RegDF.CodigoTributo11 & "]"
            Debug.Print "DF monto tributo 11        = [" & acomprob.RegDF.ImporteTributo11 & "]"
            Debug.Print
            Debug.Print "DF total tributos          = [" & acomprob.RegDF.TotalTributos & "]"
            Debug.Print
        Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & acomprob.Registro & "]"
            Debug.Print
            Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE ACUMULAN="
            Debug.Print "---------------------------------------------------------"
            Debug.Print "DNFH Tipo Comprob.         = [" & FromTiposComprobanteToString(acomprob.RegDF.CodigoComprobante) & "]"
            Debug.Print "DNFH Nro. inicial          = [" & acomprob.RegDNFH.NumeroInicial & "]"
            Debug.Print "DNFH Nro. Final            = [" & acomprob.RegDNFH.NumeroFinal & "]"
            Debug.Print "DNFH total acumulado       = [" & acomprob.RegDNFH.Total & "]"
            Debug.Print
        Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & acomprob.Registro & "]"
            Debug.Print
            Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE NO ACUMULAN="
            Debug.Print "------------------------------------------------------------"
            Debug.Print "Tipo Comprobante = [" & FromTiposComprobanteToString(acomprob.RegDF.CodigoComprobante) & "]"
            Debug.Print "N� inicial       = [" & acomprob.RegDNFH_NoAcum.NumeroInicial & "]"
            Debug.Print "N� final         = [" & acomprob.RegDNFH_NoAcum.NumeroFinal & "]"
            Debug.Print
        Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & acomprob.Registro & "]"
            Debug.Print
            Debug.Print "INFORMACION GLOBAL:"
            Debug.Print "-------------------"
            Debug.Print "DF cant. cancelados = [" & acomprob.RegGlobal.DF_CantidadCancelados & "]"
            Debug.Print "DF cant. emitidos   = [" & acomprob.RegGlobal.DF_CantidadEmitidos & "]"
            Debug.Print
            Debug.Print "DF total ventas     = [" & acomprob.RegGlobal.DF_Total & "]"
            Debug.Print
            Debug.Print "DF total gravado    = [" & acomprob.RegGlobal.DF_TotalGravado & "]"
            Debug.Print "DF total IVA        = [" & acomprob.RegGlobal.DF_TotalIVA & "]"
            Debug.Print "DF total tributos   = [" & acomprob.RegGlobal.DF_TotalTributos & "]"
            Debug.Print "DF total no gravado = [" & acomprob.RegGlobal.DF_TotalNoGravado & "]"
            Debug.Print "DF total exento     = [" & acomprob.RegGlobal.DF_TotalExento & "]"
            Debug.Print
            Debug.Print "DF total DNFH       = [" & acomprob.RegGlobal.DNFH_Total & "]"
            Debug.Print
    End Select

    acomprob2 = HASARNG.ContinuarConsultaAcumulados

    While (acomprob2.Registro <> HasarArgentina.TiposDeRegistroInforme.RegistroFinal)
        Select Case acomprob2.Registro
            Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & acomprob2.Registro & "]"
                Debug.Print
                Debug.Print "DF (Documentos Fiscales):"
                Debug.Print "-------------------------"
                Debug.Print "DF Tipo comprobante        = [" & FromTiposComprobanteToString(acomprob2.RegDF.CodigoComprobante) & "]"
                Debug.Print "DF Nro. inicial            = [" & acomprob2.RegDF.NumeroInicial & "]"
                Debug.Print "DF Nro. final              = [" & acomprob2.RegDF.NumeroFinal & "]"
                Debug.Print "DF cancelados              = [" & acomprob2.RegDF.CantidadCancelados & "]"
                Debug.Print
                Debug.Print "DF total ventas            = [" & acomprob2.RegDF.Total & "]"
                Debug.Print "DF total gravado           = [" & acomprob2.RegDF.TotalGravado & "]"
                Debug.Print "DF total no gravado        = [" & acomprob2.RegDF.TotalNoGravado & "]"
                Debug.Print "DF total exento            = [" & acomprob2.RegDF.TotalExento & "]"
                Debug.Print
                Debug.Print "DF IVA 1                   = [" & acomprob2.RegDF.AlicuotaIVA_1 & "]"
                Debug.Print "DF monto IVA 1             = [" & acomprob2.RegDF.MontoIVA_1 & "]"
                Debug.Print "DF IVA 2                   = [" & acomprob2.RegDF.AlicuotaIVA_2 & "]"
                Debug.Print "DF monto IVA 2             = [" & acomprob2.RegDF.MontoIVA_2 & "]"
                Debug.Print "DF IVA 3                   = [" & acomprob2.RegDF.AlicuotaIVA_3 & "]"
                Debug.Print "DF monto IVA 3             = [" & acomprob2.RegDF.MontoIVA_3 & "]"
                Debug.Print "DF IVA 4                   = [" & acomprob2.RegDF.AlicuotaIVA_4 & "]"
                Debug.Print "DF monto IVA 4             = [" & acomprob2.RegDF.MontoIVA_4 & "]"
                Debug.Print "DF IVA 5                   = [" & acomprob2.RegDF.AlicuotaIVA_5 & "]"
                Debug.Print "DF monto IVA 5             = [" & acomprob2.RegDF.MontoIVA_5 & "]"
                Debug.Print "DF IVA 6                   = [" & acomprob2.RegDF.AlicuotaIVA_6 & "]"
                Debug.Print "DF monto IVA 6             = [" & acomprob2.RegDF.MontoIVA_6 & "]"
                Debug.Print "DF IVA 7                   = [" & acomprob2.RegDF.AlicuotaIVA_7 & "]"
                Debug.Print "DF monto IVA 7             = [" & acomprob2.RegDF.MontoIVA_7 & "]"
                Debug.Print "DF IVA 8                   = [" & acomprob2.RegDF.AlicuotaIVA_8 & "]"
                Debug.Print "DF monto IVA 8             = [" & acomprob2.RegDF.MontoIVA_8 & "]"
                Debug.Print "DF IVA 9                   = [" & acomprob2.RegDF.AlicuotaIVA_9 & "]"
                Debug.Print "DF monto IVA 9             = [" & acomprob2.RegDF.MontoIVA_9 & "]"
                Debug.Print "DF IVA 10                  = [" & acomprob2.RegDF.AlicuotaIVA_10 & "]"
                Debug.Print "DF monto IVA 10            = [" & acomprob2.RegDF.MontoIVA_10 & "]"
                Debug.Print
                Debug.Print "DF total IVA               = [" & acomprob2.RegDF.TotalIVA & "]"
                Debug.Print
                Debug.Print "DF tributo 1               = [" & acomprob2.RegDF.CodigoTributo1 & "]"
                Debug.Print "DF monto tributo 1         = [" & acomprob2.RegDF.ImporteTributo1 & "]"
                Debug.Print "DF tributo 2               = [" & acomprob2.RegDF.CodigoTributo2 & "]"
                Debug.Print "DF monto tributo 2         = [" & acomprob2.RegDF.ImporteTributo2 & "]"
                Debug.Print "DF tributo 3               = [" & acomprob2.RegDF.CodigoTributo3 & "]"
                Debug.Print "DF monto tributo 3         = [" & acomprob2.RegDF.ImporteTributo3 & "]"
                Debug.Print "DF tributo 4               = [" & acomprob2.RegDF.CodigoTributo4 & "]"
                Debug.Print "DF monto tributo 4         = [" & acomprob2.RegDF.ImporteTributo4 & "]"
                Debug.Print "DF tributo 5               = [" & acomprob2.RegDF.CodigoTributo5 & "]"
                Debug.Print "DF monto tributo 5         = [" & acomprob2.RegDF.ImporteTributo5 & "]"
                Debug.Print "DF tributo 6               = [" & acomprob2.RegDF.CodigoTributo6 & "]"
                Debug.Print "DF monto tributo 6         = [" & acomprob2.RegDF.ImporteTributo6 & "]"
                Debug.Print "DF tributo 7               = [" & acomprob2.RegDF.CodigoTributo7 & "]"
                Debug.Print "DF monto tributo 7         = [" & acomprob2.RegDF.ImporteTributo7 & "]"
                Debug.Print "DF tributo 8               = [" & acomprob2.RegDF.CodigoTributo8 & "]"
                Debug.Print "DF monto tributo 8         = [" & acomprob2.RegDF.ImporteTributo8 & "]"
                Debug.Print "DF tributo 9               = [" & acomprob2.RegDF.CodigoTributo9 & "]"
                Debug.Print "DF monto tributo 9         = [" & acomprob2.RegDF.ImporteTributo9 & "]"
                Debug.Print "DF tributo 10              = [" & acomprob2.RegDF.CodigoTributo10 & "]"
                Debug.Print "DF monto tributo 10        = [" & acomprob2.RegDF.ImporteTributo10 & "]"
                Debug.Print "DF tributo 11              = [" & acomprob2.RegDF.CodigoTributo11 & "]"
                Debug.Print "DF monto tributo 11        = [" & acomprob2.RegDF.ImporteTributo11 & "]"
                Debug.Print
                Debug.Print "DF total tributos          = [" & acomprob2.RegDF.TotalTributos & "]"
                Debug.Print
            Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & acomprob2.Registro & "]"
                Debug.Print
                Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE ACUMULAN="
                Debug.Print "---------------------------------------------------------"
                Debug.Print "DNFH Tipo Comprob.         = [" & FromTiposComprobanteToString(acomprob2.RegDF.CodigoComprobante) & "]"
                Debug.Print "DNFH Nro. inicial          = [" & acomprob2.RegDNFH.NumeroInicial & "]"
                Debug.Print "DNFH Nro. Final            = [" & acomprob2.RegDNFH.NumeroFinal & "]"
                Debug.Print "DNFH total acumulado       = [" & acomprob2.RegDNFH.Total & "]"
                Debug.Print
            Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & acomprob2.Registro & "]"
                Debug.Print
                Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE NO ACUMULAN="
                Debug.Print "------------------------------------------------------------"
                Debug.Print "Tipo Comprobante = [" & FromTiposComprobanteToString(acomprob2.RegDF.CodigoComprobante) & "]"
                Debug.Print "N� inicial       = [" & acomprob2.RegDNFH_NoAcum.NumeroInicial & "]"
                Debug.Print "N� final         = [" & acomprob2.RegDNFH_NoAcum.NumeroFinal & "]"
                Debug.Print
            Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & acomprob2.Registro & "]"
                Debug.Print
                Debug.Print "INFORMACION GLOBAL:"
                Debug.Print "-------------------"
                Debug.Print "DF cant. cancelados = [" & acomprob2.RegGlobal.DF_CantidadCancelados & "]"
                Debug.Print "DF cant. emitidos   = [" & acomprob2.RegGlobal.DF_CantidadEmitidos & "]"
                Debug.Print
                Debug.Print "DF total ventas     = [" & acomprob2.RegGlobal.DF_Total & "]"
                Debug.Print
                Debug.Print "DF total gravado    = [" & acomprob2.RegGlobal.DF_TotalGravado & "]"
                Debug.Print "DF total IVA        = [" & acomprob2.RegGlobal.DF_TotalIVA & "]"
                Debug.Print "DF total tributos   = [" & acomprob2.RegGlobal.DF_TotalTributos & "]"
                Debug.Print "DF total no gravado = [" & acomprob2.RegGlobal.DF_TotalNoGravado & "]"
                Debug.Print "DF total exento     = [" & acomprob2.RegGlobal.DF_TotalExento & "]"
                Debug.Print
                Debug.Print "DF total DNFH       = [" & acomprob2.RegGlobal.DNFH_Total & "]"
                Debug.Print
        End Select
        
        acomprob2 = HASARNG.ContinuarConsultaAcumulados
    Wend

    Debug.Print
    Debug.Print "NO HAY MAS INFORMACION DISPONIBLE !..."
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA..."
    Debug.Print

    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description
    
End Sub

'//====================================================================================================================
'// Consulta de acumulados en la memoria de trabajo de la impresora fiscal.
'//====================================================================================================================
Private Sub CommandConsAcumMemoria_Click()
Dim amem As RespuestaConsultarAcumuladosMemoriaDeTrabajo
Dim amem2 As RespuestaContinuarConsultaAcumulados

    Debug.Print
    Debug.Print "CONSULTANDO ACUMULADOS MEMORIA DE TRABAJO..."
    Debug.Print

On Error GoTo keka
    amem = HASARNG.ConsultarAcumuladosMemoriaDeTrabajo(NoDocumento)
    
    If (amem.Registro = HasarArgentina.TiposDeRegistroInforme.RegistroFinal) Then
        Debug.Print
        Debug.Print "NO HAY INFORMACION DISPONIBLE !..."
        Debug.Print
        
        Exit Sub
    End If
    
    Select Case amem.Registro
        Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & amem.Registro & "]"
            Debug.Print
            Debug.Print "DF (Documentos Fiscales):"
            Debug.Print "-------------------------"
            Debug.Print "DF cancelados              = [" & amem.RegDF.CantidadCancelados & "]"
            Debug.Print "DF Tipo comprobante        = [" & FromTiposComprobanteToString(amem.RegDF.CodigoComprobante) & "]"
            Debug.Print "DF Nro. inicial            = [" & amem.RegDF.NumeroInicial & "]"
            Debug.Print "DF Nro. final              = [" & amem.RegDF.NumeroFinal & "]"
            Debug.Print
            Debug.Print "DF total ventas            = [" & amem.RegDF.Total & "]"
            Debug.Print
            Debug.Print "DF total gravado           = [" & amem.RegDF.TotalGravado & "]"
            Debug.Print "DF total no gravado        = [" & amem.RegDF.TotalNoGravado & "]"
            Debug.Print "DF total exento            = [" & amem.RegDF.TotalExento & "]"
            Debug.Print
            Debug.Print "DF IVA 1                   = [" & amem.RegDF.AlicuotaIVA_1 & "]"
            Debug.Print "DF monto IVA 1             = [" & amem.RegDF.MontoIVA_1 & "]"
            Debug.Print "DF IVA 2                   = [" & amem.RegDF.AlicuotaIVA_2 & "]"
            Debug.Print "DF monto IVA 2             = [" & amem.RegDF.MontoIVA_2 & "]"
            Debug.Print "DF IVA 3                   = [" & amem.RegDF.AlicuotaIVA_3 & "]"
            Debug.Print "DF monto IVA 3             = [" & amem.RegDF.MontoIVA_3 & "]"
            Debug.Print "DF IVA 4                   = [" & amem.RegDF.AlicuotaIVA_4 & "]"
            Debug.Print "DF monto IVA 4             = [" & amem.RegDF.MontoIVA_4 & "]"
            Debug.Print "DF IVA 5                   = [" & amem.RegDF.AlicuotaIVA_5 & "]"
            Debug.Print "DF monto IVA 5             = [" & amem.RegDF.MontoIVA_5 & "]"
            Debug.Print "DF IVA 6                   = [" & amem.RegDF.AlicuotaIVA_6 & "]"
            Debug.Print "DF monto IVA 6             = [" & amem.RegDF.MontoIVA_6 & "]"
            Debug.Print "DF IVA 7                   = [" & amem.RegDF.AlicuotaIVA_7 & "]"
            Debug.Print "DF monto IVA 7             = [" & amem.RegDF.MontoIVA_7 & "]"
            Debug.Print "DF IVA 8                   = [" & amem.RegDF.AlicuotaIVA_8 & "]"
            Debug.Print "DF monto IVA 8             = [" & amem.RegDF.MontoIVA_8 & "]"
            Debug.Print "DF IVA 9                   = [" & amem.RegDF.AlicuotaIVA_9 & "]"
            Debug.Print "DF monto IVA 9             = [" & amem.RegDF.MontoIVA_9 & "]"
            Debug.Print "DF IVA 10                  = [" & amem.RegDF.AlicuotaIVA_10 & "]"
            Debug.Print "DF monto IVA 10            = [" & amem.RegDF.MontoIVA_10 & "]"
            Debug.Print
            Debug.Print "DF total IVA               = [" & amem.RegDF.TotalIVA & "]"
            Debug.Print
            Debug.Print "DF tributo 1               = [" & amem.RegDF.CodigoTributo1 & "]"
            Debug.Print "DF monto tributo 1         = [" & amem.RegDF.ImporteTributo1 & "]"
            Debug.Print "DF tributo 2               = [" & amem.RegDF.CodigoTributo2 & "]"
            Debug.Print "DF monto tributo 2         = [" & amem.RegDF.ImporteTributo2 & "]"
            Debug.Print "DF tributo 3               = [" & amem.RegDF.CodigoTributo3 & "]"
            Debug.Print "DF monto tributo 3         = [" & amem.RegDF.ImporteTributo3 & "]"
            Debug.Print "DF tributo 4               = [" & amem.RegDF.CodigoTributo4 & "]"
            Debug.Print "DF monto tributo 4         = [" & amem.RegDF.ImporteTributo4 & "]"
            Debug.Print "DF tributo 5               = [" & amem.RegDF.CodigoTributo5 & "]"
            Debug.Print "DF monto tributo 5         = [" & amem.RegDF.ImporteTributo5 & "]"
            Debug.Print "DF tributo 6               = [" & amem.RegDF.CodigoTributo6 & "]"
            Debug.Print "DF monto tributo 6         = [" & amem.RegDF.ImporteTributo6 & "]"
            Debug.Print "DF tributo 7               = [" & amem.RegDF.CodigoTributo7 & "]"
            Debug.Print "DF monto tributo 7         = [" & amem.RegDF.ImporteTributo7 & "]"
            Debug.Print "DF tributo 8               = [" & amem.RegDF.CodigoTributo8 & "]"
            Debug.Print "DF monto tributo 8         = [" & amem.RegDF.ImporteTributo8 & "]"
            Debug.Print "DF tributo 9               = [" & amem.RegDF.CodigoTributo9 & "]"
            Debug.Print "DF monto tributo 9         = [" & amem.RegDF.ImporteTributo9 & "]"
            Debug.Print "DF tributo 10              = [" & amem.RegDF.CodigoTributo10 & "]"
            Debug.Print "DF monto tributo 10        = [" & amem.RegDF.ImporteTributo10 & "]"
            Debug.Print "DF tributo 11              = [" & amem.RegDF.CodigoTributo11 & "]"
            Debug.Print "DF monto tributo 11        = [" & amem.RegDF.ImporteTributo11 & "]"
            Debug.Print
            Debug.Print "DF total tributos          = [" & amem.RegDF.TotalTributos & "]"
            Debug.Print
        Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & amem.Registro & "]"
            Debug.Print
            Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE ACUMULAN="
            Debug.Print "---------------------------------------------------------"
            Debug.Print "DNFH Tipo Comprob.         = [" & FromTiposComprobanteToString(amem.RegDNFH.CodigoComprobante) & "]"
            Debug.Print "DNFH Nro. inicial          = [" & amem.RegDNFH.NumeroInicial & "]"
            Debug.Print "DNFH Nro. Final            = [" & amem.RegDNFH.NumeroFinal & "]"
            Debug.Print "DNFH total acumulado       = [" & amem.RegDNFH.Total & "]"
            Debug.Print
        Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & amem.Registro & "]"
            Debug.Print
            Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE NO ACUMULAN="
            Debug.Print "------------------------------------------------------------"
            Debug.Print "Tipo Comprobante = [" & FromTiposComprobanteToString(amem.RegDNFH_NoAcum.CodigoComprobante) & "]"
            Debug.Print "N� inicial       = [" & amem.RegDNFH_NoAcum.NumeroInicial & "]"
            Debug.Print "N� final         = [" & amem.RegDNFH_NoAcum.NumeroFinal & "]"
            Debug.Print
        Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & amem.Registro & "]"
            Debug.Print
            Debug.Print "INFORMACION GLOBAL:"
            Debug.Print "-------------------"
            Debug.Print "DF cant. cancelados = [" & amem.RegGlobal.DF_CantidadCancelados & "]"
            Debug.Print "DF cant. emitidos   = [" & amem.RegGlobal.DF_CantidadEmitidos & "]"
            Debug.Print
            Debug.Print "DF total ventas     = [" & amem.RegGlobal.DF_Total & "]"
            Debug.Print
            Debug.Print "DF total gravado    = [" & amem.RegGlobal.DF_TotalGravado & "]"
            Debug.Print "DF total IVA        = [" & amem.RegGlobal.DF_TotalIVA & "]"
            Debug.Print "DF total tributos   = [" & amem.RegGlobal.DF_TotalTributos & "]"
            Debug.Print "DF total no gravado = [" & amem.RegGlobal.DF_TotalNoGravado & "]"
            Debug.Print "DF total exento     = [" & amem.RegGlobal.DF_TotalExento & "]"
            Debug.Print
            Debug.Print "DF total DNFH       = [" & amem.RegGlobal.DNFH_Total & "]"
            Debug.Print
    End Select

    amem2 = HASARNG.ContinuarConsultaAcumulados

    While (amem2.Registro <> HasarArgentina.TiposDeRegistroInforme.RegistroFinal)
        Select Case amem2.Registro
            Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & amem2.Registro & "]"
                Debug.Print
                Debug.Print "DF (Documentos Fiscales):"
                Debug.Print "-------------------------"
                Debug.Print "DF cancelados              = [" & amem2.RegDF.CantidadCancelados & "]"
                Debug.Print "DF Tipo comprobante        = [" & FromTiposComprobanteToString(amem2.RegDF.CodigoComprobante) & "]"
                Debug.Print "DF Nro. inicial            = [" & amem2.RegDF.NumeroInicial & "]"
                Debug.Print "DF Nro. final              = [" & amem2.RegDF.NumeroFinal & "]"
                Debug.Print
                Debug.Print "DF total ventas            = [" & amem2.RegDF.Total & "]"
                Debug.Print
                Debug.Print "DF total gravado           = [" & amem2.RegDF.TotalGravado & "]"
                Debug.Print "DF total no gravado        = [" & amem2.RegDF.TotalNoGravado & "]"
                Debug.Print "DF total exento            = [" & amem2.RegDF.TotalExento & "]"
                Debug.Print
                Debug.Print "DF IVA 1                   = [" & amem2.RegDF.AlicuotaIVA_1 & "]"
                Debug.Print "DF monto IVA 1             = [" & amem2.RegDF.MontoIVA_1 & "]"
                Debug.Print "DF IVA 2                   = [" & amem2.RegDF.AlicuotaIVA_2 & "]"
                Debug.Print "DF monto IVA 2             = [" & amem2.RegDF.MontoIVA_2 & "]"
                Debug.Print "DF IVA 3                   = [" & amem2.RegDF.AlicuotaIVA_3 & "]"
                Debug.Print "DF monto IVA 3             = [" & amem2.RegDF.MontoIVA_3 & "]"
                Debug.Print "DF IVA 4                   = [" & amem2.RegDF.AlicuotaIVA_4 & "]"
                Debug.Print "DF monto IVA 4             = [" & amem2.RegDF.MontoIVA_4 & "]"
                Debug.Print "DF IVA 5                   = [" & amem2.RegDF.AlicuotaIVA_5 & "]"
                Debug.Print "DF monto IVA 5             = [" & amem2.RegDF.MontoIVA_5 & "]"
                Debug.Print "DF IVA 6                   = [" & amem2.RegDF.AlicuotaIVA_6 & "]"
                Debug.Print "DF monto IVA 6             = [" & amem2.RegDF.MontoIVA_6 & "]"
                Debug.Print "DF IVA 7                   = [" & amem2.RegDF.AlicuotaIVA_7 & "]"
                Debug.Print "DF monto IVA 7             = [" & amem2.RegDF.MontoIVA_7 & "]"
                Debug.Print "DF IVA 8                   = [" & amem2.RegDF.AlicuotaIVA_8 & "]"
                Debug.Print "DF monto IVA 8             = [" & amem2.RegDF.MontoIVA_8 & "]"
                Debug.Print "DF IVA 9                   = [" & amem2.RegDF.AlicuotaIVA_9 & "]"
                Debug.Print "DF monto IVA 9             = [" & amem2.RegDF.MontoIVA_9 & "]"
                Debug.Print "DF IVA 10                  = [" & amem2.RegDF.AlicuotaIVA_10 & "]"
                Debug.Print "DF monto IVA 10            = [" & amem2.RegDF.MontoIVA_10 & "]"
                Debug.Print
                Debug.Print "DF total IVA               = [" & amem2.RegDF.TotalIVA & "]"
                Debug.Print
                Debug.Print "DF tributo 1               = [" & amem2.RegDF.CodigoTributo1 & "]"
                Debug.Print "DF monto tributo 1         = [" & amem2.RegDF.ImporteTributo1 & "]"
                Debug.Print "DF tributo 2               = [" & amem2.RegDF.CodigoTributo2 & "]"
                Debug.Print "DF monto tributo 2         = [" & amem2.RegDF.ImporteTributo2 & "]"
                Debug.Print "DF tributo 3               = [" & amem2.RegDF.CodigoTributo3 & "]"
                Debug.Print "DF monto tributo 3         = [" & amem2.RegDF.ImporteTributo3 & "]"
                Debug.Print "DF tributo 4               = [" & amem2.RegDF.CodigoTributo4 & "]"
                Debug.Print "DF monto tributo 4         = [" & amem2.RegDF.ImporteTributo4 & "]"
                Debug.Print "DF tributo 5               = [" & amem2.RegDF.CodigoTributo5 & "]"
                Debug.Print "DF monto tributo 5         = [" & amem2.RegDF.ImporteTributo5 & "]"
                Debug.Print "DF tributo 6               = [" & amem2.RegDF.CodigoTributo6 & "]"
                Debug.Print "DF monto tributo 6         = [" & amem2.RegDF.ImporteTributo6 & "]"
                Debug.Print "DF tributo 7               = [" & amem2.RegDF.CodigoTributo7 & "]"
                Debug.Print "DF monto tributo 7         = [" & amem2.RegDF.ImporteTributo7 & "]"
                Debug.Print "DF tributo 8               = [" & amem2.RegDF.CodigoTributo8 & "]"
                Debug.Print "DF monto tributo 8         = [" & amem2.RegDF.ImporteTributo8 & "]"
                Debug.Print "DF tributo 9               = [" & amem2.RegDF.CodigoTributo9 & "]"
                Debug.Print "DF monto tributo 9         = [" & amem2.RegDF.ImporteTributo9 & "]"
                Debug.Print "DF tributo 10              = [" & amem2.RegDF.CodigoTributo10 & "]"
                Debug.Print "DF monto tributo 10        = [" & amem2.RegDF.ImporteTributo10 & "]"
                Debug.Print "DF tributo 11              = [" & amem2.RegDF.CodigoTributo11 & "]"
                Debug.Print "DF monto tributo 11        = [" & amem2.RegDF.ImporteTributo11 & "]"
                Debug.Print
                Debug.Print "DF total tributos          = [" & amem2.RegDF.TotalTributos & "]"
                Debug.Print
            Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & amem2.Registro & "]"
                Debug.Print
                Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE ACUMULAN="
                Debug.Print "---------------------------------------------------------"
                Debug.Print "DNFH Tipo Comprob.         = [" & FromTiposComprobanteToString(amem2.RegDNFH.CodigoComprobante) & "]"
                Debug.Print "DNFH Nro. inicial          = [" & amem2.RegDNFH.NumeroInicial & "]"
                Debug.Print "DNFH Nro. Final            = [" & amem2.RegDNFH.NumeroFinal & "]"
                Debug.Print "DNFH total acumulado       = [" & amem2.RegDNFH.Total & "]"
                Debug.Print
            Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & amem2.Registro & "]"
                Debug.Print
                Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE NO ACUMULAN="
                Debug.Print "------------------------------------------------------------"
                Debug.Print "Tipo Comprobante = [" & FromTiposComprobanteToString(amem2.RegDNFH_NoAcum.CodigoComprobante) & "]"
                Debug.Print "N� inicial       = [" & amem2.RegDNFH_NoAcum.NumeroInicial & "]"
                Debug.Print "N� final         = [" & amem2.RegDNFH_NoAcum.NumeroFinal & "]"
                Debug.Print
            Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & amem2.Registro & "]"
                Debug.Print
                Debug.Print "INFORMACION GLOBAL:"
                Debug.Print "-------------------"
                Debug.Print "DF cant. cancelados = [" & amem2.RegGlobal.DF_CantidadCancelados & "]"
                Debug.Print "DF cant. emitidos   = [" & amem2.RegGlobal.DF_CantidadEmitidos & "]"
                Debug.Print
                Debug.Print "DF total ventas     = [" & amem2.RegGlobal.DF_Total & "]"
                Debug.Print
                Debug.Print "DF total gravado    = [" & amem2.RegGlobal.DF_TotalGravado & "]"
                Debug.Print "DF total IVA        = [" & amem2.RegGlobal.DF_TotalIVA & "]"
                Debug.Print "DF total tributos   = [" & amem2.RegGlobal.DF_TotalTributos & "]"
                Debug.Print "DF total no gravado = [" & amem2.RegGlobal.DF_TotalNoGravado & "]"
                Debug.Print "DF total exento     = [" & amem2.RegGlobal.DF_TotalExento & "]"
                Debug.Print
                Debug.Print "DF total DNFH       = [" & amem2.RegGlobal.DNFH_Total & "]"
                Debug.Print
        End Select
        
        amem2 = HASARNG.ContinuarConsultaAcumulados
    Wend
    
    Debug.Print
    Debug.Print "NO HAY MAS INFORMACION DISPONIBLE !..."
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub
    
keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consultar configuraci�n de conectividad en red.
'//====================================================================================================================
Private Sub CommandConsCfgRed_Click()
Dim cfgred As RespuestaConsultarConfiguracionRed

    Debug.Print
    Debug.Print "CONSULTANDO CONFIGURACION DE RED..."
    Debug.Print

On Error GoTo keka
    cfgred = HASARNG.ConsultarConfiguracionRed
    Debug.Print
    Debug.Print "CONFIGURACION DE RED:"
    Debug.Print "Direcc. IP = [" & cfgred.DireccionIP & "]"
    Debug.Print "Gateway    = [" & cfgred.Gateway & "]"
    Debug.Print "M�scara    = [" & cfgred.Mascara & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description
    
End Sub

'//==================================================================================================================
'// Consulta de acumulados para determinado Cierre Diario 'Z' - Memoria fiscal de la impresora fiscal.
'//==================================================================================================================
Private Sub CommandConsCierreZ_Click()
Dim azeta As RespuestaConsultarAcumuladosCierreZeta
Dim azeta2 As RespuestaContinuarConsultaAcumulados

    Debug.Print
    Debug.Print "CONSULTANDO ACUMULADOS CIERRE 'Z'..."
    Debug.Print

On Error GoTo keka
    azeta = HASARNG.ConsultarAcumuladosCierreZeta(ReporteZNumero, 11)
    
    If (azeta.Registro = HasarArgentina.TiposDeRegistroInforme.RegistroFinal) Then
        Debug.Print
        Debug.Print "NO HAY INFORMACION DISPONIBLE !..."
        Debug.Print
        
        Exit Sub
    End If

    Select Case azeta.Registro
        Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & azeta.Registro & "]"
            Debug.Print
            Debug.Print "DF (Documentos Fiscales):"
            Debug.Print "-------------------------"
            Debug.Print "DF Tipo comprobante        = [" & FromTiposComprobanteToString(azeta.RegDF.CodigoComprobante) & "]"
            Debug.Print "DF Nro. inicial            = [" & azeta.RegDF.NumeroInicial & "]"
            Debug.Print "DF Nro. final              = [" & azeta.RegDF.NumeroFinal & "]"
            Debug.Print "DF cancelados              = [" & azeta.RegDF.CantidadCancelados & "]"
            Debug.Print
            Debug.Print "DF total ventas            = [" & azeta.RegDF.Total & "]"
            Debug.Print
            Debug.Print "DF total gravado           = [" & azeta.RegDF.TotalGravado & "]"
            Debug.Print "DF total no gravado        = [" & azeta.RegDF.TotalNoGravado & "]"
            Debug.Print "DF total exento            = [" & azeta.RegDF.TotalExento & "]"
            Debug.Print
            Debug.Print "DF IVA 1                   = [" & azeta.RegDF.AlicuotaIVA_1 & "]"
            Debug.Print "DF monto IVA 1             = [" & azeta.RegDF.MontoIVA_1 & "]"
            Debug.Print "DF IVA 2                   = [" & azeta.RegDF.AlicuotaIVA_2 & "]"
            Debug.Print "DF monto IVA 2             = [" & azeta.RegDF.MontoIVA_2 & "]"
            Debug.Print "DF IVA 3                   = [" & azeta.RegDF.AlicuotaIVA_3 & "]"
            Debug.Print "DF monto IVA 3             = [" & azeta.RegDF.MontoIVA_3 & "]"
            Debug.Print "DF IVA 4                   = [" & azeta.RegDF.AlicuotaIVA_4 & "]"
            Debug.Print "DF monto IVA 4             = [" & azeta.RegDF.MontoIVA_4 & "]"
            Debug.Print "DF IVA 5                   = [" & azeta.RegDF.AlicuotaIVA_5 & "]"
            Debug.Print "DF monto IVA 5             = [" & azeta.RegDF.MontoIVA_5 & "]"
            Debug.Print "DF IVA 6                   = [" & azeta.RegDF.AlicuotaIVA_6 & "]"
            Debug.Print "DF monto IVA 6             = [" & azeta.RegDF.MontoIVA_6 & "]"
            Debug.Print "DF IVA 7                   = [" & azeta.RegDF.AlicuotaIVA_7 & "]"
            Debug.Print "DF monto IVA 7             = [" & azeta.RegDF.MontoIVA_7 & "]"
            Debug.Print "DF IVA 8                   = [" & azeta.RegDF.AlicuotaIVA_8 & "]"
            Debug.Print "DF monto IVA 8             = [" & azeta.RegDF.MontoIVA_8 & "]"
            Debug.Print "DF IVA 9                   = [" & azeta.RegDF.AlicuotaIVA_9 & "]"
            Debug.Print "DF monto IVA 9             = [" & azeta.RegDF.MontoIVA_9 & "]"
            Debug.Print "DF IVA 10                  = [" & azeta.RegDF.AlicuotaIVA_10 & "]"
            Debug.Print "DF monto IVA 10            = [" & azeta.RegDF.MontoIVA_10 & "]"
            Debug.Print
            Debug.Print "DF total IVA               = [" & azeta.RegDF.TotalIVA & "]"
            Debug.Print
            Debug.Print "DF tributo 1               = [" & azeta.RegDF.CodigoTributo1 & "]"
            Debug.Print "DF monto tributo 1         = [" & azeta.RegDF.ImporteTributo1 & "]"
            Debug.Print "DF tributo 2               = [" & azeta.RegDF.CodigoTributo2 & "]"
            Debug.Print "DF monto tributo 2         = [" & azeta.RegDF.ImporteTributo2 & "]"
            Debug.Print "DF tributo 3               = [" & azeta.RegDF.CodigoTributo3 & "]"
            Debug.Print "DF monto tributo 3         = [" & azeta.RegDF.ImporteTributo3 & "]"
            Debug.Print "DF tributo 4               = [" & azeta.RegDF.CodigoTributo4 & "]"
            Debug.Print "DF monto tributo 4         = [" & azeta.RegDF.ImporteTributo4 & "]"
            Debug.Print "DF tributo 5               = [" & azeta.RegDF.CodigoTributo5 & "]"
            Debug.Print "DF monto tributo 5         = [" & azeta.RegDF.ImporteTributo5 & "]"
            Debug.Print "DF tributo 6               = [" & azeta.RegDF.CodigoTributo6 & "]"
            Debug.Print "DF monto tributo 6         = [" & azeta.RegDF.ImporteTributo6 & "]"
            Debug.Print "DF tributo 7               = [" & azeta.RegDF.CodigoTributo7 & "]"
            Debug.Print "DF monto tributo 7         = [" & azeta.RegDF.ImporteTributo7 & "]"
            Debug.Print "DF tributo 8               = [" & azeta.RegDF.CodigoTributo8 & "]"
            Debug.Print "DF monto tributo 8         = [" & azeta.RegDF.ImporteTributo8 & "]"
            Debug.Print "DF tributo 9               = [" & azeta.RegDF.CodigoTributo9 & "]"
            Debug.Print "DF monto tributo 9         = [" & azeta.RegDF.ImporteTributo9 & "]"
            Debug.Print "DF tributo 10              = [" & azeta.RegDF.CodigoTributo10 & "]"
            Debug.Print "DF monto tributo 10        = [" & azeta.RegDF.ImporteTributo10 & "]"
            Debug.Print "DF tributo 11              = [" & azeta.RegDF.CodigoTributo11 & "]"
            Debug.Print "DF monto tributo 11        = [" & azeta.RegDF.ImporteTributo11 & "]"
            Debug.Print
            Debug.Print "DF total tributos          = [" & azeta.RegDF.TotalTributos & "]"
            Debug.Print
        Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & azeta.Registro & "]"
            Debug.Print
            Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE ACUMULAN="
            Debug.Print "---------------------------------------------------------"
            Debug.Print "DNFH Tipo Comprob.         = [" & FromTiposComprobanteToString(azeta.RegDNFH.CodigoComprobante) & "]"
            Debug.Print "DNFH Nro. inicial          = [" & azeta.RegDNFH.NumeroInicial & "]"
            Debug.Print "DNFH Nro. Final            = [" & azeta.RegDNFH.NumeroFinal & "]"
            Debug.Print "DNFH total acumulado       = [" & azeta.RegDNFH.Total & "]"
            Debug.Print
        Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & azeta.Registro & "]"
            Debug.Print
            Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE NO ACUMULAN="
            Debug.Print "------------------------------------------------------------"
            Debug.Print "Tipo Comprobante = [" & FromTiposComprobanteToString(azeta.RegDNFH_NoAcum.CodigoComprobante) & "]"
            Debug.Print "N� inicial       = [" & azeta.RegDNFH_NoAcum.NumeroInicial & "]"
            Debug.Print "N� final         = [" & azeta.RegDNFH_NoAcum.NumeroFinal & "]"
            Debug.Print
        Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
            Debug.Print
            Debug.Print "TIPO REGISTRO:"
            Debug.Print "[" & azeta.Registro & "]"
            Debug.Print
            Debug.Print "INFORMACION GLOBAL:"
            Debug.Print "-------------------"
            Debug.Print "DF cant. cancelados = [" & azeta.RegGlobal.DF_CantidadCancelados & "]"
            Debug.Print "DF cant. emitidos   = [" & azeta.RegGlobal.DF_CantidadEmitidos & "]"
            Debug.Print
            Debug.Print "DF total ventas     = [" & azeta.RegGlobal.DF_Total & "]"
            Debug.Print
            Debug.Print "DF total gravado    = [" & azeta.RegGlobal.DF_TotalGravado & "]"
            Debug.Print "DF total IVA        = [" & azeta.RegGlobal.DF_TotalIVA & "]"
            Debug.Print "DF total tributos   = [" & azeta.RegGlobal.DF_TotalTributos & "]"
            Debug.Print "DF total no gravado = [" & azeta.RegGlobal.DF_TotalNoGravado & "]"
            Debug.Print "DF total exento     = [" & azeta.RegGlobal.DF_TotalExento & "]"
            Debug.Print
            Debug.Print "DF total DNFH       = [" & azeta.RegGlobal.DNFH_Total & "]"
            Debug.Print
    End Select

    azeta2 = HASARNG.ContinuarConsultaAcumulados

    While (azeta2.Registro <> HasarArgentina.TiposDeRegistroInforme.RegistroFinal)
        Select Case azeta2.Registro
            Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & azeta2.Registro & "]"
                Debug.Print
                Debug.Print "DF (Documentos Fiscales):"
                Debug.Print "-------------------------"
                Debug.Print "DF Tipo comprobante        = [" & FromTiposComprobanteToString(azeta2.RegDF.CodigoComprobante) & "]"
                Debug.Print "DF Nro. inicial            = [" & azeta2.RegDF.NumeroInicial & "]"
                Debug.Print "DF Nro. final              = [" & azeta2.RegDF.NumeroFinal & "]"
                Debug.Print "DF cancelados              = [" & azeta2.RegDF.CantidadCancelados & "]"
                Debug.Print
                Debug.Print "DF total ventas            = [" & azeta2.RegDF.Total & "]"
                Debug.Print
                Debug.Print "DF total gravado           = [" & azeta2.RegDF.TotalGravado & "]"
                Debug.Print "DF total no gravado        = [" & azeta2.RegDF.TotalNoGravado & "]"
                Debug.Print "DF total exento            = [" & azeta2.RegDF.TotalExento & "]"
                Debug.Print
                Debug.Print "DF IVA 1                   = [" & azeta2.RegDF.AlicuotaIVA_1 & "]"
                Debug.Print "DF monto IVA 1             = [" & azeta2.RegDF.MontoIVA_1 & "]"
                Debug.Print "DF IVA 2                   = [" & azeta2.RegDF.AlicuotaIVA_2 & "]"
                Debug.Print "DF monto IVA 2             = [" & azeta2.RegDF.MontoIVA_2 & "]"
                Debug.Print "DF IVA 3                   = [" & azeta2.RegDF.AlicuotaIVA_3 & "]"
                Debug.Print "DF monto IVA 3             = [" & azeta2.RegDF.MontoIVA_3 & "]"
                Debug.Print "DF IVA 4                   = [" & azeta2.RegDF.AlicuotaIVA_4 & "]"
                Debug.Print "DF monto IVA 4             = [" & azeta2.RegDF.MontoIVA_4 & "]"
                Debug.Print "DF IVA 5                   = [" & azeta2.RegDF.AlicuotaIVA_5 & "]"
                Debug.Print "DF monto IVA 5             = [" & azeta2.RegDF.MontoIVA_5 & "]"
                Debug.Print "DF IVA 6                   = [" & azeta2.RegDF.AlicuotaIVA_6 & "]"
                Debug.Print "DF monto IVA 6             = [" & azeta2.RegDF.MontoIVA_6 & "]"
                Debug.Print "DF IVA 7                   = [" & azeta2.RegDF.AlicuotaIVA_7 & "]"
                Debug.Print "DF monto IVA 7             = [" & azeta2.RegDF.MontoIVA_7 & "]"
                Debug.Print "DF IVA 8                   = [" & azeta2.RegDF.AlicuotaIVA_8 & "]"
                Debug.Print "DF monto IVA 8             = [" & azeta2.RegDF.MontoIVA_8 & "]"
                Debug.Print "DF IVA 9                   = [" & azeta2.RegDF.AlicuotaIVA_9 & "]"
                Debug.Print "DF monto IVA 9             = [" & azeta2.RegDF.MontoIVA_9 & "]"
                Debug.Print "DF IVA 10                  = [" & azeta2.RegDF.AlicuotaIVA_10 & "]"
                Debug.Print "DF monto IVA 10            = [" & azeta2.RegDF.MontoIVA_10 & "]"
                Debug.Print
                Debug.Print "DF total IVA               = [" & azeta2.RegDF.TotalIVA & "]"
                Debug.Print
                Debug.Print "DF tributo 1               = [" & azeta2.RegDF.CodigoTributo1 & "]"
                Debug.Print "DF monto tributo 1         = [" & azeta2.RegDF.ImporteTributo1 & "]"
                Debug.Print "DF tributo 2               = [" & azeta2.RegDF.CodigoTributo2 & "]"
                Debug.Print "DF monto tributo 2         = [" & azeta2.RegDF.ImporteTributo2 & "]"
                Debug.Print "DF tributo 3               = [" & azeta2.RegDF.CodigoTributo3 & "]"
                Debug.Print "DF monto tributo 3         = [" & azeta2.RegDF.ImporteTributo3 & "]"
                Debug.Print "DF tributo 4               = [" & azeta2.RegDF.CodigoTributo4 & "]"
                Debug.Print "DF monto tributo 4         = [" & azeta2.RegDF.ImporteTributo4 & "]"
                Debug.Print "DF tributo 5               = [" & azeta2.RegDF.CodigoTributo5 & "]"
                Debug.Print "DF monto tributo 5         = [" & azeta2.RegDF.ImporteTributo5 & "]"
                Debug.Print "DF tributo 6               = [" & azeta2.RegDF.CodigoTributo6 & "]"
                Debug.Print "DF monto tributo 6         = [" & azeta2.RegDF.ImporteTributo6 & "]"
                Debug.Print "DF tributo 7               = [" & azeta2.RegDF.CodigoTributo7 & "]"
                Debug.Print "DF monto tributo 7         = [" & azeta2.RegDF.ImporteTributo7 & "]"
                Debug.Print "DF tributo 8               = [" & azeta2.RegDF.CodigoTributo8 & "]"
                Debug.Print "DF monto tributo 8         = [" & azeta2.RegDF.ImporteTributo8 & "]"
                Debug.Print "DF tributo 9               = [" & azeta2.RegDF.CodigoTributo9 & "]"
                Debug.Print "DF monto tributo 9         = [" & azeta2.RegDF.ImporteTributo9 & "]"
                Debug.Print "DF tributo 10              = [" & azeta2.RegDF.CodigoTributo10 & "]"
                Debug.Print "DF monto tributo 10        = [" & azeta2.RegDF.ImporteTributo10 & "]"
                Debug.Print "DF tributo 11              = [" & azeta2.RegDF.CodigoTributo11 & "]"
                Debug.Print "DF monto tributo 11        = [" & azeta2.RegDF.ImporteTributo11 & "]"
                Debug.Print
                Debug.Print "DF total tributos          = [" & azeta2.RegDF.TotalTributos & "]"
                Debug.Print
            Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & azeta2.Registro & "]"
                Debug.Print
                Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE ACUMULAN="
                Debug.Print "---------------------------------------------------------"
                Debug.Print "DNFH Tipo Comprob.         = [" & FromTiposComprobanteToString(azeta2.RegDNFH.CodigoComprobante) & "]"
                Debug.Print "DNFH Nro. inicial          = [" & azeta2.RegDNFH.NumeroInicial & "]"
                Debug.Print "DNFH Nro. Final            = [" & azeta2.RegDNFH.NumeroFinal & "]"
                Debug.Print "DNFH total acumulado       = [" & azeta2.RegDNFH.Total & "]"
                Debug.Print
            Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & azeta2.Registro & "]"
                Debug.Print
                Debug.Print "DNFH (Documentos No Fiscales Homologados): =QUE NO ACUMULAN="
                Debug.Print "------------------------------------------------------------"
                Debug.Print "Tipo Comprobante = [" & FromTiposComprobanteToString(azeta2.RegDNFH_NoAcum.CodigoComprobante) & "]"
                Debug.Print "N� inicial       = [" & azeta2.RegDNFH_NoAcum.NumeroInicial & "]"
                Debug.Print "N� final         = [" & azeta2.RegDNFH_NoAcum.NumeroFinal & "]"
                Debug.Print
            Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
                Debug.Print
                Debug.Print "TIPO REGISTRO:"
                Debug.Print "[" & azeta2.Registro & "]"
                Debug.Print
                Debug.Print "INFORMACION GLOBAL:"
                Debug.Print "-------------------"
                Debug.Print "DF cant. cancelados = [" & azeta2.RegGlobal.DF_CantidadCancelados & "]"
                Debug.Print "DF cant. emitidos   = [" & azeta2.RegGlobal.DF_CantidadEmitidos & "]"
                Debug.Print
                Debug.Print "DF total ventas     = [" & azeta2.RegGlobal.DF_Total & "]"
                Debug.Print
                Debug.Print "DF total gravado    = [" & azeta2.RegGlobal.DF_TotalGravado & "]"
                Debug.Print "DF total IVA        = [" & azeta2.RegGlobal.DF_TotalIVA & "]"
                Debug.Print "DF total tributos   = [" & azeta2.RegGlobal.DF_TotalTributos & "]"
                Debug.Print "DF total no gravado = [" & azeta2.RegGlobal.DF_TotalNoGravado & "]"
                Debug.Print "DF total exento     = [" & azeta2.RegGlobal.DF_TotalExento & "]"
                Debug.Print
                Debug.Print "DF total DNFH       = [" & azeta2.RegGlobal.DNFH_Total & "]"
                Debug.Print
        End Select
        
        azeta2 = HASARNG.ContinuarConsultaAcumulados
    Wend
    
    Debug.Print
    Debug.Print "NO HAY MAS INFORMACION DISPONIBLE !..."
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    Exit Sub
    
keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consultar configuraci�n del servidor de correo electr�nico.
'//====================================================================================================================
Private Sub CommandConsCorreo_Click()
Dim respcorreo As RespuestaConsultarConfiguracionServidorCorreo

    Debug.Print
    Debug.Print "CONSULTANDO CONFIGURACION DEL SERVIDOR DE CORREO ELECTRONICO..."
    Debug.Print

On Error GoTo keka
    respcorreo = HASARNG.ConsultarConfiguracionServidorCorreo
    Debug.Print
    Debug.Print "IP Servidor SMTP     = [" & respcorreo.DireccionIP & "]"
    Debug.Print "Port Servidor SMTP   = [" & respcorreo.Puerto & "]"
    Debug.Print "Responder a          = [" & respcorreo.DireccionRemitente & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consultar documentos asociados.
'//====================================================================================================================
Private Sub CommandConsDocAsoc_Click()
Dim respdoc As RespuestaConsultarDocumentoAsociado
    Debug.Print
    Debug.Print "CONSULTANDO DOCUMENTOS ASOCIADOS..."
    Debug.Print

On Error GoTo keka
    respdoc = HASARNG.ConsultarDocumentoAsociado(1)
    Debug.Print
    Debug.Print "=LINEA 1="
    Debug.Print "Tipo comprobante = [" & FromTiposComprobanteToString(respdoc.CodigoComprobante) & "]"
    Debug.Print "Nro. comprobante = [" & respdoc.NumeroPos & "-" & respdoc.NumeroComprobante & "]"
    Debug.Print
    
    respdoc = HASARNG.ConsultarDocumentoAsociado(2)
    Debug.Print
    Debug.Print "=LINEA 2="
    Debug.Print "Tipo comprobante = [" & FromTiposComprobanteToString(respdoc.CodigoComprobante) & "]"
    Debug.Print "Nro. comprobante = [" & respdoc.NumeroPos & "-" & respdoc.NumeroComprobante & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consultar informaci�n �ltimo error producido.
'//====================================================================================================================
Private Sub CommandConsError_Click()
Dim resperr As RespuestaConsultarUltimoError

    Debug.Print
    Debug.Print "CONSULTANDO ULTIMO ERROR..."
    Debug.Print

On Error GoTo keka
    resperr = HASARNG.ConsultarUltimoError
    Debug.Print
    Debug.Print "ID error = [" & resperr.UltimoError & "]"
    Debug.Print "Campo    = [" & resperr.NumeroParametro & " :: " & resperr.NombreParametro & "]"
    Debug.Print "Mensaje  = [" & resperr.Descripcion & "]"
    Debug.Print "Contexto = [" & resperr.Contexto & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consultar estado general de la impresora fiscal.
'//====================================================================================================================
Private Sub CommandConsEstado_Click()
Dim respest As RespuestaConsultarEstado

    Debug.Print
    Debug.Print "CONSULTANDO ESTADO GENERAL IMPRESORA FISCAL..."
    Debug.Print

On Error GoTo keka
    respest = HASARNG.ConsultarEstado(NoDocumento)
    Debug.Print
    Debug.Print "Tipo Comprobante      = [" & FromTiposComprobanteToString(respest.CodigoComprobante) & "]"
    Debug.Print "Ultimo Nro.           = [" & respest.NumeroUltimoComprobante & "]"
    Debug.Print "Cancelados            = [" & respest.CantidadCancelados & "]"
    Debug.Print "Emitidos              = [" & respest.CantidadEmitidos & "]"
    Debug.Print
    Debug.Print "Almacenado en memoria de trabajo..."
    Debug.Print "C�digo de barras      = [" & respest.EstadoAuxiliar.CodigoBarrasAlmacenado & "]"
    Debug.Print "Datos del cliente     = [" & respest.EstadoAuxiliar.DatosClienteAlmacenados & "]"
    Debug.Print
    Debug.Print "Memoria de auditoria..."
    Debug.Print "Casi llena            = [" & respest.EstadoAuxiliar.MemoriaAuditoriaCasiLlena & "]"
    Debug.Print "Llena                 = [" & respest.EstadoAuxiliar.MemoriaAuditoriaLlena & "]"
    Debug.Print
    Debug.Print "Otra info..."
    Debug.Print "Modo entrenamiento    = [" & respest.EstadoAuxiliar.ModoEntrenamiento & "]"
    Debug.Print "Ult. comprob. cancel. = [" & respest.EstadoAuxiliar.UltimoComprobanteFueCancelado & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consultar informaci�n acerca del modelo y versi�n de la impresora fiscal.
'//====================================================================================================================
Private Sub CommandConsModvers_Click()
Dim resp As RespuestaConsultarVersion

    Debug.Print
    Debug.Print "CONSULTANDO MODELO Y VERSION IMPRESORA FISCAL..."
    Debug.Print

On Error GoTo keka
    resp = HASARNG.ConsultarVersion
    Debug.Print
    Debug.Print "Impresora fiscal  = [" & resp.NombreProducto & "]"
    Debug.Print "Marca             = [" & resp.Marca & "]"
    Debug.Print "Modelo y versi�n  = [" & resp.Version & "]"
    Debug.Print "Fecha firmware    = [" & resp.FechaFirmware & "]"
    Debug.Print "Versi�n motor     = [" & resp.VersionMotor & "]"
    Debug.Print "Versi�n protocolo = [" & resp.VersionProtocolo & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consultar capacidad de cierres 'Z'.
'//====================================================================================================================
Private Sub CommandConsZetas_Click()
Dim respz As RespuestaConsultarCapacidadZetas

    Debug.Print
    Debug.Print "CONSULTANDO CAPACIDAD CIERRES 'Z'..."
    Debug.Print

On Error GoTo keka
    respz = HASARNG.ConsultarCapacidadZetas
    Debug.Print
    Debug.Print "Cierres 'Z' Remantentes = [" & respz.CantidadDeZetasRemanentes & "]"
    Debug.Print "Cierres 'Z' realizados  = [" & respz.UltimaZeta & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consultar l�neas de usuario, por zona.
'//====================================================================================================================
Private Sub CommandConsZona_Click()
Dim resp As RespuestaConsultarZona

    Debug.Print
    Debug.Print "CONSULTANDO LINEAS DE USUARIO POR ZONA..."
    Debug.Print

On Error GoTo keka
    resp = HASARNG.ConsultarZona(1, EstacionTicket, ZonaFantasia)
    Debug.Print
    Debug.Print "=LINEA 1 , Fantas�a="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto               = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(2, EstacionTicket, ZonaFantasia)
    Debug.Print
    Debug.Print "=LINEA 2 , Fantas�a="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(1, EstacionTicket, Zona1Encabezado)
    Debug.Print
    Debug.Print "=LINEA 1 , Encabezado 1="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(2, EstacionTicket, Zona1Encabezado)
    Debug.Print
    Debug.Print "=LINEA 2 , Encabezado 1="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(3, EstacionTicket, Zona1Encabezado)
    Debug.Print
    Debug.Print "=LINEA 3 , Encabezado 1="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(1, EstacionTicket, Zona2Encabezado)
    Debug.Print
    Debug.Print "=LINEA 1 , Encabezado 2="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(2, EstacionTicket, Zona2Encabezado)
    Debug.Print
    Debug.Print "=LINEA 2 , Encabezado 2="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(3, EstacionTicket, Zona2Encabezado)
    Debug.Print
    Debug.Print "=LINEA 3 , Encabezado 2="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(1, EstacionTicket, Zona1Cola)
    Debug.Print
    Debug.Print "=LINEA 1 , Cola 1="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(2, EstacionTicket, Zona1Cola)
    Debug.Print
    Debug.Print "=LINEA 2 , Cola 1="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(3, EstacionTicket, Zona1Cola)
    Debug.Print
    Debug.Print "=LINEA 3 , Cola 1="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(4, EstacionTicket, Zona1Cola)
    Debug.Print
    Debug.Print "=LINEA 4 , Cola 1="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(1, EstacionTicket, Zona2Cola)
    Debug.Print
    Debug.Print "=LINEA 1 , Cola 2="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(2, EstacionTicket, Zona2Cola)
    Debug.Print
    Debug.Print "=LINEA 2 , Cola 2="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(3, EstacionTicket, Zona2Cola)
    Debug.Print
    Debug.Print "=LINEA 3 , Cola 2="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(4, EstacionTicket, Zona2Cola)
    Debug.Print
    Debug.Print "=LINEA 4 , Cola 2="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(5, EstacionTicket, Zona2Cola)
    Debug.Print
    Debug.Print "=LINEA 5 , Cola 2="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(6, EstacionTicket, Zona2Cola)
    Debug.Print
    Debug.Print "=LINEA 6 , Cola 2="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(1, EstacionTicket, ZonaDomicilioEmisor)
    Debug.Print
    Debug.Print "=LINEA 1 , Domicilio emisor="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(2, EstacionTicket, ZonaDomicilioEmisor)
    Debug.Print
    Debug.Print "=LINEA 2 , Domicilio emisor="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(3, EstacionTicket, ZonaDomicilioEmisor)
    Debug.Print
    Debug.Print "=LINEA 3 , Domicilio emisor="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    Debug.Print
    
    resp = HASARNG.ConsultarZona(4, EstacionTicket, ZonaDomicilioEmisor)
    Debug.Print
    Debug.Print "=LINEA 4 , Domicilio emisor="
    Debug.Print "Estilo: Borrado     = [" & resp.Atributos.BorradoTexto & "]"
    Debug.Print "Estilo: Centrado    = [" & resp.Atributos.Centrado & "]"
    Debug.Print "Estilo: Doble ancho = [" & resp.Atributos.DobleAncho & "]"
    Debug.Print "Estilo: Negrita     = [" & resp.Atributos.Negrita & "]"
    Debug.Print "Texto  = [" & resp.Descripcion & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print

    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Se pide la reimpresi�n de un comprobante cualquera.
'//====================================================================================================================
Private Sub CommandCopiarDoc_Click()

    Debug.Print
    Debug.Print "SOLICITANDO REIMPRESION DE COMPROBANTE..."
    Debug.Print
    
On Error GoTo keka
    Debug.Print "Copia de Tique Factura 'A' N� 13"
    HASARNG.CopiarComprobante TiqueFacturaA, 13
    
    Debug.Print
    Debug.Print "COMPROBANTE REIMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consultar datos de inicializaci�n de la impresora fiscal.
'//====================================================================================================================
Private Sub CommandDatosIni_Click()
Dim datosini As RespuestaConsultarDatosInicializacion

    Debug.Print
    Debug.Print "CONSULTANDO DATOS DE INCIALIZACION..."
    Debug.Print

On Error GoTo keka
    datosini = HASARNG.ConsultarDatosInicializacion
    Debug.Print
    Debug.Print "Raz�n Social      = [" & datosini.RazonSocial & "]"
    Debug.Print "CUIT              = [" & datosini.Cuit & "]"
    Debug.Print "Responsab. IVA    = [" & FromTiposDeResponsabilidadesImpresorToString(datosini.ResponsabilidadIVA) & "]"
    Debug.Print "Ingr. Brutos      = [" & datosini.IngBrutos & "]"
    Debug.Print "Fecha inicio act. = [" & datosini.FechaInicioActividades & "]"
    Debug.Print "Nro. POS          = [" & datosini.NumeroPos & "]"
    Debug.Print "Reg. impr. fiscal = [" & datosini.Registro & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description
    
End Sub

'//====================================================================================================================
'// Comando para el env�o de comprobantes, por correo electr�nico.
'//====================================================================================================================
Private Sub CommandDocxMail_Click()

    Debug.Print
    Debug.Print "ENVIANDO COMPROBANTE POR MAIL:"
    Debug.Print
    
On Error GoTo keka
    Debug.Print "Enviando: Tique Factura 'A' N� 13 :: rcardenes@hasar.com"
    Debug.Print
    HASARNG.EnviarDocumentoCorreo TiqueFacturaA, 13, "rcardenes@hasar.com"
    
    Debug.Print
    Debug.Print "COMPROBANTE ENVIADO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Comprobante Donaci�n.
'//====================================================================================================================
Private Sub CommandDONA_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - COMPROBANTE DONACION:"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", ResponsableInscripto, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    HASARNG.CargarDocumentoAsociado 1, TiqueFacturaA, 2019, 1458
    HASARNG.CargarBeneficiario "Raz�n Social Beneficiario...", "00000000000", TipoCUIL, "Domicilio Beneficiario..."
    respabrir = HASARNG.AbrirDocumento(ComprobanteDonacion)
    
    Debug.Print
    Debug.Print " - (abrir) Comprob. Donaci�n Nro.   = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    HASARNG.ImprimirItem "Monto parcial vuelto...", 1, 10, Exento, 0, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, , "DONA", Donacion
    resppago = HASARNG.ImprimirPago("Efectivo...", 10, Pagar, DisplayNo, , Efectivo, , "")
    
    Debug.Print
    Debug.Print " - Pagando... Cambio                 = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar)Comprobante Donaci�n Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                           = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "COMPROBANTE DONACION IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n del Detalle de ventas (cierre 'X').
'//====================================================================================================================
Private Sub CommandEquis_Click()
Dim cierre As RespuestaCerrarJornadaFiscal
Dim equis As CerrarJornadaFiscalX
Dim rep As TipoReporte

    Debug.Print
    Debug.Print "DETALLE DE VENTAS:"
    Debug.Print

On Error GoTo keka
    cierre = HASARNG.CerrarJornadaFiscal(ReporteX)
    equis = cierre.X
    rep = cierre.Reporte
    
    Debug.Print
    Debug.Print "Reporte...        = [" & rep & "]"
    Debug.Print
    Debug.Print "Detalle Nro.      = [" & equis.Numero & "]"
    Debug.Print
    Debug.Print "Fecha inicial     = [" & equis.FechaInicio & "]"
    Debug.Print "Hora inicial      = [" & equis.HoraInicio & "]"
    Debug.Print "Fecha final       = [" & equis.FechaCierre & "]"
    Debug.Print "Hora final        = [" & equis.HoraCierre & "]"
    Debug.Print
    Debug.Print "DF Emitidos       = [" & equis.DF_CantidadEmitidos & "]"
    Debug.Print "DF total ventas   = [" & equis.DF_Total & "]"
    Debug.Print "DF total IVA      = [" & equis.DF_TotalIVA & "]"
    Debug.Print "DF total tributos = [" & equis.DF_TotalTributos & "]"
    Debug.Print
    Debug.Print "DNFH emitidos     = [" & equis.DNFH_CantidadEmitidos & "]"
    Debug.Print
    Debug.Print "NC emitidas       = [" & equis.NC_CantidadEmitidos & "]"
    Debug.Print "NC total cr�dito  = [" & equis.NC_Total & "]"
    Debug.Print "NC total IVA      = [" & equis.NC_TotalIVA & "]"
    Debug.Print "NC total tributos = [" & equis.NC_TotalTributos & "]"
    
    Debug.Print
    Debug.Print "DETALLE DE VENTAS IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Documento Gen�rico.
'//====================================================================================================================
Private Sub CommandFechaHora_Click()
Dim respfyh As RespuestaConsultarFechaHora

    Debug.Print
    Debug.Print "CONSULTA DE FECHA Y HORA:"
    Debug.Print

On Error GoTo keka
    respfyh = HASARNG.ConsultarFechaHora
    Debug.Print
    Debug.Print "Fecha = [" & respfyh.Fecha & "]"
    Debug.Print "Hora  = [" & respfyh.Hora & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Documento Gen�rico.
'//====================================================================================================================
Private Sub CommandGenerico_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - DOCUMENTO GENERICO:"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", ResponsableInscripto, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    HASARNG.CargarDocumentoAsociado 1, FacturaA, 2019, 1458

    respabrir = HASARNG.AbrirDocumento(Generico)
    Debug.Print
    Debug.Print " - (abrir) Documento Gen�rico Nro. = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.DobleAncho = True
    HASARNG.ImprimirTextoGenerico estilo, "Texto doble ancho.", DisplayNo
    
    estilo.DobleAncho = False
    estilo.Negrita = True
    HASARNG.ImprimirTextoGenerico estilo, "Texto en negrita.", DisplayNo
    
    estilo.Negrita = False
    estilo.Centrado = True
    HASARNG.ImprimirTextoGenerico estilo, "Texto centrado.", DisplayNo
    
    estilo.Centrado = False
    estilo.DobleAncho = True
    estilo.Negrita = True
    HASARNG.ImprimirTextoGenerico estilo, "Texto doble ancho y en negrita.", DisplayNo
    
    estilo.Centrado = True
    estilo.DobleAncho = True
    estilo.Negrita = False
    HASARNG.ImprimirTextoGenerico estilo, "Texto doble ancho y centrado.", DisplayNo
    
    estilo.Centrado = True
    estilo.DobleAncho = False
    estilo.Negrita = True
    HASARNG.ImprimirTextoGenerico estilo, "Texto en negrita y centrado.", DisplayNo
    
    estilo.Centrado = True
    estilo.DobleAncho = True
    estilo.Negrita = True
    HASARNG.ImprimirTextoGenerico estilo, "Texto dob anch, negr y cent.", DisplayNo
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar)Documento Gen�rico Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                         = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "DOCUMENTO GENERICO IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Presupuesto 'X' clase 'A'.
'// Se requiere un Cierre 'Z' previo.
'//====================================================================================================================
Private Sub CommandModifCateg_Click()

    Debug.Print
    Debug.Print "MODIFICANDO CATEGORIA IVA..."
    Debug.Print
    Debug.Print "Nueva responsabilidad IVA = Responsable Monotributo"
    Debug.Print

On Error GoTo keka
    HASARNG.CambiarCategoriaIVA IFMonotributo
    Debug.Print
    Debug.Print "CATEGORIA IVA MODIFICADA !..."
    Debug.Print
    
    Exit Sub
    
keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Modificar el c�digo de inscripci�n en Ingresos Brutos.
'// Se requiere un Cierre 'Z' previo.
'//====================================================================================================================
Private Sub CommandModifIIBB_Click()

    Debug.Print
    Debug.Print "MODIFICANDO INSCRIPCION INGRESOS BRUTOS..."
    Debug.Print
    Debug.Print "Nuevo c�digo Ingresos Brutos = IIBB-54321099876543210"
    Debug.Print
    

On Error GoTo keka
    HASARNG.CambiarInscripIIBB "IIBB-54321099876543210"
    Debug.Print
    Debug.Print "INSCRIPCION INGRESOS BRUTOS MODIFICADA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Modificar la fecha de inicio de actividades.
'// Se requiere un Cierre 'Z' previo.
'//====================================================================================================================
Private Sub CommandModifIniActiv_Click()

    Debug.Print
    Debug.Print "MODIFICANDO FECHA DE INICIO DE ACTIVIDADES..."
    Debug.Print
    Debug.Print "Nueva fecha inicio actividades = 23/06/2015"
    Debug.Print

On Error GoTo keka
    HASARNG.CambiarFechaInicioActividades "23/06/2015"
    Debug.Print
    Debug.Print "FECHA DE INICIO DE ACTIVIDADES MODIFICADA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Descarga de CTD.
'//====================================================================================================================
Private Sub CommandObtRepAudit_Click()
Dim resp As RespuestaObtenerPrimerBloqueAuditoria
Dim resp2 As RespuestaObtenerSiguienteBloqueAuditoria

    Debug.Print
    Debug.Print "DESCARGA DE CTD (Cinta Testigo Digital):"
    Debug.Print
    
On Error GoTo keka
    Debug.Print "Per�odo: 01/02/2015 al 27/02/2015 , Comprimir, XML �nico"
    Debug.Print
    resp = HASARNG.ObtenerPrimerBloqueAuditoria(150201, 150227, ReporteZFecha, Comprime, XMLUnico)
    Debug.Print
    
    If (resp.Registro = BloqueFinal) Then
        Debug.Print "NO HAY INFORMACION DISPONIBLE !..."
        Debug.Print
        
        Exit Sub
    Else
        Debug.Print "Primer bloque de informaci�n    = [" & resp.Informacion & "]"
        Debug.Print
    End If
    
    While (True)
        resp2 = HASARNG.ObtenerSiguienteBloqueAuditoria
        
        If (resp2.Registro = BloqueInformacion) Then
            Debug.Print "Siguiente bloque de informaci�n = [" & resp2.Informacion & "]"
            Debug.Print
        Else
            Debug.Print
            Debug.Print "NO HAY MAS INFORMACION DISPONIBLE !..."
            Debug.Print
            Debug.Print "INFORMACION CTD DESCARGADA !..."
            Debug.Print
            
            Exit Sub
        End If
    Wend
        
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Descarga de reportes electr�nicos semanales.
'//====================================================================================================================
Private Sub CommandObtRepDoc_Click()
Dim resp As RespuestaObtenerPrimerBloqueDocumento
Dim resp2 As RespuestaObtenerSiguienteBloqueDocumento

    Debug.Print
    Debug.Print "DESCARGA INFORMACION COMPROBANTES:"
    Debug.Print

On Error GoTo keka
    resp = HASARNG.ObtenerPrimerBloqueDocumento(1, 4, TiqueFacturaA, Comprime, XMLUnico)
    
    If (resp.Registro = BloqueFinal) Then
        Debug.Print "NO HAY INFORMACION DISPONIBLE !...!"
        Debug.Print
        
        Exit Sub
    Else
        Debug.Print "Primer bloque de informaci�n    = [" & resp.Informacion & "]"
        Debug.Print
    End If
            
    While (True)
        resp2 = HASARNG.ObtenerSiguienteBloqueDocumento
        
        If (resp2.Registro = BloqueInformacion) Then
            Debug.Print "Siguiente bloque de informaci�n = [" & resp2.Informacion & "]"
            Debug.Print
        Else
            Debug.Print
            Debug.Print "NO HAY MAS INFORMACION DISPONIBLE !..."
            Debug.Print
            Debug.Print "INFORMACION COMPROBANTES DESCARGADA !..."
            Debug.Print
            
            Exit Sub
        End If
    Wend
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Descarga de reportes electr�nicos semanales.
'//====================================================================================================================
Private Sub CommandObtRepElectr_Click()
Dim resp As RespuestaObtenerPrimerBloqueReporteElectronico
Dim resp2 As RespuestaObtenerSiguienteBloqueReporteElectronico

    Debug.Print
    Debug.Print "DESCARGA DE REPORTES SEMANALES:"
    Debug.Print

On Error GoTo keka
    Debug.Print "Per�odo: 01/02/2015 al 27/02/2015"
    resp = HASARNG.ObtenerPrimerBloqueReporteElectronico("01/02/2015", "27/02/2015", ReporteAFIPCompleto)
    
    If (resp.Registro = BloqueFinal) Then
        Debug.Print
        Debug.Print "NO HAY INFORMACION DISPONIBLE !..."
        Debug.Print
        
        Exit Sub
    Else
        Debug.Print "Primer bloque de informaci�n    = [" & resp.Informacion & "]"
        Debug.Print
    End If

    
    While (True)
        resp2 = HASARNG.ObtenerSiguienteBloqueReporteElectronico
        
        If (resp2.Registro = BloqueInformacion) Then
            Debug.Print "Siguiente bloque de informaci�n = [" & resp2.Informacion & "]"
            Debug.Print
        Else
            Debug.Print
            Debug.Print "NO HAY M�S INFORMACION DISPONIBLE !..."
            Debug.Print
            Debug.Print "REPORTES SEMANALES DECARGADOS !..."
            Debug.Print
            
            Exit Sub
        End If
    Wend
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Descarga del LOG interno de la impresora fiscal.
'//====================================================================================================================
Private Sub CommandObtRepLog_Click()
Dim resp As RespuestaObtenerPrimerBloqueLog
Dim resp2 As RespuestaObtenerSiguienteBloqueLog

    Debug.Print
    Debug.Print "DESCARGA LOG INTERNO:"
    Debug.Print

On Error GoTo keka
    resp = HASARNG.ObtenerPrimerBloqueLog
    
    If (resp.Registro = BloqueFinal) Then
        Debug.Print
        Debug.Print "NO HAY INFORMACION DISPONIBLE !..."
        Debug.Print
        Exit Sub
    Else
        Debug.Print "Primer bloque de informaci�n    = [" & resp.Informacion & "]"
        Debug.Print
    End If
        
    While (True)
        resp2 = HASARNG.ObtenerSiguienteBloqueLog
        
        If (resp2.Registro = BloqueInformacion) Then
            Debug.Print "Siguiente bloque de informaci�n = [" & resp2.Informacion & "]"
            Debug.Print
        Else
            Debug.Print
            Debug.Print "NO HAY MAS INFORMACION DISPONIBLE !..."
            Debug.Print
            Debug.Print "LOG INTERNO DESCARGADO !..."
            Debug.Print
            
            Exit Sub
        End If
    Wend
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consulta de estado fiscal del �ltimo comando ejecutado.
'//====================================================================================================================
Private Sub CommandObtStFiscal_Click()
Dim resp As EstadoFiscal

    Debug.Print
    Debug.Print "CONSULTA ULTIMO ESTADO FISCAL:"
    Debug.Print

On Error GoTo keka
    resp = HASARNG.ObtenerUltimoEstadoFiscal
    Debug.Print "Doc. abierto             = [" & resp.DocumentoAbierto & "]"
    Debug.Print "Doc. fiscal abierto      = [" & resp.DocumentoFiscalAbierto & "]"
    Debug.Print "Error aritm�tico         = [" & resp.ErrorAritmetico & "]"
    Debug.Print "Error de ejecuci�n       = [" & resp.ErrorEjecucion & "]"
    Debug.Print "Error de estado          = [" & resp.ErrorEstado & "]"
    Debug.Print "Error general            = [" & resp.ErrorGeneral & "]"
    Debug.Print "Error mem. auditor�a     = [" & resp.ErrorMemoriaAuditoria & "]"
    Debug.Print "Error mem. fiscal        = [" & resp.ErrorMemoriaFiscal & "]"
    Debug.Print "Error mem. trabajo       = [" & resp.ErrorMemoriaTrabajo & "]"
    Debug.Print "Error par�metro          = [" & resp.ErrorParametro & "]"
    Debug.Print "Mem. fiscal casi llena   = [" & resp.MemoriaFiscalCasiLlena & "]"
    Debug.Print "Mem. fiscal inicializada = [" & resp.MemoriaFiscalInicializada & "]"
    Debug.Print "Mem. fiscal llena        = [" & resp.MemoriaFiscalLlena & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Consulta �ltimo estado de la impresora.
'//====================================================================================================================
Private Sub CommandObtStImpr_Click()
Dim resp As EstadoImpresora

    Debug.Print
    Debug.Print "CONSULTA ULTIMO ESTADO DE IMPRESORA:"
    Debug.Print

On Error GoTo keka
    resp = HASARNG.ObtenerUltimoEstadoImpresora
    Debug.Print "Caj�n abierto        = [" & resp.CajonAbierto & "]"
    Debug.Print "Error impresora      = [" & resp.ErrorImpresora & "]"
    Debug.Print "Falta papel testigo  = [" & resp.FaltaPapelJournal & "]"
    Debug.Print "Falta papel comprob. = [" & resp.FaltaPapelReceipt & "]"
    Debug.Print "Impresora ocupada    = [" & resp.ImpresoraOcupada & "]"
    Debug.Print "Impresora offline    = [" & resp.ImpresoraOffLine & "]"
    Debug.Print "Estado Or l�gico     = [" & resp.OrLogico & "]"
    Debug.Print "Tapa abierta         = [" & resp.TapaAbierta & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de Documento Patr�n, seg�n definici�n RG AFIP N� 3561/13.
'//====================================================================================================================
Private Sub CommandPatron_Click()
Dim estilo As AtributosDeTexto
Dim resp As RespuestaAbrirDocumento

    Debug.Print
    Debug.Print "IMPRESION DE DOCUMENTO PATRON:"
    Debug.Print

On Error GoTo keka
    estilo.BorradoTexto = False
    estilo.Centrado = False
    estilo.DobleAncho = False
    estilo.Negrita = False
    HASARNG.ConfigurarZona 1, estilo, "Pasaje Ignoto 777 - (1408) Villa Real ..........................", EstacionTicket, ZonaDomicilioEmisor
    HASARNG.ConfigurarZona 2, estilo, "Ciudad Aut�noma de Buenos Aires ................................", EstacionTicket, ZonaDomicilioEmisor
    HASARNG.ConfigurarZona 3, estilo, "Rep�blica Argentina ............................................", EstacionTicket, ZonaDomicilioEmisor
    HASARNG.ConfigurarZona 4, estilo, "Am�rica del Sur ................................................", EstacionTicket, ZonaDomicilioEmisor

    HASARNG.CargarDatosCliente "El Kioskazo de Villa Real ........................", "22222222226", ResponsableInscripto, TipoCUIT, "Avenida Julio Argentino Roca 852 - Local 9 - Entrepiso .........", "(1401) Ciudad Aut�noma de Buenos Aires .........................", "Rep�blica Argentina ............................................", "Am�rica del Sur ................................................"
    resp = HASARNG.AbrirDocumento(TiqueFacturaA)
    Debug.Print
    Debug.Print "(abrir) Doc. Patr�n Nro. = [" & resp.NumeroComprobante & "]... ABIERTO !"
    Debug.Print
    
    HASARNG.ImprimirTextoFiscal estilo, "Producto de fabricaci�n nacional .................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Origen: Salta ................................... ", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Producto de exportaci�n ..........................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Presentaci�n: Bolsa x Kilo .......................", DisplayNo
    HASARNG.ImprimirItem "CARAMELOS UNO ....................", 2.5, 9.99, Gravado, 21#, ModoSumaMonto, IIFijoMonto, 1.99, DisplayNo, ModoPrecioTotal, 1, "779123456791", "C1131", Kilo

    HASARNG.ImprimirTextoFiscal estilo, "Producto de fabricaci�n nacional .................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Origen: Jujuy ................................... ", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Producto de exportaci�n ..........................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Presentaci�n: Bolsa x Kilo .......................", DisplayNo
    HASARNG.ImprimirItem "CARAMELOS DOS .....................", 2.5, 9.99, Gravado, 21#, ModoSumaMonto, IIFijoMonto, 1.99, DisplayNo, ModoPrecioTotal, 1, "779123456792", "C1132", Kilo

    HASARNG.ImprimirTextoFiscal estilo, "Producto de fabricaci�n nacional .................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Origen: Chaco ................................... ", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Producto de exportaci�n ..........................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Presentaci�n: Bolsa x Kilo .......................", DisplayNo
    HASARNG.ImprimirItem "CARAMELOS TRES ....................", 2.5, 9.99, Gravado, 21#, ModoSumaMonto, IIFijoMonto, 1.99, DisplayNo, ModoPrecioTotal, 1, "779123456793", "C1133", Kilo

    HASARNG.ImprimirTextoFiscal estilo, "Producto de fabricaci�n nacional .................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Origen: Formosa ................................. ", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Producto de exportaci�n ..........................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Presentaci�n: Bolsa x Kilo .......................", DisplayNo
    HASARNG.ImprimirItem "CARAMELOS CUATRO ..................", 2.5, 9.99, Gravado, 21#, ModoSumaMonto, IIFijoMonto, 1.99, DisplayNo, ModoPrecioTotal, 1, "779123456794", "C1134", Kilo

    HASARNG.ImprimirTextoFiscal estilo, "Producto de fabricaci�n nacional .................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Origen: Tucum�n ................................. ", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Producto de exportaci�n ..........................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Presentaci�n: Bolsa x Kilo .......................", DisplayNo
    HASARNG.ImprimirItem "CARAMELOS CINCO ...................", 2.5, 9.99, Gravado, 21#, ModoSumaMonto, IIFijoMonto, 1.99, DisplayNo, ModoPrecioTotal, 1, "779123456795", "C1135", Kilo

    HASARNG.ImprimirTextoFiscal estilo, "Producto de fabricaci�n nacional .................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Origen: Santiago del Estero ..................... ", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Producto de exportaci�n ..........................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Presentaci�n: Bolsa x Kilo .......................", DisplayNo
    HASARNG.ImprimirItem "CARAMELOS SEIS ....................", 2.5, 9.99, Gravado, 21#, ModoSumaMonto, IIFijoMonto, 1.99, DisplayNo, ModoPrecioTotal, 1, "779123456796", "C1136", Kilo

    HASARNG.ImprimirTextoFiscal estilo, "Producto de fabricaci�n nacional .................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Origen: Catamarca ............................... ", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Producto de exportaci�n ..........................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Presentaci�n: Bolsa x Kilo .......................", DisplayNo
    HASARNG.ImprimirItem "CARAMELOS SIETE ...................", 2.5, 9.99, Gravado, 21#, ModoSumaMonto, IIFijoMonto, 1.99, DisplayNo, ModoPrecioTotal, 1, "779123456797", "C1137", Kilo

    HASARNG.ImprimirTextoFiscal estilo, "Producto de fabricaci�n nacional .................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Origen: La Rioja ................................ ", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Producto de exportaci�n ..........................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Presentaci�n: Bolsa x Kilo .......................", DisplayNo
    HASARNG.ImprimirItem "CARAMELOS OCHO ....................", 2.5, 9.99, Gravado, 21#, ModoSumaMonto, IIFijoMonto, 1.99, DisplayNo, ModoPrecioTotal, 1, "779123456798", "C1138", Kilo

    HASARNG.ImprimirTextoFiscal estilo, "Producto de fabricaci�n nacional .................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Origen: Mendoza ................................. ", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Producto de exportaci�n ..........................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Presentaci�n: Bolsa x Kilo .......................", DisplayNo
    HASARNG.ImprimirItem "CARAMELOS NUEVE ...................", 2.5, 9.99, Gravado, 21#, ModoSumaMonto, IIFijoMonto, 1.99, DisplayNo, ModoPrecioTotal, 1, "779123456799", "C1139", Kilo

    HASARNG.ImprimirTextoFiscal estilo, "Producto de fabricaci�n nacional .................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Origen: San Luis ................................ ", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Producto de exportaci�n ..........................", DisplayNo
    HASARNG.ImprimirTextoFiscal estilo, "Presentaci�n: Bolsa x Kilo .......................", DisplayNo
    HASARNG.ImprimirItem "CARAMELOS DIEZ ....................", 2.5, 9.99, Gravado, 21#, ModoSumaMonto, IIFijoMonto, 1.99, DisplayNo, ModoPrecioTotal, 1, "779123456790", "C1130", Kilo

    HASARNG.ImprimirOtrosTributos IIBB, "Percepci�n Ingresos Brutos ....", 165.3, 19.99
    HASARNG.ImprimirOtrosTributos ImpuestosMunicipales, "Percepci�n Imp Municipal ......", 165.3, 19.99

    HASARNG.ImprimirPago "Efectivo ........................................", 289.78, Pagar, DisplayNo, "Pesos Argentinos ................................", Efectivo, , ""

    HASARNG.CerrarDocumento , ""

    Debug.Print
    Debug.Print "DOCUMENTO PATRON IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Presupuesto 'X' clase 'A'.
'//====================================================================================================================
Private Sub CommandPRESUP_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - PRESUPUESTO 'X' Clase 'A':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", ResponsableInscripto, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(PresupuestoX)
    Debug.Print
    Debug.Print " - (abrir)Presupuesto 'X' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar)Presupuesto 'X' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                      = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "PRESUPUESTO 'X' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Establecer conexi�n con la impresora fiscal y consultar estado fiscal, y de impresora, actual.
'//====================================================================================================================
Private Sub CommandProtocolo_Click()
Dim stprn As EstadoImpresora
Dim resp As RespuestaConsultarVersion

On Error GoTo keka
    HASARNG.ArchivoRegistro "ifh2g.log"
    
    Debug.Print
    Debug.Print "========================================"
    Debug.Print "VERIFICANDO CONEXION A IMPRESORA FISCAL:"
    Debug.Print
    Debug.Print "ETHERNET - Conectando a... 10.0.7.100"
    'Debug.Print "ETHERNET - Conectando a... 127.0.0.1"
    Debug.Print
    HASARNG.Conectar "10.0.7.100"
    'HASARNG.Conectar "127.0.0.1"
    Debug.Print
    Debug.Print "CONECTADO ! ..."
    
    CommandObtStImpr_Click
    CommandObtStFiscal_Click
    Debug.Print
    Debug.Print "OCX FISCAL HASAR 2G Protocolo  = [" & HASARNG.ObtenerVersionProtocolo & "]"
    Debug.Print "OCX FISCAL HASAR 2G Revisi�n   = [" & HASARNG.ObtenerRevision & "]"
    Debug.Print
    CommandConsModvers_Click
    Debug.Print "========================================"
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description
     
End Sub

'//====================================================================================================================
'// Consultar rango de fechas, o n�meros de cierres 'Z'.
'//====================================================================================================================
Private Sub CommandRangoZ_Click()
Dim resp1 As RespuestaObtenerRangoFechasPorZetas
Dim resp2 As RespuestaObtenerRangoZetasPorFechas

On Error GoTo keka
    Debug.Print
    Debug.Print "CONSULTA RANGO NUMEROS DE CIERRES 'Z':"
    Debug.Print
    Debug.Print "Rango consultado: 01/01/2015 al 31/12/2015..."
    Debug.Print
    
    resp2 = HASARNG.ObtenerRangoZetasPorFechas("01/01/2014", "31/12/2015")
    Debug.Print
    Debug.Print "Nro. 'Z' inicial  = [" & resp2.ZetaInicial & "]"
    Debug.Print "Nro. 'Z' final    = [" & resp2.ZetaFinal & "]"
    Debug.Print
    
    Debug.Print "CONSULTA RANGO FECHAS DE CIERRES 'Z':"
    Debug.Print
    Debug.Print "Rango consultado: 1 al 4000..."
    Debug.Print
    
    resp1 = HASARNG.ObtenerRangoFechasPorZetas(1, 4000)
    Debug.Print
    Debug.Print "Fecha 'Z' inicial = [" & resp1.FechaInicial & "]"
    Debug.Print "Fecha 'Z' final   = [" & resp1.FechaFinal & "]"
    
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Recibo X'.
'//====================================================================================================================
Private Sub CommandRBOX_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - RECIBO 'X':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDocumentoAsociado 1, TiqueFacturaA, 2019, 1458
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", ResponsableInscripto, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(ReciboX)
    Debug.Print
    Debug.Print " - (abrir) Recibo 'X' Nro. = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    HASARNG.ImprimirItem "Item a la venta...", 1, 3500, Gravado, 0, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirConceptoRecibo "Alquiler mes de Marzo de 2015..."
    
    resppago = HASARNG.ImprimirPago("Efectivo...", 3500, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio       = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    Debug.Print
    Debug.Print " - (cerrar)Recibo 'X' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                 = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "RECIBO 'X' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description
    
End Sub

'//====================================================================================================================
'// Se pide otro ejemplar del �ltimo comprobante emitido.
'//====================================================================================================================
Private Sub CommandReimpr_Click()
Dim resp As RespuestaPedirReimpresion

    Debug.Print
    Debug.Print "REIMPRESION COMPROBANTE:"
    Debug.Print
    
On Error GoTo keka
    resp = HASARNG.PedirReimpresion
    Debug.Print
    Debug.Print "Copia Nro. = [" & resp.NumeroDeCopia & "]"
    Debug.Print
    Debug.Print "FIN DE LA REIMPRESION !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Remito 'R'.
'//====================================================================================================================
Private Sub CommandRTOR_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - REMITO 'R':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", ResponsableInscripto, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    HASARNG.CargarTransportista "Raz�n Social Transportista...", "12345678903", "Domicilio Transportista...", "Nombre del Chofer...", TipoCI, "12345678", "ABC123", "DEF456"
    HASARNG.CargarDocumentoAsociado 1, TiqueFacturaA, 2019, 1458
    respabrir = HASARNG.AbrirDocumento(RemitoR)
    Debug.Print
    Debug.Print " - (abrir) Remito 'R' Nro. = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    resppago = HASARNG.ImprimirPago("Efectivo...", 100, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio       = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar)Remito 'R' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                 = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "REMITO 'R' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Remito X'.
'//====================================================================================================================
Private Sub CommandRTOX_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - REMITO 'X':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", ResponsableInscripto, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    HASARNG.CargarTransportista "Raz�n Social Transportista...", "12345678903", "Domicilio Transportista...", "Nombre del Chofer...", TipoCI, "12345678", "ABC123", "DEF456"
    HASARNG.CargarDocumentoAsociado 1, TiqueFacturaA, 2019, 1458
    respabrir = HASARNG.AbrirDocumento(RemitoX)
    Debug.Print
    Debug.Print " - (abrir) Remito 'X' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    resppago = HASARNG.ImprimirPago("Efectivo...", 100, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio        = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Remito 'X' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                  = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "REMITO 'X' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Se abandona la aplicaci�n.
'//====================================================================================================================
Private Sub CommandSalir_Click()

    Debug.Print
    Debug.Print "...Oh !................................................."
    Debug.Print "...................CERRANDO PROGRAMA...................."
    Debug.Print "...............................................Chau !..."
    Debug.Print
    
    End
    
End Sub

'//====================================================================================================================
'// Consulta de subtotal.
'//====================================================================================================================
Private Sub CommandSubtot_Click()
Dim respsubt As RespuestaConsultarSubtotal

    Debug.Print
    Debug.Print "CONSULTA SUBTOTAL:"
    Debug.Print

On Error GoTo keka
    respsubt = HASARNG.ConsultarSubtotal(NoImprimeSubtotal, DisplayNo)
    Debug.Print
    Debug.Print "Cant. Items    = [" & respsubt.CantidadItems & "]"
    Debug.Print "Subtotal       = [" & respsubt.Subtotal & "]"
    Debug.Print "Base imponible = [" & respsubt.MontoBase & "]"
    Debug.Print "IVA            = [" & respsubt.MontoIVA & "]"
    Debug.Print "Imp. Internos  = [" & respsubt.MontoImpInternos & "]"
    Debug.Print "Otros tributos = [" & respsubt.MontoOtrosTributos & "]"
    Debug.Print "Pagado         = [" & respsubt.MontoPagado & "]"
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Factura 'A'.
'//====================================================================================================================
Private Sub CommandTFA_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE FACTURA 'A' / 'A CON LEYENDA':"
    Debug.Print
      
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", ResponsableInscripto, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(TiqueFacturaA)
    Debug.Print
    Debug.Print " - (abrir) Tique Factura 'A' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0.00000001, DisplayNo, ModoPrecioBase, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioBase
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio               = [" & resppago.Saldo & "]"
    Debug.Print
            
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Factura 'A' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                         = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE FACTURA 'A' IMPRESO !..."
    Debug.Print
    
    Exit Sub
                
keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description
    
End Sub
        
'//====================================================================================================================
'// Impresi�n de un Tique Factura 'B'.
'//====================================================================================================================
Private Sub CommandTFB_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE FACTURA 'B':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", TiposDeResponsabilidadesCliente.ConsumidorFinal, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(TiqueFacturaB)
    Debug.Print
    Debug.Print " - (abrir) Tique Factura 'B' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0.00000001, DisplayNo, ModoPrecioBase, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioBase
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio               = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Factura 'B' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                         = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE FACTURA 'B' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description
    
End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Factura 'C'.
'//====================================================================================================================
Private Sub CommandTFC_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE FACTURA 'C':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", Monotributo, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(TiqueFacturaC)
    Debug.Print
    Debug.Print " - (abrir) Tique Factura 'C' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio               = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Factura 'C' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                         = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE FACTURA 'C'IMPRESO !..."
    Debug.Print

    Exit Sub
    
keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Factura 'M'.
'//====================================================================================================================
Private Sub CommandTFM_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE FACTURA 'M':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", Monotributo, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(TiqueFacturaM)
    Debug.Print
    Debug.Print " - (abrir) Tique Factura 'M' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio               = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Factura 'M' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                         = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE FACTURA 'M'IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique a Consumidor Final.
'//====================================================================================================================
Private Sub CommandTique_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE A CONSUMIDOR FINAL:"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    respabrir = HASARNG.AbrirDocumento(Tique)
    Debug.Print
    Debug.Print " - (abrir) Tique Cons. Final Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0.00000001, DisplayNo, ModoPrecioBase, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioBase

    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio               = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Cons. Final Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                         = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE A CONSUMIDOR FINAL IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Nota de Cr�dito a Consumidor Final.
'//====================================================================================================================
Private Sub CommandTiqueNC_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE NOTA DE CREDITO A CONSUMIDOR FINAL:"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    respabrir = HASARNG.AbrirDocumento(TiqueNotaCredito)
    Debug.Print
    Debug.Print " - (abrir) Tique Nota de Cr�dito a Cons. Final Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio                                 = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Nota de Cr�dito a Cons. Final Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                                           = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE NOTA DE CREDITO A CONSUMIDOR FINAL IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Nota de Cr�dito 'A'.
'//====================================================================================================================
Private Sub CommandTNCA_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE NOTA DE CREDITO 'A' / 'A con leyenda':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", ResponsableInscripto, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(TiqueNotaCreditoA)
    Debug.Print
    Debug.Print " - (abrir) Tique Nota de Cr�dito 'A' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio                       = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Nota de Cr�dito 'A' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                                 = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE NOTA DE CREDITO 'A' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Nota de Cr�dito 'B'.
'//====================================================================================================================
Private Sub CommandTNCB_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE NOTA DE CREDITO 'B':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", Monotributo, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(TiqueNotaCreditoB)
    Debug.Print
    Debug.Print " - (abrir) Tique Nota de Cr�dito 'B' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio                       = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Nota de Cr�dito 'B' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                                 = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE NOTA DE CREDITO 'B' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Nota de Cr�dito 'C'.
'//====================================================================================================================
Private Sub CommandTNCC_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE NOTA DE CREDITO 'C':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", Monotributo, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(TiqueNotaCreditoC)
    Debug.Print
    Debug.Print " - (abrir) Tique Nota de Cr�dito 'C' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio                       = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Nota de Cr�dito 'C' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                                 = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE NOTA DE CREDITO 'C' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Nota de Cr�dito 'M'.
'//====================================================================================================================
Private Sub CommandTNCM_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE NOTA DE CREDITO 'M':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", Monotributo, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(TiqueNotaCreditoM)
    Debug.Print
    Debug.Print " - (abrir) Tique Nota de Cr�dito 'M' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio                       = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Nota de Cr�dito 'M' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                                 = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE NOTA DE CREDITO 'M' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Nota de D�bito 'A'.
'//====================================================================================================================
Private Sub CommandTNDA_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE NOTA DE DEBITO 'A' / 'A con leyenda':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", ResponsableInscripto, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(TiqueNotaDebitoA)
    Debug.Print
    Debug.Print " - (abrir) Tique Nota de D�bito 'A' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio                      = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Nota de D�bito 'A' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                                = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE NOTA DE DEBITO 'A' IMPRESO !..."
    Debug.Print

    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Nota de D�bito 'B'.
'//====================================================================================================================
Private Sub CommandTNDB_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE NOTA DE DEBITO 'B':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", Monotributo, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(TiqueNotaDebitoB)
    Debug.Print
    Debug.Print " - (abrir) Tique Nota de D�bito 'B' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio                      = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Nota de D�bito 'B' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                                = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE NOTA DE DEBITO 'B' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description
    
End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Nota de D�bito 'C'.
'//====================================================================================================================
Private Sub CommandTNDC_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE NOTA DE DEBITO 'C':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", Monotributo, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(NotaDeDebitoC)
    Debug.Print
    Debug.Print " - (abrir) Tique Nota de D�btio 'C' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio                      = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Nota de D�btio 'C' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                                = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE NOTA DE DEBITO 'C' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description
    
End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Nota de D�bito 'M'.
'//====================================================================================================================
Private Sub CommandTNDM_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE NOTA DE DEBITO 'M':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", Monotributo, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(NotaDeDebitoM)
    Debug.Print
    Debug.Print " - (abrir) Tique Nota de D�bito 'M' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio                      = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Nota de D�bito 'M' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                                = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE NOTA DE DEBITO 'M' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Recibo 'A'.
'//====================================================================================================================
Private Sub CommandTRA_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE RECIBO 'A' / 'A con leyenda':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", ResponsableInscripto, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(ReciboA)
    Debug.Print
    Debug.Print " - (abrir) Tique Recibo 'A' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print "Pagando... Cambio                 = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Recibo 'A' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                        = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE RECIBO 'A' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Recibo 'B'.
'//====================================================================================================================
Private Sub CommandTRB_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE RECIBO 'B':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", Monotributo, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(ReciboB)
    Debug.Print
    Debug.Print " - (abrir) Tique Recibo 'B' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio              = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Recibo 'B' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                        = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE RECIBO 'B' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Recibo 'C'.
'//====================================================================================================================
Private Sub CommandTRC_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE RECIBO 'C':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", Monotributo, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(ReciboC)
    Debug.Print " - (abrir) Tique Recibo 'C' Nro.  = [" & respabrir.NumeroComprobante & "]"
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio              = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Recibo 'C' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                        = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE RECIBO 'C'IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Impresi�n de un Tique Recibo 'M'.
'//====================================================================================================================
Private Sub CommandTRM_Click()
Dim respabrir As RespuestaAbrirDocumento
Dim resppago As RespuestaImprimirPago
Dim estilo As AtributosDeTexto
Dim respcierre As RespuestaCerrarDocumento

On Error GoTo keka
    Debug.Print
    Debug.Print "IMPRIMIENDO - TIQUE RECIBO 'M':"
    Debug.Print
    Debug.Print "Cargando c�digo de barras..."
    HASARNG.CargarCodigoBarras CodigoTipoEAN13, "779123456789", ImprimeNumerosCodigo, ProgramaCodigo
    
    HASARNG.CargarDatosCliente "Raz�n Social Cliente...", "99999999995", Monotributo, TipoCUIT, "Domicilio Cliente...", "Domicilio extensi�n 1...", "Domicilio extensi�n 2...", "Domicilio extensi�n 3...."
    respabrir = HASARNG.AbrirDocumento(ReciboM)
    Debug.Print
    Debug.Print " - (abrir) Tique Recibo 'M' Nro.  = [" & respabrir.NumeroComprobante & "]"
    Debug.Print
    
    estilo.Negrita = True
    HASARNG.ImprimirTextoFiscal estilo, "Hasta agotar stock...", DisplayNo
    HASARNG.ImprimirItem "Item a la venta...", 1, 100, Gravado, 21, ModoSumaMonto, IIVariablePorcentual, 0, DisplayNo, ModoPrecioTotal, 1, "779123456789", , Unidad
    HASARNG.ImprimirDescuentoItem "Oferta 10%...", 10, DisplayNo, ModoPrecioTotal
    resppago = HASARNG.ImprimirPago("Efectivo...", 90, Pagar, DisplayNo, , Efectivo, , "")
    Debug.Print
    Debug.Print " - Pagando... Cambio              = [" & resppago.Saldo & "]"
    Debug.Print
    
    respcierre = HASARNG.CerrarDocumento(, "")
    
    Debug.Print
    Debug.Print " - (cerrar) Tique Recibo 'M' Nro. = [" & respcierre.NumeroComprobante & "]"
    Debug.Print " - P�ginas                        = [" & respcierre.CantidadDePaginas & "]"
    Debug.Print
    Debug.Print "TIQUE RECIBO 'M' IMPRESO !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description
    
End Sub

'//====================================================================================================================
'// Se consulta el n�mero de la 'Z' establecida como l�mite para el borrado de la CTD (Cinta testigo Digital) .
'//====================================================================================================================
Private Sub CommandZBorr_Click()
Dim resp As RespuestaConsultarZetaBorrable

    Debug.Print
    Debug.Print "CONSULTA LIMITE BORRADO CIERRES 'Z':"
    Debug.Print

On Error GoTo keka
    resp = HASARNG.ConsultarZetaBorrable
    Debug.Print
    Debug.Print "L�mite Cierre 'Z' Nro. = [" & resp.NumeroZeta & "]"
    Debug.Print
    Debug.Print "FIN DE LA CONSULTA !..."
    Debug.Print
    
    Exit Sub

keka:
    Debug.Print "Error = " & Err.Number & " : " & Err.Description

End Sub

'//====================================================================================================================
'// Carga de la ventana principal.
'// Se activa el archivo de registro de actividades del OCX.
'//====================================================================================================================
Private Sub Form_Load()

    HASARNG.ArchivoRegistro "ocx.log"

End Sub

'//====================================================================================================================
'// Procesando comando actual.
'//====================================================================================================================
Private Sub HASARNG_ComandoEnProceso()

    If (Not (proc)) Then
        Debug.Print "Comando en proceso..."
        proc = True
    End If
    
End Sub

'//====================================================================================================================
'// Fin de procesamiento del comando enviado.
'//====================================================================================================================
Private Sub HASARNG_ComandoProcesado()

    Debug.Print "Comando procesado !"
    proc = False

End Sub

'//====================================================================================================================
'// Procesamiento del evento impresora.
'//====================================================================================================================
Private Sub HASARNG_EventoImpresora(Estado As EstadoImpresora)

    Debug.Print "Evento impresora..."
    Debug.Print "Estado Caj�n = [" & Estado.CajonAbierto & "]"
    
End Sub

'//=====================================================================================================================
'// Procesando impresora ocupada.
'//=====================================================================================================================
Private Sub HASARNG_EventoEstadoEspera(EstadoImpresion As EstadoImpresora)

    Debug.Print "DC2/DC4 Aguarde y ser� atendido..."
    Debug.Print "Impresora ocupada = [" & EstadoImpresion.ImpresoraOcupada & "]"

End Sub
