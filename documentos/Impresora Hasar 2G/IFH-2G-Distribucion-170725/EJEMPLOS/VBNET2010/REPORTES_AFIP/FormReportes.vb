﻿'//###################################################################################################################################################
'// Impresoras Fiscales HASAR (IFH) Segunda Generación (2G)                                 Dto. Software de Base, Compañía HASAR SAIC, Grupo HASAR
'// RG AFIP Nº 3561/13 , Controladores Fiscales Nueva Tecnología                            HasarLibreriaFiscal3561.dll, Protocolo '0', Revisión '103'
'// Argentina, Abril 2016                                                                   VB.Net, Visual Studio 2010
'//
'// DESCARGA DE REPORTES - Desde la IFH 2G
'// Reportes Electrónicos AFIP, CTD (Cinta Testigo Digital) -nativo-, Duplicado Comprob. 'A' -nativo-, Registro interno de actividades la IFH 2G (Log)
'//
'// HASAR no asume responsabilidad alguna por los errores u omisiones que este código pudiese tener, ni de los daños que su uso pudiere causar.
'//###################################################################################################################################################
Option Explicit On

Imports System
Imports System.Globalization
Imports System.Text
Imports System.IO
Imports System.Net.WebRequestMethods

'=====================================================================================================================================================
' Formulario principal de la aplicación ...
'=====================================================================================================================================================
Public Class FormReportes
    Private WithEvents hasar As hfl.argentina.HasarImpresoraFiscalRG3561 = New hfl.argentina.HasarImpresoraFiscalRG3561
    Dim pos As Long = 1                                                       '// Posicionamiento en archivos de descarga (lectura/escritura)
    Dim poslog As Long = 1                                                    '// Posicionamiento en el archivo de registro de descargas
    Dim archrep As String = "Descargas.txt"                                   '// Reporte de descargas realizadas
    Dim logapp As String = "Reportes_IFH2G.log"                               '// Registro de actividades de la aplicación
    Dim flag As Boolean = False                                               '// Indica si se realizó al menos una descarga
    Dim conectado As Boolean = False                                          '// Hubo conexión inicial

    Public Shared ReadOnly Property StartupPath As String                     '// Retorna el path de la aplicación
        Get
            StartupPath = Application.StartupPath
        End Get
    End Property

    '==================================================================================================================================================
    ' BOTÓN: "CONECTAR" - Intenta establecer conexión con la IFH 2G ...
    '==================================================================================================================================================
    Private Sub ButtonConectar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonConectar.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion
        Dim res As Microsoft.VisualBasic.MsgBoxResult
        Dim msj As String                                                     '// A mostrar en MsgBox()
        Dim dirIP As String                                                   '// IP de la IFH 2G, o del emulador HASAR
        Dim puerto As Integer                                                 '// Puerto HTTP del emulador HASAR

        Try
            Application.DoEvents()
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            TextBoxMens.Clear()
            hasar.archivoRegistro(logapp)                                     '// Log de la aplicación
            hasar.establecerTiempoDeEspera(30000)                             '// Por defecto: 10000
            hasar.establecerTiempoDeEsperaLecturaEscritura(35000)             '// Por defecto: 15000
            dirIP = TextBoxIP1.Text & "." & TextBoxIP2.Text & "." & TextBoxIP3.Text & "." & TextBoxIP4.Text

            If (dirIP = "...") Then
                dirIP = "127.0.0.1"                                           '// Conectarse al emulador HASAR
            End If

            If (TextBoxPuerto.Text = "") Then
                puerto = 80                                                   '// Por defecto, para el emulador HASAR
            Else
                puerto = Val(TextBoxPuerto.Text)
            End If

            hasar.conectar(dirIP, puerto)
            resp = hasar.ConsultarVersion()

            'If (resp.getVersionProtocolo() > hasar.ObtenerVersionProtocolo()) Then
            '    msj = "DLL INCOMPATIBLE CON IFH 2G" & vbCrLf & vbCrLf & _
            '          "DLL 2G Protocolo" & vbTab & "=[" & hasar.ObtenerVersionProtocolo() & "]" & vbCrLf & _
            '          "IFH 2G Protocolo" & vbTab & "=[" & resp.getVersionProtocolo() & "]" & vbCrLf & vbCrLf & _
            '          "Debe ser:  Protocolo DLL >= Protocolo IFH" & vbCrLf & vbCrLf & _
            '          "Luego de ACEPTAR ..." & vbCrLf & _
            '          "Haga click en el botón TERMINAR ..."
            '    MsgBox(msj, vbOKOnly + vbCritical, "ERROR compatibilidad ...")

            '    Me.Cursor = System.Windows.Forms.Cursors.Arrow
            '    conectado = False
            '    Exit Sub
            'End If

            LabelConexion.Text = "OK - Conectado"
            LabelConexion.ForeColor = Color.Green
            LabelLogApp.Text = "LOG App = [ReportesIFH2G.log]"
            LabelProto.Text = "Protocolo DLL = [" & hasar.ObtenerVersionProtocolo() & "]"
            LabelRevis.Text = "Revisión DLL = [" & hasar.ObtenerRevision() & "]"

            LabelProd.Text = "Producto = [" & resp.getNombreProducto() & "]   -   Marca = [" & resp.getMarca() & "]"
            LabelModelo.Text = "Modelo y Versión = [" & resp.getVersion() & "]"
            LabelFirmware.Text = "Fecha de Firmware = [" & resp.getFechaFirmware() & "]"
            LabelMotor.Text = "Motor Versión = [" & resp.getVersionMotor() & "]"
            LabelIFHProto.Text = "Protocolo IFH 2G = [" & resp.getVersionProtocolo() & "]"
            TextBoxDDAfipDesde.Focus()
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            If (System.IO.File.Exists(archrep)) Then
                msj = "REPORTE DE DESCARGAS : " & archrep & vbCrLf & vbCrLf & _
                  "Se encontró el correspondiente a descargas anteriores ..." & vbCrLf & _
                  "¿ Desea eliminarlo ?"
                res = MsgBox(msj, vbOKCancel + vbQuestion, "Archivo: REPORTE DE DESCARGAS ...")

                If res = vbCancel Then
                    conectado = True
                    Exit Sub
                End If

                My.Computer.FileSystem.DeleteFile(archrep)
            End If

            conectado = True
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description & vbCrLf & vbCrLf & _
                  "Luego de ACEPTAR ..." & vbCrLf & _
                  "Intente con otra dirección IP, o bien .." & vbCrLf & _
                  "Haga click en el botón TERMINAR ..."
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR conexión...")

            TextBoxIP1.Focus()
        End Try

    End Sub

    '===================================================================================================================================================
    ' BOTÓN: "TERMINAR" - Finaliza la ejecución del software ...
    '===================================================================================================================================================
    Private Sub ButtonTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTerminar.Click
        Dim p As System.ComponentModel.CancelEventArgs = New System.ComponentModel.CancelEventArgs

        p.Cancel = True
        FormReportes_Closing(sender, p)

    End Sub

    '===================================================================================================================================================
    ' Finaliza la ejecución del software sea porque se hizo clic en el botón "TERMNAR", o porque se hizo clic en [X] ...
    '===================================================================================================================================================
    Private Sub FormReportes_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.FormClosing
        Dim resp As Microsoft.VisualBasic.MsgBoxResult
        Dim msj As String                                                 '// A mostrar en MsgBox()
        Dim aux As String                                                 '// Se muestra si hubo descarga de información

        msj = "CERRANDO PROGRAMA ... Chau ! ..."
        aux = vbCrLf & vbCrLf & "Toda la informac ión descargada, disponible en:" & vbCrLf & vbCrLf & StartupPath

        If ((flag) And conectado) Then
            msj = msj & aux
        End If

        resp = MsgBox(msj, vbOKCancel + vbInformation, "IFH 2G - Descarga de Reportes ...")

        If (resp = vbCancel) Then
            e.Cancel() = True                                              '// Se continúa ejecutando la aplicación
            Exit Sub
        End If

        Me.Finalize()                                                      '// Se liberan recursos
        Application.Exit()                                                 '// No se aceptan más mensajes a la aplicación

    End Sub

    '===================================================================================================================================================
    ' Rutina para escritura de información sobre el archivo de registro de las descargas realizadas.
    '===================================================================================================================================================
    Private Sub GrabarLog(ByVal archivo As String, ByVal info As String)
        Dim ifr As Integer                                                 '// Identificador de archivo
        Dim rectam As Integer = 1024                                       '// Tamaño de registro

        ifr = FreeFile()
        FileOpen(ifr, archivo, OpenMode.Binary, , , rectam)
        FilePut(ifr, info, poslog)
        poslog = Seek(ifr)
        FileClose(ifr)

    End Sub

    '===================================================================================================================================================
    ' Rutina para escritura de información sobre un archivo de descarga de información.
    '===================================================================================================================================================
    Private Sub GrabarInfoBin(ByVal archivo As String, ByVal info As String)
        Dim ifr As Integer                                                 '// Identificador de archivo
        Dim rectam As Integer = 1024                                       '// Tamaño de registro

        ifr = FreeFile()
        FileOpen(ifr, archivo, OpenMode.Binary, , , rectam)
        FilePut(ifr, info, pos)
        pos = Seek(ifr)
        FileClose(ifr)

    End Sub

    '====================================================================================================================================================
    ' EVENTO: Comando en proceso ...
    '====================================================================================================================================================
    Private Sub hasar_eventoComandoEnProceso() Handles hasar.eventoComandoEnProceso

        Application.DoEvents()
        TextBoxMens.Text = "Procesando comando ...  "

    End Sub

    '====================================================================================================================================================
    ' EVENTO: Comando procesado ...
    '====================================================================================================================================================
    Private Sub hasar_eventoComandoProcesado() Handles hasar.eventoComandoProcesado

        Application.DoEvents()
        TextBoxMens.Text = "Listo ! - Comando procesado ...  "

    End Sub

    '====================================================================================================================================================
    ' EVENTO: Evento impresora ...
    '====================================================================================================================================================
    Private Sub hasar_eventoImpresora(ByVal printer As hfl.argentina.Estados_Fiscales_RG3561.EstadoImpresora) Handles hasar.eventoImpresora

        Application.DoEvents()
        TextBoxMens.Text = "EVENTO IMPRESORA :  " & _
                           "Cajón abierto = [" & printer.getCajonAbierto() & "] , " & _
                           "Error impresora = [" & printer.getErrorImpresora() & "] , " & _
                           "Falta papel J = [" & printer.getFaltaPapelJournal() & "] , " & _
                           "Falta papel R = [" & printer.getFaltaPapelReceipt() & "] , " & _
                           "Impresora ocupada = [" & printer.getImpresoraOcupada() & "] , " & _
                           "Impresora offline = [" & printer.getImpresoraOffLine() & "] , " & _
                           "Tapa abierta = [" & printer.getTapaAbierta() & "] , "

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: "BORRAR" - Limpia los TextBox con la dirección IP y puerto HTTP ...
    '====================================================================================================================================================
    Private Sub ButtonLimpiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonLimpiar.Click

        TextBoxIP1.Clear()
        TextBoxIP1.Focus()
        TextBoxIP2.Clear()
        TextBoxIP3.Clear()
        TextBoxIP4.Clear()
        TextBoxPuerto.Clear()

    End Sub

    '====================================================================================================================================================
    ' Carga del formulario principal de la aplicación ...
    '====================================================================================================================================================
    Private Sub FormReportes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.ButtonLimpiar_Click(sender, e)
        Me.AcceptButton = Me.ButtonConectar
        ComboBoxDupDoc.SelectedIndex = 3

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: REPORTES ELECTRÓNICOS - Descarga ...
    '
    ' La descarga permite generar un archivo ZIP, resultado de la conversión de ASCII85 a Binario de toda la información obtenida de la IFH 2G. El ZIP 
    ' contendrá 3 archivos ".PEM" en formato PKCS#7:
    '
    '    - Reporte de totales (información de la memoria fiscal, a elevar a la AFIP)
    '    - Duplicado comprobantes 'A', 'A' con leyenda y 'M' (información extraída de la Cinta Testigo Digital -CTD-, a elevar a la AFIP)
    '    - CTD, o memoria de auditoría (el contribuyente debe conservar dos copias de esta información)
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información, accediendo a la interfaz web de la IFH 2G.
    '===================================================================================================================================================
    Private Sub ButtonRepElect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRepElect.Click
        Dim bloque1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueReporteElectronico
        Dim bloque2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueReporteElectronico
        Dim rep As hfl.argentina.HasarImpresoraFiscalRG3561.TiposReporteAFIP
        Dim res As Microsoft.VisualBasic.MsgBoxResult
        Dim msj As String                                                      '// A mostrar en MsgBox()
        Dim arch As String = "Reportes.a85"                                    '// Para grabar la descarga de información, en ASCII85
        Dim archzip As String = "Reportes.zip"                                 '// Resultado final de la descarga completa
        Dim archmem As String = "MemFiscal.zip"                                '// Sólo Memoria fiscal
        Dim pp As Byte()                                                       '// Almacenamiento de ASCII85 decodificado a binario
        Dim ja As Ascii85 = New Ascii85                                        '// Instancia de la clase de decodificación
        Dim desde As String                                                    '// Comienzo del rango de selección
        Dim hasta As String                                                    '// Final del rango de selección
        Dim fileReader As String                                               '// Para leer archivos en el "encoding" original
        Dim oFileStream As System.IO.FileStream                                '// Para generar el ZIP

        Try
            If (System.IO.File.Exists(archzip) Or System.IO.File.Exists(arch) Or (System.IO.File.Exists(archmem))) Then
                msj = "REPORTE ELECTRÓNICOS : " & vbCrLf & vbCrLf & _
                  "Se encontraron archivos de descargas anteriores ..." & vbCrLf & _
                  "Si continúa se perderá dicha información ..." & vbCrLf & vbCrLf & _
                  "¿ Desea continuar ?"
                res = MsgBox(msj, vbOKCancel + vbQuestion, "Descarga: REPORTES ELECTRÓNICOS ...")

                If res = vbCancel Then
                    NuevaDescargaReportesAFIP()
                    Exit Sub
                End If

                If (System.IO.File.Exists(archzip)) Then My.Computer.FileSystem.DeleteFile(archzip)
                If (System.IO.File.Exists(arch)) Then My.Computer.FileSystem.DeleteFile(arch)
                If (System.IO.File.Exists(archmem)) Then My.Computer.FileSystem.DeleteFile(archmem)
            End If

            TextBoxMens.Clear()

            msj = "REPORTES ELECTRÓNICOS AFIP ..." & vbCrLf & vbCrLf & _
                  "A descargar información ..." & vbCrLf & vbCrLf & _
                  "Si acepta, ... POR FAVOR ESPERE ..." & vbCrLf & _
                  "Esta operación puede durar algunos minutos ..."
            res = MsgBox(msj, vbOKCancel + vbInformation, "Descarga: REPORTES ELECTRÓNICOS AFIP ...")

            If (res = vbCancel) Then
                MsgBox("DESCARGA CANCELADA ! ...", vbOKOnly + vbInformation, "Descarga: REPORTES ELECTRÓNICOS AFIP ...")
                NuevaDescargaReportesAFIP()
                Exit Sub
            End If

            If (RadioButtonAfipCompleto.Checked) Then
                rep = hfl.argentina.HasarImpresoraFiscalRG3561.TiposReporteAFIP.REPORTE_AFIP_COMPLETO
                LabelRepElect.Text = "Guardando en: " & archzip
            Else
                rep = hfl.argentina.HasarImpresoraFiscalRG3561.TiposReporteAFIP.REPORTE_AFIP_MEMORIA_FISCAL
                LabelRepElect.Text = "Guardando en: " & archmem
            End If

            desde = TextBoxDDAfipDesde.Text & "/" & TextBoxMMAfipDesde.Text & "/" & TextBoxAAAfipDesde.Text
            hasta = TextBoxDDAfipHasta.Text & "/" & TextBoxMMAfipHasta.Text & "/" & TextBoxAAAfipHasta.Text

            If ((Not (IsDate(desde))) Or (Not (IsDate(hasta)))) Then
                MsgBox("RANGO DE FECHAS INVÁLIDO ! ..." & vbCrLf & "Por favor, verifique que ambas fechas sean correctas ...", vbOKOnly + vbCritical, "Descarga: REPORTES ELECTRÓNICOS AFIP ...")
                NuevaDescargaReportesAFIP()
                Exit Sub
            End If

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - REPORTES ELECTRÓNICOS AFIP" & vbCrLf)

            If (RadioButtonAfipCompleto.Checked) Then
                GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - TODOS: CTD / DUPLICADOS / TOTALES" & vbCrLf)
            Else
                GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - SÓLO TOTALES" & vbCrLf)
            End If

            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - Desde: " & desde & " Hasta: " & hasta & vbCrLf)
            bloque1 = hasar.ObtenerPrimerBloqueReporteElectronico(desde, hasta, rep)

            If (bloque1.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                msj = "Descarga: REPORTES ELECTRÓNICOS AFIP ..." & vbCrLf & vbCrLf & _
                      "No hay información disponible ! ..."
                MsgBox(msj, vbOKOnly + vbInformation, "Descarga: REPORTES ELECTRÓNICOS AFIP ...")

                NuevaDescargaReportesAFIP()
                Me.Cursor = System.Windows.Forms.Cursors.Arrow
                Exit Sub
            End If

            '// Generación del archivo que contendrá ASCII85
            TextBoxMens.Text = bloque1.getInformacion()
            GrabarInfoBin(arch, bloque1.getInformacion())
            bloque2 = hasar.ObtenerSiguienteBloqueReporteElectronico()

            While (bloque2.getRegistro() <> hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                TextBoxMens.Text = bloque2.getInformacion()
                GrabarInfoBin(arch, bloque2.getInformacion())
                bloque2 = hasar.ObtenerSiguienteBloqueReporteElectronico()
                Application.DoEvents()
            End While

            If (bloque2.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                TextBoxMens.Text = bloque2.getInformacion()
                GrabarInfoBin(arch, bloque2.getInformacion())
            End If

            TextBoxMens.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            msj = "REPORTES ELECTRÓNICOS AFIP :" & vbCrLf & vbCrLf & _
                  "Descarga finalizada ! ..."
            MsgBox(msj, vbOKOnly + vbInformation, "Descarga: REPORTES ELECTRÓNICOS AFIP ...")

            '// Se lee el archivo con ASCII85
            fileReader = My.Computer.FileSystem.ReadAllText(arch)
            pp = ja.Decode(fileReader)     '// Convierte ASCII85 a binario

            '// Se genera el ZIP con la decodificación
            If (RadioButtonAfipCompleto.Checked) Then
                oFileStream = New System.IO.FileStream(archzip, System.IO.FileMode.Create)
            Else
                oFileStream = New System.IO.FileStream(archmem, System.IO.FileMode.Create)
            End If

            oFileStream.Write(pp, 0, pp.Length)
            oFileStream.Close()

            flag = True
            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - Descarga OK ..." & vbCrLf & vbCrLf)
            NuevaDescargaReportesAFIP()
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Descarga: REPORTES ELECTRÓNICOS AFIP ...")

            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & msj & vbCrLf & vbCrLf)
            NuevaDescargaReportesAFIP()

            If (Err.Number = 5) Then
                TextBoxIP1.Focus()
            End If
        End Try
    End Sub

    '====================================================================================================================================================
    ' BOTÓN: REPORTE DOCUMENTOS - Descarga ...
    '
    ' La descarga permite generar un archivo ZIP con el detalle de uno o más comprobantes emitidos (del mismo tipo). 
    ' La información es extraída de la cinta testigo digital -CTD- (o memoria de auditoría).
    ' Información de interés para el software y/o el contribuyente.
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información, accediendo a la interfaz web de la IFH 2G. 
    '====================================================================================================================================================
    Private Sub ButtonRepDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRepDoc.Click
        Dim bloque1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueDocumento
        Dim bloque2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueDocumento
        Dim res As Microsoft.VisualBasic.MsgBoxResult
        Dim doc As hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante
        Dim comp As hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion
        Dim equisml As hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada
        Dim msj As String                                                           '// A mostrar en MsgBox()
        Dim pp As Byte()                                                            '// Almacenamiento de ASCII85 decodificado a binario
        Dim ja As Ascii85 = New Ascii85                                             '// Instancia de la clase de decodificación
        Dim fileReader As String                                                    '// Para leer el archivo ".a85"
        Dim oFileStream As System.IO.FileStream                                     '// Para generar el ZIP
        Dim arch As String                                                          '// Auxiliar
        Dim archzip As String = "Comprobantes.zip"                                  '// Resultado final de la descarga. Opción por defecto.
        Dim arch85 As String = "Comprobantes.a85"                                   '// Archivo intermedio. Contenido en ASCII85.
        Dim archxml As String = "Comprobantes.xml"                                  '// Resultado final de la descarga. Opción no disponible.
        Dim abData() As Byte                                                        '// Para encoding original
        Dim origen As Encoding = Encoding.UTF8                                      '// Encoding original para descargar XML
        Dim destino As Encoding = Encoding.GetEncoding(28591)                       '// ISO-8859-1, XML generado
        Dim newData() As Byte                                                       '// Para encoding final

        Try
            If (System.IO.File.Exists(archxml) Or System.IO.File.Exists(archzip) Or System.IO.File.Exists(arch85)) Then
                msj = "REPORTE COMPROBANTES : " & vbCrLf & vbCrLf & _
                  "Se encontraron archivos de descargas anteriores ..." & vbCrLf & _
                  "Si continúa se perderá dicha información ..." & vbCrLf & vbCrLf & _
                  "¿ Desea continuar ?"
                res = MsgBox(msj, vbOKCancel + vbQuestion, "Descarga: REPORTES COMPROBANTES ...")

                If res = vbCancel Then
                    NuevaDescargaComprobantes()
                    Exit Sub
                End If

                If (System.IO.File.Exists(archxml)) Then My.Computer.FileSystem.DeleteFile(archxml)
                If (System.IO.File.Exists(archzip)) Then My.Computer.FileSystem.DeleteFile(archzip)
                If (System.IO.File.Exists(arch85)) Then My.Computer.FileSystem.DeleteFile(arch85)
            End If

            TextBoxMens.Clear()
            msj = "REPORTE COMPROBANTES : " & vbCrLf & vbCrLf & _
                  "A descargar información ..." & vbCrLf & vbCrLf & _
                  "Si acepta, ... POR FAVOR ESPERE ..." & vbCrLf & _
                  "Esta operación puede durar algunos minutos ..."
            res = MsgBox(msj, vbOKCancel + vbInformation, "Descarga: REPORTES COMPROBANTES ...")

            If (res = vbCancel) Then
                MsgBox("DESCARGA CANCELADA ! ...", vbOKOnly + vbInformation, "Descarga: REPORTES COMPROBANTES ...")
                NuevaDescargaComprobantes()
                Exit Sub
            End If

            If ((Not IsNumeric(TextBoxDupDesde.Text)) Or (Not IsNumeric(TextBoxDupHasta.Text))) Then
                MsgBox("RANGO NUMÉRICO INCORRECTO ! ..." & vbCrLf & "Por favor, verifique que ambos datos sean numéricos ...", vbOKOnly + vbCritical, "Descarga: REPORTES COMPROBANTES ...")
                NuevaDescargaComprobantes()
                Exit Sub
            End If

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            If (CheckBoxDupComprimir.Checked) Then
                comp = hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion.COMPRIME
                arch = arch85                                                '// Donde se almacenará la descarga de información en ASCII85.
                LabelRepDup.Text = "Guardando en: " & archzip
            Else
                comp = hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion.NO_COMPRIME
                arch = archxml
                LabelRepDup.Text = "Guardando en: " & archxml
            End If

            If (CheckBoxDupXML.Checked) Then
                equisml = hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada.XML_UNICO
            Else
                equisml = hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada.VARIOS_XMLS
            End If

            Select Case ComboBoxDupDoc.SelectedIndex
                Case 0
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.COMPROBANTE_DONACION
                Case 1
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO
                Case 2
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.DETALLE_DE_VENTAS
                Case 3
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A
                Case 4
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B
                Case 5
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_C
                Case 6
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_M
                Case 7
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.INFORME_DE_AUDITORIA
                Case 8
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.INFORME_DIARIO_DE_CIERRE
                Case 9
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_A
                Case 10
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B
                Case 11
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_C
                Case 12
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_M
                Case 13
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_A
                Case 14
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_B
                Case 15
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_C
                Case 16
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_M
                Case 17
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.PRESUPUESTO_X
                Case 18
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_A
                Case 19
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_B
                Case 20
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_C
                Case 21
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_M
                Case 22
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_X
                Case 23
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.REMITO_R
                Case 24
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.REMITO_X
                Case 25
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE
                Case 26
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_A
                Case 27
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_B
                Case 28
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_C
                Case 29
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_M
                Case 30
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO
                Case 31
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A
                Case 32
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B
                Case 33
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_C
                Case 34
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_M
                Case 35
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_A
                Case 36
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_B
                Case 37
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_C
                Case 38
                    doc = hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_M
            End Select

            '// Se descarga el primer bloque de información.
            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - REPORTE DOCUMENTOS" & vbCrLf)
            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - Tipo: " & doc.ToString & " --> Desde: " & TextBoxDupDesde.Text & " Hasta: " & TextBoxDupHasta.Text & vbCrLf)
            bloque1 = hasar.ObtenerPrimerBloqueDocumento(Val(TextBoxDupDesde.Text), Val(TextBoxDupHasta.Text), doc, comp, equisml)

            If (bloque1.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                msj = "Descarga: REPORTE COMPROBANTES ..." & vbCrLf & vbCrLf & _
                      "No hay información disponible ! ..."
                MsgBox(msj, vbOKOnly + vbInformation, "Descarga: REPORTE COMPROBANTES ...")

                NuevaDescargaComprobantes()
                Me.Cursor = System.Windows.Forms.Cursors.Arrow
                Exit Sub
            End If

            '// Se almacena la información descargada, del primer bloque.
            TextBoxMens.Text = bloque1.getInformacion()
            GrabarInfoBin(arch, bloque1.getInformacion())

            '// Se obtiene el segundo bloque de información.
            bloque2 = hasar.ObtenerSiguienteBloqueDocumento()

            While (bloque2.getRegistro() <> hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                '// Se almacena la información descargada.
                TextBoxMens.Text = bloque2.getInformacion()
                GrabarInfoBin(arch, bloque2.getInformacion())

                '// Se obtiene el próximo bloque de información.
                bloque2 = hasar.ObtenerSiguienteBloqueDocumento()
                Application.DoEvents()
            End While

            If (bloque2.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                '// Se almacena el último bloque de información.
                TextBoxMens.Text = bloque2.getInformacion()
                GrabarInfoBin(arch, bloque2.getInformacion())
            End If

            TextBoxMens.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            msj = "REPORTE COMPROBANTES : " & vbCrLf & vbCrLf & _
                  "Descarga finalizada ! ..."
            MsgBox(msj, vbOKOnly + vbInformation, "Descarga: REPORTE COMPROBANTES ...")

            If (CheckBoxDupComprimir.Checked) Then
                fileReader = My.Computer.FileSystem.ReadAllText(arch85)

                pp = ja.Decode(fileReader)     '// Convierte ASCII85 a binario

                '// Se genera el ZIP con la decodificación
                oFileStream = New System.IO.FileStream(archzip, System.IO.FileMode.Create)
                oFileStream.Write(pp, 0, pp.Length)
                oFileStream.Close()
            End If

            flag = True
            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - Descarga OK ..." & vbCrLf & vbCrLf)
            NuevaDescargaComprobantes()
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            msj = "Codigo: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Descarga: REPORTE COMPROBANTES ...")

            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & msj & vbCrLf & vbCrLf)
            NuevaDescargaComprobantes()

            If (Err.Number = 5) Then
                TextBoxIP1.Focus()
            End If
        End Try
    End Sub

    '====================================================================================================================================================
    ' BOTÓN: CINTA TESTIGO DIGITAL - Descarga ...
    '
    ' La descarga permite generar un archivo ZIP con el detalle del contenido de la cinta testigo digital (CTD). 
    ' Información de interés para el software y/o el contribuyente.
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información accediendo a la interfaz web de la IFH 2G. 
    '====================================================================================================================================================
    Private Sub ButtonCTD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCTD.Click
        Dim bloque1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueAuditoria
        Dim bloque2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueAuditoria
        Dim comp As hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion
        Dim equisml As hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada
        Dim zetapor As hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteZ
        Dim res As Microsoft.VisualBasic.MsgBoxResult
        Dim msj As String                                                         '// A mostrar en MsgBox()
        Dim arch As String                                                        '// Archivo donde almacenar las descarga.
        Dim fileReader As String                                                  '// Para leer el archivo ".a85"
        Dim oFileStream As System.IO.FileStream                                   '// Para generar el ZIP
        Dim archzip As String = "MemAudit.zip"                                    '// Resultado final de la descarga. Opción por defecto.
        Dim arch85 As String = "Memaudit.a85"                                     '// Archivo intermedio. Contenido en ASCII85.
        Dim archxml As String = "Memaudit.xml"                                    '// Resultado final de la descarga. Opción no disponible.
        Dim pp As Byte()                                                          '// Almacenamiento de ASCII85 decodificado a binario
        Dim ja As Ascii85 = New Ascii85                                           '// Instancia de la clase de decodificación
        Dim desde As String                                                       '// Comienzo del rango de selección
        Dim hasta As String                                                       '// Final del rango de selección

        Try
            If (System.IO.File.Exists(archzip) Or System.IO.File.Exists(arch85) Or System.IO.File.Exists(archxml)) Then
                msj = "Descarga CTD : " & vbCrLf & vbCrLf & _
                  "Se encontraron archivos de descargas anteriores ..." & vbCrLf & _
                  "Si continúa se perderá dicha información ..." & vbCrLf & vbCrLf & _
                  "¿ Desea continuar ?"
                res = MsgBox(msj, vbOKCancel + vbQuestion, "Descarga: CTD ...")

                If res = vbCancel Then
                    NuevaDescargaReportesAFIP()
                    Exit Sub
                End If

                If (System.IO.File.Exists(archzip)) Then My.Computer.FileSystem.DeleteFile(archzip)
                If (System.IO.File.Exists(arch85)) Then My.Computer.FileSystem.DeleteFile(arch85)
                If (System.IO.File.Exists(archxml)) Then My.Computer.FileSystem.DeleteFile(archxml)
            End If

            TextBoxMens.Clear()

            msj = "CTD (Cinta Testigo Digital) : " & vbCrLf & vbCrLf & _
                  "A descargar información ..." & vbCrLf & vbCrLf & _
                  "Si acepta, ... POR FAVOR ESPERE ..." & vbCrLf & _
                  "Esta operación puede durar algunos minutos ..."
            res = MsgBox(msj, vbOKCancel + vbInformation, "Descarga: CTD ...")

            If (res = vbCancel) Then
                MsgBox("DESCARGA CANCELADA ! ...", vbOKOnly + vbInformation, "Descarga: CTD ...")
                NuevaDescargaCTD()
                Exit Sub
            End If

            If (RadioButtonCTDNro.Checked) Then
                If ((Not IsNumeric(TextBoxCTDDesdeNro.Text)) Or (Not IsNumeric(TextBoxCTDHastaNro.Text))) Then
                    MsgBox("RANGO NUMÉRICO INCORRECTO ! ..." & vbCrLf & "Por favor, verifique que ambos datos sean numéricos ...", vbOKOnly + vbCritical, "Descarga: REPORTES COMPROBANTES ...")
                    NuevaDescargaCTD()
                    Exit Sub
                End If
            Else
                desde = TextBoxCTDDDDesde.Text & "/" & TextBoxCTDMMDesde.Text & "/" & TextBoxCTDAADesde.Text
                hasta = TextBoxCTDDDhasta.Text & "/" & TextBoxCTDMMHasta.Text & "/" & TextBoxCTDAAHasta.Text

                If ((Not (IsDate(desde))) Or (Not (IsDate(hasta)))) Then
                    MsgBox("RANGO DE FECHAS INVÁLIDO ! ..." & vbCrLf & "Por favor, verifique que ambas fechas sean correctas ...", vbOKOnly + vbCritical, "Descarga: REPORTES ELECTRÓNICOS AFIP ...")
                    NuevaDescargaCTD()
                    Exit Sub
                End If
            End If

            If (CheckBoxCTDComprimir.Checked) Then
                comp = hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion.COMPRIME
                arch = arch85
                LabelCTD.Text = "Guardando en: " & archzip
            Else
                comp = hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion.NO_COMPRIME
                arch = archxml
                LabelCTD.Text = "Guardando en: " & archxml
            End If

            If (CheckBoxCTDXML.Checked) Then
                equisml = hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada.XML_UNICO
            Else
                equisml = hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada.VARIOS_XMLS
            End If

            If (RadioButtonCTDFecha.Checked) Then
                zetapor = hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteZ.REPORTE_Z_FECHA
                desde = Mid$(TextBoxCTDAADesde.Text, 3, 2) & TextBoxCTDMMDesde.Text & TextBoxCTDDDDesde.Text
                hasta = Mid$(TextBoxCTDAAHasta.Text, 3, 2) & TextBoxCTDMMHasta.Text & TextBoxCTDDDhasta.Text
            Else
                zetapor = hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteZ.REPORTE_Z_NUMERO
                desde = TextBoxCTDDesdeNro.Text
                hasta = TextBoxCTDHastaNro.Text
            End If

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - CINTA TESTIGO DIGITAL (CTD)" & vbCrLf)

            If (RadioButtonCTDFecha.Checked) Then
                GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - POR RANGO DE FECHAS" & vbCrLf)
            Else
                GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - POR RANGO NUMÉRICO" & vbCrLf)
            End If

            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - Desde: " & desde & " Hasta: " & hasta & vbCrLf)
            bloque1 = hasar.ObtenerPrimerBloqueAuditoria(desde, hasta, zetapor, comp, equisml)

            If (bloque1.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                msj = "CTD (Cinta testigo Digital) ..." & vbCrLf & vbCrLf & _
                      "No hay información disponible ! ..."
                MsgBox(msj, vbOKOnly + vbInformation, "Descarga: CTD ...")

                NuevaDescargaCTD()
                Me.Cursor = System.Windows.Forms.Cursors.Arrow
                Exit Sub
            End If

            TextBoxMens.Text = bloque1.getInformacion()
            GrabarInfoBin(arch, bloque1.getInformacion())
            bloque2 = hasar.ObtenerSiguienteBloqueAuditoria()

            While (bloque2.getRegistro() <> hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                TextBoxMens.Text = bloque2.getInformacion()
                GrabarInfoBin(arch, bloque2.getInformacion())
                bloque2 = hasar.ObtenerSiguienteBloqueAuditoria()
                Application.DoEvents()
            End While

            If (bloque2.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                TextBoxMens.Text = bloque2.getInformacion()
                GrabarInfoBin(arch, bloque2.getInformacion())
            End If

            TextBoxMens.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            msj = "CTD (Cinta Testigo Digital) : " & vbCrLf & vbCrLf & _
                  "Descarga finalizada ! ..."
            MsgBox(msj, vbOKOnly + vbInformation, "Descarga: CTD ...")

            If (CheckBoxCTDComprimir.Checked) Then
                fileReader = My.Computer.FileSystem.ReadAllText(arch85)
                pp = ja.Decode(fileReader)                '// Convierte ASCII85 a binario
                oFileStream = New System.IO.FileStream(archzip, System.IO.FileMode.Create)
                oFileStream.Write(pp, 0, pp.Length)
                oFileStream.Close()
            End If

            flag = True
            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - Descarga OK ..." & vbCrLf & vbCrLf)
            NuevaDescargaCTD()
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Descarga: CTD ...")

            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & msj & vbCrLf & vbCrLf)
            NuevaDescargaCTD()

            If (Err.Number = 5) Then
                TextBoxIP1.Focus()
            End If
        End Try
    End Sub

    '====================================================================================================================================================
    ' BOTÓN: LOG INTERNO - Descarga ...
    '
    ' La descarga permite generar un archivo con el contenido del registro interno de actividades de la IFH 2G.
    ' Sólo de interés para el servicio técnico fiscal autorizado.
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información accediendo a la interfaz web de la IFH 2G. 
    '====================================================================================================================================================
    Private Sub ButtonLog_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonLog.Click
        Dim bloque1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueLog
        Dim bloque2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueLog
        Dim res As Microsoft.VisualBasic.MsgBoxResult
        Dim msj As String                                          '// A mostrar en MsgBox()
        Dim arch As String = "LogIFH2G.txt"                        '// Archivo donde almacenar la información a descargar.

        Try
            If (System.IO.File.Exists(arch)) Then
                msj = "LOG INTERNO IFH 2G : " & vbCrLf & vbCrLf & _
                      "Se encontró archivo de una descarga anterior ..." & vbCrLf & _
                      "Si continúa se perderá dicha información ..." & vbCrLf & vbCrLf & _
                       "¿ Desea continuar ?"
                res = MsgBox(msj, vbOKCancel + vbQuestion, "Descarga: LOG INTERNO IFH 2G ...")

                If res = vbCancel Then
                    pos = 1
                    LabelLogIFH.Text = ""
                    Exit Sub
                End If

                My.Computer.FileSystem.DeleteFile(arch)
            End If

            msj = "LOG INTERNO IFH 2G : " & vbCrLf & vbCrLf & _
                  "A descargar información ..." & vbCrLf & vbCrLf & _
                  "Si acepta, ... POR FAVOR ESPERE ..." & vbCrLf & _
                  "Esta operación puede durar algunos minutos ..."
            res = MsgBox(msj, vbOKCancel + vbInformation, "Descarga: LOG INTERNO IFH 2G ...")

            If (res = vbCancel) Then
                MsgBox("DESCARGA CANCELADA ! ...", vbOKOnly + vbInformation, "Descarga: LOG INTERNO IFH 2G ...")
                pos = 1
                LabelLogIFH.Text = ""
                Exit Sub
            End If

            LabelLogIFH.Text = "Guardando en: " & arch
            TextBoxMens.Clear()

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - LOG INTERNO IFH 2G" & vbCrLf)
            bloque1 = hasar.ObtenerPrimerBloqueLog()

            If (bloque1.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                msj = "LOG INTERNO IFH 2G ..." & vbCrLf & vbCrLf & _
                      "No hay información disponible ! ..."
                MsgBox(msj, vbOKOnly + vbInformation, "Descarga: LOG INTERNO IFH 2G ...")

                Me.Cursor = System.Windows.Forms.Cursors.Arrow
                pos = 1
                LabelLogIFH.Text = ""
                Exit Sub
            End If

            TextBoxMens.Text = bloque1.getInformacion()
            GrabarInfoBin(arch, bloque1.getInformacion())
            bloque2 = hasar.ObtenerSiguienteBloqueLog()

            While (bloque2.getRegistro() <> hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                TextBoxMens.Text = bloque2.getInformacion()
                GrabarInfoBin(arch, bloque2.getInformacion())
                bloque2 = hasar.ObtenerSiguienteBloqueLog()
                Application.DoEvents()
            End While

            If (bloque2.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                TextBoxMens.Text = bloque2.getInformacion()
                GrabarInfoBin(arch, bloque2.getInformacion())
            End If

            TextBoxMens.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "LOG INTERNO IFH 2G : " & vbCrLf & vbCrLf & _
                  "Descarga finalizada ! ..."
            MsgBox(msj, vbOKOnly + vbInformation, "Descarga: LOG INTERNO IFH 2G ...")

            pos = 1                        '// Para volver al primer byte del archivo
            LabelLogIFH.Text = ""
            flag = True
            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & " - Descarga OK ..." & vbCrLf & vbCrLf)
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Descarga: LOG INTERNO IFH 2G ...")

            pos = 1
            LabelLogIFH.Text = ""
            GrabarLog(archrep, Format(Now, "dd MMM yyyy") & " , " & TimeString & msj & vbCrLf & vbCrLf)

            If (Err.Number = 5) Then
                TextBoxIP1.Focus()
                TextBoxMens.Clear()
            End If
        End Try
    End Sub

    '====================================================================================================================================================
    ' Se opta por la descarga completa  de los Reportes Electrónicos AFIP ...
    '====================================================================================================================================================
    Private Sub RadioButtonAfipCompleto_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButtonAfipCompleto.Click

        RadioButtonAfipCompleto.Checked = True
        RadioButtonAfipMF.Checked = False
        TextBoxDDAfipDesde.Focus()

    End Sub

    '====================================================================================================================================================
    ' Se opta por la descarga, únicamente, del Reporte de Totales (memoria fiscal) correspondiente a los Reportes Electrónicos AFIP ...
    '====================================================================================================================================================
    Private Sub RadioButtonAfipMF_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButtonAfipMF.Click

        RadioButtonAfipCompleto.Checked = False
        RadioButtonAfipMF.Checked = True
        TextBoxDDAfipDesde.Focus()

    End Sub

    '====================================================================================================================================================
    ' CTD: Se descargará la información seleccionando los cierres por número ...
    '====================================================================================================================================================
    Private Sub RadioButtonCTDNro_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButtonCTDNro.Click

        RadioButtonCTDNro.Checked = True
        RadioButtonCTDFecha.Checked = False

        TextBoxCTDAADesde.Enabled = False
        TextBoxCTDAAHasta.Enabled = False
        TextBoxCTDDDDesde.Enabled = False
        TextBoxCTDDDhasta.Enabled = False
        TextBoxCTDMMDesde.Enabled = False
        TextBoxCTDMMHasta.Enabled = False

        TextBoxCTDDesdeNro.Enabled = True
        TextBoxCTDHastaNro.Enabled = True

        TextBoxCTDDesdeNro.Focus()

    End Sub

    '====================================================================================================================================================
    ' CTD: Se descargará la información seleccionando los cierres por fecha ...
    '====================================================================================================================================================
    Private Sub RadioButtonCTDFecha_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButtonCTDFecha.Click

        RadioButtonCTDNro.Checked = False
        RadioButtonCTDFecha.Checked = True

        TextBoxCTDAADesde.Enabled = True
        TextBoxCTDAAHasta.Enabled = True
        TextBoxCTDDDDesde.Enabled = True
        TextBoxCTDDDhasta.Enabled = True
        TextBoxCTDMMDesde.Enabled = True
        TextBoxCTDMMHasta.Enabled = True

        TextBoxCTDDesdeNro.Enabled = False
        TextBoxCTDHastaNro.Enabled = False

        TextBoxCTDDDDesde.Focus()

    End Sub

    '====================================================================================================================================================
    ' Se prepara la aplicación para una nueva descarga de Reportes Electrónicos AFIP ...
    '====================================================================================================================================================
    Private Sub NuevaDescargaReportesAFIP()

        pos = 1                                                   '// Para volver al comienzo del archivo de descarga
        LabelRepElect.Text = ""
        TextBoxDDAfipDesde.Clear()
        TextBoxMMAfipDesde.Clear()
        TextBoxAAAfipDesde.Clear()
        TextBoxDDAfipHasta.Clear()
        TextBoxMMAfipHasta.Clear()
        TextBoxAAAfipHasta.Clear()
        RadioButtonAfipCompleto.Checked = True
        RadioButtonAfipMF.Checked = False
        TextBoxDDAfipDesde.Focus()
        TextBoxMens.Clear()

    End Sub

    '====================================================================================================================================================
    ' Se prepara la aplicación para una nueva descarga de Reporte de Comprobantes ...
    '====================================================================================================================================================
    Private Sub NuevaDescargaComprobantes()

        pos = 1                                                     '// Para volver al primer byte del archivo de descarga
        LabelRepDup.Text = ""
        ComboBoxDupDoc.SelectedIndex = 3
        TextBoxDupDesde.Clear()
        TextBoxDupHasta.Clear()
        CheckBoxDupComprimir.Checked = True
        CheckBoxDupXML.Checked = False
        TextBoxDupDesde.Focus()
        TextBoxMens.Clear()

    End Sub

    '====================================================================================================================================================
    ' Se prepara la aplicación para una nueva descarga de Cinta Testigo Digital ...
    '====================================================================================================================================================
    Private Sub NuevaDescargaCTD()

        pos = 1                                                    '// Para volver al primer byte del archivo de descarga
        RadioButtonCTDNro.Checked = True
        RadioButtonCTDFecha.Checked = False
        TextBoxCTDAADesde.Clear()
        TextBoxCTDAAHasta.Clear()
        TextBoxCTDDDDesde.Clear()
        TextBoxCTDDDhasta.Clear()
        TextBoxCTDMMDesde.Clear()
        TextBoxCTDMMHasta.Clear()
        TextBoxCTDDesdeNro.Clear()
        TextBoxCTDHastaNro.Clear()
        TextBoxCTDAADesde.Enabled = False
        TextBoxCTDAAHasta.Enabled = False
        TextBoxCTDDDDesde.Enabled = False
        TextBoxCTDDDhasta.Enabled = False
        TextBoxCTDMMDesde.Enabled = False
        TextBoxCTDMMHasta.Enabled = False
        TextBoxCTDDesdeNro.Enabled = True
        TextBoxCTDHastaNro.Enabled = True
        LabelCTD.Text = ""
        CheckBoxCTDComprimir.Checked = True
        CheckBoxCTDXML.Checked = False
        TextBoxCTDDesdeNro.Focus()
        TextBoxMens.Clear()

    End Sub
End Class
