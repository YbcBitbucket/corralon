﻿'//###################################################################################################################################################
'// Impresoras Fiscales HASAR (IFH) Segunda Generación (2G)                                 Dto. Software de Base, Compañía HASAR SAIC, Grupo HASAR
'// RG AFIP Nº 3561/13 , Controladores Fiscales Nueva Tecnología                            HasarLibreriaFiscal3561.dll, Protocolo '0', Revisión '103'
'// Argentina, Abril 2016                                                                   VB.Net, Visual Studio 2010, Ejemplo orientativo
'//
'// Ejemplo de diálogo con el EFH (Emulador Fiscal HASAR), usando Ethernet.
'// Modficar la dirección IP como argumento del método Conectar() para dialogar con una IFH 2G física.
'//
'// HASAR no asume responsabilidad alguna por las consecuencias del uso de este código, tal y como está, o luego de modificaciones hechas por los 
'// programadores.
'// HASAR se reserva el derecho de modificar, sin previo aviso, la codificación de este ejemplo.
'//###################################################################################################################################################
Option Explicit On

Imports System
Imports System.Globalization
Imports System.Text
Imports System.IO

'=====================================================================================================================================================
' Nace con todos los botones deshabilitados, a excepción de "Conectar" y "Terminar". Ello es para acostumbrarse a que no se puede enviar el primer co-
' mando, sin haber ejecutado previamente "Conectar()".
' Luego de presionar el botón "Conectar" se habilita el uso del resto de los botones.
'=====================================================================================================================================================
Public Class FormIFH2G
    Private WithEvents hasar As hfl.argentina.HasarImpresoraFiscalRG3561 = New hfl.argentina.HasarImpresoraFiscalRG3561
    Dim estilo As hfl.argentina.Hasar_Funcs.AtributosDeTexto = New hfl.argentina.Hasar_Funcs.AtributosDeTexto
    Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion
    Dim pos As Long = 1   '// Para manejar posiciones de archivos en lectura/escritua

    '==================================================================================================================================================
    ' BOTÓN: "CONECTAR" - Establecer conexión con la IFH 2G ...
    '==================================================================================================================================================
    Private Sub ButtonConectarse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonConectarse.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion
        Dim msj As String   '// A mostrar con MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            msj = "Registrando actividad en archivo: 'DLL_IFH2G.log' ..." & vbCrLf & vbCrLf
            hasar.archivoRegistro("DLL_IFH2G.log")

            '// Valores por defecto. Se muestran las sentencias por si fuese necesario modificar dichos valores
            '// hasar.establecerTiempoDeEspera(10000)
            '// hasar.establecerTiempoDeEsperaLecturaEscritura(15000)
            hasar.establecerTiempoDeEspera(30000)
            hasar.establecerTiempoDeEsperaLecturaEscritura(35000)

            '// 127.0.0.1 = Emulador Fiscal HASAR , otro valor IFH 2G física
            '// A la salida de fábrica: 192.168.1.1 , modificar antes de intentar conexión.
            msj = msj & "VERIFICADA LA CONEXIÓN CON IFH 2G / EFH ..." & vbCrLf
            'msj = msj & "ETHERNET - Conectado a ..." & vbCrLf
            'msj = msj & "Impresora fiscal 2G ..." & vbCrLf
            hasar.conectar("10.0.7.69")
            msj = msj & "Saliendo vía Proxy Fiscal y Puerto Serie ..." & vbCrLf
            'hasar.conectar("127.0.0.1")

            msj = msj & "CONEXIÓN OK ! ..." & vbCrLf & vbCrLf & _
                  "DLL 2G Protocolo" & vbTab & "=[" & hasar.ObtenerVersionProtocolo() & "]" & vbCrLf & _
                  "DLL 2G Revisión" & vbTab & "=[" & hasar.ObtenerRevision() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "DEMO IFH 2G - Iniciando tareas ...")
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarVersion()

            '// No dejar de considerar este If
            'If (resp.getVersionProtocolo() > hasar.ObtenerVersionProtocolo()) Then
            '    msj = "DLL INCOMPATIBLE CON IFH 2G" & vbCrLf & vbCrLf & _
            '          "DLL 2G Protocolo" & vbTab & "=[" & hasar.ObtenerVersionProtocolo() & "]" & vbCrLf & _
            '          "IFH 2G Protocolo" & vbTab & "=[" & resp.getVersionProtocolo() & "]" & vbCrLf & vbCrLf & _
            '          "Luego de ACEPTAR ..." & vbCrLf & _
            '          "Haga click en el botón TERMINAR ..."
            '    MsgBox(msj, vbOKOnly + vbCritical, "ERROR compatibilidad ...")
            '    Exit Sub
            'End If

            '// Habilitación de los botones que nacen deshabilitados
            Me.GroupConsultas.Enabled = True
            Me.GroupDF.Enabled = True
            Me.GroupBox1.Enabled = True
            Me.GroupReportes.Enabled = True
            Me.GroupDNFH.Enabled = True
            Me.GroupBox2.Enabled = True
            Me.GroupMiscel.Enabled = True
            Me.ButtonCancel.Enabled = True
            Me.ButtonConectarse.Enabled = False
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description & vbCrLf & vbCrLf & _
                  "Luego de ACEPTAR ..." & vbCrLf & _
                  "Haga click en el botón TERMINAR ..."
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR conexión...")
        End Try
    End Sub

    '====================================================================================================================================================
    ' BOTÓN: "TERMINAR" - Finalizar la ejecución del software ...
    '====================================================================================================================================================
    Private Sub ButtonTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTerminar.Click
        Dim p As System.ComponentModel.CancelEventArgs = New System.ComponentModel.CancelEventArgs

        p.Cancel = True
        FormIFH2G_Closing(sender, p)

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: "CANCELAR" - Cancelación de un comprobante abierto ...
    '====================================================================================================================================================
    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Dim stfiscal As hfl.argentina.Estados_Fiscales_RG3561.EstadoFiscal
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado
        Dim msj As String    '// A mostrar con MsgBox

        resp = hasar.ConsultarEstado()
        stfiscal = hasar.ObtenerUltimoEstadoFiscal

        If (Not (stfiscal.getDocumentoAbierto())) Then
            msj = "CANCELACIÓN DE COMPROBANTE :" & vbCrLf & vbCrLf & _
                  "No hay un comprobante abierto para cancelar ..."
            MsgBox(msj, vbOKOnly + vbCritical, "Comando: Cancelar() ...")
            Exit Sub
        End If

        Try
            hasar.Cancelar()
            msj = "CANCELACIÓN DE COMPROBANTE :" & vbCrLf & vbCrLf & _
                  "Comprobante encontrado abierto: fue CANCELADO ..."
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: Cancelar() ...")
        Catch
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Cancelando comprobante ...")
        End Try

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE - Impresión de un comprobante tipo '83' ...
    '====================================================================================================================================================
    Private Sub ButtonTique_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTique.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String    '// A mostrar on MsgBox()
        Dim item As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirItem

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.INTERLINEADO, "N")
            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE)
            msj = "Tique abierto Nº  =[" & respabrir.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            item = hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")
            msj = "TIQUE - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique cerrado Nº  =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE ...")
        End Try

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/FACTURA 'A' - Impresión de un comprobante tipo '81' ...
    '====================================================================================================================================================
    Private Sub Button26TiqueFactA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button26TiqueFactA.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String      '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_I2OF5, "779123456789123456", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")
            hasar.CargarDocumentoAsociado(1, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO, 1, 123)
            hasar.CargarDocumentoAsociado(2, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO, 1, 124)

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_A)
            msj = "Tique/Factura 'A' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.KILO)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/FACTURA 'A' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/FACTURA 'A' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Factura 'A' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'A' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/FACTURA 'A' ...")
        End Try
    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/FACTURA 'B' - Impresión de un comprobante tipo '82' ...
    '====================================================================================================================================================
    Private Sub ButtonTiqueFactB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTiqueFactB.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String     '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.CONSUMIDOR_FINAL, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_B)
            msj = "Tique/Factura 'B' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/FACTURA 'B' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/FACTURA 'B' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Factura 'B' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'B' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/FACTURA 'B' ...")
        End Try

    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE/FACTURA 'C' - Impresión de un comprobante tipo '111' ...
    '============================================================================================================================
    Private Sub ButtonTiqueFactC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTiqueFactC.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String      '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.MONOTRIBUTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")
            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_C)
            msj = "Tique/Factura 'C' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/FACTURA 'C' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/FACTURA 'C' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Factura 'C' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'C' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/FACTURA 'C' ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: INFORME DIARIO DE CIERRE 'Z' - Impresión de un comprobante tipo '80' ...
    '============================================================================================================================
    Private Sub ButtonRepZeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRepZeta.Click
        Dim cierre As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal
        Dim zeta As hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalZ
        Dim msj As String          '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z)
            zeta = cierre.Z

            msj = "INFORME DIARIO DE CIERRE :" & vbCrLf & vbCrLf & _
                  "Cierre 'Z' Nº " & vbTab & vbTab & "=[" & zeta.getNumero() & "]" & vbCrLf & _
                  "Fecha del Cierre" & vbTab & vbTab & "=[" & zeta.getFecha() & "]" & vbCrLf & vbCrLf & _
                  "DF Emitidos" & vbTab & vbTab & "=[" & zeta.getDF_CantidadEmitidos() & "]" & vbCrLf & _
                  "DF Cancelados" & vbTab & vbTab & "=[" & zeta.getDF_CantidadCancelados() & "]" & vbCrLf & vbCrLf & _
                  "DF Total" & vbTab & vbTab & vbTab & "=[" & zeta.getDF_Total() & "]" & vbCrLf & vbCrLf & _
                  "DF Total Gravado" & vbTab & vbTab & "=[" & zeta.getDF_TotalGravado() & "]" & vbCrLf & _
                  "DF Total No Gravado" & vbTab & vbTab & "=[" & zeta.getDF_TotalNoGravado() & "]" & vbCrLf & _
                  "DF Total Exento" & vbTab & vbTab & "=[" & zeta.getDF_TotalExento() & "]" & vbCrLf & _
                  "DF Total IVA" & vbTab & vbTab & "=[" & zeta.getDF_TotalIVA() & "]" & vbCrLf & _
                  "DF Total Otros Tributos" & vbTab & "=[" & zeta.getDF_TotalTributos() & "]" & vbCrLf & vbCrLf & _
                  "NC Emitidas" & vbTab & vbTab & "=[" & zeta.getNC_CantidadEmitidos() & "]" & vbCrLf & _
                  "NC Canceladas" & vbTab & vbTab & "=[" & zeta.getNC_CantidadCancelados() & "]" & vbCrLf & vbCrLf & _
                  "NC Total" & vbTab & vbTab & vbTab & "=[" & zeta.getNC_Total() & "]" & vbCrLf & vbCrLf & _
                  "NC Total Gravado" & vbTab & vbTab & "=[" & zeta.getNC_TotalGravado() & "]" & vbCrLf & _
                  "NC Total No Gravado" & vbTab & "=[" & zeta.getNC_TotalNoGravado() & "]" & vbCrLf & _
                  "NC Total Exento" & vbTab & vbTab & "=[" & zeta.getNC_TotalExento() & "]" & vbCrLf & _
                  "NC Total IVA" & vbTab & vbTab & "=[" & zeta.getNC_TotalIVA() & "]" & vbCrLf & _
                  "NC Total Otros Tributos" & vbTab & "=[" & zeta.getNC_TotalTributos() & "]" & vbCrLf & vbCrLf & _
                  "DNFH Emitidos" & vbTab & vbTab & "=[" & zeta.getDNFH_CantidadEmitidos() & "]" & vbCrLf & _
                  "DNFH Total" & vbTab & vbTab & "=[" & zeta.getDNFH_Total() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: CerrarJornadaFiscal()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR CerrarJornadaFiscal() ...")
        End Try
    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/NOTA DE DÉBITO 'A' - Impresión de un comprobante tipo '115' ...
    '====================================================================================================================================================
    Private Sub ButtonTNDA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNDA.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String     '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_A)
            msj = "Tique/Nota de Débito 'A' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/NOTA DE DÉBITO 'A' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/NOTA DE DÉBITO 'A' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Nota de Débito 'A' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'A' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/NOTA DE DÉBITO 'A' ...")
        End Try

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE NOTA DE CRÉDITO - Impresión de un comprobante tipo '110' ... A Consumidor Final - Opuesto al TIQUE
    '====================================================================================================================================================
    Private Sub ButtonTiqueNC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTiqueNC.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String   '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO)
            msj = "Tique Nota de Crédito abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE NOTA DE CRÉDITO ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE NOTA DE CRÉDITO - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE NOTA DE CRÉDITO ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE NOTA DE CRÉDITO - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE NOTA DE CRÉDITO ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique Nota de Crédito cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE NOTA DE CRÉDITO ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE NOTA DE CRÉDITO ...")
        End Try

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/NOTA DE CRÉDITO 'A' - Impresión de un comprobante tipo '112' ...
    '====================================================================================================================================================
    Private Sub ButtonTNCA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNCA.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String     '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A)
            msj = "Tique/Nota de Crédito 'A' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/NOTA DE CRÉDITO 'A' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/NOTA DE CRÉDITO 'A' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Nota de Crédito 'A' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'A' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/NOTA DE CRÉDITO 'A' ...")
        End Try

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/NOTA DE CRÉDITO 'B' - Impresión de un comprobante tipo '113' ...
    '====================================================================================================================================================
    Private Sub ButtonTNCB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNCB.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String    '// A mostrar con MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.MONOTRIBUTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B)
            msj = "Tique/Nota de Crédito 'B' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/NOTA DE CRÉDITO 'B' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/NOTA DE CRÉDITO 'B' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Nota de Crédito 'B' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'B' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/NOTA DE CRÉDITO 'B' ...")
        End Try

    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE/NOTA DE CRÉDITO 'C' - Impresión de un comprobante tipo '114' ...
    '============================================================================================================================
    Private Sub ButtonTNCC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNCC.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String      '// A mostrsr en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.MONOTRIBUTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")
            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_C)
            msj = "Tique/Nota de Crédito 'C' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/NOTA DE CRÉDITO 'C' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/NOTA DE CRÉDITO 'C' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Nota de Crédito 'C' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'C' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/NOTA DE CRÉDITO 'C' ...")
        End Try
    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/FACTURA 'M' - Impresión de un comprobante tipo '118' ...
    '====================================================================================================================================================
    Private Sub ButtonTiqueFactM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTiqueFactM.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String     '// A mostrar en MSgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_M)
            msj = "Tique/Factura 'M' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: FACTURA 'M' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/FACTURA 'M' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'M' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/FACTURA 'M' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'M' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Factura 'M' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/FACTURA 'M' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/FACTURA 'M' ...")
        End Try

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/NOTA DE CRÉDITO 'M' - Impresión de un comprobante tipo '119' ...
    '====================================================================================================================================================
    Private Sub ButtonTNCM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNCM.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String      '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_M)
            msj = "Tique/Nota de Crédito 'M' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: NOTA DE CRÉDITO 'M' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/NOTA DE CRÉDITO 'M' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'M' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/NOTA DE CRÉDITO 'M' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'M' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Notqa de Crédito 'M' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE CRÉDITO 'M' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/NOTA DE CRÉDITO 'M' ...")
        End Try

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/RECIBO 'A' - Impresión de un comprobante tipo '4' ...
    '====================================================================================================================================================
    Private Sub ButtonTRA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTRA.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String     '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_A)
            msj = "Tique/Recibo 'A' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 2.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Ajuste negativo ...", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.AJUSTE_NEG)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 241.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/RECIBO 'A' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/RECIBO 'A' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'A' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Recibo 'A' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'A' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/RECIBO 'A' ...")
        End Try

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/NOTA DE DÉBITO 'B' - Impresión de un comprobante tipo '116' ...
    '====================================================================================================================================================
    Private Sub ButtonTNDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNDB.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String      '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.MONOTRIBUTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_B)
            msj = "Tique/Nota de Débito 'B' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/NOTA DE DÉBITO 'B' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/NOTA DE DÉBITO 'B' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Nota de Débito 'B' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'B' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/NOTA DE DÉBITO 'B' ...")
        End Try

    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/RECIBO 'B' - Impresión de un comprobante tipo '9' ...
    '====================================================================================================================================================
    Private Sub ButtonTRB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTRB.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String     '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.Arrow
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.MONOTRIBUTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_B)
            msj = "Tique/Recibo 'B' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly, "Imprimiendo: TIQUE/RECIBO 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/RECIBO 'B' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/RECIBO 'B' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'B' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Recibo 'B' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'B' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/RECIBO 'B' ...")
        End Try

    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE/NOTA DE DÉBITO 'C' - Impresión de un comprobante tipo '117' ...
    '============================================================================================================================
    Private Sub ButtonTNDC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNDC.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String    '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.MONOTRIBUTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")
            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_C)
            msj = "Tique/Nota de Débito 'C' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/NOTA DE DÉBITO 'C' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/NOTA DE DÉBITO 'C' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Nota de Débito 'C' cerrado/a Nº =[" & respcerrar.getNumeroComprobante() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'C' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/NOTA DE DÉBITO 'C' ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE/RECIBO 'C' - Impresión de un comprobante tipo '15' ...
    '============================================================================================================================
    Private Sub ButtonTRC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTRC.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String     '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.MONOTRIBUTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")
            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_C)
            msj = "Tique/Recibo 'C' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/RECIBO 'C' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/RECIBO 'C' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'C' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Recibo 'C' cerrado/a Nº =[" & respcerrar.getNumeroComprobante() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'C' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/RECIBO 'C' ...")
        End Try
    End Sub

    '====================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/NOTA DE DÉBITO 'M' - Impresión de un comprobante tipo '120' ...
    '====================================================================================================================================================
    Private Sub ButtonTNDM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNDM.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String      '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_M)
            msj = "Tique/Nota de Débito 'M' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'M' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/NOTA DE DÉBITO 'M' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'M' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/NOTA DE DÉBITO 'M' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'M' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Nota de Débito 'M' cerrado/a Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/NOTA DE DÉBITO 'M' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/NOTA DE DÉBITO 'M' ...")
        End Try

    End Sub

    '===================================================================================================================================================
    ' BOTÓN: (DF) TIQUE/RECIBO 'M' - Impresión de un comprobante tipo '54' ...
    '===================================================================================================================================================
    Private Sub ButtonTRM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTRM.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String    '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_M)
            msj = "Tique/Recibo 'M' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'M' ...")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/RECIBO 'M' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'M' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/RECIBO 'M' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'M' ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Recibo 'M' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: TIQUE/RECIBO 'M' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/RECIBO 'M' ...")
        End Try

    End Sub

    '===================================================================================================================================================
    ' BOTÓN: (DF) DOCUMENTO PATRON - Impresión de un comprobante Patrón ...                         = NO MODIFICAR ESTA RUTINA =
    ' Comprobante establecido por la RG AFIP Nº 3561/13.
    '===================================================================================================================================================
    Private Sub ButtonPatron_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPatron.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String    '// A mostrar en MsgBox()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.ConfigurarZona(1, estilo, "Pasaje Ignoto 777 - (1408) Villa Real ..........................", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_DOMICILIO_EMISOR)
            hasar.ConfigurarZona(2, estilo, "Ciudad Autónoma de Buenos Aires ................................", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_DOMICILIO_EMISOR)
            hasar.ConfigurarZona(3, estilo, "República Argentina ............................................", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_DOMICILIO_EMISOR)
            hasar.ConfigurarZona(4, estilo, "América del Sur ................................................", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_DOMICILIO_EMISOR)
            hasar.CargarDatosCliente("El kioskazo de Villa Real ........................", "22222222226", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Avenida Julio Argentino Roca 852 - Local 9 - Entrepiso .........", "(1401) Ciudad Autónoma de Buenos Aires .........................", "República Argentina ............................................", "América del Sur ................................................")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_A)
            msj = "Doc. Patrón abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: DOC. PATRÓN ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Salta ....................................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("CARAMELOS UNO ....................", 2.5, 9.99, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 1.99, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.KILO)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Jujuy ....................................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("CARAMELOS DOS ....................", 2.5, 9.99, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 1.99, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500002", "00002", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.KILO)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Chaco ....................................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("CARAMELOS TRES ...................", 2.5, 9.99, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 1.99, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500003", "00003", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.KILO)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Formosa ..................................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("CARAMELOS CUATRO .................", 2.5, 9.99, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 1.99, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500004", "00004", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.KILO)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Tucumán ..................................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("CARAMELOS CINCO ..................", 2.5, 9.99, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 1.99, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500005", "00005", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.KILO)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Santiago del Estero ......................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("CARAMELOS SEIS ...................", 2.5, 9.99, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 1.99, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500006", "00006", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.KILO)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Catamarca ................................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("CARAMELOS SIETE ..................", 2.5, 9.99, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 1.99, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500007", "00007", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.KILO)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Origen: La Rioja .................................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("CARAMELOS OCHO ...................", 2.5, 9.99, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 1.99, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500008", "00008", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.KILO)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Mendoza ..................................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("CARAMELOS NUEVE ..................", 2.5, 9.99, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 1.99, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500009", "00009", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.KILO)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Origen: San Luis .................................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("CARAMELOS DIEZ ...................", 2.5, 9.99, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_FIJO_MONTO, 1.99, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500010", "00010", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.KILO)

            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Percepción Ingresos Brutos ....", 165.3, 19.99)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Percepción Imp Municipal ......", 165.3, 19.99)

            pago = hasar.ImprimirPago("Efectivo ........................................", 289.78, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Pesos Argentinos ................................", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO, 0, "", "")

            msj = "DOC. PATRÓN - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: DOC. PATRÓN ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            subt = hasar.ConsultarSubtotal()

            msj = "DOC. PATRÓN - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: DOC. PATRÓN ...")

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            respcerrar = hasar.CerrarDocumento()
            msj = "Doc. Patrón cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Imprimiendo: DOC. PATRÓN ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: DOC. PATRÓN ...")
        End Try

    End Sub

    '============================================================================================================================
    ' BOTÓN: DETALLE DE VENTAS 'X' - Impresión de un comprobante tipo '940' ...
    '============================================================================================================================
    Private Sub ButtonEquis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEquis.Click
        Dim cierre As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal
        Dim equis As hfl.argentina.HasarImpresoraFiscalRG3561.CerrarJornadaFiscalX
        Dim msj As String     '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_X)
            equis = cierre.X

            msj = "DETALLE DE VENTAS :" & vbCrLf & vbCrLf & _
                  "Detalle 'X' Nº" & vbTab & vbTab & "=[" & equis.getNumero() & "]" & vbCrLf & vbCrLf & _
                  "Fecha de Inicio" & vbTab & vbTab & "=[" & equis.getFechaInicio().ToShortDateString() & "]" & vbCrLf & _
                  "Hora de Inicio" & vbTab & vbTab & "=[" & equis.getHoraInicio().ToString() & "]" & vbCrLf & _
                  "Fecha de Cierre" & vbTab & vbTab & "=[" & equis.getFechaCierre().ToShortDateString() & "]" & vbCrLf & _
                  "Hora de Cierre" & vbTab & vbTab & "=[" & equis.getHoraCierre().ToString() & "]" & vbCrLf & vbCrLf & _
                  "DF Emitidos" & vbTab & vbTab & "=[" & equis.getDF_CantidadEmitidos() & "]" & vbCrLf & _
                  "DF Total" & vbTab & vbTab & vbTab & "=[" & equis.getDF_Total() & "]" & vbCrLf & _
                  "DF Total IVA" & vbTab & vbTab & "=[" & equis.getDF_TotalIVA() & "]" & vbCrLf & _
                  "DF Total Otros Tributos" & vbTab & "=[" & equis.getDF_TotalTributos() & "]" & vbCrLf & vbCrLf & _
                  "NC Emitidas" & vbTab & vbTab & "=[" & equis.getNC_CantidadEmitidos() & "]" & vbCrLf & _
                  "NC Total" & vbTab & vbTab & vbTab & "=[" & equis.getNC_Total() & "]" & vbCrLf & _
                  "NC Total IVA" & vbTab & vbTab & "=[" & equis.getNC_TotalIVA() & "]" & vbCrLf & _
                  "NC Total Otros Tributos" & vbTab & "=[" & equis.getNC_TotalTributos() & "]" & vbCrLf & vbCrLf & _
                  "DNFH Emitidos" & vbTab & vbTab & "=[" & equis.getDNFH_CantidadEmitidos() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: CerrarJornadaFiscal()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR CerrarJornadaFiscal() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ESTADO GENERAL - Consulta ...
    '============================================================================================================================
    Private Sub ButtonEstadoGral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEstadoGral.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarEstado
        Dim msj As String     '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarEstado(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A)
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            msj = "-Consulta-" & vbCrLf & _
                  "ESTADO GENERAL IFH 2G :" & vbCrLf & vbCrLf & _
                  "Operando en Modo Entrenamiento ?" & vbTab & "=[" & resp.EstadoAuxiliar.getModoEntrenamiento() & "]" & vbCrLf & _
                  "Estado Interno" & vbTab & vbTab & vbTab & "=[" & resp.getEstadoInterno() & " - " & ToStrEstadosFiscales(resp.getEstadoInterno()) & "]" & vbCrLf & _
                  vbCrLf & _
                  "Comprobante en Curso" & vbTab & vbTab & "=[" & resp.getComprobanteEnCurso() & " - " & ToStrCodComprob(resp.getComprobanteEnCurso()) & "]" & vbCrLf & _
                  "Código Últ. Comprob. consultado" & vbTab & "=[" & resp.getCodigoComprobante() & " - " & ToStrCodComprob(resp.getCodigoComprobante()) & "]" & vbCrLf & _
                  "Número Últ. Comprob. consultado" & vbTab & "=[" & resp.getNumeroUltimoComprobante() & "]" & vbCrLf & _
                  "Últ. Comprob. Emitido Cancel.  ?" & vbTab & "=[" & resp.EstadoAuxiliar.getUltimoComprobanteFueCancelado() & "]" & vbCrLf & _
                  vbCrLf & _
                  "Comprobantes Emitidos" & vbTab & vbTab & "=[" & resp.getCantidadEmitidos() & "]" & vbCrLf & _
                  "Comprobantes Cancelados" & vbTab & vbTab & "=[" & resp.getCantidadCancelados() & "]" & vbCrLf & _
                  vbCrLf & _
                  "Código Barras Memorizado ?" & vbTab & vbTab & "=[" & resp.EstadoAuxiliar.getCodigoBarrasAlmacenado() & "]" & vbCrLf & _
                  "Datos de Cliente Memorizados ?" & vbTab & "=[" & resp.EstadoAuxiliar.getDatosClienteAlmacenados() & "]" & vbCrLf & _
                  "Memoria Auditoría Casi LLena ?" & vbTab & vbTab & "=[" & resp.EstadoAuxiliar.getMemoriaAuditoriaCasiLlena() & "]" & vbCrLf & _
                  "Memoria Auditoría Llena ?" & vbTab & vbTab & "=[" & resp.EstadoAuxiliar.getMemoriaAuditoriaLlena() & "]"

            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarEstado()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarEstado() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: MODELO Y VERSIÓN - Consulta ...
    '============================================================================================================================
    Private Sub ButtonModelo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonModelo.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarVersion
        Dim msj As String       '// A mostrar en MSgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarVersion()

            msj = "-Consulta-" & vbCrLf & _
                  "IDENTIFICACIÓN IFH 2G :" & vbCrLf & vbCrLf & _
                  "Producto" & vbTab & vbTab & "=[" & resp.getNombreProducto() & "]" & vbCrLf & _
                  "Marca" & vbTab & vbTab & "=[" & resp.getMarca() & "]" & vbCrLf & _
                  "Modelo y Versión" & vbTab & "=[" & resp.getVersion() & "]" & vbCrLf & _
                  "Fecha de Firmware" & vbTab & "=[" & resp.getFechaFirmware() & "]" & vbCrLf & _
                  "Motor Versión" & vbTab & "=[" & resp.getVersionMotor() & "]" & vbCrLf & _
                  "Protocolo Versión" & vbTab & "=[" & resp.getVersionProtocolo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarVersion()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarVersion() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: DATOS DE INICIALIZACIÓN - Consulta ...
    '============================================================================================================================
    Private Sub ButtonDatosIni_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDatosIni.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDatosInicializacion
        Dim msj As String         '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarDatosInicializacion()

            msj = "-Consulta-" & vbCrLf & _
                  "DATOS FISCALES EMISOR :" & vbCrLf & vbCrLf & _
                  "Razón Social" & vbTab & "=[" & resp.getRazonSocial() & "]" & vbCrLf & _
                  "Número CUIT" & vbTab & "=[" & resp.getCUIT() & "]" & vbCrLf & _
                  "Categoría IVA" & vbTab & "=[" & resp.getResponsabilidadIVA() & " - " & ToStrCategIVA(resp.getResponsabilidadIVA()) & "]" & vbCrLf & _
                  "Ingresos Brutos" & vbTab & "=[" & resp.getIngBrutos() & "]" & vbCrLf & _
                  "Fecha Inicio Activ." & vbTab & "=[" & resp.getFechaInicioActividades() & "]" & vbCrLf & vbCrLf & _
                  "Número POS" & vbTab & "=[" & resp.getNumeroPos() & "]" & vbCrLf & _
                  "Código Registro" & vbTab & "=[" & resp.getRegistro() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarDatosInicializacion()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarDatosInicializacion() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: FECHA Y HORA - Consulta ...
    '============================================================================================================================
    Private Sub ButtonFechayHora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonFechayHora.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarFechaHora
        Dim msj As String       '// A mostrar en MSgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarFechaHora()

            msj = "-Consulta-" & vbCrLf & _
                  "FECHA/HORA ACTUAL :" & vbCrLf & vbCrLf & _
                  "Fecha Actual" & vbTab & "=[" & resp.getFecha().ToShortDateString() & "]" & vbCrLf & _
                  "Hora Actual" & vbTab & "=[" & resp.getHora().ToString() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarFechaHora()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarFechaHora() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ÚLTIMO ESTADO FISCAL - Consulta ...
    '============================================================================================================================
    Private Sub ButtonUltEstFiscal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUltEstFiscal.Click
        Dim msj As String       '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            msj = "-Consulta-" & vbCrLf & _
                  "ÚLTIMO ESTADO FISCAL :" & vbCrLf & vbCrLf & _
                  "Documento Abierto" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getDocumentoAbierto() & "]" & vbCrLf & _
                  "Documento Fiscal Abierto" & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getDocumentoFiscalAbierto() & "]" & vbCrLf & _
                  "Error Aritmético" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getErrorAritmetico() & "]" & vbCrLf & _
                  "Error Ejecución" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getErrorEjecucion() & "]" & vbCrLf & _
                  "Error Estado" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getErrorEstado() & "]" & vbCrLf & _
                  "Error General" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getErrorGeneral() & "]" & vbCrLf & _
                  "Error Memoria Auditoría" & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getErrorMemoriaAuditoria() & "]" & vbCrLf & _
                  "Error Memoria Fiscal" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getErrorMemoriaFiscal() & "]" & vbCrLf & _
                  "Error Memoria de Trabajo" & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getErrorMemoriaTrabajo() & "]" & vbCrLf & _
                  "Error de Parámetro" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getErrorParametro() & "]" & vbCrLf & _
                  "Memoria Fiscal Casi Llena" & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getMemoriaFiscalCasiLlena() & "]" & vbCrLf & _
                  "Memoria Fiscal Inicializada" & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getMemoriaFiscalInicializada & "]" & vbCrLf & _
                  "Memoria Fiscal Llena" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoFiscal.getMemoriaFiscalLlena() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Función: ObtenerUltimoEstadoFiscal()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ObtenerUltimoEstadoFiscal() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ÚLTIMO ESTADO IMPRESORA - Consulta ...
    '============================================================================================================================
    Private Sub ButtonUltEstImpr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUltEstImpr.Click
        Dim msj As String       '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            msj = "-Consulta-" & vbCrLf & _
                  "ÚLTIMO ESTADO PRN :" & vbCrLf & vbCrLf & _
                  "Cajón Dinero Abierto" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoImpresora.getCajonAbierto() & "]" & vbCrLf & _
                  "Impresora en Error" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoImpresora.getErrorImpresora() & "]" & vbCrLf & _
                  "Falta Papel J." & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoImpresora.getFaltaPapelJournal() & "]" & vbCrLf & _
                  "Falta Papel T." & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoImpresora.getFaltaPapelReceipt() & "]" & vbCrLf & _
                  "Impresora Ocupada" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoImpresora.getImpresoraOcupada() & "]" & vbCrLf & _
                  "Impresora Offline" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoImpresora.getImpresoraOffLine() & "]" & vbCrLf & _
                  "Tapa Abierta" & vbTab & vbTab & "? =[" & hasar.ObtenerUltimoEstadoImpresora.getTapaAbierta() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Función: ObtenerUltimoEstadoImpresora()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ObtenerUltimoEstadoImpresora() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: CARACTERÍSTICAS BÁSICAS - Consulta ...
    '============================================================================================================================
    Private Sub ButtonCapacidades_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCapacidades.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadesImpresoraFiscal
        Dim msj As String       '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarCapacidadesImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Capacidades.ALTO_LOGO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_SLIP, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B)
            msj = "-Consulta-" & vbCrLf & _
                  "CAPACIDADES IFH 2G : :" & vbCrLf & vbCrLf & _
                  "Alto Logo" & vbTab & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarCapacidadesImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Capacidades.ANCHO_LOGO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_SLIP, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B)
            msj = msj & "Ancho Logo" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarCapacidadesImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Capacidades.ANCHO_RAZON_SOCIAL, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_SLIP, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B)
            msj = msj & "Ancho Razón Social" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarCapacidadesImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Capacidades.ANCHO_TEXTO_FISCAL, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_SLIP, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B)
            msj = msj & "Ancho Texto Fiscal" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarCapacidadesImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Capacidades.ANCHO_TEXTO_LINEAS_USUARIO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_SLIP, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B)
            msj = msj & "Ancho Texto Líneas Usuario" & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarCapacidadesImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Capacidades.ANCHO_TEXTO_NO_FISCAL, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_SLIP, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B)
            msj = msj & "Ancho Texto No Fiscal" & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarCapacidadesImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Capacidades.ANCHO_TEXTO_VENTA, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_SLIP, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B)
            msj = msj & "Ancho Texto Venta" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarCapacidadesImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Capacidades.ANCHO_TOTAL_IMPRESION, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_SLIP, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B)
            msj = msj & "Ancho Total Impresión" & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarCapacidadesImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Capacidades.SOPORTA_CAJON, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_SLIP, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B)
            msj = msj & "Soporta Cajón Dinero" & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarCapacidadesImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Capacidades.SOPORTA_COMPROBANTE, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_SLIP, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B)
            msj = msj & "Soporta Factura 'B'" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarCapacidadesImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Capacidades.SOPORTA_ESTACION, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_SLIP, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NO_DOCUMENTO)
            msj = msj & "Soporta Estación Slip" & vbTab & vbTab & "=[" & resp.getValor() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarCapacidadesImpresoraFiscal()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarCapacidadesImpresoraFiscal() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ZETAS TOTALES - Consulta ...
    '============================================================================================================================
    Private Sub Button17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button17.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarCapacidadZetas
        Dim msj As String       '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarCapacidadZetas()

            msj = "-Consulta-" & vbCrLf & _
                  "INFO CIERRES DIARIOS :" & vbCrLf & vbCrLf & _
                  "Último Cierre 'Z'" & vbTab & vbTab & "=[" & resp.getUltimaZeta() & "]" & vbTab & "(a)" & vbCrLf & _
                  "Cierres 'Z' Remanentes" & vbTab & "=[" & resp.getCantidadDeZetasRemanentes() & "]" & vbTab & "(b)" & vbCrLf & vbCrLf & _
                  "CAPACIDAD MEMORIA FISCAL : Interpretación" & vbCrLf & _
                  "Modo entrenamiento" & vbTab & "= (b) + '0' = (b)" & vbTab & "Cierres diarios" & vbCrLf & _
                  "Modo Fiscal" & vbTab & vbTab & "= (b) + (a)" & vbTab & vbTab & "Cierres diarios"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarCapacidadZetas() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarCapacidadZetas() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: CONFIGURACIÓN RED - Consulta ...
    '============================================================================================================================
    Private Sub ButtonCfgRed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCfgRed.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarConfiguracionRed
        Dim msj As String      '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarConfiguracionRed()

            msj = "-Consulta-" & vbCrLf & _
                  "CONFIGURACIÓN RED :" & vbCrLf & vbCrLf & _
                  "Dirección IP" & vbTab & "=[" & resp.getDireccionIP() & "]" & vbCrLf & _
                  "Máscara" & vbTab & vbTab & "=[" & resp.getMascara() & "]" & vbCrLf & _
                  "Gateway" & vbTab & vbTab & "=[" & resp.getGateway() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarConfiguracionRed() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarConfiguracionRed() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: CONFIGURACIÓN CORREO - Consulta ...
    '============================================================================================================================
    Private Sub ButtonCfgCorreo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCfgCorreo.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarConfiguracionServidorCorreo
        Dim msj As String     '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarConfiguracionServidorCorreo()

            msj = "-Consulta-" & vbCrLf & _
                  "CONFIGURACIÓN CORREO :" & vbCrLf & vbCrLf & _
                  "Remitente" & vbTab & vbTab & "=[" & resp.getDireccionRemitente() & "]" & vbCrLf & _
                  "Dirección IP" & vbTab & "=[" & resp.getDireccionIP() & "]" & vbCrLf & _
                  "Puerto" & vbTab & vbTab & "=[" & resp.getPuerto() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarConfiguracionServidorCorreo()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarConfiguracionServidorCorreo() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: SUBTOTAL COMPROBANTE - Consulta ...
    '============================================================================================================================
    Private Sub ButtonSubtot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSubtot.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String      '// A mostrar MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarSubtotal()

            msj = "-Consulta-" & vbCrLf & _
                  "SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & resp.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & resp.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & resp.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & resp.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & resp.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & resp.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & resp.getAjusteRedondeo() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarSubtotal()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarSubtotal() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: RANGO NROS. ZETA - Consulta ...
    '============================================================================================================================
    Private Sub ButtonRangoZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRangoZ.Click
        Dim resp1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerRangoFechasPorZetas
        Dim resp2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerRangoZetasPorFechas
        Dim msj As String      '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            msj = "-Consulta-" & vbCrLf & _
                  "RANGO DE CIERRES DIARIOS :" & vbCrLf & vbCrLf & _
                  "Consulta por rango de números de 'Z', desde '1' hasta '3650' ..." & vbCrLf

            resp1 = hasar.ObtenerRangoFechasPorZetas(1, 3650)

            msj = msj & "Fecha Inicial" & vbTab & "=[" & resp1.getFechaInicial() & "]" & vbCrLf & _
                  "Fecha Final" & vbTab & "=[" & resp1.getFechaFinal() & "]" & vbCrLf & vbCrLf & _
                  "Consulta por rango de fechas, desde '20/01/2001' hasta 31/12/2016' ..." & vbCrLf

            resp2 = hasar.ObtenerRangoZetasPorFechas("20/01/2001", "31/12/2016")

            msj = msj & "Zeta Inicial" & vbTab & "=[" & resp2.getZetaInicial() & "]" & vbCrLf & _
                  "Zeta Final" & vbTab & vbTab & "=[" & resp2.getZetaFinal() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comandos: ObtenerRango..Zetas..() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ObtenerRango..Zetas..() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: LÍMITE BORRADO ZETAS - Consulta ...
    '============================================================================================================================
    Private Sub Button22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button22.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarZetaBorrable
        Dim msj As String       '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarZetaBorrable()

            msj = "-Consulta-" & vbCrLf & _
                  "BORRADO DE CTD :" & vbCrLf & vbCrLf & _
                  "En caso de ser requerido ..." & vbCrLf & _
                  "Borrar hasta 'Z' Nº =[" & resp.getNumeroZeta() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarZetaBorrable()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarZetaBorrable() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ÚLTIMO ERROR - Consulta ...
    '============================================================================================================================
    Private Sub ButtonUltError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUltError.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarUltimoError
        Dim msj As String      '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            resp = hasar.ConsultarUltimoError()

            msj = "-Consulta-" & vbCrLf & _
                  "INFO ÚLTIMO ERROR :" & vbCrLf & vbCrLf & _
                  "Parámetro Nº" & vbTab & "=[" & resp.getNumeroParametro() & "]" & vbCrLf & _
                  "Nombre Parámetro" & vbTab & "=[" & resp.getNombreParametro() & "]" & vbCrLf & _
                  "Último Error" & vbTab & "=[" & resp.getUltimoError() & "]" & vbCrLf

            If (resp.getUltimoError() = "NO_CURRENT_ERROR") Then
                msj = msj & "Descripción" & vbTab & "=[No se reporta ningún error]" & vbCrLf
            Else
                msj = msj & "Descripción" & vbTab & "=[" & resp.getDescripcion() & "]" & vbCrLf
            End If

            msj = msj & "Contexto" & vbTab & vbTab & "=[" & resp.getContexto() & "]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarUltimoError() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarUltimoError() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: COMPROBANTES ASOCIADOS - Consulta ...
    '============================================================================================================================
    Private Sub ButtonDocAsoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDocAsoc.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarDocumentoAsociado
        Dim k As Integer         '// Índice a la línea a consultar.
        Dim msj As String        '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            msj = "-Consulta-" & vbCrLf & _
                  "COMPROBANTES ASOCIADOS :" & vbCrLf & vbCrLf

            For k = 1 To 2
                resp = hasar.ConsultarDocumentoAsociado(k)

                msj = msj & "Línea " & k & " ..." & vbCrLf & _
                      "Nº POS" & vbTab & vbTab & "=[" & resp.getNumeroPos() & "]" & vbCrLf & _
                      "Cód. Comprob." & vbTab & "=[" & resp.getCodigoComprobante() & " - " & ToStrCodComprob(resp.getCodigoComprobante()) & "]" & vbCrLf & _
                      "Comprobante Nº" & vbTab & "=[" & resp.getNumeroComprobante() & "]" & vbCrLf & vbCrLf
            Next

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarDocumentoAsociado()")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarDocumentoAsociado() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: LÍNEAS DE USUARIO - Consulta ...
    '============================================================================================================================
    Private Sub Button25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button25.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarZona
        Dim k As Integer         '// Índice a la línea a consultar
        Dim msj As String        '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            msj = "-Consulta-" & vbCrLf & _
                  "LÍNEAS DE USUARIO : " & vbCrLf & vbCrLf & _
                  "Líneas de fantasía ..." & vbCrLf & vbCrLf

            For k = 1 To 2
                resp = hasar.ConsultarZona(k, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_FANTASIA)

                msj = msj & "Línea " & k & " ..." & vbCrLf & _
                      "Texto" & vbTab & vbTab & "=[" & resp.getDescripcion() & "]" & vbCrLf & _
                      "Centrado ?" & vbTab & "=[" & resp.Atributos.getCentrado() & "]" & vbCrLf & _
                      "Doble Ancho ?" & vbTab & "=[" & resp.Atributos.getDobleAncho() & "]" & vbCrLf & _
                      "Negrita ?" & vbTab & vbTab & "=[" & resp.Atributos.getNegrita() & "]" & vbCrLf & _
                      "Borrado ?" & vbTab & vbTab & "=[" & resp.Atributos.getBorradoTexto() & "]" & vbCrLf & vbCrLf
            Next

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarZona() ...")
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            msj = "-Consulta-" & vbCrLf & _
                  "LÍNEAS DE USUARIO : " & vbCrLf & vbCrLf & _
                  "Líneas de encabezado, zona 1 ..." & vbCrLf & vbCrLf

            For k = 1 To 3
                resp = hasar.ConsultarZona(k, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO)

                msj = msj & "Línea " & k & " ..." & vbCrLf & _
                      "Texto" & vbTab & vbTab & "=[" & resp.getDescripcion() & "]" & vbCrLf & _
                      "Centrado ?" & vbTab & "=[" & resp.Atributos.getCentrado() & "]" & vbCrLf & _
                      "Doble Ancho ?" & vbTab & "=[" & resp.Atributos.getDobleAncho() & "]" & vbCrLf & _
                      "Negrita ?" & vbTab & vbTab & "=[" & resp.Atributos.getNegrita() & "]" & vbCrLf & _
                      "Borrado ?" & vbTab & vbTab & "=[" & resp.Atributos.getBorradoTexto() & "]" & vbCrLf & vbCrLf
            Next

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarZona() ...")
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            'msj = "-Consulta-" & vbCrLf & _
            '      "LÍNEAS DE USUARIO : " & vbCrLf & vbCrLf & _
            '      "Líneas de encabezado, zona 2 ..." & vbcrlf & vbcrlf    '// Sólo tickeadoras

            'For k = 1 To 3
            'resp = hasar.ConsultarZona(k, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_ENCABEZADO)

            'msj = msj & "Línea " & k & " ..." & vbCrLf & _
            '  "Texto" & vbTab & vbTab & "=[" & resp.getDescripcion() & "]" & vbCrLf & _
            '  "Centrado ?" & vbTab & "=[" & resp.Atributos.getCentrado() & "]" & vbCrLf & _
            '  "Doble Ancho ?" & vbTab & "=[" & resp.Atributos.getDobleAncho() & "]" & vbCrLf & _
            '  "Negrita ?" & vbTab & vbTab & "=[" & resp.Atributos.getNegrita() & "]" & vbCrLf & _
            '  "Borrado ?" & vbTab & vbTab & "=[" & resp.Atributos.getBorradoTexto() & "]" & vbCrLf & vbCrLf
            'Next

            'Me.Cursor = System.Windows.Forms.Cursors.Arrow 
            'MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarZona() ...")
            'Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            msj = "-Consulta-" & vbCrLf & _
                  "LÍNEAS DE USUARIO : " & vbCrLf & vbCrLf & _
                  "Líneas de cola, zona 1 ..." & vbCrLf & vbCrLf

            For k = 1 To 4
                resp = hasar.ConsultarZona(k, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_COLA)

                msj = msj & "Línea " & k & " ..." & vbCrLf & _
                      "Texto" & vbTab & vbTab & "=[" & resp.getDescripcion() & "]" & vbCrLf & _
                      "Centrado ?" & vbTab & "=[" & resp.Atributos.getCentrado() & "]" & vbCrLf & _
                      "Doble Ancho ?" & vbTab & "=[" & resp.Atributos.getDobleAncho() & "]" & vbCrLf & _
                      "Negrita ?" & vbTab & vbTab & "=[" & resp.Atributos.getNegrita() & "]" & vbCrLf & _
                      "Borrado ?" & vbTab & vbTab & "=[" & resp.Atributos.getBorradoTexto() & "]" & vbCrLf & vbCrLf
            Next

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarZona() ...")
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            'msj ="-Consulta-" & vbCrLf & _
            '      "LÍNEAS DE USUARIO : " & vbCrLf & vbCrLf & _
            '      "Líneas de cola, zona 2 ..." & vbcrlf & vbcrlf   '// Sólo tickeadoras

            'For k = 1 To 6
            'resp = hasar.ConsultarZona(k, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_COLA)

            'msj = msj & "Línea " & k & " ..." & vbCrLf & _
            '  "Texto" & vbTab & vbTab & "=[" & resp.getDescripcion() & "]" & vbCrLf & _
            '  "Centrado ?" & vbTab & "=[" & resp.Atributos.getCentrado() & "]" & vbCrLf & _
            '  "Doble Ancho ?" & vbTab & "=[" & resp.Atributos.getDobleAncho() & "]" & vbCrLf & _
            '  "Negrita ?" & vbTab & vbTab & "=[" & resp.Atributos.getNegrita() & "]" & vbCrLf & _
            '  "Borrado ?" & vbTab & vbTab & "=[" & resp.Atributos.getBorradoTexto() & "]" & vbCrLf & vbCrLf
            'Next

            'Me.Cursor = System.Windows.Forms.Cursors.Arrow 
            'MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarZona() ...")
            'Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            msj = "-Consulta-" & vbCrLf & _
                  "LÍNEAS DE USUARIO : " & vbCrLf & vbCrLf & _
                  "Líneas de domicilio del emisor ..." & vbCrLf & vbCrLf

            For k = 1 To 2  '// 4 en tickeadoras
                resp = hasar.ConsultarZona(k, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_DOMICILIO_EMISOR)

                msj = msj & "Línea " & k & " ..." & vbCrLf & _
                      "Texto" & vbTab & vbTab & "=[" & resp.getDescripcion() & "]" & vbCrLf & _
                      "Centrado ?" & vbTab & "=[" & resp.Atributos.getCentrado() & "]" & vbCrLf & _
                      "Doble Ancho ?" & vbTab & "=[" & resp.Atributos.getDobleAncho() & "]" & vbCrLf & _
                      "Negrita ?" & vbTab & vbTab & "=[" & resp.Atributos.getNegrita() & "]" & vbCrLf & _
                      "Borrado ?" & vbTab & vbTab & "=[" & resp.Atributos.getBorradoTexto() & "]" & vbCrLf & vbCrLf
            Next

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConsultarZona() ...")
            MsgBox("-Consulta-" & vbCrLf & "LÍNEAS DE USUARIO : " & vbCrLf & vbCrLf & "Fin de datos consultados ! ...", vbOKOnly + vbInformation, "Comando: ConsultarZona() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarZona() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: SERVIDOR DE CORREO - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetCorreo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetCorreo.Click
        Dim msj As String      '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ConfigurarServidorCorreo("", 25, "hasarventas@hasar.com")

            msj = "-Configuración-" & vbCrLf & _
                  "CORREO ELECTRÓNICO :" & vbCrLf & vbCrLf & _
                  "NUEVOS VALORES ..." & vbCrLf & vbCrLf & _
                  "Dirección IP" & vbTab & "=[]" & vbCrLf & _
                  "Puerto" & vbTab & vbTab & "=[25]" & vbCrLf & _
                  "Remitente" & vbTab & vbTab & "=[hasarventas@hasar.com]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConfigurarServidorCorreo() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConfigurarServidorCorreo() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: CONECTIVIDAD EN RED - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetRed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetRed.Click
        Dim msj As String        '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ConfigurarRed("10.0.7.69", "255.255.255.0", "10.0.7.69", hfl.argentina.HasarImpresoraFiscalRG3561.SiNo.NO)

            msj = "-Configuración-" & vbCrLf & _
                  "PARÁMETROS DE RED :" & vbCrLf & vbCrLf & _
                  "NUEVOS VALORES ..." & vbCrLf & vbCrLf & _
                  "Dirección IP" & vbTab & "=[10.0.7.69]" & vbCrLf & _
                  "Máscara" & vbTab & vbTab & "=[255.255.255.0]" & vbCrLf & _
                  "Gateway" & vbTab & vbTab & "=[10.0.7.69]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConfigurarRed() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConfigurarRed() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: BAUDRATE SERIAL - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetBaudios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetBaudios.Click
        Dim msj As String        '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.CambiarVelocidadPuerto(hfl.argentina.HasarImpresoraFiscalRG3561.Baudios.BAUDRATE_9600)

            msj = "-Configuración-" & vbCrLf & _
                  "PUERTO RS-232 :" & vbCrLf & vbCrLf & _
                  "NUEVO VALOR ..." & vbCrLf & vbCrLf & _
                  "Baudrate" & vbTab & "=[9600]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: CambiarVelocidadPuerto() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR CambiarVelocidadPuerto() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: FECHA Y HORA - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetFechaHora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetFechaHora.Click
        Dim hora As TimeSpan = New TimeSpan(16, 17, 0)
        Dim fecha As DateTime = New DateTime(2016, 1, 8, New GregorianCalendar())
        Dim msj As String        '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            msj = "-Configuración-" & vbCrLf & _
                  "FECHA Y HORA :" & vbCrLf & vbCrLf & _
                  "NUEVOS VALORES ..." & vbCrLf & vbCrLf & _
                  "Fecha" & vbTab & "=[" & Mid$(fecha.ToString(), 1, 10) & "]" & vbCrLf & _
                  "Hora" & vbTab & "=[" & hora.ToString() & "]"

            hasar.ConfigurarFechaHora(fecha, hora)
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConfigurarFechaHora() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConfigurarFechaHora() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: COMPORTAMIENTO IFH - Configuración ...
    '============================================================================================================================
    Private Sub ButtonsETPARAM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonsETPARAM.Click
        Dim msj As String      '// A mostrar en MsgBox()
        'Dim cierre As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.ALTO_HOJA, "A")
            msj = "-Configuración-" & vbCrLf & _
                  "COMPORTAMIENTO IFH 2G :" & vbCrLf & vbCrLf & _
                  "NUEVOS VALORES ..." & vbCrLf & vbCrLf & _
                  "Alto Hoja" & vbTab & vbTab & vbTab & "=[A] - Tamaño A4" & vbCrLf                                    '// O=Oficio, C=Carta , A=A4
            'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.TIPO_HABILITACION, "M")
            'Exit Sub
            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.ANCHO_HOJA, "N")
            msj = msj & "Ancho Hoja" & vbTab & vbTab & "=[N] - Normal" & vbCrLf                                        '// M=Reducido, N=Normal

            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.BORRADO_AUTOMATICO_AUDITORIA, "P")
            msj = msj & "Borrado CTD Autom." & vbTab & vbTab & "=[P] - Automático" & vbCrLf                            '// P=Automático, Otro=Manual

            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.CHEQUEO_DESBORDE, "P")
            msj = msj & "Chequeo Desborde" & vbTab & vbTab & "=[P] - Siempre" & vbCrLf                                 '// P=Siempre, Otro=Sólo al cerrar

            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.CORTE_PAPEL, "N")
            msj = msj & "Corte Papel" & vbTab & vbTab & "=[N] - No Cortar" & vbCrLf                                    '// F=Total, P=Parcial, N=No Cortar

            'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.ESTACION_REPORTES_XZ, "S")
            'msj = msj & "Estación X/Z" & vbTab & vbTab & "=[S] - Página Completa" & vbCrLf                             '// T=Rollo, S=Pág. Completa 
            '// ESTACION_REPORTES_XZ no soportado en página completa

            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_CAMBIO, "N")
            msj = msj & "Impresión CAMBIO" & vbTab & vbTab & "=[N] - No imprime" & vbCrLf                               '// P=Imprime, Otro=No Imprime

            'cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z)
            'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_CODIGO_QR, "P")
            'msj = msj & "Impresión QR" & vbTab & vbTab & "=[P] - Imprime" & vbCrLf                                      '// P=Imprime, Otro=No Imprime
            '// IMPRESION_CODIGO_QR requiere hacer 'Z' previamente

            'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_COLOR_ALTERNATIVO, "P")
            'msj = msj & "Color Alternativo" & vbTab & vbTab & "=[P] - No Imprime" & vbCrLf                              '// P=Imprime, Otro=No Imprime
            '// IMPRESION_COLOR_ALTERNATIVO no soportado en página completa

            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_MARCO, "N")
            msj = msj & "Impresión Marco" & vbTab & vbTab & "=[N] - No Imprime" & vbCrLf                                 '// P=Imprime, Otro=No Imprime

            'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_USAR_COLOR_ALTERNATIVO, "N")
            'msj = msj & "Usar color altern." & vbTab & vbTab & "=[N] - No Imprime" & vbCrLf                             '// P=Usar, Otro=No Usar
            '// IMPRESION_USAR_COLOR_ALTERNATIVO no soportado en página completa

            'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.INTERLINEADO, "N")
            'msj = msj & "Interlineado" & vbTab & vbTab & "=[N] - No Imprime" & vbCrLf                                   '// A=Máx, M=Mín, N=Normal
            '// INTERLINEADO no soportado en página completa

            'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.LIMITE_BC, "N")
            'msj = msj & "Límite sin nominar" & vbTab & vbTab & "=[N] - No Imprime" & vbCrLf                             '// 0=SinLímite
            '// LIMITE_BC no soportado en página completa

            'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.MODO_IMPRESION, "N")
            'msj = msj & "Modo impresión" & vbTab & vbTab & "=[M] - Mixto" & vbCrLf                                      '// M=Mixto , A=Sólo Rollo
            '// MODO_IMPRESION no soportado en página completa

            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.PAGO_SALDO, "Saldo")
            msj = msj & "Texto saldo" & vbTab & vbTab & "=[Saldo]" & vbCrLf                                              '// Default="Saldo"

            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.REIMPRESION_CANCELADOS, "N")
            msj = msj & "Reimpresión cancel." & vbTab & vbTab & "=[N] - No Reimprime" & vbCrLf                           '// P=Reimprimir , Otro=No Reimpr

            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.SONIDO_AVISO, "P")
            msj = msj & "Aviso sonoro" & vbTab & vbTab & "=[P] - Avisar" & vbCrLf                                        '// P=Avisar , Otro=No Avisar

            hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.TIMEOUT_ENVIO_RESPUESTA_EN_ESPERA, "10")
            msj = msj & "Espera timeout" & vbTab & vbTab & "=[10] - ms" & vbCrLf                                        '// Default=10

            'cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z)
            'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.TIPO_HABILITACION, "A")
            'msj = msj & "Habilitación" & vbTab & vbTab & "=[A] - Comprob. 'A'" & vbCrLf                                  '// A=A , L=A con leyenda , M=M
            '// TIPO_HABILITACION requiere hacer 'Z' previamente

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConfigurarImpresoraFiscal() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConfigurarImpresoraFiscal() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: LÍMITE BORRADO 'Z' - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetBorrZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetBorrZ.Click
        Dim msj As String      '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.ConfigurarZetaBorrable(1)
            msj = "-Configuración-" & vbCrLf & _
                  "LÍMITE BORRADO CTD :" & vbCrLf & vbCrLf & _
                  "NUEVOS VALORES ..." & vbCrLf & vbCrLf & _
                  "Borrar hasta 'Z' Nº 1"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly, "Comando: ConfigurarZetaBorrable() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConfigurarZetaBorrable() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: LÍNEAS DE USUARIO - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetZona_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetZona.Click
        Dim msj As String     '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            msj = "-Configuración-" & vbCrLf & _
                  "LÍNEAS DE USUARIO :" & vbCrLf & vbCrLf & _
                  "NUEVOS VALORES ..." & vbCrLf & vbCrLf

            estilo.setBorradoTexto(False)
            estilo.setCentrado(False)
            estilo.setDobleAncho(False)
            estilo.setNegrita(False)

            hasar.ConfigurarZona(1, estilo, "Texto libre fantasía, línea 1 ... +++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_FANTASIA)
            hasar.ConfigurarZona(2, estilo, "Texto libre fantasía, línea 2 ... +++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_FANTASIA)

            hasar.ConfigurarZona(1, estilo, "Texto libre encabezado, zona 1, línea 1 ... +++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO)
            hasar.ConfigurarZona(2, estilo, "Texto libre encabezado, zona 1, línea 2 ... +++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO)
            hasar.ConfigurarZona(3, estilo, "Texto libre encabezado, zona 1, línea 3 ... +++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO)

            hasar.ConfigurarZona(1, estilo, "Texto libre domicilio emisor, línea 1 ... +++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_DOMICILIO_EMISOR)
            hasar.ConfigurarZona(2, estilo, "Texto libre domicilio emisor, línea 2 ... +++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_DOMICILIO_EMISOR)
            hasar.ConfigurarZona(3, estilo, "Texto libre domicilio emisor, línea 3 ... +++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_DOMICILIO_EMISOR)
            hasar.ConfigurarZona(4, estilo, "Texto libre domicilio emisor, línea 4 ... +++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_DOMICILIO_EMISOR)

            hasar.ConfigurarZona(1, estilo, "Texto libre pie, zona 1, línea 1 ... ++++++++++++++++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_COLA)
            hasar.ConfigurarZona(2, estilo, "Texto libre pie, zona 1, línea 2 ... ++++++++++++++++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_COLA)
            hasar.ConfigurarZona(3, estilo, "Texto libre pie, zona 1, línea 3 ... ++++++++++++++++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_COLA)
            hasar.ConfigurarZona(4, estilo, "Texto libre pie, zona 1, línea 4 ... ++++++++++++++++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_COLA)

            hasar.ConfigurarZona(1, estilo, "Texto libre pie, zona 2, línea 1 ... ++++++++++++++++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_COLA)
            hasar.ConfigurarZona(2, estilo, "Texto libre pie, zona 2, línea 2 ... ++++++++++++++++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_COLA)
            hasar.ConfigurarZona(3, estilo, "Texto libre pie, zona 2, línea 3 ... ++++++++++++++++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_COLA)
            hasar.ConfigurarZona(4, estilo, "Texto libre pie, zona 2, línea 4 ... ++++++++++++++++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_COLA)
            hasar.ConfigurarZona(5, estilo, "Texto libre pie, zona 2, línea 5 ... ++++++++++++++++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_COLA)
            hasar.ConfigurarZona(6, estilo, "Texto libre pie, zona 2, línea 6 ... ++++++++++++++++++++++++++++++++++++++++++++++", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_COLA)
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            Exit Sub

            ''--------- Burger King
            'hasar.ConfigurarZona(2, estilo, "", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_COLA)
            'hasar.ConfigurarZona(3, estilo, "", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_COLA)
            'Me.Cursor = System.Windows.Forms.Cursors.Arrow
            'Exit Sub

            'hasar.ConfigurarZona(1, estilo, "Completa la encuesta", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_COLA)
            'hasar.ConfigurarZona(2, estilo, "WWW.EVALUABK.COM", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_COLA)
            'hasar.ConfigurarZona(3, estilo, "BK Número: 17127", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_COLA)
            'Me.Cursor = System.Windows.Forms.Cursors.Arrow
            'Exit Sub
            ''--------- fin Burger King

            hasar.ConfigurarZona(1, estilo, "Texto fantasía, línea 1 ::: Centrado", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_FANTASIA)
            msj = msj & "Texto fantasía, línea 1 ::: Centrado y Negrita" & vbCrLf

            'estilo.setNegrita(False)
            hasar.ConfigurarZona(1, estilo, "Texto encabezado, zona 1, línea 1 ::: Centrado", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_ENCABEZADO)
            msj = msj & "Texto encabezado, zona 1, línea 1 ::: Centrado" & vbCrLf

            'estilo.setCentrado(False)
            'hasar.ConfigurarZona(1, estilo, "Texto encabezado, zona 2, línea 1 ::: Justificado a izq.", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_ENCABEZADO)
            'msj = msj & "Texto encabezado, zona 2, línea 1 ::: Justificado a izq." & vbCrLf
            '// Zona 2, encabezado, no soportdo en página completa.

            hasar.ConfigurarZona(1, estilo, "Texto domicilio emisor, línea 1 ::: Justificado a izq.", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_DOMICILIO_EMISOR)
            msj = msj & "Texto domicilio emisor, línea 1 ::: Justificado a izq." & vbCrLf

            hasar.ConfigurarZona(1, estilo, "Texto de cola, zona 1, línea 1 ::: Justificado a izq.", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_1_COLA)
            msj = msj & "Texto de cola, zona 1, línea 1 ::: Justificado a izq." & vbCrLf

            'hasar.ConfigurarZona(1, estilo, "Texto de cola, zona 2, línea 1 ::: Justificado a izq.", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeEstacion.ESTACION_POR_DEFECTO, hfl.argentina.HasarImpresoraFiscalRG3561.ZonasDeLineasDeUsuario.ZONA_2_COLA)
            'msj = msj & "Texto de cola, zona 2, línea 1 ::: Justificado a izq."
            '// Zona 2, cola, no soportdo en página completa.

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ConfigurarZona() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConfigurarZona() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ABRIR CAJÓN - Misceláneas ...
    '============================================================================================================================
    Private Sub ButtonCajon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCajon.Click
        Dim msj As String        '// A mostrar en MsgBox()
        Dim proc As Integer
        Dim msj1 As String = "http://10.0.7.69/datafiles.xml"
        Dim msj2 As String = " --noproxy 10.0.7.69 -H "
        Dim msj3 As String = """Content-Type: text/xml """
        Dim msj4 As String = "-u:9999 --data-binary "
        Dim msj5 As String = "@C:\basura\curl\Alta_Preferencias.xml"""

        '-----------------------
        Dim procID As Integer
        Dim newProc As Diagnostics.Process
        Dim procEC As Integer = -1

        newProc = Diagnostics.Process.Start("C:\WINDOWS\NOTEPAD.EXE", "pp.txt")
        'newProc = Diagnostics.Process.Start("C:\basura\curl\curl.exe", _
        '                                    msj1 & msj2 & msj3 & msj4 & msj5 & """ > """ & "C:\basura\curl\resp.xml")
        '& " > " & _
        '"""C:\GRUPO HASAR\EQUIPOS FISCALES 2G\R-HAS-6100-FAR\PRUEBAS\CURL\BASES DE DATOS\resp.xml""")
        procID = newProc.Id
        newProc.WaitForExit()

        If newProc.HasExited Then
            procEC = newProc.ExitCode
        End If
        MsgBox("Process with ID " & CStr(procID) & _
            " terminated with exit code " & CStr(procEC))
        Exit Sub
        '-------------------------------

        'Dim proces As New Process()
        'proces.StartInfo.FileName = "notepad.exe"
        'proces.Start()
        'proc = Shell("C:\GRUPO HASAR\EQUIPOS FISCALES 2G\HERRAMIENTAS\HFL_TEST\hfl " & "ethernet 127.0.0.1", AppWinStyle.NormalFocus, False)
        msj1 = "C:\GRUPO HASAR\EQUIPOS FISCALES 2G\R-HAS-6100-FAR\PRUEBAS\CURL\BASES DE DATOS\curl "
        msj2 = "http://10.0.7.69/datafiles.xml"
        msj3 = " --noproxy 10.0.7.69 -H "
        msj4 = """Content-Type: text/xml """
        msj = msj1 & msj2 & msj3 & msj4 & "-u:9999 --data-binary @""C:\GRUPO HASAR\EQUIPOS FISCALES 2G\R-HAS-6100-FAR\PRUEBAS\CURL\BASES DE DATOS\Alta_Preferencias.xml"" > ""C:\GRUPO HASAR\EQUIPOS FISCALES 2G\R-HAS-6100-FAR\PRUEBAS\CURL\BASES DE DATOS\resp.xml"""
        'msj = """C:\GRUPO HASAR\EQUIPOS FISCALES 2G\R-HAS-6100-FAR\PRUEBAS\CURL\BASES DE DATOS\curl """ & """http://10.0.7.69/datafiles.xml"" --noproxy 10.0.7.69 -H ""Content-Type: text/xml"" -u:9999 --data-binary @""C:\GRUPO HASAR\EQUIPOS FISCALES 2G\R-HAS-6100-FAR\PRUEBAS\CURL\BASES DE DATOS\Alta_Preferencias.xml"""
        proc = Shell(msj, AppWinStyle.NormalFocus, False)
        Debug.Print("proc = ", proc)
        Exit Sub

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.AbrirCajonDinero()
            msj = "ABRIENDO CAJÓN DE DINERO :" & vbCrLf & vbCrLf & _
                  "Cajón abierto ..."

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: AbrirCajonDinero() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR AbrirCajonDinero() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: AVANZAR PAPEL - Misceláneas ...                                                      NO SOPORTADO EN PÁGINA COMPLETA
    '============================================================================================================================
    'Private Sub ButtonAvanzarPapel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAvanzarPapel.Click
    'Dim msj As String        '// A mostrar en MsgBox()

    'Try
    '    hasar.AvanzarPapelAmbasEstaciones(5)
    '    msj = "AVANZANDO PAPEL :" & vbcrlf & vbcrlf & _
    '          "Avanzado de 5 líneas ..."

    '    MsgBox(msj, vbOKOnly, "Comando: AvanzarPapelAmbasEstaciones() ...")
    'Catch
    '    msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
    '    MsgBox(msj, vbOKOnly + vbCritical, "ERROR AvanzarPapelAmbasEstaciones() ...")
    'End Try
    'End Sub

    '============================================================================================================================
    ' BOTÓN: CARGAR LOGO - Misceláneas ...                                                           EJEMPLO PARA TÉRMICAS ROLLO
    '
    ' La imagen (BMP/PNG) debe ser convertida a una lista de comandos soportados por la IFH 2G.
    ' Una forma más amigable de cargar el LOGO, es utilizar un navegador de intenet, y a través de la interfaz HTTP que provee
    ' la IFH 2G.
    '============================================================================================================================
    'Private Sub ButtonCargaLogo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCargaLogo.Click
    '    Dim msj As String        '// A mostrar en MsgBox()
    '    Dim cierre As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal

    '    Try
    '        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
    '        '// Para poder cargar el logo se requiere un cierre 'Z' previo
    '        cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z)

    '        msj = "CARGANDO LOGO DEL EMISOR :" & vbCrLf & vbCrLf & _
    '              "Comenzando la carga del LOGO ..." & vbCrLf
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.COMIENZO_CARGA_LOGO, "424D9E180000000000003E00000028000000DC010000680000000100010000000000601800000000000000000000000000000000000000000000FFFFFF00FFFF")

    '        msj = msj & "Carga del LOGO en curso ..." & vbCrLf
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFF00003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFF0000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF800000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF800000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFF0FFFFFFC000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFF0FFFFFF00000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFF0FFFFFE00000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FE00000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF8000000")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0000000000001FF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFC0000000000000FFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF800000000000007FFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF800000000000007FFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF8000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF8000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF00000000000003C07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF00000000000007F07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFE0000000000000FF81FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFF0FFE0000000000000FF81FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFF0FFC0000000000003FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFF0FFC0000000000007FE07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF80")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "000000000087FC7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF8000018000")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "000FFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF8000018000000FFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF0000780000001FFFFE1FFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF0003000000003FFFFE1FFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE0000007FFFFFFFFFFE0FFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE0000007FFFFFFFFFFE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE004007FFFFFFFFFFFE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE007CFFFFFFFFFFFFF80FFFFFFCFFFF9FBFFFFC7FFC0FFFFDFFFF80FFFFF873FFFC")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "7FFF1FFFEFFFE00FFFFE00FFE1FE7FC1FEFFFFC07FFFFFFFFFF0FE003FFFFFFFFFFFFFF80FFFFFFCFFFF9FBFFFFC7FFC0FFFFDFFFF80FFFFF873FFFC7FFF1FFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "EFFFE00FFFFE00FFE1FE7FC1FEFFFFC07FFFFFFFFFF0FC003FFFFFFFFFFFFFF807FFFFFCFFFF3FBFFFFCFF80007FFDFFFF0EFFFFF8F3FFF87FFF1FFFEFFE0001")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFE0001FE1FE7FC0FEFFF80007FFFFFFFFF0FC003FFFFFFFFFFFFFF807FFFFFCFFFF3FBFFFFCFF80007FFDFFFF0EFFFFF8F3FFF87FFF1FFFEFFE0001FFE0001F")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "E1FE7FC0FEFFF80007FFFFFFFFF0FC003FFFFFFFFFFF003007FFFFFCFFFF3FBFFFF0FE0FFE1FFDFFFC7E3FFFE0F3FFF87FFF1FFFEFF81FF87F83FF0FE1FE7F80")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FEFFE07FF1FFFFFFFFF0FC000FFFFFFFFFF8000007FFFFFCFFFF3FBFFFF0FE0FFE1FFDFFFC7E3FFFE0F3FFF87FFF1FFFEFF81FF87F83FF0FE1FE7F80FEFFE07F")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "F1FFFFFFFFF0FC000FFFFFFFFFFF000007FFFFFCFFF07FBFFFE3FE7FFF9FFDFFF0FE3FFFE7F3FFE07FFF1FFFEFF8FFFE3F1FFFCFE1FE7F8EFEFFE7FFF9FFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFF0FC000FFFFFFFFFFF000007FFFFFCFFF07FBFFFE3FE7FFF9FFDFFF0FE3FFFE7F3FFE07FFF1FFFEFF8FFFE3F1FFFCFE1FE7F8EFEFFE7FFF9FFFFFFFFF0FC00")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "07FFFFFFFFFFF00007FFFFFCFFE0FFBFFFEFFCFFFFC7FDFFF0FF07FFC7F3FF047FFF1FFFEFF1FFFE3F1FFFEFE1FE7FBE7EFFCFFFFC3FFFFFFFF0FC0000FFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFC007FFFFFCFFE0FFBFFFEFFCFFFFC7FDFFF0FF07FFC7F3FF047FFF1FFFEFF1FFFE3F1FFFEFE1FE7FBE7EFFCFFFFC3FFFFFFFF0F800001FFFFFFFFFFFE0")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "03FFFFFCFFEFFFBFFF8FFDFFFFE7FDFFE3FF000007F3FF1C7FFF1FFFEFF1FFFE3F7FFFEFE1FE7FBE7EFFDFFFFE3FFFFFFFF0F800001FFFFFFFFFFFE003FFFFFC")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFEFFFBFFF8FFDFFFFE7FDFFE3FF000007F3FF1C7FFF1FFFEFF1FFFE3F7FFFEFE1FE7FBE7EFFDFFFFE3FFFFFFFF0F8000001FFFFFFFFFFF003FFFFFCFFCFFFBF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FC1FF1FFFFE0FC3F0FFFC0001FF3FE1C7FFF1FFFEFFFFFFE3FFFFFCFE1FE7E3F7EFF9FFFFE0FFFFFFFF0F80000001FFFFFFFFFF003FFFFFCFFCFFFBFFC1FF1FF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFE0FC3F0FFFC0001FF3FE1C7FFF1FFFEFFFFFFE3FFFFFCFE1FE7E3F7EFF9FFFFE0FFFFFFFF0F800000007FFFFFFFFF063FFFFFC7E1FFF87F07FF1FFFFE0FC0F")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "1FFFC3FF9FF3F8FC7FFF1FFFEFFFFFF07FFFFF0FE1FE7E7F7EFF9FFFFE0FFFFFFFF0FC00000003FFFFFFFFF067FFFFFC7E1FFF87F07FF1FFFFE0FC0F1FFFC3FF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "9FF3F8FC7FFF1FFFEFFFFFF07FFFFF0FE1FE7E7F7EFF9FFFFE0FFFFFFFF0FC00000003FFFFFFFFF067FFFFFC0C3FFF80000FF1FFFFE0FC041FFFC3FFBFF3F8FC")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "7FFF1FFFEFFFFC01FFFF003FE1FE7E7F3EFF9FFFFE0FFFFFFFF0FC00000001FFFFFFFFF867FFFFFC0C3FFF80000FF1FFFFE0FC041FFFC3FFBFF3F8FC7FFF1FFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "EFFFFC01FFFF003FE1FE7E7F3EFF9FFFFE0FFFFFFFF0FC00000000FFFFFFF00007FFFFFC0C3FFF80000FF1FFFFE0FC041FFFC3FFBFF3F8FC7FFF1FFFEFFFFC01")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFF003FE1FE7E7F3EFF9FFFFE0FFFFFFFF0FC000000007FFFFFFC0007FFFFFC803FFF87FF83F1FFFFE0FDF0FFFFF8FE3FF3F1FC7FFF1FFFEFFF000FFFE007FF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "E1FE787F3EFF9FFFFE0FFFFFFFF0FC000000007FFFFFFC0007FFFFFC803FFF87FF83F1FFFFE0FDF0FFFFF8FE3FF3F1FC7FFF1FFFEFFF000FFFE007FFE1FE787F")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "3EFF9FFFFE0FFFFFFFF0FC000000001FFFFFFF0007FFFFFCF1FFFFBFFFE3F1FFFFE0FDF8FFFFF8FE7FF3F7FC7FFF1FFFEFF81FFFFF81FFFFE1FE787FBEFF9FFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FE0FFFFFFFF0FC0000000007FFFFFF8007FFFFFCF1FFFFBFFFE3F1FFFFE0FDF8FFFFF8FE7FF3F7FC7FFF1FFFEFF81FFFFF81FFFFE1FE787FBEFF9FFFFE0FFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFF0FC0000000003FFFFFFC007FFFFFCFC3FFFBFFFF0FDFFFFE7FDFC1FFFFCF07FF3C7FC7FFF1FFFEFF8FFFFFF1FFFFFE1FE79FFBEFFDFFFFE3FFFFFFFF0FC00")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "00000003FFFFFFC007FFFFFCFC3FFFBFFFF0FDFFFFE7FDFC1FFFFCF07FF3C7FC7FFF1FFFEFF8FFFFFF1FFFFFE1FE79FFBEFFDFFFFE3FFFFFFFF0FE0000000001")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFF00FFFFFFCFE1FFFBFFFF0FCFFFFC7FDFC0FFFFC707FF38FFC7FFF1FFFEFF9FFFFFF1FFFFFE1FE71FF82FFCFFFFC3FFFFFFFF0FE0000000000FFFFFFF8")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "0FFFFFFCFE1FFFBFFFF0FCFFFFC7FDFC0FFFFC707FF38FFC7FFF1FFFEFF9FFFFFF1FFFFFE1FE71FF82FFCFFFFC3FFFFFFFF0FE00000000003FFFFFFF0FFFFFFC")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFCFFFBFFFF0FCFFFFC7FDFF03FFFC71FFF00FFC7FFF1FFFEFF9FFF87F1FFFCFE1FE71FFC0FFCFFFFC3FFFFFFFF0FF00000000001FFFFFFF9FFFFFFCFFCFFFBF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFF0FCFFFFC7FDFF03FFFC71FFF00FFC7FFF1FFFEFF9FFF87F1FFFCFE1FE71FFC0FFCFFFFC3FFFFFFFF0FF00000000001FFFFFFF9FFFFFFCFFE0FFBFFFE3FE7F")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FF9FFDFFE0FFFF61FFF03FFC7FFF1FFFEFF8FFF87F9FFF1FE1FE07FFC0FFE7FFF9FFFFFFFFF0FF00000000000FFFFFFFFFFFFFFCFFE0FFBFFFE3FE7FFF9FFDFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "E0FFFF61FFF03FFC7FFF1FFFEFF8FFF87F9FFF1FE1FE07FFC0FFE7FFF9FFFFFFFFF0FF800000000007FFFFFFFFFFFFFCFFF07F800003FF03F03FFDFFF07FFF0F")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFF07FFC7E00000FEFFE0781FF81F81FE1FE07FFC0FFF03F83FFFFFFFFF0FF800000000003FFFFFFFFFFFFFCFFF07F800003FF03F03FFDFFF07FFF0FFFF07FFC")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "7E00000FEFFE0781FF81F81FE1FE07FFC0FFF03F83FFFFFFFFF0FF800000000003FFFFFFFFFFFFFCFFFF3F80001FFF80007FFDFFFC7FFF8FFFF07FFC7E00000F")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "EFFF0007FFE000FFE1FE07FFC0FFF80007FFFFFFFFF0FF800000000000FFFFDFFFFFFFFCFFFF3F80001FFF80007FFDFFFC7FFF8FFFF07FFC7E00000FEFFF0007")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFE000FFE1FE07FFC0FFF80007FFFFFFFFF0FFC000000000007FFFE1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFE000000000001FFFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFF0FFE000000000000FFFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFF0FFE000000000000FFFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFF0FFF0000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF8")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC00000000")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC000000000000FFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC0000000000007FFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF0000000000003FFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF8000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF8000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF8000000000303FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFC000000000801FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF01000000603EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF01000000603EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF80100007007F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFF0FFFFFFE001FFE003FF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFF0FFFFFFFE0000001FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFF0FFFFFFFE0000001FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFE00000FFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFF00")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.CARGA_LOGO_EN_CURSO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")

    '        hasar.CargarLogoEmisor(hfl.argentina.HasarImpresoraFiscalRG3561.OperacionesCargaLogoUsuario.FIN_CARGA_LOGO, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0")
    '        msj = msj & "Carga de LOGO finalizada ..."

    '        Me.Cursor = System.Windows.Forms.Cursors.Arrow
    '        MsgBox(msj, vbOKOnly + vbInformation, "Comando: CargarLogoEmisor() ...")
    '    Catch
    '        Me.Cursor = System.Windows.Forms.Cursors.Arrow
    '        msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
    '        MsgBox(msj, vbOKOnly + vbCritical, "ERROR CargarLogoEmisor() ...")
    '    End Try
    'End Sub

    '============================================================================================================================
    ' BOTÓN: ELIMINAR LOGO - Misceláneas ...
    '============================================================================================================================
    Private Sub ButtonElimLogo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonElimLogo.Click
        Dim msj As String        '// A mostrar en MsgBox()
        Dim cierre As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarJornadaFiscal

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            '// Para poder cargar el logo se requiere un cierre 'Z' previo
            cierre = hasar.CerrarJornadaFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporte.REPORTE_Z)

            msj = "ELIMINANDO LOGO EMISOR:" & vbCrLf & vbCrLf
            hasar.EliminarLogoEmisor()
            msj = msj & "LOGO eliminado ..."

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: EliminarLogoEmisor() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR EliminarLogoEmisor() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ENVIAR POR MAIL - Misceláneas ...
    '
    ' Debe estar configurado el servidor de correo. Aplicable, solamente, cuando el servicio de correo no utiliza SSL.
    '============================================================================================================================
    Private Sub ButtonEnviarCorreo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEnviarCorreo.Click
        Dim msj As String       '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.EnviarDocumentoCorreo(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A, 1, "cliente@suempresa.com")
            msj = "" & vbCrLf & vbCrLf & _
                  "Enviando:  Factura 'A' Nº 1 ::: Destino:  cliente@suempresa.com"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: EnviarDocumentoCorreo() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR EnviarDocumentoCorreo() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: COPIAR DOCUMENTO - Misceláneas ...
    '============================================================================================================================
    Private Sub ButtonCopiarDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCopiarDoc.Click
        Dim msj As String       '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.CopiarComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A, 99)
            msj = "REIMPRESIÓN : :" & vbCrLf & vbCrLf & _
                  "Copia de: Factura 'A' Nº 99 ..."

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: CopiarComprobante() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR CopiarComprobante() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: REIMPRIMIR DOC - Misceláneas ...
    '============================================================================================================================
    Private Sub ButtonReimpr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReimpr.Click
        Dim msj As String        '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            hasar.PedirReimpresion()
            msj = "REIMPRESIÓN :" & vbCrLf & vbCrLf & _
                  "Comprobante reimpreso ..."

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: PedirReimpresion() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR PedirReimpresion() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ACUM. COMPROBANTE - Acumulados/Reportes ...
    '============================================================================================================================
    Private Sub ButtonAcumComprob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAcumComprob.Click
        Dim comprob1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarAcumuladosComprobante
        Dim comprob2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaContinuarConsultaAcumulados
        Dim msj As String        '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            comprob1 = hasar.ConsultarAcumuladosComprobante(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE, 32)

            If (comprob1.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_FINAL) Then
                msj = "ACUMULADO COMPROBANTES :" & vbCrLf & _
                      "Acumulados de:  Tique, Nº 32 ..." & vbCrLf & vbCrLf & _
                      "No hay información disponible ..."

                Me.Cursor = System.Windows.Forms.Cursors.Arrow
                MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Comprobantes ...")
                Exit Sub
            End If

            Select Case comprob1.getRegistro()
                Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DF
                    msj = "ACUMULADO COMPROBANTES :" & vbCrLf & _
                          "Acumulados de:  Tique, Nº 32 ..." & vbCrLf & vbCrLf & _
                          "(DF) Documento Fiscal ..." & vbCrLf & _
                          "---------------------------" & vbCrLf & _
                          "Cód. Comprob." & vbTab & "=[" & comprob1.RegDF.getCodigoComprobante() & "] - " & ToStrCodComprob(comprob1.RegDF.getCodigoComprobante()) & vbCrLf & _
                          "Desde Nº" & vbTab & vbTab & "=[" & comprob1.RegDF.getNumeroInicial() & "]" & vbCrLf & _
                          "Hasta Nº" & vbTab & vbTab & "=[" & comprob1.RegDF.getNumeroFinal() & "]" & vbCrLf & _
                          "Cancelados" & vbTab & "=[" & comprob1.RegDF.getCantidadCancelados() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Comprobantes ...")

                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    msj = "ACUMULADO COMPROBANTES :" & vbCrLf & _
                          "Acumulados de:  Tique, Nº 32 ..." & vbCrLf & vbCrLf & _
                          "TOTAL" & vbTab & vbTab & "=[" & comprob1.RegDF.getTotal() & "]" & vbCrLf & vbCrLf & _
                          "Total Gravado" & vbTab & "=[" & comprob1.RegDF.getTotalGravado() & "]" & vbCrLf & _
                          "Total No Gravado" & vbTab & "=[" & comprob1.RegDF.getTotalNoGravado() & "]" & vbCrLf & _
                          "Total Exento" & vbTab & "=[" & comprob1.RegDF.getTotalExento() & "]" & vbCrLf & _
                          "Total IVA" & vbTab & vbTab & "=[" & comprob1.RegDF.getTotalIVA() & "]" & vbCrLf & _
                          "Total Otros Trib." & vbTab & "=[" & comprob1.RegDF.getTotalTributos() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Comprobantes ...")

                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    msj = "ACUMULADO COMPROBANTES :" & vbCrLf & _
                          "Acumulados de:  Tique, Nº 32 ..." & vbCrLf & vbCrLf & _
                          "Monto Base 1" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoNetoSinIVA_1() & "]" & vbCrLf & _
                          "Alícuota 1" & vbTab & vbTab & vbTab & "=[" & comprob1.RegDF.getAlicuotaIVA_1() & "]" & vbCrLf & _
                          "Monto IVA 1" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoIVA_1() & "]" & vbCrLf & _
                          "Monto Base 2" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoNetoSinIVA_2() & "]" & vbCrLf & _
                          "Alícuota 2" & vbTab & vbTab & vbTab & "=[" & comprob1.RegDF.getAlicuotaIVA_2() & "]" & vbCrLf & _
                          "Monto IVA 2" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoIVA_2() & "]" & vbCrLf & _
                          "Monto Base 3" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoNetoSinIVA_3() & "]" & vbCrLf & _
                          "Alícuota 3" & vbTab & vbTab & vbTab & "=[" & comprob1.RegDF.getAlicuotaIVA_3() & "]" & vbCrLf & _
                          "Monto IVA 3" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoIVA_3() & "]" & vbCrLf & _
                          "Monto Base 4" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoNetoSinIVA_4() & "]" & vbCrLf & _
                          "Alícuota 4" & vbTab & vbTab & vbTab & "=[" & comprob1.RegDF.getAlicuotaIVA_4() & "]" & vbCrLf & _
                          "Monto IVA 4" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoIVA_4() & "]" & vbCrLf & _
                          "Monto Base 5" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoNetoSinIVA_5() & "]" & vbCrLf & _
                          "Alícuota 5" & vbTab & vbTab & vbTab & "=[" & comprob1.RegDF.getAlicuotaIVA_5() & "]" & vbCrLf & _
                          "Monto IVA 5" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoIVA_5() & "]" & vbCrLf & _
                          "Monto Base 6" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoNetoSinIVA_6() & "]" & vbCrLf & _
                          "Alícuota 6" & vbTab & vbTab & vbTab & "=[" & comprob1.RegDF.getAlicuotaIVA_6() & "]" & vbCrLf & _
                          "Monto IVA 6" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoIVA_6() & "]" & vbCrLf & _
                          "Monto Base 7" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoNetoSinIVA_7() & "]" & vbCrLf & _
                          "Alícuota 7" & vbTab & vbTab & vbTab & "=[" & comprob1.RegDF.getAlicuotaIVA_7() & "]" & vbCrLf & _
                          "Monto IVA 7" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoIVA_7() & "]" & vbCrLf & _
                          "Monto Base 8" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoNetoSinIVA_8() & "]" & vbCrLf & _
                          "Alícuota 8" & vbTab & vbTab & vbTab & "=[" & comprob1.RegDF.getAlicuotaIVA_8() & "]" & vbCrLf & _
                          "Monto IVA 8" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoIVA_8() & "]" & vbCrLf & _
                          "Monto Base 9" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoNetoSinIVA_9() & "]" & vbCrLf & _
                          "Alícuota 9" & vbTab & vbTab & vbTab & "=[" & comprob1.RegDF.getAlicuotaIVA_9() & "]" & vbCrLf & _
                          "Monto IVA 9" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoIVA_9() & "]" & vbCrLf & _
                          "Monto Base 10" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoNetoSinIVA_10() & "]" & vbCrLf & _
                          "Alícuota 10" & vbTab & vbTab & "=[" & comprob1.RegDF.getAlicuotaIVA_10() & "]" & vbCrLf & _
                          "Monto IVA 10" & vbTab & vbTab & "=[" & comprob1.RegDF.getMontoIVA_10() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Comprobantes ...")

                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    msj = "ACUMULADO COMPROBANTES :" & vbCrLf & _
                          "Acumulados de:  Tique, Nº 32 ..." & vbCrLf & vbCrLf & _
                          "Cód. Tributo 1" & vbTab & "=[" & comprob1.RegDF.getCodigoTributo1() & "] - " & ToStrCodTributo(comprob1.RegDF.getCodigoTributo1()) & vbCrLf & _
                          "Importe 1" & vbTab & vbTab & "=[" & comprob1.RegDF.getImporteTributo1() & "]" & vbCrLf & _
                          "Cód. Tributo 2" & vbTab & "=[" & comprob1.RegDF.getCodigoTributo2() & "] - " & ToStrCodTributo(comprob1.RegDF.getCodigoTributo2()) & vbCrLf & _
                          "Importe 2" & vbTab & vbTab & "=[" & comprob1.RegDF.getImporteTributo2() & "]" & vbCrLf & _
                          "Cód. Tributo 3" & vbTab & "=[" & comprob1.RegDF.getCodigoTributo3() & "] - " & ToStrCodTributo(comprob1.RegDF.getCodigoTributo3()) & vbCrLf & _
                          "Importe 3" & vbTab & vbTab & "=[" & comprob1.RegDF.getImporteTributo3() & "]" & vbCrLf & _
                          "Cód. Tributo 4" & vbTab & "=[" & comprob1.RegDF.getCodigoTributo4() & "] - " & ToStrCodTributo(comprob1.RegDF.getCodigoTributo4()) & vbCrLf & _
                          "Importe 4" & vbTab & vbTab & "=[" & comprob1.RegDF.getImporteTributo4() & "]" & vbCrLf & _
                          "Cód. Tributo 5" & vbTab & "=[" & comprob1.RegDF.getCodigoTributo5() & "] - " & ToStrCodTributo(comprob1.RegDF.getCodigoTributo5()) & vbCrLf & _
                          "Importe 5" & vbTab & vbTab & "=[" & comprob1.RegDF.getImporteTributo5() & "]" & vbCrLf & _
                          "Cód. Tributo 6" & vbTab & "=[" & comprob1.RegDF.getCodigoTributo6() & "] - " & ToStrCodTributo(comprob1.RegDF.getCodigoTributo6()) & vbCrLf & _
                          "Importe 6" & vbTab & vbTab & "=[" & comprob1.RegDF.getImporteTributo6() & "]" & vbCrLf & _
                          "Cód. Tributo 7" & vbTab & "=[" & comprob1.RegDF.getCodigoTributo7() & "] - " & ToStrCodTributo(comprob1.RegDF.getCodigoTributo7()) & vbCrLf & _
                          "Importe 7" & vbTab & vbTab & "=[" & comprob1.RegDF.getImporteTributo7() & "]" & vbCrLf & _
                          "Cód. Tributo 8" & vbTab & "=[" & comprob1.RegDF.getCodigoTributo8() & "] - " & ToStrCodTributo(comprob1.RegDF.getCodigoTributo8()) & vbCrLf & _
                          "Importe 8" & vbTab & vbTab & "=[" & comprob1.RegDF.getImporteTributo8() & "]" & vbCrLf & _
                          "Cód. Tributo 9" & vbTab & "=[" & comprob1.RegDF.getCodigoTributo9() & "] - " & ToStrCodTributo(comprob1.RegDF.getCodigoTributo9()) & vbCrLf & _
                          "Importe 9" & vbTab & vbTab & "=[" & comprob1.RegDF.getImporteTributo9() & "]" & vbCrLf & _
                          "Cód. Tributo 10" & vbTab & "=[" & comprob1.RegDF.getCodigoTributo10() & "] - " & ToStrCodTributo(comprob1.RegDF.getCodigoTributo10()) & vbCrLf & _
                          "Importe 10" & vbTab & "=[" & comprob1.RegDF.getImporteTributo10() & "]" & vbCrLf & _
                          "Cód. Tributo 11" & vbTab & "=[" & comprob1.RegDF.getCodigoTributo11() & "] - " & ToStrCodTributo(comprob1.RegDF.getCodigoTributo11()) & vbCrLf & _
                          "Importe 11" & vbTab & "=[" & comprob1.RegDF.getImporteTributo11() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Comprobantes ...")
                Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DNFH
                    msj = "ACUMULADO COMPROBANTES :" & vbCrLf & _
                          "Acumulados de: ..." & vbCrLf & vbCrLf & _
                          "(DNFH) Documento NO Fiscal Homologado (que SI acumula) ..." & vbCrLf & _
                          "--------------------------------------------------------------------" & vbCrLf & _
                          "Cód. Comprob." & vbTab & "=[" & comprob1.RegDNFH.getCodigoComprobante() & "] - " & ToStrCodComprob(comprob1.RegDNFH.getCodigoComprobante()) & vbCrLf & _
                          "Desde Nº" & vbTab & vbTab & "=[" & comprob1.RegDNFH.getNumeroInicial() & "]" & vbCrLf & _
                          "Hasta Nº" & vbTab & vbTab & "=[" & comprob1.RegDNFH.getNumeroFinal() & "]" & vbCrLf & vbCrLf & _
                          "TOTAL" & vbTab & vbTab & "=[" & comprob1.RegDNFH.getTotal() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Comprobantes ...")
                Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DNFH_NO_ACUM
                    msj = "ACUMULADO COMPROBANTES :" & vbCrLf & _
                          "Acumulados de: ..." & vbCrLf & vbCrLf & _
                          "(DNFH) Documento NO Fiscal Homologado (que NO acumula) ..." & vbCrLf & _
                          "----------------------------------------------------------------------" & vbCrLf & _
                          "Cód. Comprob." & vbTab & "=[" & comprob1.RegDNFH_NoAcum.getCodigoComprobante() & "] - " & ToStrCodComprob(comprob1.RegDNFH_NoAcum.getCodigoComprobante()) & vbCrLf & _
                          "Desde Nº" & vbTab & vbTab & "=[" & comprob1.RegDNFH_NoAcum.getNumeroInicial() & "]" & vbCrLf & _
                          "Hasta Nº" & vbTab & vbTab & "=[" & comprob1.RegDNFH_NoAcum.getNumeroFinal() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Comprobantes ...")
                Case Else
                    msj = "ACUMULADO COMPROBANTES :" & vbCrLf & _
                          "Acumulados de:  Tique, Nº 32 ..." & vbCrLf & vbCrLf & _
                          "Primer registro de información: DESCONOCIDO ! ..."

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbCritical, "Acumulado Comprobantes ...")
                    Exit Sub
            End Select

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            comprob2 = hasar.ContinuarConsultaAcumulados()

            While (comprob2.getRegistro() <> hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_FINAL)
                Select Case comprob2.getRegistro()
                    Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_GLOBAL
                        msj = "ACUMULADO COMPROBANTES :" & vbCrLf & _
                              "Acumulados de:  Tique, Nº 32 ..." & vbCrLf & vbCrLf & _
                              "Información Global ..." & vbCrLf & _
                              "-----------------------" & vbCrLf & _
                              "DF Emitidos" & vbTab & vbTab & "=[" & comprob2.RegGlobal.getDF_CantidadEmitidos() & "]" & vbCrLf & _
                              "DF Cancelados" & vbTab & vbTab & "=[" & comprob2.RegGlobal.getDF_CantidadCancelados() & "]" & vbCrLf & _
                              "DF TOTAL" & vbTab & vbTab & vbTab & "=[" & comprob2.RegGlobal.getDF_Total() & "]" & vbCrLf & vbCrLf & _
                              "DF Total Gravado" & vbTab & vbTab & "=[" & comprob2.RegGlobal.getDF_TotalGravado() & "]" & vbCrLf & _
                              "DF Total No Gravado" & vbTab & vbTab & "=[" & comprob2.RegGlobal.getDF_TotalNoGravado() & "]" & vbCrLf & _
                              "DF Total Exento" & vbTab & vbTab & "=[" & comprob2.RegGlobal.getDF_TotalExento() & "]" & vbCrLf & _
                              "DF Total IVA" & vbTab & vbTab & "=[" & comprob2.RegGlobal.getDF_TotalIVA() & "]" & vbCrLf & _
                              "DF Total Tributos" & vbTab & vbTab & "=[" & comprob2.RegGlobal.getDF_TotalTributos() & "]" & vbCrLf & vbCrLf & _
                              "DNFH TOTAL" & vbTab & vbTab & "=[" & comprob2.RegGlobal.getDNFH_Total() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Comprobantes ...")
                    Case Else
                        msj = "ACUMULADO COMPROBANTES :" & vbCrLf & _
                              "Acumulados de:  Tique, Nº 32 ..." & vbCrLf & vbCrLf & _
                              "Registro de información: DESCONOCIDO ! ..."

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbCritical, "Acumulado Comprobantes ...")
                        Exit Sub
                End Select

                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                comprob2 = hasar.ContinuarConsultaAcumulados()
            End While

            msj = "ACUMULADO COMPROBANTES :" & vbCrLf & _
                  "Acumulados de:  Tique, Nº 32 ..." & vbCrLf & vbCrLf & _
                  "No hay más información disponible ..."

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Comprobantes ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Acumulado comprobantes ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ACUM. MEM. de TRABAJO - Acumulados/Reportes ...
    '============================================================================================================================
    Private Sub ButtonAcumMemTrab_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAcumMemTrab.Click
        Dim memo1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarAcumuladosMemoriaDeTrabajo
        Dim memo2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaContinuarConsultaAcumulados
        Dim msj As String      '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            memo1 = hasar.ConsultarAcumuladosMemoriaDeTrabajo(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NO_DOCUMENTO)

            If (memo1.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_FINAL) Then
                msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                      "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                      "No hay información disponible ..."

                Me.Cursor = System.Windows.Forms.Cursors.Arrow
                MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")
                Exit Sub
            End If

            Select Case memo1.getRegistro()
                Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DF
                    msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                          "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                          "(DF) Documento Fiscal ..." & vbCrLf & _
                          "---------------------------" & vbCrLf & _
                          "Cód. Comprob." & vbTab & "=[" & memo1.RegDF.getCodigoComprobante() & "] - " & ToStrCodComprob(memo1.RegDF.getCodigoComprobante()) & vbCrLf & _
                          "Desde Nº" & vbTab & vbTab & "=[" & memo1.RegDF.getNumeroInicial() & "]" & vbCrLf & _
                          "Hasta Nº" & vbTab & vbTab & "=[" & memo1.RegDF.getNumeroFinal() & "]" & vbCrLf & _
                          "Cancelados" & vbTab & "=[" & memo1.RegDF.getCantidadCancelados() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")

                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                          "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                          "(DF) Documento Fiscal ..." & vbCrLf & _
                          "---------------------------" & vbCrLf & _
                          "TOTAL" & vbTab & vbTab & "=[" & memo1.RegDF.getTotal() & "]" & vbCrLf & vbCrLf & _
                          "Total Gravado" & vbTab & "=[" & memo1.RegDF.getTotalGravado() & "]" & vbCrLf & _
                          "Total No Gravado" & vbTab & "=[" & memo1.RegDF.getTotalNoGravado() & "]" & vbCrLf & _
                          "Total Exento" & vbTab & "=[" & memo1.RegDF.getTotalExento() & "]" & vbCrLf & _
                          "Total IVA" & vbTab & vbTab & "=[" & memo1.RegDF.getTotalIVA() & "]" & vbCrLf & _
                          "Total Otros Trib." & vbTab & "=[" & memo1.RegDF.getTotalTributos() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")

                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                          "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                          "(DF) Documento Fiscal ..." & vbCrLf & _
                          "---------------------------" & vbCrLf & _
                          "Alícuota 1" & vbTab & vbTab & "=[" & memo1.RegDF.getAlicuotaIVA_1() & "]" & vbCrLf & _
                          "Monto IVA 1" & vbTab & "=[" & memo1.RegDF.getMontoIVA_1() & "]" & vbCrLf & _
                          "Alícuota 2" & vbTab & vbTab & "=[" & memo1.RegDF.getAlicuotaIVA_2() & "]" & vbCrLf & _
                          "Monto IVA 2" & vbTab & "=[" & memo1.RegDF.getMontoIVA_2() & "]" & vbCrLf & _
                          "Alícuota 3" & vbTab & vbTab & "=[" & memo1.RegDF.getAlicuotaIVA_3() & "]" & vbCrLf & _
                          "Monto IVA 3" & vbTab & "=[" & memo1.RegDF.getMontoIVA_3() & "]" & vbCrLf & _
                          "Alícuota 4" & vbTab & vbTab & "=[" & memo1.RegDF.getAlicuotaIVA_4() & "]" & vbCrLf & _
                          "Monto IVA 4" & vbTab & "=[" & memo1.RegDF.getMontoIVA_4() & "]" & vbCrLf & _
                          "Alícuota 5" & vbTab & vbTab & "=[" & memo1.RegDF.getAlicuotaIVA_5() & "]" & vbCrLf & _
                          "Monto IVA 5" & vbTab & "=[" & memo1.RegDF.getMontoIVA_5() & "]" & vbCrLf & _
                          "Alícuota 6" & vbTab & vbTab & "=[" & memo1.RegDF.getAlicuotaIVA_6() & "]" & vbCrLf & _
                          "Monto IVA 6" & vbTab & "=[" & memo1.RegDF.getMontoIVA_6() & "]" & vbCrLf & _
                          "Alícuota 7" & vbTab & vbTab & "=[" & memo1.RegDF.getAlicuotaIVA_7() & "]" & vbCrLf & _
                          "Monto IVA 7" & vbTab & "=[" & memo1.RegDF.getMontoIVA_7() & "]" & vbCrLf & _
                          "Alícuota 8" & vbTab & vbTab & "=[" & memo1.RegDF.getAlicuotaIVA_8() & "]" & vbCrLf & _
                          "Monto IVA 8" & vbTab & "=[" & memo1.RegDF.getMontoIVA_8() & "]" & vbCrLf & _
                          "Alícuota 9" & vbTab & vbTab & "=[" & memo1.RegDF.getAlicuotaIVA_9() & "]" & vbCrLf & _
                          "Monto IVA 9" & vbTab & "=[" & memo1.RegDF.getMontoIVA_9() & "]" & vbCrLf & _
                          "Alícuota 10" & vbTab & "=[" & memo1.RegDF.getAlicuotaIVA_10() & "]" & vbCrLf & _
                          "Monto IVA 10" & vbTab & "=[" & memo1.RegDF.getMontoIVA_10() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                          "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                          "(DF) Documento Fiscal ..." & vbCrLf & _
                          "---------------------------" & vbCrLf & _
                          "Cód. Tributo 1" & vbTab & "=[" & memo1.RegDF.getCodigoTributo1() & "] - " & ToStrCodTributo(memo1.RegDF.getCodigoTributo1()) & vbCrLf & _
                          "Importe 1" & vbTab & vbTab & "=[" & memo1.RegDF.getImporteTributo1() & "]" & vbCrLf & _
                          "Cód. Tributo 2" & vbTab & "=[" & memo1.RegDF.getCodigoTributo2() & "] - " & ToStrCodTributo(memo1.RegDF.getCodigoTributo2()) & vbCrLf & _
                          "Importe 2" & vbTab & vbTab & "=[" & memo1.RegDF.getImporteTributo2() & "]" & vbCrLf & _
                          "Cód. Tributo 3" & vbTab & "=[" & memo1.RegDF.getCodigoTributo3() & "] - " & ToStrCodTributo(memo1.RegDF.getCodigoTributo3()) & vbCrLf & _
                          "Importe 3" & vbTab & vbTab & "=[" & memo1.RegDF.getImporteTributo3() & "]" & vbCrLf & _
                          "Cód. Tributo 4" & vbTab & "=[" & memo1.RegDF.getCodigoTributo4() & "] - " & ToStrCodTributo(memo1.RegDF.getCodigoTributo4()) & vbCrLf & _
                          "Importe 4" & vbTab & vbTab & "=[" & memo1.RegDF.getImporteTributo4() & "]" & vbCrLf & _
                          "Cód. Tributo 5" & vbTab & "=[" & memo1.RegDF.getCodigoTributo5() & "] - " & ToStrCodTributo(memo1.RegDF.getCodigoTributo5()) & vbCrLf & _
                          "Importe 5" & vbTab & vbTab & "=[" & memo1.RegDF.getImporteTributo5() & "]" & vbCrLf & _
                          "Cód. Tributo 6" & vbTab & "=[" & memo1.RegDF.getCodigoTributo6() & "] - " & ToStrCodTributo(memo1.RegDF.getCodigoTributo6()) & vbCrLf & _
                          "Importe 6" & vbTab & vbTab & "=[" & memo1.RegDF.getImporteTributo6() & "]" & vbCrLf & _
                          "Cód. Tributo 7" & vbTab & "=[" & memo1.RegDF.getCodigoTributo7() & "] - " & ToStrCodTributo(memo1.RegDF.getCodigoTributo7()) & vbCrLf & _
                          "Importe 7" & vbTab & vbTab & "=[" & memo1.RegDF.getImporteTributo7() & "]" & vbCrLf & _
                          "Cód. Tributo 8" & vbTab & "=[" & memo1.RegDF.getCodigoTributo8() & "] - " & ToStrCodTributo(memo1.RegDF.getCodigoTributo8()) & vbCrLf & _
                          "Importe 8" & vbTab & vbTab & "=[" & memo1.RegDF.getImporteTributo8() & "]" & vbCrLf & _
                          "Cód. Tributo 9" & vbTab & "=[" & memo1.RegDF.getCodigoTributo9() & "] - " & ToStrCodTributo(memo1.RegDF.getCodigoTributo9()) & vbCrLf & _
                          "Importe 9" & vbTab & vbTab & "=[" & memo1.RegDF.getImporteTributo9() & "]" & vbCrLf & _
                          "Cód. Tributo 10" & vbTab & "=[" & memo1.RegDF.getCodigoTributo10() & "] - " & ToStrCodTributo(memo1.RegDF.getCodigoTributo10()) & vbCrLf & _
                          "Importe 10" & vbTab & "=[" & memo1.RegDF.getImporteTributo10() & "]" & vbCrLf & _
                          "Cód. Tributo 11" & vbTab & "=[" & memo1.RegDF.getCodigoTributo11() & "] - " & ToStrCodTributo(memo1.RegDF.getCodigoTributo11()) & vbCrLf & _
                          "Importe 11" & vbTab & "=[" & memo1.RegDF.getImporteTributo11() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")
                Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DNFH
                    msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                          "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                          "(DNFH) Documento NO Fiscal Homologado (que SI acumula) ..." & vbCrLf & _
                          "--------------------------------------------------------------------" & vbCrLf & vbCrLf & _
                          "Cód. Comprob." & vbTab & "=[" & memo1.RegDNFH.getCodigoComprobante() & "] - " & ToStrCodComprob(memo1.RegDNFH.getCodigoComprobante()) & vbCrLf & _
                          "Desde Nº" & vbTab & vbTab & "=[" & memo1.RegDNFH.getNumeroInicial() & "]" & vbCrLf & _
                          "Hasta Nº" & vbTab & vbTab & "=[" & memo1.RegDNFH.getNumeroFinal() & "]" & vbCrLf & vbCrLf & _
                          "TOTAL" & vbTab & vbTab & "=[" & memo1.RegDNFH.getTotal() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")
                Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DNFH_NO_ACUM
                    msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                          "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                          "(DNFH) Documento NO Fiscal Homologado (que NO acumula) ..." & vbCrLf & _
                          "----------------------------------------------------------" & vbCrLf & vbCrLf & _
                          "Cód. Comprob." & vbTab & "=[" & memo1.RegDNFH_NoAcum.getCodigoComprobante() & "] - " & ToStrCodComprob(memo1.RegDNFH_NoAcum.getCodigoComprobante()) & vbCrLf & _
                          "Desde Nº" & vbTab & vbTab & "=[" & memo1.RegDNFH_NoAcum.getNumeroInicial() & "]" & vbCrLf & _
                          "Hasta Nº" & vbTab & vbTab & "=[" & memo1.RegDNFH_NoAcum.getNumeroFinal() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")
                Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_GLOBAL
                    msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                          "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                          "Información Global ..." & vbCrLf & _
                          "-----------------------" & vbCrLf & vbCrLf & _
                          "DF Emitidos" & vbTab & vbTab & "=[" & memo1.RegGlobal.getDF_CantidadEmitidos() & "]" & vbCrLf & _
                          "DF Cancelados" & vbTab & vbTab & "=[" & memo1.RegGlobal.getDF_CantidadCancelados() & "]" & vbCrLf & _
                          "DF TOTAL" & vbTab & vbTab & vbTab & "=[" & memo1.RegGlobal.getDF_Total() & "]" & vbCrLf & vbCrLf & _
                          "DF Total Gravado" & vbTab & vbTab & "=[" & memo1.RegGlobal.getDF_TotalGravado() & "]" & vbCrLf & _
                          "DF Total No Gravado" & vbTab & vbTab & "=[" & memo1.RegGlobal.getDF_TotalNoGravado() & "]" & vbCrLf & _
                          "DF Total Exento" & vbTab & vbTab & "=[" & memo1.RegGlobal.getDF_TotalExento() & "]" & vbCrLf & _
                          "DF Total IVA" & vbTab & vbTab & "=[" & memo1.RegGlobal.getDF_TotalIVA() & "]" & vbCrLf & _
                          "DF Total Tributos" & vbTab & vbTab & "=[" & memo1.RegGlobal.getDF_TotalTributos() & "]" & vbCrLf & vbCrLf & _
                          "DNFH TOTAL" & vbTab & vbTab & "=[" & memo1.RegGlobal.getDNFH_Total() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")
                Case Else
                    msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                          "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                          "Primer registro de información: DESCONOCIDO ! ..."

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbCritical, "Acumulado Comprobantes ...")
                    Exit Sub
            End Select

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            memo2 = hasar.ContinuarConsultaAcumulados()

            While (memo2.getRegistro() <> hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_FINAL)
                Select Case memo2.getRegistro()
                    Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DF
                        msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                              "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                              "(DF) Documento Fiscal ..." & vbCrLf & _
                              "---------------------------" & vbCrLf & _
                              "Cód. Comprob." & vbTab & "=[" & memo2.RegDF.getCodigoComprobante() & "] - " & ToStrCodComprob(memo2.RegDF.getCodigoComprobante()) & vbCrLf & _
                              "Desde Nº" & vbTab & vbTab & "=[" & memo2.RegDF.getNumeroInicial() & "]" & vbCrLf & _
                              "Hasta Nº" & vbTab & vbTab & "=[" & memo2.RegDF.getNumeroFinal() & "]" & vbCrLf & _
                              "Cancelados" & vbTab & "=[" & memo2.RegDF.getCantidadCancelados() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")

                        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                        msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                              "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                              "(DF) Documento Fiscal ..." & vbCrLf & _
                              "---------------------------" & vbCrLf & _
                              "TOTAL" & vbTab & vbTab & "=[" & memo2.RegDF.getTotal() & "]" & vbCrLf & vbCrLf & _
                              "Total Gravado" & vbTab & "=[" & memo2.RegDF.getTotalGravado() & "]" & vbCrLf & _
                              "Total No Gravado" & vbTab & "=[" & memo2.RegDF.getTotalNoGravado() & "]" & vbCrLf & _
                              "Total Exento" & vbTab & "=[" & memo2.RegDF.getTotalExento() & "]" & vbCrLf & _
                              "Total IVA" & vbTab & vbTab & "=[" & memo2.RegDF.getTotalIVA() & "]" & vbCrLf & _
                              "Total Otros Trib." & vbTab & "=[" & memo2.RegDF.getTotalTributos() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly, "Acumulado memoria de trabajo ...")

                        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                        msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                              "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                              "(DF) Documento Fiscal ..." & vbCrLf & _
                              "---------------------------" & vbCrLf & _
                              "Alícuota 1" & vbTab & vbTab & "=[" & memo2.RegDF.getAlicuotaIVA_1() & "]" & vbCrLf & _
                              "Monto IVA 1" & vbTab & "=[" & memo2.RegDF.getMontoIVA_1() & "]" & vbCrLf & _
                              "Alícuota 2" & vbTab & vbTab & "=[" & memo2.RegDF.getAlicuotaIVA_2() & "]" & vbCrLf & _
                              "Monto IVA 2" & vbTab & "=[" & memo2.RegDF.getMontoIVA_2() & "]" & vbCrLf & _
                              "Alícuota 3" & vbTab & vbTab & "=[" & memo2.RegDF.getAlicuotaIVA_3() & "]" & vbCrLf & _
                              "Monto IVA 3" & vbTab & "=[" & memo2.RegDF.getMontoIVA_3() & "]" & vbCrLf & _
                              "Alícuota 4" & vbTab & vbTab & "=[" & memo2.RegDF.getAlicuotaIVA_4() & "]" & vbCrLf & _
                              "Monto IVA 4" & vbTab & "=[" & memo2.RegDF.getMontoIVA_4() & "]" & vbCrLf & _
                              "Alícuota 5" & vbTab & vbTab & "=[" & memo2.RegDF.getAlicuotaIVA_5() & "]" & vbCrLf & _
                              "Monto IVA 5" & vbTab & "=[" & memo2.RegDF.getMontoIVA_5() & "]" & vbCrLf & _
                              "Alícuota 6" & vbTab & vbTab & "=[" & memo2.RegDF.getAlicuotaIVA_6() & "]" & vbCrLf & _
                              "Monto IVA 6" & vbTab & "=[" & memo2.RegDF.getMontoIVA_6() & "]" & vbCrLf & _
                              "Alícuota 7" & vbTab & vbTab & "=[" & memo2.RegDF.getAlicuotaIVA_7() & "]" & vbCrLf & _
                              "Monto IVA 7" & vbTab & "=[" & memo2.RegDF.getMontoIVA_7() & "]" & vbCrLf & _
                              "Alícuota 8" & vbTab & vbTab & "=[" & memo2.RegDF.getAlicuotaIVA_8() & "]" & vbCrLf & _
                              "Monto IVA 8" & vbTab & "=[" & memo2.RegDF.getMontoIVA_8() & "]" & vbCrLf & _
                              "Alícuota 9" & vbTab & vbTab & "=[" & memo2.RegDF.getAlicuotaIVA_9() & "]" & vbCrLf & _
                              "Monto IVA 9" & vbTab & "=[" & memo2.RegDF.getMontoIVA_9() & "]" & vbCrLf & _
                              "Alícuota 10" & vbTab & "=[" & memo2.RegDF.getAlicuotaIVA_10() & "]" & vbCrLf & _
                              "Monto IVA 10" & vbTab & "=[" & memo2.RegDF.getMontoIVA_10() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")

                        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                        msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                              "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                              "(DF) Documento Fiscal ..." & vbCrLf & _
                              "---------------------------" & vbCrLf & _
                              "Cód. Tributo 1" & vbTab & "=[" & memo2.RegDF.getCodigoTributo1() & "] - " & ToStrCodTributo(memo2.RegDF.getCodigoTributo1()) & vbCrLf & _
                              "Importe 1" & vbTab & vbTab & "=[" & memo2.RegDF.getImporteTributo1() & "]" & vbCrLf & _
                              "Cód. Tributo 2" & vbTab & "=[" & memo2.RegDF.getCodigoTributo2() & "]- " & ToStrCodTributo(memo2.RegDF.getCodigoTributo2()) & vbCrLf & _
                              "Importe 2" & vbTab & vbTab & "=[" & memo2.RegDF.getImporteTributo2() & "]" & vbCrLf & _
                              "Cód. Tributo 3" & vbTab & "=[" & memo2.RegDF.getCodigoTributo3() & "] - " & ToStrCodTributo(memo2.RegDF.getCodigoTributo3()) & vbCrLf & _
                              "Importe 3" & vbTab & vbTab & "=[" & memo2.RegDF.getImporteTributo3() & "]" & vbCrLf & _
                              "Cód. Tributo 4" & vbTab & "=[" & memo2.RegDF.getCodigoTributo4() & "] - " & ToStrCodTributo(memo2.RegDF.getCodigoTributo4()) & vbCrLf & _
                              "Importe 4" & vbTab & vbTab & "=[" & memo2.RegDF.getImporteTributo4() & "]" & vbCrLf & _
                              "Cód. Tributo 5" & vbTab & "=[" & memo2.RegDF.getCodigoTributo5() & "] - " & ToStrCodTributo(memo2.RegDF.getCodigoTributo5()) & vbCrLf & _
                              "Importe 5" & vbTab & vbTab & "=[" & memo2.RegDF.getImporteTributo5() & "]" & vbCrLf & _
                              "Cód. Tributo 6" & vbTab & "=[" & memo2.RegDF.getCodigoTributo6() & "] - " & ToStrCodTributo(memo2.RegDF.getCodigoTributo6()) & vbCrLf & _
                              "Importe 6" & vbTab & vbTab & "=[" & memo2.RegDF.getImporteTributo6() & "]" & vbCrLf & _
                              "Cód. Tributo 7" & vbTab & "=[" & memo2.RegDF.getCodigoTributo7() & "] - " & ToStrCodTributo(memo2.RegDF.getCodigoTributo7()) & vbCrLf & _
                              "Importe 7" & vbTab & vbTab & "=[" & memo2.RegDF.getImporteTributo7() & "]" & vbCrLf & _
                              "Cód. Tributo 8" & vbTab & "=[" & memo2.RegDF.getCodigoTributo8() & "] - " & ToStrCodTributo(memo2.RegDF.getCodigoTributo8()) & vbCrLf & _
                              "Importe 8" & vbTab & vbTab & "=[" & memo2.RegDF.getImporteTributo8() & "]" & vbCrLf & _
                              "Cód. Tributo 9" & vbTab & "=[" & memo2.RegDF.getCodigoTributo9() & "] - " & ToStrCodTributo(memo2.RegDF.getCodigoTributo9()) & vbCrLf & _
                              "Importe 9" & vbTab & vbTab & "=[" & memo2.RegDF.getImporteTributo9() & "]" & vbCrLf & _
                              "Cód. Tributo 10" & vbTab & "=[" & memo2.RegDF.getCodigoTributo10() & "] - " & ToStrCodTributo(memo2.RegDF.getCodigoTributo10()) & vbCrLf & _
                              "Importe 10" & vbTab & "=[" & memo2.RegDF.getImporteTributo10() & "]" & vbCrLf & _
                              "Cód. Tributo 11" & vbTab & "=[" & memo2.RegDF.getCodigoTributo11() & "]- " & ToStrCodTributo(memo2.RegDF.getCodigoTributo11()) & vbCrLf & _
                              "Importe 11" & vbTab & "=[" & memo2.RegDF.getImporteTributo11() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")
                    Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DNFH
                        msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                              "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                              "(DNFH) Documento NO Fiscal Homologado (que SI acumula) ..." & vbCrLf & _
                              "--------------------------------------------------------------------" & vbCrLf & vbCrLf & _
                              "Cód. Comprob." & vbTab & "=[" & memo2.RegDNFH.getCodigoComprobante() & "] - " & ToStrCodComprob(memo2.RegDNFH.getCodigoComprobante()) & vbCrLf & _
                              "Desde Nº" & vbTab & vbTab & "=[" & memo2.RegDNFH.getNumeroInicial() & "]" & vbCrLf & _
                              "Hasta Nº" & vbTab & vbTab & "=[" & memo2.RegDNFH.getNumeroFinal() & "]" & vbCrLf & vbCrLf & _
                              "TOTAL" & vbTab & vbTab & "=[" & memo2.RegDNFH.getTotal() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")
                    Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DNFH_NO_ACUM
                        msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                              "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                              "(DNFH) Documento NO Fiscal Homologado (que NO acumula) ..." & vbCrLf & _
                              "----------------------------------------------------------------------" & vbCrLf & vbCrLf & _
                              "Cód. Comprob." & vbTab & "=[" & memo2.RegDNFH_NoAcum.getCodigoComprobante() & "] - " & ToStrCodComprob(memo2.RegDNFH_NoAcum.getCodigoComprobante()) & vbCrLf & _
                              "Desde Nº" & vbTab & vbTab & "=[" & memo2.RegDNFH_NoAcum.getNumeroInicial() & "]" & vbCrLf & _
                              "Hasta Nº" & vbTab & vbTab & "=[" & memo2.RegDNFH_NoAcum.getNumeroFinal() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")
                    Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_GLOBAL
                        msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                              "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                              "Información Global ..." & vbCrLf & _
                              "-----------------------" & vbCrLf & vbCrLf & _
                              "DF Emitidos" & vbTab & vbTab & "=[" & memo2.RegGlobal.getDF_CantidadEmitidos() & "]" & vbCrLf & _
                              "DF Cancelados" & vbTab & vbTab & "=[" & memo2.RegGlobal.getDF_CantidadCancelados() & "]" & vbCrLf & _
                              "DF TOTAL" & vbTab & vbTab & vbTab & "=[" & memo2.RegGlobal.getDF_Total() & "]" & vbCrLf & vbCrLf & _
                              "DF Total Gravado" & vbTab & vbTab & "=[" & memo2.RegGlobal.getDF_TotalGravado() & "]" & vbCrLf & _
                              "DF Total No Gravado" & vbTab & vbTab & "=[" & memo2.RegGlobal.getDF_TotalNoGravado() & "]" & vbCrLf & _
                              "DF Total Exento" & vbTab & vbTab & "=[" & memo2.RegGlobal.getDF_TotalExento() & "]" & vbCrLf & _
                              "DF Total IVA" & vbTab & vbTab & "=[" & memo2.RegGlobal.getDF_TotalIVA() & "]" & vbCrLf & _
                              "DF Total Tributos" & vbTab & vbTab & "=[" & memo2.RegGlobal.getDF_TotalTributos() & "]" & vbCrLf & vbCrLf & _
                              "DNFH TOTAL" & vbTab & vbTab & "=[" & memo2.RegGlobal.getDNFH_Total() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")
                    Case Else
                        msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                              "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                              "Registro de información: DESCONOCIDO ! ..."

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbCritical, "Acumulado memoria de trabajo ...")
                        Exit Sub
                End Select

                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                memo2 = hasar.ContinuarConsultaAcumulados()
            End While

            msj = "ACUMULADO MEMORIA DE TRABAJO :" & vbCrLf & _
                  "Acumulados de:  NO_DOCUMENTO ..." & vbCrLf & vbCrLf & _
                  "No hay más información disponible ..."

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Acumulado memoria de trabajo ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Acumulado memoria de trabajo ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ACUMULADOS CIERRE 'Z' - Acumulados/Reportes ...
    '============================================================================================================================
    Private Sub ButtonAcumZeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAcumZeta.Click
        Dim cierre1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarAcumuladosCierreZeta
        Dim cierre2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaContinuarConsultaAcumulados
        Dim msj As String       '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            cierre1 = hasar.ConsultarAcumuladosCierreZeta(hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteZ.REPORTE_Z_NUMERO, "6", hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NO_DOCUMENTO)

            If (cierre1.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_FINAL) Then
                msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                       "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                      "No hay información disponible ..."

                Me.Cursor = System.Windows.Forms.Cursors.Arrow
                MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")
                Exit Sub
            End If

            Select Case cierre1.getRegistro()
                Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DF
                    msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                          "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                          "(DF) Documento Fiscal ..." & vbCrLf & _
                          "---------------------------" & vbCrLf & _
                          "Cód. Comprob." & vbTab & "=[" & cierre1.RegDF.getCodigoComprobante() & "] - " & ToStrCodComprob(cierre1.RegDF.getCodigoComprobante()) & vbCrLf & _
                          "Desde Nº" & vbTab & vbTab & "=[" & cierre1.RegDF.getNumeroInicial() & "]" & vbCrLf & _
                          "Hasta Nº" & vbTab & vbTab & "=[" & cierre1.RegDF.getNumeroFinal() & "]" & vbCrLf & _
                          "Cancelados" & vbTab & "=[" & cierre1.RegDF.getCantidadCancelados() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")

                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                          "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                          "(DF) Documento Fiscal ..." & vbCrLf & _
                          "---------------------------" & vbCrLf & _
                          "TOTAL" & vbTab & vbTab & "=[" & cierre1.RegDF.getTotal() & "]" & vbCrLf & vbCrLf & _
                          "Total Gravado" & vbTab & "=[" & cierre1.RegDF.getTotalGravado() & "]" & vbCrLf & _
                          "Total No Gravado" & vbTab & "=[" & cierre1.RegDF.getTotalNoGravado() & "]" & vbCrLf & _
                          "Total Exento" & vbTab & "=[" & cierre1.RegDF.getTotalExento() & "]" & vbCrLf & _
                          "Total IVA" & vbTab & vbTab & "=[" & cierre1.RegDF.getTotalIVA() & "]" & vbCrLf & _
                          "Total Otros Trib." & vbTab & "=[" & cierre1.RegDF.getTotalTributos() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")

                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                          "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                          "(DF) Documento Fiscal ..." & vbCrLf & _
                          "---------------------------" & vbCrLf & _
                          "Alícuota 1" & vbTab & vbTab & "=[" & cierre1.RegDF.getAlicuotaIVA_1() & "]" & vbCrLf & _
                          "Monto IVA 1" & vbTab & "=[" & cierre1.RegDF.getMontoIVA_1() & "]" & vbCrLf & _
                          "Alícuota 2" & vbTab & vbTab & "=[" & cierre1.RegDF.getAlicuotaIVA_2() & "]" & vbCrLf & _
                          "Monto IVA 2" & vbTab & "=[" & cierre1.RegDF.getMontoIVA_2() & "]" & vbCrLf & _
                          "Alícuota 3" & vbTab & vbTab & "=[" & cierre1.RegDF.getAlicuotaIVA_3() & "]" & vbCrLf & _
                          "Monto IVA 3" & vbTab & "=[" & cierre1.RegDF.getMontoIVA_3() & "]" & vbCrLf & _
                          "Alícuota 4" & vbTab & vbTab & "=[" & cierre1.RegDF.getAlicuotaIVA_4() & "]" & vbCrLf & _
                          "Monto IVA 4" & vbTab & "=[" & cierre1.RegDF.getMontoIVA_4() & "]" & vbCrLf & _
                          "Alícuota 5" & vbTab & vbTab & "=[" & cierre1.RegDF.getAlicuotaIVA_5() & "]" & vbCrLf & _
                          "Monto IVA 5" & vbTab & "=[" & cierre1.RegDF.getMontoIVA_5() & "]" & vbCrLf & _
                          "Alícuota 6" & vbTab & vbTab & "=[" & cierre1.RegDF.getAlicuotaIVA_6() & "]" & vbCrLf & _
                          "Monto IVA 6" & vbTab & "=[" & cierre1.RegDF.getMontoIVA_6() & "]" & vbCrLf & _
                          "Alícuota 7" & vbTab & vbTab & "=[" & cierre1.RegDF.getAlicuotaIVA_7() & "]" & vbCrLf & _
                          "Monto IVA 7" & vbTab & "=[" & cierre1.RegDF.getMontoIVA_7() & "]" & vbCrLf & _
                          "Alícuota 8" & vbTab & vbTab & "=[" & cierre1.RegDF.getAlicuotaIVA_8() & "]" & vbCrLf & _
                          "Monto IVA 8" & vbTab & "=[" & cierre1.RegDF.getMontoIVA_8() & "]" & vbCrLf & _
                          "Alícuota 9" & vbTab & vbTab & "=[" & cierre1.RegDF.getAlicuotaIVA_9() & "]" & vbCrLf & _
                          "Monto IVA 9" & vbTab & "=[" & cierre1.RegDF.getMontoIVA_9() & "]" & vbCrLf & _
                          "Alícuota 10" & vbTab & "=[" & cierre1.RegDF.getAlicuotaIVA_10() & "]" & vbCrLf & _
                          "Monto IVA 10" & vbTab & "=[" & cierre1.RegDF.getMontoIVA_10() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")

                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                    msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                          "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                          "(DF) Documento Fiscal ..." & vbCrLf & _
                          "---------------------------" & vbCrLf & _
                              "Cód. Tributo 1" & vbTab & "=[" & cierre1.RegDF.getCodigoTributo1() & "] - " & ToStrCodTributo(cierre1.RegDF.getCodigoTributo1()) & vbCrLf & _
                              "Importe 1" & vbTab & vbTab & "=[" & cierre1.RegDF.getImporteTributo1() & "]" & vbCrLf & _
                              "Cód. Tributo 2" & vbTab & "=[" & cierre1.RegDF.getCodigoTributo2() & "] - " & ToStrCodTributo(cierre1.RegDF.getCodigoTributo2()) & vbCrLf & _
                              "Importe 2" & vbTab & vbTab & "=[" & cierre1.RegDF.getImporteTributo2() & "]" & vbCrLf & _
                              "Cód. Tributo 3" & vbTab & "=[" & cierre1.RegDF.getCodigoTributo3() & "] - " & ToStrCodTributo(cierre1.RegDF.getCodigoTributo3()) & vbCrLf & _
                              "Importe 3" & vbTab & vbTab & "=[" & cierre1.RegDF.getImporteTributo3() & "]" & vbCrLf & _
                              "Cód. Tributo 4" & vbTab & "=[" & cierre1.RegDF.getCodigoTributo4() & "] - " & ToStrCodTributo(cierre1.RegDF.getCodigoTributo4()) & vbCrLf & _
                              "Importe 4" & vbTab & vbTab & "=[" & cierre1.RegDF.getImporteTributo4() & "]" & vbCrLf & _
                              "Cód. Tributo 5" & vbTab & "=[" & cierre1.RegDF.getCodigoTributo5() & "] - " & ToStrCodTributo(cierre1.RegDF.getCodigoTributo5()) & vbCrLf & _
                              "Importe 5" & vbTab & vbTab & "=[" & cierre1.RegDF.getImporteTributo5() & "]" & vbCrLf & _
                              "Cód. Tributo 6" & vbTab & "=[" & cierre1.RegDF.getCodigoTributo6() & "] - " & ToStrCodTributo(cierre1.RegDF.getCodigoTributo6()) & vbCrLf & _
                              "Importe 6" & vbTab & vbTab & "=[" & cierre1.RegDF.getImporteTributo6() & "]" & vbCrLf & _
                              "Cód. Tributo 7" & vbTab & "=[" & cierre1.RegDF.getCodigoTributo7() & "] - " & ToStrCodTributo(cierre1.RegDF.getCodigoTributo7()) & vbCrLf & _
                              "Importe 7" & vbTab & vbTab & "=[" & cierre1.RegDF.getImporteTributo7() & "]" & vbCrLf & _
                              "Cód. Tributo 8" & vbTab & "=[" & cierre1.RegDF.getCodigoTributo8() & "] - " & ToStrCodTributo(cierre1.RegDF.getCodigoTributo8()) & vbCrLf & _
                              "Importe 8" & vbTab & vbTab & "=[" & cierre1.RegDF.getImporteTributo8() & "]" & vbCrLf & _
                              "Cód. Tributo 9" & vbTab & "=[" & cierre1.RegDF.getCodigoTributo9() & "] - " & ToStrCodTributo(cierre1.RegDF.getCodigoTributo9()) & vbCrLf & _
                              "Importe 9" & vbTab & vbTab & "=[" & cierre1.RegDF.getImporteTributo9() & "]" & vbCrLf & _
                              "Cód. Tributo 10" & vbTab & "=[" & cierre1.RegDF.getCodigoTributo10() & "] - " & ToStrCodTributo(cierre1.RegDF.getCodigoTributo10()) & vbCrLf & _
                              "Importe 10" & vbTab & "=[" & cierre1.RegDF.getImporteTributo10() & "]" & vbCrLf & _
                              "Cód. Tributo 11" & vbTab & "=[" & cierre1.RegDF.getCodigoTributo11() & "] - " & ToStrCodTributo(cierre1.RegDF.getCodigoTributo11()) & vbCrLf & _
                              "Importe 11" & vbTab & "=[" & cierre1.RegDF.getImporteTributo11() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")
                Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DNFH
                    msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                          "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                          "(DNFH) Documento NO Fiscal Homologado (que SI acumula) ..." & vbCrLf & _
                          "--------------------------------------------------------------------" & vbCrLf & _
                          "Cód. Comprob." & vbTab & "=[" & cierre1.RegDNFH.getCodigoComprobante() & "] - " & ToStrCodComprob(cierre1.RegDNFH.getCodigoComprobante()) & vbCrLf & _
                          "Desde Nº" & vbTab & vbTab & "=[" & cierre1.RegDNFH.getNumeroInicial() & "]" & vbCrLf & _
                          "Hasta Nº" & vbTab & vbTab & "=[" & cierre1.RegDNFH.getNumeroFinal() & "]" & vbCrLf & vbCrLf & _
                          "TOTAL" & vbTab & vbTab & "=[" & cierre1.RegDNFH.getTotal() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")
                Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DNFH_NO_ACUM
                    msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                          "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                          "(DNFH) Documento NO Fiscal Homologado (que NO acumula) ..." & vbCrLf & _
                          "----------------------------------------------------------------------" & vbCrLf & _
                          "Cód. Comprob." & vbTab & "=[" & cierre1.RegDNFH_NoAcum.getCodigoComprobante() & "] - " & ToStrCodComprob(cierre1.RegDNFH_NoAcum.getCodigoComprobante()) & vbCrLf & _
                          "Desde Nº" & vbTab & vbTab & "=[" & cierre1.RegDNFH_NoAcum.getNumeroInicial() & "]" & vbCrLf & _
                          "Hasta Nº" & vbTab & vbTab & "=[" & cierre1.RegDNFH_NoAcum.getNumeroFinal() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")
                Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_GLOBAL
                    msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                          "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                          "Información Global ..." & vbCrLf & _
                          "-----------------------" & vbCrLf & _
                          "DF Emitidos" & vbTab & vbTab & "=[" & cierre1.RegGlobal.getDF_CantidadEmitidos() & "]" & vbCrLf & _
                          "DF Cancelados" & vbTab & vbTab & "=[" & cierre1.RegGlobal.getDF_CantidadCancelados() & "]" & vbCrLf & _
                          "DF TOTAL" & vbTab & vbTab & vbTab & "=[" & cierre1.RegGlobal.getDF_Total() & "]" & vbCrLf & vbCrLf & _
                          "DF Total Gravado" & vbTab & vbTab & "=[" & cierre1.RegGlobal.getDF_TotalGravado() & "]" & vbCrLf & _
                          "DF Total No Gravado" & vbTab & vbTab & "=[" & cierre1.RegGlobal.getDF_TotalNoGravado() & "]" & vbCrLf & _
                          "DF Total Exento" & vbTab & vbTab & "=[" & cierre1.RegGlobal.getDF_TotalExento() & "]" & vbCrLf & _
                          "DF Total IVA" & vbTab & vbTab & "=[" & cierre1.RegGlobal.getDF_TotalIVA() & "]" & vbCrLf & _
                          "DF Total Tributos" & vbTab & vbTab & "=[" & cierre1.RegGlobal.getDF_TotalTributos() & "]" & vbCrLf & vbCrLf & _
                          "DNFH TOTAL" & vbTab & vbTab & "=[" & cierre1.RegGlobal.getDNFH_Total() & "]"

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")
                Case Else
                    msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                          "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                          "Primer registro de información: DESCONOCIDO ! ..."

                    Me.Cursor = System.Windows.Forms.Cursors.Arrow
                    MsgBox(msj, vbOKOnly + vbCritical, "Acumulado Cierre 'Z' ...")
                    Exit Sub
            End Select

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            cierre2 = hasar.ContinuarConsultaAcumulados()

            While (cierre2.getRegistro() <> hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_FINAL)
                Select Case cierre2.getRegistro()
                    Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DF
                        msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                              "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                              "(DF) Documento Fiscal ..." & vbCrLf & _
                              "---------------------------" & vbCrLf & _
                              "Cód. Comprob." & vbTab & "=[" & cierre2.RegDF.getCodigoComprobante() & "] - " & ToStrCodComprob(cierre2.RegDF.getCodigoComprobante()) & vbCrLf & _
                              "Desde Nº" & vbTab & vbTab & "=[" & cierre2.RegDF.getNumeroInicial() & "]" & vbCrLf & _
                              "Hasta Nº" & vbTab & vbTab & "=[" & cierre2.RegDF.getNumeroFinal() & "]" & vbCrLf & _
                              "Cancelados" & vbTab & "=[" & cierre2.RegDF.getCantidadCancelados() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")

                        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                        msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                              "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                              "(DF) Documento Fiscal ..." & vbCrLf & _
                              "---------------------------" & vbCrLf & _
                              "TOTAL" & vbTab & vbTab & "=[" & cierre2.RegDF.getTotal() & "]" & vbCrLf & vbCrLf & _
                              "Total Gravado" & vbTab & "=[" & cierre2.RegDF.getTotalGravado() & "]" & vbCrLf & _
                              "Total No Gravado" & vbTab & "=[" & cierre2.RegDF.getTotalNoGravado() & "]" & vbCrLf & _
                              "Total Exento" & vbTab & "=[" & cierre2.RegDF.getTotalExento() & "]" & vbCrLf & _
                              "Total IVA" & vbTab & vbTab & "=[" & cierre2.RegDF.getTotalIVA() & "]" & vbCrLf & _
                              "Total Otros Trib." & vbTab & "=[" & cierre2.RegDF.getTotalTributos() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")

                        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                        msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                              "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                              "(DF) Documento Fiscal ..." & vbCrLf & _
                              "---------------------------" & vbCrLf & _
                              "Alícuota 1" & vbTab & vbTab & "=[" & cierre2.RegDF.getAlicuotaIVA_1() & "]" & vbCrLf & _
                              "Monto IVA 1" & vbTab & "=[" & cierre2.RegDF.getMontoIVA_1() & "]" & vbCrLf & _
                              "Alícuota 2" & vbTab & vbTab & "=[" & cierre2.RegDF.getAlicuotaIVA_2() & "]" & vbCrLf & _
                              "Monto IVA 2" & vbTab & "=[" & cierre2.RegDF.getMontoIVA_2() & "]" & vbCrLf & _
                              "Alícuota 3" & vbTab & vbTab & "=[" & cierre2.RegDF.getAlicuotaIVA_3() & "]" & vbCrLf & _
                              "Monto IVA 3" & vbTab & "=[" & cierre2.RegDF.getMontoIVA_3() & "]" & vbCrLf & _
                              "Alícuota 4" & vbTab & vbTab & "=[" & cierre2.RegDF.getAlicuotaIVA_4() & "]" & vbCrLf & _
                              "Monto IVA 4" & vbTab & "=[" & cierre2.RegDF.getMontoIVA_4() & "]" & vbCrLf & _
                              "Alícuota 5" & vbTab & vbTab & "=[" & cierre2.RegDF.getAlicuotaIVA_5() & "]" & vbCrLf & _
                              "Monto IVA 5" & vbTab & "=[" & cierre2.RegDF.getMontoIVA_5() & "]" & vbCrLf & _
                              "Alícuota 6" & vbTab & vbTab & "=[" & cierre2.RegDF.getAlicuotaIVA_6() & "]" & vbCrLf & _
                              "Monto IVA 6" & vbTab & "=[" & cierre2.RegDF.getMontoIVA_6() & "]" & vbCrLf & _
                              "Alícuota 7" & vbTab & vbTab & "=[" & cierre2.RegDF.getAlicuotaIVA_7() & "]" & vbCrLf & _
                              "Monto IVA 7" & vbTab & "=[" & cierre2.RegDF.getMontoIVA_7() & "]" & vbCrLf & _
                              "Alícuota 8" & vbTab & vbTab & "=[" & cierre2.RegDF.getAlicuotaIVA_8() & "]" & vbCrLf & _
                              "Monto IVA 8" & vbTab & "=[" & cierre2.RegDF.getMontoIVA_8() & "]" & vbCrLf & _
                              "Alícuota 9" & vbTab & vbTab & "=[" & cierre2.RegDF.getAlicuotaIVA_9() & "]" & vbCrLf & _
                              "Monto IVA 9" & vbTab & "=[" & cierre2.RegDF.getMontoIVA_9() & "]" & vbCrLf & _
                              "Alícuota 10" & vbTab & "=[" & cierre2.RegDF.getAlicuotaIVA_10() & "]" & vbCrLf & _
                              "Monto IVA 10" & vbTab & "=[" & cierre2.RegDF.getMontoIVA_10() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")

                        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                        msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                              "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                              "(DF) Documento Fiscal ..." & vbCrLf & _
                              "---------------------------" & vbCrLf & _
                              "Cód. Tributo 1" & vbTab & "=[" & cierre2.RegDF.getCodigoTributo1() & "] - " & ToStrCodTributo(cierre2.RegDF.getCodigoTributo1()) & vbCrLf & _
                              "Importe 1" & vbTab & vbTab & "=[" & cierre2.RegDF.getImporteTributo1() & "]" & vbCrLf & _
                              "Cód. Tributo 2" & vbTab & "=[" & cierre2.RegDF.getCodigoTributo2() & "] - " & ToStrCodTributo(cierre2.RegDF.getCodigoTributo2()) & vbCrLf & _
                              "Importe 2" & vbTab & vbTab & "=[" & cierre2.RegDF.getImporteTributo2() & "]" & vbCrLf & _
                              "Cód. Tributo 3" & vbTab & "=[" & cierre2.RegDF.getCodigoTributo3() & "] - " & ToStrCodTributo(cierre2.RegDF.getCodigoTributo3()) & vbCrLf & _
                              "Importe 3" & vbTab & vbTab & "=[" & cierre2.RegDF.getImporteTributo3() & "]" & vbCrLf & _
                              "Cód. Tributo 4" & vbTab & "=[" & cierre2.RegDF.getCodigoTributo4() & "] - " & ToStrCodTributo(cierre2.RegDF.getCodigoTributo4()) & vbCrLf & _
                              "Importe 4" & vbTab & vbTab & "=[" & cierre2.RegDF.getImporteTributo4() & "]" & vbCrLf & _
                              "Cód. Tributo 5" & vbTab & "=[" & cierre2.RegDF.getCodigoTributo5() & "] - " & ToStrCodTributo(cierre2.RegDF.getCodigoTributo5()) & vbCrLf & _
                              "Importe 5" & vbTab & vbTab & "=[" & cierre2.RegDF.getImporteTributo5() & "]" & vbCrLf & _
                              "Cód. Tributo 6" & vbTab & "=[" & cierre2.RegDF.getCodigoTributo6() & "] - " & ToStrCodTributo(cierre2.RegDF.getCodigoTributo6()) & vbCrLf & _
                              "Importe 6" & vbTab & vbTab & "=[" & cierre2.RegDF.getImporteTributo6() & "]" & vbCrLf & _
                              "Cód. Tributo 7" & vbTab & "=[" & cierre2.RegDF.getCodigoTributo7() & "] - " & ToStrCodTributo(cierre2.RegDF.getCodigoTributo7()) & vbCrLf & _
                              "Importe 7" & vbTab & vbTab & "=[" & cierre2.RegDF.getImporteTributo7() & "]" & vbCrLf & _
                              "Cód. Tributo 8" & vbTab & "=[" & cierre2.RegDF.getCodigoTributo8() & "] - " & ToStrCodTributo(cierre2.RegDF.getCodigoTributo8()) & vbCrLf & _
                              "Importe 8" & vbTab & vbTab & "=[" & cierre2.RegDF.getImporteTributo8() & "]" & vbCrLf & _
                              "Cód. Tributo 9" & vbTab & "=[" & cierre2.RegDF.getCodigoTributo9() & "] - " & ToStrCodTributo(cierre2.RegDF.getCodigoTributo9()) & vbCrLf & _
                              "Importe 9" & vbTab & vbTab & "=[" & cierre2.RegDF.getImporteTributo9() & "]" & vbCrLf & _
                              "Cód. Tributo 10" & vbTab & "=[" & cierre2.RegDF.getCodigoTributo10() & "] - " & ToStrCodTributo(cierre2.RegDF.getCodigoTributo10()) & vbCrLf & _
                              "Importe 10" & vbTab & "=[" & cierre2.RegDF.getImporteTributo10() & "]" & vbCrLf & _
                              "Cód. Tributo 11" & vbTab & "=[" & cierre2.RegDF.getCodigoTributo11() & "] - " & ToStrCodTributo(cierre2.RegDF.getCodigoTributo11()) & vbCrLf & _
                              "Importe 11" & vbTab & "=[" & cierre2.RegDF.getImporteTributo11() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")
                    Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DNFH
                        msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                              "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                              "(DNFH) Documento NO Fiscal Homologado (que SI acumula) ..." & vbCrLf & _
                              "--------------------------------------------------------------------" & vbCrLf & _
                              "Cód. Comprob." & vbTab & "=[" & cierre2.RegDNFH.getCodigoComprobante() & "] - " & ToStrCodComprob(cierre2.RegDNFH.getCodigoComprobante()) & vbCrLf & _
                              "Desde Nº" & vbTab & vbTab & "=[" & cierre2.RegDNFH.getNumeroInicial() & "]" & vbCrLf & _
                              "Hasta Nº" & vbTab & vbTab & "=[" & cierre2.RegDNFH.getNumeroFinal() & "]" & vbCrLf & vbCrLf & _
                              "TOTAL" & vbTab & vbTab & "=[" & cierre2.RegDNFH.getTotal() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")
                    Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_DETALLADO_DNFH_NO_ACUM
                        msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                              "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                              "(DNFH) Documento NO Fiscal Homologado (que NO acumula) ..." & vbCrLf & _
                              "----------------------------------------------------------------------" & vbCrLf & _
                              "Cód. Comprob." & vbTab & "=[" & cierre2.RegDNFH_NoAcum.getCodigoComprobante() & "] - " & ToStrCodComprob(cierre2.RegDNFH_NoAcum.getCodigoComprobante()) & vbCrLf & _
                              "Desde Nº" & vbTab & vbTab & "=[" & cierre2.RegDNFH_NoAcum.getNumeroInicial() & "]" & vbCrLf & _
                              "Hasta Nº" & vbTab & vbTab & "=[" & cierre2.RegDNFH_NoAcum.getNumeroFinal() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")
                    Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeRegistroInforme.REGISTRO_GLOBAL
                        msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                              "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                              "Información Global ..." & vbCrLf & _
                              "-----------------------" & vbCrLf & _
                              "DF Emitidos" & vbTab & vbTab & "=[" & cierre2.RegGlobal.getDF_CantidadEmitidos() & "]" & vbCrLf & _
                              "DF Cancelados" & vbTab & vbTab & "=[" & cierre2.RegGlobal.getDF_CantidadCancelados() & "]" & vbCrLf & _
                              "DF TOTAL" & vbTab & vbTab & vbTab & "=[" & cierre2.RegGlobal.getDF_Total() & "]" & vbCrLf & vbCrLf & _
                              "DF Total Gravado" & vbTab & vbTab & "=[" & cierre2.RegGlobal.getDF_TotalGravado() & "]" & vbCrLf & _
                              "DF Total No Gravado" & vbTab & vbTab & "=[" & cierre2.RegGlobal.getDF_TotalNoGravado() & "]" & vbCrLf & _
                              "DF Total Exento" & vbTab & vbTab & "=[" & cierre2.RegGlobal.getDF_TotalExento() & "]" & vbCrLf & _
                              "DF Total IVA" & vbTab & vbTab & "=[" & cierre2.RegGlobal.getDF_TotalIVA() & "]" & vbCrLf & _
                              "DF Total Tributos" & vbTab & vbTab & "=[" & cierre2.RegGlobal.getDF_TotalTributos() & "]" & vbCrLf & vbCrLf & _
                              "DNFH TOTAL" & vbTab & vbTab & "=[" & cierre2.RegGlobal.getDNFH_Total() & "]"

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")
                    Case Else
                        msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                              "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                              "Registro de información: DESCONOCIDO ! ..."

                        Me.Cursor = System.Windows.Forms.Cursors.Arrow
                        MsgBox(msj, vbOKOnly + vbCritical, "Acumulado Cierre 'Z' ...")
                        Exit Sub
                End Select

                Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
                cierre2 = hasar.ContinuarConsultaAcumulados()
            End While

            msj = "ACUMULADO CIERRE 'Z' : " & vbCrLf & _
                  "Acumulado de : Cierre 'Z' Nº 6 ..." & vbCrLf & vbCrLf & _
                  "No hay más información disponible ..."

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Acumulado Cierre 'Z' ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Acumulado Cierre 'Z' ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: AUDITORÍA por FECHAS - Acumulados/Reportes ...
    '============================================================================================================================
    Private Sub ButtonAudFechas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAudFechas.Click
        Dim msj As String     '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Debug.Print("Inicio aud. x fechas: " & TimeOfDay)
            hasar.ReportarZetasPorFecha("01/09/2016", "31/12/2016", hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteAuditoria.REPORTE_AUDITORIA_GLOBAL)
            Debug.Print("Fin aud. x fechas: " & TimeOfDay)
            msj = "Auditoría GLOBAL impresa ..." & vbCrLf & vbCrLf & _
                  "Inicio de rango" & vbTab & "=[01/09/2016]" & vbCrLf & _
                  "Fin de rango" & vbTab & "=[31/12/2016]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ReportarZetasPorFecha() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ReportarZetasPorFecha() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: AUDITORÍA por NRO. de ZETA - Acumulados/Reportes ...
    '============================================================================================================================
    Private Sub ButtonAudNroZeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAudNroZeta.Click
        Dim msj As String    '// A mostrar en MsgBox()

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Debug.Print("Inicio aud. x fechas: " & TimeOfDay)
            hasar.ReportarZetasPorNumeroZeta(153, 162, hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteAuditoria.REPORTE_AUDITORIA_GLOBAL)
            Debug.Print("Fin aud. x fechas: " & TimeOfDay)
            msj = "Auditoría GLOBAL impresa ..." & vbCrLf & vbCrLf & _
                  "Inicio de rango" & vbTab & "=[153]" & vbCrLf &
                  "Fin de rango" & vbTab & "=[162]"

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly + vbInformation, "Comando: ReportarZetasPorNumeroZeta() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ReportarZetasPorNumeroZeta() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: TIQUE/REMITO 'R' - Doc. No Fisc. Homolog ...
    '============================================================================================================================
    Private Sub ButtonRemitoR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRemitoR.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String

        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")
            hasar.CargarTransportista("Transportador Uno ...", "00000000000", "Calle 4 entre 5 y 6", "Chofer Uno ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI, "123345678", "ABC 123", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.REMITO_R)
            msj = "Tique/Remito 'R' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: TIQUE/REMITO 'R' ...")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/REMITO 'R' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            MsgBox(msj, vbOKOnly, "Imprimiendo: TIQUE/REMITO 'R' ...")

            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/REMITO 'R' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            MsgBox(msj, vbOKOnly, "Imprimiendo: TIQUE/REMITO 'R' ...")

            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Remito 'R' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: TIQUE/REMITO 'R' ...")
        Catch
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/REMITO 'R' ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: TIQUE/REMITO 'X' - Doc. No Fisc. Homolog ...
    '============================================================================================================================
    Private Sub ButtonRemitoX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRemitoX.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String

        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")
            hasar.CargarTransportista("Transportador Uno ...", "00000000000", "Calle 4 entre 5 y 6", "Chofer Uno ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_DNI, "123345678", "ABC 123", "")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.REMITO_X)
            msj = "Tique/Remito 'X' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: TIQUE/REMITO 'X' ...")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "TIQUE/REMITO 'X' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            MsgBox(msj, vbOKOnly, "Imprimiendo: TIQUE/REMITO 'X' ...")

            subt = hasar.ConsultarSubtotal()

            msj = "TIQUE/REMITO 'X' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            MsgBox(msj, vbOKOnly, "Imprimiendo: TIQUE/REMITO 'X' ...")

            respcerrar = hasar.CerrarDocumento()
            msj = "Tique/Remito 'X' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: TIQUE/REMITO 'X' ...")
        Catch
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: TIQUE/REMITO 'X' ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: PRESUPUESTO 'X' - Doc. No Fisc. Homolog ...
    '============================================================================================================================
    Private Sub ButtonPresup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPresup.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String

        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")
            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.PRESUPUESTO_X)
            msj = "Presupuesto 'X' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: PRESUPUESTO 'X' ...")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            subt = hasar.ConsultarSubtotal()

            msj = "PRESUPUESTO 'X' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            MsgBox(msj, vbOKOnly, "Imprimiendo: PRESUPUESTO 'X' ...")

            respcerrar = hasar.CerrarDocumento()
            msj = "Presupuesto 'X' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: PRESUPUESTO 'X' ...")
        Catch
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: PRESUPUESTO 'X' ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: RECIBO 'X' - Doc. No Fisc. Homolog ...
    '============================================================================================================================
    Private Sub ButtonReciboX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReciboX.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String

        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")
            hasar.CargarDocumentoAsociado(1, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_A, 2999, 43)

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_X)
            msj = "Recibo 'X' abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: RECIBO 'X' ...")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "7791234500001", "00001", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.UNIDAD)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.GRAVADO, 21.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 10.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeOperacionesGlobalesIVA.BONIFICACION_IVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, "7790001001030", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeAjustes.BONIFICACION_GENERAL)
            hasar.ImprimirOtrosTributos(hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            hasar.ImprimirConceptoRecibo("Expensas extraordinarias OCT 2015 ...")
            pago = hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "Cheque Bco. Nación Nº ...", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.CHEQUE, 0, "", "")

            msj = "RECIBO 'X' - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            MsgBox(msj, vbOKOnly, "Imprimiendo: RECIBO 'X' ...")

            subt = hasar.ConsultarSubtotal()

            msj = "RECIBO 'X' - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            MsgBox(msj, vbOKOnly, "Imprimiendo: RECIBO 'X' ...")

            respcerrar = hasar.CerrarDocumento()
            msj = "Recibo 'X' cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: RECIBO 'X' ...")
        Catch
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: RECIBO 'X' ...")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: DONACIÓN - Doc. No Fisc. Homolog ...
    '============================================================================================================================
    Private Sub ButtonDonacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDonacion.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim pago As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaImprimirPago
        Dim subt As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarSubtotal
        Dim msj As String

        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")
            hasar.CargarDocumentoAsociado(1, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_A, 2999, 43)
            hasar.CargarBeneficiario("La Escuelita Uno ...", "30124567897", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle 100, entre 11 y 12 ...")

            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.COMPROBANTE_DONACION)
            msj = "Doc. Donación abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: DONACIÓN ...")

            hasar.ImprimirItem("Donación de vuelto ...", 1.0, 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.CondicionesIVA.EXENTO, 0.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeMonto.MODO_SUMA_MONTO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeImpuestosInternos.II_VARIABLE_PORCENTUAL, 0.0, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePrecio.MODO_PRECIO_TOTAL, 1, "", "DONA", hfl.argentina.HasarImpresoraFiscalRG3561.UnidadesMedida.DONACION)
            pago = hasar.ImprimirPago("Efectivo ...", 2.5, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDePago.PAGAR, hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO, "", hfl.argentina.HasarImpresoraFiscalRG3561.TiposPago.EFECTIVO, 0, "", "")

            msj = "DONACIÓN - PAGO :" & vbCrLf & vbCrLf & _
                  "Medio de Pago Uno" & vbCrLf & _
                  "Saldo =[" & pago.getSaldo() & "]"

            MsgBox(msj, vbOKOnly, "Imprimiendo: DONACIÓN ...")

            subt = hasar.ConsultarSubtotal()

            msj = "DONACIÓN - SUBTOTAL :" & vbCrLf & vbCrLf & _
                  "Monto Total" & vbTab & "=[" & subt.getSubtotal() & "]" & vbCrLf & _
                  "Monto Pagado" & vbTab & "=[" & subt.getMontoPagado() & "]" & vbCrLf & _
                  "Monto Base" & vbTab & "=[" & subt.getMontoBase() & "]" & vbCrLf & _
                  "Monto IVA" & vbTab & "=[" & subt.getMontoIVA() & "]" & vbCrLf & _
                  "Monto Imp. Int." & vbTab & "=[" & subt.getMontoImpInternos() & "]" & vbCrLf & _
                  "Monto Otros Trib." & vbTab & "=[" & subt.getMontoOtrosTributos() & "]" & vbCrLf & _
                  "Ajuste Redondeo" & vbTab & "=[" & subt.getAjusteRedondeo() & "]"

            MsgBox(msj, vbOKOnly, "Imprimiendo: DONACIÓN ...")

            respcerrar = hasar.CerrarDocumento()
            msj = "Doc. Donación cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: DONACIÓN ...")
        Catch
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: DONACIÓN ...")
        End Try
    End Sub

    '===============================================================================================================================
    ' BOTÓN: GENÉRICO - Doc. No Fisc. Homolog ...
    ' Este tipo de comprobantes es de uso libre para cubrir las necesidades comerciales de los usuarios.  
    ' Por ejemplo, para emitir vouchers de tarjeta, talones de estacionamiento, reparto, tiques regalo, cláusulas de créditos, etc..
    '===============================================================================================================================
    Private Sub ButtonGenerico_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonGenerico.Click
        Dim respabrir As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaAbrirDocumento
        Dim respcerrar As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaCerrarDocumento
        Dim msj As String

        estilo.setNegrita(False)
        estilo.setDobleAncho(False)
        estilo.setCentrado(False)
        estilo.setBorradoTexto(False)

        'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.INTERLINEADO, "A")
        'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.INTERLINEADO, "M")
        'hasar.ConfigurarImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.INTERLINEADO, "N")

        Try
            hasar.CargarCodigoBarras(hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeCodigoDeBarras.CODIGO_TIPO_EAN13, "779123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionNumeroCodigoDeBarras.IMPRIME_NUMEROS_CODIGO, hfl.argentina.HasarImpresoraFiscalRG3561.ImpresionCodigoDeBarras.IMPRIME_CODIGO)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesCliente.RESPONSABLE_INSCRIPTO, hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeDocumentoCliente.TIPO_CUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", "", "", "")
            hasar.CargarDocumentoAsociado(1, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_A, 2999, 43)
            respabrir = hasar.AbrirDocumento(hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO)
            msj = "Doc. Genérico abierto Nº =[" & respabrir.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: GENÉRICO ...")

            estilo.setCentrado(True)
            estilo.setDobleAncho(True)
            estilo.setNegrita(False)
            hasar.ImprimirTextoGenerico(estilo, "TITULO DOC. GENERICO", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)

            estilo.setCentrado(False)
            estilo.setDobleAncho(False)
            estilo.setNegrita(False)
            hasar.ImprimirTextoGenerico(estilo, ".", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "Se puede emplear todo el ancho del papel para imprimir lo que se", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "desee, y en diferentes estilos. Por ejemplo, centrado y doble an", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "cho (como el título del documento genérico), o bien, ...", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)

            hasar.ImprimirTextoGenerico(estilo, ".", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "Sólo doble ancho:", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            estilo.setCentrado(False)
            estilo.setDobleAncho(True)
            estilo.setNegrita(False)
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)

            estilo.setDobleAncho(False)
            hasar.ImprimirTextoGenerico(estilo, ".", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "Sólo centrado:", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            estilo.setCentrado(True)
            estilo.setDobleAncho(False)
            estilo.setNegrita(False)
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)

            estilo.setCentrado(False)
            hasar.ImprimirTextoGenerico(estilo, ".", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "Sólo en negrita:", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            estilo.setCentrado(False)
            estilo.setNegrita(True)
            estilo.setDobleAncho(False)
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)

            estilo.setNegrita(False)
            hasar.ImprimirTextoGenerico(estilo, ".", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "Centrado y en negrita:", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            estilo.setCentrado(True)
            estilo.setNegrita(True)
            estilo.setDobleAncho(False)
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)

            estilo.setCentrado(False)
            estilo.setNegrita(False)
            hasar.ImprimirTextoGenerico(estilo, ".", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "Doble ancho y en negrita:", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            estilo.setCentrado(False)
            estilo.setDobleAncho(True)
            estilo.setNegrita(True)
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)

            estilo.setDobleAncho(False)
            estilo.setNegrita(False)
            hasar.ImprimirTextoGenerico(estilo, ".", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "Centrado, negrita y doble ancho:", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            estilo.setCentrado(True)
            estilo.setNegrita(True)
            estilo.setDobleAncho(True)
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)

            estilo.setCentrado(False)
            estilo.setNegrita(False)
            estilo.setDobleAncho(False)
            hasar.ImprimirTextoGenerico(estilo, ".", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "Estilo por defecto:", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, ".", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "Tener presente que la normativa fiscal vigente NO permite la im-", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "presión de comprobantes que se asemejen a los documentos fisca-", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, "les, emitidos mediante el uso de controladores fiscales.", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)
            hasar.ImprimirTextoGenerico(estilo, ".", hfl.argentina.HasarImpresoraFiscalRG3561.ModosDeDisplay.DISPLAY_NO)

            respcerrar = hasar.CerrarDocumento()
            msj = "Doc. Genérico cerrado Nº =[" & respcerrar.getNumeroComprobante() & "]"
            MsgBox(msj, vbOKOnly, "Imprimiendo: GENÉRICO ...")
        Catch
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Imprimiendo: GENÉRICO ...")
        End Try
    End Sub

    '===============================================================================================================================
    ' BOTÓN: REPORTES ELECTRÓNICOS - Descarga ...
    '
    ' La descarga permite generar un archivo ZIP que contiene 3 archivos ".PEM" en formato PKCS#7 (XMLs firmados digitalmente, con encriptación 
    ' estándar y clave pública):
    '
    '    - "F8011.......pem" : Reporte de totales (información de la memoria fiscal, a elevar a la AFIP)
    '    - "F8012.......pem" : Duplicado comprobantes 'A', 'A' con leyenda y 'M' (información extraída de la cinta testigo digital, a elevar a la AFIP).
    '    - "F8010.......pem" : Cinta testigo digital (CTD), o memoria de auditoría (el contribuyente debe conservar dos copias de esta información).
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información, accediendo a la interfaz HTTP
    ' de la impresora fiscal. Conectarse a la dirección IP configurada en el equipo.
    '===============================================================================================================================
    Private Sub ButtonRepElect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRepElect.Click
        Dim bloque1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueReporteElectronico
        Dim bloque2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueReporteElectronico
        Dim res As Microsoft.VisualBasic.MsgBoxResult
        Dim msj As String                                                      '// A mostrar en MsgBox()
        Dim arch As String = "Reportes.a85"                                    '// Archivo donde grabar la descarga de información, en ASCII85
        Dim archzip As String = "Reportes.zip"                                 '// Resultado final de la descarga completa
        Dim pp As Byte()                                                       '// Almacenamiento de ASCII85 decodificado a binario
        Dim ja As Ascii85 = New Ascii85                                        '// Instancia de la clase de decodificación
        Dim fileReader As String                                               '// Para leer archivos en el "encoding" original
        Dim oFileStream As System.IO.FileStream                                '// Para generar el ZIP

        Try
            If (System.IO.File.Exists(archzip) Or System.IO.File.Exists(arch)) Then
                msj = "REPORTE ELECTRÓNICOS : " & vbCrLf & vbCrLf & _
                  "Se encontraron archivos de descargas anteriores ..." & vbCrLf & _
                  "Si continúa se perderá dicha información ..." & vbCrLf & vbCrLf & _
                  "¿ Desea continuar ?"
                res = MsgBox(msj, vbOKCancel + vbQuestion, "Descarga: REPORTES ELECTRÓNICOS ...")

                If res = vbCancel Then
                    pos = 1
                    Exit Sub
                End If

                If (System.IO.File.Exists(archzip)) Then My.Computer.FileSystem.DeleteFile(archzip)
                If (System.IO.File.Exists(arch)) Then My.Computer.FileSystem.DeleteFile(arch)
            End If

            msj = "REPORTES ELECTRÓNICOS AFIP ..." & vbCrLf & vbCrLf & _
                  "A descargar información ..." & vbCrLf & vbCrLf & _
                  "Si acepta, ... POR FAVOR ESPERE ..." & vbCrLf & _
                  "Esta operación puede durar algunos minutos ..."
            res = MsgBox(msj, vbOKCancel + vbInformation, "Descarga: REPORTES ELECTRÓNICOS AFIP ...")

            If (res = vbCancel) Then
                MsgBox("DESCARGA CANCELADA ! ...", vbOKOnly + vbInformation, "Descarga: REPORTES ELECTRÓNICOS AFIP ...")
                pos = 1
                Exit Sub
            End If

            '// Se descarga el primer bloque de información.
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Debug.Print("Inicio descarga: " & TimeOfDay)
            bloque1 = hasar.ObtenerPrimerBloqueReporteElectronico("01/09/2016", "31/12/2016", hfl.argentina.HasarImpresoraFiscalRG3561.TiposReporteAFIP.REPORTE_AFIP_COMPLETO)

            If (bloque1.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                msj = "Descarga: REPORTES ELECTRÓNICOS AFIP ..." & vbCrLf & vbCrLf & _
                      "No hay información disponible ! ..."
                MsgBox(msj, vbOKOnly + vbInformation, "Descarga: REPORTES ELECTRÓNICOS AFIP ...")

                pos = 1
                Me.Cursor = System.Windows.Forms.Cursors.Arrow
                Exit Sub
            End If

            '// Generación del archivo ".a85". Se guarda en el archivo la información leída.
            GrabarInfoBin(arch, bloque1.getInformacion())
            '// Se obtiene el segundo bloque de información.
            bloque2 = hasar.ObtenerSiguienteBloqueReporteElectronico()

            While (bloque2.getRegistro() <> hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                '// Se guarda en el archivo la información leída.
                GrabarInfoBin(arch, bloque2.getInformacion())
                '// Se obtiene el próximo bloque de información.
                bloque2 = hasar.ObtenerSiguienteBloqueReporteElectronico()
                Application.DoEvents()
            End While

            If (bloque2.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                '// Se guarda en el archivo la información leída.
                GrabarInfoBin(arch, bloque2.getInformacion())
            End If

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            Debug.Print("Fin descarga: " & TimeOfDay)

            msj = "REPORTES ELECTRÓNICOS AFIP :" & vbCrLf & vbCrLf & _
                  "Descarga finalizada ! ..."
            MsgBox(msj, vbOKOnly + vbInformation, "Descarga: REPORTES ELECTRÓNICOS AFIP ...")

            '// Se lee el archivo generado.
            fileReader = My.Computer.FileSystem.ReadAllText(arch)
            pp = ja.Decode(fileReader)     '// Convierte ASCII85 a binario
            '// Se genera el ZIP con la decodificación
            oFileStream = New System.IO.FileStream(archzip, System.IO.FileMode.Create)
            oFileStream.Write(pp, 0, pp.Length)
            oFileStream.Close()
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Descarga: REPORTES ELECTRÓNICOS AFIP ...")

            pos = 1

        End Try
    End Sub

    '===============================================================================================================================
    ' BOTÓN: REPORTE DOCUMENTOS - Descarga ...
    '
    ' La descarga permite generar un archivo ZIP que contiene los XMLs con el detalle de los comprobantes solicitados. 
    ' La información es extraída de la cinta testigo digital -CTD- (o memoria de auditoría).
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información, accediendo a la interfaz HTTP
    ' de la impresora fiscal. Conectarse a la dirección IP configurada en el equipo.
    '===============================================================================================================================
    Private Sub ButtonRepDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRepDoc.Click
        Dim bloque1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueDocumento
        Dim bloque2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueDocumento
        Dim msj As String                                                           '// A mostrar en MsgBox()
        Dim res As Microsoft.VisualBasic.MsgBoxResult
        Dim pp As Byte()                                                            '// Almacenamiento de ASCII85 decodificado a binario
        Dim ja As Ascii85 = New Ascii85                                             '// Instancia de la clase de decodificación
        Dim fileReader As String                                                    '// Para leer el archivo ".a85"
        Dim oFileStream As System.IO.FileStream                                     '// Para generar el ZIP
        Dim arch As String = "Comprobantes.a85"                                     '// Archivo donde almacenar la información descargada en ASCII85.
        Dim archzip As String = "Comprobantes.zip"                                  '// Resultado final de la descarga

        Try
            If (System.IO.File.Exists(archzip) Or System.IO.File.Exists(arch)) Then
                msj = "REPORTE COMPROBANTES : " & vbCrLf & vbCrLf & _
                  "Se encontraron archivos de descargas anteriores ..." & vbCrLf & _
                  "Si continúa se perderá dicha información ..." & vbCrLf & vbCrLf & _
                  "¿ Desea continuar ?"
                res = MsgBox(msj, vbOKCancel + vbQuestion, "Descarga: REPORTES COMPROBANTES ...")

                If res = vbCancel Then
                    pos = 1
                    Exit Sub
                End If

                If (System.IO.File.Exists(archzip)) Then My.Computer.FileSystem.DeleteFile(archzip)
                If (System.IO.File.Exists(arch)) Then My.Computer.FileSystem.DeleteFile(arch)
            End If

            msj = "REPORTE COMPROBANTES : " & vbCrLf & vbCrLf & _
                  "A descargar información ..." & vbCrLf & vbCrLf & _
                  "Si acepta, ... POR FAVOR ESPERE ..." & vbCrLf & _
                  "Esta operación puede durar algunos minutos ..."
            res = MsgBox(msj, vbOKCancel + vbInformation, "Descarga: REPORTES COMPROBANTES ...")

            If (res = vbCancel) Then
                MsgBox("DESCARGA CANCELADA ! ...", vbOKOnly + vbInformation, "Descarga: REPORTES COMPROBANTES ...")
                pos = 1
                Exit Sub
            End If

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            '// Se descarga el primer bloque de información.
            Debug.Print("Inicio descarga: " & TimeOfDay)
            bloque1 = hasar.ObtenerPrimerBloqueDocumento(1, 3, hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_A, hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion.COMPRIME, hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada.VARIOS_XMLS)

            If (bloque1.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                msj = "Descarga: REPORTE COMPROBANTES ..." & vbCrLf & vbCrLf & _
                      "No hay información disponible ! ..."
                MsgBox(msj, vbOKOnly + vbInformation, "Descarga: REPORTE COMPROBANTES ...")

                pos = 1
                Me.Cursor = System.Windows.Forms.Cursors.Arrow
                Exit Sub
            End If

            '// Se almacena la información descargada, del primer bloque.
            GrabarInfoBin(arch, bloque1.getInformacion())
            '// Se obtiene el segundo bloque de información.
            bloque2 = hasar.ObtenerSiguienteBloqueDocumento()

            While (bloque2.getRegistro() <> hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                '// Se almacena la información descargada.
                GrabarInfoBin(arch, bloque2.getInformacion())
                '// Se obtiene el próximo bloque de información.
                bloque2 = hasar.ObtenerSiguienteBloqueDocumento()
                Application.DoEvents()
            End While

            If (bloque2.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                '// Se almacena el último bloque de información.
                GrabarInfoBin(arch, bloque2.getInformacion())
            End If

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            Debug.Print("Fin descarga: " & TimeOfDay)

            msj = "REPORTE COMPROBANTES : " & vbCrLf & vbCrLf & _
                  "Descarga finalizada ! ..."
            MsgBox(msj, vbOKOnly + vbInformation, "Descarga: REPORTE COMPROBANTES ...")

            fileReader = My.Computer.FileSystem.ReadAllText(arch)
            pp = ja.Decode(fileReader)     '// Convierte ASCII85 a binario
            '// Se genera el ZIP con la decodificación
            oFileStream = New System.IO.FileStream(archzip, System.IO.FileMode.Create)
            oFileStream.Write(pp, 0, pp.Length)
            oFileStream.Close()
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            msj = "Codigo: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Descarga: REPORTE COMPROBANTES ...")

            pos = 1
        End Try
    End Sub

    '===============================================================================================================================
    ' BOTÓN: CINTA TESTIGO DIGITAL - Descarga ...
    '
    ' La descarga permite generar un archivo ZIP que contiene los XMLs con el detalle de los cierres 'Z' solicitados. 
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información, accediendo a la interfaz HTTP
    ' de la impresora fiscal. Conectarse a la dirección IP configurada en el equipo.
    '===============================================================================================================================
    Private Sub ButtonCTD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCTD.Click
        Dim bloque1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueAuditoria
        Dim bloque2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueAuditoria
        Dim msj As String                                                         '// A mostrar en MsgBox()
        Dim arch As String = "Memaudit.a85"                                                      '// Archivo donde almacenar las descarga.
        Dim res As Microsoft.VisualBasic.MsgBoxResult
        Dim fileReader As String                                                  '// Para leer el archivo ".a85"
        Dim oFileStream As System.IO.FileStream                                   '// Para generar el ZIP
        Dim archzip As String = "MemAudit.zip"                                    '// Resultado final de la descarga
        Dim pp As Byte()                                                          '// Almacenamiento de ASCII85 decodificado a binario
        Dim ja As Ascii85 = New Ascii85                                           '// Instancia de la clase de decodificación

        Try
            If (System.IO.File.Exists(archzip) Or System.IO.File.Exists(arch)) Then
                msj = "Descarga CTD : " & vbCrLf & vbCrLf & _
                  "Se encontraron archivos de descargas anteriores ..." & vbCrLf & _
                  "Si continúa se perderá dicha información ..." & vbCrLf & vbCrLf & _
                  "¿ Desea continuar ?"
                res = MsgBox(msj, vbOKCancel + vbQuestion, "Descarga: CTD ...")

                If res = vbCancel Then
                    pos = 1
                    Exit Sub
                End If

                If (System.IO.File.Exists(archzip)) Then My.Computer.FileSystem.DeleteFile(archzip)
                If (System.IO.File.Exists(arch)) Then My.Computer.FileSystem.DeleteFile(arch)
            End If

            msj = "CTD (Cinta Testigo Digital) : " & vbCrLf & vbCrLf & _
                  "A descargar información ..." & vbCrLf & vbCrLf & _
                  "Si acepta, ... POR FAVOR ESPERE ..." & vbCrLf & _
                  "Esta operación puede durar algunos minutos ..."
            res = MsgBox(msj, vbOKCancel + vbInformation, "Descarga: CTD ...")

            If (res = vbCancel) Then
                MsgBox("DESCARGA CANCELADA ! ...", vbOKOnly + vbInformation, "Descarga: CTD ...")
                pos = 1
                Exit Sub
            End If

            '// Se descarga el primer bloque de información.
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Debug.Print("Inicio descarga: " & TimeOfDay)
            bloque1 = hasar.ObtenerPrimerBloqueAuditoria("115", "162", hfl.argentina.HasarImpresoraFiscalRG3561.TipoReporteZ.REPORTE_Z_NUMERO, hfl.argentina.HasarImpresoraFiscalRG3561.CompresionDeInformacion.COMPRIME, hfl.argentina.HasarImpresoraFiscalRG3561.XMLsPorBajada.VARIOS_XMLS)

            If (bloque1.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                msj = "CTD (Cinta testigo Digital) ..." & vbCrLf & vbCrLf & _
                      "No hay información disponible ! ..."
                MsgBox(msj, vbOKOnly + vbInformation, "Descarga: CTD ...")

                pos = 1
                Me.Cursor = System.Windows.Forms.Cursors.Arrow
                Exit Sub
            End If

            '// Se almacena la información descargada, del primer bloque.
            GrabarInfoBin(arch, bloque1.getInformacion())

            '// Se descarga el segundo bloque de información.
            bloque2 = hasar.ObtenerSiguienteBloqueAuditoria()

            While (bloque2.getRegistro() <> hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                '// Se almacena la información descargada.
                GrabarInfoBin(arch, bloque2.getInformacion())

                '// Se descarga el próximo bloque de información.
                bloque2 = hasar.ObtenerSiguienteBloqueAuditoria()
                Application.DoEvents()
            End While

            If (bloque2.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                '// Se almacena el último bloque de información.
                GrabarInfoBin(arch, bloque2.getInformacion())
            End If

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            Debug.Print("Fin descarga: " & TimeOfDay)

            msj = "CTD (Cinta Testigo Digital) : " & vbCrLf & vbCrLf & _
                  "Descarga finalizada ! ..."
            MsgBox(msj, vbOKOnly + vbInformation, "Descarga: CTD ...")

            fileReader = My.Computer.FileSystem.ReadAllText(arch)
            pp = ja.Decode(fileReader)                '// Convierte ASCII85 a binario
            '// Se genera el ZIP con la decodificación
            oFileStream = New System.IO.FileStream(archzip, System.IO.FileMode.Create)
            oFileStream.Write(pp, 0, pp.Length)
            oFileStream.Close()
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow

            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Descarga: CTD ...")

            pos = 1
        End Try
    End Sub

    '===============================================================================================================================
    ' BOTÓN: LOG INTERNO - Descarga ...
    '
    ' La descarga permite generar un archivo con el contenido del registro interno de actividades, de la IFH 2G.. 
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información, accediendo a la interfaz HTTP
    ' de la impresora fiscal. Conectarse a la dirección IP configurada en el equipo.
    '===============================================================================================================================
    Private Sub ButtonLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonLog.Click
        Dim bloque1 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerPrimerBloqueLog
        Dim bloque2 As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaObtenerSiguienteBloqueLog
        Dim msj As String                                                '// A mostrar en MsgBox()
        Dim arch As String = "LogInterno.txt"                              '// Archivo donde almacenar la información a descargar.
        Dim res As Microsoft.VisualBasic.MsgBoxResult

        Try
            msj = "LOG INTERNO : " & vbCrLf & vbCrLf & _
                  "A descargar información ..." & vbCrLf & vbCrLf & _
                  "Si acepta, ... POR FAVOR ESPERE ..." & vbCrLf & _
                  "Esta operación puede durar algunos minutos ..."
            res = MsgBox(msj, vbOKCancel + vbInformation, "Descarga: LOG INTERNO ...")

            If (res = vbCancel) Then
                MsgBox("DESCARGA CANCELADA ! ...", vbOKOnly + vbInformation, "Descarga: LOG INTERNO ...")
                pos = 1
                Exit Sub
            End If

            If (System.IO.File.Exists(arch)) Then My.Computer.FileSystem.DeleteFile(arch)

            '// Se descarga el primer bloque de información.
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            Debug.Print("Inicio descarga: " & TimeOfDay)
            bloque1 = hasar.ObtenerPrimerBloqueLog()

            If (bloque1.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                msj = "LOG INTERNO ..." & vbCrLf & vbCrLf & _
                      "No hay información disponible ! ..."
                MsgBox(msj, vbOKOnly + vbInformation, "Descarga: LOG INTERNO ...")

                Me.Cursor = System.Windows.Forms.Cursors.Arrow
                pos = 1
                Exit Sub
            End If

            '// Se almacena la información descargada, del primer bloque.
            GrabarInfoBin(arch, bloque1.getInformacion())

            '// Se descarga el segundo bloque de información.
            bloque2 = hasar.ObtenerSiguienteBloqueLog()

            While (bloque2.getRegistro() <> hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL)
                '// Se almacena la información descargada.
                GrabarInfoBin(arch, bloque2.getInformacion())

                '// Se obtiene el próximo bloque de información.
                bloque2 = hasar.ObtenerSiguienteBloqueLog()
                Application.DoEvents()
            End While

            If (bloque2.getRegistro() = hfl.argentina.HasarImpresoraFiscalRG3561.IdentificadorBloque.BLOQUE_FINAL) Then
                '// Se almacena el último bloque de información.
                GrabarInfoBin(arch, bloque2.getInformacion())
            End If

            Debug.Print("Fin descarga: " & TimeOfDay)
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "LOG INTERNO : " & vbCrLf & vbCrLf & _
                  "Descarga finalizada ! ..."
            MsgBox(msj, vbOKOnly + vbInformation, "Descarga: LOG INTERNO ...")

            pos = 1                        '// Para volver al primer byte del archivo
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR Descarga: LOG INTERNO ...")

            pos = 1
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: COMPORTAMIENTO IFH 2G - Consulta ...
    '============================================================================================================================
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim resp As hfl.argentina.HasarImpresoraFiscalRG3561.RespuestaConsultarConfiguracionImpresoraFiscal
        Dim msj As String

        Try
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            msj = "-Consulta-" & vbCrLf & _
                  "CONFIGURACIÓN IFH 2G :" & vbCrLf & vbCrLf
            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.ALTO_HOJA)
            msj = msj & "Alto Hoja ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.ANCHO_HOJA)
            msj = msj & "Ancho Hoja ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.SONIDO_AVISO)
            msj = msj & "Aviso sonoro ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.BORRADO_AUTOMATICO_AUDITORIA)
            msj = msj & "Borrado automático ?" & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.CHEQUEO_DESBORDE)
            msj = msj & "Chequeo desbordes ?" & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.CORTE_PAPEL)
            msj = msj & "Corte de Papel ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            'resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.ESTACION_REPORTES_XZ)
            'msj = msj & "Estación X/Z ?" & vbTab & "=[" & resp.getValor() & "]"                            '// No válido en página completa

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.TIPO_HABILITACION)
            msj = msj & "Habilitación ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_CAMBIO)
            msj = msj & "Imprimir CAMBIO $0.00 ?" & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_CODIGO_QR)
            msj = msj & "Imprimir QR ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            'resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_COLOR_ALTERNATIVO)
            'msj = msj & "Imprimir QR ?" & vbTab & vbTab & "=[" & resp.getValor() & "]"                            '// No válido en página completa

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_LEYENDAS)
            msj = msj & "Imprimir leyendas ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_MARCO)
            msj = msj & "Imprimir marco ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            'resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.IMPRESION_USAR_COLOR_ALTERNATIVO)
            'msj = msj & "Usar color altern. ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf                '// No válido en página completa

            'resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.INTERLINEADO)
            'msj = msj & "Interlineado ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf   '// No válido en página completa

            'resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.LIMITE_BC)
            'msj = msj & "Límite sin nominar ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf  '// No válido en página completa

            'resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.MODO_IMPRESION)
            'msj = msj & "Modo impresión ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf   '// No válido en página completa

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.PAGO_SALDO)
            msj = msj & "Texto saldo ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.REIMPRESION_CANCELADOS)
            msj = msj & "Reimpr. Cancelados ?" & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            resp = hasar.ConsultarConfiguracionImpresoraFiscal(hfl.argentina.HasarImpresoraFiscalRG3561.Configuracion.TIMEOUT_ENVIO_RESPUESTA_EN_ESPERA)
            msj = msj & "Timeout en espera ?" & vbTab & vbTab & "=[" & resp.getValor() & "]" & vbCrLf

            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            MsgBox(msj, vbOKOnly, "Comando: ConsultarConfiguracionImpresoraFiscal() ...")
        Catch
            Me.Cursor = System.Windows.Forms.Cursors.Arrow
            msj = "Código: " & vbTab & Err.Number & vbCrLf & "Mensaje: " & vbTab & Err.Description
            MsgBox(msj, vbOKOnly + vbCritical, "ERROR ConsultarConfiguracionImpresoraFiscal() ...")
        End Try
    End Sub

    '============================================================================================================================
    ' EVENTO: Comando en proceso ...
    '============================================================================================================================
    Private Sub hasar_eventoComandoEnProceso() Handles hasar.eventoComandoEnProceso
        Debug.Print("Procesando comando ...")
    End Sub

    '============================================================================================================================
    ' EVENTO: Comando procesado ...
    '============================================================================================================================
    Private Sub hasar_eventoComandoProcesado() Handles hasar.eventoComandoProcesado
        Debug.Print("Listo ! - Comando procesado ...")
    End Sub

    '============================================================================================================================
    ' EVENTO: Evento impresora ...
    '============================================================================================================================
    Private Sub hasar_eventoImpresora(ByVal printer As hfl.argentina.Estados_Fiscales_RG3561.EstadoImpresora) Handles hasar.eventoImpresora
        Debug.Print("EVENTO IMPRESORA :")
        Debug.Print("Cajón abierto      ? =[" & printer.getCajonAbierto() & "]")
        Debug.Print("Error impresora    ? =[" & printer.getErrorImpresora() & "]")
        Debug.Print("Falta papel J      ? =[" & printer.getFaltaPapelJournal() & "]")
        Debug.Print("Falta papel R      ? =[" & printer.getFaltaPapelReceipt() & "]")
        Debug.Print("Impresora ocupada  ? =[" & printer.getImpresoraOcupada() & "]")
        Debug.Print("Impresora offline  ? =[" & printer.getImpresoraOffLine() & "]")
        Debug.Print("Tapa abierta       ? =[" & printer.getTapaAbierta() & "]")
    End Sub

    '============================================================================================================================
    ' Función para escritura de información sobre un archivo, en forma binaria.
    '============================================================================================================================
    Private Sub GrabarInfoBin(ByVal archivo As String, ByVal info As String)
        Dim ifr As Integer                            '// Identificador de archivo
        Dim rectam As Integer = 1024                  '// Tamaño de registro

        ifr = FreeFile()
        FileOpen(ifr, archivo, OpenMode.Binary, , , rectam)
        FilePut(ifr, info, pos)
        pos = Seek(ifr)
        FileClose(ifr)

    End Sub

    '============================================================================================================================
    ' Finaliza la ejecución del software sea porque se hizo Clic en el bootón "TERMNAR", o porque se hizo clic en [X] ...
    '============================================================================================================================
    Private Sub FormIFH2G_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Me.FormClosing
        Dim msj As String
        Dim resp As Microsoft.VisualBasic.MsgBoxResult

        msj = "CERRANDO PROGRAMA ... Chau ! ..."
        resp = MsgBox(msj, vbOKCancel + vbInformation, "DEMO IFH 2G - FIN ....")

        If (resp = vbCancel) Then
            e.Cancel() = True
            Exit Sub
        End If

        Me.Finalize()
        End
    End Sub
End Class


