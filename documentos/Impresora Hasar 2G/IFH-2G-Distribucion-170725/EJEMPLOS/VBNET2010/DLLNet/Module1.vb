﻿'//#####################################################################################################################################################
'// Impresoras Fiscales HASAR (IFH) Segunda Generación (2G)                                   Dto. Software de Base, Compañía HASAR SAIC, Grupo HASAR
'// RG AFIP Nº 3561/13 , Controladores Fiscales Nueva Tecnología                              HasarLibreriaFiscal3561.dll, Protocolo '0', Revisión '103'
'// Argentina, Abril 2016                                                                     VB.Net, Visual Studio 2010, Ejemplo orientativo
'//
'// HASAR no asume responsabilidad alguna por las consecuencias del uso de este código, tal y como está, o luego de modificaciones hechas por los progra- 
'// madores.
'// HASAR se reserva el derecho de modificar, sin previo aviso, la codificación de este ejemplo.
'//######################################################################################################################################################
Option Explicit On

Module Module1
    '====================================================================================================================================================
    ' ToStrCodComprob( ): Recibe un código de tributo y retorna una leyenda asociada a él.
    '====================================================================================================================================================
    Public Function ToStrCodTributo(ByVal codigo As hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos) As String

        ToStrCodTributo = "Tipo de tributo desconocido ! ..."

        Select Case codigo
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IIBB
                ToStrCodTributo = "Ingresos Brutos ..."
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IMPUESTO_INTERNO_ITEM
                ToStrCodTributo = "Impuesto Interno Ítem ..."
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IMPUESTOS_INTERNOS
                ToStrCodTributo = "Impuesto Interno ..."
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IMPUESTOS_MUNICIPALES
                ToStrCodTributo = "Impuesto Municipal ..."
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IMPUESTOS_NACIONALES
                ToStrCodTributo = "Impuesto Nacional ..."
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.IMPUESTOS_PROVINCIALES
                ToStrCodTributo = "Impuesto Provincial ..."
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.OTRAS_PERCEPCIONES
                ToStrCodTributo = "Otras Percepciones ..."
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.OTROS_TRIBUTOS
                ToStrCodTributo = "Otros Tributos ..."
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.PERCEPCION_IIBB
                ToStrCodTributo = "Precepción Ingresos Brutos ..."
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.PERCEPCION_IMPUESTOS_MUNICIPALES
                ToStrCodTributo = "Percepción Impuesto Municipal ..."
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.PERCEPCION_IVA
                ToStrCodTributo = "Percepción IVA ..."
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposTributos.SIN_IMPUESTOS
                ToStrCodTributo = "Sin Impuesto ..."
        End Select

    End Function

    '====================================================================================================================================================
    ' ToStrCodComprob( ): Recibe un código de comprobante y retorna una leyenda asociada a él.
    '====================================================================================================================================================
    Public Function ToStrCodComprob(ByVal codigo As hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante) As String

        ToStrCodComprob = "Tipo de comprobante desconocido ! ..."

        Select Case codigo
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.CAMBIO_CATEGORIZACION_IVA
                ToStrCodComprob = "Cambio categorización IVA"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.CAMBIO_FECHA_HORA
                ToStrCodComprob = "Cambio fecha y hora"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.CAMBIO_INSCRIPCION_ING_BRUTOS
                ToStrCodComprob = "Cambio inscripción Ingresos Brutos"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.COMPROBANTE_DONACION
                ToStrCodComprob = "Comprobante Donación"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.DETALLE_DE_VENTAS
                ToStrCodComprob = "Detalle de ventas"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_A
                ToStrCodComprob = "Factura 'A'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_B
                ToStrCodComprob = "Factura 'B'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_C
                ToStrCodComprob = "Factura 'C'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.FACTURA_M
                ToStrCodComprob = "Factura 'M'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.GENERICO
                ToStrCodComprob = "Documento Genérico"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.INFORME_DE_AUDITORIA
                ToStrCodComprob = "Informe de Auditoría"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.INFORME_DIARIO_DE_CIERRE
                ToStrCodComprob = "Informe Diario de Cierre"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.MENSAJE_CF
                ToStrCodComprob = "Mensaje Controlador Fiscal"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NO_DOCUMENTO
                ToStrCodComprob = "No Documento"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_A
                ToStrCodComprob = "Nota de Crédito 'A'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_B
                ToStrCodComprob = "Nota de Crédito 'B'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_C
                ToStrCodComprob = "Nota de Crédito 'C'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_CREDITO_M
                ToStrCodComprob = "Nota de Crédito 'M'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_A
                ToStrCodComprob = "Nota de Débito 'A'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_B
                ToStrCodComprob = "Nota de Débito 'B'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_C
                ToStrCodComprob = "Nota de Débito 'C'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.NOTA_DE_DEBITO_M
                ToStrCodComprob = "Nota de Débito 'M'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.PRESUPUESTO_X
                ToStrCodComprob = "Presupuesto 'X'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_A
                ToStrCodComprob = "Recibo 'A'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_B
                ToStrCodComprob = "Recibo 'B'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_M
                ToStrCodComprob = "Recibo 'M'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.RECIBO_X
                ToStrCodComprob = "Recibo 'X'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.REMITO_R
                ToStrCodComprob = "Remito 'R'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.REMITO_X
                ToStrCodComprob = "Remito 'X'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.REPORTE_CTD
                ToStrCodComprob = "Reporte CTD"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.REPORTE_RESUMEN_TOTALES
                ToStrCodComprob = "Reporte resumen de totales"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.REPORTES_DUPLICADOS_A
                ToStrCodComprob = "Reporte duplicados 'A'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE
                ToStrCodComprob = "Tique"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_A
                ToStrCodComprob = "Tique Factura 'A'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_B
                ToStrCodComprob = "Tique Factura 'B'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_C
                ToStrCodComprob = "Tique factura 'C'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_FACTURA_M
                ToStrCodComprob = "Tique Factura 'M'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO
                ToStrCodComprob = "Tique Nota de Crédito"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_A
                ToStrCodComprob = "Tique Nota de Crédito 'A'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_B
                ToStrCodComprob = "Tique Nota de Crédito 'B'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_C
                ToStrCodComprob = "Tique Nota de Crédito 'C'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_CREDITO_M
                ToStrCodComprob = "Tique Nota de Crédito 'M'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_A
                ToStrCodComprob = "Tique Nota de Débito 'A'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_B
                ToStrCodComprob = "Tique Nota de Débito 'B'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_C
                ToStrCodComprob = "Tique Nota de Débito 'C'"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposComprobante.TIQUE_NOTA_DEBITO_M
                ToStrCodComprob = "Tique Nota de Débito 'M'"
        End Select

    End Function

    '====================================================================================================================================================
    ' ToStrEstadosFiscales( ): Recibe un código de estado fiscal y retorna una leyenda asociada a él.
    '====================================================================================================================================================
    Public Function ToStrEstadosFiscales(ByVal codigo As hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales) As String

        ToStrEstadosFiscales = "Estado Fiscal no identificable ! ..."

        Select Case codigo
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.CINTA_AUDITORIA_CASI_LLENA
                ToStrEstadosFiscales = "CTD casi llena"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.CINTA_AUDITORIA_LLENA
                ToStrEstadosFiscales = "CTD llena"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.CONTROLADOR_FISCAL_BLOQUEADO
                ToStrEstadosFiscales = "Controlador Fiscal Bloqueado"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.CONTROLADOR_FISCAL_DADO_DE_BAJA
                ToStrEstadosFiscales = "Controlador Fiscal dado de baja"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.CONTROLADOR_FISCAL_ESPERANDO_BAJA
                ToStrEstadosFiscales = "Controlador Fiscal esperando baja"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.DESCONOCIDO
                ToStrEstadosFiscales = "Estado fiscal Desconocido"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.EN_JORNADA_FISCAL
                ToStrEstadosFiscales = "Jornada fiscal en evolución"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.IMPRIMIENDO_LINEAS_RECIBO
                ToStrEstadosFiscales = "Imprimirndo líneas de concepto en recibo"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.IMPRIMIENDO_TEXTO_FISCAL
                ToStrEstadosFiscales = "Imprimiendo texto fiscal"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.IMPRIMIENDO_TEXTO_NO_FISCAL
                ToStrEstadosFiscales = "Imprimiendo texto no fiscal"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.INGRESANDO_OTROS_TRIBUTOS
                ToStrEstadosFiscales = "Ingresando otros tributos"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.INICIO_JORNADA_FISCAL
                ToStrEstadosFiscales = "Comenzando jornada fiscal"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.NO_INICIALIZADO
                ToStrEstadosFiscales = "Controlador Fiscal no inicializado"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.PAGANDO
                ToStrEstadosFiscales = "Ingresando medios de pago"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.REALIZANDO_OPERACION_AJUSTE
                ToStrEstadosFiscales = "Realizando un ajuste"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.REALIZANDO_OPERACION_ANTICIPO
                ToStrEstadosFiscales = "Realizando un anticipo"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.REALIZANDO_OPERACION_GLOBAL_IVA
                ToStrEstadosFiscales = "Operación global IVA"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.EstadosFiscales.VENDIENDO_ITEMS
                ToStrEstadosFiscales = "Vendiendo ítems"
        End Select

    End Function

    '====================================================================================================================================================
    ' ToStrCategIVA( ): Recibe un código de categorización IVA (del emisor) y retorna una leyenda asociada a él.
    '====================================================================================================================================================
    Public Function ToStrCategIVA(ByVal codigo As hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesImpresor) As String

        ToStrCategIVA = "Categoría IVA del emisor desconocida !"

        Select Case codigo
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesImpresor.IF_MONOTRIBUTO
                ToStrCategIVA = "Monotributo"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesImpresor.IF_MONOTRIBUTO_SOCIAL
                ToStrCategIVA = "Monotributo Social"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesImpresor.IF_NO_RESPONSABLE
                ToStrCategIVA = "No Responsable"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesImpresor.IF_RESPONSABLE_EXENTO
                ToStrCategIVA = "Responsable Exento"
            Case hfl.argentina.HasarImpresoraFiscalRG3561.TiposDeResponsabilidadesImpresor.IF_RESPONSABLE_INSCRIPTO
                ToStrCategIVA = "Responsable Inscripto"
        End Select

    End Function

End Module
