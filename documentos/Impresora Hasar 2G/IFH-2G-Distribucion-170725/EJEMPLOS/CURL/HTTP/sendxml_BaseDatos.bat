@echo off
cls
echo .
echo . ### Probando: XML ###
echo .
echo . ###    INICIO     ###
echo .
if "%2" == "" goto salgo

curl http://%1/datafiles.xml --noproxy %1 -H "Content-Type: text/xml" -u:9999 --data-binary @%2 > resp_BDatos.xml
echo .
echo .
type resp_BDatos.xml
echo .
echo .
goto final
:salgo

echo . ERROR de invocaci�n... !! 
echo .
echo . USO:   sendxml ip archivo.xml
echo . Ej.:   sendxml 192.168.1.1 Consulta_Preferencia.xml       
echo .
:final

echo . ###    FIN        ###
echo .

