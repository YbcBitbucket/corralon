host=$1
zeta=$2
password=$3

xmllint="./xmllint --nowarning --html"
curl="./curl"
doclist="verdocs.html"
doc="doc.html"
docs="docs.html"
address="http://user:"$password"@"$host

$curl --noproxy $host --data FECHA=$zeta --data VER=Ver --output $doclist $address/$doclist
$xmllint --shell $doclist <<< `echo 'cat //a/@href'` 2>/dev/null | grep href=\"verdoc > links

echo "<html><head><meta content=\"text/html; charset=ISO-8859-1\" http-equiv=\"Content-Type\"><title>Documentos de la jornada</title>" > $docs
gotstyle=0
for i in `cat links`;
do
	tmp1=${i#*\"};
	tmp2=${tmp1%\"*};
	link=${tmp2/amp;/}
	$curl "$address/$link" --output $doc;
	if [ $gotstyle == 0 ];
	then
		$xmllint $doc --xpath "//style[1]" 2>/dev/null >> $docs
		echo "</head><body>" >> $docs
		gotstyle=1
	fi
	$xmllint $doc --xpath "//table[1]"  2>/dev/null >> $docs
done
echo "</body></html>" >> $docs




