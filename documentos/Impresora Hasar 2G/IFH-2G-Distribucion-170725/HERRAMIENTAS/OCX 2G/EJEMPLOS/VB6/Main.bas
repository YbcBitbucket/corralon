Attribute VB_Name = "ModuleInicial"
Option Explicit

Public proc As Boolean

Sub main()

    proc = False
    FormPrincipal.Show
    
End Sub

'//"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Public Function FromCodigosMatrixDefaultToString(ByVal Value As CodigosMatrixDefault) As String
        Select Case Value
                Case CodigosMatrixDefault.ConceptosFinancieros: FromCodigosMatrixDefaultToString = "ConceptosFinancieros"
                Case CodigosMatrixDefault.DescuentosBonificacionesComerciales: FromCodigosMatrixDefaultToString = "DescuentosBonificacionesComerciales"
                Case CodigosMatrixDefault.AjustesImpositivos: FromCodigosMatrixDefaultToString = "AjustesImpositivos"
                Case CodigosMatrixDefault.Anticipos: FromCodigosMatrixDefaultToString = "Anticipos"
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "CodigosMatrixDefault", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToCodigosMatrixDefault(ByVal Value As String) As CodigosMatrixDefault
        Select Case Value
                Case "ConceptosFinancieros": FromStringToCodigosMatrixDefault = CodigosMatrixDefault.ConceptosFinancieros
                Case "DescuentosBonificacionesComerciales": FromStringToCodigosMatrixDefault = CodigosMatrixDefault.DescuentosBonificacionesComerciales
                Case "AjustesImpositivos": FromStringToCodigosMatrixDefault = CodigosMatrixDefault.AjustesImpositivos
                Case "Anticipos": FromStringToCodigosMatrixDefault = CodigosMatrixDefault.Anticipos
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "CodigosMatrixDefault", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromCondicionesIVAToString(ByVal Value As CondicionesIVA) As String
        Select Case Value
                Case CondicionesIVA.SinCondicionIVA: FromCondicionesIVAToString = "SinCondicionIVA"
                Case CondicionesIVA.NoGravado: FromCondicionesIVAToString = "NoGravado"
                Case CondicionesIVA.Exento: FromCondicionesIVAToString = "Exento"
                Case CondicionesIVA.Gravado: FromCondicionesIVAToString = "Gravado"
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "CondicionesIVA", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToCondicionesIVA(ByVal Value As String) As CondicionesIVA
        Select Case Value
                Case "SinCondicionIVA": FromStringToCondicionesIVA = CondicionesIVA.SinCondicionIVA
                Case "NoGravado": FromStringToCondicionesIVA = CondicionesIVA.NoGravado
                Case "Exento": FromStringToCondicionesIVA = CondicionesIVA.Exento
                Case "Gravado": FromStringToCondicionesIVA = CondicionesIVA.Gravado
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "CondicionesIVA", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTipoDocReceptorToString(ByVal Value As TipoDocReceptor) As String
        Select Case Value
                Case TipoDocReceptor.Cuit: FromTipoDocReceptorToString = "CUIT"
                Case TipoDocReceptor.CUIL: FromTipoDocReceptorToString = "CUIL"
                Case TipoDocReceptor.CDI: FromTipoDocReceptorToString = "CDI"
                Case TipoDocReceptor.LE: FromTipoDocReceptorToString = "LE"
                Case TipoDocReceptor.LC: FromTipoDocReceptorToString = "LC"
                Case TipoDocReceptor.CIExtranjera: FromTipoDocReceptorToString = "CIExtranjera"
                Case TipoDocReceptor.EnTramite: FromTipoDocReceptorToString = "EnTramite"
                Case TipoDocReceptor.ActaNacimiento: FromTipoDocReceptorToString = "ActaNacimiento"
                Case TipoDocReceptor.Pasaporte: FromTipoDocReceptorToString = "Pasaporte"
                Case TipoDocReceptor.CIBsAsRNP: FromTipoDocReceptorToString = "CIBsAsRNP"
                Case TipoDocReceptor.DNI: FromTipoDocReceptorToString = "DNI"
                Case TipoDocReceptor.SinIdentificar: FromTipoDocReceptorToString = "SinIdentificar"
                Case TipoDocReceptor.CertificadoMigracion: FromTipoDocReceptorToString = "CertificadoMigracion"
                Case TipoDocReceptor.PadronANSES: FromTipoDocReceptorToString = "PadronANSES"
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TipoDocReceptor", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTipoDocReceptor(ByVal Value As String) As TipoDocReceptor
        Select Case Value
                Case "CUIT": FromStringToTipoDocReceptor = TipoDocReceptor.Cuit
                Case "CUIL": FromStringToTipoDocReceptor = TipoDocReceptor.CUIL
                Case "CDI": FromStringToTipoDocReceptor = TipoDocReceptor.CDI
                Case "LE": FromStringToTipoDocReceptor = TipoDocReceptor.LE
                Case "LC": FromStringToTipoDocReceptor = TipoDocReceptor.LC
                Case "CIExtranjera": FromStringToTipoDocReceptor = TipoDocReceptor.CIExtranjera
                Case "EnTramite": FromStringToTipoDocReceptor = TipoDocReceptor.EnTramite
                Case "ActaNacimiento": FromStringToTipoDocReceptor = TipoDocReceptor.ActaNacimiento
                Case "Pasaporte": FromStringToTipoDocReceptor = TipoDocReceptor.Pasaporte
                Case "CIBsAsRNP": FromStringToTipoDocReceptor = TipoDocReceptor.CIBsAsRNP
                Case "DNI": FromStringToTipoDocReceptor = TipoDocReceptor.DNI
                Case "SinIdentificar": FromStringToTipoDocReceptor = TipoDocReceptor.SinIdentificar
                Case "CertificadoMigracion": FromStringToTipoDocReceptor = TipoDocReceptor.CertificadoMigracion
                Case "PadronANSES": FromStringToTipoDocReceptor = TipoDocReceptor.PadronANSES
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TipoDocReceptor", "Enumerador " & Value & " desconocido"
        End Select
End Function

'//====================================================================================================================
Public Function FromTiposComprobanteToString(ByVal Value As TiposComprobante) As String
        Select Case Value
                Case TiposComprobante.NoDocumento: FromTiposComprobanteToString = "NoDocumento" '// Valor interno especial.
                Case TiposComprobante.FacturaA: FromTiposComprobanteToString = "FacturaA"
                Case TiposComprobante.NotaDeDebitoA: FromTiposComprobanteToString = "NotaDeDebitoA"
                Case TiposComprobante.NotaDeCreditoA: FromTiposComprobanteToString = "NotaDeCreditoA"
                Case TiposComprobante.ReciboA: FromTiposComprobanteToString = "ReciboA"
                Case TiposComprobante.FacturaB: FromTiposComprobanteToString = "FacturaB"
                Case TiposComprobante.NotaDeDebitoB: FromTiposComprobanteToString = "NotaDeDebitoB"
                Case TiposComprobante.NotaDeCreditoB: FromTiposComprobanteToString = "NotaDeCreditoB"
                Case TiposComprobante.ReciboB: FromTiposComprobanteToString = "ReciboB"
                Case TiposComprobante.FacturaC: FromTiposComprobanteToString = "FacturaC"
                Case TiposComprobante.NotaDeDebitoC: FromTiposComprobanteToString = "NotaDeDebitoC"
                Case TiposComprobante.NotaDeCreditoC: FromTiposComprobanteToString = "NotaDeCreditoC"
                Case TiposComprobante.ReciboC: FromTiposComprobanteToString = "ReciboC"
                Case TiposComprobante.FacturaM: FromTiposComprobanteToString = "FacturaM"
                Case TiposComprobante.NotaDeDebitoM: FromTiposComprobanteToString = "NotaDeDebitoM"
                Case TiposComprobante.NotaDeCreditoM: FromTiposComprobanteToString = "NotaDeCreditoM"
                Case TiposComprobante.ReciboM: FromTiposComprobanteToString = "ReciboM"
                Case TiposComprobante.InformeDiarioDeCierre: FromTiposComprobanteToString = "InformeDiarioDeCierre"
                Case TiposComprobante.TiqueFacturaA: FromTiposComprobanteToString = "TiqueFacturaA"
                Case TiposComprobante.TiqueFacturaB: FromTiposComprobanteToString = "TiqueFacturaB"
                Case TiposComprobante.Tique: FromTiposComprobanteToString = "Tique"
                Case TiposComprobante.RemitoR: FromTiposComprobanteToString = "RemitoR"
                Case TiposComprobante.TiqueNotaCredito: FromTiposComprobanteToString = "TiqueNotaCredito"
                Case TiposComprobante.TiqueFacturaC: FromTiposComprobanteToString = "TiqueFacturaC"
                Case TiposComprobante.TiqueNotaCreditoA: FromTiposComprobanteToString = "TiqueNotaCreditoA"
                Case TiposComprobante.TiqueNotaCreditoB: FromTiposComprobanteToString = "TiqueNotaCreditoB"
                Case TiposComprobante.TiqueNotaCreditoC: FromTiposComprobanteToString = "TiqueNotaCreditoC"
                Case TiposComprobante.TiqueNotaDebitoA: FromTiposComprobanteToString = "TiqueNotaDebitoA"
                Case TiposComprobante.TiqueNotaDebitoB: FromTiposComprobanteToString = "TiqueNotaDebitoB"
                Case TiposComprobante.TiqueNotaDebitoC: FromTiposComprobanteToString = "TiqueNotaDebitoC"
                Case TiposComprobante.TiqueFacturaM: FromTiposComprobanteToString = "TiqueFacturaM"
                Case TiposComprobante.TiqueNotaCreditoM: FromTiposComprobanteToString = "TiqueNotaCreditoM"
                Case TiposComprobante.TiqueNotaDebitoM: FromTiposComprobanteToString = "TiqueNotaDebitoM"
                Case TiposComprobante.RemitoX: FromTiposComprobanteToString = "RemitoX"
                Case TiposComprobante.ReciboX: FromTiposComprobanteToString = "ReciboX"
                Case TiposComprobante.PresupuestoX: FromTiposComprobanteToString = "PresupuestoX"
                Case TiposComprobante.InformeDeAuditoria: FromTiposComprobanteToString = "InformeDeAuditoria"
                Case TiposComprobante.ComprobanteDonacion: FromTiposComprobanteToString = "ComprobanteDonacion"
                Case TiposComprobante.Generico: FromTiposComprobanteToString = "Generico"
                Case TiposComprobante.MensajeCF: FromTiposComprobanteToString = "MensajeCF"
                Case TiposComprobante.DetalleDeVentas: FromTiposComprobanteToString = "DetalleDeVentas"
                Case TiposComprobante.CambioFechaHora: FromTiposComprobanteToString = "CambioFechaHora"
                Case TiposComprobante.CambioCategorizacionIVA: FromTiposComprobanteToString = "CambioCategorizacionIVA"
                Case TiposComprobante.CambioInscripcionIngBrutos: FromTiposComprobanteToString = "CambioInscripcionIngBrutos"
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposComprobante", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposComprobante(ByVal Value As String) As TiposComprobante
        Select Case Value
                Case "NoDocumento": FromStringToTiposComprobante = TiposComprobante.NoDocumento 'Valor interno especial.
                Case "FacturaA": FromStringToTiposComprobante = TiposComprobante.FacturaA 'Factura 'A'
                Case "NotaDeDebitoA": FromStringToTiposComprobante = TiposComprobante.NotaDeDebitoA 'Nota de DTbito 'A'
                Case "NotaDeCreditoA": FromStringToTiposComprobante = TiposComprobante.NotaDeCreditoA 'Nota de CrTdito 'A'
                Case "ReciboA": FromStringToTiposComprobante = TiposComprobante.ReciboA 'Recibo 'A'
                Case "FacturaB": FromStringToTiposComprobante = TiposComprobante.FacturaB 'Factura 'B'
                Case "NotaDeDebitoB": FromStringToTiposComprobante = TiposComprobante.NotaDeDebitoB 'Nota de DTbito 'B'
                Case "NotaDeCreditoB": FromStringToTiposComprobante = TiposComprobante.NotaDeCreditoB 'Nota de CrTdito 'B'
                Case "ReciboB": FromStringToTiposComprobante = TiposComprobante.ReciboB 'Recibo 'B'
                Case "FacturaC": FromStringToTiposComprobante = TiposComprobante.FacturaC 'Factura 'C'
                Case "NotaDeDebitoC": FromStringToTiposComprobante = TiposComprobante.NotaDeDebitoC 'Nota de DTbito 'C'
                Case "NotaDeCreditoC": FromStringToTiposComprobante = TiposComprobante.NotaDeCreditoC 'N ota de CrTdito 'C'
                Case "ReciboC": FromStringToTiposComprobante = TiposComprobante.ReciboC 'Recibo 'C'
                Case "FacturaM": FromStringToTiposComprobante = TiposComprobante.FacturaM 'Factura 'M'
                Case "NotaDeDebitoM": FromStringToTiposComprobante = TiposComprobante.NotaDeDebitoM 'Nota de DTbito 'M'
                Case "NotaDeCreditoM": FromStringToTiposComprobante = TiposComprobante.NotaDeCreditoM 'Nota de CrTdito 'M'
                Case "ReciboM": FromStringToTiposComprobante = TiposComprobante.ReciboM 'Factura 'M'
                Case "InformeDiarioDeCierre": FromStringToTiposComprobante = TiposComprobante.InformeDiarioDeCierre 'Informe Diario de Cierre 'Z'
                Case "TiqueFacturaA": FromStringToTiposComprobante = TiposComprobante.TiqueFacturaA 'Tique Factura 'A'
                Case "TiqueFacturaB": FromStringToTiposComprobante = TiposComprobante.TiqueFacturaB 'Tique Factura 'B'
                Case "Tique": FromStringToTiposComprobante = TiposComprobante.Tique 'Tique
                Case "RemitoR": FromStringToTiposComprobante = TiposComprobante.RemitoR 'Remito 'R'
                Case "TiqueNotaCredito": FromStringToTiposComprobante = TiposComprobante.TiqueNotaCredito 'Tique Nota de CrTdito
                Case "TiqueFacturaC": FromStringToTiposComprobante = TiposComprobante.TiqueFacturaC 'Tique Factura 'C'
                Case "TiqueNotaCreditoA": FromStringToTiposComprobante = TiposComprobante.TiqueNotaCreditoA 'Tique Nota de CrTdito 'A'
                Case "TiqueNotaCreditoB": FromStringToTiposComprobante = TiposComprobante.TiqueNotaCreditoB 'Tique Nota de CrTdito 'B'
                Case "TiqueNotaCreditoC": FromStringToTiposComprobante = TiposComprobante.TiqueNotaCreditoC 'Tique Nota de CrTdito 'C'
                Case "TiqueNotaDebitoA": FromStringToTiposComprobante = TiposComprobante.TiqueNotaDebitoA 'Tique Nota de DTbito 'A'
                Case "TiqueNotaDebitoB": FromStringToTiposComprobante = TiposComprobante.TiqueNotaDebitoB 'Tique Nota de DTbito 'B'
                Case "TiqueNotaDebitoC": FromStringToTiposComprobante = TiposComprobante.TiqueNotaDebitoC 'Tique Nota de DTbito 'C'
                Case "TiqueFacturaM": FromStringToTiposComprobante = TiposComprobante.TiqueFacturaM 'Tique Factura 'M'
                Case "TiqueNotaCreditoM": FromStringToTiposComprobante = TiposComprobante.TiqueNotaCreditoM 'Tique Nota de CrTdito 'M'
                Case "TiqueNotaDebitoM": FromStringToTiposComprobante = TiposComprobante.TiqueNotaDebitoM 'Tique Nota de DTbito 'B'
                Case "RemitoX": FromStringToTiposComprobante = TiposComprobante.RemitoX 'Remito 'X'
                Case "ReciboX": FromStringToTiposComprobante = TiposComprobante.ReciboX 'Recibo 'X'
                Case "PresupuestoX": FromStringToTiposComprobante = TiposComprobante.PresupuestoX 'Presupuesto 'X'
                Case "InformeDeAuditoria": FromStringToTiposComprobante = TiposComprobante.InformeDeAuditoria 'Informe de Auditorfa
                Case "ComprobanteDonacion": FromStringToTiposComprobante = TiposComprobante.ComprobanteDonacion 'Comprobante Donaci=n
                Case "Generico": FromStringToTiposComprobante = TiposComprobante.Generico 'Documento GenTrico
                Case "MensajeCF": FromStringToTiposComprobante = TiposComprobante.MensajeCF 'Mensaje del Contgrolador Fiscal
                Case "DetalleDeVentas": FromStringToTiposComprobante = TiposComprobante.DetalleDeVentas 'Detalle de Ventas (Cierre 'X')
                Case "CambioFechaHora": FromStringToTiposComprobante = TiposComprobante.CambioFechaHora 'Cambio de Fecha y Hora
                Case "CambioCategorizacionIVA": FromStringToTiposComprobante = TiposComprobante.CambioCategorizacionIVA 'Cambio de categorfa frente a IVA
                Case "CambioInscripcionIngBrutos": FromStringToTiposComprobante = TiposComprobante.CambioInscripcionIngBrutos 'Cambio Inscripci=n en Ingresos Brutos
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposComprobante", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposHabilitacionToString(ByVal Value As TiposHabilitacion) As String
        Select Case Value
                Case TiposHabilitacion.ComprobantesA: FromTiposHabilitacionToString = "ComprobantesA"
                Case TiposHabilitacion.ComprobantesAConLeyenda: FromTiposHabilitacionToString = "ComprobantesAConLeyenda"
                Case TiposHabilitacion.ComprobantesM: FromTiposHabilitacionToString = "ComprobantesM"
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposHabilitacion", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposHabilitacion(ByVal Value As String) As TiposHabilitacion
        Select Case Value
                Case "ComprobantesA": FromStringToTiposHabilitacion = TiposHabilitacion.ComprobantesA
                Case "ComprobantesAConLeyenda": FromStringToTiposHabilitacion = TiposHabilitacion.ComprobantesAConLeyenda
                Case "ComprobantesM": FromStringToTiposHabilitacion = TiposHabilitacion.ComprobantesM
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposHabilitacion", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposPagoToString(ByVal Value As TiposPago) As String
        Select Case Value
                Case TiposPago.Cambio: FromTiposPagoToString = "Cambio"
                Case TiposPago.CartaDeCreditoDocumentario: FromTiposPagoToString = "CartaDeCreditoDocumentario"
                Case TiposPago.CartaDeCreditoSimple: FromTiposPagoToString = "CartaDeCreditoSimple"
                Case TiposPago.Cheque: FromTiposPagoToString = "Cheque"
                Case TiposPago.ChequeCancelatorios: FromTiposPagoToString = "ChequeCancelatorios"
                Case TiposPago.CreditoDocumentario: FromTiposPagoToString = "CreditoDocumentario"
                Case TiposPago.CuentaCorriente: FromTiposPagoToString = "CuentaCorriente"
                Case TiposPago.Deposito: FromTiposPagoToString = "Deposito"
                Case TiposPago.Efectivo: FromTiposPagoToString = "Efectivo"
                Case TiposPago.EndosoDeCheque: FromTiposPagoToString = "EndosoDeCheque"
                Case TiposPago.FacturaDeCredito: FromTiposPagoToString = "FacturaDeCredito"
                Case TiposPago.GarantiaBancaria: FromTiposPagoToString = "GarantiaBancaria"
                Case TiposPago.Giro: FromTiposPagoToString = "Giro"
                Case TiposPago.LetraDeCambio: FromTiposPagoToString = "LetraDeCambio"
                Case TiposPago.MedioDePagoDeComercioExterior: FromTiposPagoToString = "MedioDePagoDeComercioExterior"
                Case TiposPago.OrdenDePagoDocumentaria: FromTiposPagoToString = "OrdenDePagoDocumentaria"
                Case TiposPago.OrdenDePagoSimple: FromTiposPagoToString = "OrdenDePagoSimple"
                Case TiposPago.PagoContraReembolso: FromTiposPagoToString = "PagoContraReembolso"
                Case TiposPago.RemesaDocumentaria: FromTiposPagoToString = "RemesaDocumentaria"
                Case TiposPago.RemesaSimple: FromTiposPagoToString = "RemesaSimple"
                Case TiposPago.TarjetaDeCredito: FromTiposPagoToString = "TarjetaDeCredito"
                Case TiposPago.TarjetaDeDebito: FromTiposPagoToString = "TarjetaDeDebito"
                Case TiposPago.Ticket: FromTiposPagoToString = "Ticket"
                Case TiposPago.TransferenciaBancaria: FromTiposPagoToString = "TransferenciaBancaria"
                Case TiposPago.TransferenciaNoBancaria: FromTiposPagoToString = "TransferenciaNoBancaria"
                Case TiposPago.OtrosMediosPago: FromTiposPagoToString = "OtrosMediosPago"
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposPago", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposPago(ByVal Value As String) As TiposPago
        Select Case Value
                Case "Cambio": FromStringToTiposPago = TiposPago.Cambio
                Case "CartaDeCreditoDocumentario": FromStringToTiposPago = TiposPago.CartaDeCreditoDocumentario
                Case "CartaDeCreditoSimple": FromStringToTiposPago = TiposPago.CartaDeCreditoSimple
                Case "Cheque": FromStringToTiposPago = TiposPago.Cheque
                Case "ChequeCancelatorios": FromStringToTiposPago = TiposPago.ChequeCancelatorios
                Case "CreditoDocumentario": FromStringToTiposPago = TiposPago.CreditoDocumentario
                Case "CuentaCorriente": FromStringToTiposPago = TiposPago.CuentaCorriente
                Case "Deposito": FromStringToTiposPago = TiposPago.Deposito
                Case "Efectivo": FromStringToTiposPago = TiposPago.Efectivo
                Case "EndosoDeCheque": FromStringToTiposPago = TiposPago.EndosoDeCheque
                Case "FacturaDeCredito": FromStringToTiposPago = TiposPago.FacturaDeCredito
                Case "GarantiaBancaria": FromStringToTiposPago = TiposPago.GarantiaBancaria
                Case "Giro": FromStringToTiposPago = TiposPago.Giro
                Case "LetraDeCambio": FromStringToTiposPago = TiposPago.LetraDeCambio
                Case "MedioDePagoDeComercioExterior": FromStringToTiposPago = TiposPago.MedioDePagoDeComercioExterior
                Case "OrdenDePagoDocumentaria": FromStringToTiposPago = TiposPago.OrdenDePagoDocumentaria
                Case "OrdenDePagoSimple": FromStringToTiposPago = TiposPago.OrdenDePagoSimple
                Case "PagoContraReembolso": FromStringToTiposPago = TiposPago.PagoContraReembolso
                Case "RemesaDocumentaria": FromStringToTiposPago = TiposPago.RemesaDocumentaria
                Case "RemesaSimple": FromStringToTiposPago = TiposPago.RemesaSimple
                Case "TarjetaDeCredito": FromStringToTiposPago = TiposPago.TarjetaDeCredito
                Case "TarjetaDeDebito": FromStringToTiposPago = TiposPago.TarjetaDeDebito
                Case "Ticket": FromStringToTiposPago = TiposPago.Ticket
                Case "TransferenciaBancaria": FromStringToTiposPago = TiposPago.TransferenciaBancaria
                Case "TransferenciaNoBancaria": FromStringToTiposPago = TiposPago.TransferenciaNoBancaria
                Case "OtrosMediosPago": FromStringToTiposPago = TiposPago.OtrosMediosPago
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposPago", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposResponsableToString(ByVal Value As TiposResponsable) As String
        Select Case Value
                Case TiposResponsable.IVAResponsableInscripto: FromTiposResponsableToString = "IVAResponsableInscripto"
                Case TiposResponsable.IVAResponsableNoInscripto: FromTiposResponsableToString = "IVAResponsableNoInscripto"
                Case TiposResponsable.IVANoResponsable: FromTiposResponsableToString = "IVANoResponsable"
                Case TiposResponsable.IVASujetoExento: FromTiposResponsableToString = "IVASujetoExento"
                Case TiposResponsable.ConsumidorFinal: FromTiposResponsableToString = "ConsumidorFinal"
                Case TiposResponsable.ResponsableMonotributo: FromTiposResponsableToString = "ResponsableMonotributo"
                Case TiposResponsable.SujetoNoCategorizado: FromTiposResponsableToString = "SujetoNoCategorizado"
                Case TiposResponsable.ProveedorDelExterior: FromTiposResponsableToString = "ProveedorDelExterior"
                Case TiposResponsable.ClienteDelExterior: FromTiposResponsableToString = "ClienteDelExterior"
                Case TiposResponsable.IVALiberadoLey19640: FromTiposResponsableToString = "IVALiberadoLey19640"
                Case TiposResponsable.IVAResponsableInscriptoAgentePercepcion: FromTiposResponsableToString = "IVAResponsableInscriptoAgentePercepcion"
                Case TiposResponsable.PequenoContribuyenteEventual: FromTiposResponsableToString = "PequenoContribuyenteEventual"
                Case TiposResponsable.MonotributistaSocial: FromTiposResponsableToString = "MonotributistaSocial"
                Case TiposResponsable.PequenoContribuyenteEventualSocial: FromTiposResponsableToString = "PequenoContribuyenteEventualSocial"
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposResponsable", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposResponsable(ByVal Value As String) As TiposResponsable
        Select Case Value
                Case "IVAResponsableInscripto": FromStringToTiposResponsable = TiposResponsable.IVAResponsableInscripto
                Case "IVAResponsableNoInscripto": FromStringToTiposResponsable = TiposResponsable.IVAResponsableNoInscripto
                Case "IVANoResponsable": FromStringToTiposResponsable = TiposResponsable.IVANoResponsable
                Case "IVASujetoExento": FromStringToTiposResponsable = TiposResponsable.IVASujetoExento
                Case "ConsumidorFinal": FromStringToTiposResponsable = TiposResponsable.ConsumidorFinal
                Case "ResponsableMonotributo": FromStringToTiposResponsable = TiposResponsable.ResponsableMonotributo
                Case "SujetoNoCategorizado": FromStringToTiposResponsable = TiposResponsable.SujetoNoCategorizado
                Case "ProveedorDelExterior": FromStringToTiposResponsable = TiposResponsable.ProveedorDelExterior
                Case "ClienteDelExterior": FromStringToTiposResponsable = TiposResponsable.ClienteDelExterior
                Case "IVALiberadoLey19640": FromStringToTiposResponsable = TiposResponsable.IVALiberadoLey19640
                Case "IVAResponsableInscriptoAgentePercepcion": FromStringToTiposResponsable = TiposResponsable.IVAResponsableInscriptoAgentePercepcion
                Case "PequenoContribuyenteEventual": FromStringToTiposResponsable = TiposResponsable.PequenoContribuyenteEventual
                Case "MonotributistaSocial": FromStringToTiposResponsable = TiposResponsable.MonotributistaSocial
                Case "PequenoContribuyenteEventualSocial": FromStringToTiposResponsable = TiposResponsable.PequenoContribuyenteEventualSocial
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposResponsable", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposTributosToString(ByVal Value As TiposTributos) As String
        Select Case Value
                Case TiposTributos.SinImpuestos: FromTiposTributosToString = "SinImpuestos"
                Case TiposTributos.ImpuestosNacionales: FromTiposTributosToString = "ImpuestosNacionales"
                Case TiposTributos.ImpuestosProvinciales: FromTiposTributosToString = "ImpuestosProvinciales"
                Case TiposTributos.ImpuestosMunicipales: FromTiposTributosToString = "ImpuestosMunicipales"
                Case TiposTributos.ImpuestosInternos: FromTiposTributosToString = "ImpuestosInternos"
                Case TiposTributos.IIBB: FromTiposTributosToString = "IIBB"
                Case TiposTributos.PercepcionIVA: FromTiposTributosToString = "PercepcionIVA"
                Case TiposTributos.PercepcionIIBB: FromTiposTributosToString = "PercepcionIIBB"
                Case TiposTributos.PercepcionImpuestosMunicipales: FromTiposTributosToString = "PercepcionImpuestosMunicipales"
                Case TiposTributos.OtrasPercepciones: FromTiposTributosToString = "OtrasPercepciones"
                Case TiposTributos.ImpuestoInternoItem: FromTiposTributosToString = "ImpuestoInternoItem"
                Case TiposTributos.OtrosTributos: FromTiposTributosToString = "OtrosTributos"
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposTributos", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposTributos(ByVal Value As String) As TiposTributos
        Select Case Value
                Case "SinImpuestos": FromStringToTiposTributos = TiposTributos.SinImpuestos
                Case "ImpuestosNacionales": FromStringToTiposTributos = TiposTributos.ImpuestosNacionales
                Case "ImpuestosProvinciales": FromStringToTiposTributos = TiposTributos.ImpuestosProvinciales
                Case "ImpuestosMunicipales": FromStringToTiposTributos = TiposTributos.ImpuestosMunicipales
                Case "ImpuestosInternos": FromStringToTiposTributos = TiposTributos.ImpuestosInternos
                Case "IIBB": FromStringToTiposTributos = TiposTributos.IIBB
                Case "PercepcionIVA": FromStringToTiposTributos = TiposTributos.PercepcionIVA
                Case "PercepcionIIBB": FromStringToTiposTributos = TiposTributos.PercepcionIIBB
                Case "PercepcionImpuestosMunicipales": FromStringToTiposTributos = TiposTributos.PercepcionImpuestosMunicipales
                Case "OtrasPercepciones": FromStringToTiposTributos = TiposTributos.OtrasPercepciones
                Case "ImpuestoInternoItem": FromStringToTiposTributos = TiposTributos.ImpuestoInternoItem
                Case "OtrosTributos": FromStringToTiposTributos = TiposTributos.OtrosTributos
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposTributos", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromUnidadesMedidaToString(ByVal Value As UnidadesMedida) As String
        Select Case Value
                Case UnidadesMedida.SinDescripcion: FromUnidadesMedidaToString = "SinDescripcion" 'Vacfo. No hay sufijo.
                Case UnidadesMedida.Kilo: FromUnidadesMedidaToString = "Kilo" 'Kg
                Case UnidadesMedida.Metro: FromUnidadesMedidaToString = "Metro" 'm
                Case UnidadesMedida.MetroCuadrado: FromUnidadesMedidaToString = "MetroCuadrado" 'm2
                Case UnidadesMedida.MetroCubico: FromUnidadesMedidaToString = "MetroCubico" 'm3
                Case UnidadesMedida.Litro: FromUnidadesMedidaToString = "Litro" 'lt
                Case UnidadesMedida.KWH: FromUnidadesMedidaToString = "KWH" 'kw
                Case UnidadesMedida.Unidad: FromUnidadesMedidaToString = "Unidad" 'vacfo. No hay sufijo.
                Case UnidadesMedida.Par: FromUnidadesMedidaToString = "Par" 'par(es) Seg�n corresponda.
                Case UnidadesMedida.Docena: FromUnidadesMedidaToString = "Docena" 'docena(s) seg�n corresponda.
                Case UnidadesMedida.Quilate: FromUnidadesMedidaToString = "Quilate" 'CT
                Case UnidadesMedida.Millar: FromUnidadesMedidaToString = "Millar" 'millar(es) Seg�n corresponda.
                Case UnidadesMedida.MegaUInterActAntib: FromUnidadesMedidaToString = "MegaUInterActAntib" 'Mega unidad internacional de actividad antibi=ticos.
                Case UnidadesMedida.UnidadInternaActInmung: FromUnidadesMedidaToString = "UnidadInternaActInmung" 'Unidad internacional de actividad inmunoglobulina.
                Case UnidadesMedida.Gramo: FromUnidadesMedidaToString = "Gramo" 'g
                Case UnidadesMedida.Milimetro: FromUnidadesMedidaToString = "Milimetro" 'mm
                Case UnidadesMedida.MilimetroCubico: FromUnidadesMedidaToString = "MilimetroCubico" 'mm3
                Case UnidadesMedida.Kilometro: FromUnidadesMedidaToString = "Kilometro" 'Km
                Case UnidadesMedida.Hectolitro: FromUnidadesMedidaToString = "Hectolitro" 'hl
                Case UnidadesMedida.MegaUnidadIntActInmung: FromUnidadesMedidaToString = "MegaUnidadIntActInmung" 'muia Mega unidad internacional de actividad inmunoglobulina.
                Case UnidadesMedida.Centimetro: FromUnidadesMedidaToString = "Centimetro" 'cm
                Case UnidadesMedida.KilogramoActivo: FromUnidadesMedidaToString = "KilogramoActivo" 'Kg act
                Case UnidadesMedida.GramoActivo: FromUnidadesMedidaToString = "GramoActivo" 'g act
                Case UnidadesMedida.GramoBase: FromUnidadesMedidaToString = "GramoBase" 'g base
                Case UnidadesMedida.UIACTHOR: FromUnidadesMedidaToString = "UIACTHOR" 'uiacthor Unidad internacional de actividad hormonal.
                Case UnidadesMedida.JuegoPaqueteMazoNaipes: FromUnidadesMedidaToString = "JuegoPaqueteMazoNaipes" 'mazo(s) seg�n corresponda.
                Case UnidadesMedida.MUIACTHOR: FromUnidadesMedidaToString = "MUIACTHOR" 'muiacthor Mega unidad internacional de actividad hormonal.
                Case UnidadesMedida.CentimetroCubico: FromUnidadesMedidaToString = "CentimetroCubico" 'cm3
                Case UnidadesMedida.UIACTANT: FromUnidadesMedidaToString = "UIACTANT" 'uiactant Unidad internacional de actividad antibi=ticos.
                Case UnidadesMedida.Tonelada: FromUnidadesMedidaToString = "Tonelada" 'ton
                Case UnidadesMedida.DecametroCubico: FromUnidadesMedidaToString = "DecametroCubico" 'dam3
                Case UnidadesMedida.HectometroCubico: FromUnidadesMedidaToString = "HectometroCubico" 'hm3
                Case UnidadesMedida.KilometroCubico: FromUnidadesMedidaToString = "KilometroCubico" 'Km3
                Case UnidadesMedida.Microgramo: FromUnidadesMedidaToString = "Microgramo" 'mcg
                Case UnidadesMedida.Nanogramo: FromUnidadesMedidaToString = "Nanogramo" 'ng
                Case UnidadesMedida.Picogramo: FromUnidadesMedidaToString = "Picogramo" 'pg
                Case UnidadesMedida.MUIACTANT: FromUnidadesMedidaToString = "MUIACTANT" 'Mega unidad internacional de actividad antibi=ticos.
                Case UnidadesMedida.UIACTIG: FromUnidadesMedidaToString = "UIACTIG" 'Unidad internacional de actividad inmunoglobulina.
                Case UnidadesMedida.Miligramo: FromUnidadesMedidaToString = "Miligramo" 'mcg
                Case UnidadesMedida.Mililitro: FromUnidadesMedidaToString = "Mililitro" 'ml
                Case UnidadesMedida.Curie: FromUnidadesMedidaToString = "Curie" 'Ci
                Case UnidadesMedida.Milicurie: FromUnidadesMedidaToString = "Milicurie" 'mCi
                Case UnidadesMedida.Microcurie: FromUnidadesMedidaToString = "Microcurie"
                Case UnidadesMedida.UInterActHormonal: FromUnidadesMedidaToString = "UInterActHormonal" 'uiah Unidad internacional de actividad hormonal.
                Case UnidadesMedida.MegaUInterActHor: FromUnidadesMedidaToString = "MegaUInterActHor" 'muiah Mega unidad internacional de actividad hormonal.
                Case UnidadesMedida.KilogramoBase: FromUnidadesMedidaToString = "KilogramoBase" 'Kg base
                Case UnidadesMedida.Gruesa: FromUnidadesMedidaToString = "Gruesa" 'gruesa(s) Seg�n corresponda.
                Case UnidadesMedida.MUIACTIG: FromUnidadesMedidaToString = "MUIACTIG" 'muiactig Mega unidad internacional de actividad inmunoglobulina.
                Case UnidadesMedida.KilogramoBruto: FromUnidadesMedidaToString = "KilogramoBruto" 'Kg bruto(s) Seg�n corresponda.
                Case UnidadesMedida.Pack: FromUnidadesMedidaToString = "Pack" 'pack
                Case UnidadesMedida.Horma: FromUnidadesMedidaToString = "Horma" 'horma(s) Seg�n corresponda.
                Case UnidadesMedida.Donacion: FromUnidadesMedidaToString = "Donacion" 'Vacfo. No hay sufijo.
                Case UnidadesMedida.Ajustes: FromUnidadesMedidaToString = "Ajustes" 'Vacfo. No hay sufijo.
                Case UnidadesMedida.Anulacion: FromUnidadesMedidaToString = "Anulacion" 'Vacfo. No hay sufijo.
                Case UnidadesMedida.SenasAnticipos: FromUnidadesMedidaToString = "SenasAnticipos" 'Vacfo. No hay sufijo.
                Case UnidadesMedida.OtrasUnidades: FromUnidadesMedidaToString = "OtrasUnidades" 'Vacfo. No hay sufijo.
                Case UnidadesMedida.Bonificacion: FromUnidadesMedidaToString = "Bonificacion" 'Vacfo. No hay sufijo.
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "UnidadesMedida", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToUnidadesMedida(ByVal Value As String) As UnidadesMedida
        Select Case Value
                Case "SinDescripcion": FromStringToUnidadesMedida = UnidadesMedida.SinDescripcion 'Vacfo. No hay sufijo.
                Case "Kilo": FromStringToUnidadesMedida = UnidadesMedida.Kilo 'Kg
                Case "Metro": FromStringToUnidadesMedida = UnidadesMedida.Metro 'm
                Case "MetroCuadrado": FromStringToUnidadesMedida = UnidadesMedida.MetroCuadrado 'm2
                Case "MetroCubico": FromStringToUnidadesMedida = UnidadesMedida.MetroCubico 'm3
                Case "Litro": FromStringToUnidadesMedida = UnidadesMedida.Litro 'lt
                Case "KWH": FromStringToUnidadesMedida = UnidadesMedida.KWH 'kw
                Case "Unidad": FromStringToUnidadesMedida = UnidadesMedida.Unidad 'vacfo. No hay sufijo.
                Case "Par": FromStringToUnidadesMedida = UnidadesMedida.Par 'par(es) Seg�n corresponda.
                Case "Docena": FromStringToUnidadesMedida = UnidadesMedida.Docena 'docena(s) seg�n corresponda.
                Case "Quilate": FromStringToUnidadesMedida = UnidadesMedida.Quilate 'CT
                Case "Millar": FromStringToUnidadesMedida = UnidadesMedida.Millar 'millar(es) Seg�n corresponda.
                Case "MegaUInterActAntib": FromStringToUnidadesMedida = UnidadesMedida.MegaUInterActAntib 'Mega unidad internacional de actividad antibi=ticos.
                Case "UnidadInternaActInmung": FromStringToUnidadesMedida = UnidadesMedida.UnidadInternaActInmung 'Unidad internacional de actividad inmunoglobulina.
                Case "Gramo": FromStringToUnidadesMedida = UnidadesMedida.Gramo 'g
                Case "Milimetro": FromStringToUnidadesMedida = UnidadesMedida.Milimetro 'mm
                Case "MilimetroCubico": FromStringToUnidadesMedida = UnidadesMedida.MilimetroCubico 'mm3
                Case "Kilometro": FromStringToUnidadesMedida = UnidadesMedida.Kilometro 'Km
                Case "Hectolitro": FromStringToUnidadesMedida = UnidadesMedida.Hectolitro 'hl
                Case "MegaUnidadIntActInmung": FromStringToUnidadesMedida = UnidadesMedida.MegaUnidadIntActInmung 'muia Mega unidad internacional de actividad inmunoglobulina.
                Case "Centimetro": FromStringToUnidadesMedida = UnidadesMedida.Centimetro 'cm
                Case "KilogramoActivo": FromStringToUnidadesMedida = UnidadesMedida.KilogramoActivo 'Kg act
                Case "GramoActivo": FromStringToUnidadesMedida = UnidadesMedida.GramoActivo 'g act
                Case "GramoBase": FromStringToUnidadesMedida = UnidadesMedida.GramoBase 'g base
                Case "UIACTHOR": FromStringToUnidadesMedida = UnidadesMedida.UIACTHOR 'uiacthor Unidad internacional de actividad hormonal.
                Case "JuegoPaqueteMazoNaipes": FromStringToUnidadesMedida = UnidadesMedida.JuegoPaqueteMazoNaipes 'mazo(s) seg�n corresponda.
                Case "MUIACTHOR": FromStringToUnidadesMedida = UnidadesMedida.MUIACTHOR 'muiacthor Mega unidad internacional de actividad hormonal.
                Case "CentimetroCubico": FromStringToUnidadesMedida = UnidadesMedida.CentimetroCubico 'cm3
                Case "UIACTANT": FromStringToUnidadesMedida = UnidadesMedida.UIACTANT 'uiactant Unidad internacional de actividad antibi=ticos.
                Case "Tonelada": FromStringToUnidadesMedida = UnidadesMedida.Tonelada 'ton
                Case "DecametroCubico": FromStringToUnidadesMedida = UnidadesMedida.DecametroCubico 'dam3
                Case "HectometroCubico": FromStringToUnidadesMedida = UnidadesMedida.HectometroCubico 'hm3
                Case "KilometroCubico": FromStringToUnidadesMedida = UnidadesMedida.KilometroCubico 'Km3
                Case "Microgramo": FromStringToUnidadesMedida = UnidadesMedida.Microgramo 'mcg
                Case "Nanogramo": FromStringToUnidadesMedida = UnidadesMedida.Nanogramo 'ng
                Case "Picogramo": FromStringToUnidadesMedida = UnidadesMedida.Picogramo 'pg
                Case "MUIACTANT": FromStringToUnidadesMedida = UnidadesMedida.MUIACTANT 'Mega unidad internacional de actividad antibi=ticos.
                Case "UIACTIG": FromStringToUnidadesMedida = UnidadesMedida.UIACTIG 'Unidad internacional de actividad inmunoglobulina.
                Case "Miligramo": FromStringToUnidadesMedida = UnidadesMedida.Miligramo 'mcg
                Case "Mililitro": FromStringToUnidadesMedida = UnidadesMedida.Mililitro 'ml
                Case "Curie": FromStringToUnidadesMedida = UnidadesMedida.Curie 'Ci
                Case "Milicurie": FromStringToUnidadesMedida = UnidadesMedida.Milicurie 'mCi
                Case "Microcurie": FromStringToUnidadesMedida = UnidadesMedida.Microcurie
                Case "UInterActHormonal": FromStringToUnidadesMedida = UnidadesMedida.UInterActHormonal 'uiah Unidad internacional de actividad hormonal.
                Case "MegaUInterActHor": FromStringToUnidadesMedida = UnidadesMedida.MegaUInterActHor 'muiah Mega unidad internacional de actividad hormonal.
                Case "KilogramoBase": FromStringToUnidadesMedida = UnidadesMedida.KilogramoBase 'Kg base
                Case "Gruesa": FromStringToUnidadesMedida = UnidadesMedida.Gruesa 'gruesa(s) Seg�n corresponda.
                Case "MUIACTIG": FromStringToUnidadesMedida = UnidadesMedida.MUIACTIG 'muiactig Mega unidad internacional de actividad inmunoglobulina.
                Case "KilogramoBruto": FromStringToUnidadesMedida = UnidadesMedida.KilogramoBruto 'Kg bruto(s) Seg�n corresponda.
                Case "Pack": FromStringToUnidadesMedida = UnidadesMedida.Pack 'pack
                Case "Horma": FromStringToUnidadesMedida = UnidadesMedida.Horma 'horma(s) Seg�n corresponda.
                Case "Donacion": FromStringToUnidadesMedida = UnidadesMedida.Donacion 'Vacfo. No hay sufijo.
                Case "Ajustes": FromStringToUnidadesMedida = UnidadesMedida.Ajustes 'Vacfo. No hay sufijo.
                Case "Anulacion": FromStringToUnidadesMedida = UnidadesMedida.Anulacion 'Vacfo. No hay sufijo.
                Case "SenasAnticipos": FromStringToUnidadesMedida = UnidadesMedida.SenasAnticipos 'Vacfo. No hay sufijo.
                Case "OtrasUnidades": FromStringToUnidadesMedida = UnidadesMedida.OtrasUnidades 'Vacfo. No hay sufijo.
                Case "Bonificacion": FromStringToUnidadesMedida = UnidadesMedida.Bonificacion 'Vacfo. No hay sufijo.
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "UnidadesMedida", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromBaudiosToString(ByVal Value As Baudios) As String
        Select Case Value
                Case Baudios.Baudrate1200: FromBaudiosToString = "Baudrate1200" '1200 baudios
                Case Baudios.Baudrate2400: FromBaudiosToString = "Baudrate2400" '2400 baudios
                Case Baudios.Baudrate4800: FromBaudiosToString = "Baudrate4800" '4800 baudios
                Case Baudios.Baudrate9600: FromBaudiosToString = "Baudrate9600" '9600 baudios
                Case Baudios.Baudrate19200: FromBaudiosToString = "Baudrate19200" '19200 baudios
                Case Baudios.Baudrate38400: FromBaudiosToString = "Baudrate38400" '38400 baudios
                Case Baudios.Baudrate57600: FromBaudiosToString = "Baudrate57600" '57600 baudios
                Case Baudios.Baudrate115200: FromBaudiosToString = "Baudrate115200" '115200 baudios
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "Baudios", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToBaudios(ByVal Value As String) As Baudios
        Select Case Value
                Case "Baudrate1200": FromStringToBaudios = Baudios.Baudrate1200 '1200 baudios
                Case "Baudrate2400": FromStringToBaudios = Baudios.Baudrate2400 '2400 baudios
                Case "Baudrate4800": FromStringToBaudios = Baudios.Baudrate4800 '4800 baudios
                Case "Baudrate9600": FromStringToBaudios = Baudios.Baudrate9600 '9600 baudios
                Case "Baudrate19200": FromStringToBaudios = Baudios.Baudrate19200 '19200 baudios
                Case "Baudrate38400": FromStringToBaudios = Baudios.Baudrate38400 '38400 baudios
                Case "Baudrate57600": FromStringToBaudios = Baudios.Baudrate57600 '57600 baudios
                Case "Baudrate115200": FromStringToBaudios = Baudios.Baudrate115200 '115200 baudios
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "Baudios", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromCapacidadesToString(ByVal Value As Capacidades) As String
        Select Case Value
                Case Capacidades.SoportaEstacion: FromCapacidadesToString = "SoportaEstacion" 'Soporte de tipo de estaci=n
                Case Capacidades.SoportaComprobante: FromCapacidadesToString = "SoportaComprobante" 'Soporte de tipos de comprobante en una determinada estaci=n
                Case Capacidades.SoportaCajon: FromCapacidadesToString = "SoportaCajon" 'Soporte de caj=n
                Case Capacidades.AnchoTotalImpresion: FromCapacidadesToString = "AnchoTotalImpresion" 'Ancho total de impresi=n en una determinada estaci=n
                Case Capacidades.AnchoRazonSocial: FromCapacidadesToString = "AnchoRazonSocial" 'Ancho m�ximo de la raz=n social en una determinada estaci=n
                Case Capacidades.AnchoTextoFiscal: FromCapacidadesToString = "AnchoTextoFiscal" 'Ancho m�ximo de texto fiscal en una determinada estaci=n
                Case Capacidades.AnchoTextoVenta: FromCapacidadesToString = "AnchoTextoVenta" 'Ancho m�ximo del campo de venta
                Case Capacidades.AnchoTextoNoFiscal: FromCapacidadesToString = "AnchoTextoNoFiscal" 'Ancho m�ximo del texto no fiscal
                Case Capacidades.AnchoTextoLineasUsuario: FromCapacidadesToString = "AnchoTextoLineasUsuario" 'Ancho m�ximo de lfneas de usuario
                Case Capacidades.AnchoLogo: FromCapacidadesToString = "AnchoLogo" 'Ancho del logo
                Case Capacidades.AltoLogo: FromCapacidadesToString = "AltoLogo" 'Alto del logo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "Capacidades", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToCapacidades(ByVal Value As String) As Capacidades
        Select Case Value
                Case "SoportaEstacion": FromStringToCapacidades = Capacidades.SoportaEstacion 'Soporte de tipo de estaci=n
                Case "SoportaComprobante": FromStringToCapacidades = Capacidades.SoportaComprobante 'Soporte de tipos de comprobante en una determinada estaci=n
                Case "SoportaCajon": FromStringToCapacidades = Capacidades.SoportaCajon 'Soporte de caj=n
                Case "AnchoTotalImpresion": FromStringToCapacidades = Capacidades.AnchoTotalImpresion 'Ancho total de impresi=n en una determinada estaci=n
                Case "AnchoRazonSocial": FromStringToCapacidades = Capacidades.AnchoRazonSocial 'Ancho m�ximo de la raz=n social en una determinada estaci=n
                Case "AnchoTextoFiscal": FromStringToCapacidades = Capacidades.AnchoTextoFiscal 'Ancho m�ximo de texto fiscal en una determinada estaci=n
                Case "AnchoTextoVenta": FromStringToCapacidades = Capacidades.AnchoTextoVenta 'Ancho m�ximo del campo de venta
                Case "AnchoTextoNoFiscal": FromStringToCapacidades = Capacidades.AnchoTextoNoFiscal 'Ancho m�ximo del texto no fiscal
                Case "AnchoTextoLineasUsuario": FromStringToCapacidades = Capacidades.AnchoTextoLineasUsuario 'Ancho m�ximo de lfneas de usuario
                Case "AnchoLogo": FromStringToCapacidades = Capacidades.AnchoLogo 'Ancho del logo
                Case "AltoLogo": FromStringToCapacidades = Capacidades.AltoLogo 'Alto del logo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "Capacidades", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromCompresionDeInformacionToString(ByVal Value As CompresionDeInformacion) As String
        Select Case Value
                Case CompresionDeInformacion.Comprime: FromCompresionDeInformacionToString = "Comprime" 'Comprime -zipea- la informaci=n
                Case CompresionDeInformacion.NoComprime: FromCompresionDeInformacionToString = "NoComprime" 'No comprime la informaci=n
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "CompresionDeInformacion", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToCompresionDeInformacion(ByVal Value As String) As CompresionDeInformacion
        Select Case Value
                Case "Comprime": FromStringToCompresionDeInformacion = CompresionDeInformacion.Comprime 'Comprime -zipea- la informaci=n
                Case "NoComprime": FromStringToCompresionDeInformacion = CompresionDeInformacion.NoComprime 'No comprime la informaci=n
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "CompresionDeInformacion", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromConfiguracionToString(ByVal Value As Configuracion) As String
        Select Case Value
                Case Configuracion.LimiteBC: FromConfiguracionToString = "LimiteBC" 'Monto m�ximo comprobantes 'B/C'.
                Case Configuracion.ImpresionCambio: FromConfiguracionToString = "ImpresionCambio" 'Impresi=n "Cambio $0.00"
                Case Configuracion.ImpresionLeyendas: FromConfiguracionToString = "ImpresionLeyendas" 'Impresi=n de leyendas opcionales
                Case Configuracion.CortePapel: FromConfiguracionToString = "CortePapel" 'Opciones de corte de papel
                Case Configuracion.ReimpresionCancelados: FromConfiguracionToString = "ReimpresionCancelados" 'Reimpresi=n de comprobantes cancelados por corte de energfa
                Case Configuracion.PagoSaldo: FromConfiguracionToString = "PagoSaldo" 'Descripci=n del medio de pago de saldo
                Case Configuracion.SonidoAviso: FromConfiguracionToString = "SonidoAviso" 'Sonido de aviso
                Case Configuracion.ChequeoDesborde: FromConfiguracionToString = "ChequeoDesborde" 'Chequeo de desborde
                Case Configuracion.BorradoAutomaticoAuditoria: FromConfiguracionToString = "BorradoAutomaticoAuditoria" 'Borrado autom�tico de jornadas no usadas de auditorfa
                Case Configuracion.TipoHabilitacion: FromConfiguracionToString = "TipoHabilitacion" 'Tipo de habilitaci=n para comprobantes tipo A
                Case Configuracion.Interlineado: FromConfiguracionToString = "Interlineado" 'Tama�o de interlineado
                Case Configuracion.ImpresionMarco: FromConfiguracionToString = "ImpresionMarco" 'Impresion de marco
                Case Configuracion.AltoHoja: FromConfiguracionToString = "AltoHoja" 'Alto de hoja
                Case Configuracion.AnchoHoja: FromConfiguracionToString = "AnchoHoja" 'Ancho de hoja
                Case Configuracion.EstacionReportesXZ: FromConfiguracionToString = "EstacionReportesXZ" 'Estaci=n de reportes X/Z
                Case Configuracion.ModoImpresion: FromConfiguracionToString = "ModoImpresion" 'Modo de impresi=n para equipos con m�s de una estaci=n
                Case Configuracion.ImpresionUsarColorAlternativo: FromConfiguracionToString = "ImpresionUsarColorAlternativo" 'Usar el color alternativo de la cinta de impresion
                Case Configuracion.ImpresionColorAlternativo: FromConfiguracionToString = "ImpresionColorAlternativo" 'De quT color es el color alternativo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "Configuracion", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToConfiguracion(ByVal Value As String) As Configuracion
        Select Case Value
                Case "LimiteBC": FromStringToConfiguracion = Configuracion.LimiteBC 'Monto m�ximo comprobantes 'B/C'.
                Case "ImpresionCambio": FromStringToConfiguracion = Configuracion.ImpresionCambio 'Impresi=n "Cambio $0.00"
                Case "ImpresionLeyendas": FromStringToConfiguracion = Configuracion.ImpresionLeyendas 'Impresi=n de leyendas opcionales
                Case "CortePapel": FromStringToConfiguracion = Configuracion.CortePapel 'Opciones de corte de papel
                Case "ReimpresionCancelados": FromStringToConfiguracion = Configuracion.ReimpresionCancelados 'Reimpresi=n de comprobantes cancelados por corte de energfa
                Case "PagoSaldo": FromStringToConfiguracion = Configuracion.PagoSaldo 'Descripci=n del medio de pago de saldo
                Case "SonidoAviso": FromStringToConfiguracion = Configuracion.SonidoAviso 'Sonido de aviso
                Case "ChequeoDesborde": FromStringToConfiguracion = Configuracion.ChequeoDesborde 'Chequeo de desborde
                Case "BorradoAutomaticoAuditoria": FromStringToConfiguracion = Configuracion.BorradoAutomaticoAuditoria 'Borrado autom�tico de jornadas no usadas de auditorfa
                Case "TipoHabilitacion": FromStringToConfiguracion = Configuracion.TipoHabilitacion 'Tipo de habilitaci=n para comprobantes tipo A
                Case "Interlineado": FromStringToConfiguracion = Configuracion.Interlineado 'Tama�o de interlineado
                Case "ImpresionMarco": FromStringToConfiguracion = Configuracion.ImpresionMarco 'Impresion de marco
                Case "AltoHoja": FromStringToConfiguracion = Configuracion.AltoHoja 'Alto de hoja
                Case "AnchoHoja": FromStringToConfiguracion = Configuracion.AnchoHoja 'Ancho de hoja
                Case "EstacionReportesXZ": FromStringToConfiguracion = Configuracion.EstacionReportesXZ 'Estaci=n de reportes X/Z
                Case "ModoImpresion": FromStringToConfiguracion = Configuracion.ModoImpresion 'Modo de impresi=n para equipos con m�s de una estaci=n
                Case "ImpresionUsarColorAlternativo": FromStringToConfiguracion = Configuracion.ImpresionUsarColorAlternativo 'Usar el color alternativo de la cinta de impresion
                Case "ImpresionColorAlternativo": FromStringToConfiguracion = Configuracion.ImpresionColorAlternativo 'De quT color es el color alternativo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "Configuracion", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromEstadosFiscalesToString(ByVal Value As EstadosFiscales) As String
        Select Case Value
                Case EstadosFiscales.Desconocido: FromEstadosFiscalesToString = "Desconocido" 'Estado desconocido
                Case EstadosFiscales.NoInicializado: FromEstadosFiscalesToString = "NoInicializado" 'CF No Incializado
                Case EstadosFiscales.InicioJornadaFiscal: FromEstadosFiscalesToString = "InicioJornadaFiscal" 'Inicio Jornada Fiscal
                Case EstadosFiscales.EnJornadaFiscal: FromEstadosFiscalesToString = "EnJornadaFiscal" 'En Jornada Fiscal
                Case EstadosFiscales.VendiendoItems: FromEstadosFiscalesToString = "VendiendoItems" 'Vendiendo items
                Case EstadosFiscales.ImprimiendoTextoFiscal: FromEstadosFiscalesToString = "ImprimiendoTextoFiscal" 'Imprimiendo texto fiscal
                Case EstadosFiscales.Pagando: FromEstadosFiscalesToString = "Pagando" 'Pagando
                Case EstadosFiscales.IngresandoOtrosTributos: FromEstadosFiscalesToString = "IngresandoOtrosTributos" 'Ingresando otros tributos
                Case EstadosFiscales.RealizandoOperacionAjuste: FromEstadosFiscalesToString = "RealizandoOperacionAjuste" 'Realizando operaci=n ajuste
                Case EstadosFiscales.RealizandoOperacionGlobalIVA: FromEstadosFiscalesToString = "RealizandoOperacionGlobalIVA" 'Realizando operaci=n global IVA
                Case EstadosFiscales.RealizandoOperacionAnticipo: FromEstadosFiscalesToString = "RealizandoOperacionAnticipo" 'Realizando operaci=n anticipo
                Case EstadosFiscales.ImprimiendoLineasRecibo: FromEstadosFiscalesToString = "ImprimiendoLineasRecibo" 'Imprimiendo lineas de recibo
                Case EstadosFiscales.ImprimiendoTextoNoFiscal: FromEstadosFiscalesToString = "ImprimiendoTextoNoFiscal" 'Imprimendo texto no fiscal
                Case EstadosFiscales.CintaAuditoriaCasiLlena: FromEstadosFiscalesToString = "CintaAuditoriaCasiLlena" 'Cinta de auditorfa completa, esperando �ltima zeta
                Case EstadosFiscales.CintaAuditoriaLlena: FromEstadosFiscalesToString = "CintaAuditoriaLlena" 'Cinta de auditorfa completa
                Case EstadosFiscales.ControladorFiscalEsperandoBaja: FromEstadosFiscalesToString = "ControladorFiscalEsperandoBaja" 'Controlador fiscal esperando baja
                Case EstadosFiscales.ControladorFiscalDadoDeBaja: FromEstadosFiscalesToString = "ControladorFiscalDadoDeBaja" 'Controlador fiscal dado de baja
                Case EstadosFiscales.ControladorFiscalBloqueado: FromEstadosFiscalesToString = "ControladorFiscalBloqueado" 'Controlador fiscal bloqueado
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "EstadosFiscales", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToEstadosFiscales(ByVal Value As String) As EstadosFiscales
        Select Case Value
                Case "Desconocido": FromStringToEstadosFiscales = EstadosFiscales.Desconocido 'Estado desconocido
                Case "NoInicializado": FromStringToEstadosFiscales = EstadosFiscales.NoInicializado 'CF No Incializado
                Case "InicioJornadaFiscal": FromStringToEstadosFiscales = EstadosFiscales.InicioJornadaFiscal 'Inicio Jornada Fiscal
                Case "EnJornadaFiscal": FromStringToEstadosFiscales = EstadosFiscales.EnJornadaFiscal 'En Jornada Fiscal
                Case "VendiendoItems": FromStringToEstadosFiscales = EstadosFiscales.VendiendoItems 'Vendiendo items
                Case "ImprimiendoTextoFiscal": FromStringToEstadosFiscales = EstadosFiscales.ImprimiendoTextoFiscal 'Imprimiendo texto fiscal
                Case "Pagando": FromStringToEstadosFiscales = EstadosFiscales.Pagando 'Pagando
                Case "IngresandoOtrosTributos": FromStringToEstadosFiscales = EstadosFiscales.IngresandoOtrosTributos 'Ingresando otros tributos
                Case "RealizandoOperacionAjuste": FromStringToEstadosFiscales = EstadosFiscales.RealizandoOperacionAjuste 'Realizando operaci=n ajuste
                Case "RealizandoOperacionGlobalIVA": FromStringToEstadosFiscales = EstadosFiscales.RealizandoOperacionGlobalIVA 'Realizando operaci=n global IVA
                Case "RealizandoOperacionAnticipo": FromStringToEstadosFiscales = EstadosFiscales.RealizandoOperacionAnticipo 'Realizando operaci=n anticipo
                Case "ImprimiendoLineasRecibo": FromStringToEstadosFiscales = EstadosFiscales.ImprimiendoLineasRecibo 'Imprimiendo lineas de recibo
                Case "ImprimiendoTextoNoFiscal": FromStringToEstadosFiscales = EstadosFiscales.ImprimiendoTextoNoFiscal 'Imprimendo texto no fiscal
                Case "CintaAuditoriaCasiLlena": FromStringToEstadosFiscales = EstadosFiscales.CintaAuditoriaCasiLlena 'Cinta de auditorfa completa, esperando �ltima zeta
                Case "CintaAuditoriaLlena": FromStringToEstadosFiscales = EstadosFiscales.CintaAuditoriaLlena 'Cinta de auditorfa completa
                Case "ControladorFiscalEsperandoBaja": FromStringToEstadosFiscales = EstadosFiscales.ControladorFiscalEsperandoBaja 'Controlador fiscal esperando baja
                Case "ControladorFiscalDadoDeBaja": FromStringToEstadosFiscales = EstadosFiscales.ControladorFiscalDadoDeBaja 'Controlador fiscal dado de baja
                Case "ControladorFiscalBloqueado": FromStringToEstadosFiscales = EstadosFiscales.ControladorFiscalBloqueado 'Controlador fiscal bloqueado
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "EstadosFiscales", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromIdentificadorBloqueToString(ByVal Value As IdentificadorBloque) As String
        Select Case Value
                Case IdentificadorBloque.BloqueInformacion: FromIdentificadorBloqueToString = "BloqueInformacion" 'Bloque de informaci=n
                Case IdentificadorBloque.BloqueFinal: FromIdentificadorBloqueToString = "BloqueFinal" 'Bloque final de informaci=n
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "IdentificadorBloque", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToIdentificadorBloque(ByVal Value As String) As IdentificadorBloque
        Select Case Value
                Case "BloqueInformacion": FromStringToIdentificadorBloque = IdentificadorBloque.BloqueInformacion 'Bloque de informaci=n
                Case "BloqueFinal": FromStringToIdentificadorBloque = IdentificadorBloque.BloqueFinal 'Bloque final de informaci=n
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "IdentificadorBloque", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromImpresionCodigoDeBarrasToString(ByVal Value As ImpresionCodigoDeBarras) As String
        Select Case Value
                Case ImpresionCodigoDeBarras.ImprimeCodigo: FromImpresionCodigoDeBarrasToString = "ImprimeCodigo" 'Imprime el c=digo en el momento
                Case ImpresionCodigoDeBarras.ProgramaCodigo: FromImpresionCodigoDeBarrasToString = "ProgramaCodigo" 'Programa el c=digo para imprimirlo en el cierre
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ImpresionCodigoDeBarras", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToImpresionCodigoDeBarras(ByVal Value As String) As ImpresionCodigoDeBarras
        Select Case Value
                Case "ImprimeCodigo": FromStringToImpresionCodigoDeBarras = ImpresionCodigoDeBarras.ImprimeCodigo 'Imprime el c=digo en el momento
                Case "ProgramaCodigo": FromStringToImpresionCodigoDeBarras = ImpresionCodigoDeBarras.ProgramaCodigo 'Programa el c=digo para imprimirlo en el cierre
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ImpresionCodigoDeBarras", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromImpresionNumeroCodigoDeBarrasToString(ByVal Value As ImpresionNumeroCodigoDeBarras) As String
        Select Case Value
                Case ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo: FromImpresionNumeroCodigoDeBarrasToString = "ImprimeNumerosCodigo" 'Imprime los n�meros debajo del c=digo
                Case ImpresionNumeroCodigoDeBarras.NoImprimeNumerosCodigo: FromImpresionNumeroCodigoDeBarrasToString = "NoImprimeNumerosCodigo" 'No imprime los n�meros debajo del c=digo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ImpresionNumeroCodigoDeBarras", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToImpresionNumeroCodigoDeBarras(ByVal Value As String) As ImpresionNumeroCodigoDeBarras
        Select Case Value
                Case "ImprimeNumerosCodigo": FromStringToImpresionNumeroCodigoDeBarras = ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo 'Imprime los n�meros debajo del c=digo
                Case "NoImprimeNumerosCodigo": FromStringToImpresionNumeroCodigoDeBarras = ImpresionNumeroCodigoDeBarras.NoImprimeNumerosCodigo 'No imprime los n�meros debajo del c=digo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ImpresionNumeroCodigoDeBarras", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromImpresionSubtotalToString(ByVal Value As ImpresionSubtotal) As String
        Select Case Value
                Case ImpresionSubtotal.ImprimeSubtotal: FromImpresionSubtotalToString = "ImprimeSubtotal" 'Imprime Subtotal
                Case ImpresionSubtotal.NoImprimeSubtotal: FromImpresionSubtotalToString = "NoImprimeSubtotal" 'No imprime Subtotal
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ImpresionSubtotal", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToImpresionSubtotal(ByVal Value As String) As ImpresionSubtotal
        Select Case Value
                Case "ImprimeSubtotal": FromStringToImpresionSubtotal = ImpresionSubtotal.ImprimeSubtotal 'Imprime Subtotal
                Case "NoImprimeSubtotal": FromStringToImpresionSubtotal = ImpresionSubtotal.NoImprimeSubtotal 'No imprime Subtotal
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ImpresionSubtotal", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromModosDeDisplayToString(ByVal Value As ModosDeDisplay) As String
        Select Case Value
                Case ModosDeDisplay.DisplayNo: FromModosDeDisplayToString = "DisplayNo" 'No escribe en Display
                Case ModosDeDisplay.DisplaySi: FromModosDeDisplayToString = "DisplaySi" 'Escribe en Display
                Case ModosDeDisplay.DisplayRep: FromModosDeDisplayToString = "DisplayRep" 'Incrementa contador de repetici=n
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ModosDeDisplay", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToModosDeDisplay(ByVal Value As String) As ModosDeDisplay
        Select Case Value
                Case "DisplayNo": FromStringToModosDeDisplay = ModosDeDisplay.DisplayNo 'No escribe en Display
                Case "DisplaySi": FromStringToModosDeDisplay = ModosDeDisplay.DisplaySi 'Escribe en Display
                Case "DisplayRep": FromStringToModosDeDisplay = ModosDeDisplay.DisplayRep 'Incrementa contador de repetici=n
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ModosDeDisplay", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromModosDeImpuestosInternosToString(ByVal Value As ModosDeImpuestosInternos) As String
        Select Case Value
                Case ModosDeImpuestosInternos.IIVariableKIVA: FromModosDeImpuestosInternosToString = "IIVariableKIVA" 'II Variable por Coeficiente
                Case ModosDeImpuestosInternos.IIVariablePorcentual: FromModosDeImpuestosInternosToString = "IIVariablePorcentual" 'II Variable por Porcentaje
                Case ModosDeImpuestosInternos.IIFijoKIVA: FromModosDeImpuestosInternosToString = "IIFijoKIVA" 'II Fijo por Coeficiente
                Case ModosDeImpuestosInternos.IIFijoMonto: FromModosDeImpuestosInternosToString = "IIFijoMonto" 'II Fijo por Monto
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ModosDeImpuestosInternos", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToModosDeImpuestosInternos(ByVal Value As String) As ModosDeImpuestosInternos
        Select Case Value
                Case "IIVariableKIVA": FromStringToModosDeImpuestosInternos = ModosDeImpuestosInternos.IIVariableKIVA 'II Variable por Coeficiente
                Case "IIVariablePorcentual": FromStringToModosDeImpuestosInternos = ModosDeImpuestosInternos.IIVariablePorcentual 'II Variable por Porcentaje
                Case "IIFijoKIVA": FromStringToModosDeImpuestosInternos = ModosDeImpuestosInternos.IIFijoKIVA 'II Fijo por Coeficiente
                Case "IIFijoMonto": FromStringToModosDeImpuestosInternos = ModosDeImpuestosInternos.IIFijoMonto 'II Fijo por Monto
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ModosDeImpuestosInternos", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromModosDeMontoToString(ByVal Value As ModosDeMonto) As String
        Select Case Value
                Case ModosDeMonto.ModoSumaMonto: FromModosDeMontoToString = "ModoSumaMonto" 'Suma monto
                Case ModosDeMonto.ModoRestaMonto: FromModosDeMontoToString = "ModoRestaMonto" 'Resta monto
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ModosDeMonto", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToModosDeMonto(ByVal Value As String) As ModosDeMonto
        Select Case Value
                Case "ModoSumaMonto": FromStringToModosDeMonto = ModosDeMonto.ModoSumaMonto 'Suma monto
                Case "ModoRestaMonto": FromStringToModosDeMonto = ModosDeMonto.ModoRestaMonto 'Resta monto
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ModosDeMonto", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromModosDePagoToString(ByVal Value As ModosDePago) As String
        Select Case Value
                Case ModosDePago.Pagar: FromModosDePagoToString = "Pagar" 'Pago
                Case ModosDePago.Anular: FromModosDePagoToString = "Anular" 'Anulaci=n de pago previo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ModosDePago", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToModosDePago(ByVal Value As String) As ModosDePago
        Select Case Value
                Case "Pagar": FromStringToModosDePago = ModosDePago.Pagar 'Pago
                Case "Anular": FromStringToModosDePago = ModosDePago.Anular 'Anulaci=n de pago previo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ModosDePago", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromModosDePrecioToString(ByVal Value As ModosDePrecio) As String
        Select Case Value
                Case ModosDePrecio.ModoPrecioBase: FromModosDePrecioToString = "ModoPrecioBase" 'Precio Base especificado
                Case ModosDePrecio.ModoPrecioTotal: FromModosDePrecioToString = "ModoPrecioTotal" 'Precio Total especificado
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ModosDePrecio", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToModosDePrecio(ByVal Value As String) As ModosDePrecio
        Select Case Value
                Case "ModoPrecioBase": FromStringToModosDePrecio = ModosDePrecio.ModoPrecioBase 'Precio Base especificado
                Case "ModoPrecioTotal": FromStringToModosDePrecio = ModosDePrecio.ModoPrecioTotal 'Precio Total especificado
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ModosDePrecio", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromOperacionesCargaLogoUsuarioToString(ByVal Value As OperacionesCargaLogoUsuario) As String
        Select Case Value
                Case OperacionesCargaLogoUsuario.ComienzoCargaLogo: FromOperacionesCargaLogoUsuarioToString = "ComienzoCargaLogo" 'Inicio de carga de logotipo
                Case OperacionesCargaLogoUsuario.CargaLogoEnCurso: FromOperacionesCargaLogoUsuarioToString = "CargaLogoEnCurso" 'Carga en transcurso
                Case OperacionesCargaLogoUsuario.FinCargaLogo: FromOperacionesCargaLogoUsuarioToString = "FinCargaLogo" 'Ultimo paquete del logotipo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "OperacionesCargaLogoUsuario", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToOperacionesCargaLogoUsuario(ByVal Value As String) As OperacionesCargaLogoUsuario
        Select Case Value
                Case "ComienzoCargaLogo": FromStringToOperacionesCargaLogoUsuario = OperacionesCargaLogoUsuario.ComienzoCargaLogo 'Inicio de carga de logotipo
                Case "CargaLogoEnCurso": FromStringToOperacionesCargaLogoUsuario = OperacionesCargaLogoUsuario.CargaLogoEnCurso 'Carga en transcurso
                Case "FinCargaLogo": FromStringToOperacionesCargaLogoUsuario = OperacionesCargaLogoUsuario.FinCargaLogo 'Ultimo paquete del logotipo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "OperacionesCargaLogoUsuario", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromSiNoToString(ByVal Value As SiNo) As String
        Select Case Value
                Case SiNo.Si: FromSiNoToString = "Si" 'afirmativo
                Case SiNo.No: FromSiNoToString = "No" 'negativo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "SiNo", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToSiNo(ByVal Value As String) As SiNo
        Select Case Value
                Case "Si": FromStringToSiNo = SiNo.Si 'afirmativo
                Case "No": FromStringToSiNo = SiNo.No 'negativo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "SiNo", "Enumerador " & Value & " desconocido"
        End Select
End Function

'//====================================================================================================================
Public Function FromTipoReporteToString(ByVal Value As TipoReporte) As String
        Select Case Value
                Case TipoReporte.ReporteX: FromTipoReporteToString = "ReporteX" 'Reporte X
                Case TipoReporte.ReporteZ: FromTipoReporteToString = "ReporteZ" 'Reporte Z
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TipoReporte", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTipoReporte(ByVal Value As String) As TipoReporte
        Select Case Value
                Case "ReporteX": FromStringToTipoReporte = TipoReporte.ReporteX 'Reporte X
                Case "ReporteZ": FromStringToTipoReporte = TipoReporte.ReporteZ 'Reporte Z
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TipoReporte", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTipoReporteAuditoriaToString(ByVal Value As TipoReporteAuditoria) As String
        Select Case Value
                Case TipoReporteAuditoria.ReporteAuditoriaGlobal: FromTipoReporteAuditoriaToString = "ReporteAuditoriaGlobal" 'Reporte de auditorfa global
                Case TipoReporteAuditoria.ReporteAuditoriaDiscriminado: FromTipoReporteAuditoriaToString = "ReporteAuditoriaDiscriminado" 'Reporte de auditorfa discriminado por cierre Z
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TipoReporteAuditoria", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTipoReporteAuditoria(ByVal Value As String) As TipoReporteAuditoria
        Select Case Value
                Case "ReporteAuditoriaGlobal": FromStringToTipoReporteAuditoria = TipoReporteAuditoria.ReporteAuditoriaGlobal 'Reporte de auditorfa global
                Case "ReporteAuditoriaDiscriminado": FromStringToTipoReporteAuditoria = TipoReporteAuditoria.ReporteAuditoriaDiscriminado 'Reporte de auditorfa discriminado por cierre Z
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TipoReporteAuditoria", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTipoReporteZToString(ByVal Value As TipoReporteZ) As String
        Select Case Value
                Case TipoReporteZ.ReporteZFecha: FromTipoReporteZToString = "ReporteZFecha" 'Reporte Z por fecha
                Case TipoReporteZ.ReporteZNumero: FromTipoReporteZToString = "ReporteZNumero" 'Reporte Z por zeta
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TipoReporteZ", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTipoReporteZ(ByVal Value As String) As TipoReporteZ
        Select Case Value
                Case "ReporteZFecha": FromStringToTipoReporteZ = TipoReporteZ.ReporteZFecha 'Reporte Z por fecha
                Case "ReporteZNumero": FromStringToTipoReporteZ = TipoReporteZ.ReporteZNumero 'Reporte Z por zeta
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TipoReporteZ", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposDeAjustesToString(ByVal Value As TiposDeAjustes) As String
        Select Case Value
                Case TiposDeAjustes.AjusteNeg: FromTiposDeAjustesToString = "AjusteNeg" 'Operaci=n Ajuste(-)
                Case TiposDeAjustes.AjustePos: FromTiposDeAjustesToString = "AjustePos" 'Operaci=n Ajuste(+)
                Case TiposDeAjustes.BonificacionGeneral: FromTiposDeAjustesToString = "BonificacionGeneral" 'Operaci=n Bonificaci=n General
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeAjustes", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposDeAjustes(ByVal Value As String) As TiposDeAjustes
        Select Case Value
                Case "AjusteNeg": FromStringToTiposDeAjustes = TiposDeAjustes.AjusteNeg 'Operaci=n Ajuste(-)
                Case "AjustePos": FromStringToTiposDeAjustes = TiposDeAjustes.AjustePos 'Operaci=n Ajuste(+)
                Case "BonificacionGeneral": FromStringToTiposDeAjustes = TiposDeAjustes.BonificacionGeneral 'Operaci=n Bonificaci=n General
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeAjustes", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposDeCodigoDeBarrasToString(ByVal Value As TiposDeCodigoDeBarras) As String
        Select Case Value
                Case TiposDeCodigoDeBarras.CodigoDeBorrado: FromTiposDeCodigoDeBarrasToString = "CodigoDeBorrado" 'Borra el c=digo almacenado
                Case TiposDeCodigoDeBarras.CodigoTipoEAN13: FromTiposDeCodigoDeBarrasToString = "CodigoTipoEAN13" 'C=digo EAN13
                Case TiposDeCodigoDeBarras.CodigoTipoEAN8: FromTiposDeCodigoDeBarrasToString = "CodigoTipoEAN8" 'C=digo EAN8
                Case TiposDeCodigoDeBarras.CodigoTipoUPCA: FromTiposDeCodigoDeBarrasToString = "CodigoTipoUPCA" 'C=digo UPCA
                Case TiposDeCodigoDeBarras.CodigoTipoI2OF5: FromTiposDeCodigoDeBarrasToString = "CodigoTipoI2OF5" 'C=digo INTERLEAVE 2 de 5
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeCodigoDeBarras", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposDeCodigoDeBarras(ByVal Value As String) As TiposDeCodigoDeBarras
        Select Case Value
                Case "CodigoDeBorrado": FromStringToTiposDeCodigoDeBarras = TiposDeCodigoDeBarras.CodigoDeBorrado 'Borra el c=digo almacenado
                Case "CodigoTipoEAN13": FromStringToTiposDeCodigoDeBarras = TiposDeCodigoDeBarras.CodigoTipoEAN13 'C=digo EAN13
                Case "CodigoTipoEAN8": FromStringToTiposDeCodigoDeBarras = TiposDeCodigoDeBarras.CodigoTipoEAN8 'C=digo EAN8
                Case "CodigoTipoUPCA": FromStringToTiposDeCodigoDeBarras = TiposDeCodigoDeBarras.CodigoTipoUPCA 'C=digo UPCA
                Case "CodigoTipoI2OF5": FromStringToTiposDeCodigoDeBarras = TiposDeCodigoDeBarras.CodigoTipoI2OF5 'C=digo INTERLEAVE 2 de 5
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeCodigoDeBarras", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposDeDocumentoClienteToString(ByVal Value As TiposDeDocumentoCliente) As String
        Select Case Value
                Case TiposDeDocumentoCliente.TipoCUIT: FromTiposDeDocumentoClienteToString = "TipoCUIT" 'C.U.I.T.
                Case TiposDeDocumentoCliente.TipoCUIL: FromTiposDeDocumentoClienteToString = "TipoCUIL" 'C.U.I.L.
                Case TiposDeDocumentoCliente.TipoLE: FromTiposDeDocumentoClienteToString = "TipoLE" 'Libreta de Enrolamiento
                Case TiposDeDocumentoCliente.TipoLC: FromTiposDeDocumentoClienteToString = "TipoLC" 'Libreta Cfvica
                Case TiposDeDocumentoCliente.TipoDNI: FromTiposDeDocumentoClienteToString = "TipoDNI" 'Documanto Nacional de Identidad
                Case TiposDeDocumentoCliente.TipoPasaporte: FromTiposDeDocumentoClienteToString = "TipoPasaporte" 'Pasaporte
                Case TiposDeDocumentoCliente.TipoCI: FromTiposDeDocumentoClienteToString = "TipoCI" 'CTdula de Identidad
                Case TiposDeDocumentoCliente.TipoNinguno: FromTiposDeDocumentoClienteToString = "TipoNinguno" 'Sin identificar
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeDocumentoCliente", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposDeDocumentoCliente(ByVal Value As String) As TiposDeDocumentoCliente
        Select Case Value
                Case "TipoCUIT": FromStringToTiposDeDocumentoCliente = TiposDeDocumentoCliente.TipoCUIT 'C.U.I.T.
                Case "TipoCUIL": FromStringToTiposDeDocumentoCliente = TiposDeDocumentoCliente.TipoCUIL 'C.U.I.L.
                Case "TipoLE": FromStringToTiposDeDocumentoCliente = TiposDeDocumentoCliente.TipoLE 'Libreta de Enrolamiento
                Case "TipoLC": FromStringToTiposDeDocumentoCliente = TiposDeDocumentoCliente.TipoLC 'Libreta Cfvica
                Case "TipoDNI": FromStringToTiposDeDocumentoCliente = TiposDeDocumentoCliente.TipoDNI 'Documanto Nacional de Identidad
                Case "TipoPasaporte": FromStringToTiposDeDocumentoCliente = TiposDeDocumentoCliente.TipoPasaporte 'Pasaporte
                Case "TipoCI": FromStringToTiposDeDocumentoCliente = TiposDeDocumentoCliente.TipoCI 'CTdula de Identidad
                Case "TipoNinguno": FromStringToTiposDeDocumentoCliente = TiposDeDocumentoCliente.TipoNinguno 'Sin identificar
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeDocumentoCliente", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposDeEstacionToString(ByVal Value As TiposDeEstacion) As String
        Select Case Value
                Case TiposDeEstacion.EstacionTicket: FromTiposDeEstacionToString = "EstacionTicket" 'Estaci=n Ticket
                Case TiposDeEstacion.EstacionSlip: FromTiposDeEstacionToString = "EstacionSlip" 'Estaci=n Slip / Tractor
                Case TiposDeEstacion.EstacionPorDefecto: FromTiposDeEstacionToString = "EstacionPorDefecto" 'Estaci=n principal del modelo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeEstacion", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposDeEstacion(ByVal Value As String) As TiposDeEstacion
        Select Case Value
                Case "EstacionTicket": FromStringToTiposDeEstacion = TiposDeEstacion.EstacionTicket 'Estaci=n Ticket
                Case "EstacionSlip": FromStringToTiposDeEstacion = TiposDeEstacion.EstacionSlip 'Estaci=n Slip / Tractor
                Case "EstacionPorDefecto": FromStringToTiposDeEstacion = TiposDeEstacion.EstacionPorDefecto 'Estaci=n principal del modelo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeEstacion", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposDeOperacionesGlobalesIVAToString(ByVal Value As TiposDeOperacionesGlobalesIVA) As String
        Select Case Value
                Case TiposDeOperacionesGlobalesIVA.BonificacionIVA: FromTiposDeOperacionesGlobalesIVAToString = "BonificacionIVA" 'Operaci=n Bonificaci=n IVA
                Case TiposDeOperacionesGlobalesIVA.RecargoIVA: FromTiposDeOperacionesGlobalesIVAToString = "RecargoIVA" 'Operaci=n Recargo IVA
                Case TiposDeOperacionesGlobalesIVA.DevolucionEnvases: FromTiposDeOperacionesGlobalesIVAToString = "DevolucionEnvases" 'Operaci=n Devoluci=n Envases
                Case TiposDeOperacionesGlobalesIVA.CobroAnticipo: FromTiposDeOperacionesGlobalesIVAToString = "CobroAnticipo" 'Operaci=n Cobro Anticipo
                Case TiposDeOperacionesGlobalesIVA.DescuentoAnticipo: FromTiposDeOperacionesGlobalesIVAToString = "DescuentoAnticipo" 'Operaci=n Descuento Anticipo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeOperacionesGlobalesIVA", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposDeOperacionesGlobalesIVA(ByVal Value As String) As TiposDeOperacionesGlobalesIVA
        Select Case Value
                Case "BonificacionIVA": FromStringToTiposDeOperacionesGlobalesIVA = TiposDeOperacionesGlobalesIVA.BonificacionIVA 'Operaci=n Bonificaci=n IVA
                Case "RecargoIVA": FromStringToTiposDeOperacionesGlobalesIVA = TiposDeOperacionesGlobalesIVA.RecargoIVA 'Operaci=n Recargo IVA
                Case "DevolucionEnvases": FromStringToTiposDeOperacionesGlobalesIVA = TiposDeOperacionesGlobalesIVA.DevolucionEnvases 'Operaci=n Devoluci=n Envases
                Case "CobroAnticipo": FromStringToTiposDeOperacionesGlobalesIVA = TiposDeOperacionesGlobalesIVA.CobroAnticipo 'Operaci=n Cobro Anticipo
                Case "DescuentoAnticipo": FromStringToTiposDeOperacionesGlobalesIVA = TiposDeOperacionesGlobalesIVA.DescuentoAnticipo 'Operaci=n Descuento Anticipo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeOperacionesGlobalesIVA", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposDeRegistroInformeToString(ByVal Value As TiposDeRegistroInforme) As String
        Select Case Value
                Case TiposDeRegistroInforme.RegistroFinal: FromTiposDeRegistroInformeToString = "RegistroFinal" 'Registro final
                Case TiposDeRegistroInforme.RegistroDetalladoDF: FromTiposDeRegistroInformeToString = "RegistroDetalladoDF" 'Registro detallado de DF
                Case TiposDeRegistroInforme.RegistroDetalladoDNFH: FromTiposDeRegistroInformeToString = "RegistroDetalladoDNFH" 'Registro detallado de DNFH
                Case TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum: FromTiposDeRegistroInformeToString = "RegistroDetalladoDNFHNoAcum" 'Registro detallado de DNFH que no acumulan
                Case TiposDeRegistroInforme.RegistroGlobal: FromTiposDeRegistroInformeToString = "RegistroGlobal" 'Registro global
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeRegistroInforme", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposDeRegistroInforme(ByVal Value As String) As TiposDeRegistroInforme
        Select Case Value
                Case "RegistroFinal": FromStringToTiposDeRegistroInforme = TiposDeRegistroInforme.RegistroFinal 'Registro final
                Case "RegistroDetalladoDF": FromStringToTiposDeRegistroInforme = TiposDeRegistroInforme.RegistroDetalladoDF 'Registro detallado de DF
                Case "RegistroDetalladoDNFH": FromStringToTiposDeRegistroInforme = TiposDeRegistroInforme.RegistroDetalladoDNFH 'Registro detallado de DNFH
                Case "RegistroDetalladoDNFHNoAcum": FromStringToTiposDeRegistroInforme = TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum 'Registro detallado de DNFH que no acumulan
                Case "RegistroGlobal": FromStringToTiposDeRegistroInforme = TiposDeRegistroInforme.RegistroGlobal 'Registro global
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeRegistroInforme", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposDeResponsabilidadesClienteToString(ByVal Value As TiposDeResponsabilidadesCliente) As String
        Select Case Value
                Case TiposDeResponsabilidadesCliente.ResponsableInscripto: FromTiposDeResponsabilidadesClienteToString = "ResponsableInscripto" 'Responsable Inscripto
                Case TiposDeResponsabilidadesCliente.ResponsableExento: FromTiposDeResponsabilidadesClienteToString = "ResponsableExento" 'Responsable Exento
                Case TiposDeResponsabilidadesCliente.NoResponsable: FromTiposDeResponsabilidadesClienteToString = "NoResponsable" 'No Responsable
                Case TiposDeResponsabilidadesCliente.ConsumidorFinal: FromTiposDeResponsabilidadesClienteToString = "ConsumidorFinal" 'Consumidor Final
                Case TiposDeResponsabilidadesCliente.NoCategorizado: FromTiposDeResponsabilidadesClienteToString = "NoCategorizado" 'Sujeto No Categorizado
                Case TiposDeResponsabilidadesCliente.Monotributo: FromTiposDeResponsabilidadesClienteToString = "Monotributo" 'Monotributista
                Case TiposDeResponsabilidadesCliente.MonotributoSocial: FromTiposDeResponsabilidadesClienteToString = "MonotributoSocial" 'Monotributista Social
                Case TiposDeResponsabilidadesCliente.Eventual: FromTiposDeResponsabilidadesClienteToString = "Eventual" 'Peque�o Contribuyente Eventual
                Case TiposDeResponsabilidadesCliente.EventualSocial: FromTiposDeResponsabilidadesClienteToString = "EventualSocial" 'Peque�o Contribuyente Eventual Social
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeResponsabilidadesCliente", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposDeResponsabilidadesCliente(ByVal Value As String) As TiposDeResponsabilidadesCliente
        Select Case Value
                Case "ResponsableInscripto": FromStringToTiposDeResponsabilidadesCliente = TiposDeResponsabilidadesCliente.ResponsableInscripto 'Responsable Inscripto
                Case "ResponsableExento": FromStringToTiposDeResponsabilidadesCliente = TiposDeResponsabilidadesCliente.ResponsableExento 'Responsable Exento
                Case "NoResponsable": FromStringToTiposDeResponsabilidadesCliente = TiposDeResponsabilidadesCliente.NoResponsable 'No Responsable
                Case "ConsumidorFinal": FromStringToTiposDeResponsabilidadesCliente = TiposDeResponsabilidadesCliente.ConsumidorFinal 'Consumidor Final
                Case "NoCategorizado": FromStringToTiposDeResponsabilidadesCliente = TiposDeResponsabilidadesCliente.NoCategorizado 'Sujeto No Categorizado
                Case "Monotributo": FromStringToTiposDeResponsabilidadesCliente = TiposDeResponsabilidadesCliente.Monotributo 'Monotributista
                Case "MonotributoSocial": FromStringToTiposDeResponsabilidadesCliente = TiposDeResponsabilidadesCliente.MonotributoSocial 'Monotributista Social
                Case "Eventual": FromStringToTiposDeResponsabilidadesCliente = TiposDeResponsabilidadesCliente.Eventual 'Peque�o Contribuyente Eventual
                Case "EventualSocial": FromStringToTiposDeResponsabilidadesCliente = TiposDeResponsabilidadesCliente.EventualSocial 'Peque�o Contribuyente Eventual Social
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeResponsabilidadesCliente", "Enumerador " & Value & " desconocido"
        End Select
End Function

'//====================================================================================================================
'// Dado el valor del enum de una categor�a de IVA del emisor, retorna el identificador asociado (nombre simb�lico).
'//====================================================================================================================
Public Function FromTiposDeResponsabilidadesImpresorToString(ByVal Value As TiposDeResponsabilidadesImpresor) As String
        Select Case Value
                Case TiposDeResponsabilidadesImpresor.IFResponsableInscripto: FromTiposDeResponsabilidadesImpresorToString = "IFResponsableInscripto" 'Responsable Inscripto
                Case TiposDeResponsabilidadesImpresor.IFResponsableExento: FromTiposDeResponsabilidadesImpresorToString = "IFResponsableExento" 'Responsable Exento
                Case TiposDeResponsabilidadesImpresor.IFNoResponsable: FromTiposDeResponsabilidadesImpresorToString = "IFNoResponsable" 'No Responsable
                Case TiposDeResponsabilidadesImpresor.IFMonotributo: FromTiposDeResponsabilidadesImpresorToString = "IFMonotributo" 'Monotributista
                Case TiposDeResponsabilidadesImpresor.IFMonotributoSocial: FromTiposDeResponsabilidadesImpresorToString = "IFMonotributoSocial" 'Monotributista Social
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeResponsabilidadesImpresor", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposDeResponsabilidadesImpresor(ByVal Value As String) As TiposDeResponsabilidadesImpresor
        Select Case Value
                Case "IFResponsableInscripto": FromStringToTiposDeResponsabilidadesImpresor = TiposDeResponsabilidadesImpresor.IFResponsableInscripto 'Responsable Inscripto
                Case "IFResponsableExento": FromStringToTiposDeResponsabilidadesImpresor = TiposDeResponsabilidadesImpresor.IFResponsableExento 'Responsable Exento
                Case "IFNoResponsable": FromStringToTiposDeResponsabilidadesImpresor = TiposDeResponsabilidadesImpresor.IFNoResponsable 'No Responsable
                Case "IFMonotributo": FromStringToTiposDeResponsabilidadesImpresor = TiposDeResponsabilidadesImpresor.IFMonotributo 'Monotributista
                Case "IFMonotributoSocial": FromStringToTiposDeResponsabilidadesImpresor = TiposDeResponsabilidadesImpresor.IFMonotributoSocial 'Monotributista Social
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposDeResponsabilidadesImpresor", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromTiposInterlineadoToString(ByVal Value As TiposInterlineado) As String
        Select Case Value
                Case TiposInterlineado.InterlineadoNoDefinido: FromTiposInterlineadoToString = "InterlineadoNoDefinido"
                Case TiposInterlineado.InterlineadoMinimo: FromTiposInterlineadoToString = "InterlineadoMinimo"
                Case TiposInterlineado.InterlineadoNormal: FromTiposInterlineadoToString = "InterlineadoNormal"
                Case TiposInterlineado.InterlineadoMaximo: FromTiposInterlineadoToString = "InterlineadoMaximo"
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposInterlineado", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToTiposInterlineado(ByVal Value As String) As TiposInterlineado
        Select Case Value
                Case "InterlineadoNoDefinido": FromStringToTiposInterlineado = TiposInterlineado.InterlineadoNoDefinido
                Case "InterlineadoMinimo": FromStringToTiposInterlineado = TiposInterlineado.InterlineadoMinimo
                Case "InterlineadoNormal": FromStringToTiposInterlineado = TiposInterlineado.InterlineadoNormal
                Case "InterlineadoMaximo": FromStringToTiposInterlineado = TiposInterlineado.InterlineadoMaximo
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "TiposInterlineado", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromXMLsPorBajadaToString(ByVal Value As XMLsPorBajada) As String
        Select Case Value
                Case XMLsPorBajada.XMLUnico: FromXMLsPorBajadaToString = "XMLUnico" 'XML �nico
                Case XMLsPorBajada.VariosXMLs: FromXMLsPorBajadaToString = "VariosXMLs" 'Varios XMLs
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "XMLsPorBajada", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToXMLsPorBajada(ByVal Value As String) As XMLsPorBajada
        Select Case Value
                Case "XMLUnico": FromStringToXMLsPorBajada = XMLsPorBajada.XMLUnico 'XML �nico
                Case "VariosXMLs": FromStringToXMLsPorBajada = XMLsPorBajada.VariosXMLs 'Varios XMLs
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "XMLsPorBajada", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromZonasDeLineasDeUsuarioToString(ByVal Value As ZonasDeLineasDeUsuario) As String
        Select Case Value
                Case ZonasDeLineasDeUsuario.ZonaFantasia: FromZonasDeLineasDeUsuarioToString = "ZonaFantasia" 'Zona de nombre de Fantasfa. Lfneas 1 y 2.
                Case ZonasDeLineasDeUsuario.ZonaDomicilioEmisor: FromZonasDeLineasDeUsuarioToString = "ZonaDomicilioEmisor" 'Zona de domicilio del emisor. Lfneas 1 a 4.
                Case ZonasDeLineasDeUsuario.Zona1Encabezado: FromZonasDeLineasDeUsuarioToString = "Zona1Encabezado" 'Zona 1 de encabezado. Lfneas 1 a 3.
                Case ZonasDeLineasDeUsuario.Zona2Encabezado: FromZonasDeLineasDeUsuarioToString = "Zona2Encabezado" 'Zona 2 de encabezado. Lfneas 1 a 3.
                Case ZonasDeLineasDeUsuario.Zona1Cola: FromZonasDeLineasDeUsuarioToString = "Zona1Cola" 'Zona 1 de cola. Lfneas 1 a 4.
                Case ZonasDeLineasDeUsuario.Zona2Cola: FromZonasDeLineasDeUsuarioToString = "Zona2Cola" 'Zona 1 de cola. Lfneas 1 a 6.
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ZonasDeLineasDeUsuario", "Enumerador " & Value & " desconocido"
        End Select
End Function

Public Function FromStringToZonasDeLineasDeUsuario(ByVal Value As String) As ZonasDeLineasDeUsuario
        Select Case Value
                Case "ZonaFantasia": FromStringToZonasDeLineasDeUsuario = ZonasDeLineasDeUsuario.ZonaFantasia 'Zona de nombre de Fantasfa. Lfneas 1 y 2.
                Case "ZonaDomicilioEmisor": FromStringToZonasDeLineasDeUsuario = ZonasDeLineasDeUsuario.ZonaDomicilioEmisor 'Zona de domicilio del emisor. Lfneas 1 a 4.
                Case "Zona1Encabezado": FromStringToZonasDeLineasDeUsuario = ZonasDeLineasDeUsuario.Zona1Encabezado 'Zona 1 de encabezado. Lfneas 1 a 3.
                Case "Zona2Encabezado": FromStringToZonasDeLineasDeUsuario = ZonasDeLineasDeUsuario.Zona2Encabezado 'Zona 2 de encabezado. Lfneas 1 a 3.
                Case "Zona1Cola": FromStringToZonasDeLineasDeUsuario = ZonasDeLineasDeUsuario.Zona1Cola 'Zona 1 de cola. Lfneas 1 a 4.
                Case "Zona2Cola": FromStringToZonasDeLineasDeUsuario = ZonasDeLineasDeUsuario.Zona2Cola 'Zona 1 de cola. Lfneas 1 a 6.
                Case Else: Err.Raise FISCAL_OBJECT_ERROR, "ZonasDeLineasDeUsuario", "Enumerador " & Value & " desconocido"
        End Select
End Function
