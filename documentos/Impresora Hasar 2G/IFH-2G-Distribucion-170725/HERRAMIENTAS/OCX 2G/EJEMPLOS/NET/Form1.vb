﻿'//###################################################################################################################################################
'// Impresoras Fiscales HASAR (IFH) Segunda Generación (2G)                                   Dto. Software de Base, Compañía HASAR SAIC, Grupo HASAR
'// RG AFIP Nº 3561/13 , Controladores Fiscales Nueva Tecnología                              HasarArgentina.ocx, Protocolo '0', Revisión '86'
'// Argentina, Octubre 2015                                                                   VB.Net, Visual Studio 2010, ejemplo orientativo
'//
'// Ejemplo de diálogo con el EFH (Emulador Fiscal HASAR), usando Ethernet.
'// Modficar la dirección IP como argumento del método Conectar() para dialogar con una IFH 2G física.
'// Para emular conexión serie, indicar IP = LocalHost (127.0.0.1) y tener corriendo como servicio el Proxy Fiscal HASAR.
'//###################################################################################################################################################
Option Explicit On

Public Class FormIFH2G
    Dim hasar As HasarArgentina.ImpresoraFiscalRG3561 = New HasarArgentina.ImpresoraFiscalRG3561
    Private WithEvents evhasar As HasarArgentina.ImpresoraFiscalRG3561

    '============================================================================================================================
    ' BOTÓN: "CONECTAR" - Establecer conexión con Emulador Fiscal HASAR/IFH 2G ...
    '============================================================================================================================
    Private Sub ButtonConectarse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonConectarse.Click
        Try
            Debug.Print("")
            Debug.Print("===================================================================")
            Debug.Print("Ejecutando Demo IFH 2G ... Hola ! ...")
            Debug.Print("Creando/Agregando registro(s) de actividades ... 'OCX2G.log' ...")
            hasar.ArchivoRegistro("C:\GRUPO HASAR\EQUIPOS FISCALES 2G\EJEMPLOS\VBNET2010\IFH2daGeneracion\OCX2G.log")
            Debug.Print("")

            Debug.Print("VERIFICANDO CONEXIÓN CON LA IFH 2G ...")
            'Debug.Print("ETHERNET - Conectando con ... 127.0.0.1 ... (Emulador Fiscal HASAR)")
            'hasar.Conectar("127.0.0.1", )
            hasar.Conectar("10.0.7.69")
            'Debug.Print("ETHERNET - Conectando con ... 192.0.0.135 ... (Impresora Fiscal HASAR 2G)")
            'hasar.Conectar("192.0.0.135", )
            Debug.Print("CONECTADO ! ...")
            Debug.Print("")

            Debug.Print("OCX 2G Protocolo = [" & hasar.ObtenerVersionProtocolo & "]")
            Debug.Print("OCX 2G Revisión  = [" & hasar.ObtenerRevision & "]")
            Debug.Print("===================================================================")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: "TERMINAR" - Finalizar la ejecución del software ...
    '============================================================================================================================
    Private Sub ButtonTerminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTerminar.Click
        Debug.Print("================================")
        Debug.Print("CERRANDO PROGRAMA ... Chau ! ...")
        Debug.Print("================================")
        Debug.Print("")
        Me.Finalize()
        End
    End Sub

    '============================================================================================================================
    ' BOTÓN: "CANCELAR" - Cancelación de un comprobante abierto ...
    '============================================================================================================================
    Private Sub ButtonCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCancel.Click
        Dim stfiscal As HasarArgentina.EstadoFiscal

        Try
            Debug.Print("CANCELANDO COMPROBANTE ABIERTO ...")
            hasar.Cancelar()
            Debug.Print("CANCELADO ...")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            stfiscal = hasar.ObtenerUltimoEstadoFiscal

            If (Not (stfiscal.DocumentoAbierto)) Then
                Debug.Print("Cancelar(): No hay un comprobante abierto para cancelar.")
            End If

            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE - Impresión de un comprobante tipo '83' ...
    '============================================================================================================================
    Private Sub ButtonTique_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTique.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("================================")
            Debug.Print("A imprimir: TIQUE (Cód.: 83) ...")
            Debug.Print("================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.Tique)
            Debug.Print("Tique Nº          =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total       =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado      =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base        =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA         =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.   =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib. =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE FACTURA 'A' - Impresión de un comprobante tipo '81' ...
    '============================================================================================================================
    Private Sub Button26TiqueFactA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button26TiqueFactA.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("============================================")
            Debug.Print("A imprimir: TIQUE FACTURA 'A' (Cód.: 81) ...")
            Debug.Print("============================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueFacturaA)
            Debug.Print("Tique Factura 'A' Nº =[" & respabrir.NumeroComprobante & "]")

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 2...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 3...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase)

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 2...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 3...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 4...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirItem("Producto Dos ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirItem("Producto Tres ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cuatro ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cinco ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Seis ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Siete ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Ocho ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Nueve ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Diez ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Once ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Doce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Trece ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Catorce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Dos ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Tres ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Cuatro ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Cinco ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Seis ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Siete ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Ocho ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Nueve ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Diez ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Once ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Doce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Trece ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Catorce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Dos ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Tres ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cuatro ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cinco ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Seis ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Siete ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Ocho ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Nueve ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Diez ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Once ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Doce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Trece ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Catorce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)


            'hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            'hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Dos ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)

            'hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            'hasar.ImprimirAjuste("Bonificación Gral. Dos", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)

            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosMunicipales, "Otro Tributo Dos ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosNacionales, "Otro Tributo Tres ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosProvinciales, "Otro Tributo Cuatro ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionImpuestosMunicipales, "Otro Tributo Cinco ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionIVA, "Otro Tributo Seis ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionIIBB, "Otro Tributo Siete ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.OtrosTributos, "Otro Tributo Ocho ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.OtrasPercepciones, "Otro Tributo Nueve ...", 81.21, 4.06)

            'hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.Cheque, 0, "")
            'hasar.ImprimirPago("Medio de Pago Dos ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.Efectivo, 0, "")
            'hasar.ImprimirPago("Medio de Pago Tres ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TarjetaDeCredito, 0, "")
            'hasar.ImprimirPago("Medio de Pago Cuatro ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TarjetaDeDebito, 0, "")
            'hasar.ImprimirPago("Medio de Pago Cinco ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TransferenciaBancaria, 0, "")

            'subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            'Debug.Print("Monto Total          =[" & subt.Subtotal & "]")
            'Debug.Print("Monto Pagado         =[" & subt.MontoPagado & "]")
            'Debug.Print("Monto Base           =[" & subt.MontoBase & "]")
            'Debug.Print("Monto IVA            =[" & subt.MontoIVA & "]")
            'Debug.Print("Monto Imp. Int.      =[" & subt.MontoImpInternos & "]")
            'Debug.Print("Monto Otros Trib.    =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE FACTURA 'B' - Impresión de un comprobante tipo '82' ...
    '============================================================================================================================
    Private Sub ButtonTiqueFactB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTiqueFactB.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("============================================")
            Debug.Print("A imprimir: TIQUE FACTURA 'B' (Cód.: 82) ...")
            Debug.Print("============================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.Monotributo, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueFacturaB)
            Debug.Print("Tique Factura 'B' Nº =[" & respabrir.NumeroComprobante & "]")

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 2...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 3...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase)

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 2...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 3...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 4...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Dos ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Tres ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Cuatro ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Cinco ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Seis ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Siete ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Ocho ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Nueve ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Diez ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Once ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Doce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Trece ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Catorce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Quince ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Dos ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)

            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirAjuste("Bonificación Gral. Dos", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)

            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosMunicipales, "Otro Tributo Dos ...", 81.21, 4.06)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosNacionales, "Otro Tributo Tres ...", 81.21, 4.06)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosProvinciales, "Otro Tributo Cuatro ...", 81.21, 4.06)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionImpuestosMunicipales, "Otro Tributo Cinco ...", 81.21, 4.06)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionIVA, "Otro Tributo Seis ...", 81.21, 4.06)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionIIBB, "Otro Tributo Siete ...", 81.21, 4.06)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.OtrosTributos, "Otro Tributo Ocho ...", 81.21, 4.06)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.OtrasPercepciones, "Otro Tributo Nueve ...", 81.21, 4.06)

            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.Cheque, 0, "")
            hasar.ImprimirPago("Medio de Pago Dos ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.Efectivo, 0, "")
            hasar.ImprimirPago("Medio de Pago Tres ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TarjetaDeCredito, 0, "")
            hasar.ImprimirPago("Medio de Pago Cuatro ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TarjetaDeDebito, 0, "")
            hasar.ImprimirPago("Medio de Pago Cinco ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TransferenciaBancaria, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total          =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado         =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base           =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA            =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.      =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.    =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE FACTURA 'C' - Impresión de un comprobante tipo '111' ...
    '============================================================================================================================
    Private Sub ButtonTiqueFactC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTiqueFactC.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("=============================================")
            Debug.Print("A imprimir: TIQUE FACTURA 'C' (Cód.: 111) ...")
            Debug.Print("=============================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.Monotributo, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueFacturaC)
            Debug.Print("Tique Factura 'C' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total          =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado         =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base           =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA            =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.      =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.    =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: INFORME DIARIO DE CIERRE 'Z' - Impresión de un comprobante tipo '80' ...
    '============================================================================================================================
    Private Sub ButtonRepZeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRepZeta.Click
        Dim cierre As HasarArgentina.RespuestaCerrarJornadaFiscal
        Dim zeta As HasarArgentina.CerrarJornadaFiscalZ

        Try
            Debug.Print("=======================================================")
            Debug.Print("A imprimir: INFORME DIARIO DE CIERRE 'Z' (Cód.: 80) ...")
            Debug.Print("=======================================================")
            cierre = hasar.CerrarJornadaFiscal(HasarArgentina.TipoReporte.ReporteZ)
            zeta = cierre.Z

            Debug.Print("Cierre 'Z' Nº           =[" & zeta.Numero & "]")
            Debug.Print("Fecha del Cierre        =[" & zeta.Fecha & "]")
            Debug.Print("")
            Debug.Print("DF Emitidos             =[" & zeta.DF_CantidadEmitidos & "]")
            Debug.Print("DF Cancelados           =[" & zeta.DF_CantidadCancelados & "]")
            Debug.Print("")
            Debug.Print("DF Total                =[" & zeta.DF_Total & "]")
            Debug.Print("DF Total Gravado        =[" & zeta.DF_TotalGravado & "]")
            Debug.Print("DF Total No Gravado     =[" & zeta.DF_TotalNoGravado & "]")
            Debug.Print("DF Total Exento         =[" & zeta.DF_TotalExento & "]")
            Debug.Print("DF Total IVA            =[" & zeta.DF_TotalIVA & "]")
            Debug.Print("DF Total Otros Tributos =[" & zeta.DF_TotalTributos & "]")
            Debug.Print("")
            Debug.Print("NC Emitidas             =[" & zeta.NC_CantidadEmitidos & "]")
            Debug.Print("NC Canceladas           =[" & zeta.NC_CantidadCancelados & "]")
            Debug.Print("")
            Debug.Print("NC Total                =[" & zeta.NC_Total & "]")
            Debug.Print("NC Total Gravado        =[" & zeta.NC_TotalGravado & "]")
            Debug.Print("NC Total No Gravado     =[" & zeta.NC_TotalNoGravado & "]")
            Debug.Print("NC Total Exento         =[" & zeta.NC_TotalExento & "]")
            Debug.Print("NC Total IVA            =[" & zeta.NC_TotalIVA & "]")
            Debug.Print("NC Total Otros Tributos =[" & zeta.NC_TotalTributos & "]")
            Debug.Print("")
            Debug.Print("DNFH Emitidos           =[" & zeta.DNFH_CantidadEmitidos & "]")
            Debug.Print("DNFH Total              =[" & zeta.DNFH_Total & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE NOTA DE DÉBITO 'A' - Impresión de un comprobante tipo '115' ...
    '============================================================================================================================
    Private Sub ButtonTNDA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNDA.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("====================================================")
            Debug.Print("A imprimir: TIQUE NOTA DE DÉBITO 'A' (Cód.: 115) ...")
            Debug.Print("====================================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueNotaDebitoA)
            Debug.Print("Tique Nota de Débito 'A' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total                 =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado                =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base                  =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA                   =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.             =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.           =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE NOTA DE CRÉDITO - Impresión de un comprobante tipo '110' ...
    '             A Consumidor Final - Opuesto al TIQUE
    '============================================================================================================================
    Private Sub ButtonTiqueNC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTiqueNC.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("=================================================")
            Debug.Print("A imprimir: TIQUE NOTA DE CRÉDITO (Cód.: 110) ...")
            Debug.Print("=================================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueNotaCredito)
            Debug.Print("Tique Nota de Crédito Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total              =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado             =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base               =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA                =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.          =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.        =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE NOTA DE CRÉDITO 'A' - Impresión de un comprobante tipo '112' ...
    '============================================================================================================================
    Private Sub ButtonTNCA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNCA.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("=====================================================")
            Debug.Print("A imprimir: TIQUE NOTA DE CRÉDITO 'A' (Cód.: 112) ...")
            Debug.Print("=====================================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueNotaCreditoA)
            Debug.Print("Tique Nota de Crédito 'A' Nº =[" & respabrir.NumeroComprobante & "]")

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 2...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 3...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase)

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 2...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 3...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 4...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirItem("Producto Dos ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirItem("Producto Tres ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cuatro ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cinco ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Seis ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Siete ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Ocho ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Nueve ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Diez ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Once ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Doce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Trece ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Catorce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Dos ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Tres ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Cuatro ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Cinco ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Seis ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Siete ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Ocho ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Nueve ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Diez ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Once ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Doce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Trece ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Catorce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Dos ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Tres ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cuatro ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cinco ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Seis ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Siete ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Ocho ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Nueve ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Diez ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Once ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Doce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Trece ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Catorce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)


            'hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            'hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Dos ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)

            'hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            'hasar.ImprimirAjuste("Bonificación Gral. Dos", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)

            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosMunicipales, "Otro Tributo Dos ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosNacionales, "Otro Tributo Tres ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosProvinciales, "Otro Tributo Cuatro ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionImpuestosMunicipales, "Otro Tributo Cinco ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionIVA, "Otro Tributo Seis ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionIIBB, "Otro Tributo Siete ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.OtrosTributos, "Otro Tributo Ocho ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.OtrasPercepciones, "Otro Tributo Nueve ...", 81.21, 4.06)

            'hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.Cheque, 0, "")
            'hasar.ImprimirPago("Medio de Pago Dos ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.Efectivo, 0, "")
            'hasar.ImprimirPago("Medio de Pago Tres ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TarjetaDeCredito, 0, "")
            'hasar.ImprimirPago("Medio de Pago Cuatro ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TarjetaDeDebito, 0, "")
            'hasar.ImprimirPago("Medio de Pago Cinco ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TransferenciaBancaria, 0, "")

            'subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            'Debug.Print("Monto Total          =[" & subt.Subtotal & "]")
            'Debug.Print("Monto Pagado         =[" & subt.MontoPagado & "]")
            'Debug.Print("Monto Base           =[" & subt.MontoBase & "]")
            'Debug.Print("Monto IVA            =[" & subt.MontoIVA & "]")
            'Debug.Print("Monto Imp. Int.      =[" & subt.MontoImpInternos & "]")
            'Debug.Print("Monto Otros Trib.    =[" & subt.MontoOtrosTributos & "]")


            '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            'hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            'hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            'hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            'subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            'Debug.Print("Monto Total                  =[" & subt.Subtotal & "]")
            'Debug.Print("Monto Pagado                 =[" & subt.MontoPagado & "]")
            'Debug.Print("Monto Base                   =[" & subt.MontoBase & "]")
            'Debug.Print("Monto IVA                    =[" & subt.MontoIVA & "]")
            'Debug.Print("Monto Imp. Int.              =[" & subt.MontoImpInternos & "]")
            'Debug.Print("Monto Otros Trib.            =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE NOTA DE CRÉDITO 'B' - Impresión de un comprobante tipo '113' ...
    '============================================================================================================================
    Private Sub ButtonTNCB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNCB.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("=====================================================")
            Debug.Print("A imprimir: TIQUE NOTA DE CRÉDITO 'B' (Cód.: 113) ...")
            Debug.Print("=====================================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.Monotributo, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueNotaCreditoB)
            Debug.Print("Tique Nota de Crédito 'B' Nº =[" & respabrir.NumeroComprobante & "]")

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 2...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 3...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase)

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 2...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 3...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 4...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirItem("Producto Dos ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock 1...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirItem("Producto Tres ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cuatro ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cinco ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Seis ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Siete ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Ocho ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Nueve ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Diez ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Once ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Doce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Trece ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Catorce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Dos ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Tres ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Cuatro ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Cinco ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Seis ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Siete ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Ocho ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Nueve ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 2.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Diez ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 3.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Once ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 4.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Doce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 5.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Trece ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 6.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirItem("Producto Catorce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 7.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)

            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Dos ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Tres ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cuatro ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Cinco ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Seis ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Siete ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Ocho ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Nueve ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Diez ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Once ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Doce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Trece ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirItem("Producto Catorce ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)


            'hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            'hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Dos ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 1.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)

            'hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            'hasar.ImprimirAjuste("Bonificación Gral. Dos", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioBase, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)

            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosMunicipales, "Otro Tributo Dos ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosNacionales, "Otro Tributo Tres ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.ImpuestosProvinciales, "Otro Tributo Cuatro ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionImpuestosMunicipales, "Otro Tributo Cinco ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionIVA, "Otro Tributo Seis ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.PercepcionIIBB, "Otro Tributo Siete ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.OtrosTributos, "Otro Tributo Ocho ...", 81.21, 4.06)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.OtrasPercepciones, "Otro Tributo Nueve ...", 81.21, 4.06)

            'hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.Cheque, 0, "")
            'hasar.ImprimirPago("Medio de Pago Dos ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.Efectivo, 0, "")
            'hasar.ImprimirPago("Medio de Pago Tres ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TarjetaDeCredito, 0, "")
            'hasar.ImprimirPago("Medio de Pago Cuatro ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TarjetaDeDebito, 0, "")
            'hasar.ImprimirPago("Medio de Pago Cinco ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "", HasarArgentina.TiposPago.TransferenciaBancaria, 0, "")

            'subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            'Debug.Print("Monto Total          =[" & subt.Subtotal & "]")
            'Debug.Print("Monto Pagado         =[" & subt.MontoPagado & "]")
            'Debug.Print("Monto Base           =[" & subt.MontoBase & "]")
            'Debug.Print("Monto IVA            =[" & subt.MontoIVA & "]")
            'Debug.Print("Monto Imp. Int.      =[" & subt.MontoImpInternos & "]")
            'Debug.Print("Monto Otros Trib.    =[" & subt.MontoOtrosTributos & "]")





            '00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
            'hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            'hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            'hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            'hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            'hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            'hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            'hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            'subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            'Debug.Print("Monto Total                  =[" & subt.Subtotal & "]")
            'Debug.Print("Monto Pagado                 =[" & subt.MontoPagado & "]")
            'Debug.Print("Monto Base                   =[" & subt.MontoBase & "]")
            'Debug.Print("Monto IVA                    =[" & subt.MontoIVA & "]")
            'Debug.Print("Monto Imp. Int.              =[" & subt.MontoImpInternos & "]")
            'Debug.Print("Monto Otros Trib.            =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE NOTA DE CRÉDITO 'C' - Impresión de un comprobante tipo '114' ...
    '============================================================================================================================
    Private Sub ButtonTNCC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNCC.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("=====================================================")
            Debug.Print("A imprimir: TIQUE NOTA DE CRÉDITO 'C' (Cód.: 114) ...")
            Debug.Print("=====================================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.Monotributo, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueNotaCreditoC)
            Debug.Print("Tique Nota de Crédito 'C' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total                  =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado                 =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base                   =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA                    =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.              =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.            =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE FACTURA 'M' - Impresión de un comprobante tipo '118' ...
    '============================================================================================================================
    Private Sub ButtonTiqueFactM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTiqueFactM.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("=============================================")
            Debug.Print("A imprimir: TIQUE FACTURA 'M' (Cód.: 118) ...")
            Debug.Print("=============================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueFacturaM)
            Debug.Print("Tique Factura 'M' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total         =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado        =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base          =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA           =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.     =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.   =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE NOTA DE CRÉDITO 'M' - Impresión de un comprobante tipo '119' ...
    '============================================================================================================================
    Private Sub ButtonTNCM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNCM.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("=====================================================")
            Debug.Print("A imprimir: TIQUE NOTA DE CRÉDITO 'M' (Cód.: 119) ...")
            Debug.Print("=====================================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueNotaCreditoM)
            Debug.Print("Tique Nota de Crédito 'M' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total                  =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado                 =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base                   =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA                    =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.              =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.            =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE RECIBO 'A' - Impresión de un comprobante tipo '4' ...
    '============================================================================================================================
    Private Sub ButtonTRA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTRA.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("==========================================")
            Debug.Print("A imprimir: TIQUE RECIBO 'A' (Cód.: 4) ...")
            Debug.Print("==========================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.ReciboA)
            Debug.Print("Tique Recibo 'A' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total         =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado        =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base          =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA           =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.     =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.   =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE NOTA DE DÉBITO 'B' - Impresión de un comprobante tipo '116' ...
    '============================================================================================================================
    Private Sub ButtonTNDB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNDB.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("====================================================")
            Debug.Print("A imprimir: TIQUE NOTA DE DÉBITO 'B' (Cód.: 116) ...")
            Debug.Print("====================================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.Monotributo, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueNotaDebitoB)
            Debug.Print("Tique Nota de Débito 'B' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total                 =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado                =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base                  =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA                   =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.             =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.           =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE RECIBO 'B' - Impresión de un comprobante tipo '9' ...
    '============================================================================================================================
    Private Sub ButtonTRB_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTRB.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("==========================================")
            Debug.Print("A imprimir: TIQUE RECIBO 'B' (Cód.: 9) ...")
            Debug.Print("==========================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.Monotributo, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.ReciboB)
            Debug.Print("Tique Recibo 'B' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total         =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado        =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base          =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA           =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.     =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.   =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE NOTA DE DÉBITO 'C' - Impresión de un comprobante tipo '117' ...
    '============================================================================================================================
    Private Sub ButtonTNDC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNDC.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("====================================================")
            Debug.Print("A imprimir: TIQUE NOTA DE DÉBITO 'C' (Cód.: 117) ...")
            Debug.Print("====================================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.Monotributo, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueNotaDebitoC)
            Debug.Print("Tique Nota de Débito 'C' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total                 =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado                =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base                  =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA                   =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.             =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.           =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE RECIBO 'C' - Impresión de un comprobante tipo '15' ...
    '============================================================================================================================
    Private Sub ButtonTRC_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTRC.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("===========================================")
            Debug.Print("A imprimir: TIQUE RECIBO 'C' (Cód.: 15) ...")
            Debug.Print("===========================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.Monotributo, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.ReciboC)
            Debug.Print("Tique Recibo 'C' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total         =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado        =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base          =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA           =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.     =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.   =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE NOTA DE DÉBITO 'M' - Impresión de un comprobante tipo '120' ...
    '============================================================================================================================
    Private Sub ButtonTNDM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTNDM.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("====================================================")
            Debug.Print("A imprimir: TIQUE NOTA DE DÉBITO 'M' (Cód.: 120) ...")
            Debug.Print("====================================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueNotaDebitoM)
            Debug.Print("Tique Nota de Débito 'M' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total                  =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado                 =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base                   =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA                    =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.              =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.            =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) TIQUE RECIBO 'M' - Impresión de un comprobante tipo '54' ...
    '============================================================================================================================
    Private Sub ButtonTRM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonTRM.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("===========================================")
            Debug.Print("A imprimir: TIQUE RECIBO 'M' (Cód.: 54) ...")
            Debug.Print("===========================================")
            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.ReciboM)
            Debug.Print("Tique Recibo 'M' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total         =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado        =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base          =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA           =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.     =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.   =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: (DF) DOCUMENTO PATRON - Impresión de un comprobante Patrón ...                         = NO MODIFICAR ESTA RUTINA =
    '
    ' Comprobante establecido por la RG AFIP Nº 3561/13.
    '============================================================================================================================
    Private Sub ButtonPatron_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPatron.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("===============================================================")
            Debug.Print("A imprimir: DOCUMENTO PATRÓN - TIQUE FACTURA 'A' (Cód.: 81) ...")
            Debug.Print("===============================================================")

            hasar.ConfigurarZona(1, estilo, "Pasaje Ignoto 777 - (1408) Villa Real ..........................", HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.ZonasDeLineasDeUsuario.ZonaDomicilioEmisor)
            hasar.ConfigurarZona(2, estilo, "Ciudad Autónoma de Buenos Aires ................................", HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.ZonasDeLineasDeUsuario.ZonaDomicilioEmisor)
            ' hasar.ConfigurarZona(3, estilo, "República Argentina ............................................", HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.ZonasDeLineasDeUsuario.ZonaDomicilioEmisor)
            ' hasar.ConfigurarZona(4, estilo, "América del Sur ................................................", HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.ZonasDeLineasDeUsuario.ZonaDomicilioEmisor)

            hasar.CargarDatosCliente("El kioskazo de Villa Real ........................", "22222222226", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Avenida Julio Argentino Roca 852 - Local 9 - Entrepiso .........", "(1401) Ciudad Autónoma de Buenos Aires .........................", "República Argentina ............................................", "América del Sur ................................................")
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.TiqueFacturaA)
            Debug.Print("Documento Patrón - Tique Factura 'A' Nº =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Salta ....................................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("CARAMELOS UNO ....................", 2.5, 9.99, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIFijoMonto, 1.99, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Kilo)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Jujuy ....................................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("CARAMELOS DOS ....................", 2.5, 9.99, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIFijoMonto, 1.99, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500002", "00002", HasarArgentina.UnidadesMedida.Kilo)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Chaco ....................................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("CARAMELOS TRES ...................", 2.5, 9.99, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIFijoMonto, 1.99, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500003", "00003", HasarArgentina.UnidadesMedida.Kilo)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Formosa ..................................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("CARAMELOS CUATRO .................", 2.5, 9.99, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIFijoMonto, 1.99, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500004", "00004", HasarArgentina.UnidadesMedida.Kilo)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Tucumán ..................................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("CARAMELOS CINCO ..................", 2.5, 9.99, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIFijoMonto, 1.99, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500005", "00005", HasarArgentina.UnidadesMedida.Kilo)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Santiago del Estero ......................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("CARAMELOS SEIS ...................", 2.5, 9.99, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIFijoMonto, 1.99, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500006", "00006", HasarArgentina.UnidadesMedida.Kilo)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Catamarca ................................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("CARAMELOS SIETE ..................", 2.5, 9.99, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIFijoMonto, 1.99, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500007", "00007", HasarArgentina.UnidadesMedida.Kilo)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Origen: La Rioja .................................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("CARAMELOS OCHO ...................", 2.5, 9.99, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIFijoMonto, 1.99, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500008", "00008", HasarArgentina.UnidadesMedida.Kilo)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Origen: Mendoza ..................................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("CARAMELOS NUEVE ..................", 2.5, 9.99, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIFijoMonto, 1.99, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500009", "00009", HasarArgentina.UnidadesMedida.Kilo)

            hasar.ImprimirTextoFiscal(estilo, "Producto de fabricación nacional .................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Origen: San Luis .................................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Producto de exportación ..........................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoFiscal(estilo, "Presentación: Bolsa x Kilo .......................", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("CARAMELOS DIEZ ...................", 2.5, 9.99, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIFijoMonto, 1.99, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500010", "00010", HasarArgentina.UnidadesMedida.Kilo)

            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Percepción Ingresos Brutos ....", 165.3, 19.99)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Percepción Imp Municipal ......", 165.3, 19.99)
            hasar.ImprimirPago("Efectivo ........................................", 289.78, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Pesos Argentinos ................................", HasarArgentina.TiposPago.Efectivo, , "")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: DETALLE DE VENTAS 'X' - Impresión de un comprobante tipo '940' ...
    '============================================================================================================================
    Private Sub ButtonEquis_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEquis.Click
        Dim cierre As HasarArgentina.RespuestaCerrarJornadaFiscal
        Dim equis As HasarArgentina.CerrarJornadaFiscalX

        Try
            Debug.Print("=================================================")
            Debug.Print("A imprimir: DETALLE DE VENTAS 'X' (Cód.: 940) ...")
            Debug.Print("=================================================")
            cierre = hasar.CerrarJornadaFiscal(HasarArgentina.TipoReporte.ReporteX)
            equis = cierre.X

            Debug.Print("Detalle de Ventas 'X' Nº =[" & equis.Numero & "]")
            Debug.Print("Fecha de Inicio          =[" & equis.FechaInicio & "]")
            Debug.Print("Hora de Inicio           =[" & equis.HoraInicio & "]")
            Debug.Print("Fecha de Cierre          =[" & equis.FechaCierre & "]")
            Debug.Print("Hora de Cierre           =[" & equis.HoraCierre & "]")
            Debug.Print("")
            Debug.Print("DF Emitidos              =[" & equis.DF_CantidadEmitidos & "]")
            Debug.Print("DF Total                 =[" & equis.DF_Total & "]")
            Debug.Print("DF Total IVA             =[" & equis.DF_TotalIVA & "]")
            Debug.Print("DF Total Otros Tributos  =[" & equis.DF_TotalTributos & "]")
            Debug.Print("")
            Debug.Print("NC Emitidas              =[" & equis.NC_CantidadEmitidos & "]")
            Debug.Print("NC Total                 =[" & equis.NC_Total & "]")
            Debug.Print("NC Total IVA             =[" & equis.NC_TotalIVA & "]")
            Debug.Print("NC Total Otros Tributos  =[" & equis.NC_TotalTributos & "]")
            Debug.Print("")
            Debug.Print("DNFH Emitidos            =[" & equis.DNFH_CantidadEmitidos & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ESTADO GENERAL - Consulta ...
    '============================================================================================================================
    Private Sub ButtonEstadoGral_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEstadoGral.Click
        Dim resp As HasarArgentina.RespuestaConsultarEstado

        Try
            Debug.Print("=========================================================")
            Debug.Print("Consulta: ESTADO GENERAL ... (0=Falso , -1=Verdadero) ...")
            Debug.Print("=========================================================")

            resp = hasar.ConsultarEstado(HasarArgentina.TiposComprobante.NoDocumento)
            Debug.Print("Operando en Modo Entrenamiento ? =[" & resp.EstadoAuxiliar.ModoEntrenamiento & "]")
            Debug.Print("Estado Interno                   =[" & resp.EstadoInterno & "]")
            Debug.Print("")
            Debug.Print("Código Comprobante               =[" & resp.CodigoComprobante & "]")
            Debug.Print("Comprobante en Curso             =[" & resp.ComprobanteEnCurso & "]")
            Debug.Print("Número Último Comprobante        =[" & resp.NumeroUltimoComprobante & "]")
            Debug.Print("Último Comprobante Cancelado   ? =[" & resp.EstadoAuxiliar.UltimoComprobanteFueCancelado & "]")
            Debug.Print("")
            Debug.Print("Comprobantes Emitidos            =[" & resp.CantidadEmitidos & "]")
            Debug.Print("Comprobantes Cancelados          =[" & resp.CantidadCancelados & "]")
            Debug.Print("")
            Debug.Print("Código Barras Memorizado       ? =[" & resp.EstadoAuxiliar.CodigoBarrasAlmacenado & "]")
            Debug.Print("Datos de Cliente Memorizados   ? =[" & resp.EstadoAuxiliar.DatosClienteAlmacenados & "]")
            Debug.Print("Memoria Auditoría Casi LLena   ? =[" & resp.EstadoAuxiliar.MemoriaAuditoriaCasiLlena & "]")
            Debug.Print("Memoria Auditoría Llena        ? =[" & resp.EstadoAuxiliar.MemoriaAuditoriaLlena & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: MODELO Y VERSIÓN - Consulta ...
    '============================================================================================================================
    Private Sub ButtonModelo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonModelo.Click
        Dim resp As HasarArgentina.RespuestaConsultarVersion

        Try
            Debug.Print("==============================")
            Debug.Print("Consulta: MODELO Y VERSIÓN ...")
            Debug.Print("==============================")

            resp = hasar.ConsultarVersion()
            Debug.Print("Producto          =[" & resp.NombreProducto & "]")
            Debug.Print("Marca             =[" & resp.Marca & "]")
            Debug.Print("Modelo y Versión  =[" & resp.Version & "]")
            Debug.Print("Fecha de Firmware =[" & resp.FechaFirmware & "]")
            Debug.Print("Motor Versión     =[" & resp.VersionMotor & "]")
            Debug.Print("Protocolo Versión =[" & resp.VersionProtocolo & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: DATOS DE INICIALIZACIÓN - Consulta ...
    '============================================================================================================================
    Private Sub ButtonDatosIni_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDatosIni.Click
        Dim resp As HasarArgentina.RespuestaConsultarDatosInicializacion

        Try
            Debug.Print("=====================================")
            Debug.Print("Consulta: DATOS DE INICIALIZACIÓN ...")
            Debug.Print("=====================================")

            resp = hasar.ConsultarDatosInicializacion
            Debug.Print("Razón Social        =[" & resp.RazonSocial & "]")
            Debug.Print("Número CUIT         =[" & resp.Cuit & "]")
            Debug.Print("Categoría IVA       =[" & resp.ResponsabilidadIVA & "]")
            Debug.Print("Ingresos Brutos     =[" & resp.IngBrutos & "]")
            Debug.Print("Fecha Inicio Activ. =[" & resp.FechaInicioActividades & "]")
            Debug.Print("")
            Debug.Print("Número POS          =[" & resp.NumeroPos & "]")
            Debug.Print("Código Registro     =[" & resp.Registro & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: FECHA Y HORA - Consulta ...
    '============================================================================================================================
    Private Sub ButtonFechayHora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonFechayHora.Click
        Dim resp As HasarArgentina.RespuestaConsultarFechaHora

        Try
            Debug.Print("==========================")
            Debug.Print("Consulta: FECHA Y HORA ...")
            Debug.Print("==========================")

            resp = hasar.ConsultarFechaHora()
            Debug.Print("Fecha Actual =[" & resp.Fecha & "]")
            Debug.Print("Hora Actual  =[" & resp.Hora & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ÚLTIMO ESTADO FISCAL - Consulta ...
    '============================================================================================================================
    Private Sub ButtonUltEstFiscal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUltEstFiscal.Click
        Dim resp As HasarArgentina.EstadoFiscal

        Try
            Debug.Print("===============================================================")
            Debug.Print("Consulta: ÚLTIMO ESTADO FISCAL ... (0=Falso , -1=Verdadero) ...")
            Debug.Print("===============================================================")

            resp = hasar.ObtenerUltimoEstadoFiscal()
            Debug.Print("Documento Abierto           ? =[" & resp.DocumentoAbierto & "]")
            Debug.Print("Documento Fiscal Abierto    ? =[" & resp.DocumentoFiscalAbierto & "]")
            Debug.Print("Error Aritmético            ? =[" & resp.ErrorAritmetico & "]")
            Debug.Print("Error Ejecución             ? =[" & resp.ErrorEjecucion & "]")
            Debug.Print("Error Estado                ? =[" & resp.ErrorEstado & "]")
            Debug.Print("Error General               ? =[" & resp.ErrorGeneral & "]")
            Debug.Print("Error Memoria Auditoría     ? =[" & resp.ErrorMemoriaAuditoria & "]")
            Debug.Print("Error Memoria Fiscal        ? =[" & resp.ErrorMemoriaFiscal & "]")
            Debug.Print("Error Memoria de Trabajo    ? =[" & resp.ErrorMemoriaTrabajo & "]")
            Debug.Print("Error de Parámetro          ? =[" & resp.ErrorParametro & "]")
            Debug.Print("Memoria Fiscal Casi Llena   ? =[" & resp.MemoriaFiscalCasiLlena & "]")
            Debug.Print("Memoria Fiscal Inicializada ? =[" & resp.MemoriaFiscalInicializada & "]")
            Debug.Print("Memoria Fiscal Llena        ? =[" & resp.MemoriaFiscalLlena & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ÚLTIMO ESTADO IMPRESORA - Consulta ...
    '============================================================================================================================
    Private Sub ButtonUltEstImpr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUltEstImpr.Click
        Dim resp As HasarArgentina.EstadoImpresora

        Try
            Debug.Print("==================================================================")
            Debug.Print("Consulta: ÚLTIMO ESTADO IMPRESORA ... (0=Falso , -1=Verdadero) ...")
            Debug.Print("==================================================================")

            resp = hasar.ObtenerUltimoEstadoImpresora()
            Debug.Print("Cajón Dinero Abierto ? =[" & resp.CajonAbierto & "]")
            Debug.Print("Impresora en Error   ? =[" & resp.ErrorImpresora & "]")
            Debug.Print("Falta Papel J.       ? =[" & resp.FaltaPapelJournal & "]")
            Debug.Print("Falta Papel T.       ? =[" & resp.FaltaPapelReceipt & "]")
            Debug.Print("Impresora Ocupada    ? =[" & resp.ImpresoraOcupada & "]")
            Debug.Print("Impresora Offline    ? =[" & resp.ImpresoraOffLine & "]")
            Debug.Print("Tapa Abierta         ? =[" & resp.TapaAbierta & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: CARACTERÍSTICAS BÁSICAS - Consulta ...
    '============================================================================================================================
    Private Sub ButtonCapacidades_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCapacidades.Click
        Dim resp As HasarArgentina.RespuestaConsultarCapacidadesImpresoraFiscal

        Try
            Debug.Print("=====================================")
            Debug.Print("Consulta: CARACTERÍSTICAS BÁSICAS ...")
            Debug.Print("=====================================")

            resp = hasar.ConsultarCapacidadesImpresoraFiscal(HasarArgentina.Capacidades.AltoLogo, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.TiposComprobante.NoDocumento)
            Debug.Print("Alto Logo                  =[" & resp.Valor & "]")
            resp = hasar.ConsultarCapacidadesImpresoraFiscal(HasarArgentina.Capacidades.AnchoLogo, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.TiposComprobante.NoDocumento)
            Debug.Print("Ancho Logo                 =[" & resp.Valor & "]")
            resp = hasar.ConsultarCapacidadesImpresoraFiscal(HasarArgentina.Capacidades.AnchoRazonSocial, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.TiposComprobante.NoDocumento)
            Debug.Print("Ancho Razón Social         =[" & resp.Valor & "]")
            resp = hasar.ConsultarCapacidadesImpresoraFiscal(HasarArgentina.Capacidades.AnchoTextoFiscal, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.TiposComprobante.NoDocumento)
            Debug.Print("Ancho Texto Fiscal         =[" & resp.Valor & "]")
            resp = hasar.ConsultarCapacidadesImpresoraFiscal(HasarArgentina.Capacidades.AnchoTextoLineasUsuario, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.TiposComprobante.NoDocumento)
            Debug.Print("Ancho Texto Líneas Usuario =[" & resp.Valor & "]")
            resp = hasar.ConsultarCapacidadesImpresoraFiscal(HasarArgentina.Capacidades.AnchoTextoNoFiscal, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.TiposComprobante.NoDocumento)
            Debug.Print("Ancho Texto No Fiscal      =[" & resp.Valor & "]")
            resp = hasar.ConsultarCapacidadesImpresoraFiscal(HasarArgentina.Capacidades.AnchoTextoVenta, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.TiposComprobante.NoDocumento)
            Debug.Print("Ancho Texto Venta          =[" & resp.Valor & "]")
            resp = hasar.ConsultarCapacidadesImpresoraFiscal(HasarArgentina.Capacidades.AnchoTotalImpresion, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.TiposComprobante.NoDocumento)
            Debug.Print("Ancho Total Impresión      =[" & resp.Valor & "]")
            resp = hasar.ConsultarCapacidadesImpresoraFiscal(HasarArgentina.Capacidades.SoportaCajon, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.TiposComprobante.NoDocumento)
            Debug.Print("Soporta Cajón Dinero       =[" & resp.Valor & "]")
            resp = hasar.ConsultarCapacidadesImpresoraFiscal(HasarArgentina.Capacidades.SoportaComprobante, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.TiposComprobante.TiqueFacturaB)
            Debug.Print("Soporta Tique Factura 'B'  =[" & resp.Valor & "]")
            resp = hasar.ConsultarCapacidadesImpresoraFiscal(HasarArgentina.Capacidades.SoportaEstacion, HasarArgentina.TiposDeEstacion.EstacionSlip, HasarArgentina.TiposComprobante.NoDocumento)
            Debug.Print("Soporta Estación Slip      =[" & resp.Valor & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ZETAS TOTALES - Consulta ...
    '============================================================================================================================
    Private Sub Button17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button17.Click
        Dim resp As HasarArgentina.RespuestaConsultarCapacidadZetas

        Try
            Debug.Print("===========================")
            Debug.Print("Consulta: ZETAS TOTALES ...")
            Debug.Print("===========================")

            resp = hasar.ConsultarCapacidadZetas()
            Debug.Print("Último Cierre 'Z     ' =[" & resp.UltimaZeta & "]")
            Debug.Print("Cierres 'Z' Remanentes =[" & resp.CantidadDeZetasRemanentes & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub


    '============================================================================================================================
    ' BOTÓN: CONFIGURACIÓN RED - Consulta ...
    '============================================================================================================================
    Private Sub ButtonCfgRed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCfgRed.Click
        Dim resp As HasarArgentina.RespuestaConsultarConfiguracionRed

        Try
            Debug.Print("===============================")
            Debug.Print("Consulta: CONFIGURACIÓN RED ...")
            Debug.Print("===============================")

            resp = hasar.ConsultarConfiguracionRed()
            Debug.Print("Dirección IP =[" & resp.DireccionIP & "]")
            Debug.Print("Máscara      =[" & resp.Mascara & "]")
            Debug.Print("Gateway      =[" & resp.Gateway & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: CONFIGURACIÓN CORREO - Consulta ...
    '============================================================================================================================
    Private Sub ButtonCfgCorreo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCfgCorreo.Click
        Dim resp As HasarArgentina.RespuestaConsultarConfiguracionServidorCorreo

        Try
            Debug.Print("==================================")
            Debug.Print("Consulta: CONFIGURACIÓN CORREO ...")
            Debug.Print("==================================")

            resp = hasar.ConsultarConfiguracionServidorCorreo()
            Debug.Print("Remitente    =[" & resp.DireccionRemitente & "]")
            Debug.Print("Dirección IP =[" & resp.DireccionIP & "]")
            Debug.Print("Puerto       =[" & resp.Puerto & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: SUBTOTAL COMPROBANTE - Consulta ...
    '============================================================================================================================
    Private Sub ButtonSubtot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSubtot.Click
        Dim resp As HasarArgentina.RespuestaConsultarSubtotal

        Try
            Debug.Print("==================================")
            Debug.Print("Consulta: SUBTOTAL COMPROBANTE ...")
            Debug.Print("==================================")

            resp = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total       =[" & resp.Subtotal & "]")
            Debug.Print("Monto Pagado      =[" & resp.MontoPagado & "]")
            Debug.Print("Monto Base        =[" & resp.MontoBase & "]")
            Debug.Print("Monto IVA         =[" & resp.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.   =[" & resp.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib. =[" & resp.MontoOtrosTributos & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: RANGO NROS. ZETA - Consulta ...
    '============================================================================================================================
    Private Sub ButtonRangoZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRangoZ.Click
        Dim resp1 As HasarArgentina.RespuestaObtenerRangoFechasPorZetas
        Dim resp2 As HasarArgentina.RespuestaObtenerRangoZetasPorFechas

        Try
            Debug.Print("==============================")
            Debug.Print("Consulta: RANGO NROS. ZETA ...")
            Debug.Print("==============================")

            Debug.Print("Consulta por rango de números de 'Z', desde '1' hasta '8' ...")
            Debug.Print("-------------------------------------------------------------")
            resp1 = hasar.ObtenerRangoFechasPorZetas(1, 3650)
            Debug.Print("Fecha Inicial =[" & resp1.FechaInicial & "]")
            Debug.Print("Fecha Final   =[" & resp1.FechaFinal & "]")
            Debug.Print("")

            Debug.Print("Consulta por rango de fechas, desde '16/07/2015' hasta '14/10/2015' ...")
            Debug.Print("-----------------------------------------------------------------------")
            resp2 = hasar.ObtenerRangoZetasPorFechas("16/07/2015", "14/10/2015")
            Debug.Print("Zeta Inicial =[" & resp2.ZetaInicial & "]")
            Debug.Print("Zeta Final   =[" & resp2.ZetaFinal & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: LÍMITE BORRADO ZETAS - Consulta ...
    '============================================================================================================================
    Private Sub Button22_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button22.Click
        Dim resp As HasarArgentina.RespuestaConsultarZetaBorrable

        Try
            Debug.Print("===============================================================")
            Debug.Print("Consulta: LÍMITE BORRADO ZETAS ... (0 = Borrado automático) ...")
            Debug.Print("===============================================================")

            resp = hasar.ConsultarZetaBorrable()
            Debug.Print("En caso de ser requerido ...")
            Debug.Print("Borrar hasta 'Z' Nº =[" & resp.NumeroZeta & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ÚLTIMO ERROR - Consulta ...
    '============================================================================================================================
    Private Sub ButtonUltError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonUltError.Click
        Dim resp As HasarArgentina.RespuestaConsultarUltimoError

        Try
            Debug.Print("==========================")
            Debug.Print("Consulta: ÚLTIMO ERROR ...")
            Debug.Print("==========================")

            resp = hasar.ConsultarUltimoError()
            Debug.Print("Parámetro Nº     =[" & resp.NumeroParametro & "]")
            Debug.Print("Nombre Parámetro =[" & resp.NombreParametro & "]")
            Debug.Print("Último Error     =[" & resp.UltimoError & "]")
            Debug.Print("Descripción      =[" & resp.Descripcion & "]")
            Debug.Print("Contexto         =[" & resp.Contexto & "]")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: COMPROBANTES ASOCIADOS - Consulta ...
    '============================================================================================================================
    Private Sub ButtonDocAsoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDocAsoc.Click
        Dim resp As HasarArgentina.RespuestaConsultarDocumentoAsociado
        Dim k As Integer

        Try
            Debug.Print("====================================")
            Debug.Print("Consulta: COMPROBANTES ASOCIADOS ...")
            Debug.Print("====================================")

            For k = 1 To 2
                resp = hasar.ConsultarDocumentoAsociado(k)
                Debug.Print("Línea " & k & " ...")
                Debug.Print("-----------")
                Debug.Print("Nº POS         =[" & resp.NumeroPos & "]")
                Debug.Print("Cód. Comprob.  =[" & resp.CodigoComprobante & "]")
                Debug.Print("Comprobante Nº =[" & resp.NumeroComprobante & "]")
                Debug.Print("")
            Next
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: LÍNEAS DE USUARIO - Consulta ...
    '============================================================================================================================
    Private Sub Button25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button25.Click
        Dim resp As HasarArgentina.RespuestaConsultarZona
        Dim k As Integer

        Try
            Debug.Print("=================================================================")
            Debug.Print("Consulta: COMPROBANTES ASOCIADOS ... (0=Falso , -1=Verdadero) ...")
            Debug.Print("=================================================================")

            Debug.Print("Líneas de fantasía ...")
            Debug.Print("----------------------")
            For k = 1 To 2
                resp = hasar.ConsultarZona(k, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.ZonasDeLineasDeUsuario.ZonaFantasia)
                Debug.Print("Línea " & k & " ...")
                Debug.Print("Texto       =[" & resp.Descripcion & "]")
                Debug.Print("Centrado    =[" & resp.Atributos.Centrado & "]")
                Debug.Print("Doble Ancho =[" & resp.Atributos.DobleAncho & "]")
                Debug.Print("Negrita     =[" & resp.Atributos.Negrita & "]")
                Debug.Print("Borrado   ? =[" & resp.Atributos.BorradoTexto & "]")
                Debug.Print("")
            Next

            Debug.Print("Líneas de encabezado, zona 1 ...")
            Debug.Print("--------------------------------")
            For k = 1 To 3
                resp = hasar.ConsultarZona(k, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.ZonasDeLineasDeUsuario.Zona1Encabezado)
                Debug.Print("Línea " & k & " ...")
                Debug.Print("Texto       =[" & resp.Descripcion & "]")
                Debug.Print("Centrado    =[" & resp.Atributos.Centrado & "]")
                Debug.Print("Doble Ancho =[" & resp.Atributos.DobleAncho & "]")
                Debug.Print("Negrita     =[" & resp.Atributos.Negrita & "]")
                Debug.Print("Borrado   ? =[" & resp.Atributos.BorradoTexto & "]")
                Debug.Print("")
            Next

            Debug.Print("Líneas de encabezado, zona 2 ...")
            Debug.Print("--------------------------------")
            For k = 1 To 3
                resp = hasar.ConsultarZona(k, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.ZonasDeLineasDeUsuario.Zona2Encabezado)
                Debug.Print("Línea " & k & " ...")
                Debug.Print("Texto       =[" & resp.Descripcion & "]")
                Debug.Print("Centrado    =[" & resp.Atributos.Centrado & "]")
                Debug.Print("Doble Ancho =[" & resp.Atributos.DobleAncho & "]")
                Debug.Print("Negrita     =[" & resp.Atributos.Negrita & "]")
                Debug.Print("Borrado   ? =[" & resp.Atributos.BorradoTexto & "]")
                Debug.Print("")
            Next

            Debug.Print("Líneas de cola, zona 1 ...")
            Debug.Print("--------------------------")
            For k = 1 To 4
                resp = hasar.ConsultarZona(k, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.ZonasDeLineasDeUsuario.Zona1Cola)
                Debug.Print("Línea " & k & " ...")
                Debug.Print("Texto       =[" & resp.Descripcion & "]")
                Debug.Print("Centrado    =[" & resp.Atributos.Centrado & "]")
                Debug.Print("Doble Ancho =[" & resp.Atributos.DobleAncho & "]")
                Debug.Print("Negrita     =[" & resp.Atributos.Negrita & "]")
                Debug.Print("Borrado   ? =[" & resp.Atributos.BorradoTexto & "]")
                Debug.Print("")
            Next

            Debug.Print("Líneas de cola, zona 2 ...")
            Debug.Print("--------------------------")
            For k = 1 To 6
                resp = hasar.ConsultarZona(k, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.ZonasDeLineasDeUsuario.Zona2Cola)
                Debug.Print("Línea " & k & " ...")
                Debug.Print("Texto       =[" & resp.Descripcion & "]")
                Debug.Print("Centrado    =[" & resp.Atributos.Centrado & "]")
                Debug.Print("Doble Ancho =[" & resp.Atributos.DobleAncho & "]")
                Debug.Print("Negrita     =[" & resp.Atributos.Negrita & "]")
                Debug.Print("Borrado   ? =[" & resp.Atributos.BorradoTexto & "]")
                Debug.Print("")
            Next

            Debug.Print("Líneas de domicilio del emisor ...")
            Debug.Print("----------------------------------")
            For k = 1 To 4
                resp = hasar.ConsultarZona(k, HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.ZonasDeLineasDeUsuario.ZonaDomicilioEmisor)
                Debug.Print("Línea " & k & " ...")
                Debug.Print("Texto       =[" & resp.Descripcion & "]")
                Debug.Print("Centrado    =[" & resp.Atributos.Centrado & "]")
                Debug.Print("Doble Ancho =[" & resp.Atributos.DobleAncho & "]")
                Debug.Print("Negrita     =[" & resp.Atributos.Negrita & "]")
                Debug.Print("Borrado   ? =[" & resp.Atributos.BorradoTexto & "]")
                Debug.Print("")
            Next
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: SERVIDOR DE CORREO - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetCorreo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetCorreo.Click
        Try
            Debug.Print("=====================================")
            Debug.Print("Configuración: SERVIDOR DE CORREO ...")
            Debug.Print("=====================================")

            hasar.ConfigurarServidorCorreo(, , "hasarventas@hasar.com")
            Debug.Print("IP = '' , Puerto = '' , Remitente = hasarventas@hasar.com")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: CONECTIVIDAD EN RED - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetRed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetRed.Click
        Try
            Debug.Print("======================================")
            Debug.Print("Configuración: CONECTIVIDAD EN RED ...")
            Debug.Print("======================================")

            hasar.ConfigurarRed("10.0.7.69", "255.255.255.0", "10.0.7.69")
            Debug.Print("IP = 10.0.7.69 , Máscara = 255.255.255.0 , Gateway = 10.0.7.69")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: BAUDRATE SERIAL - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetBaudios_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetBaudios.Click
        Try
            Debug.Print("==================================")
            Debug.Print("Configuración: BAUDRATE SERIAL ...")
            Debug.Print("==================================")

            hasar.CambiarVelocidadPuerto(HasarArgentina.Baudios.Baudrate9600)
            Debug.Print("Baudrate = 9600")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: FECHA Y HORA - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetFechaHora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetFechaHora.Click
        Try
            Debug.Print("===============================")
            Debug.Print("Configuración: FECHA Y HORA ...")
            Debug.Print("===============================")

            hasar.ConfigurarFechaHora("06/04/2016", "14:50:00")
            Debug.Print("Fecha = 06/04/2016 , Hora = 14:50:00")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: COMPORTAMIENTO IFH - Configuración ...
    '============================================================================================================================
    Private Sub ButtonsETPARAM_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonsETPARAM.Click
        Try
            Debug.Print("=====================================")
            Debug.Print("Configuración: COMPORTAMIENTO IFH ...")
            Debug.Print("=====================================")

            hasar.ConfigurarImpresoraFiscal(HasarArgentina.Configuracion.ImpresionCambio, "P")
            Debug.Print("- Imprimir, CAMBIO $ 0.00")
            hasar.ConfigurarImpresoraFiscal(HasarArgentina.Configuracion.CortePapel, "P")
            Debug.Print("- Corte de papel, PARCIAL")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: LÍMITE BORRADO 'Z' - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetBorrZ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetBorrZ.Click
        Try
            Debug.Print("=====================================")
            Debug.Print("Configuración: LÍMITE BORRADO 'Z' ...")
            Debug.Print("=====================================")

            hasar.ConfigurarZetaBorrable(1)
            Debug.Print("Borrar hasta 'Z' Nº 1")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: LÍNEAS DE USUARIO - Configuración ...
    '============================================================================================================================
    Private Sub ButtonSetZona_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonSetZona.Click
        Dim estilo As HasarArgentina.AtributosDeTexto

        Try
            Debug.Print("====================================")
            Debug.Print("Configuración: LÍNEAS DE USUARIO ...")
            Debug.Print("====================================")

            estilo.BorradoTexto = False
            estilo.Centrado = True
            estilo.DobleAncho = False
            estilo.Negrita = False

            hasar.ConfigurarZona(1, estilo, "Texto encabezado, zona 2, línea 1", HasarArgentina.TiposDeEstacion.EstacionPorDefecto, HasarArgentina.ZonasDeLineasDeUsuario.Zona2Encabezado)
            Debug.Print("Texto encabezado, zona 1, línea 1 ::: Centrado")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ABRIR CAJÓN - Misceláneas ...
    '============================================================================================================================
    Private Sub ButtonCajon_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCajon.Click
        Try
            Debug.Print("============================")
            Debug.Print("Misceláneas: ABRIR CAJÓN ...")
            Debug.Print("============================")

            hasar.AbrirCajonDinero()
            Debug.Print("Cajón abierto ...")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: AVANZAR PAPEL - Misceláneas ...
    '============================================================================================================================
    Private Sub ButtonAvanzarPapel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAvanzarPapel.Click
        Try
            Debug.Print("==============================")
            Debug.Print("Misceláneas: AVANZAR PAPEL ...")
            Debug.Print("==============================")

            hasar.AvanzarPapelEstacionTique(5)
            Debug.Print("Avanzando 5 líneas ...")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: CARGAR LOGO - Misceláneas ...
    '
    ' La imagen (BMP/PNG) debe ser convertida a una lista de comandos soportados por la IFH 2G.
    ' Una forma más amigable de cargar el LOGO, es utilizar un navegador de intenet, y a través de la interfaz HTTP que provee
    ' la IFH 2G.
    '============================================================================================================================
    Private Sub ButtonCargaLogo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCargaLogo.Click
        Try
            Debug.Print("============================")
            Debug.Print("Misceláneas: CARGAR LOGO ...")
            Debug.Print("============================")

            Debug.Print("Comienzo de la carga del LOGO ...")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.ComienzoCargaLogo, "424D9E180000000000003E00000028000000DC010000680000000100010000000000601800000000000000000000000000000000000000000000FFFFFF00FFFF")

            Debug.Print("Carga del LOG en curso ...")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFF00003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFF0000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF800000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFF800000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFF0FFFFFFC000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFF0FFFFFF00000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFF0FFFFFE00000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FE00000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF8000000")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF0000000000001FF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFC0000000000000FFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF800000000000007FFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF800000000000007FFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF000000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC000000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF8000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF8000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF00000000000003C07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF00000000000007F07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFE0000000000000FF81FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFF0FFE0000000000000FF81FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFF0FFC0000000000003FF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFF0FFC0000000000007FE07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF80")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "000000000087FC7F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF8000018000")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "000FFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF8000018000000FFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF0000780000001FFFFE1FFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FF0003000000003FFFFE1FFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE0000007FFFFFFFFFFE0FFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE0000007FFFFFFFFFFE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE004007FFFFFFFFFFFE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FE007CFFFFFFFFFFFFF80FFFFFFCFFFF9FBFFFFC7FFC0FFFFDFFFF80FFFFF873FFFC")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "7FFF1FFFEFFFE00FFFFE00FFE1FE7FC1FEFFFFC07FFFFFFFFFF0FE003FFFFFFFFFFFFFF80FFFFFFCFFFF9FBFFFFC7FFC0FFFFDFFFF80FFFFF873FFFC7FFF1FFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "EFFFE00FFFFE00FFE1FE7FC1FEFFFFC07FFFFFFFFFF0FC003FFFFFFFFFFFFFF807FFFFFCFFFF3FBFFFFCFF80007FFDFFFF0EFFFFF8F3FFF87FFF1FFFEFFE0001")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFE0001FE1FE7FC0FEFFF80007FFFFFFFFF0FC003FFFFFFFFFFFFFF807FFFFFCFFFF3FBFFFFCFF80007FFDFFFF0EFFFFF8F3FFF87FFF1FFFEFFE0001FFE0001F")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "E1FE7FC0FEFFF80007FFFFFFFFF0FC003FFFFFFFFFFF003007FFFFFCFFFF3FBFFFF0FE0FFE1FFDFFFC7E3FFFE0F3FFF87FFF1FFFEFF81FF87F83FF0FE1FE7F80")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FEFFE07FF1FFFFFFFFF0FC000FFFFFFFFFF8000007FFFFFCFFFF3FBFFFF0FE0FFE1FFDFFFC7E3FFFE0F3FFF87FFF1FFFEFF81FF87F83FF0FE1FE7F80FEFFE07F")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "F1FFFFFFFFF0FC000FFFFFFFFFFF000007FFFFFCFFF07FBFFFE3FE7FFF9FFDFFF0FE3FFFE7F3FFE07FFF1FFFEFF8FFFE3F1FFFCFE1FE7F8EFEFFE7FFF9FFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFF0FC000FFFFFFFFFFF000007FFFFFCFFF07FBFFFE3FE7FFF9FFDFFF0FE3FFFE7F3FFE07FFF1FFFEFF8FFFE3F1FFFCFE1FE7F8EFEFFE7FFF9FFFFFFFFF0FC00")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "07FFFFFFFFFFF00007FFFFFCFFE0FFBFFFEFFCFFFFC7FDFFF0FF07FFC7F3FF047FFF1FFFEFF1FFFE3F1FFFEFE1FE7FBE7EFFCFFFFC3FFFFFFFF0FC0000FFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFC007FFFFFCFFE0FFBFFFEFFCFFFFC7FDFFF0FF07FFC7F3FF047FFF1FFFEFF1FFFE3F1FFFEFE1FE7FBE7EFFCFFFFC3FFFFFFFF0F800001FFFFFFFFFFFE0")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "03FFFFFCFFEFFFBFFF8FFDFFFFE7FDFFE3FF000007F3FF1C7FFF1FFFEFF1FFFE3F7FFFEFE1FE7FBE7EFFDFFFFE3FFFFFFFF0F800001FFFFFFFFFFFE003FFFFFC")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFEFFFBFFF8FFDFFFFE7FDFFE3FF000007F3FF1C7FFF1FFFEFF1FFFE3F7FFFEFE1FE7FBE7EFFDFFFFE3FFFFFFFF0F8000001FFFFFFFFFFF003FFFFFCFFCFFFBF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FC1FF1FFFFE0FC3F0FFFC0001FF3FE1C7FFF1FFFEFFFFFFE3FFFFFCFE1FE7E3F7EFF9FFFFE0FFFFFFFF0F80000001FFFFFFFFFF003FFFFFCFFCFFFBFFC1FF1FF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFE0FC3F0FFFC0001FF3FE1C7FFF1FFFEFFFFFFE3FFFFFCFE1FE7E3F7EFF9FFFFE0FFFFFFFF0F800000007FFFFFFFFF063FFFFFC7E1FFF87F07FF1FFFFE0FC0F")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "1FFFC3FF9FF3F8FC7FFF1FFFEFFFFFF07FFFFF0FE1FE7E7F7EFF9FFFFE0FFFFFFFF0FC00000003FFFFFFFFF067FFFFFC7E1FFF87F07FF1FFFFE0FC0F1FFFC3FF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "9FF3F8FC7FFF1FFFEFFFFFF07FFFFF0FE1FE7E7F7EFF9FFFFE0FFFFFFFF0FC00000003FFFFFFFFF067FFFFFC0C3FFF80000FF1FFFFE0FC041FFFC3FFBFF3F8FC")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "7FFF1FFFEFFFFC01FFFF003FE1FE7E7F3EFF9FFFFE0FFFFFFFF0FC00000001FFFFFFFFF867FFFFFC0C3FFF80000FF1FFFFE0FC041FFFC3FFBFF3F8FC7FFF1FFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "EFFFFC01FFFF003FE1FE7E7F3EFF9FFFFE0FFFFFFFF0FC00000000FFFFFFF00007FFFFFC0C3FFF80000FF1FFFFE0FC041FFFC3FFBFF3F8FC7FFF1FFFEFFFFC01")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFF003FE1FE7E7F3EFF9FFFFE0FFFFFFFF0FC000000007FFFFFFC0007FFFFFC803FFF87FF83F1FFFFE0FDF0FFFFF8FE3FF3F1FC7FFF1FFFEFFF000FFFE007FF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "E1FE787F3EFF9FFFFE0FFFFFFFF0FC000000007FFFFFFC0007FFFFFC803FFF87FF83F1FFFFE0FDF0FFFFF8FE3FF3F1FC7FFF1FFFEFFF000FFFE007FFE1FE787F")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "3EFF9FFFFE0FFFFFFFF0FC000000001FFFFFFF0007FFFFFCF1FFFFBFFFE3F1FFFFE0FDF8FFFFF8FE7FF3F7FC7FFF1FFFEFF81FFFFF81FFFFE1FE787FBEFF9FFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FE0FFFFFFFF0FC0000000007FFFFFF8007FFFFFCF1FFFFBFFFE3F1FFFFE0FDF8FFFFF8FE7FF3F7FC7FFF1FFFEFF81FFFFF81FFFFE1FE787FBEFF9FFFFE0FFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFF0FC0000000003FFFFFFC007FFFFFCFC3FFFBFFFF0FDFFFFE7FDFC1FFFFCF07FF3C7FC7FFF1FFFEFF8FFFFFF1FFFFFE1FE79FFBEFFDFFFFE3FFFFFFFF0FC00")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "00000003FFFFFFC007FFFFFCFC3FFFBFFFF0FDFFFFE7FDFC1FFFFCF07FF3C7FC7FFF1FFFEFF8FFFFFF1FFFFFE1FE79FFBEFFDFFFFE3FFFFFFFF0FE0000000001")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFF00FFFFFFCFE1FFFBFFFF0FCFFFFC7FDFC0FFFFC707FF38FFC7FFF1FFFEFF9FFFFFF1FFFFFE1FE71FF82FFCFFFFC3FFFFFFFF0FE0000000000FFFFFFF8")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "0FFFFFFCFE1FFFBFFFF0FCFFFFC7FDFC0FFFFC707FF38FFC7FFF1FFFEFF9FFFFFF1FFFFFE1FE71FF82FFCFFFFC3FFFFFFFF0FE00000000003FFFFFFF0FFFFFFC")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFCFFFBFFFF0FCFFFFC7FDFF03FFFC71FFF00FFC7FFF1FFFEFF9FFF87F1FFFCFE1FE71FFC0FFCFFFFC3FFFFFFFF0FF00000000001FFFFFFF9FFFFFFCFFCFFFBF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFF0FCFFFFC7FDFF03FFFC71FFF00FFC7FFF1FFFEFF9FFF87F1FFFCFE1FE71FFC0FFCFFFFC3FFFFFFFF0FF00000000001FFFFFFF9FFFFFFCFFE0FFBFFFE3FE7F")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FF9FFDFFE0FFFF61FFF03FFC7FFF1FFFEFF8FFF87F9FFF1FE1FE07FFC0FFE7FFF9FFFFFFFFF0FF00000000000FFFFFFFFFFFFFFCFFE0FFBFFFE3FE7FFF9FFDFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "E0FFFF61FFF03FFC7FFF1FFFEFF8FFF87F9FFF1FE1FE07FFC0FFE7FFF9FFFFFFFFF0FF800000000007FFFFFFFFFFFFFCFFF07F800003FF03F03FFDFFF07FFF0F")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFF07FFC7E00000FEFFE0781FF81F81FE1FE07FFC0FFF03F83FFFFFFFFF0FF800000000003FFFFFFFFFFFFFCFFF07F800003FF03F03FFDFFF07FFF0FFFF07FFC")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "7E00000FEFFE0781FF81F81FE1FE07FFC0FFF03F83FFFFFFFFF0FF800000000003FFFFFFFFFFFFFCFFFF3F80001FFF80007FFDFFFC7FFF8FFFF07FFC7E00000F")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "EFFF0007FFE000FFE1FE07FFC0FFF80007FFFFFFFFF0FF800000000000FFFFDFFFFFFFFCFFFF3F80001FFF80007FFDFFFC7FFF8FFFF07FFC7E00000FEFFF0007")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFE000FFE1FE07FFC0FFF80007FFFFFFFFF0FFC000000000007FFFE1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFE000000000001FFFF1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFF0FFE000000000000FFFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFF0FFE000000000000FFFF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFF0FFF0000000000007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFF8")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "000000000003FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC00000000")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC000000000000FFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFC0000000000007FFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF0000000000003FFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF8000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF8000000000001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFF8000000000303FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFC000000000801FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF01000000603EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF01000000603EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFF80100007007F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFF0FFFFFFE001FFE003FF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFF0FFFFFFFE0000001FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFF0FFFFFFFE0000001FFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFE00000FFFFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFF00")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.CargaLogoEnCurso, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")

            hasar.CargarLogoEmisor(HasarArgentina.OperacionesCargaLogoUsuario.FinCargaLogo, "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0")
            Debug.Print("Carga de LOGO finalizada ...")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ELIMINAR LOGO - Misceláneas ...
    '============================================================================================================================
    Private Sub ButtonElimLogo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonElimLogo.Click
        Try
            Debug.Print("==============================")
            Debug.Print("Misceláneas: ELIMINAR LOGO ...")
            Debug.Print("==============================")

            hasar.EliminarLogoEmisor()
            Debug.Print("LOGO eliminado ...")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ENVIAR POR MAIL - Misceláneas ...
    '
    ' Debe estar configurado el servidor de correo. Aplicable, solamente, cuando el servicio de correo no utiliza SSL.
    '============================================================================================================================
    Private Sub ButtonEnviarCorreo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonEnviarCorreo.Click
        Try
            Debug.Print("================================")
            Debug.Print("Misceláneas: ENVIAR POR MAIL ...")
            Debug.Print("================================")

            hasar.EnviarDocumentoCorreo(HasarArgentina.TiposComprobante.Tique, 2, "cliente@suempresa.com")
            Debug.Print("Enviando:  Tique Nº 2 ::: Destino:  cliente@suempresa.com")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: COPIAR DOCUMENTO - Misceláneas ...
    '============================================================================================================================
    Private Sub ButtonCopiarDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCopiarDoc.Click
        Try
            Debug.Print("=================================")
            Debug.Print("Misceláneas: COPIAR DOCUMENTO ...")
            Debug.Print("=================================")

            'hasar.CopiarComprobante(HasarArgentina.TiposComprobante.TiqueFacturaA, 2)
            hasar.CopiarComprobante(HasarArgentina.TiposComprobante.ComprobanteDonacion, 1)

            Debug.Print("Copia de: Tique Factura 'A' Nº 2 ...")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: REIMPRIMIR DOC - Misceláneas ...
    '============================================================================================================================
    Private Sub ButtonReimpr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReimpr.Click
        Try
            Debug.Print("===============================")
            Debug.Print("Misceláneas: REIMPRIMIR DOC ...")
            Debug.Print("===============================")

            hasar.PedirReimpresion()
            Debug.Print("Comprobante reimpreso ...")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ACUM. COMPROBANTE - Acumulados/Reportes ...
    '============================================================================================================================
    Private Sub ButtonAcumComprob_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAcumComprob.Click
        Dim comprob1 As HasarArgentina.RespuestaConsultarAcumuladosComprobante
        Dim comprob2 As HasarArgentina.RespuestaContinuarConsultaAcumulados

        Try
            Debug.Print("==========================================")
            Debug.Print("Acumulados/Reportes: ACUM. COMPROBANTE ...")
            Debug.Print("==========================================")

            Debug.Print("Obteniendo primer registro de información ...")
            Debug.Print("Acumulados de:  Tique Factura B, Nº 3 ...")
            Debug.Print("")
            comprob1 = hasar.ConsultarAcumuladosComprobante(HasarArgentina.TiposComprobante.TiqueFacturaB, 3)

            If (comprob1.Registro = HasarArgentina.TiposDeRegistroInforme.RegistroFinal) Then
                Debug.Print("No hay información disponible ...")
                Debug.Print("")
                Exit Sub
            End If

            Select Case comprob1.Registro
                Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
                    Debug.Print("(DF) Documento Fiscal ...")
                    Debug.Print("-------------------------")
                    Debug.Print("Cód. Comprob.     =[" & comprob1.RegDF.CodigoComprobante & "]")
                    Debug.Print("Desde Nº          =[" & comprob1.RegDF.NumeroInicial & "]")
                    Debug.Print("Hasta Nº          =[" & comprob1.RegDF.NumeroFinal & "]")
                    Debug.Print("Cancelados        =[" & comprob1.RegDF.CantidadCancelados & "]")
                    Debug.Print("")
                    Debug.Print("TOTAL             =[" & comprob1.RegDF.Total & "]")
                    Debug.Print("Total Gravado     =[" & comprob1.RegDF.TotalGravado & "]")
                    Debug.Print("Total No Gravado  =[" & comprob1.RegDF.TotalNoGravado & "]")
                    Debug.Print("Total Exento      =[" & comprob1.RegDF.TotalExento & "]")
                    Debug.Print("Total IVA         =[" & comprob1.RegDF.TotalIVA & "]")
                    Debug.Print("Total Otros Trib. =[" & comprob1.RegDF.TotalTributos & "]")
                    Debug.Print("")
                    Debug.Print("Monto Base 1      =[" & comprob1.RegDF.MontoNetoSinIVA_1 & "]")
                    Debug.Print("Alícuota 1        =[" & comprob1.RegDF.AlicuotaIVA_1 & "]")
                    Debug.Print("Monto IVA 1       =[" & comprob1.RegDF.MontoIVA_1 & "]")
                    Debug.Print("Monto Base 2      =[" & comprob1.RegDF.MontoNetoSinIVA_2 & "]")
                    Debug.Print("Alícuota 2        =[" & comprob1.RegDF.AlicuotaIVA_2 & "]")
                    Debug.Print("Monto IVA 2       =[" & comprob1.RegDF.MontoIVA_2 & "]")
                    Debug.Print("Monto Base 3      =[" & comprob1.RegDF.MontoNetoSinIVA_3 & "]")
                    Debug.Print("Alícuota 3        =[" & comprob1.RegDF.AlicuotaIVA_3 & "]")
                    Debug.Print("Monto IVA 3       =[" & comprob1.RegDF.MontoIVA_3 & "]")
                    Debug.Print("Monto Base 4      =[" & comprob1.RegDF.MontoNetoSinIVA_4 & "]")
                    Debug.Print("Alícuota 4        =[" & comprob1.RegDF.AlicuotaIVA_4 & "]")
                    Debug.Print("Monto IVA 4       =[" & comprob1.RegDF.MontoIVA_4 & "]")
                    Debug.Print("Monto Base 5      =[" & comprob1.RegDF.MontoNetoSinIVA_5 & "]")
                    Debug.Print("Alícuota 5        =[" & comprob1.RegDF.AlicuotaIVA_5 & "]")
                    Debug.Print("Monto IVA 5       =[" & comprob1.RegDF.MontoIVA_5 & "]")
                    Debug.Print("Monto Base 6      =[" & comprob1.RegDF.MontoNetoSinIVA_6 & "]")
                    Debug.Print("Alícuota 6        =[" & comprob1.RegDF.AlicuotaIVA_6 & "]")
                    Debug.Print("Monto IVA 6       =[" & comprob1.RegDF.MontoIVA_6 & "]")
                    Debug.Print("Monto Base 7      =[" & comprob1.RegDF.MontoNetoSinIVA_7 & "]")
                    Debug.Print("Alícuota 7        =[" & comprob1.RegDF.AlicuotaIVA_7 & "]")
                    Debug.Print("Monto IVA 7       =[" & comprob1.RegDF.MontoIVA_7 & "]")
                    Debug.Print("Monto Base 8      =[" & comprob1.RegDF.MontoNetoSinIVA_8 & "]")
                    Debug.Print("Alícuota 8        =[" & comprob1.RegDF.AlicuotaIVA_8 & "]")
                    Debug.Print("Monto IVA 8       =[" & comprob1.RegDF.MontoIVA_8 & "]")
                    Debug.Print("Monto Base 9      =[" & comprob1.RegDF.MontoNetoSinIVA_9 & "]")
                    Debug.Print("Alícuota 9        =[" & comprob1.RegDF.AlicuotaIVA_9 & "]")
                    Debug.Print("Monto IVA 9       =[" & comprob1.RegDF.MontoIVA_9 & "]")
                    Debug.Print("Monto Base 10     =[" & comprob1.RegDF.MontoNetoSinIVA_10 & "]")
                    Debug.Print("Alícuota 10       =[" & comprob1.RegDF.AlicuotaIVA_10 & "]")
                    Debug.Print("Monto IVA 10      =[" & comprob1.RegDF.MontoIVA_10 & "]")
                    Debug.Print("")
                    Debug.Print("Cód. Tributo 1    =[" & comprob1.RegDF.CodigoTributo1 & "]")
                    Debug.Print("Importe 1         =[" & comprob1.RegDF.ImporteTributo1 & "]")
                    Debug.Print("Cód. Tributo 2    =[" & comprob1.RegDF.CodigoTributo2 & "]")
                    Debug.Print("Importe 2         =[" & comprob1.RegDF.ImporteTributo2 & "]")
                    Debug.Print("Cód. Tributo 3    =[" & comprob1.RegDF.CodigoTributo3 & "]")
                    Debug.Print("Importe 3         =[" & comprob1.RegDF.ImporteTributo3 & "]")
                    Debug.Print("Cód. Tributo 4    =[" & comprob1.RegDF.CodigoTributo4 & "]")
                    Debug.Print("Importe 4         =[" & comprob1.RegDF.ImporteTributo4 & "]")
                    Debug.Print("Cód. Tributo 5    =[" & comprob1.RegDF.CodigoTributo5 & "]")
                    Debug.Print("Importe 5         =[" & comprob1.RegDF.ImporteTributo5 & "]")
                    Debug.Print("Cód. Tributo 6    =[" & comprob1.RegDF.CodigoTributo6 & "]")
                    Debug.Print("Impoorte 6        =[" & comprob1.RegDF.ImporteTributo6 & "]")
                    Debug.Print("Cód. Tributo 7    =[" & comprob1.RegDF.CodigoTributo7 & "]")
                    Debug.Print("Importe 7         =[" & comprob1.RegDF.ImporteTributo7 & "]")
                    Debug.Print("Cód. Tributo 8    =[" & comprob1.RegDF.CodigoTributo8 & "]")
                    Debug.Print("Importe 8         =[" & comprob1.RegDF.ImporteTributo8 & "]")
                    Debug.Print("Cód. Tributo 9    =[" & comprob1.RegDF.CodigoTributo9 & "]")
                    Debug.Print("Importe 9         =[" & comprob1.RegDF.ImporteTributo9 & "]")
                    Debug.Print("Cód. Tributo 10   =[" & comprob1.RegDF.CodigoTributo10 & "]")
                    Debug.Print("Importe 10        =[" & comprob1.RegDF.ImporteTributo10 & "]")
                    Debug.Print("Cód. Tributo 11   =[" & comprob1.RegDF.CodigoTributo11 & "]")
                    Debug.Print("Importe 11        =[" & comprob1.RegDF.ImporteTributo11 & "]")
                    Debug.Print("")
                Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
                    Debug.Print("(DNFH) Documento NO Fiscal Homologado (que SI acumula) ...")
                    Debug.Print("----------------------------------------------------------")
                    Debug.Print("Cód. Comprob. =[" & comprob1.RegDNFH.CodigoComprobante & "]")
                    Debug.Print("Desde Nº      =[" & comprob1.RegDNFH.NumeroInicial & "]")
                    Debug.Print("Hasta Nº      =[" & comprob1.RegDNFH.NumeroFinal & "]")
                    Debug.Print("")
                    Debug.Print("TOTAL         =[" & comprob1.RegDNFH.Total & "]")
                    Debug.Print("")
                Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
                    Debug.Print("(DNFH) Documento NO Fiscal Homologado (que NO acumula) ...")
                    Debug.Print("----------------------------------------------------------")
                    Debug.Print("Cód. Comprob. =[" & comprob1.RegDNFH_NoAcum.CodigoComprobante & "]")
                    Debug.Print("Desde Nº      =[" & comprob1.RegDNFH_NoAcum.NumeroInicial & "]")
                    Debug.Print("Hasta Nº      =[" & comprob1.RegDNFH_NoAcum.NumeroFinal & "]")
                    Debug.Print("")
                Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
                    Debug.Print("Información Global ...")
                    Debug.Print("----------------------")
                    Debug.Print("DF Emitidos         =[" & comprob1.RegGlobal.DF_CantidadEmitidos & "]")
                    Debug.Print("DF Cancelados       =[" & comprob1.RegGlobal.DF_CantidadCancelados & "]")
                    Debug.Print("DF TOTAL            =[" & comprob1.RegGlobal.DF_Total & "]")
                    Debug.Print("DF Total Gravado    =[" & comprob1.RegGlobal.DF_TotalGravado & "]")
                    Debug.Print("DF Total No Gravado =[" & comprob1.RegGlobal.DF_TotalNoGravado & "]")
                    Debug.Print("DF Total Exento     =[" & comprob1.RegGlobal.DF_TotalExento & "]")
                    Debug.Print("DF Total IVA        =[" & comprob1.RegGlobal.DF_TotalIVA & "]")
                    Debug.Print("DF Total Tributos   =[" & comprob1.RegGlobal.DF_TotalTributos & "]")
                    Debug.Print("")
                    Debug.Print("DNFH TOTAL          =[" & comprob1.RegGlobal.DNFH_Total & "]")
                    Debug.Print("")
                Case Else
                    Debug.Print("Primer registro de información: DESCONOCIDO ! ...")
                    Debug.Print("")
            End Select

            comprob2 = hasar.ContinuarConsultaAcumulados

            While (comprob2.Registro <> HasarArgentina.TiposDeRegistroInforme.RegistroFinal)
                Select Case comprob2.Registro
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
                        Debug.Print("(DF) Documento Fiscal ...")
                        Debug.Print("-------------------------")
                        Debug.Print("Cód. Comprob.     =[" & comprob2.RegDF.CodigoComprobante & "]")
                        Debug.Print("Desde Nº          =[" & comprob2.RegDF.NumeroInicial & "]")
                        Debug.Print("Hasta Nº          =[" & comprob2.RegDF.NumeroFinal & "]")
                        Debug.Print("Cancelados        =[" & comprob2.RegDF.CantidadCancelados & "]")
                        Debug.Print("")
                        Debug.Print("TOTAL             =[" & comprob2.RegDF.Total & "]")
                        Debug.Print("Total Gravado     =[" & comprob2.RegDF.TotalGravado & "]")
                        Debug.Print("Total No Gravado  =[" & comprob2.RegDF.TotalNoGravado & "]")
                        Debug.Print("Total Exento      =[" & comprob2.RegDF.TotalExento & "]")
                        Debug.Print("Total IVA         =[" & comprob2.RegDF.TotalIVA & "]")
                        Debug.Print("Total Otros Trib. =[" & comprob2.RegDF.TotalTributos & "]")
                        Debug.Print("")
                        Debug.Print("Alícuota 1        =[" & comprob2.RegDF.AlicuotaIVA_1 & "]")
                        Debug.Print("Monto IVA 1       =[" & comprob2.RegDF.MontoIVA_1 & "]")
                        Debug.Print("Alícuota 2        =[" & comprob2.RegDF.AlicuotaIVA_2 & "]")
                        Debug.Print("Monto IVA 2       =[" & comprob2.RegDF.MontoIVA_2 & "]")
                        Debug.Print("Alícuota 3        =[" & comprob2.RegDF.AlicuotaIVA_3 & "]")
                        Debug.Print("Monto IVA 3       =[" & comprob2.RegDF.MontoIVA_3 & "]")
                        Debug.Print("Alícuota 4        =[" & comprob2.RegDF.AlicuotaIVA_4 & "]")
                        Debug.Print("Monto IVA 4       =[" & comprob2.RegDF.MontoIVA_4 & "]")
                        Debug.Print("Alícuota 5        =[" & comprob2.RegDF.AlicuotaIVA_5 & "]")
                        Debug.Print("Monto IVA 5       =[" & comprob2.RegDF.MontoIVA_5 & "]")
                        Debug.Print("Alícuota 6        =[" & comprob2.RegDF.AlicuotaIVA_6 & "]")
                        Debug.Print("Monto IVA 6       =[" & comprob2.RegDF.MontoIVA_6 & "]")
                        Debug.Print("Alícuota 7        =[" & comprob2.RegDF.AlicuotaIVA_7 & "]")
                        Debug.Print("Monto IVA 7       =[" & comprob2.RegDF.MontoIVA_7 & "]")
                        Debug.Print("Alícuota 8        =[" & comprob2.RegDF.AlicuotaIVA_8 & "]")
                        Debug.Print("Monto IVA 8       =[" & comprob2.RegDF.MontoIVA_8 & "]")
                        Debug.Print("Alícuota 9        =[" & comprob2.RegDF.AlicuotaIVA_9 & "]")
                        Debug.Print("Monto IVA 9       =[" & comprob2.RegDF.MontoIVA_9 & "]")
                        Debug.Print("Alícuota 10       =[" & comprob2.RegDF.AlicuotaIVA_10 & "]")
                        Debug.Print("Monto IVA 10      =[" & comprob2.RegDF.MontoIVA_10 & "]")
                        Debug.Print("")
                        Debug.Print("Cód. Tributo 1    =[" & comprob2.RegDF.CodigoTributo1 & "]")
                        Debug.Print("Importe 1         =[" & comprob2.RegDF.ImporteTributo1 & "]")
                        Debug.Print("Cód. Tributo 2    =[" & comprob2.RegDF.CodigoTributo2 & "]")
                        Debug.Print("Importe 2         =[" & comprob2.RegDF.ImporteTributo2 & "]")
                        Debug.Print("Cód. Tributo 3    =[" & comprob2.RegDF.CodigoTributo3 & "]")
                        Debug.Print("Importe 3         =[" & comprob2.RegDF.ImporteTributo3 & "]")
                        Debug.Print("Cód. Tributo 4    =[" & comprob2.RegDF.CodigoTributo4 & "]")
                        Debug.Print("Importe 4         =[" & comprob2.RegDF.ImporteTributo4 & "]")
                        Debug.Print("Cód. Tributo 5    =[" & comprob2.RegDF.CodigoTributo5 & "]")
                        Debug.Print("Importe 5         =[" & comprob2.RegDF.ImporteTributo5 & "]")
                        Debug.Print("Cód. Tributo 6    =[" & comprob2.RegDF.CodigoTributo6 & "]")
                        Debug.Print("Impoorte 6        =[" & comprob2.RegDF.ImporteTributo6 & "]")
                        Debug.Print("Cód. Tributo 7    =[" & comprob2.RegDF.CodigoTributo7 & "]")
                        Debug.Print("Importe 7         =[" & comprob2.RegDF.ImporteTributo7 & "]")
                        Debug.Print("Cód. Tributo 8    =[" & comprob2.RegDF.CodigoTributo8 & "]")
                        Debug.Print("Importe 8         =[" & comprob2.RegDF.ImporteTributo8 & "]")
                        Debug.Print("Cód. Tributo 9    =[" & comprob2.RegDF.CodigoTributo9 & "]")
                        Debug.Print("Importe 9         =[" & comprob2.RegDF.ImporteTributo9 & "]")
                        Debug.Print("Cód. Tributo 10   =[" & comprob2.RegDF.CodigoTributo10 & "]")
                        Debug.Print("Importe 10        =[" & comprob2.RegDF.ImporteTributo10 & "]")
                        Debug.Print("Cód. Tributo 11   =[" & comprob2.RegDF.CodigoTributo11 & "]")
                        Debug.Print("Importe 11        =[" & comprob2.RegDF.ImporteTributo11 & "]")
                        Debug.Print("")
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
                        Debug.Print("(DNFH) Documento NO Fiscal Homologado (que SI acumula) ...")
                        Debug.Print("----------------------------------------------------------")
                        Debug.Print("Cód. Comprob. =[" & comprob2.RegDNFH.CodigoComprobante & "]")
                        Debug.Print("Desde Nº      =[" & comprob2.RegDNFH.NumeroInicial & "]")
                        Debug.Print("Hasta Nº      =[" & comprob2.RegDNFH.NumeroFinal & "]")
                        Debug.Print("")
                        Debug.Print("TOTAL         =[" & comprob2.RegDNFH.Total & "]")
                        Debug.Print("")
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
                        Debug.Print("(DNFH) Documento NO Fiscal Homologado (que NO acumula) ...")
                        Debug.Print("----------------------------------------------------------")
                        Debug.Print("Cód. Comprob. =[" & comprob2.RegDNFH_NoAcum.CodigoComprobante & "]")
                        Debug.Print("Desde Nº      =[" & comprob2.RegDNFH_NoAcum.NumeroInicial & "]")
                        Debug.Print("Hasta Nº      =[" & comprob2.RegDNFH_NoAcum.NumeroFinal & "]")
                        Debug.Print("")
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
                        Debug.Print("Información Global ...")
                        Debug.Print("----------------------")
                        Debug.Print("DF Emitidos         =[" & comprob2.RegGlobal.DF_CantidadEmitidos & "]")
                        Debug.Print("DF Cancelados       =[" & comprob2.RegGlobal.DF_CantidadCancelados & "]")
                        Debug.Print("DF TOTAL            =[" & comprob2.RegGlobal.DF_Total & "]")
                        Debug.Print("DF Total Gravado    =[" & comprob2.RegGlobal.DF_TotalGravado & "]")
                        Debug.Print("DF Total No Gravado =[" & comprob2.RegGlobal.DF_TotalNoGravado & "]")
                        Debug.Print("DF Total Exento     =[" & comprob2.RegGlobal.DF_TotalExento & "]")
                        Debug.Print("DF Total IVA        =[" & comprob2.RegGlobal.DF_TotalIVA & "]")
                        Debug.Print("DF Total Tributos   =[" & comprob2.RegGlobal.DF_TotalTributos & "]")
                        Debug.Print("")
                        Debug.Print("DNFH TOTAL          =[" & comprob2.RegGlobal.DNFH_Total & "]")
                        Debug.Print("")
                    Case Else
                        Debug.Print("Primer registro de información: DESCONOCIDO ! ...")
                        Debug.Print("")
                End Select

                comprob2 = hasar.ContinuarConsultaAcumulados
            End While
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ACUM. MEM. de TRABAJO - Acumulados/Reportes ...
    '============================================================================================================================
    Private Sub ButtonAcumMemTrab_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAcumMemTrab.Click
        Dim memo1 As HasarArgentina.RespuestaConsultarAcumuladosMemoriaDeTrabajo
        Dim memo2 As HasarArgentina.RespuestaContinuarConsultaAcumulados

        Try
            Debug.Print("===========================================")
            Debug.Print("Acumulados/Reportes: ACUM. MEM. TRABAJO ...")
            Debug.Print("===========================================")

            Debug.Print("Obteniendo primer registro de información ...")
            Debug.Print("")
            memo1 = hasar.ConsultarAcumuladosMemoriaDeTrabajo(HasarArgentina.TiposComprobante.NoDocumento)

            If (memo1.Registro = HasarArgentina.TiposDeRegistroInforme.RegistroFinal) Then
                Debug.Print("No hay información disponible ...")
                Debug.Print("")
                Exit Sub
            End If

            Select Case memo1.Registro
                Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
                    Debug.Print("(DF) Documento Fiscal ...")
                    Debug.Print("-------------------------")
                    Debug.Print("Cód. Comprob.     =[" & memo1.RegDF.CodigoComprobante & "]")
                    Debug.Print("Desde Nº          =[" & memo1.RegDF.NumeroInicial & "]")
                    Debug.Print("Hasta Nº          =[" & memo1.RegDF.NumeroFinal & "]")
                    Debug.Print("Cancelados        =[" & memo1.RegDF.CantidadCancelados & "]")
                    Debug.Print("")
                    Debug.Print("TOTAL             =[" & memo1.RegDF.Total & "]")
                    Debug.Print("Total Gravado     =[" & memo1.RegDF.TotalGravado & "]")
                    Debug.Print("Total No Gravado  =[" & memo1.RegDF.TotalNoGravado & "]")
                    Debug.Print("Total Exento      =[" & memo1.RegDF.TotalExento & "]")
                    Debug.Print("Total IVA         =[" & memo1.RegDF.TotalIVA & "]")
                    Debug.Print("Total Otros Trib. =[" & memo1.RegDF.TotalTributos & "]")
                    Debug.Print("")
                    Debug.Print("Alícuota 1        =[" & memo1.RegDF.AlicuotaIVA_1 & "]")
                    Debug.Print("Monto IVA 1       =[" & memo1.RegDF.MontoIVA_1 & "]")
                    Debug.Print("Alícuota 2        =[" & memo1.RegDF.AlicuotaIVA_2 & "]")
                    Debug.Print("Monto IVA 2       =[" & memo1.RegDF.MontoIVA_2 & "]")
                    Debug.Print("Alícuota 3        =[" & memo1.RegDF.AlicuotaIVA_3 & "]")
                    Debug.Print("Monto IVA 3       =[" & memo1.RegDF.MontoIVA_3 & "]")
                    Debug.Print("Alícuota 4        =[" & memo1.RegDF.AlicuotaIVA_4 & "]")
                    Debug.Print("Monto IVA 4       =[" & memo1.RegDF.MontoIVA_4 & "]")
                    Debug.Print("Alícuota 5        =[" & memo1.RegDF.AlicuotaIVA_5 & "]")
                    Debug.Print("Monto IVA 5       =[" & memo1.RegDF.MontoIVA_5 & "]")
                    Debug.Print("Alícuota 6        =[" & memo1.RegDF.AlicuotaIVA_6 & "]")
                    Debug.Print("Monto IVA 6       =[" & memo1.RegDF.MontoIVA_6 & "]")
                    Debug.Print("Alícuota 7        =[" & memo1.RegDF.AlicuotaIVA_7 & "]")
                    Debug.Print("Monto IVA 7       =[" & memo1.RegDF.MontoIVA_7 & "]")
                    Debug.Print("Alícuota 8        =[" & memo1.RegDF.AlicuotaIVA_8 & "]")
                    Debug.Print("Monto IVA 8       =[" & memo1.RegDF.MontoIVA_8 & "]")
                    Debug.Print("Alícuota 9        =[" & memo1.RegDF.AlicuotaIVA_9 & "]")
                    Debug.Print("Monto IVA 9       =[" & memo1.RegDF.MontoIVA_9 & "]")

                    Debug.Print("Alícuota 10       =[" & memo1.RegDF.AlicuotaIVA_10 & "]")
                    Debug.Print("Monto IVA 10      =[" & memo1.RegDF.MontoIVA_10 & "]")
                    Debug.Print("")
                    Debug.Print("Cód. Tributo 1    =[" & memo1.RegDF.CodigoTributo1 & "]")
                    Debug.Print("Importe 1         =[" & memo1.RegDF.ImporteTributo1 & "]")
                    Debug.Print("Cód. Tributo 2    =[" & memo1.RegDF.CodigoTributo2 & "]")
                    Debug.Print("Importe 2         =[" & memo1.RegDF.ImporteTributo2 & "]")
                    Debug.Print("Cód. Tributo 3    =[" & memo1.RegDF.CodigoTributo3 & "]")
                    Debug.Print("Importe 3         =[" & memo1.RegDF.ImporteTributo3 & "]")
                    Debug.Print("Cód. Tributo 4    =[" & memo1.RegDF.CodigoTributo4 & "]")
                    Debug.Print("Importe 4         =[" & memo1.RegDF.ImporteTributo4 & "]")
                    Debug.Print("Cód. Tributo 5    =[" & memo1.RegDF.CodigoTributo5 & "]")
                    Debug.Print("Importe 5         =[" & memo1.RegDF.ImporteTributo5 & "]")
                    Debug.Print("Cód. Tributo 6    =[" & memo1.RegDF.CodigoTributo6 & "]")
                    Debug.Print("Impoorte 6        =[" & memo1.RegDF.ImporteTributo6 & "]")
                    Debug.Print("Cód. Tributo 7    =[" & memo1.RegDF.CodigoTributo7 & "]")
                    Debug.Print("Importe 7         =[" & memo1.RegDF.ImporteTributo7 & "]")
                    Debug.Print("Cód. Tributo 8    =[" & memo1.RegDF.CodigoTributo8 & "]")
                    Debug.Print("Importe 8         =[" & memo1.RegDF.ImporteTributo8 & "]")
                    Debug.Print("Cód. Tributo 9    =[" & memo1.RegDF.CodigoTributo9 & "]")
                    Debug.Print("Importe 9         =[" & memo1.RegDF.ImporteTributo9 & "]")
                    Debug.Print("Cód. Tributo 10   =[" & memo1.RegDF.CodigoTributo10 & "]")
                    Debug.Print("Importe 10        =[" & memo1.RegDF.ImporteTributo10 & "]")
                    Debug.Print("Cód. Tributo 11   =[" & memo1.RegDF.CodigoTributo11 & "]")
                    Debug.Print("Importe 11        =[" & memo1.RegDF.ImporteTributo11 & "]")
                    Debug.Print("")
                Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
                    Debug.Print("(DNFH) Documento NO Fiscal Homologado (que SI acumula) ...")
                    Debug.Print("----------------------------------------------------------")
                    Debug.Print("Cód. Comprob. =[" & memo1.RegDNFH.CodigoComprobante & "]")
                    Debug.Print("Desde Nº      =[" & memo1.RegDNFH.NumeroInicial & "]")
                    Debug.Print("Hasta Nº      =[" & memo1.RegDNFH.NumeroFinal & "]")
                    Debug.Print("")
                    Debug.Print("TOTAL         =[" & memo1.RegDNFH.Total & "]")
                    Debug.Print("")
                Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
                    Debug.Print("(DNFH) Documento NO Fiscal Homologado (que NO acumula) ...")
                    Debug.Print("----------------------------------------------------------")
                    Debug.Print("Cód. Comprob. =[" & memo1.RegDNFH_NoAcum.CodigoComprobante & "]")
                    Debug.Print("Desde Nº      =[" & memo1.RegDNFH_NoAcum.NumeroInicial & "]")
                    Debug.Print("Hasta Nº      =[" & memo1.RegDNFH_NoAcum.NumeroFinal & "]")
                    Debug.Print("")
                Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
                    Debug.Print("Información Global ...")
                    Debug.Print("----------------------")
                    Debug.Print("DF Emitidos         =[" & memo1.RegGlobal.DF_CantidadEmitidos & "]")
                    Debug.Print("DF Cancelados       =[" & memo1.RegGlobal.DF_CantidadCancelados & "]")
                    Debug.Print("DF TOTAL            =[" & memo1.RegGlobal.DF_Total & "]")
                    Debug.Print("DF Total Gravado    =[" & memo1.RegGlobal.DF_TotalGravado & "]")
                    Debug.Print("DF Total No Gravado =[" & memo1.RegGlobal.DF_TotalNoGravado & "]")
                    Debug.Print("DF Total Exento     =[" & memo1.RegGlobal.DF_TotalExento & "]")
                    Debug.Print("DF Total IVA        =[" & memo1.RegGlobal.DF_TotalIVA & "]")
                    Debug.Print("DF Total Tributos   =[" & memo1.RegGlobal.DF_TotalTributos & "]")
                    Debug.Print("")
                    Debug.Print("DNFH TOTAL          =[" & memo1.RegGlobal.DNFH_Total & "]")
                    Debug.Print("")
                Case Else
                    Debug.Print("Primer registro de información: DESCONOCIDO ! ...")
                    Debug.Print("")
            End Select

            memo2 = hasar.ContinuarConsultaAcumulados

            While (memo2.Registro <> HasarArgentina.TiposDeRegistroInforme.RegistroFinal)
                Select Case memo2.Registro
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
                        Debug.Print("(DF) Documento Fiscal ...")
                        Debug.Print("-------------------------")
                        Debug.Print("Cód. Comprob.     =[" & memo2.RegDF.CodigoComprobante & "]")
                        Debug.Print("Desde Nº          =[" & memo2.RegDF.NumeroInicial & "]")
                        Debug.Print("Hasta Nº          =[" & memo2.RegDF.NumeroFinal & "]")
                        Debug.Print("Cancelados        =[" & memo2.RegDF.CantidadCancelados & "]")
                        Debug.Print("")
                        Debug.Print("TOTAL             =[" & memo2.RegDF.Total & "]")
                        Debug.Print("Total Gravado     =[" & memo2.RegDF.TotalGravado & "]")
                        Debug.Print("Total No Gravado  =[" & memo2.RegDF.TotalNoGravado & "]")
                        Debug.Print("Total Exento      =[" & memo2.RegDF.TotalExento & "]")
                        Debug.Print("Total IVA         =[" & memo2.RegDF.TotalIVA & "]")
                        Debug.Print("Total Otros Trib. =[" & memo2.RegDF.TotalTributos & "]")
                        Debug.Print("")
                        Debug.Print("Alícuota 1        =[" & memo2.RegDF.AlicuotaIVA_1 & "]")
                        Debug.Print("Monto IVA 1       =[" & memo2.RegDF.MontoIVA_1 & "]")
                        Debug.Print("Alícuota 2        =[" & memo2.RegDF.AlicuotaIVA_2 & "]")
                        Debug.Print("Monto IVA 2       =[" & memo2.RegDF.MontoIVA_2 & "]")
                        Debug.Print("Alícuota 3        =[" & memo2.RegDF.AlicuotaIVA_3 & "]")
                        Debug.Print("Monto IVA 3       =[" & memo2.RegDF.MontoIVA_3 & "]")
                        Debug.Print("Alícuota 4        =[" & memo2.RegDF.AlicuotaIVA_4 & "]")
                        Debug.Print("Monto IVA 4       =[" & memo2.RegDF.MontoIVA_4 & "]")
                        Debug.Print("Alícuota 5        =[" & memo2.RegDF.AlicuotaIVA_5 & "]")
                        Debug.Print("Monto IVA 5       =[" & memo2.RegDF.MontoIVA_5 & "]")
                        Debug.Print("Alícuota 6        =[" & memo2.RegDF.AlicuotaIVA_6 & "]")
                        Debug.Print("Monto IVA 6       =[" & memo2.RegDF.MontoIVA_6 & "]")
                        Debug.Print("Alícuota 7        =[" & memo2.RegDF.AlicuotaIVA_7 & "]")
                        Debug.Print("Monto IVA 7       =[" & memo2.RegDF.MontoIVA_7 & "]")
                        Debug.Print("Alícuota 8        =[" & memo2.RegDF.AlicuotaIVA_8 & "]")
                        Debug.Print("Monto IVA 8       =[" & memo2.RegDF.MontoIVA_8 & "]")
                        Debug.Print("Alícuota 9        =[" & memo2.RegDF.AlicuotaIVA_9 & "]")
                        Debug.Print("Monto IVA 9       =[" & memo2.RegDF.MontoIVA_9 & "]")
                        Debug.Print("Alícuota 10       =[" & memo2.RegDF.AlicuotaIVA_10 & "]")
                        Debug.Print("Monto IVA 10      =[" & memo2.RegDF.MontoIVA_10 & "]")
                        Debug.Print("")
                        Debug.Print("Cód. Tributo 1    =[" & memo2.RegDF.CodigoTributo1 & "]")
                        Debug.Print("Importe 1         =[" & memo2.RegDF.ImporteTributo1 & "]")
                        Debug.Print("Cód. Tributo 2    =[" & memo2.RegDF.CodigoTributo2 & "]")
                        Debug.Print("Importe 2         =[" & memo2.RegDF.ImporteTributo2 & "]")
                        Debug.Print("Cód. Tributo 3    =[" & memo2.RegDF.CodigoTributo3 & "]")
                        Debug.Print("Importe 3         =[" & memo2.RegDF.ImporteTributo3 & "]")
                        Debug.Print("Cód. Tributo 4    =[" & memo2.RegDF.CodigoTributo4 & "]")
                        Debug.Print("Importe 4         =[" & memo2.RegDF.ImporteTributo4 & "]")
                        Debug.Print("Cód. Tributo 5    =[" & memo2.RegDF.CodigoTributo5 & "]")
                        Debug.Print("Importe 5         =[" & memo2.RegDF.ImporteTributo5 & "]")
                        Debug.Print("Cód. Tributo 6    =[" & memo2.RegDF.CodigoTributo6 & "]")
                        Debug.Print("Impoorte 6        =[" & memo2.RegDF.ImporteTributo6 & "]")
                        Debug.Print("Cód. Tributo 7    =[" & memo2.RegDF.CodigoTributo7 & "]")
                        Debug.Print("Importe 7         =[" & memo2.RegDF.ImporteTributo7 & "]")
                        Debug.Print("Cód. Tributo 8    =[" & memo2.RegDF.CodigoTributo8 & "]")
                        Debug.Print("Importe 8         =[" & memo2.RegDF.ImporteTributo8 & "]")
                        Debug.Print("Cód. Tributo 9    =[" & memo2.RegDF.CodigoTributo9 & "]")
                        Debug.Print("Importe 9         =[" & memo2.RegDF.ImporteTributo9 & "]")
                        Debug.Print("Cód. Tributo 10   =[" & memo2.RegDF.CodigoTributo10 & "]")
                        Debug.Print("Importe 10        =[" & memo2.RegDF.ImporteTributo10 & "]")
                        Debug.Print("Cód. Tributo 11   =[" & memo2.RegDF.CodigoTributo11 & "]")
                        Debug.Print("Importe 11        =[" & memo2.RegDF.ImporteTributo11 & "]")
                        Debug.Print("")
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
                        Debug.Print("(DNFH) Documento NO Fiscal Homologado (que SI acumula) ...")
                        Debug.Print("----------------------------------------------------------")
                        Debug.Print("Cód. Comprob. =[" & memo2.RegDNFH.CodigoComprobante & "]")
                        Debug.Print("Desde Nº      =[" & memo2.RegDNFH.NumeroInicial & "]")
                        Debug.Print("Hasta Nº      =[" & memo2.RegDNFH.NumeroFinal & "]")
                        Debug.Print("")
                        Debug.Print("TOTAL         =[" & memo2.RegDNFH.Total & "]")
                        Debug.Print("")
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
                        Debug.Print("(DNFH) Documento NO Fiscal Homologado (que NO acumula) ...")
                        Debug.Print("----------------------------------------------------------")
                        Debug.Print("Cód. Comprob. =[" & memo2.RegDNFH_NoAcum.CodigoComprobante & "]")
                        Debug.Print("Desde Nº      =[" & memo2.RegDNFH_NoAcum.NumeroInicial & "]")
                        Debug.Print("Hasta Nº      =[" & memo2.RegDNFH_NoAcum.NumeroFinal & "]")
                        Debug.Print("")
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
                        Debug.Print("Información Global ...")
                        Debug.Print("----------------------")
                        Debug.Print("DF Emitidos         =[" & memo2.RegGlobal.DF_CantidadEmitidos & "]")
                        Debug.Print("DF Cancelados       =[" & memo2.RegGlobal.DF_CantidadCancelados & "]")
                        Debug.Print("DF TOTAL            =[" & memo2.RegGlobal.DF_Total & "]")
                        Debug.Print("DF Total Gravado    =[" & memo2.RegGlobal.DF_TotalGravado & "]")
                        Debug.Print("DF Total No Gravado =[" & memo2.RegGlobal.DF_TotalNoGravado & "]")
                        Debug.Print("DF Total Exento     =[" & memo2.RegGlobal.DF_TotalExento & "]")
                        Debug.Print("DF Total IVA        =[" & memo2.RegGlobal.DF_TotalIVA & "]")
                        Debug.Print("DF Total Tributos   =[" & memo2.RegGlobal.DF_TotalTributos & "]")
                        Debug.Print("")
                        Debug.Print("DNFH TOTAL          =[" & memo2.RegGlobal.DNFH_Total & "]")
                        Debug.Print("")
                    Case Else
                        Debug.Print("Primer registro de información: DESCONOCIDO ! ...")
                        Debug.Print("")
                End Select

                memo2 = hasar.ContinuarConsultaAcumulados
            End While
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: ACUMULADOS CIERRE 'Z' - Acumulados/Reportes ...
    '============================================================================================================================
    Private Sub ButtonAcumZeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAcumZeta.Click
        Dim cierre1 As HasarArgentina.RespuestaConsultarAcumuladosCierreZeta
        Dim cierre2 As HasarArgentina.RespuestaContinuarConsultaAcumulados

        Try
            Debug.Print("===========================================")
            Debug.Print("Acumulados/Reportes: ACUM. MEM. TRABAJO ...")
            Debug.Print("===========================================")

            Debug.Print("Obteniendo primer registro de información ...")
            Debug.Print("")

            cierre1 = hasar.ConsultarAcumuladosCierreZeta(HasarArgentina.TipoReporteZ.ReporteZNumero, "3", HasarArgentina.TiposComprobante.NoDocumento)

            If (cierre1.Registro = HasarArgentina.TiposDeRegistroInforme.RegistroFinal) Then
                Debug.Print("No hay información disponible ...")
                Debug.Print("")
                Exit Sub
            End If

            Select Case cierre1.Registro
                Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
                    Debug.Print("(DF) Documento Fiscal ...")
                    Debug.Print("-------------------------")
                    Debug.Print("Cód. Comprob.     =[" & cierre1.RegDF.CodigoComprobante & "]")
                    Debug.Print("Desde Nº          =[" & cierre1.RegDF.NumeroInicial & "]")
                    Debug.Print("Hasta Nº          =[" & cierre1.RegDF.NumeroFinal & "]")
                    Debug.Print("Cancelados        =[" & cierre1.RegDF.CantidadCancelados & "]")
                    Debug.Print("")
                    Debug.Print("TOTAL             =[" & cierre1.RegDF.Total & "]")
                    Debug.Print("Total Gravado     =[" & cierre1.RegDF.TotalGravado & "]")
                    Debug.Print("Total No Gravado  =[" & cierre1.RegDF.TotalNoGravado & "]")
                    Debug.Print("Total Exento      =[" & cierre1.RegDF.TotalExento & "]")
                    Debug.Print("Total IVA         =[" & cierre1.RegDF.TotalIVA & "]")
                    Debug.Print("Total Otros Trib. =[" & cierre1.RegDF.TotalTributos & "]")
                    Debug.Print("")
                    Debug.Print("Alícuota 1        =[" & cierre1.RegDF.AlicuotaIVA_1 & "]")
                    Debug.Print("Monto IVA 1       =[" & cierre1.RegDF.MontoIVA_1 & "]")
                    Debug.Print("Alícuota 2        =[" & cierre1.RegDF.AlicuotaIVA_2 & "]")
                    Debug.Print("Monto IVA 2       =[" & cierre1.RegDF.MontoIVA_2 & "]")
                    Debug.Print("Alícuota 3        =[" & cierre1.RegDF.AlicuotaIVA_3 & "]")
                    Debug.Print("Monto IVA 3       =[" & cierre1.RegDF.MontoIVA_3 & "]")
                    Debug.Print("Alícuota 4        =[" & cierre1.RegDF.AlicuotaIVA_4 & "]")
                    Debug.Print("Monto IVA 4       =[" & cierre1.RegDF.MontoIVA_4 & "]")
                    Debug.Print("Alícuota 5        =[" & cierre1.RegDF.AlicuotaIVA_5 & "]")
                    Debug.Print("Monto IVA 5       =[" & cierre1.RegDF.MontoIVA_5 & "]")
                    Debug.Print("Alícuota 6        =[" & cierre1.RegDF.AlicuotaIVA_6 & "]")
                    Debug.Print("Monto IVA 6       =[" & cierre1.RegDF.MontoIVA_6 & "]")
                    Debug.Print("Alícuota 7        =[" & cierre1.RegDF.AlicuotaIVA_7 & "]")
                    Debug.Print("Monto IVA 7       =[" & cierre1.RegDF.MontoIVA_7 & "]")
                    Debug.Print("Alícuota 8        =[" & cierre1.RegDF.AlicuotaIVA_8 & "]")
                    Debug.Print("Monto IVA 8       =[" & cierre1.RegDF.MontoIVA_8 & "]")
                    Debug.Print("Alícuota 9        =[" & cierre1.RegDF.AlicuotaIVA_9 & "]")
                    Debug.Print("Monto IVA 9       =[" & cierre1.RegDF.MontoIVA_9 & "]")

                    Debug.Print("Alícuota 10       =[" & cierre1.RegDF.AlicuotaIVA_10 & "]")
                    Debug.Print("Monto IVA 10      =[" & cierre1.RegDF.MontoIVA_10 & "]")
                    Debug.Print("")
                    Debug.Print("Cód. Tributo 1    =[" & cierre1.RegDF.CodigoTributo1 & "]")
                    Debug.Print("Importe 1         =[" & cierre1.RegDF.ImporteTributo1 & "]")
                    Debug.Print("Cód. Tributo 2    =[" & cierre1.RegDF.CodigoTributo2 & "]")
                    Debug.Print("Importe 2         =[" & cierre1.RegDF.ImporteTributo2 & "]")
                    Debug.Print("Cód. Tributo 3    =[" & cierre1.RegDF.CodigoTributo3 & "]")
                    Debug.Print("Importe 3         =[" & cierre1.RegDF.ImporteTributo3 & "]")
                    Debug.Print("Cód. Tributo 4    =[" & cierre1.RegDF.CodigoTributo4 & "]")
                    Debug.Print("Importe 4         =[" & cierre1.RegDF.ImporteTributo4 & "]")
                    Debug.Print("Cód. Tributo 5    =[" & cierre1.RegDF.CodigoTributo5 & "]")
                    Debug.Print("Importe 5         =[" & cierre1.RegDF.ImporteTributo5 & "]")
                    Debug.Print("Cód. Tributo 6    =[" & cierre1.RegDF.CodigoTributo6 & "]")
                    Debug.Print("Impoorte 6        =[" & cierre1.RegDF.ImporteTributo6 & "]")
                    Debug.Print("Cód. Tributo 7    =[" & cierre1.RegDF.CodigoTributo7 & "]")
                    Debug.Print("Importe 7         =[" & cierre1.RegDF.ImporteTributo7 & "]")
                    Debug.Print("Cód. Tributo 8    =[" & cierre1.RegDF.CodigoTributo8 & "]")
                    Debug.Print("Importe 8         =[" & cierre1.RegDF.ImporteTributo8 & "]")
                    Debug.Print("Cód. Tributo 9    =[" & cierre1.RegDF.CodigoTributo9 & "]")
                    Debug.Print("Importe 9         =[" & cierre1.RegDF.ImporteTributo9 & "]")
                    Debug.Print("Cód. Tributo 10   =[" & cierre1.RegDF.CodigoTributo10 & "]")
                    Debug.Print("Importe 10        =[" & cierre1.RegDF.ImporteTributo10 & "]")
                    Debug.Print("Cód. Tributo 11   =[" & cierre1.RegDF.CodigoTributo11 & "]")
                    Debug.Print("Importe 11        =[" & cierre1.RegDF.ImporteTributo11 & "]")
                    Debug.Print("")
                Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
                    Debug.Print("(DNFH) Documento NO Fiscal Homologado (que SI acumula) ...")
                    Debug.Print("----------------------------------------------------------")
                    Debug.Print("Cód. Comprob. =[" & cierre1.RegDNFH.CodigoComprobante & "]")
                    Debug.Print("Desde Nº      =[" & cierre1.RegDNFH.NumeroInicial & "]")
                    Debug.Print("Hasta Nº      =[" & cierre1.RegDNFH.NumeroFinal & "]")
                    Debug.Print("")
                    Debug.Print("TOTAL         =[" & cierre1.RegDNFH.Total & "]")
                    Debug.Print("")
                Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
                    Debug.Print("(DNFH) Documento NO Fiscal Homologado (que NO acumula) ...")
                    Debug.Print("----------------------------------------------------------")
                    Debug.Print("Cód. Comprob. =[" & cierre1.RegDNFH_NoAcum.CodigoComprobante & "]")
                    Debug.Print("Desde Nº      =[" & cierre1.RegDNFH_NoAcum.NumeroInicial & "]")
                    Debug.Print("Hasta Nº      =[" & cierre1.RegDNFH_NoAcum.NumeroFinal & "]")
                    Debug.Print("")
                Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
                    Debug.Print("Información Global ...")
                    Debug.Print("----------------------")
                    Debug.Print("DF Emitidos         =[" & cierre1.RegGlobal.DF_CantidadEmitidos & "]")
                    Debug.Print("DF Cancelados       =[" & cierre1.RegGlobal.DF_CantidadCancelados & "]")
                    Debug.Print("DF TOTAL            =[" & cierre1.RegGlobal.DF_Total & "]")
                    Debug.Print("DF Total Gravado    =[" & cierre1.RegGlobal.DF_TotalGravado & "]")
                    Debug.Print("DF Total No Gravado =[" & cierre1.RegGlobal.DF_TotalNoGravado & "]")
                    Debug.Print("DF Total Exento     =[" & cierre1.RegGlobal.DF_TotalExento & "]")
                    Debug.Print("DF Total IVA        =[" & cierre1.RegGlobal.DF_TotalIVA & "]")
                    Debug.Print("DF Total Tributos   =[" & cierre1.RegGlobal.DF_TotalTributos & "]")
                    Debug.Print("")
                    Debug.Print("DNFH TOTAL          =[" & cierre1.RegGlobal.DNFH_Total & "]")
                    Debug.Print("")
                Case Else
                    Debug.Print("Primer registro de información: DESCONOCIDO ! ...")
                    Debug.Print("")
            End Select

            cierre2 = hasar.ContinuarConsultaAcumulados

            While (cierre2.Registro <> HasarArgentina.TiposDeRegistroInforme.RegistroFinal)
                Select Case cierre2.Registro
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDF
                        Debug.Print("(DF) Documento Fiscal ...")
                        Debug.Print("-------------------------")
                        Debug.Print("Cód. Comprob.     =[" & cierre2.RegDF.CodigoComprobante & "]")
                        Debug.Print("Desde Nº          =[" & cierre2.RegDF.NumeroInicial & "]")
                        Debug.Print("Hasta Nº          =[" & cierre2.RegDF.NumeroFinal & "]")
                        Debug.Print("Cancelados        =[" & cierre2.RegDF.CantidadCancelados & "]")
                        Debug.Print("")
                        Debug.Print("TOTAL             =[" & cierre2.RegDF.Total & "]")
                        Debug.Print("Total Gravado     =[" & cierre2.RegDF.TotalGravado & "]")
                        Debug.Print("Total No Gravado  =[" & cierre2.RegDF.TotalNoGravado & "]")
                        Debug.Print("Total Exento      =[" & cierre2.RegDF.TotalExento & "]")
                        Debug.Print("Total IVA         =[" & cierre2.RegDF.TotalIVA & "]")
                        Debug.Print("Total Otros Trib. =[" & cierre2.RegDF.TotalTributos & "]")
                        Debug.Print("")
                        Debug.Print("Alícuota 1        =[" & cierre2.RegDF.AlicuotaIVA_1 & "]")
                        Debug.Print("Monto IVA 1       =[" & cierre2.RegDF.MontoIVA_1 & "]")
                        Debug.Print("Alícuota 2        =[" & cierre2.RegDF.AlicuotaIVA_2 & "]")
                        Debug.Print("Monto IVA 2       =[" & cierre2.RegDF.MontoIVA_2 & "]")
                        Debug.Print("Alícuota 3        =[" & cierre2.RegDF.AlicuotaIVA_3 & "]")
                        Debug.Print("Monto IVA 3       =[" & cierre2.RegDF.MontoIVA_3 & "]")
                        Debug.Print("Alícuota 4        =[" & cierre2.RegDF.AlicuotaIVA_4 & "]")
                        Debug.Print("Monto IVA 4       =[" & cierre2.RegDF.MontoIVA_4 & "]")
                        Debug.Print("Alícuota 5        =[" & cierre2.RegDF.AlicuotaIVA_5 & "]")
                        Debug.Print("Monto IVA 5       =[" & cierre2.RegDF.MontoIVA_5 & "]")
                        Debug.Print("Alícuota 6        =[" & cierre2.RegDF.AlicuotaIVA_6 & "]")
                        Debug.Print("Monto IVA 6       =[" & cierre2.RegDF.MontoIVA_6 & "]")
                        Debug.Print("Alícuota 7        =[" & cierre2.RegDF.AlicuotaIVA_7 & "]")
                        Debug.Print("Monto IVA 7       =[" & cierre2.RegDF.MontoIVA_7 & "]")
                        Debug.Print("Alícuota 8        =[" & cierre2.RegDF.AlicuotaIVA_8 & "]")
                        Debug.Print("Monto IVA 8       =[" & cierre2.RegDF.MontoIVA_8 & "]")
                        Debug.Print("Alícuota 9        =[" & cierre2.RegDF.AlicuotaIVA_9 & "]")
                        Debug.Print("Monto IVA 9       =[" & cierre2.RegDF.MontoIVA_9 & "]")
                        Debug.Print("Alícuota 10       =[" & cierre2.RegDF.AlicuotaIVA_10 & "]")
                        Debug.Print("Monto IVA 10      =[" & cierre2.RegDF.MontoIVA_10 & "]")
                        Debug.Print("")
                        Debug.Print("Cód. Tributo 1    =[" & cierre2.RegDF.CodigoTributo1 & "]")
                        Debug.Print("Importe 1         =[" & cierre2.RegDF.ImporteTributo1 & "]")
                        Debug.Print("Cód. Tributo 2    =[" & cierre2.RegDF.CodigoTributo2 & "]")
                        Debug.Print("Importe 2         =[" & cierre2.RegDF.ImporteTributo2 & "]")
                        Debug.Print("Cód. Tributo 3    =[" & cierre2.RegDF.CodigoTributo3 & "]")
                        Debug.Print("Importe 3         =[" & cierre2.RegDF.ImporteTributo3 & "]")
                        Debug.Print("Cód. Tributo 4    =[" & cierre2.RegDF.CodigoTributo4 & "]")
                        Debug.Print("Importe 4         =[" & cierre2.RegDF.ImporteTributo4 & "]")
                        Debug.Print("Cód. Tributo 5    =[" & cierre2.RegDF.CodigoTributo5 & "]")
                        Debug.Print("Importe 5         =[" & cierre2.RegDF.ImporteTributo5 & "]")
                        Debug.Print("Cód. Tributo 6    =[" & cierre2.RegDF.CodigoTributo6 & "]")
                        Debug.Print("Impoorte 6        =[" & cierre2.RegDF.ImporteTributo6 & "]")
                        Debug.Print("Cód. Tributo 7    =[" & cierre2.RegDF.CodigoTributo7 & "]")
                        Debug.Print("Importe 7         =[" & cierre2.RegDF.ImporteTributo7 & "]")
                        Debug.Print("Cód. Tributo 8    =[" & cierre2.RegDF.CodigoTributo8 & "]")
                        Debug.Print("Importe 8         =[" & cierre2.RegDF.ImporteTributo8 & "]")
                        Debug.Print("Cód. Tributo 9    =[" & cierre2.RegDF.CodigoTributo9 & "]")
                        Debug.Print("Importe 9         =[" & cierre2.RegDF.ImporteTributo9 & "]")
                        Debug.Print("Cód. Tributo 10   =[" & cierre2.RegDF.CodigoTributo10 & "]")
                        Debug.Print("Importe 10        =[" & cierre2.RegDF.ImporteTributo10 & "]")
                        Debug.Print("Cód. Tributo 11   =[" & cierre2.RegDF.CodigoTributo11 & "]")
                        Debug.Print("Importe 11        =[" & cierre2.RegDF.ImporteTributo11 & "]")
                        Debug.Print("")
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFH
                        Debug.Print("(DNFH) Documento NO Fiscal Homologado (que SI acumula) ...")
                        Debug.Print("----------------------------------------------------------")
                        Debug.Print("Cód. Comprob. =[" & cierre2.RegDNFH.CodigoComprobante & "]")
                        Debug.Print("Desde Nº      =[" & cierre2.RegDNFH.NumeroInicial & "]")
                        Debug.Print("Hasta Nº      =[" & cierre2.RegDNFH.NumeroFinal & "]")
                        Debug.Print("")
                        Debug.Print("TOTAL         =[" & cierre2.RegDNFH.Total & "]")
                        Debug.Print("")
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroDetalladoDNFHNoAcum
                        Debug.Print("(DNFH) Documento NO Fiscal Homologado (que NO acumula) ...")
                        Debug.Print("----------------------------------------------------------")
                        Debug.Print("Cód. Comprob. =[" & cierre2.RegDNFH_NoAcum.CodigoComprobante & "]")
                        Debug.Print("Desde Nº      =[" & cierre2.RegDNFH_NoAcum.NumeroInicial & "]")
                        Debug.Print("Hasta Nº      =[" & cierre2.RegDNFH_NoAcum.NumeroFinal & "]")
                        Debug.Print("")
                    Case HasarArgentina.TiposDeRegistroInforme.RegistroGlobal
                        Debug.Print("Información Global ...")
                        Debug.Print("----------------------")
                        Debug.Print("DF Emitidos         =[" & cierre2.RegGlobal.DF_CantidadEmitidos & "]")
                        Debug.Print("DF Cancelados       =[" & cierre2.RegGlobal.DF_CantidadCancelados & "]")
                        Debug.Print("DF TOTAL            =[" & cierre2.RegGlobal.DF_Total & "]")
                        Debug.Print("DF Total Gravado    =[" & cierre2.RegGlobal.DF_TotalGravado & "]")
                        Debug.Print("DF Total No Gravado =[" & cierre2.RegGlobal.DF_TotalNoGravado & "]")
                        Debug.Print("DF Total Exento     =[" & cierre2.RegGlobal.DF_TotalExento & "]")
                        Debug.Print("DF Total IVA        =[" & cierre2.RegGlobal.DF_TotalIVA & "]")
                        Debug.Print("DF Total Tributos   =[" & cierre2.RegGlobal.DF_TotalTributos & "]")
                        Debug.Print("")
                        Debug.Print("DNFH TOTAL          =[" & cierre2.RegGlobal.DNFH_Total & "]")
                        Debug.Print("")
                    Case Else
                        Debug.Print("Primer registro de información: DESCONOCIDO ! ...")
                        Debug.Print("")
                End Select

                cierre2 = hasar.ContinuarConsultaAcumulados
            End While
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: AUDITORÍA por FECHAS - Acumulados/Reportes ...
    '============================================================================================================================
    Private Sub ButtonAudFechas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAudFechas.Click
        Try
            Debug.Print("=========================================================")
            Debug.Print("Acumulados/Reportes: AUDITORÍA por FECHAS (Cód.: 904) ...")
            Debug.Print("=========================================================")
            hasar.ReportarZetasPorFecha("01/01/2015", "31/12/2016", HasarArgentina.TipoReporteAuditoria.ReporteAuditoriaDiscriminado)
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: AUDITORÍA por NRO. de ZETA - Acumulados/Reportes ...
    '============================================================================================================================
    Private Sub ButtonAudNroZeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonAudNroZeta.Click
        Try
            Debug.Print("===============================================================")
            Debug.Print("Acumulados/Reportes: AUDITORÍA por NRO. de ZETA (Cód.: 904) ...")
            Debug.Print("===============================================================")
            hasar.ReportarZetasPorNumeroZeta(1, 10, HasarArgentina.TipoReporteAuditoria.ReporteAuditoriaDiscriminado)
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: REMITO 'R' - Doc. No Fisc. Homolog ...
    '============================================================================================================================
    Private Sub ButtonRemitoR_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRemitoR.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("=====================================")
            Debug.Print("A imprimir: REMITO 'R' (Cód.: 91) ...")
            Debug.Print("=====================================")

            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            hasar.CargarTransportista("Transportador Uno ...", "00000000000", "Calle 4 entre 5 y 6", "Chofer Uno ...", HasarArgentina.TiposDeDocumentoCliente.TipoDNI, "123345678", "ABC 123", )

            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.RemitoR)
            Debug.Print("Remito 'R' Nº     =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total       =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado      =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base        =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA         =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.   =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib. =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: REMITO 'X' - Doc. No Fisc. Homolog ...
    '============================================================================================================================
    Private Sub ButtonRemitoX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRemitoX.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("======================================")
            Debug.Print("A imprimir: REMITO 'X' (Cód.: 901) ...")
            Debug.Print("======================================")

            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            hasar.CargarTransportista("Transportador Uno ...", "00000000000", "Calle 4 entre 5 y 6", "Chofer Uno ...", HasarArgentina.TiposDeDocumentoCliente.TipoDNI, "123345678", "ABC 123", )

            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.RemitoX)
            Debug.Print("Remito 'X' Nº     =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total       =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado      =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base        =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA         =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.   =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib. =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: PRESUPUESTO 'X' - Doc. No Fisc. Homolog ...
    '============================================================================================================================
    Private Sub ButtonPresup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonPresup.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("===========================================")
            Debug.Print("A imprimir: PRESUPUESTO 'X' (Cód.: 903) ...")
            Debug.Print("===========================================")

            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)

            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.PresupuestoX)
            Debug.Print("Presupuesto 'X' Nº   =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total          =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado         =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base           =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA            =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.      =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.    =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: RECIBO 'X' - Doc. No Fisc. Homolog ...
    '============================================================================================================================
    Private Sub ButtonReciboX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonReciboX.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("======================================")
            Debug.Print("A imprimir: RECIBO 'X' (Cód.: 902) ...")
            Debug.Print("======================================")

            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)
            hasar.CargarDocumentoAsociado(1, HasarArgentina.TiposComprobante.FacturaA, 2999, 43)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.ReciboX)
            Debug.Print("Recibo 'X' Nº        =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirTextoFiscal(estilo, "Hasta agotar stock ...", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirItem("Producto Uno ...", 1.0, 131.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, "7791234500001", "00001", HasarArgentina.UnidadesMedida.Unidad)
            hasar.ImprimirDescuentoItem("Oferta Uno ...", 12.1, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal)

            hasar.ImprimirAnticipoBonificacionEnvases("Bonificación a IVA Uno ...", 10.0, HasarArgentina.CondicionesIVA.Gravado, 21.0, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 10.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeOperacionesGlobalesIVA.BonificacionIVA)
            hasar.ImprimirAjuste("Bonificación Gral. Uno", 2.5, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, "7790001001030", HasarArgentina.TiposDeAjustes.BonificacionGeneral)
            hasar.ImprimirOtrosTributos(HasarArgentina.TiposTributos.IIBB, "Otro Tributo Uno ...", 81.21, 4.06)

            hasar.ImprimirConceptoRecibo("Expensas extraordinarias OCT 2015 ...")
            hasar.ImprimirPago("Medio de Pago Uno ...", 110.46, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, "Cheque Bco. Nación Nº ...", HasarArgentina.TiposPago.Cheque, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total          =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado         =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base           =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA            =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.      =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.    =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '============================================================================================================================
    ' BOTÓN: DONACIÓN - Doc. No Fisc. Homolog ...
    '============================================================================================================================
    Private Sub ButtonDonacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonDonacion.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto
        Dim subt As HasarArgentina.RespuestaConsultarSubtotal

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False

        Try
            Debug.Print("====================================")
            Debug.Print("A imprimir: DONACIÓN (Cód.: 907) ...")
            Debug.Print("====================================")

            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)
            hasar.CargarDocumentoAsociado(1, HasarArgentina.TiposComprobante.FacturaA, 2999, 43)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            hasar.CargarBeneficiario("La Escuelita Uno ...", "30124567897", HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle 100, entre 11 y 12 ...")
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.ComprobanteDonacion)
            Debug.Print("Donación Nº          =[" & respabrir.NumeroComprobante & "]")

            hasar.ImprimirItem("Donación de vuelto ...", 1.0, 2.5, HasarArgentina.CondicionesIVA.Exento, 0.0, HasarArgentina.ModosDeMonto.ModoSumaMonto, HasarArgentina.ModosDeImpuestosInternos.IIVariablePorcentual, 0.0, HasarArgentina.ModosDeDisplay.DisplayNo, HasarArgentina.ModosDePrecio.ModoPrecioTotal, 1, , "DONA", HasarArgentina.UnidadesMedida.Donacion)
            hasar.ImprimirPago("Efectivo ...", 2.5, HasarArgentina.ModosDePago.Pagar, HasarArgentina.ModosDeDisplay.DisplayNo, , HasarArgentina.TiposPago.Efectivo, 0, "")

            subt = hasar.ConsultarSubtotal(HasarArgentina.ImpresionSubtotal.NoImprimeSubtotal, HasarArgentina.ModosDeDisplay.DisplayNo)
            Debug.Print("Monto Total          =[" & subt.Subtotal & "]")
            Debug.Print("Monto Pagado         =[" & subt.MontoPagado & "]")
            Debug.Print("Monto Base           =[" & subt.MontoBase & "]")
            Debug.Print("Monto IVA            =[" & subt.MontoIVA & "]")
            Debug.Print("Monto Imp. Int.      =[" & subt.MontoImpInternos & "]")
            Debug.Print("Monto Otros Trib.    =[" & subt.MontoOtrosTributos & "]")

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '===============================================================================================================================
    ' BOTÓN: GENÉRICO - Doc. No Fisc. Homolog ...
    '
    ' Este tipo de comprobantes es de uso libre para cubrir las necesidades comerciales de los usuarios.  
    ' Por ejemplo, para emitir vouchers de tarjeta, talones de estacionamiento, reparto, tiques regalo, cláusulas de créditos, etc..
    '===============================================================================================================================
    Private Sub ButtonGenerico_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonGenerico.Click
        Dim respabrir As HasarArgentina.RespuestaAbrirDocumento
        Dim estilo As HasarArgentina.AtributosDeTexto

        estilo.Centrado = False
        estilo.DobleAncho = False
        estilo.BorradoTexto = False
        estilo.Negrita = False
        Try
            Debug.Print("====================================")
            Debug.Print("A imprimir: GENÉRICO (Cód.: 910) ...")
            Debug.Print("====================================")

            hasar.CargarCodigoBarras(HasarArgentina.TiposDeCodigoDeBarras.CodigoTipoEAN13, "779123456789", HasarArgentina.ImpresionNumeroCodigoDeBarras.ImprimeNumerosCodigo, HasarArgentina.ImpresionCodigoDeBarras.ProgramaCodigo)
            hasar.CargarDocumentoAsociado(1, HasarArgentina.TiposComprobante.FacturaA, 2999, 43)
            hasar.CargarDatosCliente("Cliente Uno ...", "99999999995", HasarArgentina.TiposDeResponsabilidadesCliente.ResponsableInscripto, HasarArgentina.TiposDeDocumentoCliente.TipoCUIT, "Calle Uno, entre Dos y Tres, Nro. 1234 ...", , , )
            respabrir = hasar.AbrirDocumento(HasarArgentina.TiposComprobante.Generico)
            Debug.Print("Genérico Nº =[" & respabrir.NumeroComprobante & "]")

            estilo.Centrado = True
            estilo.DobleAncho = True
            estilo.Negrita = False
            hasar.ImprimirTextoGenerico(estilo, "TITULO DOC. GENERICO", HasarArgentina.ModosDeDisplay.DisplayNo)

            estilo.Centrado = False
            estilo.DobleAncho = False
            estilo.Negrita = False
            hasar.ImprimirTextoGenerico(estilo, ".", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "Se puede emplear todo el ancho del papel para imprimir lo que se", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "desee, y en diferentes estilos. Por ejemplo, centrado y doble an", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "cho (como el título del documento genérico), o bien, ...", HasarArgentina.ModosDeDisplay.DisplayNo)

            hasar.ImprimirTextoGenerico(estilo, ".", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "Sólo doble ancho:", HasarArgentina.ModosDeDisplay.DisplayNo)
            estilo.Centrado = False
            estilo.DobleAncho = True
            estilo.Negrita = False
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", HasarArgentina.ModosDeDisplay.DisplayNo)

            estilo.DobleAncho = False
            hasar.ImprimirTextoGenerico(estilo, ".", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "Sólo centrado:", HasarArgentina.ModosDeDisplay.DisplayNo)
            estilo.Centrado = True
            estilo.DobleAncho = False
            estilo.Negrita = False
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", HasarArgentina.ModosDeDisplay.DisplayNo)

            estilo.Centrado = False
            hasar.ImprimirTextoGenerico(estilo, ".", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "Sólo en negrita:", HasarArgentina.ModosDeDisplay.DisplayNo)
            estilo.Centrado = False
            estilo.Negrita = True
            estilo.DobleAncho = False
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", HasarArgentina.ModosDeDisplay.DisplayNo)

            estilo.Negrita = False
            hasar.ImprimirTextoGenerico(estilo, ".", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "Centrado y en negrita:", HasarArgentina.ModosDeDisplay.DisplayNo)
            estilo.Centrado = True
            estilo.Negrita = True
            estilo.DobleAncho = False
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", HasarArgentina.ModosDeDisplay.DisplayNo)

            estilo.Centrado = False
            estilo.Negrita = False
            hasar.ImprimirTextoGenerico(estilo, ".", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "Doble ancho y en negrita:", HasarArgentina.ModosDeDisplay.DisplayNo)
            estilo.Centrado = False
            estilo.DobleAncho = True
            estilo.Negrita = True
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", HasarArgentina.ModosDeDisplay.DisplayNo)

            estilo.DobleAncho = False
            estilo.Negrita = False
            hasar.ImprimirTextoGenerico(estilo, ".", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "Centrado, negrita y doble ancho:", HasarArgentina.ModosDeDisplay.DisplayNo)
            estilo.Centrado = True
            estilo.Negrita = True
            estilo.DobleAncho = True
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", HasarArgentina.ModosDeDisplay.DisplayNo)

            estilo.Centrado = False
            estilo.Negrita = False
            estilo.DobleAncho = False
            hasar.ImprimirTextoGenerico(estilo, ".", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "Estilo por defecto:", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "ABCDEFG 123456789", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, ".", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "Tener presente que la normativa fiscal vigente NO permite la im-", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "presión de comprobantes que se asemejen a los documentos fisca-", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, "les, emitidos mediante el uso de controladores fiscales.", HasarArgentina.ModosDeDisplay.DisplayNo)
            hasar.ImprimirTextoGenerico(estilo, ".", HasarArgentina.ModosDeDisplay.DisplayNo)

            hasar.CerrarDocumento(, )
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '===============================================================================================================================
    ' BOTÓN: REPORTES ELECTRÓNICOS - Descargar ...
    '
    ' La descarga de esta información permite generar un archivo de texto codificado en ASCII85. Se debe convertir a binario para 
    ' obtener un archivo ZIP que contiene 3 archivos ".PEM" en formato PKCS#7:
    '    - Reporte de totales (información de la memoria fiscal, a elevar a la AFIP)
    '    - Duplicado 'A', 'A' con leyenda y 'M' (información extraída de la cinta testigo digital, a elevar a la AFIP).
    '    - Cinta testigo digital, o memoria de auditoría (el contribuyente debe conservr dos copias de esta información).
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información, accediendo a la interfaz HTTP
    ' de la impresora fiscal.
    '===============================================================================================================================
    Private Sub ButtonRepElect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRepElect.Click
        Dim bloque1 As HasarArgentina.RespuestaObtenerPrimerBloqueReporteElectronico
        Dim bloque2 As HasarArgentina.RespuestaObtenerSiguienteBloqueReporteElectronico

        Try
            Debug.Print("===================================")
            Debug.Print("Descarga: REPORTES ELECTRÓNICOS ...")
            Debug.Print("===================================")

            bloque1 = hasar.ObtenerPrimerBloqueReporteElectronico("01/01/2015", "21/10/2015", HasarArgentina.TiposReporteAFIP.ReporteAFIPCompleto)

            If (bloque1.Registro = HasarArgentina.IdentificadorBloque.BloqueFinal) Then
                Debug.Print("No hay información disponible ! ...")
                Debug.Print("")
            End If

            Debug.Print("Primer bloque =[" & bloque1.Informacion & "]")
            bloque2 = hasar.ObtenerSiguienteBloqueReporteElectronico()

            While (bloque2.Registro <> HasarArgentina.IdentificadorBloque.BloqueFinal)
                Debug.Print("Sig. bloque   =[" & bloque2.Informacion & "]")
                bloque2 = hasar.ObtenerSiguienteBloqueReporteElectronico()
            End While

            If (bloque2.Registro = HasarArgentina.IdentificadorBloque.BloqueFinal) Then
                Debug.Print("Sig. bloque   =[" & bloque2.Informacion & "]")
            End If

            Debug.Print("Descarga finalizada ! ...")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '===============================================================================================================================
    ' BOTÓN: REPORTE DOCUMENTOS - Descargar ...
    '
    ' La descarga permite obterner, en formato XML, el detalle de uno o más comprobantes emitidos (del mismo tipo). La información
    ' es extraída de la cinta testigo digital (o memoria de auditoría).
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información, accediendo a la interfaz HTTP
    ' de la impresora fiscal.
    '===============================================================================================================================
    Private Sub ButtonRepDoc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonRepDoc.Click
        Dim bloque1 As HasarArgentina.RespuestaObtenerPrimerBloqueDocumento
        Dim bloque2 As HasarArgentina.RespuestaObtenerSiguienteBloqueDocumento

        Try
            Debug.Print("================================")
            Debug.Print("Descarga: REPORTE DOCUMENTOS ...")
            Debug.Print("================================")

            bloque1 = hasar.ObtenerPrimerBloqueDocumento(1, 5, HasarArgentina.TiposComprobante.TiqueFacturaA, HasarArgentina.CompresionDeInformacion.NoComprime, HasarArgentina.XMLsPorBajada.XMLUnico)

            If (bloque1.Registro = HasarArgentina.IdentificadorBloque.BloqueFinal) Then
                Debug.Print("No hay información disponible ! ...")
                Debug.Print("")
            End If

            Debug.Print("Primer bloque =[" & bloque1.Informacion & "]")
            bloque2 = hasar.ObtenerSiguienteBloqueDocumento()

            While (bloque2.Registro <> HasarArgentina.IdentificadorBloque.BloqueFinal)
                Debug.Print("Sig. bloque   =[" & bloque2.Informacion & "]")
                bloque2 = hasar.ObtenerSiguienteBloqueDocumento
            End While

            If (bloque2.Registro = HasarArgentina.IdentificadorBloque.BloqueFinal) Then
                Debug.Print("Sig. bloque   =[" & bloque2.Informacion & "]")
            End If

            Debug.Print("Descarga finalizada ! ...")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '===============================================================================================================================
    ' BOTÓN: CINTA TESTIGO DIGITAL - Descargar ...
    '
    ' La descarga permite obterner, en formato XML, el detalle del contenido de la cinta testigo digital. 
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información, accediendo a la interfaz HTTP
    ' de la impresora fiscal.
    '===============================================================================================================================
    Private Sub ButtonCTD_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonCTD.Click
        Dim bloque1 As HasarArgentina.RespuestaObtenerPrimerBloqueAuditoria
        Dim bloque2 As HasarArgentina.RespuestaObtenerSiguienteBloqueAuditoria

        Try
            Debug.Print("===================================")
            Debug.Print("Descarga: CINTA TESTIGO DIGITAL ...")
            Debug.Print("===================================")

            bloque1 = hasar.ObtenerPrimerBloqueAuditoria("1", "10", HasarArgentina.TipoReporteZ.ReporteZNumero, HasarArgentina.CompresionDeInformacion.NoComprime, HasarArgentina.XMLsPorBajada.XMLUnico)

            If (bloque1.Registro = HasarArgentina.IdentificadorBloque.BloqueFinal) Then
                Debug.Print("No hay información disponible ! ...")
                Debug.Print("")
            End If

            Debug.Print("Primer bloque =[" & bloque1.Informacion & "]")
            bloque2 = hasar.ObtenerSiguienteBloqueAuditoria()

            While (bloque2.Registro <> HasarArgentina.IdentificadorBloque.BloqueFinal)
                Debug.Print("Sig. bloque   =[" & bloque2.Informacion & "]")
                bloque2 = hasar.ObtenerSiguienteBloqueAuditoria()
            End While

            If (bloque2.Registro = HasarArgentina.IdentificadorBloque.BloqueFinal) Then
                Debug.Print("Sig. bloque   =[" & bloque2.Informacion & "]")
            End If

            Debug.Print("Descarga finalizada ! ...")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    '===============================================================================================================================
    ' BOTÓN: LOG INTERNO - Descargar ...
    '
    ' La descarga permite obterner el contenido del registro interno de actividades, de la IFH 2G.. 
    '
    ' El contribuyente puede emplear un navegador de internet y descargar manualmente la información, accediendo a la interfaz HTTP
    ' de la impresora fiscal.
    '===============================================================================================================================
    Private Sub ButtonLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonLog.Click
        Dim bloque1 As HasarArgentina.RespuestaObtenerPrimerBloqueLog
        Dim bloque2 As HasarArgentina.RespuestaObtenerSiguienteBloqueLog

        Try
            Debug.Print("=========================")
            Debug.Print("Descarga: LOG INTERNO ...")
            Debug.Print("=========================")

            bloque1 = hasar.ObtenerPrimerBloqueLog()

            If (bloque1.Registro = HasarArgentina.IdentificadorBloque.BloqueFinal) Then
                Debug.Print("No hay información disponible ! ...")
                Debug.Print("")
            End If

            Debug.Print("Primer bloque =[" & bloque1.Informacion & "]")
            bloque2 = hasar.ObtenerSiguienteBloqueLog()

            While (bloque2.Registro <> HasarArgentina.IdentificadorBloque.BloqueFinal)
                Debug.Print("Sig. bloque   =[" & bloque2.Informacion & "]")
                bloque2 = hasar.ObtenerSiguienteBloqueLog
            End While

            If (bloque2.Registro = HasarArgentina.IdentificadorBloque.BloqueFinal) Then
                Debug.Print("Sig. bloque   =[" & bloque2.Informacion & "]")
            End If

            Debug.Print("Descarga finalizada ! ...")
            Debug.Print("")
        Catch
            Debug.Print("Código: " & Err.Number & vbCrLf & "Mensaje: " & Err.Description)
            Debug.Print("")
        End Try
    End Sub

    Private Sub evhasar_ComandoEnProceso() Handles evhasar.ComandoEnProceso
        Debug.Print("Comando en proceso ...")
    End Sub

    Private Sub evhasar_ComandoProcesado() Handles evhasar.ComandoProcesado
        Debug.Print("Comando ya procesao ...")
    End Sub

    Private Sub evhasar_EstadoEspera(ByRef EstadoImpresion As HasarArgentina.EstadoImpresora) Handles evhasar.EstadoEspera
        Debug.Print("En estado de espera ...")
    End Sub
End Class


