@echo off
cls
echo .
echo . ### Probando: XML ###
echo .
echo . ###    INICIO     ###
echo .

if "%1" == "" goto salgo

curl --noproxy 127.0.1.1 http://127.0.1.19/fiscal.xml -H "Content-Type: text/xml" --data-binary @%1 > resp.xml

echo .
echo .

type resp.xml

echo .
echo .
goto final

:salgo
echo .
echo . ERROR de invocaci�n... !! - Falta indicar archivo XML.
echo .
echo . Ejemplo:   sendxml archivo.xml
echo .
echo .

:final
echo . ###    FIN        ###
echo .

