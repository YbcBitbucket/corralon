@echo off
cls
echo .
echo . ### Probando: JSON ###
echo .
echo . ###    INICIO      ###
echo .

if "%1" == "" goto salgo

curl --noproxy 10.0.7.69 http://10.0.7.69/fiscal.json -H "Content-Type: application/json" --data-binary @%1 > resp.json

echo .
echo .

type resp.json

echo .
echo .
goto final

:salgo
echo .
echo . ERROR de invocaci�n... !! - Falta indicar archivo JSON.
echo .
echo . Ejemplo:   sendjson archivo.json
echo .
echo .

:final
echo . ###      FIN       ###
echo .

