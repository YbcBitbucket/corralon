﻿# Host: localhost  (Version 5.5.5-10.5.5-MariaDB)
# Date: 2020-11-16 19:20:56
# Generator: MySQL-Front 6.1  (Build 1.26)


#
# Structure for table "afip_hasar_configuracion"
#

DROP TABLE IF EXISTS `afip_hasar_configuracion`;
CREATE TABLE `afip_hasar_configuracion` (
  `limitecf` double(11,2) DEFAULT 1000.00,
  `limitetf` double(11,2) DEFAULT 25000.00,
  `copias` int(11) DEFAULT 48,
  `cambio` int(1) DEFAULT 0,
  `leyendas` int(1) DEFAULT 0,
  `corte` int(11) DEFAULT 70
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "afip_hasar_configuracion"
#

INSERT INTO `afip_hasar_configuracion` VALUES (2000000.00,2000000.00,48,1,0,70);

#
# Structure for table "afip_hasar_modelos"
#

DROP TABLE IF EXISTS `afip_hasar_modelos`;
CREATE TABLE `afip_hasar_modelos` (
  `idModelo` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `modelo` varchar(255) DEFAULT NULL,
  `variable` int(11) DEFAULT NULL,
  PRIMARY KEY (`idModelo`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

#
# Data for table "afip_hasar_modelos"
#

INSERT INTO `afip_hasar_modelos` VALUES (1,'P-614F','MODELO_614',1),(2,'P-615F','MODELO_615',2),(3,'P-PR4F','MODELO_PR4',3),(4,'P-950F','MODELO_950',4),(5,'P-951F','MODELO_951',5),(6,'PT-262F','MODELO_262',6),(7,'PJ-20F','MODELO_PJ20',7),(8,'P-320F','MODELO_P320',8),(9,'P-715F','MODELO_715',9),(10,'P-715F v02.01','MODELO_715_201',25),(11,'P-715F v03.02','MODELO_715_302',29),(12,'P-715F v04.03','MODELO_715_403',30),(13,'P-715F v05.04','MODELO_715_504',40),(14,'P-PR5F v01.00','MODELO_PR5',10),(15,'P-PR5F v02.01','MODELO_PR5_201',26),(16,'P-PR5F v03.02','MODELO_PR5_302',37),(17,'PT-272F v01.00','MODELO_272',11),(18,'PT-272F v02.01','MODELO_272_201',38),(19,'PL-8F v01.00','MODELO_PPL8',12),(20,'PL-8F v02.01','MODELO_PPL8_201',17),(21,'P-321F v01.00','MODELO_P321',13),(22,'P-322F v01.00','MODELO_P322',14),(23,'P-322F v02.01','MODELO_P322_201',18),(24,'P-425F v01.00','MODELO_P425',15),(25,'P-425F v02.01','MODELO_P425_201',16),(26,'P-330F v01.00','MODELO_P330',19),(27,'P-330F v02.01','MODELO_P330_201',21),(28,'P-330F v02.02','MODELO_P330_202',23),(29,'P-330F v02.03','MODELO_P330_203',31),(30,'P-435F v01.00','MODELO_P435',20),(31,'P-435F v01.01','MODELO_P435_101',24),(32,'P-435F v01.02','MODELO_P435_102',27),(33,'P-435F v02.03','MODELO_P435_203',34),(34,'PL-9F v01.00','MODELO_PPL9',22),(35,'PL-23F v01.00','MODELO_PPL23',28),(36,'PL-23F v01.01','MODELO_PPL23_101',33),(37,'PL-23F v02.02','MODELO_PPL23_202',36),(38,'P-441F v01.00','MODELO_P441',32),(39,'P-441F v02.01','MODELO_P441_201',39),(40,'P-1120F v01.00','MODELO_P1120',35),(41,'P-340F v01.00','MODELO_P340',41);

#
# Structure for table "afip_hasar_propiedades"
#

DROP TABLE IF EXISTS `afip_hasar_propiedades`;
CREATE TABLE `afip_hasar_propiedades` (
  `idImpresoraHasar` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transporte` int(1) NOT NULL DEFAULT 0,
  `puerto` int(11) NOT NULL DEFAULT 2,
  `baudios` int(11) DEFAULT 9600,
  `direccionip` varchar(100) DEFAULT '127.0.0.1',
  PRIMARY KEY (`idImpresoraHasar`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "afip_hasar_propiedades"
#

INSERT INTO `afip_hasar_propiedades` VALUES (1,0,2,9600,'127.0.0.1'),(2,0,3,9600,'127.0.0.1'),(3,0,6,9600,'192.168.0.250');

#
# Structure for table "afip_hasar_z"
#

DROP TABLE IF EXISTS `afip_hasar_z`;
CREATE TABLE `afip_hasar_z` (
  `idZ` int(11) NOT NULL AUTO_INCREMENT,
  `NroComprobante` int(11) DEFAULT NULL,
  `Fecha` date DEFAULT NULL,
  `DF_CantidadCancelados` int(11) DEFAULT NULL,
  `DF_CantidadEmitidos` int(11) DEFAULT NULL,
  `DF_Total` double(11,2) DEFAULT NULL,
  `DF_TotalExento` double(11,2) DEFAULT NULL,
  `DF_TotalGravado` double(11,2) DEFAULT NULL,
  `DF_TotalIVA` double(11,2) DEFAULT NULL,
  `DF_TotalNoGravado` double(11,2) DEFAULT NULL,
  `DF_TotalTributo` double(11,2) DEFAULT NULL,
  `DNFH_CantidadEmitidos` int(11) DEFAULT NULL,
  `DNFH_Total` double(11,2) DEFAULT NULL,
  `NC_CantidadCancelados` int(11) DEFAULT NULL,
  `NC_CantidadEmitidos` int(11) DEFAULT NULL,
  `NC_Total` double(11,2) DEFAULT NULL,
  `NC_TotalExento` double(11,2) DEFAULT NULL,
  `NC_TotalGravado` double(11,2) DEFAULT NULL,
  `NC_TotalIVA` double(11,2) DEFAULT NULL,
  `NC_TotalNoGravado` double(11,2) DEFAULT NULL,
  `NC_TotalTributo` double(11,2) DEFAULT NULL,
  PRIMARY KEY (`idZ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "afip_hasar_z"
#


#
# Structure for table "afip_operacion"
#

DROP TABLE IF EXISTS `afip_operacion`;
CREATE TABLE `afip_operacion` (
  `idCodigoOperacion` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(1) NOT NULL DEFAULT '',
  `descripcion` varchar(50) DEFAULT '',
  PRIMARY KEY (`idCodigoOperacion`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

#
# Data for table "afip_operacion"
#

INSERT INTO `afip_operacion` VALUES (1,'0','NO CORRESPONDE'),(2,'A','NO ALCANZADO'),(3,'C','OPERACIONES DE CANJE'),(4,'E','OPERACIONES EXENTAS'),(5,'N','NO GRAVADO'),(6,'X','IMPORTACION DEL EXTERIOR'),(7,'Z','IMPORTACION DE ZONA FRANCA');

#
# Structure for table "afip_tipo_facturacion"
#

DROP TABLE IF EXISTS `afip_tipo_facturacion`;
CREATE TABLE `afip_tipo_facturacion` (
  `idAfipTipo` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT NULL,
  `tipo` int(1) DEFAULT 0,
  `estado` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idAfipTipo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "afip_tipo_facturacion"
#

INSERT INTO `afip_tipo_facturacion` VALUES (1,'WSFE',0,'HOMOLOGACION'),(2,'HASAR',0,NULL),(3,'HASAR 2G',1,NULL);

#
# Structure for table "afip_tipocbte"
#

DROP TABLE IF EXISTS `afip_tipocbte`;
CREATE TABLE `afip_tipocbte` (
  `idTipoCbte` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) DEFAULT NULL,
  `wsfe` int(11) DEFAULT NULL,
  `hasar` int(11) DEFAULT NULL,
  `fchdesde` varchar(50) DEFAULT NULL,
  `fchhasta` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idTipoCbte`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;

#
# Data for table "afip_tipocbte"
#

INSERT INTO `afip_tipocbte` VALUES (1,'FACTURAS A',1,48,'20100917',''),(2,'NOTAS DE DEBITO A',2,68,'20100917',''),(3,'NOTAS DE CREDITO A',3,NULL,'20100917',''),(4,'RECIBOS A',4,97,'20100917',''),(5,'NOTAS DE VENTA AL CONTADO A',5,NULL,'20100917',''),(6,'FACTURAS B',6,49,'20100917',''),(7,'NOTAS DE DEBITO B',7,69,'20100917',''),(8,'NOTAS DE CREDITO B',8,NULL,'20100917',''),(9,'RECIBOS B',9,98,'20100917',''),(10,'NOTAS DE VENTA AL CONTADO B',10,NULL,'20100917',''),(11,'FACTURAS C',11,NULL,'20100917',''),(12,'NOTAS DE DEBITO C',12,NULL,'20100917',''),(13,'NOTAS DE CREDITO C',13,NULL,'20100917',''),(14,'RECIBOS C',15,NULL,'20100917',''),(15,'NOTAS DE VENTA AL CONTADO C',16,NULL,'20100917',''),(16,'LIQUIDACION DE SERVICIOS PUBLICOS CLASE A',17,NULL,'20100917',''),(17,'LIQUIDACION DE SERVICIOS PUBLICOS CLASE B',18,NULL,'20100917',''),(18,'FACTURAS DE EXPORTACION',19,NULL,'20100917',''),(19,'NOTAS DE DEBITO POR OPERACIONES CON EL EXTERIOR',20,NULL,'20110426',''),(20,'NOTAS DE CREDITO POR OPERACIONES CON EL EXTERIOR',21,NULL,'20110426',''),(21,'FACTURAS - PERMISO EXPORTACION SIMPLIFICADO - DTO. 855/97',22,NULL,'20110426',''),(22,'COMPROBANTES “A” DE COMPRA PRIMARIA PARA EL SECTOR PESQUERO MARITIMO',23,NULL,'20110426',''),(23,'COMPROBANTES “A” DE CONSIGNACION PRIMARIA PARA EL SECTOR PESQUERO MARITIMO',24,NULL,'20130904',''),(24,'COMPROBANTES “B” DE COMPRA PRIMARIA PARA EL SECTOR PESQUERO MARITIMO',25,NULL,'20150522',''),(25,'COMPROBANTES “B” DE CONSIGNACION PRIMARIA PARA EL SECTOR PESQUERO MARITIMO',26,NULL,'20150522',''),(26,'LIQUIDACION UNICA COMERCIAL IMPOSITIVA CLASE A',27,NULL,'20150522',''),(27,'LIQUIDACION UNICA COMERCIAL IMPOSITIVA CLASE B',28,NULL,'20150522',''),(28,'LIQUIDACION UNICA COMERCIAL IMPOSITIVA CLASE C',29,NULL,'',''),(29,'COMPROBANTES DE COMPRA DE BIENES USADOS',30,NULL,'',''),(30,'MANDATO - CONSIGNACION',31,NULL,'',''),(31,'COMPROBANTES PARA RECICLAR MATERIALES',32,NULL,'',''),(32,'LIQUIDACION PRIMARIA DE GRANOS',33,NULL,'',''),(33,'COMPROBANTES A DEL APARTADO A  INCISO F)  R.G. N°  1415',34,NULL,'',''),(34,'COMPROBANTES B DEL ANEXO I, APARTADO A, INC. F), R.G. N° 1415',35,NULL,'',''),(35,'COMPROBANTES C DEL Anexo I, Apartado A, INC.F), R.G. N° 1415',36,NULL,'',''),(36,'NOTAS DE DEBITO O DOCUMENTO EQUIVALENTE QUE CUMPLAN CON LA R.G. N° 1415',37,NULL,'',''),(37,'NOTAS DE CREDITO O DOCUMENTO EQUIVALENTE QUE CUMPLAN CON LA R.G. N° 1415',38,NULL,'',''),(38,'OTROS COMPROBANTES A QUE CUMPLEN CON LA R G  1415',39,NULL,'',''),(39,'OTROS COMPROBANTES B QUE CUMPLAN CON LA R.G. N° 1415',40,NULL,'',''),(40,'OTROS COMPROBANTES C QUE CUMPLAN CON LA R.G. N° 1415',41,NULL,'',''),(41,'NOTA DE CREDITO LIQUIDACION UNICA COMERCIAL IMPOSITIVA CLASE B',43,NULL,'',''),(42,'NOTA DE CREDITO LIQUIDACION UNICA COMERCIAL IMPOSITIVA CLASE C',44,NULL,'',''),(43,'NOTA DE DEBITO LIQUIDACION UNICA COMERCIAL IMPOSITIVA CLASE A',45,NULL,'',''),(44,'NOTA DE DEBITO LIQUIDACION UNICA COMERCIAL IMPOSITIVA CLASE B',46,NULL,'',''),(45,'NOTA DE DEBITO LIQUIDACION UNICA COMERCIAL IMPOSITIVA CLASE C',47,NULL,'',''),(46,'NOTA DE CREDITO LIQUIDACION UNICA COMERCIAL IMPOSITIVA CLASE A',48,NULL,'',''),(47,'COMPROBANTES DE COMPRA DE BIENES NO REGISTRABLES A CONSUMIDORES FINALES',49,NULL,'',''),(48,'RECIBO FACTURA A  REGIMEN DE FACTURA DE CREDITO ',50,NULL,'',''),(49,'FACTURAS M',51,NULL,'',''),(50,'NOTAS DE DEBITO M',52,NULL,'',''),(51,'NOTAS DE CREDITO M',53,NULL,'',''),(52,'RECIBOS M',54,NULL,'',''),(53,'NOTAS DE VENTA AL CONTADO M',55,NULL,'',''),(54,'COMPROBANTES M DEL ANEXO I  APARTADO A  INC F) R.G. N° 1415',56,NULL,'',''),(55,'OTROS COMPROBANTES M QUE CUMPLAN CON LA R.G. N° 1415',57,NULL,'',''),(56,'CUENTAS DE VENTA Y LIQUIDO PRODUCTO M',58,NULL,'',''),(57,'LIQUIDACIONES M',59,NULL,'',''),(58,'CUENTAS DE VENTA Y LIQUIDO PRODUCTO A',60,NULL,'',''),(59,'CUENTAS DE VENTA Y LIQUIDO PRODUCTO B',61,NULL,'',''),(60,'LIQUIDACIONES A',63,NULL,'',''),(61,'LIQUIDACIONES B',64,NULL,'',''),(62,'DESPACHO DE IMPORTACION',66,NULL,'',''),(63,'LIQUIDACION C',68,NULL,'',''),(64,'RECIBOS FACTURA DE CREDITO',70,NULL,'',''),(65,'INFORME DIARIO DE CIERRE (ZETA) - CONTROLADORES FISCALES',80,NULL,'',''),(66,'TIQUE FACTURA A   ',81,65,'',''),(67,'TIQUE FACTURA B',82,66,'',''),(68,'TIQUE',83,NULL,'',''),(69,'REMITO ELECTRONICO',88,NULL,'',''),(70,'RESUMEN DE DATOS',89,NULL,'',''),(71,'OTROS COMPROBANTES - DOCUMENTOS EXCEPTUADOS - NOTAS DE CREDITO',90,NULL,'',''),(72,'REMITOS R',91,NULL,'',''),(73,'OTROS COMPROBANTES QUE NO CUMPLEN O ESTÁN EXCEPTUADOS DE LA R.G. 1415 Y SUS MODIF ',99,NULL,'',''),(74,'TIQUE NOTA DE CREDITO ',110,NULL,'',''),(75,'TIQUE FACTURA C',111,84,'',''),(76,'TIQUE NOTA DE CREDITO A',112,NULL,'',''),(77,'TIQUE NOTA DE CREDITO B',113,NULL,'',''),(78,'TIQUE NOTA DE CREDITO C',114,NULL,'',''),(79,'TIQUE NOTA DE DEBITO A',115,50,'',''),(80,'TIQUE NOTA DE DEBITO B',116,51,'',''),(81,'TIQUE NOTA DE DEBITO C',117,NULL,'',''),(82,'TIQUE FACTURA M',118,NULL,'',''),(83,'TIQUE NOTA DE CREDITO M',119,NULL,'',''),(84,'TIQUE NOTA DE DEBITO M',120,NULL,'',''),(85,'LIQUIDACION SECUNDARIA DE GRANOS',331,NULL,'',''),(86,'CERTIFICACION ELECTRONICA (GRANOS)',332,NULL,'','');

#
# Structure for table "afip_tipoconcepto"
#

DROP TABLE IF EXISTS `afip_tipoconcepto`;
CREATE TABLE `afip_tipoconcepto` (
  `idTipoconcepto` int(11) unsigned NOT NULL DEFAULT 0,
  `descripcion` varchar(250) DEFAULT NULL,
  `fchdesde` varchar(50) DEFAULT NULL,
  `fchhasta` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idTipoconcepto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "afip_tipoconcepto"
#


#
# Structure for table "afip_tipodoc"
#

DROP TABLE IF EXISTS `afip_tipodoc`;
CREATE TABLE `afip_tipodoc` (
  `idTipodoc` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) DEFAULT NULL,
  `wsfe` int(11) DEFAULT NULL,
  `hasar` int(11) DEFAULT NULL,
  `fchdesde` varchar(50) DEFAULT NULL,
  `fchhasta` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idTipodoc`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

#
# Data for table "afip_tipodoc"
#

INSERT INTO `afip_tipodoc` VALUES (1,'CI Policía Federal',0,52,'20080725',NULL),(2,'CI Buenos Aires',1,52,'20080725',NULL),(3,'CI Catamarca',2,52,'20080725',NULL),(4,'CI Córdoba',3,52,'20080725',NULL),(5,'CI Corrientes',4,52,'20080728',NULL),(6,'CI Entre Ríos',5,52,'20080728',NULL),(7,'CI Jujuy',6,52,'20080728',NULL),(8,'CI Mendoza',7,52,'20080728',NULL),(9,'CI La Rioja',8,52,'20080728',NULL),(10,'CI Salta',9,52,'20080728',NULL),(11,'CI San Juan',10,52,'20080728',NULL),(12,'CI San Luis',11,52,'20080728',NULL),(13,'CI Santa Fe',12,52,'20080728',NULL),(14,'CI Santiago del Estero',13,52,'20080728',NULL),(15,'CI Tucumán',14,52,'20080728',NULL),(16,'CI Chaco',16,52,'20080728',NULL),(17,'CI Chubut',17,52,'20080728',NULL),(18,'CI Formosa',18,52,'20080728',NULL),(19,'CI Misiones',19,52,'20080728',NULL),(20,'CI Neuquén',20,52,'20080728',NULL),(21,'CI La Pampa',21,52,'20080728',NULL),(22,'CI Río Negro',22,52,'20080728',NULL),(23,'CI Santa Cruz',23,52,'20080728',NULL),(24,'CI Tierra del Fuego',24,52,'20080728',NULL),(25,'CUIT',80,67,'20080725',NULL),(26,'CUIL',86,76,'20080725',NULL),(27,'CDI',87,NULL,'20080725',NULL),(28,'LE',89,48,'20080725',NULL),(29,'LC',90,49,'20080725',NULL),(30,'CI Extranjera',91,52,'20080725',NULL),(31,'en trámite',92,NULL,'20080725',NULL),(32,'Acta Nacimiento',93,NULL,'20080725',NULL),(33,'Pasaporte',94,51,'20080725',NULL),(34,'CI Bs. As. RNP',95,52,'20080725',NULL),(35,'DNI',96,50,'20080725',NULL),(36,'Doc. (otro)',99,32,'20080728',NULL);

#
# Structure for table "afip_tipoiva"
#

DROP TABLE IF EXISTS `afip_tipoiva`;
CREATE TABLE `afip_tipoiva` (
  `idTipoiva` int(11) unsigned NOT NULL DEFAULT 0,
  `descripcion` varchar(250) DEFAULT NULL,
  `fchdesde` varchar(50) DEFAULT NULL,
  `fchhasta` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idTipoiva`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "afip_tipoiva"
#

INSERT INTO `afip_tipoiva` VALUES (1,'No Gravado',NULL,NULL),(2,'Exento',NULL,NULL),(3,'0','20090220',NULL),(4,'10.5','20090220',NULL),(5,'21','20090220',NULL),(6,'27','20090220',NULL),(8,'5','20141009',NULL),(9,'2.5','20141009',NULL);

#
# Structure for table "afip_tipomoneda"
#

DROP TABLE IF EXISTS `afip_tipomoneda`;
CREATE TABLE `afip_tipomoneda` (
  `idTipomoneda` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(10) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `fchdesde` varchar(50) DEFAULT NULL,
  `fchhasta` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idTipomoneda`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

#
# Data for table "afip_tipomoneda"
#

INSERT INTO `afip_tipomoneda` VALUES (1,'PES','PESOS ARGENTINOS',NULL,NULL),(2,'DOL','DOLAR ESTADOUNIDENSE',NULL,NULL),(3,'002','DOLAR LIBRE EEUU',NULL,NULL),(4,'007','FLORINES HOLANDESES',NULL,NULL),(5,'009','FRANCO SUIZO',NULL,NULL),(6,'010','PESOS MEJICANOS',NULL,NULL),(7,'011','PESOS URUGUAYOS',NULL,NULL),(8,'012','REAL',NULL,NULL),(9,'014','CORONAS DANESAS',NULL,NULL),(10,'015','CORONAS NORUEGAS',NULL,NULL),(11,'016','CORONAS SUECAS',NULL,NULL),(12,'018','DOLAR CANADIENSE',NULL,NULL),(13,'019','YENS',NULL,NULL),(14,'021','LIBRA ESTERLINA',NULL,NULL),(15,'023','BOLIVAR VENEZOLANO',NULL,NULL),(16,'024','CORONA CHECA',NULL,NULL),(17,'025','DINAR YUGOSLAVO',NULL,NULL),(18,'026','DOLAR AUSTRALIANO',NULL,NULL),(19,'027','DRACMA GRIEGO',NULL,NULL),(20,'028','FLORIN  ANTILLAS HOLANDESAS',NULL,NULL),(21,'029','GUARANI',NULL,NULL),(22,'030','SHEKEL  ISRAEL',NULL,NULL),(23,'031','PESO BOLIVIANO',NULL,NULL),(24,'032','PESO COLOMBIANO',NULL,NULL),(25,'033','PESO CHILENO',NULL,NULL),(26,'034','RAND SUDAFRICANO',NULL,NULL),(27,'035','NUEVO SOL PERUANO',NULL,NULL),(28,'036','SUCRE ECUATORIANO',NULL,NULL),(29,'039','NUEVO LEV BULGARO',NULL,NULL),(30,'040','LEI RUMANO',NULL,NULL),(31,'041','DERECHOS ESPECIALES DE GIRO',NULL,NULL),(32,'042','PESO DOMINICANO',NULL,NULL),(33,'043','BALBOAS PANAMENAS',NULL,NULL),(34,'044','CORDOBA NICARAGUENSE',NULL,NULL),(35,'045','DIRHAM MARROQUI',NULL,NULL),(36,'046','LIBRA EGIPCIA',NULL,NULL),(37,'047','RIYAL SAUDITA',NULL,NULL),(38,'049','GRAMOS DE ORO FINO',NULL,NULL),(39,'051','DOLAR DE HONG KONG',NULL,NULL),(40,'052','DOLAR DE SINGAPUR',NULL,NULL),(41,'053','DOLAR DE JAMAICA',NULL,NULL),(42,'054','DOLAR DE TAIWAN',NULL,NULL),(43,'055','QUETZAL GUATEMALTECO',NULL,NULL),(44,'056','FORINT  HUNGRIA',NULL,NULL),(45,'057','BAHT  TAILANDIA',NULL,NULL),(46,'059','DINAR KUWAITI',NULL,NULL),(47,'060','EURO',NULL,NULL),(48,'061','ZLOTY POLACO',NULL,NULL),(49,'062','RUPIA HINDU',NULL,NULL),(50,'063','LEMPIRA HONDURENA',NULL,NULL),(51,'064','YUAN  REP  POP  CHINA',NULL,NULL);

#
# Structure for table "afip_tipopaises"
#

DROP TABLE IF EXISTS `afip_tipopaises`;
CREATE TABLE `afip_tipopaises` (
  `idTipopaises` int(11) unsigned NOT NULL DEFAULT 0,
  `descripcion` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idTipopaises`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "afip_tipopaises"
#


#
# Structure for table "afip_tiporesponsable"
#

DROP TABLE IF EXISTS `afip_tiporesponsable`;
CREATE TABLE `afip_tiporesponsable` (
  `idTiporesponsable` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) DEFAULT NULL,
  `wsfe` int(11) DEFAULT NULL,
  `hasar` int(11) DEFAULT NULL,
  PRIMARY KEY (`idTiporesponsable`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

#
# Data for table "afip_tiporesponsable"
#

INSERT INTO `afip_tiporesponsable` VALUES (1,'IVA Responsable Inscripto',1,73),(2,'IVA Responsable no Inscripto',2,78),(3,'IVA no Responsable',3,65),(4,'\r\nIVA Sujeto Exento',4,69),(5,'Consumidor Final',5,67),(6,'Responsable Monotributo',6,77),(7,'Sujeto no Categorizado',7,84),(8,'Proveedor del Exterior',8,NULL),(9,'Cliente del Exterior',9,NULL),(10,'IVA Liberado – Ley Nº 19.640',10,NULL),(11,'IVA Responsable Inscripto – Agente de Percepción',11,NULL),(12,'Pequeño Contribuyente Eventual',12,86),(13,'Monotributista Social',13,83),(14,'Pequeño Contribuyente Eventual Social',14,87);

#
# Structure for table "afip_datos"
#

DROP TABLE IF EXISTS `afip_datos`;
CREATE TABLE `afip_datos` (
  `idDatos` int(11) NOT NULL AUTO_INCREMENT,
  `idTiporesponsable` int(11) unsigned NOT NULL DEFAULT 1,
  `idTipodoc` int(11) unsigned NOT NULL DEFAULT 1,
  `IvaDocumento` varchar(50) NOT NULL DEFAULT '',
  `nombre` varchar(255) DEFAULT NULL,
  `topeDiario` double(11,2) DEFAULT NULL,
  `minimo` int(11) DEFAULT NULL,
  `maximo` int(11) DEFAULT NULL,
  `automatizacion` int(1) NOT NULL DEFAULT 1,
  `horaZ` int(11) DEFAULT NULL,
  `minutoZ` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDatos`),
  KEY `fk_afipdatos_afiptipodoc` (`idTipodoc`),
  KEY `fk_afipdatos_afiptiporesponsable` (`idTiporesponsable`),
  CONSTRAINT `fk_afipdatos_afiptipodoc` FOREIGN KEY (`idTipodoc`) REFERENCES `afip_tipodoc` (`idTipodoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_afipdatos_afiptiporesponsable` FOREIGN KEY (`idTiporesponsable`) REFERENCES `afip_tiporesponsable` (`idTiporesponsable`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "afip_datos"
#

INSERT INTO `afip_datos` VALUES (1,6,25,'20239314075','EDUARDO FORMOSO',5000.00,10,30,0,23,30);

#
# Structure for table "afip_tipotributo"
#

DROP TABLE IF EXISTS `afip_tipotributo`;
CREATE TABLE `afip_tipotributo` (
  `idTipotributo` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(250) DEFAULT NULL,
  `fchdesde` varchar(50) DEFAULT NULL,
  `fchhasta` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idTipotributo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "afip_tipotributo"
#


#
# Structure for table "caja"
#

DROP TABLE IF EXISTS `caja`;
CREATE TABLE `caja` (
  `idCaja` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT '',
  `total` double(10,2) DEFAULT NULL,
  `diferencia` double(10,2) DEFAULT NULL,
  `fecha` varchar(20) DEFAULT NULL,
  `hora` varchar(20) DEFAULT '00:00:00',
  `id_usuario` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idCaja`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

#
# Data for table "caja"
#

INSERT INTO `caja` VALUES (1,'APERTURA',250.00,NULL,'16-11-2020','17:59:37',1),(2,'CIERRE',1488.00,0.00,'16-11-2020','19:03:06',1),(3,'APERTURA',250.00,NULL,'16-11-2020','19:13:44',1),(4,'CIERRE',250.00,0.00,'16-11-2020','19:14:32',1);

#
# Structure for table "categorias"
#

DROP TABLE IF EXISTS `categorias`;
CREATE TABLE `categorias` (
  `idCategorias` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`idCategorias`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# Data for table "categorias"
#

INSERT INTO `categorias` VALUES (1,'Categoria'),(2,'A');

#
# Structure for table "clientes"
#

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `idClientes` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idTiporesponsable` int(11) unsigned NOT NULL DEFAULT 1,
  `idTipodoc` int(11) unsigned NOT NULL DEFAULT 1,
  `IvaDocumento` varchar(50) NOT NULL DEFAULT '',
  `nombre` varchar(255) DEFAULT NULL,
  `domicilio` varchar(255) DEFAULT NULL,
  `localidad` varchar(200) DEFAULT NULL,
  `provincia` varchar(200) DEFAULT NULL,
  `margen` int(11) DEFAULT NULL,
  `persona` varchar(200) DEFAULT NULL,
  `telcel` varchar(150) DEFAULT NULL,
  `telfijo` varchar(150) DEFAULT NULL,
  `mail` varchar(200) DEFAULT NULL,
  `observacion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idClientes`),
  KEY `fk_clientes_tiporesponsable` (`idTiporesponsable`),
  KEY `fk_clientes_tipodoc` (`idTipodoc`),
  CONSTRAINT `fk_clientes_tipodoc` FOREIGN KEY (`idTipodoc`) REFERENCES `afip_tipodoc` (`idTipodoc`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_clientes_tiporesponsable` FOREIGN KEY (`idTiporesponsable`) REFERENCES `afip_tiporesponsable` (`idTiporesponsable`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

#
# Data for table "clientes"
#

INSERT INTO `clientes` VALUES (1,5,36,'','CONSUMIDOR FINAL','XX',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(2,1,25,'33711620449','MARTIN FORMOSO ING.SRL.','SAN LORENZO 2413',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(3,5,36,'2','MELLACE MAXIMO',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3814731189',NULL,NULL,NULL),(4,1,25,'30716275023','ARQUITECTURA ZEBALLOS','AV. PERON 1700 -YB 2 OF 5',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(5,1,25,'33715305599','FIDEICOMISO TORRE JUNIN 615','SAN LORENZO 1183',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(6,1,25,'33714743029','ECOCLIMA SRL.','VENEZUELA 2930',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3815988680',NULL,NULL,NULL),(7,5,36,'','ABACA SEBASTIAN',' Bº   BARRANCA DEL SALI',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(8,5,36,'','PAPA PABLO','VENEZUELA 3452',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3814181839',NULL,NULL,NULL),(9,1,25,'30713851600','LAS ALCOVER SRL.','AV. GOBERNADOR DEL CAMPO',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(10,5,36,'','DANIEL ESTEBAN ARQ.','ALTO VERDE','YERBA BUENA','TUCUMAN',0,'0','3814128128',NULL,NULL,NULL),(11,5,36,'','RUIZ GUSTAVO','PJE HOLBER 3501',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3816049971',NULL,NULL,NULL),(12,5,36,'','EDUARDO CARRION','ARAGON 175',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(13,5,36,'','OVEJERO JULIO - MELLACE',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3813623386',NULL,NULL,NULL),(14,5,36,'','PANTOJA JAVIER ARQ.','CONGRESO 900',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(15,5,36,'','CAMPOPIANO CESAR ARQ.','LOMAS SEC 15 MAZ 10 CAS 25',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(16,5,36,'','JULIO DANIEL',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3816011728',NULL,NULL,NULL),(17,1,25,'30679232106','O.S.P.S.A.','DEAN FUNES 1242','BUENOS AIRES','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(18,5,36,'','KRAUS GUSTAVO','AV MATE DE LUNA 1400',' SAN MIGUEL DE TUCUMAN','BUENOS AIRES ( BS. AS )',0,'0',NULL,NULL,NULL,NULL),(19,5,36,'','BERTA JULIO','AV JUJUY 1800',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3815192787',NULL,NULL,NULL),(20,5,36,'1','NOR URBANA',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(21,5,36,'','IOCA FERNANDO','BARRIO 200 VIVI. CASA 7 CALLE 12','LAS TALITAS','TUCUMAN',0,'0','3815318603',NULL,NULL,NULL),(22,5,36,'','DILASCIO MARTA ARQ.',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(23,5,36,'','PASEO SHOPING','AV ACONQUIJA 1800','YERBA BUENA','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(24,5,36,'','ARCE ARISTIDES','SAN JUAN 2393',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(25,5,36,'','BASELGA JOSE','AV MATE DE LUNA 1600',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(26,5,36,'','DIRECCION DE MANT. SIPROSA','ITALIA 1919',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(27,5,36,'','SARMIENTO MARTIN',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(28,5,36,'','BARRIENTO - GRINGO','AV. EJERCIO DEL NORTE Y COLOMBIA',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(29,5,36,'','LLAMUR ARQ.',NULL,'LAS TALITAS','TUCUMAN',0,'0','3815393519',NULL,NULL,NULL),(30,5,36,'','RITORTO MIGUEL','PRINGLES 1306 - SMT',' SAN MIGUEL DE TUCUMAN','SANTIAGO DEL ESTERO',0,'0','3815048841',NULL,NULL,NULL),(31,5,36,'','CANO DIEGO','CORRIENTES 3936',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3815940973',NULL,NULL,NULL),(32,5,36,'','DIAZ PABLO','COLOMBIA 2686',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3813515496',NULL,NULL,NULL),(33,5,36,'','BERRONDO OSVALDO','YERBA BUIENA','YERBA BUENA (DPTO. TAFI)','TUCUMAN',0,'0','3814405050',NULL,NULL,NULL),(34,5,36,'','VALOY ADRIAN ARQ.',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3815878443',NULL,NULL,NULL),(35,5,36,'','CARNICERIA LA VICTORIA','VIAMONTE Y COLOMBIA',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(36,5,36,'','COLEGIO MONTSERRAT','COLOMBIA 2937',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(37,5,36,'','ESTIGARRIBIA STELA ARQ.','BULNES 1506 ESQ. PERU',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3815846746',NULL,NULL,NULL),(38,5,36,'','LECCESI JOSE LUIS -(ING)',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3816205908',NULL,NULL,NULL),(39,5,36,'','RIVERO LEOPOLDO','MANUEL ESTRADA 2435',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3813679522',NULL,NULL,NULL),(40,5,36,'','FERNANDEZ CAROLINA','LOMAS 600 - MAZ 31 - CASA 29     3','TAFI VIEJO','TUCUMAN',0,'0','3815090139',NULL,NULL,NULL),(41,1,25,'20338843985','BLAS LOPEZ DIEGO JOSE','PADRE ROQUE CORREA 2233',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(42,5,36,'','CATALAN MIGUEL',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3815902676',NULL,NULL,NULL),(43,5,36,'','ZOTELO JUAN',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3816021255',NULL,NULL,NULL),(44,5,36,'','CLIENTES VARIOS',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(45,5,36,'','GARCIA FERNANDO',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(46,5,36,'','NASTIQUE CARLOS',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(47,5,36,'','POLICIA DE TUCUMAN',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(48,5,36,'','HORACIO - NORTELUZ','ITALIA  2132',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(49,5,36,'','JABIF JULIAN',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(50,5,36,'','BLANCH MATIAS ARQ.',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(51,1,25,'30546670240','UNIVERSIDAD NACIONAL DE TUCUMAN','AYACUCHO 444',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(52,1,25,'30675728081','SUPERIOR GOBIERNO DE LA PCIA DE TUC','ESC. N 300 - CABO QUIPILDOR','TAFI VIEJO','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(53,5,36,'','SOL YANIES (ARQ)',NULL,'YERBA BUENA','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(54,5,36,'','ROTGER MARTIN','EL CADILLAL',' SAN MIGUEL DE TUCUMAN','CATAMARCA',0,'0',NULL,NULL,NULL,NULL),(55,5,36,'','SAAVEDRA ALVARO',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3816682334',NULL,NULL,NULL),(56,5,36,'','AMADO JOSE ING.',NULL,'TUCUMAN','TUCUMAN',0,'0','3814161550',NULL,NULL,NULL),(57,5,36,'','VERGARA SERGIO','LAS QUINTAS 1 LOTE N 9',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(58,5,36,'','OMAR CUEVAS',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(59,5,36,'','BARRIENTOS MARTIN - LA MANSA',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(60,5,36,'','ALEJANDRA','VENEZUELA 3077',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3815232982',NULL,NULL,NULL),(61,5,36,'','PAEZ ADRIAN','PJE TAGLE 3067',' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3814010384',NULL,NULL,NULL),(62,5,36,'','ACEVEDO ALEJANDRO (ARQ.)',NULL,' SAN MIGUEL DE TUCUMAN','TUCUMAN',0,'0','3815311619',NULL,NULL,NULL),(63,0,0,'',NULL,NULL,NULL,'TUCUMAN',0,'0',NULL,NULL,NULL,NULL),(64,1,35,'34324324','asdasf','DSFDSFSDFSD','FSDFSDF','SDFDSFDSF',3434343,'A','4','4','a','');

#
# Structure for table "cuentacorriente"
#

DROP TABLE IF EXISTS `cuentacorriente`;
CREATE TABLE `cuentacorriente` (
  `idCuentaCorriente` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` varchar(50) DEFAULT NULL,
  `estado` varchar(30) DEFAULT NULL,
  `idClientes` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`idCuentaCorriente`),
  UNIQUE KEY `fk_cuentacorriente_clientes` (`idClientes`),
  CONSTRAINT `fk_cuentacorriente_clientes` FOREIGN KEY (`idClientes`) REFERENCES `clientes` (`idClientes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "cuentacorriente"
#

INSERT INTO `cuentacorriente` VALUES (1,'16-11-2020','HABILITADO',17);

#
# Structure for table "deposito"
#

DROP TABLE IF EXISTS `deposito`;
CREATE TABLE `deposito` (
  `idDepositos` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idDepositos`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# Data for table "deposito"
#

INSERT INTO `deposito` VALUES (1,'DEPOSITO 1',NULL);

#
# Structure for table "detalledecuentacorriente"
#

DROP TABLE IF EXISTS `detalledecuentacorriente`;
CREATE TABLE `detalledecuentacorriente` (
  `idCtaCteDetalle` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idCuentaCorriente` int(11) unsigned NOT NULL DEFAULT 0,
  `idPedidos` int(11) unsigned DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `debe` double(10,2) NOT NULL DEFAULT 0.00,
  `haber` double(10,2) NOT NULL DEFAULT 0.00,
  `fecha` varchar(50) NOT NULL,
  `detalle` varchar(255) DEFAULT NULL,
  `id_caja` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`idCtaCteDetalle`),
  KEY `fk_ctactedetalle_ctecte` (`idCuentaCorriente`),
  KEY `fk_ctactedetalle_pedidos` (`idPedidos`),
  CONSTRAINT `fk_ctactedetalle_ctecte` FOREIGN KEY (`idCuentaCorriente`) REFERENCES `cuentacorriente` (`idCuentaCorriente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "detalledecuentacorriente"
#

INSERT INTO `detalledecuentacorriente` VALUES (1,1,NULL,'INICIO DE ACTIVIDAD',0.00,0.00,'16-11-2020',NULL,0);

#
# Structure for table "dolar"
#

DROP TABLE IF EXISTS `dolar`;
CREATE TABLE `dolar` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `cotizacion` double(6,2) NOT NULL DEFAULT 0.00,
  `fecha` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# Data for table "dolar"
#

INSERT INTO `dolar` VALUES (1,69.47,'2020-05-09'),(2,92.30,'2020-05-09');

#
# Structure for table "egresos"
#

DROP TABLE IF EXISTS `egresos`;
CREATE TABLE `egresos` (
  `idEgreso` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL DEFAULT '',
  `total` double(10,2) NOT NULL DEFAULT 0.00,
  `fecha` varchar(10) DEFAULT '31-12-2019',
  `hora` varchar(20) DEFAULT '00:00:00',
  `idCaja` int(11) unsigned NOT NULL DEFAULT 0,
  `id_usuario` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idEgreso`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

#
# Data for table "egresos"
#

INSERT INTO `egresos` VALUES (1,'ASFDSFDS',23242.00,'16-11-2020','19:01:04',1,1);

#
# Structure for table "formasdepago"
#

DROP TABLE IF EXISTS `formasdepago`;
CREATE TABLE `formasdepago` (
  `idFormasDePago` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `descuento` double(11,2) DEFAULT 0.00,
  `signo` varchar(10) NOT NULL DEFAULT 'NEGATIVO',
  PRIMARY KEY (`idFormasDePago`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Data for table "formasdepago"
#

INSERT INTO `formasdepago` VALUES (1,'CONTADO',0.00,'NEGATIVO'),(2,'CUENTA CORRIENTE',0.00,'NEGATIVO'),(3,'TARJETA DE CREDITO',0.00,'POSITIVO'),(4,'TARJETA DE DEBITO',0.00,'POSITIVO'),(5,'CHEQUE',0.00,'POSITIVO'),(6,'OTROS',3.00,'NEGATIVO');

#
# Structure for table "formasdepago_cheque"
#

DROP TABLE IF EXISTS `formasdepago_cheque`;
CREATE TABLE `formasdepago_cheque` (
  `idFormaPago_Cheque` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) DEFAULT NULL,
  `descuento` double(11,2) DEFAULT 0.00,
  PRIMARY KEY (`idFormaPago_Cheque`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

#
# Data for table "formasdepago_cheque"
#

INSERT INTO `formasdepago_cheque` VALUES (1,'Cheque a 7 Dias',7000.00),(2,'Cheque a 15 Dias',0.00),(3,'Cheque a 30 Dias',10.00),(4,'90 DIAS',10.00),(5,'DOLAR',7000.00);

#
# Structure for table "formasdepago_ctacte"
#

DROP TABLE IF EXISTS `formasdepago_ctacte`;
CREATE TABLE `formasdepago_ctacte` (
  `idFormaPago_CtaCte` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) DEFAULT NULL,
  `descuento` double(11,2) DEFAULT 0.00,
  PRIMARY KEY (`idFormaPago_CtaCte`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "formasdepago_ctacte"
#

INSERT INTO `formasdepago_ctacte` VALUES (1,'Cuenta Corriente a 30 Dias',0.00),(2,'Cuenta Corriente a 15 Dias',7000.00),(3,'Cuenta Corriente a 7 Dias',0.00);

#
# Structure for table "marcas"
#

DROP TABLE IF EXISTS `marcas`;
CREATE TABLE `marcas` (
  `idMarcas` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`idMarcas`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

#
# Data for table "marcas"
#

INSERT INTO `marcas` VALUES (1,'Varios'),(2,'A');

#
# Structure for table "nota_de_credito"
#

DROP TABLE IF EXISTS `nota_de_credito`;
CREATE TABLE `nota_de_credito` (
  `idNotaDeCredito` int(11) unsigned NOT NULL DEFAULT 0,
  `fecha` datetime DEFAULT NULL,
  `total` double(10,2) DEFAULT NULL,
  `iva` double DEFAULT NULL,
  `ingresosBrutos` double DEFAULT NULL,
  `porcentajeIva` double DEFAULT NULL,
  `idFacturacion` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`idNotaDeCredito`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "nota_de_credito"
#


#
# Structure for table "pedidos"
#

DROP TABLE IF EXISTS `pedidos`;
CREATE TABLE `pedidos` (
  `idPedidos` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `total` double(11,2) unsigned DEFAULT NULL,
  `fecha` varchar(50) DEFAULT NULL,
  `hora` varchar(50) DEFAULT NULL,
  `idClientes` int(11) unsigned NOT NULL DEFAULT 0,
  `subtotal` double(11,2) NOT NULL DEFAULT 0.00,
  `estado` varchar(10) DEFAULT NULL,
  `idCaja` int(11) unsigned NOT NULL DEFAULT 1,
  `observacion` varchar(255) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idPedidos`),
  KEY `fk_pedidos_clientes` (`idClientes`),
  KEY `fk_pedidos_caja` (`idCaja`),
  CONSTRAINT `fk_pedidos_caja` FOREIGN KEY (`idCaja`) REFERENCES `caja` (`idCaja`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pedidos_clientes` FOREIGN KEY (`idClientes`) REFERENCES `clientes` (`idClientes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "pedidos"
#

INSERT INTO `pedidos` VALUES (1,12150.00,'16-11-2020','18:03:53',1,12150.00,NULL,1,'',1),(2,1080.00,'16-11-2020','18:07:11',1,1080.00,NULL,1,'',1),(3,11250.00,'16-11-2020','18:11:24',1,11250.00,'ANULADO',1,'',1);

#
# Structure for table "pedidos_tienen_formasdepago"
#

DROP TABLE IF EXISTS `pedidos_tienen_formasdepago`;
CREATE TABLE `pedidos_tienen_formasdepago` (
  `idFormasDePago` int(11) unsigned NOT NULL DEFAULT 0,
  `idPedidos` int(11) unsigned NOT NULL DEFAULT 0,
  `descuento` double(11,2) DEFAULT 0.00,
  `descuentobase` double(11,2) DEFAULT 0.00,
  `descuentoporcentaje` double(11,2) DEFAULT 0.00,
  `subtotal` double(11,2) DEFAULT 0.00,
  `idTarjeta` int(11) unsigned DEFAULT 0,
  `interes` double(7,2) NOT NULL DEFAULT 0.00,
  `interesporcentaje` double(11,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`idFormasDePago`,`idPedidos`),
  KEY `fk_formasdepago_has_pedidos_pedidos1_idx` (`idPedidos`),
  KEY `fk_formasdepago_has_pedidos_formasdepago1_idx` (`idFormasDePago`),
  CONSTRAINT `fk_formasdepago_has_pedidos_formasdepago1` FOREIGN KEY (`idFormasDePago`) REFERENCES `formasdepago` (`idFormasDePago`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_formasdepago_has_pedidos_pedidos1` FOREIGN KEY (`idPedidos`) REFERENCES `pedidos` (`idPedidos`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "pedidos_tienen_formasdepago"
#

INSERT INTO `pedidos_tienen_formasdepago` VALUES (1,1,0.00,0.00,0.00,0.00,0,0.00,0.00),(1,2,0.00,0.00,0.00,0.00,0,0.00,0.00),(1,3,0.00,0.00,0.00,0.00,0,0.00,0.00);

#
# Structure for table "presupuestos"
#

DROP TABLE IF EXISTS `presupuestos`;
CREATE TABLE `presupuestos` (
  `idPresupuestos` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idClientes` int(11) unsigned NOT NULL DEFAULT 0,
  `total` double(11,2) unsigned DEFAULT NULL,
  `fecha` varchar(50) DEFAULT NULL,
  `hora` varchar(50) DEFAULT NULL,
  `subtotal` double(11,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`idPresupuestos`),
  KEY `fk_presupuestos_clientes` (`idClientes`),
  CONSTRAINT `fk_presupuestos_clientes` FOREIGN KEY (`idClientes`) REFERENCES `clientes` (`idClientes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "presupuestos"
#


#
# Structure for table "presupuestos_tienen_formasdepago"
#

DROP TABLE IF EXISTS `presupuestos_tienen_formasdepago`;
CREATE TABLE `presupuestos_tienen_formasdepago` (
  `idPresupuestos` int(11) unsigned NOT NULL DEFAULT 0,
  `idFormasDePago` int(11) unsigned NOT NULL DEFAULT 0,
  `descuento` double(11,2) DEFAULT 0.00,
  `descuentobase` double(11,2) DEFAULT 0.00,
  `descuentoporcentaje` double(11,2) DEFAULT 0.00,
  PRIMARY KEY (`idPresupuestos`,`idFormasDePago`),
  KEY `fk_presupuestos_has_formasdepago_formasdepago1_idx` (`idFormasDePago`),
  KEY `fk_presupuestos_has_formasdepago_presupuestos1_idx` (`idPresupuestos`),
  CONSTRAINT `fk_presupuestos_has_formasdepago_formasdepago1` FOREIGN KEY (`idFormasDePago`) REFERENCES `formasdepago` (`idFormasDePago`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_presupuestos_has_formasdepago_presupuestos1` FOREIGN KEY (`idPresupuestos`) REFERENCES `presupuestos` (`idPresupuestos`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "presupuestos_tienen_formasdepago"
#


#
# Structure for table "productos"
#

DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `idProductos` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `minimo` int(10) DEFAULT NULL,
  `unidad` varchar(100) DEFAULT NULL,
  `idCategorias` int(11) unsigned NOT NULL DEFAULT 0,
  `idMarcas` int(11) unsigned DEFAULT NULL,
  `moneda` varchar(5) NOT NULL DEFAULT 'Peso',
  PRIMARY KEY (`idProductos`),
  KEY `fk_productos_categorias` (`idCategorias`),
  KEY `fk_productos_marcas` (`idMarcas`),
  CONSTRAINT `fk_productos_categorias` FOREIGN KEY (`idCategorias`) REFERENCES `categorias` (`idCategorias`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_marcas` FOREIGN KEY (`idMarcas`) REFERENCES `marcas` (`idMarcas`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1235 DEFAULT CHARSET=utf8;

#
# Data for table "productos"
#

INSERT INTO `productos` VALUES (1,'1','CEMENTO HOLCIM X 50 KG',NULL,10,'UNIDAD',1,1,'Peso'),(2,'2','CEMENTO LOMA NEGRA X 50 KG',NULL,10,'UNIDAD',1,1,'Peso'),(3,'3','HERCAL X 40 KG',NULL,10,'UNIDAD',1,1,'Peso'),(4,'4','PLASTICOR  X 40 KG',NULL,10,'UNIDAD',1,1,'Peso'),(5,'5','CAL HIDRATADA X 20  SANTA ELENA',NULL,10,'UNIDAD',1,1,'Peso'),(6,'6','YESO ESFINGE X 40 KG',NULL,10,'UNIDAD',1,1,'Peso'),(7,'7','WEBER REV FINO X 25 KG',NULL,10,'UNIDAD',1,1,'Peso'),(8,'8','WEBER COL BASIC IMP. X 30 KG',NULL,10,'UNIDAD',1,1,'Peso'),(9,'9','PUNTA 30 - 35 CM ECO -DIEGO',NULL,10,'UNIDAD',1,1,'Peso'),(10,'10','PUNTA 35 CM - ECO',NULL,10,'UNIDAD',1,1,'Peso'),(11,'11','CORTA HIERRO 35 CM - ECO DIEGO',NULL,10,'UNIDAD',1,1,'Peso'),(12,'12','METRO DE MADERA SIMPLE (RACORT)',NULL,10,'UNIDAD',1,1,'Peso'),(13,'13','METRO DE MADERA DOBLE ( RACOT)',NULL,10,'UNIDAD',1,1,'Peso'),(14,'14','SERRUCHO 50 CM M/MAD CROSSMAN',NULL,10,'UNIDAD',1,1,'Peso'),(15,'17','HIERRO TORSIONADO 6 MM -12 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(16,'18','HIERRO TORSIONADO 8 MM -12 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(17,'19','HIERRO TORSIONADO 10  MM-12 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(18,'20','HIERRO TORSIONADO 12 MM-12 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(19,'21','HIERRO TORSIONADO 16 MM -12 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(20,'22','HIERRO TORSIONADO 25 MM -12 M',NULL,10,'UNIDAD',1,1,'Peso'),(21,'23','ALAMBRE NEGRO RET. Nº 16',NULL,10,'UNIDAD',1,1,'Peso'),(22,'24','MALLA S. Q131 15X15X5MM (6X2.40 M',NULL,10,'UNIDAD',1,1,'Peso'),(23,'26','MALLA S. R131 15X25X5MM (6X2.40 MT)',NULL,10,'UNIDAD',1,1,'Peso'),(24,'27','ARENA MEDIANA X 1  MT3 P/RETIRAR',NULL,10,'UNIDAD',1,1,'Peso'),(25,'28','ARENA MEDIANA X BOLSA',NULL,10,'UNIDAD',1,1,'Peso'),(26,'29','RIPIO BRUTO FINO  X 1 MT3 P/RETIRAR',NULL,10,'UNIDAD',1,1,'Peso'),(27,'30','RIPIO BRUTO FINO X BOLSA',NULL,10,'UNIDAD',1,1,'Peso'),(28,'31','LADRILLO COMUN DE 1º X 1 UNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(29,'32','FUELLE DE GOMA EXTRA LARGO',NULL,10,'UNIDAD',1,1,'Peso'),(30,'33','LADRILLO HUECO SAL. 8X18X30 (198)',NULL,10,'UNIDAD',1,1,'Peso'),(31,'34','LADRILLO HUECO SAL. 12X18X30 (126 )',NULL,10,'UNIDAD',1,1,'Peso'),(32,'35','LADRILLO HUECO SAL. 18X18X30 (90)',NULL,10,'UNIDAD',1,1,'Peso'),(33,'36','LADRILLO HUECO 8X18X33 (198PALLET)',NULL,10,'UNIDAD',1,1,'Peso'),(34,'37','LADRILLO HUECO 12X18X33 (144PALLET)',NULL,10,'UNIDAD',1,1,'Peso'),(35,'38','LADRILLO HUECO 18X18X33 (90PALLET)',NULL,10,'UNIDAD',1,1,'Peso'),(36,'39','LADRILLO DOS TUBOS',NULL,10,'UNIDAD',1,1,'Peso'),(37,'40','VIGUETA PRETENSADA 1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(38,'41','VIGUETA PRETENSADA X 1,20',NULL,10,'UNIDAD',1,1,'Peso'),(39,'42','VIGUETA PRETENSADA X 1,40 M',NULL,10,'UNIDAD',1,1,'Peso'),(40,'43','HORMIGONERA 130 LTS 1 HP REF.',NULL,10,'UNIDAD',1,1,'Peso'),(41,'44','CARRETILLA X 65 LTS ESTANDAR',NULL,10,'UNIDAD',1,1,'Peso'),(42,'45','CARRETILLA X 65 LTS REFORZADA',NULL,10,'UNIDAD',1,1,'Peso'),(43,'46','VIGUETA PRETENSADA X 1,60 MT',NULL,10,'UNIDAD',1,1,'Peso'),(44,'47','VIGUETA PRETENSADA X 1,80  MT',NULL,10,'UNIDAD',1,1,'Peso'),(45,'48','VIGUETA PRETENSADA X 2,00MT',NULL,10,'UNIDAD',1,1,'Peso'),(46,'49','VIGUETA PRETENSADA X 2,20 M',NULL,10,'UNIDAD',1,1,'Peso'),(47,'50','VIGUETA PRETENSADA X 2,40M',NULL,10,'UNIDAD',1,1,'Peso'),(48,'51','VIGUETA PRETENSADA X 2,60 MT',NULL,10,'UNIDAD',1,1,'Peso'),(49,'52','VIGUETA PRETENSADA X 2,80 MT',NULL,10,'UNIDAD',1,1,'Peso'),(50,'53','VIGUETA PRETENSADA X 3,00 MT',NULL,10,'UNIDAD',1,1,'Peso'),(51,'54','VIGUETA PRETENSADA X 3,10 MT',NULL,10,'UNIDAD',1,1,'Peso'),(52,'55','VIGUETA PRETENSADA X 3,20 MT',NULL,10,'UNIDAD',1,1,'Peso'),(53,'56','VIGUETA PRETENSADA X 3,30  MT',NULL,10,'UNIDAD',1,1,'Peso'),(54,'57','VIGUETA PRETENSADA X 3,40 MT',NULL,10,'UNIDAD',1,1,'Peso'),(55,'58','VIGUETA PRETENSADA X 3,50 MT',NULL,10,'UNIDAD',1,1,'Peso'),(56,'59','VIGUETA PRETENSADA X 3,60 M',NULL,10,'UNIDAD',1,1,'Peso'),(57,'60','VIGUETA PRETENSADA X 3,70 MT',NULL,10,'UNIDAD',1,1,'Peso'),(58,'61','VIGUETA PRETENSADA X 3,80 M',NULL,10,'UNIDAD',1,1,'Peso'),(59,'62','VIGUETA PRETENSADA X 3,90 MT',NULL,10,'UNIDAD',1,1,'Peso'),(60,'63','VIGUETA PRETENSADA X 4,00 MT',NULL,10,'UNIDAD',1,1,'Peso'),(61,'64','VIGUETA PRETENSADA X 4,10 M',NULL,10,'UNIDAD',1,1,'Peso'),(62,'65','VIGUETA PRETENSADA X 4,20 MT',NULL,10,'UNIDAD',1,1,'Peso'),(63,'66','VIGUETA PRETENSADA X 4,30 MT',NULL,10,'UNIDAD',1,1,'Peso'),(64,'67','VIGUETA PRETENSADA X 4,50 MT',NULL,10,'UNIDAD',1,1,'Peso'),(65,'68','VIGUETA PRETENSADA X 4,60 MT',NULL,10,'UNIDAD',1,1,'Peso'),(66,'69','VIGUETA PRETENSADA X 4,70 MT',NULL,10,'UNIDAD',1,1,'Peso'),(67,'70','VIGUETA PRETENSADA X 4,80 MT',NULL,10,'UNIDAD',1,1,'Peso'),(68,'71','VIGUETA PRETENSADA X 5,00 MT',NULL,10,'UNIDAD',1,1,'Peso'),(69,'72','VIGUETA PRETENSADA X 5,20 MT',NULL,10,'UNIDAD',1,1,'Peso'),(70,'73','VIGUETA PRETENSADA X 5,40 MT',NULL,10,'UNIDAD',1,1,'Peso'),(71,'74','VIGUETA PRETENSADA X 5,60 MT',NULL,10,'UNIDAD',1,1,'Peso'),(72,'75','VIGUETA PRETENSADA X 5,80 MT',NULL,10,'UNIDAD',1,1,'Peso'),(73,'76','VIGUETA PRETENSADA X 6,00 MT',NULL,10,'UNIDAD',1,1,'Peso'),(74,'77','VIGUETA PRETENSADA X6,20 MT',NULL,10,'UNIDAD',1,1,'Peso'),(75,'78','VIGUETA PRETENSADA X 6,40  MT',NULL,10,'UNIDAD',1,1,'Peso'),(76,'79','VIGUETA PRETENSADA X 6,60 MT',NULL,10,'UNIDAD',1,1,'Peso'),(77,'80','VIGUETA PRETENSADA X6,80 MT',NULL,10,'UNIDAD',1,1,'Peso'),(78,'81','VIGUETA PRETENSADA X 7,00 MT',NULL,10,'UNIDAD',1,1,'Peso'),(79,'82','VIGUETA PRETENSADA X 7,20 MT',NULL,10,'UNIDAD',1,1,'Peso'),(80,'83','CUCHARA GHERARDI Nº 8 RED.SOLDADA',NULL,10,'UNIDAD',1,1,'Peso'),(81,'84','CUCHARA GHERARDI Nº 7 RED',NULL,10,'UNIDAD',1,1,'Peso'),(82,'85','CUCHARA GHERARDI Nº 8 COMUN SOLD.',NULL,10,'UNIDAD',1,1,'Peso'),(83,'86','CUCHARA GHERARDI Nº 7 COMUN',NULL,10,'UNIDAD',1,1,'Peso'),(84,'87','CHOCLA C/FRASCO -TINTA -NIVEL',NULL,10,'UNIDAD',1,1,'Peso'),(85,'88','FAJA LUMBAR TALLE VARIOS',NULL,10,'UNIDAD',1,1,'Peso'),(86,'89','TENAZA ARMADOR Nº 9 CROSSMAN',NULL,10,'UNIDAD',1,1,'Peso'),(87,'90','TENAZA ARMADOR Nº 12 CROSSMAN',NULL,10,'UNIDAD',1,1,'Peso'),(88,'91','PINZA PICO LORO Nº 10- CROSSMAN',NULL,10,'UNIDAD',1,1,'Peso'),(89,'92','PINZA UNIVERSAL Nº 8 - CROSSMAN',NULL,10,'UNIDAD',1,1,'Peso'),(90,'93','REMACHADORA MANUAL CROSMAN',NULL,10,'UNIDAD',1,1,'Peso'),(91,'94','SERRUCHO P/MADERA 56 CM',NULL,10,'UNIDAD',1,1,'Peso'),(92,'95','GUANTE DESCARNE CORTO',NULL,10,'UNIDAD',1,1,'Peso'),(93,'96','GUANTE DESCARNE LARGO',NULL,10,'UNIDAD',1,1,'Peso'),(94,'97','MACHETE CIRCI 50 CM',NULL,10,'UNIDAD',1,1,'Peso'),(95,'98','DISCO DEBASTE 4 1/2\"  MM',NULL,10,'UNIDAD',1,1,'Peso'),(96,'99','DISCO DE CORTE 4 1/2 X 1 GRIDEST',NULL,10,'UNIDAD',1,1,'Peso'),(97,'100','DISCO CORTE PLANO 7\" GRIDEST 1.6',NULL,10,'UNIDAD',1,1,'Peso'),(98,'101','TALADRO GAMMA 13 MM 850 W 2900 RPM',NULL,10,'UNIDAD',1,1,'Peso'),(99,'102','GANCHO J 60 MM CON ACCESORIOS',NULL,10,'UNIDAD',1,1,'Peso'),(100,'103','GANCHO J 70 MM CON ACCESORIOS',NULL,10,'UNIDAD',1,1,'Peso'),(101,'105','MANGUERA CRISTAL 9X11 - X MT',NULL,10,'UNIDAD',1,1,'Peso'),(102,'106','LAMPARA LED DITROICA 5 W',NULL,10,'UNIDAD',1,1,'Peso'),(103,'107','TORNILLO T1 PUNTA MECHA',NULL,10,'UNIDAD',1,1,'Peso'),(104,'108','TORNILLO  T2 PUNTA AGUJA',NULL,10,'UNIDAD',1,1,'Peso'),(105,'109','TORNILLO T3 PUNTA AGUJA',NULL,10,'UNIDAD',1,1,'Peso'),(106,'110','TORNILLO AUTOPERFORANTE 14X2\"',NULL,10,'UNIDAD',1,1,'Peso'),(107,'111','TORNILLO AUTOPERFORANTE 14X2 1/2\"',NULL,10,'UNIDAD',1,1,'Peso'),(108,'112','AMOLADORA ANGULAR 4 1/2\" GLADIATOR',NULL,10,'UNIDAD',1,1,'Peso'),(109,'113','DISC TURBO 4 1/2 YARD',NULL,10,'UNIDAD',1,1,'Peso'),(110,'114','GUANTE MOTEADO',NULL,10,'UNIDAD',1,1,'Peso'),(111,'115','PINTURA ASFALTICA X 1 LT CLIPPERFLE',NULL,10,'UNIDAD',1,1,'Peso'),(112,'116','PINTURA ASFALTICA X 4 LT - EMAPI',NULL,10,'UNIDAD',1,1,'Peso'),(113,'117','TIRAFONDO 1/4 X 6,5 P/ TACO DEL \"10',NULL,10,'UNIDAD',1,1,'Peso'),(114,'118','BALDE ALBAÑIL PVC C/MANIJA',NULL,10,'UNIDAD',1,1,'Peso'),(115,'119','CABO P/PALA 0.70 MT EMPU.PLASTICA A',NULL,10,'UNIDAD',1,1,'Peso'),(116,'120','CABO P/PICO X 0.90 MT ANSA',NULL,10,'UNIDAD',1,1,'Peso'),(117,'121','CABO P/MAZA DE 0.90 MT PARA 5 KG',NULL,10,'UNIDAD',1,1,'Peso'),(118,'122','PALA ANCHA ESTAMPADA E/PLASTICA',NULL,10,'UNIDAD',1,1,'Peso'),(119,'123','PALA PUNTA ESTAMPADA E/PLASTICA',NULL,10,'UNIDAD',1,1,'Peso'),(120,'124','PALA ANCHA FORJADA BIASSONI C/CORTO',NULL,10,'UNIDAD',1,1,'Peso'),(121,'125','PALA PUNTA FORJADA BIASSONI C/CORT',NULL,10,'UNIDAD',1,1,'Peso'),(122,'126','MANDIL PLASTICO 12X25 CM',NULL,10,'UNIDAD',1,1,'Peso'),(123,'127','FRATACHO DE ALGARROBO 12X25 CM',NULL,10,'UNIDAD',1,1,'Peso'),(124,'128','FRATACHO DE PINO 12X30 CM',NULL,10,'UNIDAD',1,1,'Peso'),(125,'129','FRATACHO DE ALGARROBO 12X35 CM',NULL,10,'UNIDAD',1,1,'Peso'),(126,'130','NIVEL PVC 40 CM REF. AMARILLO',NULL,10,'UNIDAD',1,1,'Peso'),(127,'131','LLANA DENTADA 10X10 30 CM',NULL,10,'UNIDAD',1,1,'Peso'),(128,'132','NIVEL PVC 50 CM REF. - AMARILLO',NULL,10,'UNIDAD',1,1,'Peso'),(129,'133','MECHA DE WIDIA 6 MM- RWIN',NULL,10,'UNIDAD',1,1,'Peso'),(130,'134','MECHA DE WIDIA 8 MM- IRWIN',NULL,10,'UNIDAD',1,1,'Peso'),(131,'135','MECHA DE WIDIA 12MM-IRWIN',NULL,10,'UNIDAD',1,1,'Peso'),(132,'136','MECHA DE WIDIA 10 MM- IRWIN',NULL,10,'UNIDAD',1,1,'Peso'),(133,'137','MECHA DE ACERO RAP.\"5.00\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(134,'138','MECHA DE ACERO RAP. \"3.50\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(135,'139','TACO DE NYLON 6 MM C/TOPE',NULL,10,'UNIDAD',1,1,'Peso'),(136,'141','TACO DE NYLON 6 MM FX UNIVERSAL',NULL,10,'UNIDAD',1,1,'Peso'),(137,'142','TACO DE NYLON 8 MM C/TOPE',NULL,10,'UNIDAD',1,1,'Peso'),(138,'143','TACO DE NYLON 8 MM UNIVERSAL',NULL,10,'UNIDAD',1,1,'Peso'),(139,'144','TACO DE NYLON 10 MM L/HUECO',NULL,10,'UNIDAD',1,1,'Peso'),(140,'145','TACO DE NYLON 10 MM UNIVERSAL',NULL,10,'UNIDAD',1,1,'Peso'),(141,'146','TACO DE NYLON 12 MM UNIVERSAL',NULL,10,'UNIDAD',1,1,'Peso'),(142,'147','SIERRA INGCO AJUSTABLE DE 300 MM',NULL,10,'UNIDAD',1,1,'Peso'),(143,'148','HOJA DE SIERRA BIMETAL 32 DIENTES',NULL,10,'UNIDAD',1,1,'Peso'),(144,'149','REMACHE RIBOT 3.5X10MM',NULL,10,'UNIDAD',1,1,'Peso'),(145,'150','REMACHE RIBOT 3.5X14 MM',NULL,10,'UNIDAD',1,1,'Peso'),(146,'151','REMACHE RIBOT 5.0X10 MM',NULL,10,'UNIDAD',1,1,'Peso'),(147,'152','REMACHE RIBOT 5.0X14 MM',NULL,10,'UNIDAD',1,1,'Peso'),(148,'153','LLANA DENTADA 8X8 30 CM',NULL,10,'UNIDAD',1,1,'Peso'),(149,'154','LAPIZ CARPINTERO X 1\"',NULL,10,'UNIDAD',1,1,'Peso'),(150,'155','MAZA REFORZADA 1000 GS',NULL,10,'UNIDAD',1,1,'Peso'),(151,'156','MAZA REFORZADA ROTTWEILER 750 GRS',NULL,10,'UNIDAD',1,1,'Peso'),(152,'157','CODO ROSCA HEMBRA 3/4\" PPN',NULL,10,'UNIDAD',1,1,'Peso'),(153,'158','CODO ROSCA HEMBRA - HEMBRA 1\"',NULL,10,'UNIDAD',1,1,'Peso'),(154,'159','CODO ROSCA MACHO - HEMBRA 1/2\" P.',NULL,10,'UNIDAD',1,1,'Peso'),(155,'160','CODO ROSCA MACHO - HEMBRA 3/4\" P.P.',NULL,10,'UNIDAD',1,1,'Peso'),(156,'161','CODO ROSCA MACHO - HEMBRA 1\" P.',NULL,10,'UNIDAD',1,1,'Peso'),(157,'162','TEE ROSCA HEMBRA 1/2\"PPN',NULL,10,'UNIDAD',1,1,'Peso'),(158,'163','TEE ROSCA HEMBRA 3/4\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(159,'164','TEE ROSCA HEMBRA - H-H 1\"P.P.N.',NULL,10,'UNIDAD',1,1,'Peso'),(160,'165','TEE REDUCCIÓN 1/2\" X 3/4\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(161,'166','CUPLA HEMBRA - HEMBRA 1/2\" P',NULL,10,'UNIDAD',1,1,'Peso'),(162,'167','CUPLA HEMBRA - HEMBRA 3/4\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(163,'168','CUPLA HEMBRA - HEMBRA 1\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(164,'169','CUPLA REDUCCIÓN 1/2\" X 3/8\" P.P.',NULL,10,'UNIDAD',1,1,'Peso'),(165,'170','CUPLA REDUCCIÓN 3/4\" X 1/2\" P.P',NULL,10,'UNIDAD',1,1,'Peso'),(166,'171','CUPLA REDUCCIÓN 1\" X 1/2\" P\'.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(167,'172','CUPLA REDUCCIÓN 1\" X 3/4\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(168,'173','TAPÓN ROSCA MACHO 1/2\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(169,'174','TAPÓN ROSCA MACHO 3/4\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(170,'175','TAPÓN ROSCA MACHO 1\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(171,'176','TAPA ROSCA HEMBRA 3/4\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(172,'177','TAPA ROSCA HEMBRA 1\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(173,'178','ENTRE ROSCA 1/2\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(174,'179','ENTRE ROSCA 3/4\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(175,'180','ENTRE ROSCA 1\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(176,'181','UNIÓN DOBLE C./JUNTA 1/2\" P.P.N.',NULL,10,'UNIDAD',1,1,'Peso'),(177,'182','UNIÓN DOBLE C./JUNTA 3/4\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(178,'183','CONEXIÓN TANQUE COMPLETO 1/2\" P.P.',NULL,10,'UNIDAD',1,1,'Peso'),(179,'184','CONEXIÓN TANQUE COMPLETO 3/4\"',NULL,10,'UNIDAD',1,1,'Peso'),(180,'185','CONEXIÓN TANQUE COMPLETO 1\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(181,'186','NIPLE 1/2\" X 6 CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(182,'187','NIPLE 1/2\" X 8 CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(183,'188','NIPLE 1/2\" X 12CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(184,'189','NIPLE 1/2\" X 10CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(185,'190','NIPLE 1/2\" X 15 CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(186,'191','NIPLE 3/4\" X 6 CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(187,'192','NIPLE 3/4\" X 8 CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(188,'193','NIPLE 3/4\" X 10 CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(189,'194','NIPLE 3/4\" X 12 CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(190,'195','NIPLE 3/4\" X 15 CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(191,'196','NIPLE 1\" X 6 CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(192,'197','NIPLE 1\" X 8 CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(193,'198','NIPLE 1\" X 10 CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(194,'199','NIPLE 1\" X 12CM P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(195,'200','BUJE M-H REDUCCION 1 X 1/2 PPM',NULL,10,'UNIDAD',1,1,'Peso'),(196,'201','BUJE M-H REDUCCION 1 X 3/4 PPM',NULL,10,'UNIDAD',1,1,'Peso'),(197,'202','BUJE M-H REDUCCION 3/4 X 1/2 PPM',NULL,10,'UNIDAD',1,1,'Peso'),(198,'203','BUJE M-H REDUCCION 1/2 X 3/8 PPM',NULL,10,'UNIDAD',1,1,'Peso'),(199,'204','ENTRE ROSCA REDUCCIÓN 3/4\" X 1/2\"P.',NULL,10,'UNIDAD',1,1,'Peso'),(200,'205','ENTRE ROSCA REDUCCIÓN 1\" X 1/2\" P.3',NULL,10,'UNIDAD',1,1,'Peso'),(201,'206','ENTRE ROSCA REDUCCIÓN 1\" X 3/4\" P.P',NULL,10,'UNIDAD',1,1,'Peso'),(202,'207','CURVA HEMBRA - HEMBRA A 90° 1\" P.P.',NULL,10,'UNIDAD',1,1,'Peso'),(203,'208','CURVA MACHO - HEMBRA A 90° 1/2\" P.',NULL,10,'UNIDAD',1,1,'Peso'),(204,'209','CURVA MACHO - HEMBRA A 90° 3/4\" P\'.',NULL,10,'UNIDAD',1,1,'Peso'),(205,'210','CODO REDUCIDO 3/4\" X 1/2\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(206,'211','CODO REDUCIDO 1\" X 3/4\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(207,'212','CURVA HEMBRA - HEMBRA A 45° 1/2',NULL,10,'UNIDAD',1,1,'Peso'),(208,'213','CURVA HEMBRA - HEMBRA A 45° 3/4\" P.',NULL,10,'UNIDAD',1,1,'Peso'),(209,'214','CURVA HEMBRA - HEMBRA A 45° 1\" P.P.',NULL,10,'UNIDAD',1,1,'Peso'),(210,'215','CAÑO 1/2\" POLIPROP. BICAPA',NULL,10,'UNIDAD',1,1,'Peso'),(211,'216','CAÑO 3/4\" POLIPROP. BICAPA',NULL,10,'UNIDAD',1,1,'Peso'),(212,'217','CAÑO 1\" POLIPROP. BICAPA',NULL,10,'UNIDAD',1,1,'Peso'),(213,'218','CAÑO POLIPRO.TRICAPA 1/2\"',NULL,10,'UNIDAD',1,1,'Peso'),(214,'219','CAÑO POLIPRO.TRICAPA 3/4\"',NULL,10,'UNIDAD',1,1,'Peso'),(215,'220','CAÑO POLIPRO.TRICAPA 1\"',NULL,10,'UNIDAD',1,1,'Peso'),(216,'221','MASILLA SANITARIA 1/2 KG',NULL,10,'UNIDAD',1,1,'Peso'),(217,'222','MEMBRANA AUTOADHESIVA 10CM X 1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(218,'223','MEMBRANA AUTOADHESIVA 15CM X 1MTS',NULL,10,'UNIDAD',1,1,'Peso'),(219,'224','SOPAPA PLAST.REFORZ. 1 ½ \" (0.40)',NULL,10,'UNIDAD',1,1,'Peso'),(220,'225','SOPAPA PLASTICA REFORZ. 2\"(0.50)',NULL,10,'UNIDAD',1,1,'Peso'),(221,'226','TORNILLO BCE 22-70 P/INODORO Y BIDE',NULL,10,'UNIDAD',1,1,'Peso'),(222,'227','VALVULA 1/2\" BRONCE C/GOMA',NULL,10,'UNIDAD',1,1,'Peso'),(223,'228','GOMA  P/ VALVULA DE CANILLA 1/2',NULL,10,'UNIDAD',1,1,'Peso'),(224,'229','TERRAJA P/ CAÑO PLASTICO 1/2\",3/4\"Y',NULL,10,'UNIDAD',1,1,'Peso'),(225,'230','ENCHUFE ROSCA MACHO 1/2\" POLIETILE',NULL,10,'UNIDAD',1,1,'Peso'),(226,'231','ENCHUFE ROSCA MACHO 3/4\"  POLIETIL',NULL,10,'UNIDAD',1,1,'Peso'),(227,'232','ENCHUFE ROSCA MACHO \"1\" POLIETILENO',NULL,10,'UNIDAD',1,1,'Peso'),(228,'233','ENCHUFE DOBLE 1/2\"    POLIETILENO',NULL,10,'UNIDAD',1,1,'Peso'),(229,'234','ENCHUFE DOBLE 3/4\" POLIETILENO.',NULL,10,'UNIDAD',1,1,'Peso'),(230,'235','ENCHUFE DOBLE 1\"   POLIETILENO',NULL,10,'UNIDAD',1,1,'Peso'),(231,'236','ENCHUFE DOBLE RED. 3/4 A 1/2 POLIET',NULL,10,'UNIDAD',1,1,'Peso'),(232,'237','ENCHUFE DOBLE RED. 1\" A 3/4\" POLIET',NULL,10,'UNIDAD',1,1,'Peso'),(233,'238','ENCHUFE ROSCA HEMBRA 1/2\" POLIETIL',NULL,10,'UNIDAD',1,1,'Peso'),(234,'239','ENCHUFE ROSCA HEMBRA 3/4\" POLIETIL',NULL,10,'UNIDAD',1,1,'Peso'),(235,'240','ENCHUFE ROSCA HEMBRA 1\" POLIETILE',NULL,10,'UNIDAD',1,1,'Peso'),(236,'241','ENCHUFE MACHO RED. 1\" A 3/4\" POLIET',NULL,10,'UNIDAD',1,1,'Peso'),(237,'242','ENCHUFE MACHO RED. 3/4 A 1/2 POLIET',NULL,10,'UNIDAD',1,1,'Peso'),(238,'243','ENCHUFE MACHO RED. 1/2 A 3/4 POLIET',NULL,10,'UNIDAD',1,1,'Peso'),(239,'244','ENCHUFE MACHO RED. 1\" A 1/2 \" POLIE',NULL,10,'UNIDAD',1,1,'Peso'),(240,'245','TANQUE TRICAPA 400 LTS.MASTERTANQ',NULL,10,'UNIDAD',1,1,'Peso'),(241,'246','TANQUE TRICAPA 600 LTS-MASTERTANQ',NULL,10,'UNIDAD',1,1,'Peso'),(242,'247','TANQUE TRICAPA 1100 LTS-MASTERTANQ',NULL,10,'UNIDAD',1,1,'Peso'),(243,'248','TANQUE TRICAPA 850 LTS-MASTERTANQ',NULL,10,'UNIDAD',1,1,'Peso'),(244,'249','TANQUE TRICAPA 2750 LTS-MASTERTANQ',NULL,10,'UNIDAD',1,1,'Peso'),(245,'250','RODILLO MINI FORRADO 8 CM',NULL,10,'UNIDAD',1,1,'Peso'),(246,'251','AC - FULL X 1 LT -LADRILLOS METALES',NULL,10,'UNIDAD',1,1,'Peso'),(247,'253','VIRUTA GRUESA-MEDIANA-FINA X 250 GR',NULL,10,'UNIDAD',1,1,'Peso'),(248,'254','REMOVEDOR GEL 500 GRS.',NULL,10,'UNIDAD',1,1,'Peso'),(249,'255','ENDUIDO EN POLVO PREGO X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(250,'256','CEMENTO RAPIDO GRIS PREGO X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(251,'257','ENDUIDO VENTO X 1 LT',NULL,10,'UNIDAD',1,1,'Peso'),(252,'258','LATEX SOLAR INTERIOR X 20 LS-',NULL,10,'UNIDAD',1,1,'Peso'),(253,'259','LATEX VENTO EXTERIOR BLANCO X 1 LT',NULL,10,'UNIDAD',1,1,'Peso'),(254,'260','LATEX VENTO EXTERIOR BLANCO X 10 LT',NULL,10,'UNIDAD',1,1,'Peso'),(255,'261','LATEX VENTO EXTERIOR BLANCO X 20 LT',NULL,10,'UNIDAD',1,1,'Peso'),(256,'262','LATEX VENTO EXTERIOR BLANCO X 4LT',NULL,10,'UNIDAD',1,1,'Peso'),(257,'263','LATEX VENTO INTERIOR X 20 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(258,'264','ESMALTE SINTETICO NEGRO X 1LT VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(259,'265','ESMALTE SINTETICO BLANCO X 1 LT VEN',NULL,10,'UNIDAD',1,1,'Peso'),(260,'266','ESMALTE SINT.NEGRO MATE X 1 LT VENT',NULL,10,'UNIDAD',1,1,'Peso'),(261,'267','ESPATULA PINTOR N° 40',NULL,10,'UNIDAD',1,1,'Peso'),(262,'268','ESPATULA PINTOR N°60',NULL,10,'UNIDAD',1,1,'Peso'),(263,'269','ESPATULA PINTOR N° 80',NULL,10,'UNIDAD',1,1,'Peso'),(264,'270','PINCEL EKO C.BLANCA V1 Nº 10',NULL,10,'UNIDAD',1,1,'Peso'),(265,'271','PINCEL EKO C.BLANCA V1 Nº 15',NULL,10,'UNIDAD',1,1,'Peso'),(266,'272','PINCEL EKO C.BLANCA V1 Nº 20',NULL,10,'UNIDAD',1,1,'Peso'),(267,'273','LIJA AGUA AA Nº 60',NULL,10,'UNIDAD',1,1,'Peso'),(268,'274','LIJA AGUA AA Nº 80',NULL,10,'UNIDAD',1,1,'Peso'),(269,'275','LIJAS EKO N°60 AL 360 Y 600',NULL,10,'UNIDAD',1,1,'Peso'),(270,'276','CONVERTIDOR BLANCO X 1 LT VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(271,'277','CONVERTIDOR NEGRO X 1 LT VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(272,'278','RODILLO LANAR ECOLOGICO 22 CM',NULL,10,'UNIDAD',1,1,'Peso'),(273,'279','RODILLO LANAR ORO X 22 CM',NULL,10,'UNIDAD',1,1,'Peso'),(274,'280','ANTIOXIDO NEGRO X 1 LT VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(275,'281','ESTOPA BLANCA SUPER LUSTRE X 300 GR',NULL,10,'UNIDAD',1,1,'Peso'),(276,'282','LATEX VENTO INTERIOR X 4 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(277,'283','AGUARRAS X 900 CC -S10',NULL,10,'UNIDAD',1,1,'Peso'),(278,'284','THINNER ESPECIAL X 900 CC - S10',NULL,10,'UNIDAD',1,1,'Peso'),(279,'285','ESCALERA PINTOR 10 ESCALONES',NULL,10,'UNIDAD',1,1,'Peso'),(280,'286','ESCALERA PINTOR 6 ESCALONES',NULL,10,'UNIDAD',1,1,'Peso'),(281,'287','ESCALETA PINTOR 8 ESCALONES',NULL,10,'UNIDAD',1,1,'Peso'),(282,'288','ENCHUFE HEMBRA RED. 1/2 A 3/4 POLIE',NULL,10,'UNIDAD',1,1,'Peso'),(283,'289','ENCHUFE HEMBRA RED. 3/4 A 1/2 POLIE',NULL,10,'UNIDAD',1,1,'Peso'),(284,'290','ENCHUFE HEMBRA RED. 1\" A 1/2\" POLIE',NULL,10,'UNIDAD',1,1,'Peso'),(285,'291','ENCHUFE HEMBRA RED. 1\" A 3/4 POLIET',NULL,10,'UNIDAD',1,1,'Peso'),(286,'292','ENCHUFE TEE ROSCA HEMBRA \"1\" POLIET',NULL,10,'UNIDAD',1,1,'Peso'),(287,'293','ENCHUFE TEE ROSCA HEMBRA 3/4 POLIET',NULL,10,'UNIDAD',1,1,'Peso'),(288,'294','ENCHUFE TEE ROSCA HEMBRA 1/2 POLIE',NULL,10,'UNIDAD',1,1,'Peso'),(289,'295','ENCHUFE TEE \"3/4\" POLIETILENO',NULL,10,'UNIDAD',1,1,'Peso'),(290,'296','ENCHUFE TEE \"1\"  POLIETILENO',NULL,10,'UNIDAD',1,1,'Peso'),(291,'297','ENCHUFE CODO \"1/2\"   POLIETILENO',NULL,10,'UNIDAD',1,1,'Peso'),(292,'298','ENCHUFE CODO \"3/4\"   POLIETILENO',NULL,10,'UNIDAD',1,1,'Peso'),(293,'299','ENCHUFE CODO \"1\" POLIETILENO',NULL,10,'UNIDAD',1,1,'Peso'),(294,'300','ENCHUFE CODO ROSCA HEMBRA 1/2 POLIE',NULL,10,'UNIDAD',1,1,'Peso'),(295,'301','ENCHUFE CODO ROSCA HEMBRA 3/4 POLIE',NULL,10,'UNIDAD',1,1,'Peso'),(296,'302','ENCHUFE CODO ROSCA HEMBRA \"1\" POLIE',NULL,10,'UNIDAD',1,1,'Peso'),(297,'303','MANGUERA NEGRA K4  1/2\" X 1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(298,'304','MANGUERA NEGRA K4 X 3/4\"  1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(299,'305','MANGUERA NEGRA K4  1\" X 1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(300,'306','FLEXIBLE P.V.C. \"ERREDE\" 1/2\" X 30C',NULL,10,'UNIDAD',1,1,'Peso'),(301,'307','FLEXIBLE P.V.C. \"ERREDE\" 1/2\" X 40C',NULL,10,'UNIDAD',1,1,'Peso'),(302,'308','LAVATORIO ANDINA 3 AG BCO FERRUM',NULL,10,'UNIDAD',1,1,'Peso'),(303,'309','INODORO CORTO ANDINA BCO FERRUM',NULL,10,'UNIDAD',1,1,'Peso'),(304,'310','GRIFERIA DUCHA C/T ARIZONA FV',NULL,10,'UNIDAD',1,1,'Peso'),(305,'311','GRIFERIA BIDET ARIZONA PLUS FV',NULL,10,'UNIDAD',1,1,'Peso'),(306,'312','DEPOSITO COLGAR ANDINA BCO FERRUM',NULL,10,'UNIDAD',1,1,'Peso'),(307,'313','COLUMNA ANDINA BCA FERRUM',NULL,10,'UNIDAD',1,1,'Peso'),(308,'314','GRIFERIA LAVATORIO P/ALTO VIVA PLUS',NULL,10,'UNIDAD',1,1,'Peso'),(309,'315','GRIFERIA LAVATORIO ARIZONA FV',NULL,10,'UNIDAD',1,1,'Peso'),(310,'316','BIDET ANDINA 3 AG BCO FERRUM',NULL,10,'UNIDAD',1,1,'Peso'),(311,'317','STUKO NORTEÑO FINO X 25 KG',NULL,10,'UNIDAD',1,1,'Peso'),(312,'318','ADHEREX IMPERMEABLE KLAUKOL X 30 KG',NULL,10,'UNIDAD',1,1,'Peso'),(313,'319','FLEXIBLE P.V.C. \"ERREDE\" 1/2\" X 50C',NULL,10,'UNIDAD',1,1,'Peso'),(314,'320','FLEXIBLE P.V.C 1/2\" X 60 CM',NULL,10,'UNIDAD',1,1,'Peso'),(315,'321','SIFON SIMPLE PVC BLANCO VIRGEN \"MO',NULL,10,'UNIDAD',1,1,'Peso'),(316,'322','SIFON DOBLE PVC BLANCO VIRGEN \"MO',NULL,10,'UNIDAD',1,1,'Peso'),(317,'323','CONEXION  FUELLE P/INODORO GOMA',NULL,10,'UNIDAD',1,1,'Peso'),(318,'324','CODO BANDONEON P/ MOCHILA(0.50)',NULL,10,'UNIDAD',1,1,'Peso'),(319,'325','REJA VENTILACION 15 X15 ESMALTADA E',NULL,10,'UNIDAD',1,1,'Peso'),(320,'326','REJA DE HIERRO C/MARCO 20 X 20',NULL,10,'UNIDAD',1,1,'Peso'),(321,'327','CANILLA 1/2 PVC GINYPLAST',NULL,10,'UNIDAD',1,1,'Peso'),(322,'328','SELLAROSCA HIDRO 3 X 25CC',NULL,10,'UNIDAD',1,1,'Peso'),(323,'329','SELLAROSCAS HIDRO 3 X 50CC',NULL,10,'UNIDAD',1,1,'Peso'),(324,'330','CAÑAMO PEINADO FI',NULL,10,'UNIDAD',1,1,'Peso'),(325,'331','CODO P.V.C. 0.40 A 90ª H-H',NULL,10,'UNIDAD',1,1,'Peso'),(326,'332','CODO P.V.C. 0.50 A 90 H-H',NULL,10,'UNIDAD',1,1,'Peso'),(327,'333','CODO P.V.C. 0.60 A 90º H-H',NULL,10,'UNIDAD',1,1,'Peso'),(328,'334','CODO P.V.C. 0.50 A 90 M-H',NULL,10,'UNIDAD',1,1,'Peso'),(329,'335','CODO P.V.C. 060A 90  M-H',NULL,10,'UNIDAD',1,1,'Peso'),(330,'336','CODO P.V.C. 063 A 90 M-H',NULL,10,'UNIDAD',1,1,'Peso'),(331,'337','CODO P.V.C. 100 A 90  M-H',NULL,10,'UNIDAD',1,1,'Peso'),(332,'338','CODO P.V.C. 110 A 90   M-H 3.2',NULL,10,'UNIDAD',1,1,'Peso'),(333,'339','CODO P.V.C.  C/BASE 100 A 90ª',NULL,10,'UNIDAD',1,1,'Peso'),(334,'340','CODO PVC C/BASE  110 A 90ª M-H',NULL,10,'UNIDAD',1,1,'Peso'),(335,'341','CURVA P.V.C. 0.40 A 90 M-H',NULL,10,'UNIDAD',1,1,'Peso'),(336,'342','CURVA P.V.C. 0.50 A 90º  M-H',NULL,10,'UNIDAD',1,1,'Peso'),(337,'343','CURVA P.V.C. 0.60 A 90º M-H',NULL,10,'UNIDAD',1,1,'Peso'),(338,'344','CURVA P.V.C.  100 A 90º M-H',NULL,10,'UNIDAD',1,1,'Peso'),(339,'345','CURVA P.V.C  110 A 90º M-H LINEA 3.',NULL,10,'UNIDAD',1,1,'Peso'),(340,'346','CURVA P.V.C. 0.40 A 45º M-H',NULL,10,'UNIDAD',1,1,'Peso'),(341,'347','CURVA P.V.C.  0.63 A 45º M-H LINEA3',NULL,10,'UNIDAD',1,1,'Peso'),(342,'348','CURVA P.V.C.  100 A 45° M-H',NULL,10,'UNIDAD',1,1,'Peso'),(343,'349','CURVA P.V.C. 110 A 45º M-H LINEA 3.',NULL,10,'UNIDAD',1,1,'Peso'),(344,'350','CODO P.V.C.  100 A 45º M-H',NULL,10,'UNIDAD',1,1,'Peso'),(345,'351','CODO P.V.C.  110  A 45º M-H LINEA 3',NULL,10,'UNIDAD',1,1,'Peso'),(346,'352','RAMAL \"Y\" PVC 0.40X0.40X45º',NULL,10,'UNIDAD',1,1,'Peso'),(347,'353','RAMAL \"Y\" P.V.C. 100 X 0.60 X 45º',NULL,10,'UNIDAD',1,1,'Peso'),(348,'354','RAMAL \"Y\" P.V.C.  100 X 100 X 45º',NULL,10,'UNIDAD',1,1,'Peso'),(349,'355','RAMAL \"Y\" PVC 0.63X0.63X45º LINEA 3',NULL,10,'UNIDAD',1,1,'Peso'),(350,'356','RAMAL \"Y\" PVC  110X0.63X45º LINEA 3',NULL,10,'UNIDAD',1,1,'Peso'),(351,'357','RAMAL \"Y\" PVC 110X110X45º LINEA 3.2',NULL,10,'UNIDAD',1,1,'Peso'),(352,'358','RAMAL \"TEE\" PVC 0.40X0.40X90º LINEA',NULL,10,'UNIDAD',1,1,'Peso'),(353,'359','RAMAL \"TEE\" PVC 0.50X0.50X90º LINEA',NULL,10,'UNIDAD',1,1,'Peso'),(354,'360','RAMAL \"TEE\" PVC 0.63X0.63X90º LINEA',NULL,10,'UNIDAD',1,1,'Peso'),(355,'361','RAMAL \"TEE\" PVC 110X110X90º LINEA 3',NULL,10,'UNIDAD',1,1,'Peso'),(356,'362','RAMAL \"TEE\" PVC 110X0.63X90º LINEA',NULL,10,'UNIDAD',1,1,'Peso'),(357,'363','REDUCCION P.V.C.  0.40 X 0.50',NULL,10,'UNIDAD',1,1,'Peso'),(358,'364','REDUCCION P.V.C. 0.50 X 0.60',NULL,10,'UNIDAD',1,1,'Peso'),(359,'365','REDUCCION P.V.C. 0.60 X 0.40',NULL,10,'UNIDAD',1,1,'Peso'),(360,'366','REDUCCION P.V.C. 100 X 0.60',NULL,10,'UNIDAD',1,1,'Peso'),(361,'367','REDUCCION P.V.C. 110X0.63 LINEA 3.2',NULL,10,'UNIDAD',1,1,'Peso'),(362,'368','REDUCCION P.V.C. 110 X 100 LINEA 3.',NULL,10,'UNIDAD',1,1,'Peso'),(363,'369','REDUCCION P.V.C. 160 X 110',NULL,10,'UNIDAD',1,1,'Peso'),(364,'370','CUPLA P.V.C.  0.40',NULL,10,'UNIDAD',1,1,'Peso'),(365,'371','CUPLA P.V.C.  0.50',NULL,10,'UNIDAD',1,1,'Peso'),(366,'372','CUPLA P.V.C.  0.60',NULL,10,'UNIDAD',1,1,'Peso'),(367,'373','CUPLA P.V.C.  100',NULL,10,'UNIDAD',1,1,'Peso'),(368,'374','CUPLA P.V.C.  110',NULL,10,'UNIDAD',1,1,'Peso'),(369,'375','TAPA P.V.C.  0.40',NULL,10,'UNIDAD',1,1,'Peso'),(370,'376','TAPA P.V.C.  0.50',NULL,10,'UNIDAD',1,1,'Peso'),(371,'377','TAPA P.V.C.  0.63  LINEA 3.2',NULL,10,'UNIDAD',1,1,'Peso'),(372,'378','TAPA P.V.C.  100',NULL,10,'UNIDAD',1,1,'Peso'),(373,'379','TAPA P.V.C.  110 LINEA 3.2',NULL,10,'UNIDAD',1,1,'Peso'),(374,'380','EMBUDO P.V.C. 0.60 (BLANCO) C/ REJI',NULL,10,'UNIDAD',1,1,'Peso'),(375,'381','EMBUDO P.V.C.  100 (BLANCO) C/ REJI',NULL,10,'UNIDAD',1,1,'Peso'),(376,'382','EMBUDO PVC 110 LINEA 3.2(BCO) C/',NULL,10,'UNIDAD',1,1,'Peso'),(377,'383','SOMBRERO P.V.C. 0.60',NULL,10,'UNIDAD',1,1,'Peso'),(378,'384','SOMBRERO P.V.C.  100',NULL,10,'UNIDAD',1,1,'Peso'),(379,'385','BOCA DE ACCESO P.V.C. C/ TAPA 10X10',NULL,10,'UNIDAD',1,1,'Peso'),(380,'386','PORTA REJILLA P.V.C. 10 X 10',NULL,10,'UNIDAD',1,1,'Peso'),(381,'387','PORTA REJILLA P.V.C.15 X 15',NULL,10,'UNIDAD',1,1,'Peso'),(382,'388','CAÑO CAMARA 100',NULL,10,'UNIDAD',1,1,'Peso'),(383,'389','CAÑO CAMARA 110 PVC',NULL,10,'UNIDAD',1,1,'Peso'),(384,'390','CODO P.V.C. C/ACOMETIDA 110 A 90 MH',NULL,10,'UNIDAD',1,1,'Peso'),(385,'391','PILETAS DE PISO 10X10 P.V.C.',NULL,10,'UNIDAD',1,1,'Peso'),(386,'392','PILETAS DE PISO 15X15 P.V.C.',NULL,10,'UNIDAD',1,1,'Peso'),(387,'393','REJILLAS PLASTICAS DE 10X10 P/PISO',NULL,10,'UNIDAD',1,1,'Peso'),(388,'394','REJILLAS PLASTICAS DE 15X15 P/PIS',NULL,10,'UNIDAD',1,1,'Peso'),(389,'395','REJILLAS PLASTICAS DE 20X20 P/PISO',NULL,10,'UNIDAD',1,1,'Peso'),(390,'396','CODO DE 20 MM A 90º FF',NULL,10,'UNIDAD',1,1,'Peso'),(391,'397','CODO DE 25 MM A 90º FF',NULL,10,'UNIDAD',1,1,'Peso'),(392,'398','CODO DE 32 MM A 90º FF',NULL,10,'UNIDAD',1,1,'Peso'),(393,'399','CUPLA DE 20 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(394,'400','CUPLA DE 25 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(395,'401','CUPLA DE 32 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(396,'402','TEE DE 20 MM FFF',NULL,10,'UNIDAD',1,1,'Peso'),(397,'403','TEE DE 25 MM FFF',NULL,10,'UNIDAD',1,1,'Peso'),(398,'404','TEE DE 32 MM FFF',NULL,10,'UNIDAD',1,1,'Peso'),(399,'405','CAÑO FUSION 32MM   (1\")',NULL,10,'UNIDAD',1,1,'Peso'),(400,'406','CAÑO FUSION 20MM   (1/2)',NULL,10,'UNIDAD',1,1,'Peso'),(401,'407','CAÑO FUSION 25MM    (3/4)',NULL,10,'UNIDAD',1,1,'Peso'),(402,'408','TAPA DE 20 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(403,'409','TAPA DE 25 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(404,'410','TAPA DE 32 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(405,'411','UNION DOBLE DE 20 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(406,'412','UNION DOBLE DE 25 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(407,'413','UNION DOBLE DE 32 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(408,'414','CODO REDUCCION DE 25 MM X 20 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(409,'415','CODO REDUCCION DE 32 MM X 20 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(410,'416','CODO REDUCCION DE 32 MM X 25 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(411,'417','CUPLA REDUCCION DE 25 MM X 20 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(412,'418','CUPLA REDUCCION DE 32 MM X 20 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(413,'419','CUPLA REDUCCION DE 32 MM X 25 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(414,'420','TEE REDUCCION DE 25 MM X 20 MM FFF',NULL,10,'UNIDAD',1,1,'Peso'),(415,'421','TEE REDUCCION DE 32 MM X 20 MM FFF',NULL,10,'UNIDAD',1,1,'Peso'),(416,'422','TEE REDUCCION DE 32 MM X 25 MM FFF',NULL,10,'UNIDAD',1,1,'Peso'),(417,'423','BUJE REDUCCION DE 25 MM X 20 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(418,'424','BUJE REDUCCION DE 32 MM X 20 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(419,'425','BUJE REDUCCION DE 32 MM X 25 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(420,'426','SOBREPASO CORTO DE 20 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(421,'427','SOBREPASO CORTO DE 25 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(422,'428','SOBREPASO CORTO DE 32 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(423,'429','CURVA DE 20 MM A 90º FF',NULL,10,'UNIDAD',1,1,'Peso'),(424,'430','CURVA DE 25 MM A 90º FF',NULL,10,'UNIDAD',1,1,'Peso'),(425,'431','CODO DE 20 MM HH A 45º FF',NULL,10,'UNIDAD',1,1,'Peso'),(426,'432','CODO DE 25 MM HH A 45º FF',NULL,10,'UNIDAD',1,1,'Peso'),(427,'433','CODO DE 32 MM HH A 45º FF',NULL,10,'UNIDAD',1,1,'Peso'),(428,'434','CODO DE 20 MM X 1/2\" A 90º C/ INS M',NULL,10,'UNIDAD',1,1,'Peso'),(429,'435','CODO DE 20 MM X 3/4\" A 90ª C/INS M',NULL,10,'UNIDAD',1,1,'Peso'),(430,'436','CODO DE 25 MM X 1/2\" A 90º C/INC H',NULL,10,'UNIDAD',1,1,'Peso'),(431,'437','CODO DE 25 MM X 3/4\" A 90º C/ INC H',NULL,10,'UNIDAD',1,1,'Peso'),(432,'438','CODO DE 32 MM X 1/2\" A 90º C/ INS H',NULL,10,'UNIDAD',1,1,'Peso'),(433,'439','CODO DE 32 MM X 3/4\" A 90º C/ INS H',NULL,10,'UNIDAD',1,1,'Peso'),(434,'440','CODO DE 32 MM X 1\" A 90º C/ INS H',NULL,10,'UNIDAD',1,1,'Peso'),(435,'441','CINTA EMASCARAR 18 MM X 40 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(436,'442','GUANTE VAQUETA 1/2 CORTO D.P',NULL,10,'UNIDAD',1,1,'Peso'),(437,'443','CINTA AISLADORA TACSA X 10 MT NEGRA',NULL,10,'UNIDAD',1,1,'Peso'),(438,'444','CINTA AISLADORA TACSA X 20 MT NEGRA',NULL,10,'UNIDAD',1,1,'Peso'),(439,'445','FICHA 2 PATAS HEMBRA ECONOMICA',NULL,10,'UNIDAD',1,1,'Peso'),(440,'446','FICHA HEMBRA BLANCA 10 AMP',NULL,10,'UNIDAD',1,1,'Peso'),(441,'447','FICHA MACHO BIPOLAR 20 AMP',NULL,10,'UNIDAD',1,1,'Peso'),(442,'448','TORNILLO FIX 5.0X45',NULL,10,'UNIDAD',1,1,'Peso'),(443,'449','DISCO FLAP CARBORUNDUM 115 - GR  80',NULL,10,'UNIDAD',1,1,'Peso'),(444,'450','DISCO FLAT 115 MM G80 - WURTH',NULL,10,'UNIDAD',1,1,'Peso'),(445,'451','PINCELETA Nº 40',NULL,10,'UNIDAD',1,1,'Peso'),(446,'452','RODILLO MINI EPOXI 5 CM',NULL,10,'UNIDAD',1,1,'Peso'),(447,'453','RODILLO MINI EPOXI 10 CM',NULL,10,'UNIDAD',1,1,'Peso'),(448,'454','SELLADOR DE CANALETAS 230ML WURT',NULL,10,'UNIDAD',1,1,'Peso'),(449,'455','SIERRA CIRCULAR 4 1/2 40 DIENTES',NULL,10,'UNIDAD',1,1,'Peso'),(450,'456','GRAMPAS 6X11,3X0,7MM CAJA 1000 UND',NULL,10,'UNIDAD',1,1,'Peso'),(451,'457','TIJERA CORTA TUBOS PVC CROSSMASTER',NULL,10,'UNIDAD',1,1,'Peso'),(452,'458','SERRUCHO CURVO 6\" C/ ESTUCHE',NULL,10,'UNIDAD',1,1,'Peso'),(453,'459','CEPILLO P/AMOLADORATRENZADO',NULL,10,'UNIDAD',1,1,'Peso'),(454,'460','CODO DE 20 MM X 1/2\" A 90º C/ INS H',NULL,10,'UNIDAD',1,1,'Peso'),(455,'461','CODO DE 25 MM X 1/2\" A 90º C/ INC M',NULL,10,'UNIDAD',1,1,'Peso'),(456,'462','CODO DE 25 MM X 3/4\" A 90º C/ INS M',NULL,10,'UNIDAD',1,1,'Peso'),(457,'463','CODO DE 32 MM X 1\" A 90º C/ INS M',NULL,10,'UNIDAD',1,1,'Peso'),(458,'464','CUPLA DE 20 MM X 3/8\" C/ INSERTO H',NULL,10,'UNIDAD',1,1,'Peso'),(459,'465','CUPLA DE 20 MM X 1/2\" C/ INSERTO H',NULL,10,'UNIDAD',1,1,'Peso'),(460,'466','CUPLA DE 25 MM X 1/2\" C/ INSERTO H',NULL,10,'UNIDAD',1,1,'Peso'),(461,'467','CUPLA DE 25 MM X 3/4\" C/ INSERTO H',NULL,10,'UNIDAD',1,1,'Peso'),(462,'468','CUPLA DE 32 MM X 1\" C/ INSERTO H ME',NULL,10,'UNIDAD',1,1,'Peso'),(463,'469','CUPLA DE 20 MM X 1/2\" C/ INSERTO M',NULL,10,'UNIDAD',1,1,'Peso'),(464,'470','CUPLA DE 20 MM X 3/4\" C/ INSERTO M',NULL,10,'UNIDAD',1,1,'Peso'),(465,'471','CUPLA DE 25 MM X 1/2\" C/ INSERTO M',NULL,10,'UNIDAD',1,1,'Peso'),(466,'472','CUPLA DE 25 MM X 3/4\" C/ INSERTO M',NULL,10,'UNIDAD',1,1,'Peso'),(467,'473','CUPLA DE 32 MM X 3/4\" C/INSERTO M M',NULL,10,'UNIDAD',1,1,'Peso'),(468,'474','CUPLA DE 32 MM X 1\" C/ INSERTO M ME',NULL,10,'UNIDAD',1,1,'Peso'),(469,'475','TEE DE 25 MM X 1/2\" C/ INSERTO H ME',NULL,10,'UNIDAD',1,1,'Peso'),(470,'476','TEE DE 25 MM X 3/4\" C/ INSERTO H ME',NULL,10,'UNIDAD',1,1,'Peso'),(471,'477','TEE DE 32 MM X 1/2\" C/ INSERTO H ME',NULL,10,'UNIDAD',1,1,'Peso'),(472,'478','TEE DE 32 MM X 3/4\" C/ INSERTO H ME',NULL,10,'UNIDAD',1,1,'Peso'),(473,'479','TEE DE 32 MM X 1\" C/ INSERTO H MET.',NULL,10,'UNIDAD',1,1,'Peso'),(474,'480','TEE DE 20 MM X 1/2\" C/ INSERTO M ME',NULL,10,'UNIDAD',1,1,'Peso'),(475,'481','TEE DE 25 MM X 1/2\" C/ INSERTO M ME',NULL,10,'UNIDAD',1,1,'Peso'),(476,'482','TEE DE 25 MM X 3/4\" C/INSERTO M MET',NULL,10,'UNIDAD',1,1,'Peso'),(477,'483','TEE DE 32 MM X 3/4\" C/INSERTO M MET',NULL,10,'UNIDAD',1,1,'Peso'),(478,'484','TEE DE 32 MM X 1\" C/INSERTO M MET.',NULL,10,'UNIDAD',1,1,'Peso'),(479,'485','LLAVE D PASO 20 MM FF CON CAPUCHON',NULL,10,'UNIDAD',1,1,'Peso'),(480,'486','LLAVE D PASO 25 MM FF C/CAPUCHON',NULL,10,'UNIDAD',1,1,'Peso'),(481,'487','LLAVE DE PASO 32 MM FF C/CAMPANA',NULL,10,'UNIDAD',1,1,'Peso'),(482,'488','VALVULA ESFERICA FUSION 20 MM ( ESF',NULL,10,'UNIDAD',1,1,'Peso'),(483,'489','VALVULA ESFERICA FUSION 25 MM ( ESF',NULL,10,'UNIDAD',1,1,'Peso'),(484,'490','SOBREPASO DE 20 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(485,'491','SOBREPASO DE 25 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(486,'492','SOBREPASO DE 32 MM FF',NULL,10,'UNIDAD',1,1,'Peso'),(487,'493','FLEXIBLE DE ACERO INOXID.1/2\" X 20',NULL,10,'UNIDAD',1,1,'Peso'),(488,'494','FLEXIBLE DE ACERO INOXI 1/2\" X  30',NULL,10,'UNIDAD',1,1,'Peso'),(489,'495','TERMOFUSORA 1500W 20,25Y32MM C/RE',NULL,10,'UNIDAD',1,1,'Peso'),(490,'496','ADHESIVO P/ P.V.C. \"DUKE\" X 50 CC',NULL,10,'UNIDAD',1,1,'Peso'),(491,'497','ADHESIVO P/ P.V.C. \"DUKE\" X 100 CC',NULL,10,'UNIDAD',1,1,'Peso'),(492,'498','ACOPLE DE COMPRESION 3/4 \"DUKE\"',NULL,10,'UNIDAD',1,1,'Peso'),(493,'499','ACOPLE DE COMPRESION 1/2 \"DUKE\"',NULL,10,'UNIDAD',1,1,'Peso'),(494,'500','VALVULA ESFERICA P.V.C. 1/2\" \"DUKE\"',NULL,10,'UNIDAD',1,1,'Peso'),(495,'501','VALVULA ESFERICA P.V.C. 3/4\" \"DUKE\"',NULL,10,'UNIDAD',1,1,'Peso'),(496,'502','VALVULA ESFERICA P.V.C. 1\" \"DUKE\"',NULL,10,'UNIDAD',1,1,'Peso'),(497,'503','CANILLA ESFERICA P.V.C. 1/2\" \"DUKE\"',NULL,10,'UNIDAD',1,1,'Peso'),(498,'504','CANILLA ESFERICA P.V.C. 3/4\" \"DUKE\"',NULL,10,'UNIDAD',1,1,'Peso'),(499,'506','REJA P/PISO 10 X 10 A/I ECO.',NULL,10,'UNIDAD',1,1,'Peso'),(500,'507','REJA P/PISO 12X12   A/I ECO.',NULL,10,'UNIDAD',1,1,'Peso'),(501,'508','REJA P/PISO 15X15  A/I ECO.',NULL,10,'UNIDAD',1,1,'Peso'),(502,'509','REJA P/PISO 20X20 A/I ECO.',NULL,10,'UNIDAD',1,1,'Peso'),(503,'510','CAÑO P.V.C. BLANCO  0.40 X 1.5 X 4M',NULL,10,'UNIDAD',1,1,'Peso'),(504,'511','CAÑO P.V.C. BCO.  0.50 X 1.5 X 4 MT',NULL,10,'UNIDAD',1,1,'Peso'),(505,'512','CAÑO P.V.C. BCO.  0.60 X 1.5 X 4 MT',NULL,10,'UNIDAD',1,1,'Peso'),(506,'513','CAÑO P.V.C. BCO  100 X 1.5 X 4 MT',NULL,10,'UNIDAD',1,1,'Peso'),(507,'514','CAÑO P.V.C  110 X 1,8 X 4MTS',NULL,10,'UNIDAD',1,1,'Peso'),(508,'515','MAZA REFORZADFA 750 GRS.',NULL,10,'UNIDAD',1,1,'Peso'),(509,'516','MARTILLO GALPONERO NEGRO 27 MM',NULL,10,'UNIDAD',1,1,'Peso'),(510,'517','PLOMADA DE ALBAÑIL 500 GRS',NULL,10,'UNIDAD',1,1,'Peso'),(511,'518','ROLDANA DE ALBAÑIL POCERA 180 MM',NULL,10,'UNIDAD',1,1,'Peso'),(512,'519','CANDADO MAKAO 38 MM',NULL,10,'UNIDAD',1,1,'Peso'),(513,'520','CANDADO MAKAO 50 MM',NULL,10,'UNIDAD',1,1,'Peso'),(514,'521','CANDADO MAKAO 63 MM',NULL,10,'UNIDAD',1,1,'Peso'),(515,'522','CANDADO MAKAO 32 MM',NULL,10,'UNIDAD',1,1,'Peso'),(516,'523','CINTA METRICA HUALIAN X 3 MT',NULL,10,'UNIDAD',1,1,'Peso'),(517,'524','CINTA METRICA NEGRA ENGOMADA X 5 MT',NULL,10,'UNIDAD',1,1,'Peso'),(518,'525','TRINCHETA REF. 18 MM HAWK',NULL,10,'UNIDAD',1,1,'Peso'),(519,'526','ALICATE CORTE DIAG. 165 MM',NULL,10,'UNIDAD',1,1,'Peso'),(520,'527','DISCO CONTINUO 4 1/2-GRIDEST',NULL,10,'UNIDAD',1,1,'Peso'),(521,'528','DISC SEGMENTADO 4 1/2 YARD',NULL,10,'UNIDAD',1,1,'Peso'),(522,'529','DISCO SEGMENTADO 4 1/2 GRIDEST',NULL,10,'UNIDAD',1,1,'Peso'),(523,'530','TANZA PARA ALBAÑIL 0.80 X 100 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(524,'531','DISCO CORTE STANLEY 4 1/2 1.6 MM',NULL,10,'UNIDAD',1,1,'Peso'),(525,'532','RIPIO 1 AL 3 X 1 MT  P/RETIRAR',NULL,10,'UNIDAD',1,1,'Peso'),(526,'533','RIPIO 1 AL 3 POR BOLSA',NULL,10,'UNIDAD',1,1,'Peso'),(527,'534','ACEITE PARA MADERA X 900 CC -S10',NULL,10,'UNIDAD',1,1,'Peso'),(528,'535','CEMENTO BLANCO PREGO X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(529,'536','PINTURA ASFALTICA X 1LT - S10',NULL,10,'UNIDAD',1,1,'Peso'),(530,'537','AEROSOL 05 COLOR NEGRO 250 CC',NULL,10,'UNIDAD',1,1,'Peso'),(531,'538','BARNIZ BRILLANTE VENTO X 1 LT',NULL,10,'UNIDAD',1,1,'Peso'),(532,'539','ENTONADORES VARIOS COLORES X 120 CC',NULL,10,'UNIDAD',1,1,'Peso'),(533,'540','ENDUIDO SOLAR X 4 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(534,'541','ENTONADOR BERMELLON X 120 CC -VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(535,'542','ENTONADOR MARRON X 120 CC .-VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(536,'543','ENTONADOR NARANJA X120 CC. - VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(537,'544','ENTONADOR NEGRO X 120 CC - VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(538,'545','ENTONADOR VERDE CLARO X 120 CC-VEN',NULL,10,'UNIDAD',1,1,'Peso'),(539,'547','ENTONADORES VARIOS COLORES X 30 CC',NULL,10,'UNIDAD',1,1,'Peso'),(540,'548','AEROSOL ESM. SIN. BLANCO MATE X 240',NULL,10,'UNIDAD',1,1,'Peso'),(541,'549','AEROSOL ESM.SINT. NEGRO MATE X 240',NULL,10,'UNIDAD',1,1,'Peso'),(542,'550','AEROSOL ESM.SINT. BLANCO X 240 CC',NULL,10,'UNIDAD',1,1,'Peso'),(543,'551','AEROSOL ESM.SINT.BERMELLON X 240 CC',NULL,10,'UNIDAD',1,1,'Peso'),(544,'552','AEROSOL ESM.SINT.NARANJA X 240 CC',NULL,10,'UNIDAD',1,1,'Peso'),(545,'553','AEROSOL ESM.SINT.ROJO VIVO X 240 CC',NULL,10,'UNIDAD',1,1,'Peso'),(546,'554','AEROSOL ESM.SI.VERDE INGLES X 240 C',NULL,10,'UNIDAD',1,1,'Peso'),(547,'555','AEROSOL ESM.SINT.GRIS OSCURO X 24 C',NULL,10,'UNIDAD',1,1,'Peso'),(548,'556','AEROSOL ESM.SINT.AMARILLO X 240 CC',NULL,10,'UNIDAD',1,1,'Peso'),(549,'557','AEROSOL ESM.SIN. AZUL MARINO X240 C',NULL,10,'UNIDAD',1,1,'Peso'),(550,'558','PRECINTO 2.5 X 10 CM POR UNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(551,'559','PRECINTO 3.6 X 15 CM - X UNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(552,'560','PRECINTO 4.8 X 20 CM - X UNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(553,'561','PRECINTO 4.8 X 25 CM - X UNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(554,'562','PRECINTO 4.8 X 30 CM - X UNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(555,'563','PRECINTO 4.8 X 40 CM - X UNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(556,'564','FERRITE AMARILLO COROL X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(557,'565','FERRITE NEGRO COROL X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(558,'566','FERRITE ROJO COROL X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(559,'567','PRECINTO 7.5X30 CM ANCHOS -X UNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(560,'568','PRECINTO 7.5 X 40 CM ANCHO -XUNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(561,'569','CINTA ENMASCARA 48X40 MTS EKO',NULL,10,'UNIDAD',1,1,'Peso'),(562,'570','CINTA ENMASCARA 18X40 MTS - EKO',NULL,10,'UNIDAD',1,1,'Peso'),(563,'571','VENDAS P/PARED 20 CM X 25 MT',NULL,10,'UNIDAD',1,1,'Peso'),(564,'572','CINTA DE EMPAQUE TRANS. 48X40 MT',NULL,10,'UNIDAD',1,1,'Peso'),(565,'573','CINTA ENMASCARA 36X40 MT - EKO',NULL,10,'UNIDAD',1,1,'Peso'),(566,'574','LATIZADOR VENTO SACHET X 250 CC',NULL,10,'UNIDAD',1,1,'Peso'),(567,'575','KEROSENE S10 X 900 CC',NULL,10,'UNIDAD',1,1,'Peso'),(568,'576','CLAVO 2\" PUNTA PARIS',NULL,10,'UNIDAD',1,1,'Peso'),(569,'577','CLAVO 2\" 1/2 PUNTA PARIS',NULL,10,'UNIDAD',1,1,'Peso'),(570,'578','MESADA 1.20 X 60 BEIGE INTEGRAL IZQ',NULL,10,'UNIDAD',1,1,'Peso'),(571,'579','MESADA 140 X 60 GRIS CLA.BACHA DE A',NULL,10,'UNIDAD',1,1,'Peso'),(572,'580','MESADA 160 X 60 NEGRO BACHA INT.CEN',NULL,10,'UNIDAD',1,1,'Peso'),(573,'581','PILETA LAVADERO 50X40X28 BCO C/FREG',NULL,10,'UNIDAD',1,1,'Peso'),(574,'582','PILETA LAVADERO 60X50X30 BCO P/AMUR',NULL,10,'UNIDAD',1,1,'Peso'),(575,'583','PILETA LAVADERO 80X47X27 C/MES.FREG',NULL,10,'UNIDAD',1,1,'Peso'),(576,'584','CANILLA ESFERICA METALICA 1/2 ECO',NULL,10,'UNIDAD',1,1,'Peso'),(577,'585','ASIENTO PARA INODORO ECONOMICO',NULL,10,'UNIDAD',1,1,'Peso'),(578,'586','HIDROFUGO X 5 LT -SIKA',NULL,10,'UNIDAD',1,1,'Peso'),(579,'587','TAPA CAMARA 20X20 C/FILETE ACERO IN',NULL,10,'UNIDAD',1,1,'Peso'),(580,'588','TAPA CAMARA 25 X 25 C/FILETE ACERO',NULL,10,'UNIDAD',1,1,'Peso'),(581,'589','TAPA CAMARA 30 X 30 C/FILETE ACERO',NULL,10,'UNIDAD',1,1,'Peso'),(582,'590','TAPA CAMARA 40 X 40 C/FILETE ACERO',NULL,10,'UNIDAD',1,1,'Peso'),(583,'591','TAPA CAMARA 60X60 LIVIANA C/FLEJE',NULL,10,'UNIDAD',1,1,'Peso'),(584,'592','BULON TAPA CAMARA BRONCE PULIDO',NULL,10,'UNIDAD',1,1,'Peso'),(585,'594','MANGUERA DE RIEGO 1/2 X 25 MT-AZUL',NULL,10,'UNIDAD',1,1,'Peso'),(586,'595','MANGUERA P/LAVARROPA 1.5 MT',NULL,10,'UNIDAD',1,1,'Peso'),(587,'596','PINTURA AL AGUA X 4 KG VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(588,'597','CAÑO FLEXIBLE D ALUMI. 4\" COMP.X MT',NULL,10,'UNIDAD',1,1,'Peso'),(589,'598','CAÑO REDONDO DE CHAPA 3\" GALVANIZ',NULL,10,'UNIDAD',1,1,'Peso'),(590,'599','CAÑO REDONDO DE CHAPA 4\" GALVANIZ',NULL,10,'UNIDAD',1,1,'Peso'),(591,'600','CURVA ARTICULADA 3\" DE CHAPA',NULL,10,'UNIDAD',1,1,'Peso'),(592,'601','CURVA ARTICULADA 4\" DE CHAPA',NULL,10,'UNIDAD',1,1,'Peso'),(593,'602','CURVA FIJA 3\" A 90º CORRUGADA DE CH',NULL,10,'UNIDAD',1,1,'Peso'),(594,'603','CURVA FIJA 4\" A 90º CORRUGADA DE CH',NULL,10,'UNIDAD',1,1,'Peso'),(595,'604','CURVA FIJA 3\" A 45º CORRUGADA DE CH',NULL,10,'UNIDAD',1,1,'Peso'),(596,'605','CURVA FIJA 4\" A 45º CORRUGADA DE CH',NULL,10,'UNIDAD',1,1,'Peso'),(597,'606','SOMBRERETE 3\" DOBLE ARO DE CHAPA',NULL,10,'UNIDAD',1,1,'Peso'),(598,'607','SOMBRERETE 4\" DOBLE ARO DE CHAPA',NULL,10,'UNIDAD',1,1,'Peso'),(599,'608','GRAMPAS P/AMURAR 3\"COLA 30 CM(C/TU',NULL,10,'UNIDAD',1,1,'Peso'),(600,'609','GRAMPA P/AMURAR 4\" COLA 30 CM (CON',NULL,10,'UNIDAD',1,1,'Peso'),(601,'610','TEFLON DE 1/2\" X 10 MTS  \"MALVAR\"',NULL,10,'UNIDAD',1,1,'Peso'),(602,'611','TEFLON DE 3/4\" X 10 MTS \"MALVAR\"',NULL,10,'UNIDAD',1,1,'Peso'),(603,'612','CODO ROSCA HEMBRA  1/2\" PPN',NULL,10,'UNIDAD',1,1,'Peso'),(604,'613','UNIÓN DOBLE C./JUNTA 1\" P.P.N',NULL,10,'UNIDAD',1,1,'Peso'),(605,'615','DEPOSITO MOKOTO PVC 11 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(606,'616','CANILLA DOBLE P/LAVARROPAS CROMO',NULL,10,'UNIDAD',1,1,'Peso'),(607,'617','CANILLA DE PVC P/LAVARROPA ITEPA',NULL,10,'UNIDAD',1,1,'Peso'),(608,'618','GRAMPA LAVATORIO LARGA ZINCADA',NULL,10,'UNIDAD',1,1,'Peso'),(609,'619','VALVULA DE 1/2\" ESFERICA METALICA E',NULL,10,'UNIDAD',1,1,'Peso'),(610,'620','VALVULA 3/4\" ESFERICA METALICA ECON',NULL,10,'UNIDAD',1,1,'Peso'),(611,'621','VALVULA 1\" ESFERICA METALICA ECONO',NULL,10,'UNIDAD',1,1,'Peso'),(612,'622','PUERTA P/ LLAVE DE AGUA 15X15 EN AC',NULL,10,'UNIDAD',1,1,'Peso'),(613,'623','PUERTA P/ LLAVE DE AGUA 15X20 EN AC',NULL,10,'UNIDAD',1,1,'Peso'),(614,'624','PUERTA P/ LLAVE DE AGUA 20X20 EN AC',NULL,10,'UNIDAD',1,1,'Peso'),(615,'625','PUERTA DE GAS 40 X 60 TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(616,'626','PUERTA DE GAS 40 X 50 TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(617,'627','REJA VENTILACION 15 X 30 ESMALTADA',NULL,10,'UNIDAD',1,1,'Peso'),(618,'628','AISLANTE T. ALUMINIZADO 5 MM 1X20 M',NULL,10,'UNIDAD',1,1,'Peso'),(619,'629','AISLANTE T. ALUMINIZADO 10 MM1X20 M',NULL,10,'UNIDAD',1,1,'Peso'),(620,'630','MEMBRANA 3 MM C/ALUM.',NULL,10,'UNIDAD',1,1,'Peso'),(621,'631','MEMBRANA 4 MM C/ALUM.',NULL,10,'UNIDAD',1,1,'Peso'),(622,'632','PINCEL EKO C.BLANCA V1 Nº 25',NULL,10,'UNIDAD',1,1,'Peso'),(623,'633','ANTIOXIDO NEGRO X 4 LT VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(624,'634','RODILLO EPOXI X 17 CM',NULL,10,'UNIDAD',1,1,'Peso'),(625,'635','RODILLO EPOXI X 22 CM',NULL,10,'UNIDAD',1,1,'Peso'),(626,'636','VOLIGOMA 30ML',NULL,10,'UNIDAD',1,1,'Peso'),(627,'637','POXIPOL 10 MIN. GRIS X 21 GS',NULL,10,'UNIDAD',1,1,'Peso'),(628,'638','POXILINA 10 MIN. X 70 GS',NULL,10,'UNIDAD',1,1,'Peso'),(629,'639','POXIPOL TRANSPATENTE X 16 GS',NULL,10,'UNIDAD',1,1,'Peso'),(630,'640','ECOLE 9 GS',NULL,10,'UNIDAD',1,1,'Peso'),(631,'641','POXI-RAN S/TOLUENO 23 GS',NULL,10,'UNIDAD',1,1,'Peso'),(632,'642','FASTIX TRASPARENTE X 25 GS',NULL,10,'UNIDAD',1,1,'Peso'),(633,'643','POXITAS 12 UNIDADES',NULL,10,'UNIDAD',1,1,'Peso'),(634,'644','UNIPOX PEGAMENTO UNIVERSAL 25 ML',NULL,10,'UNIDAD',1,1,'Peso'),(635,'645','LA GOTITA X 2 ML',NULL,10,'UNIDAD',1,1,'Peso'),(636,'646','WD - 40 AEROSOL X 155 GS',NULL,10,'UNIDAD',1,1,'Peso'),(637,'647','LA GOTITA GEL X 3 G',NULL,10,'UNIDAD',1,1,'Peso'),(638,'648','MECHA DE WIDIA SDS PLUS 12X160 MM',NULL,10,'UNIDAD',1,1,'Peso'),(639,'649','SIERRA COPA JGO X 5 PZAS',NULL,10,'UNIDAD',1,1,'Peso'),(640,'650','FLETE DE TERCERO',NULL,10,'UNIDAD',1,1,'Peso'),(641,'651','CARETA FOTOSENSIBLE STANLEY',NULL,10,'UNIDAD',1,1,'Peso'),(642,'652','PISTOLA APLICADORA REF.',NULL,10,'UNIDAD',1,1,'Peso'),(643,'653','SOGA JASPEADA 12 MM - POR METRO',NULL,10,'UNIDAD',1,1,'Peso'),(644,'654','VIDRIO RECTANGULAR AZUL T10-11-12',NULL,10,'UNIDAD',1,1,'Peso'),(645,'655','PROTECTOR FACIAL REPUESTO',NULL,10,'UNIDAD',1,1,'Peso'),(646,'656','ANTIPARRA CLARA',NULL,10,'UNIDAD',1,1,'Peso'),(647,'657','TAPON AUDITIVO - LIBUS',NULL,10,'UNIDAD',1,1,'Peso'),(648,'658','ESCUADRA CARPINTERO 30 CM -ALUM',NULL,10,'UNIDAD',1,1,'Peso'),(649,'659','ESCUADRA CARPINERO 40 CM - ALUM',NULL,10,'UNIDAD',1,1,'Peso'),(650,'660','BARREHOJA DE PLASTICO 25 DIENTES',NULL,10,'UNIDAD',1,1,'Peso'),(651,'661','MECHA DE WIDIA SDS PLUS 6X110 MM',NULL,10,'UNIDAD',1,1,'Peso'),(652,'662','MECHA DE WIDIA SDS PLUS 8X110 MM',NULL,10,'UNIDAD',1,1,'Peso'),(653,'663','MECHA DE WIDIA SDS PLUS 10X110 MM',NULL,10,'UNIDAD',1,1,'Peso'),(654,'664','ELECTRODO 2.5 MM -FADIRCO 6013',NULL,10,'UNIDAD',1,1,'Peso'),(655,'665','DISO DESBASTE 7\" GRIDEST',NULL,10,'UNIDAD',1,1,'Peso'),(656,'666','CEPILLO DE ACERO C/CABO 4X19 CM',NULL,10,'UNIDAD',1,1,'Peso'),(657,'667','CEPILLO SIN CABO 6X19 CM',NULL,10,'UNIDAD',1,1,'Peso'),(658,'668','TIRAFONDO 1/4 X 7,5 P/ TACO DEL \"10',NULL,10,'UNIDAD',1,1,'Peso'),(659,'669','TIRAFONDO 1/4 X 10 P/ TACO DEL \"10\"',NULL,10,'UNIDAD',1,1,'Peso'),(660,'670','TIRAFONDO 5/16 X 7,5 P/TACO DEL \"12',NULL,10,'UNIDAD',1,1,'Peso'),(661,'671','TACO DE NYLON 12 MM P/ HUECO',NULL,10,'UNIDAD',1,1,'Peso'),(662,'672','ELECTRODO 3.25 MM - FADIRCO 6013',NULL,10,'UNIDAD',1,1,'Peso'),(663,'673','ELECTRODO 2 MM - FADIRCO 6013',NULL,10,'UNIDAD',1,1,'Peso'),(664,'674','RASTRILLO 16 DIENTES S/CABO BIASSON',NULL,10,'UNIDAD',1,1,'Peso'),(665,'675','MECHA WIDIA SDS PLUS 12 X 160 MM',NULL,10,'UNIDAD',1,1,'Peso'),(666,'676','PLASTER FINO A LA CAL X 25 KG',NULL,10,'UNIDAD',1,1,'Peso'),(667,'677','PLASTER PEGAMENTO P/CERAMICO X 30',NULL,10,'UNIDAD',1,1,'Peso'),(668,'678','ANTEOJO PROTECTORES CLARO',NULL,10,'UNIDAD',1,1,'Peso'),(669,'679','ANTEOJOS SPY OSCURO',NULL,10,'UNIDAD',1,1,'Peso'),(670,'680','BOQUILLA MAGNETICA 3/8 X UNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(671,'681','CARETA SOLDADOR VISOR MOVIL',NULL,10,'UNIDAD',1,1,'Peso'),(672,'682','CASCO COMPLETO AMARILLO',NULL,10,'UNIDAD',1,1,'Peso'),(673,'683','CHALECO REFLECTIVO AMARILLO',NULL,10,'UNIDAD',1,1,'Peso'),(674,'684','CERAMICO 36X36 2DA (2.6 MT2)',NULL,10,'UNIDAD',1,1,'Peso'),(675,'685','CINTA DE PELIGRO X 200 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(676,'686','TRINCHETA DUROLL O CROSSMAN 18 MM',NULL,10,'UNIDAD',1,1,'Peso'),(677,'687','DISC DIAM CONTINUO 4 1/2 YARD',NULL,10,'UNIDAD',1,1,'Peso'),(678,'688','DISCO SEGMENTADO DE \"180\" MM',NULL,10,'UNIDAD',1,1,'Peso'),(679,'689','PROTECTOR AUDITIVO SAMURAI',NULL,10,'UNIDAD',1,1,'Peso'),(680,'690','ESPUMA DE POLIURETANO ADHESIL 300',NULL,10,'UNIDAD',1,1,'Peso'),(681,'691','LUBRICANTE P/TUBOS DUKE X 400',NULL,10,'UNIDAD',1,1,'Peso'),(682,'692','GUANTE DE LATEX INDUSTRIAL LIVIANO',NULL,10,'UNIDAD',1,1,'Peso'),(683,'693','GUANTE DESCARNE AMARILLO FERRETERO',NULL,10,'UNIDAD',1,1,'Peso'),(684,'694','GUANTE DE NITRILO DESCARTABLES',NULL,10,'UNIDAD',1,1,'Peso'),(685,'695','HILO PARA ALBAÑIL X 100 GR Nº 27',NULL,10,'UNIDAD',1,1,'Peso'),(686,'696','BARBIJO MASCARILLA P/POLVO',NULL,10,'UNIDAD',1,1,'Peso'),(687,'697','PILA SONY AA X 4 UNID',NULL,10,'UNIDAD',1,1,'Peso'),(688,'698','PILA SONY AAA X 4 UNID',NULL,10,'UNIDAD',1,1,'Peso'),(689,'699','PLASTICO NEGRO 200 MIC X 4 MT ANCHO',NULL,10,'UNIDAD',1,1,'Peso'),(690,'700','PUNTA ATORNILLADORA 25 MM WURTH',NULL,10,'UNIDAD',1,1,'Peso'),(691,'701','REGULADOR DE GAS DE 0.80 C/CABEZA',NULL,10,'UNIDAD',1,1,'Peso'),(692,'702','REGULADOR DE GAS 1.50 MT C/CABEZA',NULL,10,'UNIDAD',1,1,'Peso'),(693,'703','REGULADOR DE GAS 2 MT C/CABEZA',NULL,10,'UNIDAD',1,1,'Peso'),(694,'704','SILICONA TACSA -TRANS/BCA/NEGRA',NULL,10,'UNIDAD',1,1,'Peso'),(695,'705','SOGA JASPEADA 10 MM - POR METRO',NULL,10,'UNIDAD',1,1,'Peso'),(696,'706','SOGA JASPEADA 6 MM POR METRO',NULL,10,'UNIDAD',1,1,'Peso'),(697,'707','SOGA JASPEADA 8 MM POR METRO',NULL,10,'UNIDAD',1,1,'Peso'),(698,'708','TANZA CUADRADA P/ PASTO 3 MM',NULL,10,'UNIDAD',1,1,'Peso'),(699,'709','MEDIA SOMBRA NEGRA 80 % 4.2 ANCHOXM',NULL,10,'UNIDAD',1,1,'Peso'),(700,'710','RAMALTEE 100X60X90',NULL,10,'UNIDAD',1,1,'Peso'),(701,'711','RECEPTACULO DUCHA CLOACA 110X40',NULL,10,'UNIDAD',1,1,'Peso'),(702,'712','CODO A 45 HH 63MM CLOACAL',NULL,10,'UNIDAD',1,1,'Peso'),(703,'713','CODO A 90 HH 40 MM CLOACAL',NULL,10,'UNIDAD',1,1,'Peso'),(704,'714','CODO A 90 MH 40MM CLOACAL',NULL,10,'UNIDAD',1,1,'Peso'),(705,'715','CODO A 90 MH 63 MM CLOACAL',NULL,10,'UNIDAD',1,1,'Peso'),(706,'716','CODO P V C C/BASE 100 A 90',NULL,10,'UNIDAD',1,1,'Peso'),(707,'717','RAMAL TEE 100X60X90 P V C',NULL,10,'UNIDAD',1,1,'Peso'),(708,'718','PILETA DE PATIO 40X63 5 ENT.CLOACAL',NULL,10,'UNIDAD',1,1,'Peso'),(709,'719','CODO A 45ª MH 40 MM CLOACAL',NULL,10,'UNIDAD',1,1,'Peso'),(710,'720','CODO A 90ª HH 63 MM CLOACAL',NULL,10,'UNIDAD',1,1,'Peso'),(711,'721','CODO A 90º MH 50 MM CLOACAL',NULL,10,'UNIDAD',1,1,'Peso'),(712,'722','BUJE REDUCTOR MH 63X50 CLOACAL',NULL,10,'UNIDAD',1,1,'Peso'),(713,'723','ACIDO MURIATICO X 1 LT',NULL,10,'UNIDAD',1,1,'Peso'),(714,'724','SODA CAUTICA EN GRANO X 1/2 KG',NULL,10,'UNIDAD',1,1,'Peso'),(715,'725','MEMBRANA VENTO BLANCA X 4 L',NULL,10,'UNIDAD',1,1,'Peso'),(716,'726','PINCEL EKO C.BLANCA V1 Nº 30',NULL,10,'UNIDAD',1,1,'Peso'),(717,'727','LIJA AGUA AA Nº 180',NULL,10,'UNIDAD',1,1,'Peso'),(718,'728','CHAPA GALV. ACAN. N 25  1.10 X 1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(719,'729','CHAPA GALV. ACAN. N 27 X 1.10  X MT',NULL,10,'UNIDAD',1,1,'Peso'),(720,'730','PERFIL C LC 1.6\' 80-40-15 X 12 MT',NULL,10,'UNIDAD',1,1,'Peso'),(721,'731','PERFIL C LC 1.6\' 100-40-15 X12 MT',NULL,10,'UNIDAD',1,1,'Peso'),(722,'732','WEBER FINO HIDR. EXTERIOR X 25 KG',NULL,10,'UNIDAD',1,1,'Peso'),(723,'733','MEMBRANA LIQUIDA TEJA X 4 LT -VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(724,'734','MEMBRANA VENTO CEMENTO X 4 L',NULL,10,'UNIDAD',1,1,'Peso'),(725,'735','GRIFERIA BIDET VIVA PLUS',NULL,10,'UNIDAD',1,1,'Peso'),(726,'736','GRIFERIA DUCHA EMB. VIVA PLUS',NULL,10,'UNIDAD',1,1,'Peso'),(727,'737','FIJADOR AL AGUA X 20 LTS-VENIER',NULL,10,'UNIDAD',1,1,'Peso'),(728,'738','LADRILLO TIPO VISTA X UNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(729,'739','ENVASES PLASTICOS 0.70X0.40 CM',NULL,10,'UNIDAD',1,1,'Peso'),(730,'740','DISC.CORTE NORTON 4 1/2 1. MM',NULL,10,'UNIDAD',1,1,'Peso'),(731,'741','DIS. CORTE NORTON 7\" X 1.60 MM',NULL,10,'UNIDAD',1,1,'Peso'),(732,'742','HIDROFUGO SIKA     X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(733,'743','KLAUKOL IMPERMEABLE X 30 KG',NULL,10,'UNIDAD',1,1,'Peso'),(734,'744','PASTINA KLAUKOL BLENDA X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(735,'745','PASTINA KLAUKOL BOREAL X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(736,'746','PASTINA KLAUKOL BRUMA X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(737,'747','PASTINA KLAUKOL CAOBA X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(738,'748','PASTINA KLAUKOL COBRE X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(739,'749','PASTINA KLAUKOL ESTAÑO X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(740,'750','PASTINA KLAUKOL HULLA X 1KG',NULL,10,'UNIDAD',1,1,'Peso'),(741,'751','PASTINA KLAUKOL MERCURIO X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(742,'752','PASTINA KLAUKOL TALCO BCA. X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(743,'753','PASTINA KLAUKOL TEJA X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(744,'754','RAMAL TEE 100X100X90',NULL,10,'UNIDAD',1,1,'Peso'),(745,'755','SOLDADOR TECHISTA 10 KG BOCA 50 MM',NULL,10,'UNIDAD',1,1,'Peso'),(746,'756','FLOTANTE P/TANQUE 1/2 PLASTICO',NULL,10,'UNIDAD',1,1,'Peso'),(747,'757','FLOTANTE P/TANQUE 3/4 PLASTICO',NULL,10,'UNIDAD',1,1,'Peso'),(748,'758','CAÑO PVC 110 X 4 MT LINEA 3.2 REF.',NULL,10,'UNIDAD',1,1,'Peso'),(749,'759','PILETA LAVADERO PVC 49X43X28 CM',NULL,10,'UNIDAD',1,1,'Peso'),(750,'760','TAPA CIEGA AC.INOX 10X10 CM',NULL,10,'UNIDAD',1,1,'Peso'),(751,'761','TAPA CIEGA AC.INOX 12X12 CM',NULL,10,'UNIDAD',1,1,'Peso'),(752,'762','TAPA CIEGA AC.INOX 15X15 CM',NULL,10,'UNIDAD',1,1,'Peso'),(753,'763','TAPA CIEGA AC.INOX 20X20 CM',NULL,10,'UNIDAD',1,1,'Peso'),(754,'764','CUCHARA DE ALBAÑIL DUROLL ECO 7\"',NULL,10,'UNIDAD',1,1,'Peso'),(755,'765','CUCHARA ALBAÑIL TRAMONTINA 6\"',NULL,10,'UNIDAD',1,1,'Peso'),(756,'766','WEBER PORCELLANATO X30 KG',NULL,10,'UNIDAD',1,1,'Peso'),(757,'767','VENTANA ALUMINIO DE 1X1 CORREDIZA',NULL,10,'UNIDAD',1,1,'Peso'),(758,'768','VENTANA ALUMINIO DE 1.2X1 CORREDIZA',NULL,10,'UNIDAD',1,1,'Peso'),(759,'769','VENTANA DE ALUMINIO DE 1.5X1 CORRED',NULL,10,'UNIDAD',1,1,'Peso'),(760,'770','AIREADORES 40 X  26',NULL,10,'UNIDAD',1,1,'Peso'),(761,'771','ESPATULA PINTOR N°120',NULL,10,'UNIDAD',1,1,'Peso'),(762,'772','ESPATULA PINTOR N°140',NULL,10,'UNIDAD',1,1,'Peso'),(763,'773','VENDAS P/PARED 1 MT X 25 MT',NULL,10,'UNIDAD',1,1,'Peso'),(764,'774','LLANA FLEXIBLE 12X30 CM',NULL,10,'UNIDAD',1,1,'Peso'),(765,'775','ENDUIDO VENTO X4 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(766,'776','MEMBRANA VENTO BLANCA X 20 L',NULL,10,'UNIDAD',1,1,'Peso'),(767,'778','CAJA RECTANGULAR CHAPA',NULL,10,'UNIDAD',1,1,'Peso'),(768,'780','CAJA OCTOGONAL GRANDE CHAPA',NULL,10,'UNIDAD',1,1,'Peso'),(769,'781','CONECTOR 3/4´´ MATALICO',NULL,10,'UNIDAD',1,1,'Peso'),(770,'782','CONECTOR 7/8´´ METALICO',NULL,10,'UNIDAD',1,1,'Peso'),(771,'783','CORRUGADO 3/4 IGNIFUGO BCO TECNOCOM',NULL,10,'UNIDAD',1,1,'Peso'),(772,'784','CORRUGADO 7/8 IGNIFUGO BCO TECNOCOM',NULL,10,'UNIDAD',1,1,'Peso'),(773,'788','CINTA PASACABLE 15MTS',NULL,10,'UNIDAD',1,1,'Peso'),(774,'789','CINTA PASACABLE 20MTS',NULL,10,'UNIDAD',1,1,'Peso'),(775,'790','TAPA BASTIDOR',NULL,10,'UNIDAD',1,1,'Peso'),(776,'791','1 PUNTO EXTERIOR',NULL,10,'UNIDAD',1,1,'Peso'),(777,'792','INTERRUPTOR   CIOCCA',NULL,10,'UNIDAD',1,1,'Peso'),(778,'793','1 TOMACORRIENTE EXTERIOR',NULL,10,'UNIDAD',1,1,'Peso'),(779,'794','LAMPARA LED 10 W',NULL,10,'UNIDAD',1,1,'Peso'),(780,'796','CAÑO CORRUGADO 5/8 NARANJA 25MTS',NULL,10,'UNIDAD',1,1,'Peso'),(781,'797','RECEPTACULO CURVO',NULL,10,'UNIDAD',1,1,'Peso'),(782,'799','1 INTERRUPTOR Y 1 TOMA. CIOCCA EXT.',NULL,10,'UNIDAD',1,1,'Peso'),(783,'801','1 PUNTO Y 1 TOMA PARA EXTERIOR',NULL,10,'UNIDAD',1,1,'Peso'),(784,'802','TAPA CIEGA RECTANGULAR 5X10 PVC',NULL,10,'UNIDAD',1,1,'Peso'),(785,'804','TAPA CIEGA MIGÑON PVC',NULL,10,'UNIDAD',1,1,'Peso'),(786,'805','TAPA CIEGA P/OCTOGONAL GDE PVC.',NULL,10,'UNIDAD',1,1,'Peso'),(787,'806','PUERTA PLACA 0.80 X2.04 M MADERA',NULL,10,'UNIDAD',1,1,'Peso'),(788,'807','TAPA CIEGA OCTOGONAL GRANDE DE PVC',NULL,10,'UNIDAD',1,1,'Peso'),(789,'808','CAJA MIÑON METALICA',NULL,10,'UNIDAD',1,1,'Peso'),(790,'809','CONECTOR 5/8 METALICO',NULL,10,'UNIDAD',1,1,'Peso'),(791,'810','ZAPATILLA ECONOMICA S/CABLE',NULL,10,'UNIDAD',1,1,'Peso'),(792,'811','PROLONGADOR ECO. C/CABLE 3 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(793,'813','TOMACORRIENTE DUAL',NULL,10,'UNIDAD',1,1,'Peso'),(794,'814','TAPON CIEGO',NULL,10,'UNIDAD',1,1,'Peso'),(795,'815','PLASTICO NEGRO 100 MIC 3 MT A/1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(796,'816','CABLE UNIPOLAR 1X1.5 MM X MT',NULL,10,'UNIDAD',1,1,'Peso'),(797,'817','FICHA MACHO 3 PERNOS TIPO K-3',NULL,10,'UNIDAD',1,1,'Peso'),(798,'818','FICHA HEMBRA 3 PERNOS TIPO K-3',NULL,10,'UNIDAD',1,1,'Peso'),(799,'819','PANEL EXTERIOR LED 6W CUADRADO',NULL,10,'UNIDAD',1,1,'Peso'),(800,'820','PANEL EXTERIOR LED 12W CUADRADO',NULL,10,'UNIDAD',1,1,'Peso'),(801,'821','PANEL EXTERIOR LED 18W CUADRADO',NULL,10,'UNIDAD',1,1,'Peso'),(802,'822','FLORON CIRCULAR 12CM',NULL,10,'UNIDAD',1,1,'Peso'),(803,'823','CABLE UNIPOLAAR 1X2.5 MM X MT',NULL,10,'UNIDAD',1,1,'Peso'),(804,'824','CABLE TIPO TALLER 2X 1.5 MM',NULL,10,'UNIDAD',1,1,'Peso'),(805,'825','LATEX SOLAR EXTERIOR X 20 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(806,'826','LATEX VENTO INTERIOR X 10 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(807,'827','ANTIOXIDO GRIS X 1 LT VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(808,'828','ANTIOXIDO GRIS X 4 LTS VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(809,'829','ANTIOXIDO ROJO X 1 LT VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(810,'830','ALAMBRE GALVANIZADO Nº 19 - FINO',NULL,10,'UNIDAD',1,1,'Peso'),(811,'831','ALAMBRE GALVANIZADO Nº 16',NULL,10,'UNIDAD',1,1,'Peso'),(812,'832','SIERRA CIRCULAR 4 1/2 24 DIENTES',NULL,10,'UNIDAD',1,1,'Peso'),(813,'833','MECHA ACERO RAPIDO 12 MM',NULL,10,'UNIDAD',1,1,'Peso'),(814,'834','MECHA DE ACERO RAP.\"6.00\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(815,'835','MECHA DE ACERO RAP.\"8.00\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(816,'836','MECHA DE ACERO RAP.\"10.25\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(817,'837','FLEXIBLE ACERO INOC. 1/2 X 50 CM',NULL,10,'UNIDAD',1,1,'Peso'),(818,'838','CAJA P/ 4 TERMICA BIPOLAR C/PUERTA',NULL,10,'UNIDAD',1,1,'Peso'),(819,'839','DISCO TURBO 4 1/2 GRIDEST',NULL,10,'UNIDAD',1,1,'Peso'),(820,'840','NIVEL ALUMINIO 40 CM CROSSMAN',NULL,10,'UNIDAD',1,1,'Peso'),(821,'841','CINTA METRICA  10 MTS. ENGOMADA',NULL,10,'UNIDAD',1,1,'Peso'),(822,'843','SELLADOR PARA CARROCERIA SILOC',NULL,10,'UNIDAD',1,1,'Peso'),(823,'844','LATEX COLOR X 4 LT -VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(824,'845','LATEX COLOR VIOLETA X 4 LT',NULL,10,'UNIDAD',1,1,'Peso'),(825,'846','ELECTRODO CONARCO 13A - 2.50 MM',NULL,10,'UNIDAD',1,1,'Peso'),(826,'847','CANDADO MAKAO 25 MM',NULL,10,'UNIDAD',1,1,'Peso'),(827,'848','CINTA METRICA   HUALIAN X 5 MT',NULL,10,'UNIDAD',1,1,'Peso'),(828,'849','MEMBRANA AUTOADHESIVA 25 CM X 1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(829,'850','TINTA PARA BARNIZ X 60',NULL,10,'UNIDAD',1,1,'Peso'),(830,'851','CONVERTIDOR DE OXIDO NEGRO X 4 LT',NULL,10,'UNIDAD',1,1,'Peso'),(831,'852','SILICONA TRANSPARENTE WURTH X 280',NULL,10,'UNIDAD',1,1,'Peso'),(832,'853','PUNTA PH2 DOBLE PUNTA',NULL,10,'UNIDAD',1,1,'Peso'),(833,'854','DISCO CORTE WURTH 4 1/2 X1',NULL,10,'UNIDAD',1,1,'Peso'),(834,'855','PINTURA ASFALTICA X 4 LTS -S10',NULL,10,'UNIDAD',1,1,'Peso'),(835,'856','PINTURA ASFALTICA CLIPPERFLEX X 18',NULL,10,'UNIDAD',1,1,'Peso'),(836,'857','FICHA TRIPLE REFORZADA T/ZAPATILLA',NULL,10,'UNIDAD',1,1,'Peso'),(837,'858','KLAUKOL PORCELLANATO X 30 KG',NULL,10,'UNIDAD',1,1,'Peso'),(838,'859','ACELERANTE DE FRAGUE X 20 LTS SIKA',NULL,10,'UNIDAD',1,1,'Peso'),(839,'860','CABLE UNIPOLAR 1X2.5 MM X MT-KALOP',NULL,10,'UNIDAD',1,1,'Peso'),(840,'861','YESO TUYANGO X 25 KG',NULL,10,'UNIDAD',1,1,'Peso'),(841,'862','CANILLA DE BRONCE 1/2',NULL,10,'UNIDAD',1,1,'Peso'),(842,'863','DESTAPA CAÑERIAS S10',NULL,10,'UNIDAD',1,1,'Peso'),(843,'864',' ESM. SINT. VERDE INGLES X 4LT VENT',NULL,10,'UNIDAD',1,1,'Peso'),(844,'865','SOGA PRECINTO 6 MM X 1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(845,'866','CARRETILLA REFORZADA 90 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(846,'867','MASILLA SANITARIA X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(847,'868','REJA BRONCE 10X10 PULIDO',NULL,10,'UNIDAD',1,1,'Peso'),(848,'869','REJA PISO 12X12 BRONCE PULIDO',NULL,10,'UNIDAD',1,1,'Peso'),(849,'870','PISTOLA PINTAR CLASSIC METAL',NULL,10,'UNIDAD',1,1,'Peso'),(850,'872','SELLA CANALETA UV+ ALUMINIO 280ML',NULL,10,'UNIDAD',1,1,'Peso'),(851,'873','DESTORNILLADOR PHILLIP 150MM',NULL,10,'UNIDAD',1,1,'Peso'),(852,'874','DESTORNILLADOR PHILLIP 125 MM',NULL,10,'UNIDAD',1,1,'Peso'),(853,'875','CINCEL PLANO SDS PLUS 250 MM',NULL,10,'UNIDAD',1,1,'Peso'),(854,'877','CERAMICO 36X36 DE 1° X MT2 (2.60)',NULL,10,'UNIDAD',1,1,'Peso'),(855,'878','PERFIL C LC 1.6\' 120-50-15 X 12MT',NULL,10,'UNIDAD',1,1,'Peso'),(856,'879','HIERRO LISO 8 MM BARRA 12 MT',NULL,10,'UNIDAD',1,1,'Peso'),(857,'880','HIERRO LISO 10 MM BARRA 12 MT',NULL,10,'UNIDAD',1,1,'Peso'),(858,'881','HIERRO LISO 12 MM BARRA 12 MT',NULL,10,'UNIDAD',1,1,'Peso'),(859,'882','YESO X KG',NULL,10,'UNIDAD',1,1,'Peso'),(860,'883','PUERTA PLACA 0.70X2.04 MARCO MAD.',NULL,10,'UNIDAD',1,1,'Peso'),(861,'884','TEE DE 20 MM X 1/2\" C/ INSERTO H',NULL,10,'UNIDAD',1,1,'Peso'),(862,'885','TAPA CAMARA TEMPLE 60X60',NULL,10,'UNIDAD',1,1,'Peso'),(863,'886','ELECTRODO ACINDAR 2.5 MM X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(864,'887','LATEX SOLAR EXTERIOR X 10 LS.',NULL,10,'UNIDAD',1,1,'Peso'),(865,'888','LATEX SOLAR EXTERIOR X 4 LS.',NULL,10,'UNIDAD',1,1,'Peso'),(866,'889','TENAZA ARMADOR Nº12 \"BIASSONI\"',NULL,10,'UNIDAD',1,1,'Peso'),(867,'890','PICO PUNTA Y PALA DE 75 MM \"BIASSON',NULL,10,'UNIDAD',1,1,'Peso'),(868,'891','PLOMADA 400 GRS TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(869,'892','COLA VINILICA 250 GRS',NULL,10,'UNIDAD',1,1,'Peso'),(870,'893','LASUR VENTO CRISTAL X 1 LT.',NULL,10,'UNIDAD',1,1,'Peso'),(871,'894','LASUR VENTO CRISTAL X 4 LT.',NULL,10,'UNIDAD',1,1,'Peso'),(872,'895','VENTO LADRILLOS X 10 LS',NULL,10,'UNIDAD',1,1,'Peso'),(873,'896','JUEGO FERRUM-(IN.BI.LAV.COL.DEP.)',NULL,10,'UNIDAD',1,1,'Peso'),(874,'897','CORRUGADO 1\" INOFUGO X MT',NULL,10,'UNIDAD',1,1,'Peso'),(875,'898','TAPA CIEGA OCTOGONAL CHICA DE PVC',NULL,10,'UNIDAD',1,1,'Peso'),(876,'899','MEMBRANA VENTO CEMENTO X 20 L',NULL,10,'UNIDAD',1,1,'Peso'),(877,'900','MEMBRANA VENTO TEJA X 20 L',NULL,10,'UNIDAD',1,1,'Peso'),(878,'901','SELLADOE H3 X 125 CC',NULL,10,'UNIDAD',1,1,'Peso'),(879,'902','LIMA REDONDA C/MANGO 12\" CROSSMAN',NULL,10,'UNIDAD',1,1,'Peso'),(880,'903','LIMA MEDIA CAÑA C/MANGO 12\" CROSS',NULL,10,'UNIDAD',1,1,'Peso'),(881,'904','DESTORNILLADOR PLANO 15 MM-SOFT',NULL,10,'UNIDAD',1,1,'Peso'),(882,'905','DESTORNILLADOR PHILIPS X 6 PZAS \"RO',NULL,10,'UNIDAD',1,1,'Peso'),(883,'906','DESTORNILLADOR INGCO 6 MM',NULL,10,'UNIDAD',1,1,'Peso'),(884,'907','ESCUADRA ALUMINIO 300 MM -CROSS',NULL,10,'UNIDAD',1,1,'Peso'),(885,'909','LAPIZ PTA WIDIA',NULL,10,'UNIDAD',1,1,'Peso'),(886,'910','LLAVE ALLEN JGO X 8 - STANLEY',NULL,10,'UNIDAD',1,1,'Peso'),(887,'911','DESTORNILLADOR JUEGO ELECTRO 6 P',NULL,10,'UNIDAD',1,1,'Peso'),(888,'912','GRIFERIA PERIRANO DUCHA C/TRANS',NULL,10,'UNIDAD',1,1,'Peso'),(889,'913','GRIFERIA PEIRANO BIDET C/TRANF',NULL,10,'UNIDAD',1,1,'Peso'),(890,'914','GRIFERIA PEIRANO LAVATORIO C/SOP',NULL,10,'UNIDAD',1,1,'Peso'),(891,'915','GRIFERIA PARA COCINA PEIRANO VERIN',NULL,10,'UNIDAD',1,1,'Peso'),(892,'916','INODORO CAPEA ITALIANO BCO 1ª',NULL,10,'UNIDAD',1,1,'Peso'),(893,'917','BIDET CAPEA ITALIANA BCA 1ª',NULL,10,'UNIDAD',1,1,'Peso'),(894,'918','LAVATORIO CAPEA ITALIANA 3 AG 1ª',NULL,10,'UNIDAD',1,1,'Peso'),(895,'919','COLUMNA CAPEA ITALIANA BCA 1ª',NULL,10,'UNIDAD',1,1,'Peso'),(896,'920','DEPOSITO CAPEA BCO',NULL,10,'UNIDAD',1,1,'Peso'),(897,'921','MANGUERA DE RIEGO 1/2 X 15 MT-AZUL',NULL,10,'UNIDAD',1,1,'Peso'),(898,'922','ACOPLES VARIOS TRAMONTINA',NULL,10,'UNIDAD',1,1,'Peso'),(899,'924','ACOPLE LANZA PARA RIEGO TRAMONTINA',NULL,10,'UNIDAD',1,1,'Peso'),(900,'925','CANILLA P/ LAVATORIO PVC CROMO',NULL,10,'UNIDAD',1,1,'Peso'),(901,'926','CAJA P/ 2 TERMICA BIPOLAR C/PUERTA',NULL,10,'UNIDAD',1,1,'Peso'),(902,'927','CONECTOR PLASTICO 3/4',NULL,10,'UNIDAD',1,1,'Peso'),(903,'928','CAÑO RIGIDO PVC 20 MM X 3 MTS.',NULL,10,'UNIDAD',1,1,'Peso'),(904,'930','CINCEL DE PUNTA SDS PLUS 14X250 MM',NULL,10,'UNIDAD',1,1,'Peso'),(905,'931','BOVEDILLA EPS 12 CM X 0.42 X 1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(906,'932','TENAZA INGCO 8\"',NULL,10,'UNIDAD',1,1,'Peso'),(907,'934','ADHESIVO P/PVC DUKE X 250 CC',NULL,10,'UNIDAD',1,1,'Peso'),(908,'935','JUEGO CAPEA -(IN.BI.LAV.COL.DEP)',NULL,10,'UNIDAD',1,1,'Peso'),(909,'936','MEMBRANA VENTO BLANCA X 1 L',NULL,10,'UNIDAD',1,1,'Peso'),(910,'937','AEROSOL DORADO X 250 CC',NULL,10,'UNIDAD',1,1,'Peso'),(911,'938','PROTECTOR FACIAL COMPLETO',NULL,10,'UNIDAD',1,1,'Peso'),(912,'939','SOPLETE TECHISTA 50 MM',NULL,10,'UNIDAD',1,1,'Peso'),(913,'940','TARRAJA ECONOMICA',NULL,10,'UNIDAD',1,1,'Peso'),(914,'941','MALLA PLASTICA DE SOSTEN 2 X 50 MT',NULL,10,'UNIDAD',1,1,'Peso'),(915,'942','BACHA SIMPLE 52X32X13 C/SOP.DANUBIO',NULL,10,'UNIDAD',1,1,'Peso'),(916,'943','BACHA DOBLE 56X37X13 C/SOP.DANUBIO',NULL,10,'UNIDAD',1,1,'Peso'),(917,'944','ESPATULA PINTOR N°160',NULL,10,'UNIDAD',1,1,'Peso'),(918,'945','ESPATULA PINTOR N°220',NULL,10,'UNIDAD',1,1,'Peso'),(919,'946','LLAVE SICA TOMA DOBLE',NULL,10,'UNIDAD',1,1,'Peso'),(920,'947','HIERRO LISO 6 MM BARRA 12 MT',NULL,10,'UNIDAD',1,1,'Peso'),(921,'948','LAMPARA ECO LED 48W L/D E27',NULL,10,'UNIDAD',1,1,'Peso'),(922,'949','WEBER POTENCIADO X 30 KG',NULL,10,'UNIDAD',1,1,'Peso'),(923,'950','ESM. SINTETICO BERMELLON X 4 LT',NULL,10,'UNIDAD',1,1,'Peso'),(924,'951','LATEX COLOR VERDE CEMENTO X 10 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(925,'952','LATEX COLOR NEGRO X 10 LTS VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(926,'953','RUEDA PARA HORMIGONERA',NULL,10,'UNIDAD',1,1,'Peso'),(927,'954','GUANTE DE NITRLO RUGOSO NEGRO',NULL,10,'UNIDAD',1,1,'Peso'),(928,'955','ARO DE BASE DE GOMA P/INODORO',NULL,10,'UNIDAD',1,1,'Peso'),(929,'956','ARO DESPLAZADOR P/BASE DE INOD.30 M',NULL,10,'UNIDAD',1,1,'Peso'),(930,'957','ARO DESPLAZ.INODORO DE 55 MM',NULL,10,'UNIDAD',1,1,'Peso'),(931,'958','ARO DE PVC P/BASE INODORO',NULL,10,'UNIDAD',1,1,'Peso'),(932,'959','ARO REGULABLE P/BASE DE INOD.15 MM',NULL,10,'UNIDAD',1,1,'Peso'),(933,'960','SIFON SIMPLE PVC DUKE',NULL,10,'UNIDAD',1,1,'Peso'),(934,'961','SIFON DOBLE PVC DUKE',NULL,10,'UNIDAD',1,1,'Peso'),(935,'963','TELA MOSQUITERO PLASTICO 1.2 X 1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(936,'964','CAÑO ESTRUCTURAL 40X40 1.6',NULL,10,'UNIDAD',1,1,'Peso'),(937,'965','BOQUILLA MAGNETICA 7 /16 X UNIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(938,'966','TACO METALICO MR 10 FICHER X UNID',NULL,10,'UNIDAD',1,1,'Peso'),(939,'967','CEPILLO CURVO MANGO PLASTICO',NULL,10,'UNIDAD',1,1,'Peso'),(940,'968','CEPILLO P/ AMOLADORA PELO RIZADO',NULL,10,'UNIDAD',1,1,'Peso'),(941,'969','CEPILLO P/AMOLADORA COPA 4 100 MM',NULL,10,'UNIDAD',1,1,'Peso'),(942,'970','TACO METALICO MR 12',NULL,10,'UNIDAD',1,1,'Peso'),(943,'971','REDUCCION GOLIATY A EDISON',NULL,10,'UNIDAD',1,1,'Peso'),(944,'972','CADENA PLASTICA ROJA Y BCA.X MT',NULL,10,'UNIDAD',1,1,'Peso'),(945,'973','MECHA DE WIDIA SDS 16 MM X 210',NULL,10,'UNIDAD',1,1,'Peso'),(946,'974','MECHA DE WIDIA SDS 14 MM X 210',NULL,10,'UNIDAD',1,1,'Peso'),(947,'975','LLAVE FRANCESA CROSSMAN 12\"',NULL,10,'UNIDAD',1,1,'Peso'),(948,'976','MALLA S. Q84 15X15X4 MM (5X2.00 MT)',NULL,10,'UNIDAD',1,1,'Peso'),(949,'977','GRAMPA OMEGA 0.50 AGUA 2\"',NULL,10,'UNIDAD',1,1,'Peso'),(950,'978','GRAMPA OMEGA 0.60 AGUA 2 1/2\"',NULL,10,'UNIDAD',1,1,'Peso'),(951,'979','GRAMPA OMEGA 0.75 AGUA 3\"',NULL,10,'UNIDAD',1,1,'Peso'),(952,'980','GRAMPA OMEGA 100 AGUA 4\"',NULL,10,'UNIDAD',1,1,'Peso'),(953,'981','GRAMPA OMEGA 110 AGUA',NULL,10,'UNIDAD',1,1,'Peso'),(954,'982','FLEXIBLE MALLADO 30 CM AGUA',NULL,10,'UNIDAD',1,1,'Peso'),(955,'983','FLOTANTE BCE 1/2 P/ TANQUE C/BOYA',NULL,10,'UNIDAD',1,1,'Peso'),(956,'984','FLOTANTE BCE  3/4 P/TANQUE',NULL,10,'UNIDAD',1,1,'Peso'),(957,'985','REJA DE HIERRO C/MARCO 30X30',NULL,10,'UNIDAD',1,1,'Peso'),(958,'986','REJA C/TAPA GIRATORIA  11X11',NULL,10,'UNIDAD',1,1,'Peso'),(959,'987','REJA HIERRO C/ MARCO 25X25',NULL,10,'UNIDAD',1,1,'Peso'),(960,'988','REJA C/TAPA GIRATORIA 15 X15',NULL,10,'UNIDAD',1,1,'Peso'),(961,'989','CANILLA ESFERICA METALICA 3/4 ECO',NULL,10,'UNIDAD',1,1,'Peso'),(962,'990','ENDUIDO VENTO X 20 LT',NULL,10,'UNIDAD',1,1,'Peso'),(963,'991','METAL DESPLEGADO REF (2X0.75)',NULL,10,'UNIDAD',1,1,'Peso'),(964,'992','VENTO LADRILLOS X 4 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(965,'993','FLEXIBLE DE ACERO INOX 1/2',NULL,10,'UNIDAD',1,1,'Peso'),(966,'997','CONEXION CORRUGADA EXTENS.PVC H-M',NULL,10,'UNIDAD',1,1,'Peso'),(967,'998','CONEXION CORRUGADA  PVC CROMADA',NULL,10,'UNIDAD',1,1,'Peso'),(968,'999','MANGUERA CRISTAL 9X12 GRUESA',NULL,10,'UNIDAD',1,1,'Peso'),(969,'1000','ARO PARA BACHA DOBLE DANUBIO',NULL,10,'UNIDAD',1,1,'Peso'),(970,'1001','BOTIQUIN C/LUZ  N°22  -45X62',NULL,10,'UNIDAD',1,1,'Peso'),(971,'1002','BOTIQUIN C/MODULOS LAT.N°20 -60X56',NULL,10,'UNIDAD',1,1,'Peso'),(972,'1003','ENDUIDO SOLAR X 10 KG',NULL,10,'UNIDAD',1,1,'Peso'),(973,'1004','REMACHE 5 X 30 X 500 UNIDADES',NULL,10,'UNIDAD',1,1,'Peso'),(974,'1005','CAÑO MULTICAPA CLOACAL 110X 4 MT',NULL,10,'UNIDAD',1,1,'Peso'),(975,'1006','CODO CLOACAL 110X45°M-H MULTICAPA',NULL,10,'UNIDAD',1,1,'Peso'),(976,'1007','CODO CLOACAL 110X90° M-H MULTICAPA',NULL,10,'UNIDAD',1,1,'Peso'),(977,'1008','RAMAL Y CLOACAL 110X110 M-H MULTICA',NULL,10,'UNIDAD',1,1,'Peso'),(978,'1009','PASTINA P/PORCELLANATO X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(979,'1010','CORRUGADO 3/4  X 25 MTS -OVRA',NULL,10,'UNIDAD',1,1,'Peso'),(980,'1011','CORRUGADO7/8 X 25 MTS - OVRA',NULL,10,'UNIDAD',1,1,'Peso'),(981,'1012','SOMBRERO DE PVC 110',NULL,10,'UNIDAD',1,1,'Peso'),(982,'1013','RAMAL Y PVC 050X050 X 45°',NULL,10,'UNIDAD',1,1,'Peso'),(983,'1014','CODO PVC 0.40 A 90ª M.H BLANCO',NULL,10,'UNIDAD',1,1,'Peso'),(984,'1015','CURVA P.V.C 0.50 A 45ª M.H BLANCO',NULL,10,'UNIDAD',1,1,'Peso'),(985,'1016','ALAMBRE  NEG.N 8',NULL,10,'UNIDAD',1,1,'Peso'),(986,'1017','CINTA METRICA STANLEY 3 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(987,'1018','CINTA METRICA CROSSMAN 10 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(988,'1019','ESCALERA PINTOR 9 ESCALONES',NULL,10,'UNIDAD',1,1,'Peso'),(989,'1020','GRIFERIA P/MESADA METAL HIDR. PLUS',NULL,10,'UNIDAD',1,1,'Peso'),(990,'1021','MONOCOMANDO P/COCINA 40 ECO',NULL,10,'UNIDAD',1,1,'Peso'),(991,'1022','MONOCOMANDO P/COCINA PICO CISNE',NULL,10,'UNIDAD',1,1,'Peso'),(992,'1023','MONOCOMANDO MESADA HIDRO METAL',NULL,10,'UNIDAD',1,1,'Peso'),(993,'1024','FLOTANTE ERREDE 1/2 C/BOYA',NULL,10,'UNIDAD',1,1,'Peso'),(994,'1025','FLOTANTE ERREDE 3/4 C/ BOYA',NULL,10,'UNIDAD',1,1,'Peso'),(995,'1026','VALVULA DE RETENCION 1/2 BRONCE',NULL,10,'UNIDAD',1,1,'Peso'),(996,'1027','VALVULA DE RETENCION 3/4 BRONCE',NULL,10,'UNIDAD',1,1,'Peso'),(997,'1028','VALVULA DE RETENCION 1 BRONCE',NULL,10,'UNIDAD',1,1,'Peso'),(998,'1029','ASIENTO PARA INODORO INYECTADO STAN',NULL,10,'UNIDAD',1,1,'Peso'),(999,'1030','ASIENTO INODORO MADERA',NULL,10,'UNIDAD',1,1,'Peso'),(1000,'1031','MALLA NARANJA 70 GR. X 50 MT',NULL,10,'UNIDAD',1,1,'Peso'),(1001,'1032','BASE P/TANQUE P/ 400 - 600 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(1002,'1033','BASE P/ TANQUE P/850 - 1000 LTS',NULL,10,'UNIDAD',1,1,'Peso'),(1003,'1034','TANZA CUADRADA 2.5 MM X MT',NULL,10,'UNIDAD',1,1,'Peso'),(1004,'1035','PLACA YESO 1.2 X 2.4 X 9.5 MM',NULL,10,'UNIDAD',1,1,'Peso'),(1005,'1036','BOYA TERGOPOL 1/2 C/ROSCA',NULL,10,'UNIDAD',1,1,'Peso'),(1006,'1037','BOYA TERGOPOL 3/4 C/ROSCA',NULL,10,'UNIDAD',1,1,'Peso'),(1007,'1038','CABLE CANAL 14 X 70',NULL,10,'UNIDAD',1,1,'Peso'),(1008,'1039','CABLE CANAL 18X21 C/AD RUBEN CABRER',NULL,10,'UNIDAD',1,1,'Peso'),(1009,'1041','DUCHA P/ CALEFON',NULL,10,'UNIDAD',1,1,'Peso'),(1010,'1042','RESISTENCIA TIPO PALETA DE ALUMINIO',NULL,10,'UNIDAD',1,1,'Peso'),(1011,'1043','FLEXIBLE MALLADO P/GASX30 CM',NULL,10,'UNIDAD',1,1,'Peso'),(1012,'1044','CAJA 10X10 CHAPA',NULL,10,'UNIDAD',1,1,'Peso'),(1013,'1045','CAJA OCTOGONAL CHICA CHAPA',NULL,10,'UNIDAD',1,1,'Peso'),(1014,'1046','CABLE CANAL 20 X 10',NULL,10,'UNIDAD',1,1,'Peso'),(1015,'1047','CAJA DE 1 A 2 TERMICAS EXTERIOR KAL',NULL,10,'UNIDAD',1,1,'Peso'),(1016,'1048','CAJA DE 2 A 4 TERMICAS EXTERIOR KAL',NULL,10,'UNIDAD',1,1,'Peso'),(1017,'1049','CAJA P/ 1 TERMICA BIPOLAR C/PUERTA',NULL,10,'UNIDAD',1,1,'Peso'),(1018,'1050','CAJA 2-4 EXTERIOR C/PUERTA KALOP',NULL,10,'UNIDAD',1,1,'Peso'),(1019,'1051','CAñO RIGIDO 5/8\" PLASTICO X 3MTS.',NULL,10,'UNIDAD',1,1,'Peso'),(1020,'1052','CAñO RIGIDO 3/4\" PLASTICO X3MTS.',NULL,10,'UNIDAD',1,1,'Peso'),(1021,'1053','CAñO RIGIDO 7/8\" PLASTICO X3MTS.',NULL,10,'UNIDAD',1,1,'Peso'),(1022,'1054','CORRUGADO 1\" IGNIFUGO BCO TECNOCOM',NULL,10,'UNIDAD',1,1,'Peso'),(1023,'1055','CURVA 5/8\" PLASTICA',NULL,10,'UNIDAD',1,1,'Peso'),(1024,'1056','CURVA 3/4\" PLASTICA',NULL,10,'UNIDAD',1,1,'Peso'),(1025,'1057','CURVA 7/8\" PLASTICA',NULL,10,'UNIDAD',1,1,'Peso'),(1026,'1058','CINTA PASACABLE X10MTS',NULL,10,'UNIDAD',1,1,'Peso'),(1027,'1059','PARALELO 2 X 1 MM2',NULL,10,'UNIDAD',1,1,'Peso'),(1028,'1060','FICHA 2 PATAS MACHO ECONOMICA',NULL,10,'UNIDAD',1,1,'Peso'),(1029,'1061','FICHA TRIPLE REFORZADA',NULL,10,'UNIDAD',1,1,'Peso'),(1030,'1062','FICHA MACHO O HEMBRA 20 AM MIG',NULL,10,'UNIDAD',1,1,'Peso'),(1031,'1063','GAMPAS P/CABLE N°5 PY X 50U. PY',NULL,10,'UNIDAD',1,1,'Peso'),(1032,'1064','GRAMPAS P/CABLE N°8 PY X 50U. PY',NULL,10,'UNIDAD',1,1,'Peso'),(1033,'1065','GRAMPAS P/CABLE N°10 PY X50U. PY',NULL,10,'UNIDAD',1,1,'Peso'),(1034,'1066','GRAMPAS P/CABLE COAXIL N°10 PY X 20',NULL,10,'UNIDAD',1,1,'Peso'),(1035,'1067','GRAMPAS P/CABLE COAXIL N°6 PY X 50',NULL,10,'UNIDAD',1,1,'Peso'),(1036,'1068','GRAMPAS P/CABLE COAXIL N°8 PY X50U.',NULL,10,'UNIDAD',1,1,'Peso'),(1037,'1069','GRAMPAS P/CABLE COAXIL N°8 PY X50U.',NULL,10,'UNIDAD',1,1,'Peso'),(1038,'1070','MINGITORIO OVAL FERRUM 1°',NULL,10,'UNIDAD',1,1,'Peso'),(1039,'1071','GRAMPA OMEGA 3/4',NULL,10,'UNIDAD',1,1,'Peso'),(1040,'1072','GRAMPA OMEGA 7/8\"',NULL,10,'UNIDAD',1,1,'Peso'),(1041,'1073','PORTALAMPARA 3 PIEZAS NEGRO',NULL,10,'UNIDAD',1,1,'Peso'),(1042,'1074','PORTALAMPARA 3 PIEZAS BLANCO',NULL,10,'UNIDAD',1,1,'Peso'),(1043,'1075','FLORON 15 CM',NULL,10,'UNIDAD',1,1,'Peso'),(1044,'1076','LLAVE TERMICA SICA SIMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(1045,'1077','LLAVE TERMICA BIPOLAR SICA',NULL,10,'UNIDAD',1,1,'Peso'),(1046,'1078','LLAVE DISJUNTOR 2 X 25 A',NULL,10,'UNIDAD',1,1,'Peso'),(1047,'1079','2 TOMACORRIENTES PARA EXTERIOR',NULL,10,'UNIDAD',1,1,'Peso'),(1048,'1080','PLACA YESO 1.2 X 2.4 X 12.5 MM',NULL,10,'UNIDAD',1,1,'Peso'),(1049,'1081','MASILLA P/PLACA DE YESO X 15 KG',NULL,10,'UNIDAD',1,1,'Peso'),(1050,'1082','MASILLA P/PLACA DE YESO X 32 KG',NULL,10,'UNIDAD',1,1,'Peso'),(1051,'1083','MASILLA P/PLACA DE YESO X 6  KG',NULL,10,'UNIDAD',1,1,'Peso'),(1052,'1084','MONTANTE GAL. DE 35 MM PESADO',NULL,10,'UNIDAD',1,1,'Peso'),(1053,'1085','MONTANTE GAL. DE 69 MM. PESADO',NULL,10,'UNIDAD',1,1,'Peso'),(1054,'1086','SOLERA GALV. DE 35 MM PESADO',NULL,10,'UNIDAD',1,1,'Peso'),(1055,'1087','SOLERA GALV. DE 70 MM PESADA',NULL,10,'UNIDAD',1,1,'Peso'),(1056,'1088','CINTA PAPEL MICROPEFORADA X 20 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(1057,'1089','ROSETA RECTANGULAR 7X11CM',NULL,10,'UNIDAD',1,1,'Peso'),(1058,'1090','ROSETA CIRCULAR 8CM',NULL,10,'UNIDAD',1,1,'Peso'),(1059,'1091','ROSETA CIRCULAR 10CM',NULL,10,'UNIDAD',1,1,'Peso'),(1060,'1092','PICAPORTE ALUMINIO MONASTERIO',NULL,10,'UNIDAD',1,1,'Peso'),(1061,'1093','ROSETA CIRCULAR 12 CM',NULL,10,'UNIDAD',1,1,'Peso'),(1062,'1094','RAMAL Y PVC 110X0.63X45',NULL,10,'UNIDAD',1,1,'Peso'),(1063,'1095','ENCHUFE DOBLE DE RED 1\" A 1/2 POLIE',NULL,10,'UNIDAD',1,1,'Peso'),(1064,'1096','ENCHUFE TEE \"1/2\" POLIETILENO',NULL,10,'UNIDAD',1,1,'Peso'),(1065,'1097','PANEL EXTERIOR LED 24W L/D CUADRADO',NULL,10,'UNIDAD',1,1,'Peso'),(1066,'1098','CEMENTO BLANCO A GRANEL 25KG',NULL,10,'UNIDAD',1,1,'Peso'),(1067,'1099','DEPOSITO PVC COLGAR A CADENA',NULL,10,'UNIDAD',1,1,'Peso'),(1068,'1100','DEPOSITO PVC BOTON FRONTAL MONKOTT',NULL,10,'UNIDAD',1,1,'Peso'),(1069,'1101','FLEXIBLE MALLADO P/GAS 80 CM',NULL,10,'UNIDAD',1,1,'Peso'),(1070,'1102','FLEXIBLE MALLADO P/GASX50 CM',NULL,10,'UNIDAD',1,1,'Peso'),(1071,'1103','FLEXIBLE MALLADO P/GAS 100 CM',NULL,10,'UNIDAD',1,1,'Peso'),(1072,'1104','CONEXION TRAFUL FUELLE DE GOMA',NULL,10,'UNIDAD',1,1,'Peso'),(1073,'1105','CONEXION TRAFUL COMUN DE GOMA',NULL,10,'UNIDAD',1,1,'Peso'),(1074,'1106','TAPA ROSCA H 1/2 PPN',NULL,10,'UNIDAD',1,1,'Peso'),(1075,'1107','AUTOMATICO FIBOSA F 600',NULL,10,'UNIDAD',1,1,'Peso'),(1076,'1108','CHAPA TRASLUCIDA BCA. ACAN.X 1 MT',NULL,10,'UNIDAD',1,1,'Peso'),(1077,'1109','TAPA CAMARA TEMPLE 50X50',NULL,10,'UNIDAD',1,1,'Peso'),(1078,'1110','TAPA CAMARA TEMPLE 40X40',NULL,10,'UNIDAD',1,1,'Peso'),(1079,'1111','TAPA CAMARA TEMPLE 30X30',NULL,10,'UNIDAD',1,1,'Peso'),(1080,'1112','TAPA CAMARA TEMPLE 25X25',NULL,10,'UNIDAD',1,1,'Peso'),(1081,'1114','RASTRILLO 13 DIENTES TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(1082,'1115','RASTRILLO 16 DIENTES TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(1083,'1116','PLOMADA 300 GRS TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(1084,'1117','CURVA HEMBRA HEMBRA A 90º 1/2',NULL,10,'UNIDAD',1,1,'Peso'),(1085,'1118','PLOMADA 500 GRS TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(1086,'1119','PURTA PARA CASILLA DE GAS TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(1087,'1120','LIJAS TELA EKO N°60',NULL,10,'UNIDAD',1,1,'Peso'),(1088,'1121','LIJA DE TELA  N80-100-120-150-180',NULL,10,'UNIDAD',1,1,'Peso'),(1089,'1122','TIRAFONDO 5/16 X 6,5 P/TACO DEL \"12',NULL,10,'UNIDAD',1,1,'Peso'),(1090,'1123','LISTON P/REVESTIMIENTO',NULL,10,'UNIDAD',1,1,'Peso'),(1091,'1124','CEMENTO BLANCO A GRANEL X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(1092,'1125','SEELA CANALETA ADHESIL 265 GR',NULL,10,'UNIDAD',1,1,'Peso'),(1093,'1126','LAMPARA LED 8 W',NULL,10,'UNIDAD',1,1,'Peso'),(1094,'1127','LAMPARA LED \"12 W',NULL,10,'UNIDAD',1,1,'Peso'),(1095,'1128','PINZA MAZA P/SOLDADOR 500 A',NULL,10,'UNIDAD',1,1,'Peso'),(1096,'1129','PICO Y LORO INGNO Nº 10',NULL,10,'UNIDAD',1,1,'Peso'),(1097,'1130','CUCHARA DE ALBAÑIL INGNO Nº \"6',NULL,10,'UNIDAD',1,1,'Peso'),(1098,'1131','CUCHARA DE ALBAÑIL INGNO Nº \"8',NULL,10,'UNIDAD',1,1,'Peso'),(1099,'1132','MANGUERA CRISTAL P/ AIRE 16X19 X MT',NULL,10,'UNIDAD',1,1,'Peso'),(1100,'1133','LLAVE SICA DE PUNTO Y TOMA',NULL,10,'UNIDAD',1,1,'Peso'),(1101,'1134','LLAVE SICA DE UN 1 TOMA',NULL,10,'UNIDAD',1,1,'Peso'),(1102,'1135','PORTA LAMPARA CURVO',NULL,10,'UNIDAD',1,1,'Peso'),(1103,'1136','ROCETA REDONDA \"8 Y 10\" CM',NULL,10,'UNIDAD',1,1,'Peso'),(1104,'1137','LLAVE P/ LUZ 1 PUNTO CIOCCA',NULL,10,'UNIDAD',1,1,'Peso'),(1105,'1138','PASTINA KLAUKOL SILEX X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(1106,'1139','CEMENTO A GRANEL X 1 KG',NULL,10,'UNIDAD',1,1,'Peso'),(1107,'1140','PEGAMENTO P/CERAMICO X KG',NULL,10,'UNIDAD',1,1,'Peso'),(1108,'1141','FINO X KG',NULL,10,'UNIDAD',1,1,'Peso'),(1109,'1142','REMACHE RIBOT 4.0 X 10',NULL,10,'UNIDAD',1,1,'Peso'),(1110,'1143','ROLLO TEJIDO N°16 DE 1.50 X10 M-3\"',NULL,10,'UNIDAD',1,1,'Peso'),(1111,'1144','ALCA LIQUID - X 1 LT  SOODA  CAUTIC',NULL,10,'UNIDAD',1,1,'Peso'),(1112,'1145','RODILLO MINI EPOXI 8 CM',NULL,10,'UNIDAD',1,1,'Peso'),(1113,'1146','LATEX COLOR X 1 LT - VENTO',NULL,10,'UNIDAD',1,1,'Peso'),(1114,'1147','REJILLAS C/M Y CIEGA 10X10 TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(1115,'1148','REJILLAS C/M Y CIEGA 15X15 TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(1116,'1149','TANZA REDONDA 1.5 MM P/PASTO',NULL,10,'UNIDAD',1,1,'Peso'),(1117,'1150','MEMBRANA VENTO BLANCA X 10 L',NULL,10,'UNIDAD',1,1,'Peso'),(1118,'1151','VIGUETA PRETENSADA X 4.40 MT',NULL,10,'UNIDAD',1,1,'Peso'),(1119,'1152','LATEX SOLAR INTERIOR X 1 LS.',NULL,10,'UNIDAD',1,1,'Peso'),(1120,'1153','LATEX SOLAR INTERIOR X 10 LS.',NULL,10,'UNIDAD',1,1,'Peso'),(1121,'1154','REJILLAS C/M Y CIEGA 20X20 TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(1122,'1155','REJILLAS C/M Y CIEGA 30X30 TEMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(1123,'1156','ARENA MEDIANA X MT P/OBRA',NULL,10,'UNIDAD',1,1,'Peso'),(1124,'1157','RIPIO BRUTO FINO X MT P/OBRA',NULL,10,'UNIDAD',1,1,'Peso'),(1125,'1158','RIPIO 1 AL 3 X MT P/OBRA',NULL,10,'UNIDAD',1,1,'Peso'),(1126,'1159','ANGULO DE AJUSTE',NULL,10,'UNIDAD',1,1,'Peso'),(1127,'1160','CANTONERA PERFORADA',NULL,10,'UNIDAD',1,1,'Peso'),(1128,'1161','RAMAL TEE DE \"CHAPA\" 4\" ZING',NULL,10,'UNIDAD',1,1,'Peso'),(1129,'1162','CUPLA DE 20 X 3/4 C/INSERTO H',NULL,10,'UNIDAD',1,1,'Peso'),(1130,'1163','GRAMPA OMEGA 1/2',NULL,10,'UNIDAD',1,1,'Peso'),(1131,'1164','TAPA CAMARA TEMPLE 20 X 20',NULL,10,'UNIDAD',1,1,'Peso'),(1132,'1165','1 TOMACORRIENTE \"20 A\"  EXTERIOR',NULL,10,'UNIDAD',1,1,'Peso'),(1133,'1166','CABLE T/TALLER 2X 2..5   X MT',NULL,10,'UNIDAD',1,1,'Peso'),(1134,'1167','TACO PARA DURLOK',NULL,10,'UNIDAD',1,1,'Peso'),(1135,'1168','CANILLA DE BRONCE CROMO 1/2',NULL,10,'UNIDAD',1,1,'Peso'),(1136,'1169','CINTA FIBRA AUTOAD.48MM X 45 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(1137,'1170','RODILLO LANA 22 CM 70/30',NULL,10,'UNIDAD',1,1,'Peso'),(1138,'1171','BISAGRA MUNICION 100X37 MM',NULL,10,'UNIDAD',1,1,'Peso'),(1139,'1172','BISAGRA MUNICION 100X75 MM',NULL,10,'UNIDAD',1,1,'Peso'),(1140,'1173','MECHA D. ACERO RAP \"2.00\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1141,'1174','MECHA DE ACERO RAP: \"2.25\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1142,'1175','MECHA DE ACERO RAP. \"2.50\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1143,'1176','MECHA DE ACERO RAP. \"2.75\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1144,'1177','MECHA DE ACERO RAP. \"3.00\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1145,'1178','MECHA DE ACERO RAP. \"3.25\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1146,'1179','MECHA DE ACERO RAP\"3.75\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1147,'1180','MECHA DE ACERO RAP. \"4.00\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1148,'1181','MECHA DE ACERO RAP.\"4.25\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1149,'1182','MECHA DE ACERO RAP.\"4.50\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1150,'1183','MECHA DE ACERO RAP.\"4.75\"',NULL,10,'UNIDAD',1,1,'Peso'),(1151,'1184',' MECHA DE ACERO RAP. \" 5.25\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1152,'1185','MECHA DE ACERO RAP. \"5.50\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1153,'1186','MECHA DE ACERO RAP.\"5.75\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1154,'1187','MECHA DE ACERO RAP.\"6.25\"',NULL,10,'UNIDAD',1,1,'Peso'),(1155,'1188','MECHA DE ACERO RAP.\"6.75\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1156,'1189','MECHA DE ACERO RAP.\"10.00\"MM',NULL,10,'UNIDAD',1,1,'Peso'),(1157,'1190','RIPIO 1 AL 3 POR (6 MT3) CAMIONADA',NULL,10,'UNIDAD',1,1,'Peso'),(1158,'1191','VALVULA ESFERICA \"32\"',NULL,10,'UNIDAD',1,1,'Peso'),(1159,'1192','CAÑO 100º PVC SAN LORENZO',NULL,10,'UNIDAD',1,1,'Peso'),(1160,'1193','GUANTES DE DESCARNE PUÑO CORTO',NULL,10,'UNIDAD',1,1,'Peso'),(1161,'1194','TEFLON 3/4  ALTA DENSIDAD',NULL,10,'UNIDAD',1,1,'Peso'),(1162,'1195','PERFIL OMEGA',NULL,10,'UNIDAD',1,1,'Peso'),(1163,'1196','CODO DE 20 X 3/4 C/ INSERTO \"H',NULL,10,'UNIDAD',1,1,'Peso'),(1164,'1197','ARO DOBLE   DE ALUMINIO \" DANUBIO\"',NULL,10,'UNIDAD',1,1,'Peso'),(1165,'1198','FUELLE P/ INODORO DE GOMA LARGO',NULL,10,'UNIDAD',1,1,'Peso'),(1166,'1199','CALIBRE PLASTICO',NULL,10,'UNIDAD',1,1,'Peso'),(1167,'1200','MANGUERA P/ LAVARROPA X 2 MT',NULL,10,'UNIDAD',1,1,'Peso'),(1168,'1201','MALLA SQ188 15X15X6 MM (6X2.4)',NULL,10,'UNIDAD',1,1,'Peso'),(1169,'1202','SILICONA LUXOM 280 ML',NULL,10,'UNIDAD',1,1,'Peso'),(1170,'1203','LAVATORIO PVC 36X26 CM',NULL,10,'UNIDAD',1,1,'Peso'),(1171,'1204','MENSULA P/ESTANTE 20X25 CM BCA.',NULL,10,'UNIDAD',1,1,'Peso'),(1172,'1205','MENSULA P/ESTANTE 25X30 CM BCA.',NULL,10,'UNIDAD',1,1,'Peso'),(1173,'1206','TORNILLO FIX 4.5 X 30',NULL,10,'UNIDAD',1,1,'Peso'),(1174,'1207','TORNILLO FIX 4.5 X 4',NULL,10,'UNIDAD',1,1,'Peso'),(1175,'1208','BOMBA PERIFERICA 1/2 HP-PLUVIUS',NULL,10,'UNIDAD',1,1,'Peso'),(1176,'1209','CAMARA DE INSPECCION 60X60',NULL,10,'UNIDAD',1,1,'Peso'),(1177,'1210','PILETA LAVADERO CEMENTO 0.60 CM',NULL,10,'UNIDAD',1,1,'Peso'),(1178,'1211','PILETA LAVADERO CEMENTO 0.80 CM',NULL,10,'UNIDAD',1,1,'Peso'),(1179,'1212','MANDIL PLASTICO 12X20 CM',NULL,10,'UNIDAD',1,1,'Peso'),(1180,'1213','CURVA HEMBRA HEMBRA A 90º 3/4',NULL,10,'UNIDAD',1,1,'Peso'),(1181,'1214','CURVA P.V.C 0.63 A 90º LINEA 3,2\"',NULL,10,'UNIDAD',1,1,'Peso'),(1182,'1215','ZAPATILLA POR 5 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(1183,'1216','CINTA METRICA STANLEY 8 MTS',NULL,10,'UNIDAD',1,1,'Peso'),(1184,'1217','SIFON SIMPLE D/ GOMA C/ABRAZADERA',NULL,10,'UNIDAD',1,1,'Peso'),(1185,'1218','DISCO ULTRA FINO 4 1/2 PORCELLANATO',NULL,10,'UNIDAD',1,1,'Peso'),(1186,'1219','PILETA REVESTIDA C/ MESADA',NULL,10,'UNIDAD',1,1,'Peso'),(1187,'1220','LLAVE SICA D/EMBUTIR 20 AM',NULL,10,'UNIDAD',1,1,'Peso'),(1188,'1221','CAJA P/ 6 TERMICA BIPOLAR C/ PUERTA',NULL,10,'UNIDAD',1,1,'Peso'),(1189,'1222','AIREADORES 60 X 26',NULL,10,'UNIDAD',1,1,'Peso'),(1190,'1223','BOTIQUIN N°19 SIMPLE',NULL,10,'UNIDAD',1,1,'Peso'),(1191,'1224','CERAMICO 38X38 1° 2 MT X CAJA',NULL,10,'UNIDAD',1,1,'Peso'),(1192,'1225','LISTON P/ TUBO LED 2 X 18',NULL,10,'UNIDAD',1,1,'Peso'),(1193,'1226','FICHA P/ CALEFON',NULL,10,'UNIDAD',1,1,'Peso'),(1194,'1227','CABLE PARALELO 2 X 1.5 X MT',NULL,10,'UNIDAD',1,1,'Peso'),(1195,'1228','CABLE PARALELO 2 X 2.5 X MT',NULL,10,'UNIDAD',1,1,'Peso'),(1196,'1229','MARTILLO GALONERO INGCO 220 GR',NULL,10,'UNIDAD',1,1,'Peso'),(1197,'1230','RAFIA P/CERCO VERDE 1.5 MT ANCHO',NULL,10,'UNIDAD',1,1,'Peso'),(1198,'1231','KLAUKOL REFRACTARIO X10 KG',NULL,10,'UNIDAD',1,1,'Peso'),(1199,'1232','VENTILUZ H.ANGULO 40X20',NULL,10,'UNIDAD',1,1,'Peso'),(1200,'1233','VENTILUZ H.ANGULO 40X30',NULL,10,'UNIDAD',1,1,'Peso'),(1201,'1234','VENTILUZ H.ANGULO 50X30',NULL,10,'UNIDAD',1,1,'Peso'),(1202,'1235','TELA MOSQUITERA GALVANIZADO X 1.2',NULL,10,'UNIDAD',1,1,'Peso'),(1203,'1236','ACEITE PARA MADERA X 4 LTS S10',NULL,10,'UNIDAD',1,1,'Peso'),(1204,'1237','LLAVE SICA DE 1 PUNTO',NULL,10,'UNIDAD',1,1,'Peso'),(1205,'1238','CERAMICO 46X46 1ª -(2.11 MT)X CAJA',NULL,10,'UNIDAD',1,1,'Peso'),(1206,'1239','CANILLA ESFE.PVC DOBLE P/LAVA.\"DUKE',NULL,10,'UNIDAD',1,1,'Peso'),(1207,'1240','LISTON P/ TUBO LED 1X9',NULL,10,'UNIDAD',1,1,'Peso'),(1208,'1241','TUBO LED DE 9 W FRIO',NULL,10,'UNIDAD',1,1,'Peso'),(1209,'1242','TUBO LED DE 2 X 18 W FRIO',NULL,10,'UNIDAD',1,1,'Peso'),(1210,'1243','PUNTA PH2 / WHURT',NULL,10,'UNIDAD',1,1,'Peso'),(1211,'1244','ESPATULA 2.5\"',NULL,10,'UNIDAD',1,1,'Peso'),(1212,'1245','ESPATULA 3.5\"',NULL,10,'UNIDAD',1,1,'Peso'),(1213,'1246','ESPUMA DE POLIURETANO ADHESIL 400',NULL,10,'UNIDAD',1,1,'Peso'),(1214,'1247','MONOCOMANDO LAVATORIO ALTO',NULL,10,'UNIDAD',1,1,'Peso'),(1215,'1248','PITON ZIN C/TOPE/CERRADO/ABIERTO 10',NULL,10,'UNIDAD',1,1,'Peso'),(1216,'1249','PITON ZINCA S/TOPE ESCUADRA N 1',NULL,10,'UNIDAD',1,1,'Peso'),(1217,'1250','REJA VENTILACION 20X20 ESMALTADA',NULL,10,'UNIDAD',1,1,'Peso'),(1218,'1251','TORNILLO AUTOPERFORANTE 1 1/4 (32MM',NULL,10,'UNIDAD',1,1,'Peso'),(1219,'1252','BOQUILLA WURTH 10 MM',NULL,10,'UNIDAD',1,1,'Peso'),(1220,'1253','LIJA ESPONJA N°120-220-320-400',NULL,10,'UNIDAD',1,1,'Peso'),(1221,'1254','LIJA P/PARED WURTH VARIAS MEDIDAS',NULL,10,'UNIDAD',1,1,'Peso'),(1222,'1255','SILICONA WURTH MS TURBO',NULL,10,'UNIDAD',1,1,'Peso'),(1223,'1256','POXIRAN EN LATA 225 GR',NULL,10,'UNIDAD',1,1,'Peso'),(1224,'1257','ARO DESPLAZADOR B/ INODORO 120 MM',NULL,10,'UNIDAD',1,1,'Peso'),(1225,'1258','FLEXIBLE MALLADO 40 CM AGUA',NULL,10,'UNIDAD',1,1,'Peso'),(1226,'1259','PLACA SUPERBOAD STD 8 MM 1.2X2.4 MT',NULL,10,'UNIDAD',1,1,'Peso'),(1227,'1260','BUÑAS Z',NULL,10,'UNIDAD',1,1,'Peso'),(1228,'1261','CLAVO GANCHO \"28\"CM',NULL,10,'UNIDAD',1,1,'Peso'),(1229,'1262','DICROICA LED 5 W',NULL,10,'UNIDAD',1,1,'Peso'),(1230,'1263','PASTINA WEBER PLOMO X 2 KG',NULL,10,'UNIDAD',1,1,'Peso'),(1231,'1264','PORTA MANGUERA DE PARED',NULL,10,'UNIDAD',1,1,'Peso'),(1232,'1266','CAMPANA VENTILACION ESMALTADA',NULL,10,'UNIDAD',1,1,'Peso'),(1233,'1267','CAMPANA VENTILACION AC. INOX',NULL,10,'UNIDAD',1,1,'Peso'),(1234,'10001','PINZA PORTA ELECTRODO 500 A',NULL,10,'UNIDAD',1,1,'Peso');

#
# Structure for table "detalledepresupuestos"
#

DROP TABLE IF EXISTS `detalledepresupuestos`;
CREATE TABLE `detalledepresupuestos` (
  `idProductos` int(11) unsigned NOT NULL DEFAULT 0,
  `idPresupuestos` int(11) unsigned NOT NULL DEFAULT 0,
  `cantidad` int(11) DEFAULT NULL,
  `precioVenta` double(10,2) DEFAULT NULL,
  `idTipoiva` int(11) unsigned DEFAULT 3,
  `bonificacion` double(10,2) DEFAULT 0.00,
  PRIMARY KEY (`idProductos`,`idPresupuestos`),
  KEY `fk_productos_has_presupuestos_presupuestos1_idx` (`idPresupuestos`),
  KEY `fk_productos_has_presupuestos_productos1_idx` (`idProductos`),
  KEY `fk_detallepre_tipoiva` (`idTipoiva`),
  CONSTRAINT `fk_detallepre_tipoiva` FOREIGN KEY (`idTipoiva`) REFERENCES `afip_tipoiva` (`idTipoiva`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_has_presupuestos_presupuestos1` FOREIGN KEY (`idPresupuestos`) REFERENCES `presupuestos` (`idPresupuestos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_productos_has_presupuestos_productos1` FOREIGN KEY (`idProductos`) REFERENCES `productos` (`idProductos`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "detalledepresupuestos"
#


#
# Structure for table "proveedores"
#

DROP TABLE IF EXISTS `proveedores`;
CREATE TABLE `proveedores` (
  `idProveedores` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idTipodoc` int(11) unsigned NOT NULL DEFAULT 1,
  `documento` varchar(20) NOT NULL DEFAULT '0',
  `razonsocial` varchar(255) DEFAULT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `codigopostal` int(11) DEFAULT NULL,
  PRIMARY KEY (`idProveedores`),
  KEY `fk_proveedores_tipodoc` (`idTipodoc`),
  CONSTRAINT `fk_proveedores_tipodoc` FOREIGN KEY (`idTipodoc`) REFERENCES `afip_tipodoc` (`idTipodoc`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

#
# Data for table "proveedores"
#

INSERT INTO `proveedores` VALUES (1,25,'30675407416','FERRETERA DEL NORTE SRL','TUCUMAN','SAN MIGUEL 652',4000),(2,35,'','JOSE PEREZ','TUCUMAN','MARCOS PAZ 1700',4000),(3,25,'30715786083','NOERTE HERRAMIENTAS','TUCUMAN','BENJAMIN VILLAFAÑE 1569',4000),(4,35,'1','EMI','TUCUMAN','AV ROCA 135',4000),(5,35,'','CUEVAS JUAN','TUCUMAN','SAN ANDRES',4000),(6,35,'','SUAREZ CESAR','TUCUMAN','ALDERETES',NULL),(7,35,'1','CERAMICA MARCOS PAZ','TUCUMAN','FEDERICO ROSSI 4TA CUADRA',4107),(8,35,'','NORVIGUET SRL','TUCUMAN','AV. NESTOR KIRCHENER 1801',NULL),(9,25,'20259232008','SUAREZ CARLOLS OSCAR','TUCUMAN','LARREA 452',4000),(10,35,'','DISTRIBUIDORA 70/30','TUCUMAN','CALLE 20 ENTRE 1 Y AUTOPISTA VMM',40000),(11,35,'','LIMBADI SRL','TUCUMAN','FRANCISCO DE AGUIRRE 144',40000),(12,25,'33714793069','LAITEN NOA SRL.','TUCUMAN','LAVALLE 3228',4000),(13,25,'30555427227','H.J.HERRERA SRL','TUCUMAN','SANTIAGO 2574',4000),(14,25,'20201780498','PRO NOA','TUCUMAN','Bº120 VIVIENDAS MAZ B2 CASA 25',4107),(15,25,'30712095063','DURAFORT SRL','TUCUMAN','PJE BOULONGE SUR MER 3334',4000),(16,35,'','VISCARRA LUCIANA - MEMBRATUC','TUCUMAN','RUTA PROV 301 - MANATIAL',4000),(17,25,'20184667437','JFP DISTRIBUSIONES','TUCUMAN','GENERAL PAZ 2370',4000),(18,25,'20266842717','AGRO PAC DISTRIBUIDORA','TUCUMAN',NULL,4000),(19,35,'','CRETO','TUCUMAN',NULL,NULL),(20,25,'30611887090','EXPRESO SAN JOSE SA.','TUCUMAN','AV. ALFONSINA STORNI 97',4000),(21,35,'','SANCHEZ BOLSAS','TUCUMAN','BOLIVIA 1760',4000),(22,25,'30556095597','PAREX KLAUKOL SA.','TUCUMAN','RUTA 302 KM 14',NULL),(23,35,'','DIS-NOR','TUCUMAN','RAMON CARRILLO 2189',NULL),(24,35,'','BRINGAS ABERTURAS','TUCUMAN','MANANTIAL',NULL),(25,35,'',NULL,'TUCUMAN','NORTE LUZ',NULL),(26,25,'20167845011','NORTE LUZ - ROMULO VALDEZ','TUCUMAN','ITALIA 64',4000),(27,25,'30631810582','WURTH ARGENTINA SA','BUENOS AIRES','AUTOVIA PROV KM 6 101.5 BUENOS AIRE',1814),(28,35,'','TEMPLE','CIUDAD DE BUENOS AIRES',NULL,NULL),(29,35,'','SORANE','COLOMBRES',NULL,NULL),(30,25,'20102209509','CARCARA FELIPE -FERRUM','TUCUMAN','ITALIA 873',4000),(31,35,'','ZERAMICO','TUCUMAN',NULL,NULL),(32,35,'','GOLIAT DISTRIBUIDORA','TUCUMAN','AV COLON 1200',4000),(33,35,'','SOL DISTRIBUCIONES (BERCOVICH)','TUCUMAN','LASTENIA',4000),(34,35,'','TEMPLE','TUCUMAN',NULL,4000),(35,35,'','VENTI-SERVI - PABLO','TUCUMAN','CORDOBA',4000),(36,35,'','JUAN - PLACAS DECORATIVAS','TUCUMAN',NULL,NULL),(37,25,'30712201505','MACRIAN SRL','CORDOBA UNIDAD POSTAL 19','DIAGONAL ICA 1477 CORDOBA',NULL),(38,35,'','VERA','TUCUMAN',NULL,NULL),(39,25,'30715278681','TRES A PERFILES SRL.','MANANTIAL','AV. BELGRANO  N°0 EL MANATIAL',4000),(40,35,'','VENTILUZ DE ANGULO','TUCUMAN',NULL,NULL);

#
# Structure for table "iva_compra"
#

DROP TABLE IF EXISTS `iva_compra`;
CREATE TABLE `iva_compra` (
  `idIvaCompra` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fechacbte` varchar(50) NOT NULL DEFAULT '',
  `idTipoCbte` int(11) unsigned NOT NULL DEFAULT 0,
  `puntodeventa` int(2) unsigned NOT NULL DEFAULT 0,
  `numerocbte` varchar(20) NOT NULL DEFAULT '0',
  `numerodespacho` varchar(16) NOT NULL DEFAULT '',
  `idProveedores` int(11) unsigned NOT NULL DEFAULT 1,
  `importeoperacion` double(13,2) unsigned DEFAULT 0.00,
  `importenointegraprecioneto` double(13,2) unsigned DEFAULT 0.00,
  `importeoperacionesexentas` double(13,2) unsigned DEFAULT 0.00,
  `inportevaloragregado` double(13,2) unsigned DEFAULT 0.00,
  `importeimpuestosnacionales` double(13,2) unsigned DEFAULT 0.00,
  `importeingresosbrutos` double(13,2) unsigned DEFAULT 0.00,
  `importeimpuestosmunicipales` double(13,2) unsigned DEFAULT 0.00,
  `importeimpuestosinternos` double(13,2) unsigned DEFAULT 0.00,
  `idTipomoneda` int(11) unsigned NOT NULL DEFAULT 1,
  `tipocambio` int(10) unsigned DEFAULT 0,
  `cantidadiva` int(11) DEFAULT 0,
  `idCodigoOperacion` int(11) unsigned NOT NULL DEFAULT 0,
  `creditofiscal` double(13,2) unsigned DEFAULT 0.00,
  `otrostributos` double(13,2) unsigned DEFAULT 0.00,
  `cuitcorredor` int(10) unsigned DEFAULT 0,
  `denominacioncorredor` varchar(30) DEFAULT '',
  `ivacorredor` double(13,2) unsigned DEFAULT 0.00,
  PRIMARY KEY (`idIvaCompra`),
  KEY `fk_ivacompra_tipocbte` (`idTipoCbte`),
  KEY `fk_ivacompra_tipodoc` (`idProveedores`),
  KEY `fk_ivacompra_tipomoneda` (`idTipomoneda`),
  KEY `fk_ivacompra_operacion` (`idCodigoOperacion`),
  CONSTRAINT `fk_ivacompra_operacion` FOREIGN KEY (`idCodigoOperacion`) REFERENCES `afip_operacion` (`idCodigoOperacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ivacompra_proveedores` FOREIGN KEY (`idProveedores`) REFERENCES `proveedores` (`idProveedores`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ivacompra_tipocbte` FOREIGN KEY (`idTipoCbte`) REFERENCES `afip_tipocbte` (`idTipoCbte`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ivacompra_tipomoneda` FOREIGN KEY (`idTipomoneda`) REFERENCES `afip_tipomoneda` (`idTipomoneda`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "iva_compra"
#

INSERT INTO `iva_compra` VALUES (1,'16-11-2020',1,12,'2342134324','',2,21312321.00,23.00,0.00,0.00,0.00,0.00,0.00,0.00,1,1,0,1,0.00,0.00,0,'',0.00);

#
# Structure for table "iva_compra_alicuotas"
#

DROP TABLE IF EXISTS `iva_compra_alicuotas`;
CREATE TABLE `iva_compra_alicuotas` (
  `idIvaCompraAlicuota` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idIvaCompra` int(11) unsigned NOT NULL DEFAULT 0,
  `importenetogravado` double(13,2) unsigned NOT NULL DEFAULT 0.00,
  `idTipoiva` int(11) unsigned NOT NULL DEFAULT 0,
  `impuestoliquidado` double(13,2) unsigned NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`idIvaCompraAlicuota`),
  KEY `fk_ivacompraalicuota_tipoiva` (`idTipoiva`),
  KEY `fk_ivacompraalicuota_ivacompra` (`idIvaCompra`),
  CONSTRAINT `fk_ivacompraalicuota_ivacompra` FOREIGN KEY (`idIvaCompra`) REFERENCES `iva_compra` (`idIvaCompra`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ivacompraalicuota_tipoiva` FOREIGN KEY (`idTipoiva`) REFERENCES `afip_tipoiva` (`idTipoiva`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "iva_compra_alicuotas"
#


#
# Structure for table "agenda"
#

DROP TABLE IF EXISTS `agenda`;
CREATE TABLE `agenda` (
  `idagenda` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nombreRepresentante` varchar(100) DEFAULT NULL,
  `apellidoRepresentante` varchar(100) DEFAULT NULL,
  `area` varchar(100) DEFAULT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `telefono` varchar(50) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `codigopostal` int(11) DEFAULT NULL,
  `mail` varchar(100) DEFAULT NULL,
  `idProveedores` int(11) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`idagenda`),
  KEY `fk_agenda_proveedores1` (`idProveedores`),
  CONSTRAINT `fk_agenda_proveedores1` FOREIGN KEY (`idProveedores`) REFERENCES `proveedores` (`idProveedores`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "agenda"
#


#
# Structure for table "puntosdeventas"
#

DROP TABLE IF EXISTS `puntosdeventas`;
CREATE TABLE `puntosdeventas` (
  `idPuntosdeVentas` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `numero` int(11) unsigned NOT NULL DEFAULT 0,
  `descripcion` varchar(255) DEFAULT NULL,
  `Sistema` varchar(255) DEFAULT NULL,
  `Domicilio` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idPuntosdeVentas`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "puntosdeventas"
#

INSERT INTO `puntosdeventas` VALUES (1,3,NULL,NULL,NULL);

#
# Structure for table "facturacion"
#

DROP TABLE IF EXISTS `facturacion`;
CREATE TABLE `facturacion` (
  `idFacturacion` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idPedidos` int(11) unsigned DEFAULT NULL,
  `idTipoCbte` int(11) unsigned NOT NULL DEFAULT 0,
  `numeroCbte` int(11) DEFAULT 0,
  `fechaCbte` varchar(25) DEFAULT NULL,
  `idPuntosdeVentas` int(11) unsigned NOT NULL DEFAULT 0,
  `totalVenta` double(11,4) DEFAULT 0.0000,
  `totalIVA` double(11,4) DEFAULT 0.0000,
  `CAE` int(11) DEFAULT 0,
  `descripcion` varchar(255) DEFAULT NULL,
  `horaCbte` varchar(255) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idFacturacion`),
  KEY `fk_facturacion_tipocbte` (`idTipoCbte`),
  KEY `fk_facturacion_pedidos` (`idPedidos`),
  KEY `fk_facturacion_ptovta` (`idPuntosdeVentas`),
  CONSTRAINT `fk_facturacion_pedidos` FOREIGN KEY (`idPedidos`) REFERENCES `pedidos` (`idPedidos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_facturacion_ptovta` FOREIGN KEY (`idPuntosdeVentas`) REFERENCES `puntosdeventas` (`idPuntosdeVentas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_facturacion_tipocbte` FOREIGN KEY (`idTipoCbte`) REFERENCES `afip_tipocbte` (`idTipoCbte`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "facturacion"
#


#
# Structure for table "stock"
#

DROP TABLE IF EXISTS `stock`;
CREATE TABLE `stock` (
  `idStock` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL DEFAULT 0,
  `fecha` varchar(20) DEFAULT NULL,
  `precioCompra` double(11,2) DEFAULT NULL,
  `precioVenta` double(11,2) DEFAULT NULL,
  `idProductos` int(11) unsigned NOT NULL DEFAULT 0,
  `idDepositos` int(11) unsigned NOT NULL DEFAULT 0,
  `idTipoiva` int(11) unsigned DEFAULT 3,
  `bonificacion` double(7,2) DEFAULT 0.00,
  `descripcion` varchar(255) DEFAULT NULL,
  `idProveedores` int(11) unsigned DEFAULT NULL,
  `numerocbte` varchar(20) DEFAULT '0',
  `precioVenta2` double(11,2) DEFAULT NULL,
  `id_usuario` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`idStock`),
  KEY `fk_stock_tipoiva` (`idTipoiva`),
  KEY `fk_stock_deposito` (`idDepositos`) USING BTREE,
  KEY `fk_stock_productos` (`idProductos`),
  KEY `fk_stock_proveedores` (`idProveedores`),
  CONSTRAINT `fk_stock_deposito` FOREIGN KEY (`idDepositos`) REFERENCES `deposito` (`idDepositos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stock_productos` FOREIGN KEY (`idProductos`) REFERENCES `productos` (`idProductos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stock_proveedores` FOREIGN KEY (`idProveedores`) REFERENCES `proveedores` (`idProveedores`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_stock_tipoiva` FOREIGN KEY (`idTipoiva`) REFERENCES `afip_tipoiva` (`idTipoiva`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

#
# Data for table "stock"
#

INSERT INTO `stock` VALUES (1,500,'16-11-2020',350.00,540.00,1,1,5,0.00,'COMPRA DE PRODUCTO',4,'0005151851',560.00,1),(2,800,'16-11-2020',30.00,45.00,34,1,5,0.00,'COMPRA DE PRODUCTO',7,'0000518519',60.00,1),(3,-270,'16-11-2020',NULL,45.00,34,1,5,0.00,'PEDIDO Nro 1',NULL,'0',NULL,1),(4,-2,'16-11-2020',NULL,540.00,1,1,5,0.00,'PEDIDO Nro 2',NULL,'0',NULL,1),(5,-250,'16-11-2020',NULL,45.00,34,1,5,0.00,'PEDIDO Nro 3',NULL,'0',NULL,1),(6,250,'16-11-2020',NULL,45.00,34,1,5,0.00,'ANULACION DE PEDIDO Nro 3',NULL,'0',NULL,1);

#
# Structure for table "detalledepedidos"
#

DROP TABLE IF EXISTS `detalledepedidos`;
CREATE TABLE `detalledepedidos` (
  `idDetallePedidos` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `idPedidos` int(11) unsigned NOT NULL DEFAULT 0,
  `idStock` int(11) unsigned NOT NULL DEFAULT 0,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`idDetallePedidos`),
  KEY `fk_detalle_stock` (`idStock`),
  KEY `fk_detalle_pedidos` (`idPedidos`) USING BTREE,
  CONSTRAINT `fk_detalle_pedidos` FOREIGN KEY (`idPedidos`) REFERENCES `pedidos` (`idPedidos`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_stock` FOREIGN KEY (`idStock`) REFERENCES `stock` (`idStock`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

#
# Data for table "detalledepedidos"
#

INSERT INTO `detalledepedidos` VALUES (1,1,3,270),(2,2,4,2),(3,3,5,250);

#
# Structure for table "tarjetacredito"
#

DROP TABLE IF EXISTS `tarjetacredito`;
CREATE TABLE `tarjetacredito` (
  `idTarjetaCredito` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) DEFAULT NULL,
  `descuento` double(11,2) DEFAULT 0.00,
  PRIMARY KEY (`idTarjetaCredito`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

#
# Data for table "tarjetacredito"
#

INSERT INTO `tarjetacredito` VALUES (1,'1 CUOTA',5.00),(2,'3 CUOTAS',10.00),(3,'6 CUOTAS',0.00),(8,'OTROS',5.00),(9,'DOLAR',7000.00);

#
# Structure for table "tarjetadebito"
#

DROP TABLE IF EXISTS `tarjetadebito`;
CREATE TABLE `tarjetadebito` (
  `idTarjetaDebito` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(150) DEFAULT NULL,
  `descuento` double(11,2) DEFAULT 0.00,
  PRIMARY KEY (`idTarjetaDebito`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "tarjetadebito"
#

INSERT INTO `tarjetadebito` VALUES (1,'MERCADO PAGO',5.00);

#
# Structure for table "usuarios"
#

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `idUsuarios` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `apellido` varchar(45) NOT NULL DEFAULT '',
  `nombre` varchar(45) NOT NULL DEFAULT '',
  `usuario` varchar(45) NOT NULL DEFAULT '',
  `contraseña` varchar(45) NOT NULL DEFAULT '',
  `acceso_datos` varchar(15) NOT NULL DEFAULT '000000000000000',
  `acceso_ventas` varchar(15) NOT NULL DEFAULT '000000000000000',
  `acceso_consultas` varchar(15) NOT NULL DEFAULT '000000000000000',
  `acceso_afip` varchar(15) NOT NULL DEFAULT '000000000000000',
  PRIMARY KEY (`idUsuarios`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

#
# Data for table "usuarios"
#

INSERT INTO `usuarios` VALUES (1,'YBC','Computacion','admin','admin','111111111111111','111111111111111','111111111111111','111111111111111'),(2,'Formoso','Eduardo','Formoso','FormosoE','111111111111111','111111111111111','111111111111111','111111111111111');

#
# View "vista_cliente_ranking"
#

DROP VIEW IF EXISTS `vista_cliente_ranking`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_cliente_ranking`
  AS
  SELECT
    `clientes`.`nombre` AS 'cliente',
    `afip_tiporesponsable`.`descripcion` AS 'tiporesponsable',
    `afip_tipodoc`.`descripcion` AS 'tipodoc',
    `clientes`.`IvaDocumento`,
    SUM(`pedidos`.`total`) AS 'total'
  FROM
    (((`pedidos`
      JOIN `clientes` ON (`pedidos`.`idClientes` = `clientes`.`idClientes`))
      JOIN `afip_tipodoc` ON (`clientes`.`idTipodoc` = `afip_tipodoc`.`idTipodoc`))
      JOIN `afip_tiporesponsable` ON (`clientes`.`idTiporesponsable` = `afip_tiporesponsable`.`idTiporesponsable`))
  WHERE
    `pedidos`.`idClientes` <> 1
  GROUP BY
    `clientes`.`nombre`, `clientes`.`IvaDocumento`
  ORDER BY SUM(`pedidos`.`total`) DESC;

#
# View "vista_clientes"
#

DROP VIEW IF EXISTS `vista_clientes`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_clientes`
  AS
  SELECT
    `clientes`.`idClientes`,
    `clientes`.`nombre`,
    `afip_tiporesponsable`.`descripcion` AS 'tiporesponsable',
    `afip_tipodoc`.`descripcion` AS 'tipodoc',
    `clientes`.`IvaDocumento`,
    `clientes`.`margen`
  FROM
    ((`clientes`
      JOIN `afip_tipodoc` ON (`clientes`.`idTipodoc` = `afip_tipodoc`.`idTipodoc`))
      JOIN `afip_tiporesponsable` ON (`clientes`.`idTiporesponsable` = `afip_tiporesponsable`.`idTiporesponsable`));

#
# View "vista_ctacte_detalle"
#

DROP VIEW IF EXISTS `vista_ctacte_detalle`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_ctacte_detalle`
  AS
  SELECT
    `detalledecuentacorriente`.`descripcion`,
    `detalledecuentacorriente`.`debe`,
    `detalledecuentacorriente`.`haber`,
    `detalledecuentacorriente`.`fecha`,
    `detalledecuentacorriente`.`detalle`,
    `detalledecuentacorriente`.`idPedidos`,
    `cuentacorriente`.`idClientes`,
    `clientes`.`nombre`,
    `detalledecuentacorriente`.`id_caja`
  FROM
    ((`detalledecuentacorriente`
      JOIN `cuentacorriente` ON (`detalledecuentacorriente`.`idCuentaCorriente` = `cuentacorriente`.`idCuentaCorriente`))
      JOIN `clientes` ON (`cuentacorriente`.`idClientes` = `clientes`.`idClientes`));

#
# View "vista_ctactes"
#

DROP VIEW IF EXISTS `vista_ctactes`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_ctactes`
  AS
  SELECT
    `cuentacorriente`.`idClientes`,
    `clientes`.`nombre` AS 'cliente',
    `afip_tiporesponsable`.`descripcion` AS 'tiporesponsable',
    `afip_tipodoc`.`descripcion` AS 'tipodoc',
    `clientes`.`IvaDocumento`,
    `clientes`.`margen`,
    `cuentacorriente`.`estado`,
    SUM(`detalledecuentacorriente`.`debe`) AS 'debe',
    SUM(`detalledecuentacorriente`.`haber`) AS 'haber',
    SUM(`detalledecuentacorriente`.`haber` - `detalledecuentacorriente`.`debe`) AS 'saldo',
    `cuentacorriente`.`idCuentaCorriente`
  FROM
    ((((`cuentacorriente`
      JOIN `clientes` ON (`cuentacorriente`.`idClientes` = `clientes`.`idClientes`))
      JOIN `afip_tiporesponsable` ON (`clientes`.`idTiporesponsable` = `afip_tiporesponsable`.`idTiporesponsable`))
      JOIN `afip_tipodoc` ON (`clientes`.`idTipodoc` = `afip_tipodoc`.`idTipodoc`))
      JOIN `detalledecuentacorriente` ON (`detalledecuentacorriente`.`idCuentaCorriente` = `cuentacorriente`.`idCuentaCorriente`))
  GROUP BY
    `cuentacorriente`.`idClientes`, `clientes`.`nombre`, `afip_tiporesponsable`.`descripcion`, `afip_tipodoc`.`descripcion`, `clientes`.`IvaDocumento`, `clientes`.`margen`, `cuentacorriente`.`estado`;

#
# View "vista_ctactes_sinsaldo"
#

DROP VIEW IF EXISTS `vista_ctactes_sinsaldo`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_ctactes_sinsaldo`
  AS
  SELECT
    `cuentacorriente`.`idClientes`,
    `clientes`.`nombre` AS 'cliente',
    `afip_tiporesponsable`.`descripcion` AS 'tiporesponsable',
    `afip_tipodoc`.`descripcion` AS 'tipodoc',
    `clientes`.`IvaDocumento`,
    `clientes`.`margen`,
    `cuentacorriente`.`estado`,
    `cuentacorriente`.`idCuentaCorriente`
  FROM
    (((`cuentacorriente`
      JOIN `clientes` ON (`cuentacorriente`.`idClientes` = `clientes`.`idClientes`))
      JOIN `afip_tiporesponsable` ON (`clientes`.`idTiporesponsable` = `afip_tiporesponsable`.`idTiporesponsable`))
      JOIN `afip_tipodoc` ON (`clientes`.`idTipodoc` = `afip_tipodoc`.`idTipodoc`))
  GROUP BY
    `cuentacorriente`.`idClientes`, `clientes`.`nombre`, `afip_tiporesponsable`.`descripcion`, `afip_tipodoc`.`descripcion`, `clientes`.`IvaDocumento`, `clientes`.`margen`, `cuentacorriente`.`estado`;

#
# View "vista_facturacion_cliente"
#

DROP VIEW IF EXISTS `vista_facturacion_cliente`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_facturacion_cliente`
  AS
  SELECT
    `clientes`.`idClientes`,
    `clientes`.`idTiporesponsable`,
    `afip_tipo_facturacion`.`descripcion` AS 'AfipTipo',
    `clientes`.`nombre` AS 'RazonSocial',
    `clientes`.`IvaDocumento` AS 'Documento',
    `afip_tipodoc`.`wsfe` AS 'tipodocWSFE',
    `afip_tipodoc`.`hasar` AS 'tipodocHASAR',
    `afip_tiporesponsable`.`wsfe` AS 'tiporesponsableWSFE',
    `afip_tiporesponsable`.`hasar` AS 'tiporesponsableHASAR',
    `clientes`.`domicilio` AS 'Domicilio'
  FROM
    (`afip_tipo_facturacion`
      JOIN ((`clientes`
        JOIN `afip_tipodoc` ON (`clientes`.`idTipodoc` = `afip_tipodoc`.`idTipodoc`))
        JOIN `afip_tiporesponsable` ON (`clientes`.`idTiporesponsable` = `afip_tiporesponsable`.`idTiporesponsable`)))
  WHERE
    `afip_tipo_facturacion`.`tipo` = 1;

#
# View "vista_facturacion_usuario"
#

DROP VIEW IF EXISTS `vista_facturacion_usuario`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_facturacion_usuario`
  AS
  SELECT
    `afip_datos`.`idTiporesponsable`,
    `afip_tiporesponsable`.`descripcion`,
    `afip_datos`.`nombre`,
    `afip_hasar_propiedades`.`direccionip`,
    `afip_hasar_propiedades`.`idImpresoraHasar`,
    `afip_hasar_propiedades`.`transporte`,
    `afip_hasar_propiedades`.`puerto`,
    `afip_hasar_propiedades`.`baudios`
  FROM
    ((`afip_datos`
      JOIN `afip_tiporesponsable` ON (`afip_datos`.`idTiporesponsable` = `afip_tiporesponsable`.`idTiporesponsable`))
      JOIN `afip_hasar_propiedades`)
  ORDER BY `afip_hasar_propiedades`.`idImpresoraHasar` DESC;

#
# View "vista_hora_servidor"
#

DROP VIEW IF EXISTS `vista_hora_servidor`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_hora_servidor`
  AS
  SELECT CAST(CURRENT_TIMESTAMP() AS time) AS 'hora';

#
# View "vista_iva_compra"
#

DROP VIEW IF EXISTS `vista_iva_compra`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_iva_compra`
  AS
  SELECT
    `iva_compra`.`idIvaCompra`,
    `iva_compra`.`fechacbte`,
    `iva_compra`.`numerocbte`,
    `proveedores`.`razonsocial`,
    `afip_tipocbte`.`descripcion` AS 'tipocbte',
    `iva_compra`.`importeoperacion`,
    COUNT(`iva_compra_alicuotas`.`idIvaCompra`) AS 'cantidadiva'
  FROM
    (((`iva_compra`
      JOIN `proveedores` ON (`iva_compra`.`idProveedores` = `proveedores`.`idProveedores`))
      JOIN `afip_tipocbte` ON (`iva_compra`.`idTipoCbte` = `afip_tipocbte`.`idTipoCbte`))
      LEFT JOIN `iva_compra_alicuotas` ON (`iva_compra_alicuotas`.`idIvaCompra` = `iva_compra`.`idIvaCompra`))
  GROUP BY
    `iva_compra`.`idIvaCompra`, `iva_compra`.`fechacbte`, `iva_compra`.`numerocbte`, `proveedores`.`razonsocial`, `afip_tipocbte`.`descripcion`, `iva_compra`.`importeoperacion`, `iva_compra`.`cantidadiva`;

#
# View "vista_iva_compra_alicuota"
#

DROP VIEW IF EXISTS `vista_iva_compra_alicuota`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_iva_compra_alicuota`
  AS
  SELECT
    `iva_compra_alicuotas`.`idIvaCompraAlicuota`,
    `afip_tipoiva`.`descripcion` AS 'tipoiva',
    `iva_compra_alicuotas`.`importenetogravado`,
    `iva_compra_alicuotas`.`impuestoliquidado`,
    `iva_compra`.`numerocbte`,
    `proveedores`.`razonsocial`,
    `afip_tipocbte`.`descripcion` AS 'tipocbte',
    `iva_compra`.`puntodeventa`,
    `iva_compra_alicuotas`.`idIvaCompra`
  FROM
    ((((`iva_compra_alicuotas`
      JOIN `iva_compra` ON (`iva_compra_alicuotas`.`idIvaCompra` = `iva_compra`.`idIvaCompra`))
      JOIN `afip_tipoiva` ON (`iva_compra_alicuotas`.`idTipoiva` = `afip_tipoiva`.`idTipoiva`))
      JOIN `afip_tipocbte` ON (`iva_compra`.`idTipoCbte` = `afip_tipocbte`.`idTipoCbte`))
      JOIN `proveedores` ON (`iva_compra`.`idProveedores` = `proveedores`.`idProveedores`));

#
# View "vista_iva_compra_txt"
#

DROP VIEW IF EXISTS `vista_iva_compra_txt`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_iva_compra_txt`
  AS
  SELECT
    `iva_compra`.`idIvaCompra`,
    `iva_compra`.`fechacbte`,
    `afip_tipocbte`.`descripcion` AS 'tipocbte',
    `iva_compra`.`puntodeventa`,
    `iva_compra`.`numerocbte`,
    `proveedores`.`razonsocial`,
    `proveedores`.`documento`,
    SUM(`iva_compra_alicuotas`.`importenetogravado`) AS 'IVA',
    SUM(`iva_compra_alicuotas`.`impuestoliquidado`) AS 'Gravado',
    `iva_compra`.`importenointegraprecioneto` AS 'No Gravado',
    `iva_compra`.`importeoperacion` AS 'TOTAL'
  FROM
    (((`iva_compra`
      JOIN `proveedores` ON (`iva_compra`.`idProveedores` = `proveedores`.`idProveedores`))
      JOIN `afip_tipocbte` ON (`iva_compra`.`idTipoCbte` = `afip_tipocbte`.`idTipoCbte`))
      LEFT JOIN `iva_compra_alicuotas` ON (`iva_compra_alicuotas`.`idIvaCompra` = `iva_compra`.`idIvaCompra`))
  GROUP BY
    `iva_compra`.`idIvaCompra`, `iva_compra`.`fechacbte`, `iva_compra`.`numerocbte`, `proveedores`.`razonsocial`, `afip_tipocbte`.`descripcion`, `iva_compra`.`importeoperacion`, `iva_compra`.`cantidadiva`, `iva_compra`.`puntodeventa`;

#
# View "vista_orden_compra"
#

DROP VIEW IF EXISTS `vista_orden_compra`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_orden_compra`
  AS
  SELECT
    `productos`.`idProductos`,
    `productos`.`codigo`,
    `productos`.`nombre`,
    `productos`.`minimo`,
    SUM(`stock`.`cantidad`) AS 'stock',
    `deposito`.`nombre` AS 'deposito',
    MIN(`proveedores`.`razonsocial`) AS 'proveedor',
    `categorias`.`nombre` AS 'categoria',
    `marcas`.`nombre` AS 'marca',
    `productos`.`idMarcas`,
    `productos`.`idCategorias`,
    `stock`.`idTipoiva`
  FROM
    (((((`stock`
      JOIN `productos` ON (`stock`.`idProductos` = `productos`.`idProductos`))
      JOIN `deposito` ON (`stock`.`idDepositos` = `deposito`.`idDepositos`))
      JOIN `marcas` ON (`productos`.`idMarcas` = `marcas`.`idMarcas`))
      JOIN `categorias` ON (`productos`.`idCategorias` = `categorias`.`idCategorias`))
      JOIN `proveedores` ON (`stock`.`idProveedores` = `proveedores`.`idProveedores`))
  GROUP BY
    `productos`.`idProductos`, `productos`.`codigo`, `productos`.`nombre`, `stock`.`idDepositos`, `productos`.`minimo`, `deposito`.`nombre`, `categorias`.`nombre`, `marcas`.`nombre`, `productos`.`idMarcas`, `productos`.`idCategorias`, `stock`.`idTipoiva`
  HAVING
    `stock` <= `productos`.`minimo`;

#
# View "vista_pedidos"
#

DROP VIEW IF EXISTS `vista_pedidos`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_pedidos`
  AS
  SELECT
    `pedidos`.`idPedidos`,
    `pedidos`.`fecha`,
    `pedidos`.`hora`,
    `clientes`.`nombre` AS 'cliente',
    `pedidos`.`subtotal`,
    `pedidos_tienen_formasdepago`.`descuento`,
    `pedidos`.`total`,
    `formasdepago`.`nombre` AS 'formadepago',
    `pedidos`.`estado`,
    `pedidos`.`idClientes`,
    `pedidos`.`idCaja`,
    `pedidos_tienen_formasdepago`.`interes`,
    `usuarios`.`usuario`
  FROM
    ((((`pedidos`
      JOIN `pedidos_tienen_formasdepago` ON (`pedidos_tienen_formasdepago`.`idPedidos` = `pedidos`.`idPedidos`))
      JOIN `clientes` ON (`pedidos`.`idClientes` = `clientes`.`idClientes`))
      JOIN `formasdepago` ON (`pedidos_tienen_formasdepago`.`idFormasDePago` = `formasdepago`.`idFormasDePago`))
      JOIN `usuarios` ON (`usuarios`.`idUsuarios` = `pedidos`.`id_usuario`));

#
# View "vista_facturacion_timer"
#

DROP VIEW IF EXISTS `vista_facturacion_timer`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_facturacion_timer`
  AS
  SELECT
    DATE_FORMAT(STR_TO_DATE(`vista_pedidos`.`fecha`, '%d-%m-%Y'), '%Y-%m-%d') AS 'FECHA',
    `vista_pedidos`.`total` AS 'Total',
    `vista_pedidos`.`idPedidos`,
    `vista_pedidos`.`idClientes`,
    `puntosdeventas`.`idPuntosdeVentas`,
    (SELECT SUM(`vista_pedidos`.`total`) FROM (`vista_pedidos`
        JOIN `afip_datos`) WHERE `vista_pedidos`.`estado` = 'FACTURADO' AND DATE_FORMAT(STR_TO_DATE(`vista_pedidos`.`fecha`, '%d-%m-%Y'), '%Y-%m-%d') = CURDATE()) AS 'FACTURADO_HOY',
    `afip_datos`.`topeDiario` - (SELECT SUM(`vista_pedidos`.`total`) FROM (`vista_pedidos`
        JOIN `afip_datos`) WHERE `vista_pedidos`.`estado` = 'FACTURADO' AND DATE_FORMAT(STR_TO_DATE(`vista_pedidos`.`fecha`, '%d-%m-%Y'), '%Y-%m-%d') = CURDATE()) AS 'DIFERENCIA_HOY',
    `afip_datos`.`topeDiario`,
    `vista_pedidos`.`hora`
  FROM
    ((`vista_pedidos`
      JOIN `afip_datos`)
      JOIN `puntosdeventas`)
  WHERE
    `vista_pedidos`.`estado` IS NULL AND DATE_FORMAT(STR_TO_DATE(`vista_pedidos`.`fecha`, '%d-%m-%Y'), '%Y-%m-%d') = CURDATE()
  ORDER BY `vista_pedidos`.`idPedidos`;

#
# View "vista_facturacion"
#

DROP VIEW IF EXISTS `vista_facturacion`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_facturacion`
  AS
  SELECT
    `facturacion`.`idFacturacion`,
    `afip_tipocbte`.`descripcion` AS 'tipocbte',
    `facturacion`.`numeroCbte`,
    `facturacion`.`fechaCbte`,
    `puntosdeventas`.`numero` AS 'ptovta',
    `facturacion`.`totalVenta`,
    `facturacion`.`totalIVA`,
    `facturacion`.`CAE`,
    `facturacion`.`descripcion`,
    `facturacion`.`idPedidos`,
    `vista_pedidos`.`cliente`
  FROM
    (((`facturacion`
      JOIN `afip_tipocbte` ON (`facturacion`.`idTipoCbte` = `afip_tipocbte`.`idTipoCbte`))
      JOIN `puntosdeventas` ON (`facturacion`.`idPuntosdeVentas` = `puntosdeventas`.`idPuntosdeVentas`))
      JOIN `vista_pedidos` ON (`facturacion`.`idPedidos` = `vista_pedidos`.`idPedidos`))
  ORDER BY `facturacion`.`idFacturacion`;

#
# View "vista_pedidos_detalle"
#

DROP VIEW IF EXISTS `vista_pedidos_detalle`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_pedidos_detalle`
  AS
  SELECT
    `pedidos`.`idPedidos`,
    `productos`.`codigo`,
    `productos`.`idProductos`,
    `productos`.`nombre`,
    `stock`.`bonificacion`,
    `stock`.`idTipoiva`,
    `afip_tipoiva`.`descripcion` AS 'tipoiva',
    `detalledepedidos`.`cantidad`,
    `stock`.`precioVenta`,
    `pedidos`.`total`,
    `pedidos`.`fecha`,
    `pedidos`.`hora`,
    `pedidos`.`idClientes`,
    `clientes`.`nombre` AS 'cliente',
    `stock`.`idDepositos`,
    `deposito`.`nombre` AS 'deposito',
    `formasdepago`.`idFormasDePago`,
    `formasdepago`.`nombre` AS 'formadepago',
    `pedidos_tienen_formasdepago`.`descuento`,
    `pedidos_tienen_formasdepago`.`descuentobase`,
    `pedidos_tienen_formasdepago`.`descuentoporcentaje`,
    `pedidos`.`subtotal`,
    `pedidos_tienen_formasdepago`.`interes`,
    `pedidos_tienen_formasdepago`.`interesporcentaje`,
    `pedidos`.`observacion`
  FROM
    ((((((((`pedidos`
      JOIN `detalledepedidos` ON (`detalledepedidos`.`idPedidos` = `pedidos`.`idPedidos`))
      JOIN `stock` ON (`detalledepedidos`.`idStock` = `stock`.`idStock`))
      JOIN `productos` ON (`stock`.`idProductos` = `productos`.`idProductos`))
      JOIN `deposito` ON (`stock`.`idDepositos` = `deposito`.`idDepositos`))
      JOIN `afip_tipoiva` ON (`stock`.`idTipoiva` = `afip_tipoiva`.`idTipoiva`))
      JOIN `clientes` ON (`pedidos`.`idClientes` = `clientes`.`idClientes`))
      JOIN `pedidos_tienen_formasdepago` ON (`pedidos_tienen_formasdepago`.`idPedidos` = `pedidos`.`idPedidos`))
      JOIN `formasdepago` ON (`pedidos_tienen_formasdepago`.`idFormasDePago` = `formasdepago`.`idFormasDePago`));

#
# View "vista_presupuestos"
#

DROP VIEW IF EXISTS `vista_presupuestos`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_presupuestos`
  AS
  SELECT
    `presupuestos`.`idPresupuestos`,
    `presupuestos`.`fecha`,
    `presupuestos`.`hora`,
    `clientes`.`nombre` AS 'cliente',
    `presupuestos`.`subtotal`,
    `presupuestos_tienen_formasdepago`.`descuento`,
    `presupuestos`.`total`,
    `formasdepago`.`nombre` AS 'formadepago',
    `presupuestos`.`idClientes`
  FROM
    (((`presupuestos`
      JOIN `presupuestos_tienen_formasdepago` ON (`presupuestos_tienen_formasdepago`.`idPresupuestos` = `presupuestos`.`idPresupuestos`))
      JOIN `clientes` ON (`presupuestos`.`idClientes` = `clientes`.`idClientes`))
      JOIN `formasdepago` ON (`presupuestos_tienen_formasdepago`.`idFormasDePago` = `formasdepago`.`idFormasDePago`));

#
# View "vista_presupuestos_convertir"
#

DROP VIEW IF EXISTS `vista_presupuestos_convertir`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_presupuestos_convertir`
  AS
  SELECT
    `clientes`.`idClientes`,
    `clientes`.`nombre`,
    `afip_tiporesponsable`.`descripcion` AS 'tiporesponsable',
    `afip_tipodoc`.`descripcion` AS 'tipodoc',
    `clientes`.`IvaDocumento`,
    `formasdepago`.`idFormasDePago`,
    `formasdepago`.`nombre` AS 'formadepago',
    `formasdepago`.`descuento`,
    `presupuestos`.`idPresupuestos`
  FROM
    (((((`presupuestos`
      JOIN `clientes` ON (`presupuestos`.`idClientes` = `clientes`.`idClientes`))
      JOIN `presupuestos_tienen_formasdepago` ON (`presupuestos_tienen_formasdepago`.`idPresupuestos` = `presupuestos`.`idPresupuestos`))
      JOIN `formasdepago` ON (`presupuestos_tienen_formasdepago`.`idFormasDePago` = `formasdepago`.`idFormasDePago`))
      JOIN `afip_tiporesponsable` ON (`clientes`.`idTiporesponsable` = `afip_tiporesponsable`.`idTiporesponsable`))
      JOIN `afip_tipodoc` ON (`clientes`.`idTipodoc` = `afip_tipodoc`.`idTipodoc`));

#
# View "vista_presupuestos_convertir_detalle"
#

DROP VIEW IF EXISTS `vista_presupuestos_convertir_detalle`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_presupuestos_convertir_detalle`
  AS
  SELECT
    `detalledepresupuestos`.`idProductos`,
    `productos`.`codigo`,
    `productos`.`nombre` AS 'producto',
    `stock`.`precioVenta`,
    `stock`.`bonificacion`,
    `stock`.`idTipoiva`,
    `afip_tipoiva`.`descripcion` AS 'tipoiva',
    `detalledepresupuestos`.`cantidad`,
    `detalledepresupuestos`.`idPresupuestos`
  FROM
    (((`detalledepresupuestos`
      JOIN `productos` ON (`detalledepresupuestos`.`idProductos` = `productos`.`idProductos`))
      JOIN `stock` ON (`stock`.`idProductos` = `productos`.`idProductos`))
      JOIN `afip_tipoiva` ON (`stock`.`idTipoiva` = `afip_tipoiva`.`idTipoiva`))
  WHERE
    `stock`.`idStock` IN (SELECT MAX(`stock`.`idStock`) FROM `stock` GROUP BY `stock`.`idProductos`)
  ORDER BY `detalledepresupuestos`.`idProductos`;

#
# View "vista_presupuestos_detalle"
#

DROP VIEW IF EXISTS `vista_presupuestos_detalle`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_presupuestos_detalle`
  AS
  SELECT
    `presupuestos`.`idPresupuestos`,
    `detalledepresupuestos`.`idProductos`,
    `productos`.`codigo`,
    `productos`.`nombre`,
    `detalledepresupuestos`.`bonificacion`,
    `detalledepresupuestos`.`idTipoiva`,
    `afip_tipoiva`.`descripcion` AS 'tipoiva',
    `detalledepresupuestos`.`cantidad`,
    `detalledepresupuestos`.`precioVenta`,
    `presupuestos`.`total`,
    `presupuestos`.`fecha`,
    `presupuestos`.`hora`,
    `presupuestos`.`idClientes`,
    `clientes`.`nombre` AS 'cliente',
    `presupuestos_tienen_formasdepago`.`idFormasDePago`,
    `formasdepago`.`nombre` AS 'formadepago',
    `presupuestos_tienen_formasdepago`.`descuento`,
    `presupuestos_tienen_formasdepago`.`descuentobase`,
    `presupuestos_tienen_formasdepago`.`descuentoporcentaje`,
    `presupuestos`.`subtotal`
  FROM
    ((((((`presupuestos`
      JOIN `detalledepresupuestos` ON (`detalledepresupuestos`.`idPresupuestos` = `presupuestos`.`idPresupuestos`))
      JOIN `productos` ON (`detalledepresupuestos`.`idProductos` = `productos`.`idProductos`))
      JOIN `afip_tipoiva` ON (`detalledepresupuestos`.`idTipoiva` = `afip_tipoiva`.`idTipoiva`))
      JOIN `clientes` ON (`presupuestos`.`idClientes` = `clientes`.`idClientes`))
      JOIN `presupuestos_tienen_formasdepago` ON (`presupuestos_tienen_formasdepago`.`idPresupuestos` = `presupuestos`.`idPresupuestos`))
      JOIN `formasdepago` ON (`presupuestos_tienen_formasdepago`.`idFormasDePago` = `formasdepago`.`idFormasDePago`));

#
# View "vista_producto_ranking"
#

DROP VIEW IF EXISTS `vista_producto_ranking`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_producto_ranking`
  AS
  SELECT
    `productos`.`codigo`,
    `productos`.`nombre` AS 'producto',
    SUM(`detalledepedidos`.`cantidad`) AS 'cantidad',
    `productos`.`idCategorias`,
    `categorias`.`nombre` AS 'categoria',
    `productos`.`idMarcas`,
    `marcas`.`nombre` AS 'marca'
  FROM
    ((((`detalledepedidos`
      JOIN `stock` ON (`detalledepedidos`.`idStock` = `stock`.`idStock`))
      JOIN `productos` ON (`stock`.`idProductos` = `productos`.`idProductos`))
      JOIN `categorias` ON (`productos`.`idCategorias` = `categorias`.`idCategorias`))
      JOIN `marcas` ON (`productos`.`idMarcas` = `marcas`.`idMarcas`))
  GROUP BY
    `productos`.`nombre`, `productos`.`codigo`
  ORDER BY SUM(`detalledepedidos`.`cantidad`) DESC;

#
# View "vista_productos"
#

DROP VIEW IF EXISTS `vista_productos`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_productos`
  AS
  SELECT
    `productos`.`idProductos`,
    `productos`.`codigo`,
    `productos`.`nombre`,
    `productos`.`unidad`,
    `productos`.`minimo`,
    `categorias`.`nombre` AS 'categoria',
    `marcas`.`nombre` AS 'marca',
    `productos`.`descripcion`
  FROM
    ((`productos`
      JOIN `categorias` ON (`productos`.`idCategorias` = `categorias`.`idCategorias`))
      JOIN `marcas` ON (`productos`.`idMarcas` = `marcas`.`idMarcas`))
  ORDER BY `productos`.`codigo`, `productos`.`nombre`;

#
# View "vista_proveedores"
#

DROP VIEW IF EXISTS `vista_proveedores`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_proveedores`
  AS
  SELECT
    `proveedores`.`idProveedores`,
    `proveedores`.`razonsocial`,
    `afip_tipodoc`.`descripcion` AS 'tipodoc',
    `proveedores`.`documento`,
    `proveedores`.`localidad`
  FROM
    (`proveedores`
      JOIN `afip_tipodoc` ON (`proveedores`.`idTipodoc` = `afip_tipodoc`.`idTipodoc`));

#
# View "vista_stock"
#

DROP VIEW IF EXISTS `vista_stock`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_stock`
  AS
  SELECT
    `productos`.`idProductos`,
    `productos`.`codigo`,
    `productos`.`nombre`,
    SUM(`stock`.`cantidad`) AS 'cantidad',
    `deposito`.`nombre` AS 'deposito'
  FROM
    ((`stock`
      JOIN `productos` ON (`stock`.`idProductos` = `productos`.`idProductos`))
      JOIN `deposito` ON (`stock`.`idDepositos` = `deposito`.`idDepositos`))
  GROUP BY
    `productos`.`idProductos`, `productos`.`codigo`, `productos`.`nombre`, `stock`.`idDepositos`;

#
# View "vista_stockdetalle"
#

DROP VIEW IF EXISTS `vista_stockdetalle`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_stockdetalle`
  AS
  SELECT
    `stock`.`descripcion`,
    `stock`.`cantidad`,
    `stock`.`fecha`,
    `stock`.`precioCompra`,
    `stock`.`bonificacion`,
    `stock`.`precioVenta`,
    `afip_tipoiva`.`descripcion` AS 'tipoiva',
    `deposito`.`nombre` AS 'deposito',
    `productos`.`codigo`,
    `productos`.`nombre` AS 'producto',
    `stock`.`idProductos`,
    `stock`.`precioVenta2`
  FROM
    (((`stock`
      JOIN `productos` ON (`stock`.`idProductos` = `productos`.`idProductos`))
      JOIN `deposito` ON (`stock`.`idDepositos` = `deposito`.`idDepositos`))
      JOIN `afip_tipoiva` ON (`stock`.`idTipoiva` = `afip_tipoiva`.`idTipoiva`));

#
# View "vista_ultima_cantidad_stock"
#

DROP VIEW IF EXISTS `vista_ultima_cantidad_stock`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_ultima_cantidad_stock`
  AS
  SELECT
    `idProductos`,
    `idDepositos`,
    SUM(`cantidad`) AS 'cantidad_stock',
    `bonificacion`,
    `idTipoiva`
  FROM
    `stock`
  GROUP BY
    `idProductos`, `idDepositos`;

#
# View "vista_ultimo_idstock"
#

DROP VIEW IF EXISTS `vista_ultimo_idstock`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_ultimo_idstock`
  AS
  SELECT
    MAX(`b`.`idStock`) AS 'idStock', `b`.`idProductos` AS 'idproductos'
  FROM
    `stock` b
  WHERE
    `b`.`precioCompra` IS NOT NULL
  GROUP BY
    `b`.`idProductos`;

#
# View "vista_ultimo_precio"
#

DROP VIEW IF EXISTS `vista_ultimo_precio`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_ultimo_precio`
  AS
  SELECT
    `stock`.`idStock`,
    `stock`.`idProductos`,
    `stock`.`idTipoiva`,
    `stock`.`fecha`,
    `stock`.`precioCompra`,
    IF(`vista_hora_servidor`.`hora` <= '23:00:00' AND `vista_hora_servidor`.`hora` >= '07:00:00', `stock`.`precioVenta`, `stock`.`precioVenta2`) AS 'precioVenta'
  FROM
    (((`stock`
      JOIN `vista_ultimo_idstock` ON (`stock`.`idStock` = `vista_ultimo_idstock`.`idStock`))
      JOIN `productos` ON (`productos`.`idProductos` = `stock`.`idProductos`))
      JOIN `vista_hora_servidor`);

#
# View "vista_proveedores_detalle"
#

DROP VIEW IF EXISTS `vista_proveedores_detalle`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_proveedores_detalle`
  AS
  SELECT
    `productos`.`idProductos`,
    `productos`.`codigo`,
    `productos`.`nombre`,
    `productos`.`minimo`,
    SUM(`stock`.`cantidad`) AS 'cantidad',
    `deposito`.`nombre` AS 'deposito',
    `vista_ultimo_precio`.`precioCompra`,
    `vista_ultimo_precio`.`precioVenta`,
    `marcas`.`nombre` AS 'marca',
    `categorias`.`nombre` AS 'categoria',
    `proveedores`.`razonsocial` AS 'proveedor',
    `proveedores`.`idProveedores`,
    `categorias`.`idCategorias`,
    `marcas`.`idMarcas`,
    `stock`.`bonificacion`,
    `stock`.`idDepositos`,
    `stock`.`idTipoiva`
  FROM
    ((((((`stock`
      JOIN `productos` ON (`stock`.`idProductos` = `productos`.`idProductos`))
      JOIN `deposito` ON (`stock`.`idDepositos` = `deposito`.`idDepositos`))
      JOIN `marcas` ON (`productos`.`idMarcas` = `marcas`.`idMarcas`))
      JOIN `categorias` ON (`productos`.`idCategorias` = `categorias`.`idCategorias`))
      JOIN `proveedores` ON (`stock`.`idProveedores` = `proveedores`.`idProveedores`))
      JOIN `vista_ultimo_precio` ON (`stock`.`idProductos` = `vista_ultimo_precio`.`idProductos`))
  GROUP BY
    `productos`.`idProductos`, `productos`.`codigo`, `productos`.`nombre`, `stock`.`idDepositos`;

#
# View "vista_productos_busqueda"
#

DROP VIEW IF EXISTS `vista_productos_busqueda`;
CREATE
  ALGORITHM = UNDEFINED
  VIEW `vista_productos_busqueda`
  AS
  SELECT
    `stock`.`idProductos`,
    `productos`.`codigo`,
    `productos`.`nombre`,
    `vista_ultimo_precio`.`precioVenta`,
    `stock`.`bonificacion`,
    `stock`.`idTipoiva`,
    `afip_tipoiva`.`descripcion` AS 'tipoiva',
    `deposito`.`nombre` AS 'deposito',
    `deposito`.`idDepositos`,
    `vista_ultima_cantidad_stock`.`cantidad_stock` AS 'stock',
    `marcas`.`nombre` AS 'marca',
    `categorias`.`nombre` AS 'categoria',
    `stock`.`fecha`,
    `stock`.`idStock`,
    `productos`.`descripcion`,
    `vista_ultimo_precio`.`precioCompra`
  FROM
    ((((((((`stock`
      JOIN `vista_ultimo_idstock` ON (`stock`.`idStock` = `vista_ultimo_idstock`.`idStock`))
      JOIN `productos` ON (`productos`.`idProductos` = `vista_ultimo_idstock`.`idproductos`))
      JOIN `marcas` ON (`productos`.`idMarcas` = `marcas`.`idMarcas`))
      JOIN `categorias` ON (`productos`.`idCategorias` = `categorias`.`idCategorias`))
      JOIN `vista_ultima_cantidad_stock` ON (`vista_ultima_cantidad_stock`.`idProductos` = `vista_ultimo_idstock`.`idproductos`))
      JOIN `deposito` ON (`deposito`.`idDepositos` = `vista_ultima_cantidad_stock`.`idDepositos`))
      JOIN `afip_tipoiva` ON (`stock`.`idTipoiva` = `afip_tipoiva`.`idTipoiva`))
      JOIN `vista_ultimo_precio` ON (`vista_ultimo_precio`.`idProductos` = `vista_ultimo_idstock`.`idproductos`))
  ORDER BY `productos`.`nombre`, `marcas`.`nombre`;
