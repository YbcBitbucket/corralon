package Formularios;

import Clases.ConexionMySQL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Login extends javax.swing.JFrame {

    public static int id_usuario;
    public static String apellido;
    public static String nombre;
    public static String usuario;
    public static String contraseña;
    public static String acceso_datos;
    public static String acceso_ventas;
    public static String acceso_consultas;  
    public static String acceso_afip;

    public Login() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        txtusuarios.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btncancelar = new javax.swing.JButton();
        btnaceptar = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtusuarios = new javax.swing.JTextField();
        txtcontraseña = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btncancelar.setMnemonic('c');
        btncancelar.setText("Cancelar");
        btncancelar.setNextFocusableComponent(txtusuarios);
        btncancelar.setPreferredSize(new java.awt.Dimension(120, 40));
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        btnaceptar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnaceptar.setMnemonic('a');
        btnaceptar.setText("Aceptar");
        btnaceptar.setNextFocusableComponent(btncancelar);
        btnaceptar.setPreferredSize(new java.awt.Dimension(120, 40));
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });
        btnaceptar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btnaceptarKeyPressed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Usuarios", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N
        jPanel1.setForeground(new java.awt.Color(153, 153, 153));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Key.png"))); // NOI18N
        jLabel2.setText("Contraseña:");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Caracter.png"))); // NOI18N
        jLabel1.setText("Usuario:");

        txtusuarios.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtusuarios.setNextFocusableComponent(txtcontraseña);
        txtusuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtusuariosActionPerformed(evt);
            }
        });

        txtcontraseña.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtcontraseña.setNextFocusableComponent(btnaceptar);
        txtcontraseña.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcontraseñaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtcontraseña)
                    .addComponent(txtusuarios))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtusuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtcontraseña))
                .addGap(82, 82, 82))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 84, Short.MAX_VALUE)
                        .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncancelarActionPerformed

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        int i = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectUsuarios = null;

        String sSQL = "SELECT * FROM usuarios";
        try {
            SelectUsuarios = cn.createStatement();
            ResultSet rs = SelectUsuarios.executeQuery(sSQL);
            // Recorro y me fijo donde coinciden usuario y contraseña
            while (rs.next()) {
                if (txtusuarios.getText().equals(rs.getString("usuario"))) {
                    if (txtcontraseña.getText().equals(rs.getString("contraseña"))) {
                        this.dispose();
                        i = 0;
                        // Cargo las variables para usarlos en el Main
                        id_usuario = rs.getInt("idUsuarios");
                        apellido = rs.getString("apellido");
                        nombre = rs.getString("nombre");
                        usuario = rs.getString("usuario");
                        contraseña = rs.getString("contraseña");
                        //////
                        acceso_datos = rs.getString("acceso_datos");
                        acceso_ventas = rs.getString("acceso_ventas");
                        acceso_consultas = rs.getString("acceso_consultas");
                        acceso_afip = rs.getString("acceso_afip");
                        /////
                        new Main().setVisible(true);
                        break;
                    } else {
                        i = 2;
                    }
                } else {
                    i = 1;
                }
            }
            if (i == 1) {
                JOptionPane.showMessageDialog(null, "Nombre de Usuario incorrecto...");
            }
            if (i == 2) {
                JOptionPane.showMessageDialog(null, "Contraseña incorrecta...");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (SelectUsuarios != null) {
                    SelectUsuarios.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void btnaceptarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btnaceptarKeyPressed
       int i = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectUsuarios = null;

        String sSQL = "SELECT * FROM usuarios";
        try {
            SelectUsuarios = cn.createStatement();
            ResultSet rs = SelectUsuarios.executeQuery(sSQL);
            // Recorro y me fijo donde coinciden usuario y contraseña
            while (rs.next()) {
                if (txtusuarios.getText().equals(rs.getString("usuario"))) {
                    if (txtcontraseña.getText().equals(rs.getString("contraseña"))) {
                        this.dispose();
                        i = 0;
                        // Cargo las variables para usarlos en el Main
                        id_usuario = rs.getInt("idUsuarios");
                        apellido = rs.getString("apellido");
                        nombre = rs.getString("nombre");
                        usuario = rs.getString("usuario");
                        contraseña = rs.getString("contraseña");
                        //////
                        acceso_datos = rs.getString("acceso_datos");
                        acceso_ventas = rs.getString("acceso_ventas");
                        acceso_consultas = rs.getString("acceso_consultas");
                        acceso_afip = rs.getString("acceso_afip");
                        /////
                        new Main().setVisible(true);
                        break;
                    } else {
                        i = 2;
                    }
                } else {
                    i = 1;
                }
            }
            if (i == 1) {
                JOptionPane.showMessageDialog(null, "Nombre de Usuario incorrecto...");
            }
            if (i == 2) {
                JOptionPane.showMessageDialog(null, "Contraseña incorrecta...");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (SelectUsuarios != null) {
                    SelectUsuarios.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }//GEN-LAST:event_btnaceptarKeyPressed

    private void txtusuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtusuariosActionPerformed
        txtusuarios.transferFocus();
    }//GEN-LAST:event_txtusuariosActionPerformed

    private void txtcontraseñaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcontraseñaActionPerformed
        int i = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectUsuarios = null;

        String sSQL = "SELECT * FROM usuarios ";
        try {
            SelectUsuarios = cn.createStatement();
            ResultSet rs = SelectUsuarios.executeQuery(sSQL);
            // Recorro y me fijo donde coinciden usuario y contraseña
            while (rs.next()) {
                if (txtusuarios.getText().equals(rs.getString("usuario"))) {
                    if (txtcontraseña.getText().equals(rs.getString("contraseña"))) {
                        this.dispose();
                        i = 0;
                        // Cargo las variables para usarlos en el Main
                        id_usuario = rs.getInt("idUsuarios");
                        apellido = rs.getString("apellido");
                        nombre = rs.getString("nombre");
                        usuario = rs.getString("usuario");
                        contraseña = rs.getString("contraseña");
                        //////
                        acceso_datos = rs.getString("acceso_datos");
                        acceso_ventas = rs.getString("acceso_ventas");
                        acceso_consultas = rs.getString("acceso_consultas");
                        acceso_afip = rs.getString("acceso_afip");
                        /////
                        new Main().setVisible(true);
                        break;
                    } else {
                        i = 2;
                    }
                } else {
                    i = 1;
                }
            }
            if (i == 1) {
                JOptionPane.showMessageDialog(null, "Nombre de Usuario incorrecto...");
            }
            if (i == 2) {
                JOptionPane.showMessageDialog(null, "Contraseña incorrecta...");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (SelectUsuarios != null) {
                    SelectUsuarios.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }//GEN-LAST:event_txtcontraseñaActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPasswordField txtcontraseña;
    private javax.swing.JTextField txtusuarios;
    // End of variables declaration//GEN-END:variables
}
