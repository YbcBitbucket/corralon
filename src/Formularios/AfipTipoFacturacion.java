package Formularios;

import Clases.ClaseFacturacionTimer;
import Clases.ConexionMySQL;
import Clases.cboResponsableIVA;
import Clases.cboTipoAfip;
import Clases.cboTipoDoc;
import Clases.fnCargarFecha;
import Clases.fnesNumerico;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;

public class AfipTipoFacturacion extends javax.swing.JDialog {

    public static String descripcion = "";
    int idTipoResponsable = 0;

    public AfipTipoFacturacion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargartipofacturacion();
        cargardatos();
    }

    ///// CARGAR CBO IVA RESPONSABLE /////
    void cargartipofacturacion() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipofacturacion.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement TipoAfip = null;
        Statement tipo = null;
        String sSQL = "SELECT * FROM afip_tipo_facturacion";
        String sSQL2 = "SELECT idAfipTipo, descripcion, estado FROM afip_tipo_facturacion WHERE tipo=" + 1;
        try {
            TipoAfip = cn.createStatement();
            ResultSet rs = TipoAfip.executeQuery(sSQL);
            tipo = cn.createStatement();
            ResultSet rs2 = tipo.executeQuery(sSQL2);
            rs2.next();
            int index = 0;
            int i = 0;
            while (rs.next()) {
                if (rs2.getInt(1) == (rs.getInt("idAfipTipo"))) {

                    index = i;

                    //Pregunto si es WSFE y si esta en Homologacion
                    if (rs2.getString(2).equals("WSFE")) {
                        cboestadofacturacion.enable(true);
                        if (rs2.getString(3).equals("HOMOLOGACION")) {
                            cboestadofacturacion.setSelectedIndex(0);
                        } else {
                            cboestadofacturacion.setSelectedIndex(1);
                        }
                    } else {
                        cboestadofacturacion.enable(false);
                    }

                }
                value.addElement(new cboTipoAfip(rs.getInt("idAfipTipo"), rs.getString("descripcion")));
                i++;
            }

            cbotipofacturacion.setSelectedIndex(index);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (TipoAfip != null) {
                    TipoAfip.close();
                }
                if (tipo != null) {
                    tipo.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ///// CARGAR CBO IVA RESPONSABLE /////
    void cargartiporesponsable() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboresponsable.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement Tiporespansable = null;
        Statement idTiporesponsable = null;
        String sSQL = "SELECT * FROM afip_tiporesponsable";
        String sSQL2 = "SELECT idTiporesponsable FROM afip_datos WHERE idDatos=" + 1;
        try {
            Tiporespansable = cn.createStatement();
            ResultSet rs = Tiporespansable.executeQuery(sSQL);
            idTiporesponsable = cn.createStatement();
            ResultSet rs2 = idTiporesponsable.executeQuery(sSQL2);
            rs2.next();
            int index = 0;
            int i = 0;
            while (rs.next()) {
                if (rs2.getInt(1) == (rs.getInt("idTiporesponsable"))) {
                    idTipoResponsable = rs2.getInt(1);
                    index = i;
                }
                value.addElement(new cboResponsableIVA(rs.getInt("idTiporesponsable"), rs.getString("descripcion")));
                i++;
            }
            cboresponsable.setSelectedIndex(index);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (Tiporespansable != null) {
                    Tiporespansable.close();
                }
                if (idTiporesponsable != null) {
                    idTiporesponsable.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ///// CARGAR CBO TIPO DOC /////
    void cargartipodoc() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipodoc.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement TipoDoc = null;
        Statement idTipodoc = null;
        String sSQL = "SELECT * FROM afip_tipodoc";
        String sSQL2 = "SELECT idTipodoc FROM afip_datos WHERE idDatos=" + 1;
        try {
            TipoDoc = cn.createStatement();
            ResultSet rs = TipoDoc.executeQuery(sSQL);
            idTipodoc = cn.createStatement();
            ResultSet rs2 = idTipodoc.executeQuery(sSQL2);
            rs2.next();
            int index = 0;
            int i = 0;
            while (rs.next()) {
                if (rs2.getInt(1) == (rs.getInt("idTipodoc"))) {
                    index = i;
                }
                value.addElement(new cboTipoDoc(rs.getInt("idTipodoc"), rs.getString("descripcion")));
                i++;
            }
            cbotipodoc.setSelectedIndex(index);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (TipoDoc != null) {
                    TipoDoc.close();
                }
                if (idTipodoc != null) {
                    idTipodoc.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION CARGAR DATOS //////////////////////
    void cargardatos() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectClientes = null;

        String sSQL = "SELECT * FROM afip_datos WHERE idDatos=" + 1;
        try {
            SelectClientes = cn.createStatement();
            ResultSet rs = SelectClientes.executeQuery(sSQL);
            rs.next();
            cargartiporesponsable();
            cargartipodoc();
            txtdocumento.setText(rs.getString(4));
            txtnombre.setText(rs.getString(5));
            txttope.setText(rs.getString(6));
            txtminimo.setText(rs.getString(7));
            txtmaximo.setText(rs.getString(8));
            cboauto.setSelectedIndex(rs.getInt(9));
            txtZhora.setText(rs.getString(10));
            txtZminuto.setText(rs.getString(11));

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectClientes != null) {
                    SelectClientes.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        cbotipofacturacion = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        cboestadofacturacion = new javax.swing.JComboBox<>();
        btnModificar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtdocumento = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        cboresponsable = new javax.swing.JComboBox();
        cbotipodoc = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jPanel5 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        txtminimo = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        cboauto = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        txttope = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtmaximo = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txtZhora = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        txtZminuto = new javax.swing.JTextField();

        jLabel4.setText("jLabel4");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Tipo de Facturacion");

        cbotipofacturacion.setMaximumRowCount(10);
        cbotipofacturacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotipofacturacionActionPerformed(evt);
            }
        });

        jLabel3.setText("Estado");

        cboestadofacturacion.setMaximumRowCount(2);
        cboestadofacturacion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "HOMOLOGACION", "PRODUCCION" }));
        cboestadofacturacion.setEnabled(false);

        btnModificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Modificar.png"))); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnModificar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnModificar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnSalir.setMnemonic('s');
        btnSalir.setText("Salir");
        btnSalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnSalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnSalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Afip", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel10.setText("Tipo Doc");

        jLabel17.setText("Resposable IVA");

        txtdocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdocumentoKeyReleased(evt);
            }
        });

        jLabel19.setText("Documento");

        jLabel2.setText("Razón Social / Nombre");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel17)
                    .addComponent(jLabel10)
                    .addComponent(jLabel19))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cboresponsable, 0, 250, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtdocumento, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbotipodoc, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtnombre)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(cboresponsable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cbotipodoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtdocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19)))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Automatizacion", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel18.setText("Auto:");

        txtminimo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtminimoKeyReleased(evt);
            }
        });

        jLabel20.setText("Minimo (minutos):");

        cboauto.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "NO", "SI" }));

        jLabel5.setText("Tope Diario:");

        txttope.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttopeKeyReleased(evt);
            }
        });

        jLabel21.setText("Maximo (minutos):");

        txtmaximo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmaximoKeyReleased(evt);
            }
        });

        jLabel22.setText("Z Cierre (Hora):");

        txtZhora.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtZhoraKeyReleased(evt);
            }
        });

        jLabel23.setText("Z Cierre (Minutos):");

        txtZminuto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtZminutoKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel18))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboauto, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txttope, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtminimo, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel21)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtmaximo, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addGap(12, 12, 12)
                        .addComponent(txtZhora, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtZminuto, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtZminuto, txtmaximo});

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtZhora, txtminimo});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txttope, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(cboauto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20)
                    .addComponent(txtminimo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(txtmaximo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel22)
                    .addComponent(txtZhora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23)
                    .addComponent(txtZminuto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cboestadofacturacion, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbotipofacturacion, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cbotipofacturacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cboestadofacturacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbotipofacturacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotipofacturacionActionPerformed
        cboTipoAfip tipo = (cboTipoAfip) cbotipofacturacion.getSelectedItem();
        descripcion = tipo.getdescripcion();

        if (descripcion.equals("WSFE")) {
            cboestadofacturacion.enable(true);
        } else {
            cboestadofacturacion.enable(false);
        }

    }//GEN-LAST:event_cbotipofacturacionActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        cboTipoAfip tipo = (cboTipoAfip) cbotipofacturacion.getSelectedItem();
        int idAfipTipo = tipo.getidTipoAfip();
        descripcion = tipo.getdescripcion();
        String estado = cboestadofacturacion.getSelectedItem().toString();

        //Traigo Tipo Responsable y tipo Doc
        cboResponsableIVA resp = (cboResponsableIVA) cboresponsable.getSelectedItem();
        int idResponsableIVA = resp.getidTiporesponsable();

        cboTipoDoc doc = (cboTipoDoc) cbotipodoc.getSelectedItem();
        int idTipodoc = doc.getidTipodoc();

        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement TodoCeroAfipTipo = null;
        PreparedStatement ModificarAfipTipo = null;

        PreparedStatement ModificarDatos = null;
        Statement Select = null;

        String DatosSQL = "UPDATE afip_datos SET"
                + " idTiporesponsable=?, idTipodoc=?, IvaDocumento=?,"
                + " nombre=?, topeDiario=?, minimo=?, maximo=?, horaZ=?, minutoZ=?, automatizacion=? "
                + " WHERE idDatos=" + 1;

        try {
            String SQL = "UPDATE afip_tipo_facturacion SET tipo = " + 0 + " WHERE tipo = " + 1;
            TodoCeroAfipTipo = cn.prepareStatement(SQL);
            TodoCeroAfipTipo.executeUpdate();

            String sSQL = "UPDATE afip_tipo_facturacion SET tipo=?, estado=? WHERE idAfipTipo=" + idAfipTipo;
            ModificarAfipTipo = cn.prepareStatement(sSQL);
            if (descripcion.equals("WSFE")) {
                ModificarAfipTipo.setInt(1, 1);
                ModificarAfipTipo.setString(2, estado);
            } else {
                ModificarAfipTipo.setInt(1, 1);
                ModificarAfipTipo.setString(2, null);
            }

            ModificarDatos = cn.prepareStatement(DatosSQL);
            ModificarDatos.setInt(1, idResponsableIVA);
            ModificarDatos.setInt(2, idTipodoc);
            ModificarDatos.setString(3, txtdocumento.getText());
            ModificarDatos.setString(4, txtnombre.getText());
            ModificarDatos.setDouble(5, Double.valueOf(txttope.getText()));
            ModificarDatos.setInt(6, Integer.valueOf(txtminimo.getText()));
            ModificarDatos.setInt(7, Integer.valueOf(txtmaximo.getText()));
            ModificarDatos.setInt(8, Integer.valueOf(txtZhora.getText()));
            ModificarDatos.setInt(9, Integer.valueOf(txtZminuto.getText()));
            ModificarDatos.setInt(10, cboauto.getSelectedIndex());

            ModificarDatos.executeUpdate();
            ModificarAfipTipo.executeUpdate();

            if (idTipoResponsable != idResponsableIVA) {
                String sql = "SELECT * FROM vista_ultimo_idstock INNER JOIN stock ON vista_ultimo_idstock.idStock = stock.idStock";
                Select = cn.createStatement();
                ResultSet rs = Select.executeQuery(sql);
                fnCargarFecha fecha = new fnCargarFecha();
                String fechastock = fecha.cargarfecha();
                while (rs.next()) {
                    //Inserto cada producto con su ultimo stock
                    PreparedStatement ActualizacionStock = null;
                    String ssSQL = "INSERT INTO stock (idProductos, precioCompra,precioVenta, "
                            + "bonificacion, cantidad, fecha, idDepositos, idTipoiva, descripcion, idProveedores, precioVenta2) "
                            + "VALUES (?,?,?,?,?,?,?,?,?,?,?)";
                    if (idResponsableIVA == 1) {
                        ActualizacionStock = cn.prepareStatement(ssSQL);
                        ActualizacionStock.setInt(1, rs.getInt("idProductos"));
                        ActualizacionStock.setString(2, rs.getString("precioCompra"));
                        ActualizacionStock.setString(3, rs.getString("precioVenta"));
                        ActualizacionStock.setString(4, rs.getString("bonificacion"));
                        //Cantidad 0
                        ActualizacionStock.setInt(5, 0);
                        ActualizacionStock.setString(6, fechastock);
                        ActualizacionStock.setInt(7, rs.getInt("idDepositos"));
                        //idTipoIva=5
                        ActualizacionStock.setInt(8, 5);
                        ActualizacionStock.setString(9, "CAMBIO A RESPONSABLE INSCRIPTO");
                        ActualizacionStock.setInt(10, rs.getInt("idProveedores"));
                        ActualizacionStock.setString(11, rs.getString("precioVenta2"));
                    } else {
                        ActualizacionStock = cn.prepareStatement(ssSQL);
                        ActualizacionStock.setInt(1, rs.getInt("idProductos"));
                        ActualizacionStock.setString(2, rs.getString("precioCompra"));
                        ActualizacionStock.setString(3, rs.getString("precioVenta"));
                        ActualizacionStock.setString(4, rs.getString("bonificacion"));
                        //Cantidad 0
                        ActualizacionStock.setInt(5, 0);
                        ActualizacionStock.setString(6, fechastock);
                        ActualizacionStock.setInt(7, rs.getInt("idDepositos"));
                        //idTipoIva=3
                        ActualizacionStock.setInt(8, 3);
                        ActualizacionStock.setString(9, "CAMBIO A EXENTO");
                        ActualizacionStock.setInt(10, rs.getInt("idProveedores"));
                        ActualizacionStock.setString(11, rs.getString("precioVenta2"));
                    }

                    int n = ActualizacionStock.executeUpdate();
                }
            }
            
            ClaseFacturacionTimer Timer = new ClaseFacturacionTimer();
            if (cboauto.getSelectedIndex() == 1) {
                Timer.ClaseFacturacionTimer(Integer.valueOf(txtminimo.getText()) * 60000, Integer.valueOf(txtmaximo.getText()) * 60000);
                Timer.FacturarZ(Integer.valueOf(txtZhora.getText()), Integer.valueOf(txtZminuto.getText()));
            } else {
                JOptionPane.showMessageDialog(null, "dfsdf");
                Timer.stopTimer();
            }
            
            JOptionPane.showMessageDialog(null, "Los Datos se modificaron exitosamente...");

        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (TodoCeroAfipTipo != null) {
                    TodoCeroAfipTipo.close();
                }
                if (ModificarAfipTipo != null) {
                    ModificarAfipTipo.close();
                }
                if (cn != null) {
                    cn.close();
                }
                if (Select != null) {
                    Select.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        this.dispose();

    }//GEN-LAST:event_btnModificarActionPerformed

    private void txtdocumentoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdocumentoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtdocumento.getText())) {
            txtdocumento.setText("");
        }
    }//GEN-LAST:event_txtdocumentoKeyReleased

    private void txtminimoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtminimoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtminimo.getText())) {
            txtminimo.setText("");
        }
    }//GEN-LAST:event_txtminimoKeyReleased

    private void txtmaximoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmaximoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtmaximo.getText())) {
            txtmaximo.setText("");
        }
    }//GEN-LAST:event_txtmaximoKeyReleased

    private void txtZhoraKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtZhoraKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtZhora.getText())) {
            txtZhora.setText("");
        }
    }//GEN-LAST:event_txtZhoraKeyReleased

    private void txtZminutoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtZminutoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtZminuto.getText())) {
            txtZminuto.setText("");
        }
    }//GEN-LAST:event_txtZminutoKeyReleased

    private void txttopeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttopeKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txttope.getText())) {
            txttope.setText("");
        }
    }//GEN-LAST:event_txttopeKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox cboauto;
    private javax.swing.JComboBox<String> cboestadofacturacion;
    private javax.swing.JComboBox cboresponsable;
    private javax.swing.JComboBox cbotipodoc;
    private javax.swing.JComboBox<String> cbotipofacturacion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JTextField txtZhora;
    private javax.swing.JTextField txtZminuto;
    private javax.swing.JTextField txtdocumento;
    private javax.swing.JTextField txtmaximo;
    private javax.swing.JTextField txtminimo;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextField txttope;
    // End of variables declaration//GEN-END:variables
}
