package Formularios;

import Clases.ClaseProveedores;
import Clases.ConexionMySQL;
import Clases.cboTipoDoc;
import Clases.fnesNumerico;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.*;

public class ProveedoresModifica extends javax.swing.JDialog {

    public ProveedoresModifica(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargardatos(Proveedores.id_proveedor);
    }

    ///// CARGAR CBO TIPO DOC /////
    void cargartipodoc() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipodoc.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement TipoDoc = null;
        Statement idTipodoc = null;
        String sSQL = "SELECT * FROM afip_tipodoc";
        String sSQL2 = "SELECT idTipodoc FROM proveedores WHERE idProveedores=" + Proveedores.id_proveedor;
        try {
            TipoDoc = cn.createStatement();
            ResultSet rs = TipoDoc.executeQuery(sSQL);
            idTipodoc = cn.createStatement();
            ResultSet rs2 = idTipodoc.executeQuery(sSQL2);
            rs2.next();
            int index = 0;
            int i = 0;
            while (rs.next()) {
                if (rs2.getInt(1) == (rs.getInt("idTipodoc"))) {
                    index = i;
                }
                value.addElement(new cboTipoDoc(rs.getInt("idTipodoc"), rs.getString("descripcion")));
                i++;
            }
            cbotipodoc.setSelectedIndex(index);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (TipoDoc != null) {
                    TipoDoc.close();
                }
                if (idTipodoc != null) {
                    idTipodoc.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    
  //////////////////////FUNCION CARGAR DATOS //////////////////////
    void cargardatos(int id) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectProveedores = null;
        String sSQL = "SELECT * FROM proveedores WHERE idProveedores=" + id;
        try {
            SelectProveedores = cn.createStatement();
            ResultSet rs = SelectProveedores.executeQuery(sSQL);
            while (rs.next()) {
                txtapellidoynombre.setText(rs.getString("razonsocial"));
                cargartipodoc();
                txtdocumento.setText(rs.getString("documento"));
                txtdomicilio.setText(rs.getString("direccion"));
                txtlocalidad.setText(rs.getString("localidad"));
                txtcp.setText(rs.getString("codigopostal"));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProveedores != null) {
                    SelectProveedores.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnmodificar = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        txtdomicilio = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtlocalidad = new javax.swing.JTextField();
        txtcp = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        txtdocumento = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        cbotipodoc = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        txtapellidoynombre = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Proveedor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        btnmodificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Modificar.png"))); // NOI18N
        btnmodificar.setText("Modificar");
        btnmodificar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnmodificar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnmodificar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarActionPerformed(evt);
            }
        });

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Cancelar.png"))); // NOI18N
        btncancelar.setText("Cancelar");
        btncancelar.setMaximumSize(new java.awt.Dimension(120, 50));
        btncancelar.setMinimumSize(new java.awt.Dimension(120, 50));
        btncancelar.setPreferredSize(new java.awt.Dimension(120, 50));
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        txtdomicilio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdomicilioKeyReleased(evt);
            }
        });

        jLabel3.setText("Domicilio");

        jLabel11.setText("Localidad");

        txtlocalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlocalidadKeyReleased(evt);
            }
        });

        txtcp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcpKeyReleased(evt);
            }
        });

        jLabel14.setText("Codigo Postal");

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Afip", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel10.setText("Tipo Doc");

        txtdocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdocumentoKeyReleased(evt);
            }
        });

        jLabel19.setText("Documento");

        jLabel13.setText("Razón Social / Nombre");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10)
                    .addComponent(jLabel19)
                    .addComponent(jLabel13))
                .addGap(14, 14, 14)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtdocumento, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbotipodoc, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtapellidoynombre))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cbotipodoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtdocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtapellidoynombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 126, Short.MAX_VALUE)
                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel14)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtlocalidad, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                                .addComponent(txtdomicilio, javax.swing.GroupLayout.Alignment.TRAILING))
                            .addComponent(txtcp, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtdomicilio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtcp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarActionPerformed
        //Chequea q no haya datos vacios
        if (txtapellidoynombre.getText().equals("") || txtdocumento.getText().equals("") || txtdomicilio.getText().equals("") || txtlocalidad.getText().equals("") || txtcp.getText().equals("")) {
            ///Ingresar todos los datos 
            JOptionPane.showMessageDialog(null, "No ingreso todos los datos del proveedor");
        } else {
            cboTipoDoc doc = (cboTipoDoc) cbotipodoc.getSelectedItem();
            int idTipodoc = doc.getidTipodoc();
            
            ClaseProveedores proveedor = new ClaseProveedores();
            proveedor.ModificarProveedor(Proveedores.id_proveedor, idTipodoc, txtdocumento.getText(), txtapellidoynombre.getText(), txtlocalidad.getText(), txtdomicilio.getText(), Integer.valueOf(txtcp.getText()));
        }
    }//GEN-LAST:event_btnmodificarActionPerformed

    private void txtdomicilioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdomicilioKeyReleased
        String texto = txtdomicilio.getText().toUpperCase();
        txtdomicilio.setText(texto);
    }//GEN-LAST:event_txtdomicilioKeyReleased

    private void txtlocalidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlocalidadKeyReleased
        String texto = txtlocalidad.getText().toUpperCase();
        txtlocalidad.setText(texto);
    }//GEN-LAST:event_txtlocalidadKeyReleased

    private void txtcpKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcpKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtcp.getText())) {
            txtcp.setText("");
        }
    }//GEN-LAST:event_txtcpKeyReleased

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        cargardatos(Proveedores.id_proveedor);
        btnmodificar.transferFocus();
    }//GEN-LAST:event_btncancelarActionPerformed

    private void txtdocumentoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdocumentoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtdocumento.getText())) {
            txtdocumento.setText("");
        }
    }//GEN-LAST:event_txtdocumentoKeyReleased
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btncancelar;
    private javax.swing.JButton btnmodificar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JComboBox cbotipodoc;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JTextField txtapellidoynombre;
    private javax.swing.JTextField txtcp;
    private javax.swing.JTextField txtdocumento;
    private javax.swing.JTextField txtdomicilio;
    private javax.swing.JTextField txtlocalidad;
    // End of variables declaration//GEN-END:variables

}
