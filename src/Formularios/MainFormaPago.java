package Formularios;

import Clases.fnRedondear;
import Clases.fnesNumerico;
import javax.swing.JOptionPane;

public class MainFormaPago extends javax.swing.JDialog {

    public static double contado = 0;
    public static double ctacte = 0;
    public static double credito = 0;
    public static double debito = 0;
    public static double cheque = 0;
    public static double otros = 0;
    public static double saldo = 0;

    public MainFormaPago(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        txttotal.setText(Main.subtotal);
    }

    void cargarsaldo() {
        fnRedondear redondear = new fnRedondear();
        double contado = 0;
        double ctacte = 0;
        double credito = 0;
        double debito = 0;
        double cheque = 0;
        double otros = 0;
        if (!txtcontado.getText().equals("")) {
            contado = Double.valueOf(txtcontado.getText());
        }
        if (!txtctacte.getText().equals("")) {
            ctacte = Double.valueOf(txtctacte.getText());
        }
        if (!txttarjetacredito.getText().equals("")) {
            credito = Double.valueOf(txttarjetacredito.getText());
        }
        if (!txttarjetadebito.getText().equals("")) {
            debito = Double.valueOf(txttarjetadebito.getText());
        }
        if (!txtcheque.getText().equals("")) {
            cheque = Double.valueOf(txtcheque.getText());
        }
        if (!txtotros.getText().equals("")) {
            otros = Double.valueOf(txtotros.getText());
        }
        double saldo = (Double.valueOf(txttotal.getText()) - contado - ctacte - credito - debito - cheque - otros);
        txtsaldo.setText(String.valueOf(redondear.dosDigitos(saldo)));
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txttotal = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtcontado = new javax.swing.JTextField();
        txtctacte = new javax.swing.JTextField();
        txttarjetacredito = new javax.swing.JTextField();
        txttarjetadebito = new javax.swing.JTextField();
        txtcheque = new javax.swing.JTextField();
        txtotros = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtsaldo = new javax.swing.JTextField();
        jComboBox1 = new javax.swing.JComboBox<>();
        jComboBox2 = new javax.swing.JComboBox<>();
        btnaceptar = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Formas de Pago", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Total:");

        txttotal.setEditable(false);
        txttotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txttotal.setForeground(new java.awt.Color(255, 0, 0));
        txttotal.setBorder(null);
        txttotal.setOpaque(false);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 0, 0));
        jLabel4.setText("$");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Contado:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Cuenta Corriente:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Tarjeta de Credito:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("Tarjeta de Debito:");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setText("Cheque:");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setText("Otros:");

        txtcontado.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtcontado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcontadoActionPerformed(evt);
            }
        });
        txtcontado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtcontadoKeyReleased(evt);
            }
        });

        txtctacte.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtctacte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtctacteActionPerformed(evt);
            }
        });
        txtctacte.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtctacteKeyReleased(evt);
            }
        });

        txttarjetacredito.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txttarjetacredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttarjetacreditoActionPerformed(evt);
            }
        });
        txttarjetacredito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttarjetacreditoKeyReleased(evt);
            }
        });

        txttarjetadebito.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txttarjetadebito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txttarjetadebitoActionPerformed(evt);
            }
        });
        txttarjetadebito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttarjetadebitoKeyReleased(evt);
            }
        });

        txtcheque.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtcheque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtchequeActionPerformed(evt);
            }
        });
        txtcheque.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtchequeKeyReleased(evt);
            }
        });

        txtotros.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtotros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtotrosActionPerformed(evt);
            }
        });
        txtotros.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotrosKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel2.setText("Saldo:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 30)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 0, 0));
        jLabel5.setText("$");

        txtsaldo.setEditable(false);
        txtsaldo.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtsaldo.setForeground(new java.awt.Color(255, 0, 0));
        txtsaldo.setBorder(null);
        txtsaldo.setOpaque(false);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5" }));

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5" }));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtcontado, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtctacte, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txttarjetacredito, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txttarjetadebito, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txtcheque, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtotros, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtsaldo, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(48, 48, 48))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4))
                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtcontado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txtctacte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txttarjetacredito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txttarjetadebito, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtcheque, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtotros, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 13, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel5))
                    .addComponent(txtsaldo, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btnaceptar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnaceptar.setMnemonic('e');
        btnaceptar.setText("Aceptar");
        btnaceptar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnaceptar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnaceptar.setNextFocusableComponent(btncancelar);
        btnaceptar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Cancelar.png"))); // NOI18N
        btncancelar.setMnemonic('c');
        btncancelar.setText("Cancelar");
        btncancelar.setMaximumSize(new java.awt.Dimension(120, 50));
        btncancelar.setMinimumSize(new java.awt.Dimension(120, 50));
        btncancelar.setPreferredSize(new java.awt.Dimension(120, 50));
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncancelarActionPerformed

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        fnRedondear redondear = new fnRedondear();
        //ClaseIngresaPago contado
        if (!txtsaldo.getText().equals("")) {
            if (Double.valueOf(txtsaldo.getText()) <= 0.0) {
                saldo=Double.valueOf(txtsaldo.getText());
                if (!txtcontado.getText().equals("")) {
                    if (Double.valueOf(txtcontado.getText()) != 0) {
                        contado = Double.valueOf(txtcontado.getText());
                        
                    }
                }
                if (!txtctacte.getText().equals("")) {
                    if (Double.valueOf(txtctacte.getText()) != 0) {
                        ctacte = Double.valueOf(txtctacte.getText());
                    }
                }
                if (!txttarjetacredito.getText().equals("")) {
                    if (Double.valueOf(txttarjetacredito.getText()) != 0) {
                        credito = Double.valueOf(txttarjetacredito.getText());
                    }
                }
                if (!txttarjetadebito.getText().equals("")) {
                    if (Double.valueOf(txttarjetadebito.getText()) != 0) {
                        debito = Double.valueOf(txttarjetadebito.getText());
                    }
                }
                if (!txtcheque.getText().equals("")) {
                    if (Double.valueOf(txtcheque.getText()) != 0) {
                        cheque = Double.valueOf(txtcheque.getText());
                    }
                }
                if (!txtotros.getText().equals("")) {
                    if (Double.valueOf(txtotros.getText()) != 0) {
                        otros = Double.valueOf(txtotros.getText());
                    }
                }
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(null, "El Saldo debe ser 0.0");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Debe ingresar los valores de las Formas de Pago");
        }
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void txtcontadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcontadoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtcontado.getText())) {
            txtcontado.setText("");
        }
        cargarsaldo();
    }//GEN-LAST:event_txtcontadoKeyReleased

    private void txtcontadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcontadoActionPerformed
        cargarsaldo();
    }//GEN-LAST:event_txtcontadoActionPerformed

    private void txtctacteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtctacteActionPerformed
        cargarsaldo();
    }//GEN-LAST:event_txtctacteActionPerformed

    private void txttarjetacreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttarjetacreditoActionPerformed
        cargarsaldo();
    }//GEN-LAST:event_txttarjetacreditoActionPerformed

    private void txttarjetadebitoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txttarjetadebitoActionPerformed
        cargarsaldo();
    }//GEN-LAST:event_txttarjetadebitoActionPerformed

    private void txtchequeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtchequeActionPerformed
        cargarsaldo();
    }//GEN-LAST:event_txtchequeActionPerformed

    private void txtotrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtotrosActionPerformed
        cargarsaldo();
    }//GEN-LAST:event_txtotrosActionPerformed

    private void txtctacteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtctacteKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtctacte.getText())) {
            txtctacte.setText("");
        }
        cargarsaldo();
    }//GEN-LAST:event_txtctacteKeyReleased

    private void txttarjetacreditoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttarjetacreditoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txttarjetacredito.getText())) {
            txttarjetacredito.setText("");
        }
        cargarsaldo();
    }//GEN-LAST:event_txttarjetacreditoKeyReleased

    private void txttarjetadebitoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttarjetadebitoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txttarjetadebito.getText())) {
            txttarjetadebito.setText("");
        }
        cargarsaldo();
    }//GEN-LAST:event_txttarjetadebitoKeyReleased

    private void txtchequeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtchequeKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtcheque.getText())) {
            txtcheque.setText("");
        }
        cargarsaldo();
    }//GEN-LAST:event_txtchequeKeyReleased

    private void txtotrosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotrosKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtotros.getText())) {
            txtotros.setText("");
        }
        cargarsaldo();
    }//GEN-LAST:event_txtotrosKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField txtcheque;
    private javax.swing.JTextField txtcontado;
    private javax.swing.JTextField txtctacte;
    private javax.swing.JTextField txtotros;
    private javax.swing.JTextField txtsaldo;
    private javax.swing.JTextField txttarjetacredito;
    private javax.swing.JTextField txttarjetadebito;
    private javax.swing.JTextField txttotal;
    // End of variables declaration//GEN-END:variables
}
