package Formularios;

import Clases.ConexionMySQL;
import Clases.fnAlinear;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.*;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Productos extends javax.swing.JDialog {

    DefaultTableModel model;
    public static String idProducto = "";

    public Productos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargartabla("");
        cargardatoscategoria();
        cargardatosmarca();
        dobleclick();
    }

    //////////////////////FUNCION CARGAR CATEGORIA //////////////////////
    void cargardatoscategoria() {
        String sSQL = "";
        String cat = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectCategoria = null;
        sSQL = "SELECT nombre FROM categorias";
        cbocategorias.removeAllItems();
        cbocategorias.addItem("Categorias");
        try {
            SelectCategoria = cn.createStatement();
            ResultSet rs = SelectCategoria.executeQuery(sSQL);
            while (rs.next()) {
                cat = rs.getString("nombre");
                cbocategorias.addItem(cat);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCategoria != null) {
                    SelectCategoria.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION CARGAR MARCA //////////////////////
    void cargardatosmarca() {
        String sSQL = "";
        String mar = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectMarca = null;
        sSQL = "SELECT nombre FROM marcas";
        cbomarcas.removeAllItems();
        cbomarcas.addItem("Marcas");
        try {
            SelectMarca = cn.createStatement();
            ResultSet rs = SelectMarca.executeQuery(sSQL);
            while (rs.next()) {
                mar = rs.getString("nombre");
                cbomarcas.addItem(mar);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectMarca != null) {
                    SelectMarca.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION CARGAR TABLA //////////////////////
    public void cargartabla(String valor) {
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProductos = null;
        String[] Titulo = {"idProducto", "Codigo", "Nombre", "Unidad", "Mínimo", "", "", ""};
        Object[] Registros = new Object[8];
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sql = "SELECT * FROM vista_productos WHERE CONCAT(codigo, ' ', nombre) "
                + "LIKE '%" + valor + "%'";
        try {
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                
                model.addRow(Registros);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        tablaproductos.setModel(model);
        tablaproductos.setAutoCreateRowSorter(true);
        fnAlinear alinear= new fnAlinear();
        tablaproductos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());

        tablaproductos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(1).setPreferredWidth(100);
        tablaproductos.getColumnModel().getColumn(2).setPreferredWidth(250);
        tablaproductos.getColumnModel().getColumn(3).setPreferredWidth(70);
        tablaproductos.getColumnModel().getColumn(4).setPreferredWidth(70);
        tablaproductos.getColumnModel().getColumn(5).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setPreferredWidth(0);
    }

    //////////////////////FUNCION FILTRAR CATEGORIA//////////////////////
    void filtrarcategoria(String valor) {
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProductos = null;
        String[] Titulo = {"idProducto", "Codigo", "Nombre", "Unidad", "Mínimo", "", "", ""};
        Object[] Registros = new Object[8];
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sql = "SELECT * FROM vista_productos"
                + " WHERE categoria = '" + valor + "'";
        try {
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                model.addRow(Registros);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        tablaproductos.setModel(model);
        tablaproductos.setAutoCreateRowSorter(true);
        
        fnAlinear alinear= new fnAlinear();
        tablaproductos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
        
        tablaproductos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(1).setPreferredWidth(100);
        tablaproductos.getColumnModel().getColumn(2).setPreferredWidth(250);
        tablaproductos.getColumnModel().getColumn(3).setPreferredWidth(70);
        tablaproductos.getColumnModel().getColumn(4).setPreferredWidth(70);

        tablaproductos.getColumnModel().getColumn(5).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setPreferredWidth(0);
    }

    //////////////////////FUNCION FILTRAR MARCA//////////////////////
    void filtrarmarca(String valor) {
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProductos = null;
        String[] Titulo = {"idProducto", "Codigo", "Nombre", "Unidad", "Mínimo", "", "", ""};
        Object[] Registros = new Object[8];
        model = new DefaultTableModel(null, Titulo) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        String sql = "SELECT * FROM vista_productos"
                + " WHERE marca='" + valor + "'";
        try {
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                Registros[6] = rs.getString(7);
                Registros[7] = rs.getString(8);
                model.addRow(Registros);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        tablaproductos.setModel(model);
        tablaproductos.setAutoCreateRowSorter(true);
        
        fnAlinear alinear= new fnAlinear();
        tablaproductos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
        tablaproductos.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
        
        tablaproductos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(1).setPreferredWidth(100);
        tablaproductos.getColumnModel().getColumn(2).setPreferredWidth(250);
        tablaproductos.getColumnModel().getColumn(3).setPreferredWidth(70);
        tablaproductos.getColumnModel().getColumn(4).setPreferredWidth(70);
        tablaproductos.getColumnModel().getColumn(5).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(5).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(6).setPreferredWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMaxWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setMinWidth(0);
        tablaproductos.getColumnModel().getColumn(7).setPreferredWidth(0);
    }

    //////////////////////FUNCION DOBLE CLICK //////////////////////
    void dobleclick() {
        tablaproductos.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel = tablaproductos.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        idProducto = tablaproductos.getValueAt(tablaproductos.getSelectedRow(), 0).toString();
                        new ProductosModifica(null, true).setVisible(true);
                        cargartabla("");
                    }
                }

            }
        });
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtbuscar = new javax.swing.JTextField();
        cbocategorias = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaproductos = new javax.swing.JTable();
        cbomarcas = new javax.swing.JComboBox();
        buscar = new javax.swing.JLabel();
        btnmodificar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        btnagregar = new javax.swing.JButton();
        btndetalle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        cbocategorias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbocategoriasActionPerformed(evt);
            }
        });

        tablaproductos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaproductosKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaproductos);

        cbomarcas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbomarcasActionPerformed(evt);
            }
        });

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 828, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(buscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbocategorias, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cbomarcas, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cbocategorias, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cbomarcas, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 371, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5))
        );

        btnmodificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Modificar.png"))); // NOI18N
        btnmodificar.setText("Modificar");
        btnmodificar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnmodificar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnmodificar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Agregar.png"))); // NOI18N
        btnagregar.setText("Agregar");
        btnagregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnagregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnagregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        btndetalle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btndetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Documento.png"))); // NOI18N
        btndetalle.setText("Detalle");
        btndetalle.setMaximumSize(new java.awt.Dimension(120, 50));
        btndetalle.setMinimumSize(new java.awt.Dimension(120, 50));
        btndetalle.setPreferredSize(new java.awt.Dimension(120, 50));
        btndetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndetalleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btndetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnsalir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btndetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartabla(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        new ProductosAgrega(null, true).setVisible(true);
        cargartabla("");
    }//GEN-LAST:event_btnagregarActionPerformed

    private void btnmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarActionPerformed
        int filasel = tablaproductos.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            idProducto = tablaproductos.getValueAt(tablaproductos.getSelectedRow(), 0).toString();
            new ProductosModifica(null, true).setVisible(true);
            cargartabla("");
        }
    }//GEN-LAST:event_btnmodificarActionPerformed

    private void tablaproductosKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaproductosKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablaproductosKeyPressed

    private void cbocategoriasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbocategoriasActionPerformed
        if (!cbocategorias.getSelectedItem().toString().equals("Categorias")) {
            filtrarcategoria(cbocategorias.getSelectedItem().toString());
        } else {
            cargartabla("");
        }
    }//GEN-LAST:event_cbocategoriasActionPerformed

    private void cbomarcasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbomarcasActionPerformed
        if (!cbomarcas.getSelectedItem().toString().equals("Marcas")) {
            filtrarmarca(cbomarcas.getSelectedItem().toString());
        } else {
            cargartabla("");
        }
    }//GEN-LAST:event_cbomarcasActionPerformed

    private void btndetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndetalleActionPerformed
        int filasel = tablaproductos.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            idProducto = tablaproductos.getValueAt(tablaproductos.getSelectedRow(), 0).toString();
            new ProductosDetalle(null, true).setVisible(true);
            cargartabla("");
        }
    }//GEN-LAST:event_btndetalleActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnagregar;
    private javax.swing.JButton btndetalle;
    private javax.swing.JButton btnmodificar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel buscar;
    private javax.swing.JComboBox cbocategorias;
    private javax.swing.JComboBox cbomarcas;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaproductos;
    private javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables
}
