package Formularios;

import Clases.ClaseStock;
import Clases.ConexionMySQL;
import Clases.ConexionMySQL;
import Clases.fnEditarCeldas;
import Clases.fnExportar;
import Clases.cboDeposito;
import Clases.cboDeposito;
import Clases.fnAlinear;
import Clases.fnAlinear;
import Clases.fnCargarFecha;
import Clases.fnEditarCeldas;
import Clases.fnExportar;
import Clases.fnRedondear;
import Clases.fnRedondear;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

public class ActualizarPrecios extends javax.swing.JDialog {

    DefaultTableModel model;
    public static int idProductos, idDeposito;

    public ActualizarPrecios(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargardepositos();
        cargartablabuscar("");
        /// dobleclick();
    }

    ///// CARGAR CBO DEPOSITOS /////
    void cargardepositos() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbodeposito.setModel(value);
        idDeposito = 1;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectDeposito = null;
        String sSQL = "SELECT idDepositos, nombre FROM deposito";
        try {
            SelectDeposito = cn.createStatement();
            ResultSet rs = SelectDeposito.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboDeposito(rs.getInt("idDepositos"), rs.getString("nombre")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectDeposito != null) {
                    SelectDeposito.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////

///// CARGAR TABLA BUSCAR /////
    void cargartablabuscar(String valor) {
        String[] Titulo = {"Id", "Codigo", "Nombre", "Precio Compra", "Precio Base", "Bon%", "IdIva", "Iva%", "Stock", "PTotal"};
        String[] Registros = new String[10];
        String sql = "SELECT * FROM vista_productos_busqueda WHERE CONCAT(codigo, ' ', nombre) LIKE '%" + valor + "%' and idDepositos=" + idDeposito + "";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                if (column == 3 || column == 4) {
                    return true;
                } else {
                    return false;
                }
            }

        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectVProducto = null;
        Statement SelectStock = null;
        try {
            SelectVProducto = cn.createStatement();
            ResultSet rs = SelectVProducto.executeQuery(sql);
            while (rs.next()) {
                //Se fija si la suma de Stock es cero no lo pone
                if (rs.getInt(4) != 0) {
                    Registros[0] = rs.getString(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    Registros[3] = rs.getString("precioVenta");
                    Registros[4] = rs.getString(5);
                    Registros[5] = rs.getString(6);
                    Registros[6] = rs.getString(7);
                    ////////////////////////
                    Registros[7] = rs.getString("stock");
                    Registros[8] = rs.getString("precioVenta");
                    model.addRow(Registros);
                }
            }

            tablabuscar.setModel(model);
            tablabuscar.setAutoCreateRowSorter(true);
            //escondo columna 
            tablabuscar.getColumnModel().getColumn(0).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(6).setPreferredWidth(0);
            tablabuscar.getColumnModel().getColumn(8).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(8).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(8).setPreferredWidth(0);

            fnAlinear alinear = new fnAlinear();
            tablabuscar.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablabuscar.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(7).setCellRenderer(alinear.alinearDerecha());
            //tamaño
            tablabuscar.getColumnModel().getColumn(1).setPreferredWidth(70);
            tablabuscar.getColumnModel().getColumn(2).setPreferredWidth(170);
            tablabuscar.getColumnModel().getColumn(3).setPreferredWidth(55);
            tablabuscar.getColumnModel().getColumn(4).setPreferredWidth(30);
            tablabuscar.getColumnModel().getColumn(5).setPreferredWidth(30);
            tablabuscar.getColumnModel().getColumn(7).setPreferredWidth(40);
            tablabuscar.getColumnModel().getColumn(9).setCellRenderer(new fnEditarCeldas());

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectVProducto != null) {
                    SelectVProducto.close();
                }
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////

    String cargar_ultimo_precio(int valor) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectPrecio = null;
        int bandera = 0;
        String precio_compra = "";
        String sSQL = "SELECT precioCompra FROM stock inner join afip_tipoiva on afip_tipoiva.idTipoiva = stock.idTipoiva where idProductos=" + valor + " and precioCompra is not null  order by idstock DESC limit 1";
        try {
            SelectPrecio = cn.createStatement();
            ResultSet rs = SelectPrecio.executeQuery(sSQL);
            while (rs.next()) {

                precio_compra = rs.getString(1);
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectPrecio != null) {
                    SelectPrecio.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }

        }
        return precio_compra;
    }

    ///// FILTRAR TABLA BUSCAR DEPOSITOS /////
    void filtrartablabuscardep(int codigo) {
        String[] Titulo = {"Id", "Codigo", "Nombre", "Precio Base", "Bon%", "IdIva", "Iva%", "Stock", "PTotal"};
        String[] Registros = new String[9];
        String sql = "SELECT * FROM vista_productos_busqueda WHERE idDepositos=" + codigo + "";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectVProducto = null;
        Statement SelectStock = null;
        try {
            SelectVProducto = cn.createStatement();
            ResultSet rs = SelectVProducto.executeQuery(sql);
            while (rs.next()) {
                //Se fija si la suma de Stock es cero no lo pone
                if (rs.getInt(4) != 0) {
                    Registros[0] = rs.getString(1);
                    Registros[1] = rs.getString(2);
                    Registros[2] = rs.getString(3);
                    Registros[3] = rs.getString("precioVenta");
                    Registros[4] = rs.getString(5);
                    Registros[5] = rs.getString(6);
                    Registros[6] = rs.getString(7);
                    ////////////////////////
                    Registros[7] = rs.getString("stock");
                    Registros[8] = rs.getString("precioVenta");
                    model.addRow(Registros);
                }
            }

            tablabuscar.setModel(model);
            tablabuscar.setAutoCreateRowSorter(true);
            //escondo columna 
            tablabuscar.getColumnModel().getColumn(0).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setMaxWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setMinWidth(0);
            tablabuscar.getColumnModel().getColumn(5).setPreferredWidth(0);

            fnAlinear alinear = new fnAlinear();
            tablabuscar.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablabuscar.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(6).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(7).setCellRenderer(alinear.alinearDerecha());
            tablabuscar.getColumnModel().getColumn(8).setCellRenderer(alinear.alinearDerecha());

            //tamaño
            tablabuscar.getColumnModel().getColumn(1).setPreferredWidth(70);
            tablabuscar.getColumnModel().getColumn(2).setPreferredWidth(170);
            tablabuscar.getColumnModel().getColumn(3).setPreferredWidth(55);
            tablabuscar.getColumnModel().getColumn(4).setPreferredWidth(30);
            tablabuscar.getColumnModel().getColumn(6).setPreferredWidth(30);
            tablabuscar.getColumnModel().getColumn(7).setPreferredWidth(40);
            tablabuscar.getColumnModel().getColumn(8).setPreferredWidth(60);
            tablabuscar.getColumnModel().getColumn(8).setCellRenderer(new fnEditarCeldas());

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectVProducto != null) {
                    SelectVProducto.close();
                }
                if (SelectStock != null) {
                    SelectStock.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    ////////////////////////////////////////


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtbuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablabuscar = new javax.swing.JTable();
        cbodeposito = new javax.swing.JComboBox();
        buscar = new javax.swing.JLabel();
        btnsalir = new javax.swing.JButton();
        btnamodificar = new javax.swing.JButton();
        btnexportar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        tablabuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablabuscarKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablabuscar);

        cbodeposito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbodepositoActionPerformed(evt);
            }
        });

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbodeposito, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(cbodeposito, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)))
                .addGap(5, 5, 5)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE))
        );

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        btnamodificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnamodificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Modificar.png"))); // NOI18N
        btnamodificar.setText("Modificar");
        btnamodificar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnamodificar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnamodificar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnamodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnamodificarActionPerformed(evt);
            }
        });

        btnexportar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnexportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Descargar.png"))); // NOI18N
        btnexportar.setText("Exportar");
        btnexportar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnexportar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnexportar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnexportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexportarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnamodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(326, 326, 326)
                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnamodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        /* String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartablabuscar(txtbuscar.getText());*/

        TableRowSorter sorter = new TableRowSorter(model);
        sorter.setRowFilter(RowFilter.regexFilter(".*" + txtbuscar.getText() + ".*"));
        tablabuscar.setRowSorter(sorter);
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnamodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnamodificarActionPerformed
        int n = tablabuscar.getRowCount();
        int i = 0;
        btnamodificar.setEnabled(false);
        btnsalir.setEnabled(false);
        btnexportar.setEnabled(false);
        while (i < n) {
            fnCargarFecha fecha = new fnCargarFecha();
            String fechastock = fecha.cargarfecha();
            int idProducto = Integer.valueOf(tablabuscar.getValueAt(i, 0).toString());
            String pc = tablabuscar.getValueAt(i, 3).toString();
            String pv = tablabuscar.getValueAt(i, 4).toString();
            String bonif = tablabuscar.getValueAt(i, 5).toString();
            int idiva = Integer.valueOf(tablabuscar.getValueAt(i, 6).toString());
            ClaseStock act = new ClaseStock();
            act.ActualizacionPrecio(idProducto, pc, pv, bonif, fechastock, idDeposito, idiva);
            i++;
        }
        JOptionPane.showMessageDialog(null, "Los Datos se modificaron exitosamente...");
        btnamodificar.setEnabled(true);
        btnsalir.setEnabled(true);
        btnexportar.setEnabled(true);
        this.dispose();
    }//GEN-LAST:event_btnamodificarActionPerformed

    private void tablabuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablabuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            //redondear.dosDigitos(rs2.getDouble(2) * (1 - (rs2.getDouble(3) / 100)) * (1 + (rs2.getDouble(4) / 100)))
            double pc = Double.valueOf(tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 4).toString());
            double iva;
            if (!tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 7).toString().equals("No Gravado")) {
                iva = Double.valueOf(tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 7).toString());
            } else {
                iva = 0;
            }
            double bonif = Double.valueOf(tablabuscar.getValueAt(tablabuscar.getSelectedRow(), 5).toString());
            double pv = Math.round(pc * (1 - (bonif / 100)) * (1 + (iva / 100)));

            tablabuscar.setValueAt(String.valueOf(pv), tablabuscar.getSelectedRow(), 9);
        }
    }//GEN-LAST:event_tablabuscarKeyPressed

    private void btnexportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexportarActionPerformed
        if (this.tablabuscar.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "La tabla está vacía");
            return;
        }
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Guardar Archivo");
        chooser.setMultiSelectionEnabled(false);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            List<JTable> tb = new ArrayList<>();
            List<String> nom = new ArrayList<>();
            tb.add(tablabuscar);
            nom.add("Tabla Productos");
            String archivo = chooser.getSelectedFile().toString().concat(".xls");
            try {
                Clases.fnExportar e = new fnExportar(new File(archivo), tb, nom);
                if (e.export()) {
                    JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente", "", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_btnexportarActionPerformed

    private void cbodepositoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbodepositoActionPerformed
        cboDeposito dp = (cboDeposito) cbodeposito.getSelectedItem();
        int codigo = dp.getidDeposito();
        filtrartablabuscardep(codigo);
        idDeposito = codigo;
    }//GEN-LAST:event_cbodepositoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnamodificar;
    private javax.swing.JButton btnexportar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel buscar;
    private javax.swing.JComboBox cbodeposito;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablabuscar;
    private javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables
}
