package Formularios;

import Clases.ConexionMySQL;
import Clases.fnAlinear;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ClientesElegir extends javax.swing.JDialog {

    DefaultTableModel model;

    public ClientesElegir(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        dobleclick();
        cargartabla("");
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }

    void dobleclick() {
        tablaclientes.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    btnagregar.doClick();
                }
            }
        });
    }

    void cargartabla(String valor) {
        String[] Titulo = {"id", "Razon Social/Nombre", "Resposabilidad IVA", "Tipo Doc", "Documento", "Margen"};
        String[] Registros = new String[8];
        String sql = "SELECT * FROM vista_clientes WHERE CONCAT(IvaDocumento, ' ', nombre)LIKE '%" + valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectCliente = null;
        try {
            SelectCliente = cn.createStatement();
            ResultSet rs = SelectCliente.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                model.addRow(Registros);
            }
            tablaclientes.setModel(model);
            tablaclientes.setAutoCreateRowSorter(true);
            fnAlinear alinear=new fnAlinear();
            tablaclientes.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
            tablaclientes.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearDerecha());
            tablaclientes.getColumnModel().getColumn(0).setMaxWidth(0);
            tablaclientes.getColumnModel().getColumn(0).setMinWidth(0);
            tablaclientes.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablaclientes.getColumnModel().getColumn(1).setPreferredWidth(250);
            tablaclientes.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablaclientes.getColumnModel().getColumn(3).setPreferredWidth(100);
            tablaclientes.getColumnModel().getColumn(4).setPreferredWidth(150);
            tablaclientes.getColumnModel().getColumn(5).setPreferredWidth(100);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCliente != null) {
                    SelectCliente.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaclientes = new javax.swing.JTable();
        txtbuscar = new javax.swing.JTextField();
        buscar = new javax.swing.JLabel();
        btnagregar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Elegir Clientes");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablaclientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        tablaclientes.setNextFocusableComponent(txtbuscar);
        tablaclientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaclientesKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaclientes);

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.setNextFocusableComponent(tablaclientes);
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(buscar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 498, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(buscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtbuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Agregar.png"))); // NOI18N
        btnagregar.setMnemonic('a');
        btnagregar.setText("Agregar");
        btnagregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnagregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnagregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        int filasel;
        filasel = tablaclientes.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            Main.IdCliente = Integer.valueOf(tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 0).toString());
            Main.nombreCliente = tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 1).toString();
            Main.ResposabilidadCliente = tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 2).toString();
            Main.TipodocCliente = tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 3).toString();
            Main.DocCliente = tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 4).toString();
            this.dispose();
        }
    }//GEN-LAST:event_btnagregarActionPerformed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        int fila = tablaclientes.getRowCount();
        if (fila != 0) {
            txtbuscar.transferFocus();
            tablaclientes.setRowSelectionInterval(0, 0);
        }
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        cargartabla(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void tablaclientesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaclientesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int filasel;
            filasel = tablaclientes.getSelectedRow();
            if (filasel == -1) {
                JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
            } else {
                Main.IdCliente = Integer.valueOf(tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 0).toString());
                Main.nombreCliente = tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 1).toString();
                Main.ResposabilidadCliente = tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 2).toString();
                Main.TipodocCliente = tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 3).toString();
                Main.DocCliente = tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 4).toString();
                this.dispose();
            }
        }
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            tablaclientes.transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablaclientesKeyPressed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnagregar;
    private javax.swing.JLabel buscar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaclientes;
    private javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables
}
