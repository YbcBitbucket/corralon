package Formularios;

import Clases.fnHiloInicio;
import com.sun.awt.AWTUtilities;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;


public class Inicio extends javax.swing.JFrame {

    fnHiloInicio hilo;

    public Inicio() {
        initComponents();
        AWTUtilities.setWindowOpaque(this, false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        iniciarSplash();
        hilo = new fnHiloInicio(progreso);
        hilo.start();
        
        //Le damos tamaño y posición a nuestro Frame
        // this.setLocation(500, 150);
        //   this.setSize(331, 533);
        //Liberamos recursos
        hilo = null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        progreso = new javax.swing.JProgressBar();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ybgestor");
        setModalExclusionType(null);
        setUndecorated(true);
        setResizable(false);

        progreso.setForeground(new java.awt.Color(5, 38, 88));
        progreso.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                progresoStateChanged(evt);
            }
        });
        getContentPane().add(progreso, java.awt.BorderLayout.PAGE_END);
        progreso.getAccessibleContext().setAccessibleName("");

        jPanel1.setBackground(new java.awt.Color(0, 102, 255));
        jPanel1.setMinimumSize(new java.awt.Dimension(651, 649));
        jPanel1.setPreferredSize(new java.awt.Dimension(651, 649));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("YBGestor");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 244, 620, 60));

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void progresoStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_progresoStateChanged

        if (progreso.getValue() == 100) {
            this.dispose();
        }

    }//GEN-LAST:event_progresoStateChanged

    public javax.swing.JProgressBar getjProgressBar1() {
        return progreso;
    }

    public void iniciarSplash() {
        UIManager.put("ProgressBar.background", Color.ORANGE);
        UIManager.put("ProgressBar.foreground", Color.BLUE);
        UIManager.put("ProgressBar.selectionBackground", Color.RED);
        UIManager.put("ProgressBar.selectionForeground", Color.GREEN);
        this.getjProgressBar1().setBorderPainted(false);

        /*this.getjProgressBar1().setForeground(Color.BLACK);
        this.getjProgressBar1().setBackground(Color.yellow);*/
        //[77,239,38]
        //this.getjProgressBar1().setStringPainted(true);
    }

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Inicio().setVisible(true);

                try {
                    for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                        if ("Windows".equals(info.getName())) {
                            javax.swing.UIManager.setLookAndFeel(info.getClassName());
                            break;

                        }
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "ERROR" + e.getMessage());
                }

            }
        });
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar progreso;
    // End of variables declaration//GEN-END:variables
}
