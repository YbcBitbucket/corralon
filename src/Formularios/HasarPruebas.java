package Formularios;

import Clases.ConexionMySQL;
import Clases.ServiciosHasar;
import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import java.sql.Connection;
import java.sql.PreparedStatement;


import javax.swing.JOptionPane;

public class HasarPruebas extends javax.swing.JDialog {

    public HasarPruebas(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        cbotransporte = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        cbobaudios = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        cbopuerto = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        txtdireccionip = new javax.swing.JTextField();
        btnComenzar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        btnAvanzar = new javax.swing.JButton();
        btnAvanzar1 = new javax.swing.JButton();

        jLabel4.setText("jLabel4");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Transporte:");

        cbotransporte.setMaximumRowCount(3);
        cbotransporte.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PUERTO_SERIE", "SOCKET_TCP", "SOCKET_UDP" }));
        cbotransporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotransporteActionPerformed(evt);
            }
        });

        jLabel2.setText("Baudios:");

        cbobaudios.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1200", "2400", "4800", "9600", "19200", "38400", "57600", "115200" }));
        cbobaudios.setSelectedIndex(3);

        jLabel3.setText("Puerto:");

        cbopuerto.setMaximumRowCount(10);
        cbopuerto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Puerto 0", "Puerto 1", "Puerto 2", "Puerto 3", "Puerto 4", "Puerto 5", "Puerto 6", "Puerto 7", "Puerto 8", "Puerto 9" }));

        jLabel5.setText("Direccion IP:");

        txtdireccionip.setEnabled(false);

        btnComenzar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnComenzar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnComenzar.setText("Comenzar");
        btnComenzar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnComenzar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnComenzar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnComenzar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnComenzarActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnSalir.setMnemonic('s');
        btnSalir.setText("Salir");
        btnSalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnSalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnSalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnAvanzar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAvanzar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnAvanzar.setText("Avanzar Papel");
        btnAvanzar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnAvanzar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnAvanzar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnAvanzar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvanzarActionPerformed(evt);
            }
        });

        btnAvanzar1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAvanzar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnAvanzar1.setText("Datos Iniciales");
        btnAvanzar1.setMaximumSize(new java.awt.Dimension(120, 50));
        btnAvanzar1.setMinimumSize(new java.awt.Dimension(120, 50));
        btnAvanzar1.setPreferredSize(new java.awt.Dimension(120, 50));
        btnAvanzar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAvanzar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnComenzar, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnAvanzar, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnAvanzar1, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cbotransporte, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cbopuerto, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtdireccionip, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cbobaudios, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cbotransporte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbopuerto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cbobaudios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtdireccionip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnComenzar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAvanzar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAvanzar1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnComenzarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnComenzarActionPerformed
        
        try {
        ServiciosHasar hasar = new ServiciosHasar();
        JOptionPane.showMessageDialog(null, hasar.Comenzar(cbotransporte.getSelectedIndex(), cbopuerto.getSelectedIndex(), Integer.valueOf(cbobaudios.getSelectedItem().toString()), txtdireccionip.getText()));
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }//GEN-LAST:event_btnComenzarActionPerformed

    private void cbotransporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotransporteActionPerformed
        
    }//GEN-LAST:event_cbotransporteActionPerformed

    private void btnAvanzarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvanzarActionPerformed
        try {
        ServiciosHasar hasar = new ServiciosHasar();
        hasar.Comenzar(cbotransporte.getSelectedIndex(), cbopuerto.getSelectedIndex(), Integer.valueOf(cbobaudios.getSelectedItem().toString()), txtdireccionip.getText());
        hasar.AvanzarPapel(0, 3);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }//GEN-LAST:event_btnAvanzarActionPerformed

    private void btnAvanzar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAvanzar1ActionPerformed
        try {
        ServiciosHasar hasar = new ServiciosHasar();
        hasar.Comenzar(cbotransporte.getSelectedIndex(), cbopuerto.getSelectedIndex(), Integer.valueOf(cbobaudios.getSelectedItem().toString()), txtdireccionip.getText());
        hasar.ObtenerDatosDeInicializacion();
        String Datos=   "Nro CUIT : "+hasar.Respuesta(3) +"\n"+
                        "Razón Social : "+hasar.Respuesta(4) +"\n"+ 
                        "Nro Serie : " +hasar.Respuesta(5) +"\n"+
                        "Nro Pto de Vt a : " +hasar.Respuesta(7) +"\n"+
                        "Inicio de Actividades : " +hasar.Respuesta(9) +"\n"+
                        "Categ IVA : " +hasar.Respuesta(10) +"\n";

        JOptionPane.showMessageDialog(null, Datos);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }//GEN-LAST:event_btnAvanzar1ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAvanzar;
    private javax.swing.JButton btnAvanzar1;
    private javax.swing.JButton btnComenzar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox<String> cbobaudios;
    private javax.swing.JComboBox<String> cbopuerto;
    private javax.swing.JComboBox<String> cbotransporte;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField txtdireccionip;
    // End of variables declaration//GEN-END:variables
}
