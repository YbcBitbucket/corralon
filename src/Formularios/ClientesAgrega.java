package Formularios;

import Clases.ClaseClientes;
import Clases.ConexionMySQL;
import Clases.cboResponsableIVA;
import Clases.cboTipoDoc;
import Clases.fnesNumerico;
import java.sql.*;
import javax.swing.*;

public class ClientesAgrega extends javax.swing.JDialog {

    public ClientesAgrega(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargartiporesponsable();
        cargartipodoc();
    }

    ///// CARGAR CBO IVA RESPONSABLE /////
    void cargartiporesponsable() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboresponsable.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTipoResponsable = null;
        String sSQL = "SELECT * FROM afip_tiporesponsable";
        try {
            SelectTipoResponsable = cn.createStatement();
            ResultSet rs = SelectTipoResponsable.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboResponsableIVA(rs.getInt("idTiporesponsable"), rs.getString("descripcion")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTipoResponsable != null) {
                    SelectTipoResponsable.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ///// CARGAR CBO TIPO DOC /////
    void cargartipodoc() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipodoc.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTipoDoc = null;
        String sSQL = "SELECT * FROM afip_tipodoc";
        try {
            SelectTipoDoc = cn.createStatement();
            ResultSet rs = SelectTipoDoc.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboTipoDoc(rs.getInt("idTipodoc"), rs.getString("descripcion")));
            }
            cbotipodoc.setSelectedIndex(24);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTipoDoc != null) {
                    SelectTipoDoc.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnagregar = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        txtdomicilio = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtlocalidad = new javax.swing.JTextField();
        txtprovincia = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txttelefonocel = new javax.swing.JTextField();
        txtmail = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtpersona = new javax.swing.JTextField();
        txttelefonofijo = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtobs = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtdocumento = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        cboresponsable = new javax.swing.JComboBox();
        cbotipodoc = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        txtmargen = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cliente", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Agregar.png"))); // NOI18N
        btnagregar.setText("Agregar");
        btnagregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnagregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnagregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Cancelar.png"))); // NOI18N
        btncancelar.setText("Cancelar");
        btncancelar.setMaximumSize(new java.awt.Dimension(120, 50));
        btncancelar.setMinimumSize(new java.awt.Dimension(120, 50));
        btncancelar.setPreferredSize(new java.awt.Dimension(120, 50));
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        jLabel2.setText("Razón Social / Nombre");

        txtdomicilio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdomicilioKeyReleased(evt);
            }
        });

        jLabel3.setText("Domicilio");

        jLabel11.setText("Localidad");

        txtlocalidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlocalidadKeyReleased(evt);
            }
        });

        txtprovincia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtprovinciaKeyReleased(evt);
            }
        });

        jLabel12.setText("Provincia");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Contacto", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel4.setText("Teléfono Cel");

        txttelefonocel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttelefonocelKeyReleased(evt);
            }
        });

        txtmail.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmailKeyReleased(evt);
            }
        });

        jLabel6.setText("Persona");

        txtpersona.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpersonaKeyReleased(evt);
            }
        });

        txttelefonofijo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttelefonofijoKeyReleased(evt);
            }
        });

        jLabel5.setText("Mail");

        jLabel9.setText("Teléfono Fijo");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtpersona, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                    .addComponent(txttelefonocel, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                    .addComponent(txttelefonofijo)
                    .addComponent(txtmail))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtpersona, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txttelefonocel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txttelefonofijo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtmail, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(6, 6, 6))
        );

        jLabel7.setText("Observación");

        txtobs.setColumns(5);
        txtobs.setRows(3);
        txtobs.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtobsKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(txtobs);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Afip", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel10.setText("Tipo Doc");

        jLabel17.setText("Resposable IVA");

        txtdocumento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdocumentoKeyReleased(evt);
            }
        });

        jLabel19.setText("Documento");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel10)
                    .addComponent(jLabel19))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cboresponsable, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtdocumento, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(cbotipodoc, javax.swing.GroupLayout.Alignment.LEADING, 0, 200, Short.MAX_VALUE)))
                .addGap(0, 97, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(cboresponsable, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cbotipodoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtdocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19))
                .addGap(12, 12, 12))
        );

        jLabel13.setText("Margen");

        txtmargen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmargenKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtmargen, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(txtdomicilio, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                                        .addComponent(txtlocalidad, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 301, Short.MAX_VALUE)
                                        .addComponent(txtprovincia, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtnombre, javax.swing.GroupLayout.Alignment.LEADING)))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtdomicilio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtprovincia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtmargen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addGap(12, 12, 12)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        //Chequea q no haya datos vacios
        if (txtnombre.getText().equals("") || txtdomicilio.getText().equals("") || txtlocalidad.getText().equals("")
                || txtprovincia.getText().equals("") || txtpersona.getText().equals("")
                || txttelefonocel.getText().equals("") || txtdocumento.getText().equals("")
                || txtmargen.getText().equals("")) {
            ///Ingresar todos los datos 
            JOptionPane.showMessageDialog(null, "No ingreso todos los datos del cliente");
        } else {
            //Traigo Tipo Responsable y tipo Doc
            cboResponsableIVA resp = (cboResponsableIVA) cboresponsable.getSelectedItem();
            int idResponsableIVA = resp.getidTiporesponsable();

            cboTipoDoc doc = (cboTipoDoc) cbotipodoc.getSelectedItem();
            int idTipodoc = doc.getidTipodoc();

            ClaseClientes cliente = new ClaseClientes();
            if (cliente.AgregarCliente(idResponsableIVA, idTipodoc, txtdocumento.getText(),
                    txtnombre.getText(), txtdomicilio.getText(), txtlocalidad.getText(), txtprovincia.getText(),
                    Integer.valueOf(txtmargen.getText()), txtpersona.getText(), txttelefonocel.getText(),
                    txttelefonofijo.getText(), txtmail.getText(), txtobs.getText()) == 1) {
                //Limpio Campos
                txtnombre.setText("");
                txtdomicilio.setText("");
                txtlocalidad.setText("");
                txtprovincia.setText("");
                txtmargen.setText("");
                cargartiporesponsable();
                cargartipodoc();
                txtdocumento.setText("");
                txtpersona.setText("");
                txttelefonocel.setText("");
                txttelefonofijo.setText("");
                txtmail.setText("");
                txtobs.setText("");
                btnagregar.transferFocus();
            }
        }
    }//GEN-LAST:event_btnagregarActionPerformed

    private void txtdomicilioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdomicilioKeyReleased
        String texto = txtdomicilio.getText().toUpperCase();
        txtdomicilio.setText(texto);
    }//GEN-LAST:event_txtdomicilioKeyReleased

    private void txtlocalidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlocalidadKeyReleased
        String texto = txtlocalidad.getText().toUpperCase();
        txtlocalidad.setText(texto);
    }//GEN-LAST:event_txtlocalidadKeyReleased

    private void txtprovinciaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtprovinciaKeyReleased
        String texto = txtprovincia.getText().toUpperCase();
        txtprovincia.setText(texto);
    }//GEN-LAST:event_txtprovinciaKeyReleased

    private void txtpersonaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpersonaKeyReleased
        String texto = txtpersona.getText().toUpperCase();
        txtpersona.setText(texto);
    }//GEN-LAST:event_txtpersonaKeyReleased

    private void txttelefonocelKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelefonocelKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txttelefonocel.getText())) {
            txttelefonocel.setText("");
        }
    }//GEN-LAST:event_txttelefonocelKeyReleased

    private void txtmailKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmailKeyReleased
        String texto = txtmail.getText().toLowerCase();
        txtmail.setText(texto);
    }//GEN-LAST:event_txtmailKeyReleased

    private void txtobsKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtobsKeyReleased
        String texto = txtobs.getText().toUpperCase();
        txtobs.setText(texto);
    }//GEN-LAST:event_txtobsKeyReleased

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        txtnombre.setText("");
        txtdomicilio.setText("");
        txtlocalidad.setText("");
        txtprovincia.setText("");
        txtmargen.setText("");
        cargartiporesponsable();
        cargartipodoc();
        txtdocumento.setText("");
        txtpersona.setText("");
        txttelefonocel.setText("");
        txttelefonofijo.setText("");
        txtmail.setText("");
        txtobs.setText("");
        btnagregar.transferFocus();
    }//GEN-LAST:event_btncancelarActionPerformed

    private void txttelefonofijoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttelefonofijoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txttelefonofijo.getText())) {
            txttelefonofijo.setText("");
        }
    }//GEN-LAST:event_txttelefonofijoKeyReleased

    private void txtdocumentoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdocumentoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtdocumento.getText())) {
            txtdocumento.setText("");
        }
    }//GEN-LAST:event_txtdocumentoKeyReleased

    private void txtmargenKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmargenKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtmargen.getText())) {
            txtmargen.setText("");
        }
    }//GEN-LAST:event_txtmargenKeyReleased
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnagregar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JComboBox cboresponsable;
    private javax.swing.JComboBox cbotipodoc;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField txtdocumento;
    private javax.swing.JTextField txtdomicilio;
    private javax.swing.JTextField txtlocalidad;
    private javax.swing.JTextField txtmail;
    private javax.swing.JTextField txtmargen;
    private javax.swing.JTextField txtnombre;
    private javax.swing.JTextArea txtobs;
    private javax.swing.JTextField txtpersona;
    private javax.swing.JTextField txtprovincia;
    private javax.swing.JTextField txttelefonocel;
    private javax.swing.JTextField txttelefonofijo;
    // End of variables declaration//GEN-END:variables

}
