package Formularios;

import Clases.ClaseIvaCompra;
import Clases.ConexionMySQL;
import Clases.cboOperacion;
import Clases.cboProveedor;
import Clases.cboTipoCbte;
import Clases.cboTipoMoneda;
import Clases.fnesNumerico;
import java.sql.*;
import java.text.SimpleDateFormat;
import javax.swing.*;

public class IvaCompraAgrega extends javax.swing.JDialog {

    SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");

    public IvaCompraAgrega(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargartipocomprobante();
        cargardatosproveedor();
        cargartipomoneda();
        cargaroperacion();
    }

    ///// CARGAR CBO TIPO COMPROBANTE /////
    void cargartipocomprobante() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipocbte.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTipoComprobante = null;
        String sSQL = "SELECT * FROM afip_tipocbte";
        try {
            SelectTipoComprobante = cn.createStatement();
            ResultSet rs = SelectTipoComprobante.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboTipoCbte(rs.getInt("idTipoCbte"), rs.getString("descripcion")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTipoComprobante != null) {
                    SelectTipoComprobante.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////CARGAR PROVEEDOR/////////////
    void cargardatosproveedor() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cboproveedor.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTipoProveedor = null;
        String sSQL = "SELECT * FROM proveedores";
        try {
            SelectTipoProveedor = cn.createStatement();
            ResultSet rs = SelectTipoProveedor.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboProveedor(rs.getInt("idProveedores"), rs.getString("razonsocial")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTipoProveedor != null) {
                    SelectTipoProveedor.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ///// CARGAR CBO TIPO MONEDA /////
    void cargartipomoneda() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbotipomoneda.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectTipoMoneda = null;
        String sSQL = "SELECT * FROM afip_tipomoneda";
        try {
            SelectTipoMoneda = cn.createStatement();
            ResultSet rs = SelectTipoMoneda.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboTipoMoneda(rs.getInt("idTipomoneda"), rs.getString("descripcion")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectTipoMoneda != null) {
                    SelectTipoMoneda.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    ///// CARGAR CBO TIPO MONEDA /////
    void cargaroperacion() {
        DefaultComboBoxModel value = new DefaultComboBoxModel();
        cbooperacion.setModel(value);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectOperacion = null;
        String sSQL = "SELECT * FROM afip_operacion";
        try {
            SelectOperacion = cn.createStatement();
            ResultSet rs = SelectOperacion.executeQuery(sSQL);
            while (rs.next()) {
                value.addElement(new cboOperacion(rs.getInt("idCodigoOperacion"), rs.getString("descripcion")));
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectOperacion != null) {
                    SelectOperacion.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    void fnCancelar() {
        fechacbte.setDate(null);
        cargartipocomprobante();
        txtptoventa.setText("");
        txtnumerocbte.setText("");
        cargardatosproveedor();
        txttotal.setText("");
        txtnointegra.setText("");
        txtexentas.setText("");
        txtvaloragregado.setText("");
        txtimpuestonacionales.setText("");
        txtingresosbrutos.setText("");
        txtimpuestosmunicipales.setText("");
        txtimpuestosinternos.setText("");
        txtotrostributos.setText("");
        cargartipomoneda();
        cargaroperacion();

        btnagregar.transferFocus();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jPanel1 = new javax.swing.JPanel();
        btnagregar = new javax.swing.JButton();
        btncancelar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtnumerocbte = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        txtnointegra = new javax.swing.JTextField();
        txtvaloragregado = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txttotal = new javax.swing.JTextField();
        txtexentas = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtimpuestonacionales = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtingresosbrutos = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtimpuestosmunicipales = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txtimpuestosinternos = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtotrostributos = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        cboproveedor = new javax.swing.JComboBox();
        cbotipocbte = new javax.swing.JComboBox();
        fechacbte = new com.toedter.calendar.JDateChooser();
        jLabel16 = new javax.swing.JLabel();
        cbotipomoneda = new javax.swing.JComboBox();
        jLabel17 = new javax.swing.JLabel();
        cbooperacion = new javax.swing.JComboBox();
        txtptoventa = new javax.swing.JTextField();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Comprobante IVA Compra", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Agregar.png"))); // NOI18N
        btnagregar.setText("Agregar");
        btnagregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnagregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnagregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Cancelar.png"))); // NOI18N
        btncancelar.setText("Cancelar");
        btncancelar.setMaximumSize(new java.awt.Dimension(120, 50));
        btncancelar.setMinimumSize(new java.awt.Dimension(120, 50));
        btncancelar.setPreferredSize(new java.awt.Dimension(120, 50));
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        jLabel2.setText("Fecha de Comprobante");

        jLabel3.setText("Tipo de Comprobante");

        jLabel11.setText("Punto de Venta");

        txtnumerocbte.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnumerocbteKeyReleased(evt);
            }
        });

        jLabel12.setText("Número de Comprobante");

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Importes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel4.setText("Total no integran el Neto Gravado");

        txtnointegra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnointegraKeyReleased(evt);
            }
        });

        txtvaloragregado.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtvaloragregadoKeyReleased(evt);
            }
        });

        jLabel6.setText("Total");

        txttotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txttotalKeyReleased(evt);
            }
        });

        txtexentas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtexentasKeyReleased(evt);
            }
        });

        jLabel5.setText("Impuesto al Valor Agregado");

        jLabel9.setText("Operaciones Exentas");

        jLabel7.setText("Otros Impuestos Nacionales");

        txtimpuestonacionales.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtimpuestonacionalesKeyReleased(evt);
            }
        });

        jLabel8.setText("Ingresos Brutos");

        txtingresosbrutos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtingresosbrutosKeyReleased(evt);
            }
        });

        jLabel14.setText("Impuestos Municipales");

        txtimpuestosmunicipales.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtimpuestosmunicipalesKeyReleased(evt);
            }
        });

        jLabel15.setText("Impuestos Internos");

        txtimpuestosinternos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtimpuestosinternosKeyReleased(evt);
            }
        });

        jLabel18.setText("Otros Tributos");

        txtotrostributos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtotrostributosKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txtexentas, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                    .addComponent(txtnointegra, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txttotal, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtvaloragregado)
                    .addComponent(txtimpuestonacionales)
                    .addComponent(txtingresosbrutos)
                    .addComponent(txtimpuestosmunicipales)
                    .addComponent(txtimpuestosinternos)
                    .addComponent(txtotrostributos))
                .addGap(0, 79, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtnointegra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtexentas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtvaloragregado, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtimpuestonacionales, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtingresosbrutos, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtimpuestosmunicipales, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14))
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtimpuestosinternos, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtotrostributos, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addGap(6, 6, 6))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Vendedor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        jLabel10.setText("Proveedor");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addGap(18, 18, 18)
                .addComponent(cboproveedor, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cboproveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        fechacbte.setDateFormatString("dd-MM-yyyy");

        jLabel16.setText("Moneda");

        jLabel17.setText("Operacion");

        txtptoventa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtptoventaKeyReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(cbotipocbte, 0, 301, Short.MAX_VALUE)
                                    .addComponent(fechacbte, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtnumerocbte)
                                    .addComponent(txtptoventa, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(cbooperacion, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(cbotipomoneda, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(fechacbte, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbotipocbte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txtptoventa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtnumerocbte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(cbotipomoneda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(cbooperacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        //Chequea q no haya datos vacios
        if (txtptoventa.getText().equals("") || txtnumerocbte.getText().equals("") || txttotal.getText().equals("")
                || fechacbte.getDate() == null) {
            ///Ingresar todos los datos 
            JOptionPane.showMessageDialog(null, "No ingreso todos los datos de la factura");
        } else {
            //Datos entrada
            cboTipoCbte cbte = (cboTipoCbte) cbotipocbte.getSelectedItem();
            int IdCbte = cbte.getidcbte();

            cboProveedor doc = (cboProveedor) cboproveedor.getSelectedItem();
            int idProveedores = doc.getidProveedores();

            cboTipoMoneda moneda = (cboTipoMoneda) cbotipomoneda.getSelectedItem();
            int IdMoneda = moneda.getidMoneda();
            cboOperacion operacion = (cboOperacion) cbooperacion.getSelectedItem();
            int IdOperacion = operacion.getidOperacion();
            String fcbte = formato.format(fechacbte.getDate().getTime());
            double importenointegraprecioneto = 0.0;
            double importeoperacionesexentas = 0.0;
            double inportevaloragregado = 0.0;
            double importeimpuestosnacionales = 0.0;
            double importeingresosbrutos = 0.0;
            double importeimpuestosmunicipales = 0.0;
            double importeimpuestosinternos = 0.0;
            double otrostributos = 0.0;
            if (!"".equals(txtnointegra.getText())) {
                importenointegraprecioneto = Double.valueOf(txtnointegra.getText());
            }
            if (!"".equals(txtexentas.getText())) {
                importeoperacionesexentas = Double.valueOf(txtexentas.getText());
            }
            if (!"".equals(txtvaloragregado.getText())) {
                inportevaloragregado = Double.valueOf(txtvaloragregado.getText());
            }
            if (!"".equals(txtimpuestonacionales.getText())) {
                importeimpuestosnacionales = Double.valueOf(txtimpuestonacionales.getText());
            }
            if (!"".equals(txtingresosbrutos.getText())) {
                importeingresosbrutos = Double.valueOf(txtingresosbrutos.getText());
            }
            if (!"".equals(txtimpuestosmunicipales.getText())) {
                importeimpuestosmunicipales = Double.valueOf(txtimpuestosmunicipales.getText());
            }
            if (!"".equals(txtimpuestosinternos.getText())) {
                importeimpuestosinternos = Double.valueOf(txtimpuestosinternos.getText());
            }
            if (!"".equals(txtotrostributos.getText())) {
                otrostributos = Double.valueOf(txtotrostributos.getText());
            }

            ClaseIvaCompra IvaCompra = new ClaseIvaCompra();
            IvaCompra.AgregarIvaCompra(fcbte, IdCbte, Integer.valueOf(txtptoventa.getText()), txtnumerocbte.getText(),
                    idProveedores, Double.valueOf(txttotal.getText()),
                    importenointegraprecioneto, importeoperacionesexentas, inportevaloragregado, importeimpuestosnacionales,
                    importeingresosbrutos, importeimpuestosmunicipales, importeimpuestosinternos, otrostributos,
                    IdMoneda, IdOperacion);
            //Limpio Campos
            fnCancelar();

        }
    }//GEN-LAST:event_btnagregarActionPerformed

    private void txtnumerocbteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnumerocbteKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtnumerocbte.getText())) {
            txtnumerocbte.setText("");
        }
    }//GEN-LAST:event_txtnumerocbteKeyReleased

    private void txttotalKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txttotalKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txttotal.getText())) {
            txttotal.setText("");
        }
    }//GEN-LAST:event_txttotalKeyReleased

    private void txtnointegraKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnointegraKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtnointegra.getText())) {
            txtnointegra.setText("");
        }
    }//GEN-LAST:event_txtnointegraKeyReleased

    private void txtvaloragregadoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtvaloragregadoKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtvaloragregado.getText())) {
            txtvaloragregado.setText("");
        }
    }//GEN-LAST:event_txtvaloragregadoKeyReleased

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        fnCancelar();
    }//GEN-LAST:event_btncancelarActionPerformed

    private void txtexentasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtexentasKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtexentas.getText())) {
            txtexentas.setText("");
        }
    }//GEN-LAST:event_txtexentasKeyReleased

    private void txtimpuestonacionalesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtimpuestonacionalesKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtimpuestonacionales.getText())) {
            txtimpuestonacionales.setText("");
        }
    }//GEN-LAST:event_txtimpuestonacionalesKeyReleased

    private void txtingresosbrutosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtingresosbrutosKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtingresosbrutos.getText())) {
            txtingresosbrutos.setText("");
        }
    }//GEN-LAST:event_txtingresosbrutosKeyReleased

    private void txtimpuestosmunicipalesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtimpuestosmunicipalesKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtimpuestosmunicipales.getText())) {
            txtimpuestosmunicipales.setText("");
        }
    }//GEN-LAST:event_txtimpuestosmunicipalesKeyReleased

    private void txtimpuestosinternosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtimpuestosinternosKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtimpuestosinternos.getText())) {
            txtimpuestosinternos.setText("");
        }
    }//GEN-LAST:event_txtimpuestosinternosKeyReleased

    private void txtotrostributosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtotrostributosKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtotrostributos.getText())) {
            txtotrostributos.setText("");
        }
    }//GEN-LAST:event_txtotrostributosKeyReleased

    private void txtptoventaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtptoventaKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtptoventa.getText())) {
            txtptoventa.setText("");
        }
    }//GEN-LAST:event_txtptoventaKeyReleased
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnagregar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JComboBox cbooperacion;
    private javax.swing.JComboBox cboproveedor;
    private javax.swing.JComboBox cbotipocbte;
    private javax.swing.JComboBox cbotipomoneda;
    private com.toedter.calendar.JDateChooser fechacbte;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField txtexentas;
    private javax.swing.JTextField txtimpuestonacionales;
    private javax.swing.JTextField txtimpuestosinternos;
    private javax.swing.JTextField txtimpuestosmunicipales;
    private javax.swing.JTextField txtingresosbrutos;
    private javax.swing.JTextField txtnointegra;
    private javax.swing.JTextField txtnumerocbte;
    private javax.swing.JTextField txtotrostributos;
    private javax.swing.JTextField txtptoventa;
    private javax.swing.JTextField txttotal;
    private javax.swing.JTextField txtvaloragregado;
    // End of variables declaration//GEN-END:variables

}
