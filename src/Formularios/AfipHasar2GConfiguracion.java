package Formularios;

import Clases.ClaseAfipHasar2G;
import Clases.ClaseHasar;
import Clases.ConexionMySQL;
import Clases.ServiciosHasar;
import Clases.fnesNumerico;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class AfipHasar2GConfiguracion extends javax.swing.JDialog {

    public AfipHasar2GConfiguracion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtlimiteBC = new javax.swing.JTextField();
        btnObtener = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        btnConfigurar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtImpresionCambio = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txtImpresionLeyenda = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtCortarPapel = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();

        jLabel4.setText("jLabel4");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("LimiteBC B:");

        txtlimiteBC.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlimiteBCKeyReleased(evt);
            }
        });

        btnObtener.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnObtener.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnObtener.setText("Obtener");
        btnObtener.setMaximumSize(new java.awt.Dimension(120, 50));
        btnObtener.setMinimumSize(new java.awt.Dimension(120, 50));
        btnObtener.setPreferredSize(new java.awt.Dimension(120, 50));
        btnObtener.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnObtenerActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnSalir.setMnemonic('s');
        btnSalir.setText("Salir");
        btnSalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnSalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnSalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnConfigurar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnConfigurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnConfigurar.setText(" Configurar");
        btnConfigurar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnConfigurar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnConfigurar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnConfigurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfigurarActionPerformed(evt);
            }
        });

        jLabel3.setText("Monto máximo comprobantes 'B/C'");

        jLabel5.setText("B = '0' El comprobante no está limitado a un monto máximo");

        jLabel11.setText("B <> '0' El comprobante no puede superar el monto máximo indicado en este parámetro. ");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("ImpresionCambio C:");

        txtImpresionCambio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtImpresionCambioKeyReleased(evt);
            }
        });

        jLabel7.setText("C = 'P' Imprimir (valor por defecto).");

        jLabel8.setText("Impresión leyenda \"Cambio $0.00\". ");

        jLabel12.setText("C <> 'P' No imprimir. ");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel9.setText("ImpresionLeyendas L:");

        txtImpresionLeyenda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtImpresionLeyendaKeyReleased(evt);
            }
        });

        jLabel10.setText("Impresión de leyendas opcionales. ");

        jLabel13.setText("L = 'P' Imprimir (valor por defecto). ");

        jLabel14.setText("L <> 'P' No imprimir. ");

        jLabel15.setText("Opciones de corte de papel.");

        jLabel16.setText("P = 'F' Corte total. ");

        jLabel17.setText("P = 'P' Corte parcial (valor por defecto). ");

        txtCortarPapel.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCortarPapelKeyReleased(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel18.setText("CortePapel P:");

        jLabel19.setText("P = 'N' No cortar papel. ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGap(71, 71, 71)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtImpresionCambio, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtlimiteBC, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtImpresionLeyenda, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCortarPapel, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel11)
                            .addComponent(jLabel3)
                            .addComponent(jLabel7)
                            .addComponent(jLabel12)
                            .addComponent(jLabel8))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addComponent(jLabel14)
                            .addComponent(jLabel10)
                            .addComponent(jLabel16)
                            .addComponent(jLabel17)
                            .addComponent(jLabel15)
                            .addComponent(jLabel19))
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btnObtener, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnConfigurar, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtCortarPapel, txtImpresionCambio, txtImpresionLeyenda, txtlimiteBC});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel3)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlimiteBC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel2))
                .addGap(0, 0, 0)
                .addComponent(jLabel11)
                .addGap(30, 30, 30)
                .addComponent(jLabel8)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtImpresionCambio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6))
                .addGap(0, 0, 0)
                .addComponent(jLabel12)
                .addGap(30, 30, 30)
                .addComponent(jLabel10)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtImpresionLeyenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel9))
                .addGap(0, 0, 0)
                .addComponent(jLabel14)
                .addGap(30, 30, 30)
                .addComponent(jLabel15)
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCortarPapel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel16)
                    .addComponent(jLabel18))
                .addGap(0, 0, 0)
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel19)
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnObtener, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfigurar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnObtenerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnObtenerActionPerformed
        ClaseAfipHasar2G Hasar = new ClaseAfipHasar2G();
        String[] DatosConfig = Hasar.ObtenerConfiguracion();
        txtlimiteBC.setText(DatosConfig[0]);
        txtImpresionCambio.setText(DatosConfig[1]);
        txtImpresionLeyenda.setText(DatosConfig[2]);
        txtCortarPapel.setText(DatosConfig[3]);
    }//GEN-LAST:event_btnObtenerActionPerformed

    private void txtlimiteBCKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlimiteBCKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtlimiteBC.getText())) {
            txtlimiteBC.setText("");
        }
    }//GEN-LAST:event_txtlimiteBCKeyReleased

    private void btnConfigurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfigurarActionPerformed
        //EnviarDatos
        ClaseAfipHasar2G Hasar = new ClaseAfipHasar2G();
        Hasar.Configuracion(txtlimiteBC.getText(), txtImpresionCambio.getText(), txtImpresionLeyenda.getText(), txtCortarPapel.getText());
        
        //Obtener Datos
        
        String[] DatosConfig = Hasar.ObtenerConfiguracion();
        txtlimiteBC.setText(DatosConfig[0]);
        txtImpresionCambio.setText(DatosConfig[1]);
        txtImpresionLeyenda.setText(DatosConfig[2]);
        txtCortarPapel.setText(DatosConfig[3]);
    }//GEN-LAST:event_btnConfigurarActionPerformed

    private void txtImpresionCambioKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtImpresionCambioKeyReleased
        String texto = txtImpresionCambio.getText().toUpperCase();
        txtImpresionCambio.setText(texto);
    }//GEN-LAST:event_txtImpresionCambioKeyReleased

    private void txtImpresionLeyendaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtImpresionLeyendaKeyReleased
        String texto = txtImpresionLeyenda.getText().toUpperCase();
        txtImpresionLeyenda.setText(texto);
    }//GEN-LAST:event_txtImpresionLeyendaKeyReleased

    private void txtCortarPapelKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCortarPapelKeyReleased
        String texto = txtCortarPapel.getText().toUpperCase();
        txtCortarPapel.setText(texto);
    }//GEN-LAST:event_txtCortarPapelKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConfigurar;
    private javax.swing.JButton btnObtener;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txtCortarPapel;
    private javax.swing.JTextField txtImpresionCambio;
    private javax.swing.JTextField txtImpresionLeyenda;
    private javax.swing.JTextField txtlimiteBC;
    // End of variables declaration//GEN-END:variables
}
