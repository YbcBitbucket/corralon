package Formularios;

import Clases.ConexionMySQL;
import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;

import java.sql.Connection;
import java.sql.PreparedStatement;


import javax.swing.JOptionPane;

public class HasarPropiedades extends javax.swing.JDialog {

    public HasarPropiedades(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        cbotransporte = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        cbobaudios = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtdireccionip = new javax.swing.JTextField();
        btnAgregar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        cbopuerto = new javax.swing.JComboBox<>();

        jLabel4.setText("jLabel4");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setText("Transporte:");

        cbotransporte.setMaximumRowCount(3);
        cbotransporte.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PUERTO_SERIE", "SOCKET_TCP", "SOCKET_UDP" }));
        cbotransporte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbotransporteActionPerformed(evt);
            }
        });

        jLabel2.setText("Baudios:");

        cbobaudios.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1200", "2400", "4800", "9600", "19200", "38400", "57600", "115200" }));
        cbobaudios.setSelectedIndex(3);
        cbobaudios.setEnabled(false);

        jLabel3.setText("Puerto:");

        jLabel5.setText("Direccion IP:");

        txtdireccionip.setEnabled(false);

        btnAgregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Agregar.png"))); // NOI18N
        btnAgregar.setText("Agregar");
        btnAgregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnAgregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnAgregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnSalir.setMnemonic('s');
        btnSalir.setText("Salir");
        btnSalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnSalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnSalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        cbopuerto.setMaximumRowCount(10);
        cbopuerto.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Puerto 0", "Puerto 1", "Puerto 2", "Puerto 3", "Puerto 4", "Puerto 5", "Puerto 6", "Puerto 7", "Puerto 8", "Puerto 9" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbotransporte, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbopuerto, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtdireccionip, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbobaudios, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(95, 95, 95)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cbotransporte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbopuerto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cbobaudios, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtdireccionip, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbotransporteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbotransporteActionPerformed
        if (cbotransporte.getSelectedIndex() == 0) {
            cbopuerto.enable(true);
            cbobaudios.enable(true);
            txtdireccionip.enable(false);
        } else {
            cbopuerto.enable(false);
            cbobaudios.enable(false);
            txtdireccionip.enable(true);
        }
    }//GEN-LAST:event_cbotransporteActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement AgregaPropiedades = null;
        String sSQL = "";

        try {
            ActiveXComponent hasar = new ActiveXComponent("hasar.fiscal.1");
            Dispatch sControl = hasar.getObject();
            Dispatch.put(sControl, "Transporte", cbotransporte.getSelectedIndex());
            if (cbotransporte.getSelectedIndex() == 0) {
                Dispatch.put(sControl, "Puerto", cbopuerto.getSelectedIndex());
                Dispatch.put(sControl, "Baudios", Integer.valueOf(cbobaudios.getSelectedItem().toString()));
                Dispatch.call(sControl, "Comenzar");
                
                sSQL = "INSERT INTO hasar_propiedades (transporte, puerto, baudios) VALUES(?,?,?)";
                AgregaPropiedades = cn.prepareStatement(sSQL);
                AgregaPropiedades.setInt(1, cbotransporte.getSelectedIndex());
                AgregaPropiedades.setInt(2, cbopuerto.getSelectedIndex());
                AgregaPropiedades.setInt(3, Integer.valueOf(cbobaudios.getSelectedItem().toString()));

                int n = AgregaPropiedades.executeUpdate();

                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                }
            } else {
                Dispatch.put(sControl, "Puerto", cbopuerto.getSelectedIndex());
                Dispatch.put(sControl, "DireccionIP", txtdireccionip.getText());
                Dispatch.call(sControl, "Comenzar");

                sSQL = "INSERT INTO hasar_propiedades (transporte, puerto, direccionip) VALUES(?,?,?)";
                AgregaPropiedades = cn.prepareStatement(sSQL);
                AgregaPropiedades.setInt(1, cbopuerto.getSelectedIndex());
                AgregaPropiedades.setInt(2, cbopuerto.getSelectedIndex());
                AgregaPropiedades.setString(3, txtdireccionip.getText());

                int n = AgregaPropiedades.executeUpdate();

                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (AgregaPropiedades != null) {
                    AgregaPropiedades.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }//GEN-LAST:event_btnAgregarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox<String> cbobaudios;
    private javax.swing.JComboBox<String> cbopuerto;
    private javax.swing.JComboBox<String> cbotransporte;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JTextField txtdireccionip;
    // End of variables declaration//GEN-END:variables
}
