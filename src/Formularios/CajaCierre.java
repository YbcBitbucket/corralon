package Formularios;

import Clases.ClaseCaja;
import Clases.ConexionMySQL;
import Clases.fnEscape;
import Clases.fnRedondear;
import Clases.fnesNumerico;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class CajaCierre extends javax.swing.JDialog {
int idCaja;
    public CajaCierre(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        fnEscape.funcionescape(this);
        cargar();
    }

    void cargar() {        
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectCaja = null;
        Statement SelectPedidos = null;
        Statement SelectEgresos = null;

        String sql = "SELECT * FROM caja";

        double totalPedidosContado = 0.00,totalPedidosTarjeta = 0.00, totalEgresos = 0.00, totalapertura = 0.00,totalcuentacorriete=0.0,totaltrasnferencias=0.0;
        try {
            ////Apertura
            SelectCaja = cn.createStatement();
            ResultSet rsCaja = SelectCaja.executeQuery(sql);
            rsCaja.last();
            totalapertura = rsCaja.getDouble("total");
            System.out.println("totalapertura " + totalapertura);
            idCaja = rsCaja.getInt("idCaja");
            System.out.println("id caja " + idCaja);
            //Ingresos por Pedidos contado
            String sql1 = "SELECT * FROM vista_pedidos WHERE idCaja=" + idCaja +" and (formadepago='CONTADO' or formadepago='CHEQUE' or formadepago='OTROS')";
            SelectPedidos = cn.createStatement();
            ResultSet rsPedidosContado = SelectPedidos.executeQuery(sql1);
            while (rsPedidosContado.next()) {
                    totalPedidosContado = totalPedidosContado + rsPedidosContado.getDouble("total");                    
            }
            System.out.println("totalPedidosContado "+totalPedidosContado);
            //Ingresos por Pedidos PAGO DE CTA CTE
            String sql5 = "SELECT haber FROM vista_ctacte_detalle WHERE id_Caja=" + idCaja ;
            SelectPedidos = cn.createStatement();
            ResultSet rsPedidosPagosCC = SelectPedidos.executeQuery(sql5);
            while (rsPedidosPagosCC.next()) {
                    totalPedidosContado = totalPedidosContado + rsPedidosPagosCC.getDouble(1);                    
            }
            System.out.println("totalPedidosContado con cc "+totalPedidosContado);
            //Ingresos por Pedidos tarjeta
            String sql3 = "SELECT * FROM vista_pedidos WHERE idCaja=" + idCaja +" and (formadepago='TARJETA DE CREDITO' or formadepago='TARJETA DE DEBITO')";
            SelectPedidos = cn.createStatement();
            ResultSet rsPedidosTarjeta = SelectPedidos.executeQuery(sql3);
            while (rsPedidosTarjeta.next()) {
                    totalPedidosTarjeta = totalPedidosTarjeta + rsPedidosTarjeta.getDouble("total");
            }
            System.out.println("totalPedidosTarjeta "+totalPedidosTarjeta);
            //Ingresos por Pedidos cuenta corriente
            String sql4 = "SELECT * FROM vista_pedidos WHERE idCaja=" + idCaja +" and (formadepago='CUENTA CORRIENTE')";
            SelectPedidos = cn.createStatement();
            ResultSet rsPedidoscc = SelectPedidos.executeQuery(sql4);
            while (rsPedidoscc.next()) {
                    totalcuentacorriete = totalcuentacorriete + rsPedidoscc.getDouble("total");                    
            }
             System.out.println("totalcuentacorriete "+totalcuentacorriete);
            //Ingresos por Pedidos trasnferencias
            String sql6 = "SELECT * FROM vista_pedidos WHERE idCaja=" + idCaja +" and (formadepago='TRANSFERENCIA')";
            SelectPedidos = cn.createStatement();
            ResultSet rsPedidosTransferencias = SelectPedidos.executeQuery(sql6);
            while (rsPedidosTransferencias.next()) {
                    totaltrasnferencias = totaltrasnferencias + rsPedidosTransferencias.getDouble("total");
            }
            System.out.println("totaltrasnferencias "+totaltrasnferencias); 
            
            //Egresos
            String sql2 = "SELECT * FROM egresos WHERE idCaja=" + idCaja;
            SelectEgresos = cn.createStatement();
            ResultSet rsEgresos = SelectEgresos.executeQuery(sql2);
            while (rsEgresos.next()) {
                totalEgresos = totalEgresos + rsEgresos.getDouble("total");;
            }
            System.out.println("totalEgresos "+totalEgresos);
            fnRedondear redondear=new fnRedondear();
            txtapertura.setText(rsCaja.getString("total"));
            
            txtigresosEfectivo.setText(String.valueOf(redondear.dosDigitos(totalPedidosContado)));
            txtingresosTarjeta.setText(String.valueOf(redondear.dosDigitos(totalPedidosTarjeta)));
            txtingresoscc.setText(String.valueOf(redondear.dosDigitos(totalcuentacorriete)));
            txttransferencias.setText(String.valueOf(redondear.dosDigitos(totaltrasnferencias)));
            txtegresos.setText(String.valueOf(redondear.dosDigitos(totalEgresos)));            
            txttotal.setText(String.valueOf(redondear.dosDigitos(totalapertura + redondear.dosDigitos(totalPedidosContado) - redondear.dosDigitos(totalEgresos))));
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCaja != null) {
                    SelectCaja.close();
                }
                if (SelectPedidos != null) {
                    SelectPedidos.close();
                }
                if (SelectEgresos != null) {
                    SelectEgresos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btncancelar = new javax.swing.JButton();
        txtapertura = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnaceptar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtegresos = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txttotal = new javax.swing.JLabel();
        txtdiferencia = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtigresosEfectivo = new javax.swing.JLabel();
        txtingresosTarjeta = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtingresoscc = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        txttransferencias = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cierre");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cierre de Caja Total", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 102))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Apertura de Caja $:");

        btncancelar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Cancelar.png"))); // NOI18N
        btncancelar.setMnemonic('c');
        btncancelar.setText("Cancelar");
        btncancelar.setMaximumSize(new java.awt.Dimension(120, 50));
        btncancelar.setMinimumSize(new java.awt.Dimension(120, 50));
        btncancelar.setPreferredSize(new java.awt.Dimension(120, 50));
        btncancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelarActionPerformed(evt);
            }
        });

        txtapertura.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtapertura.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel4.setText("Diferencia $:");

        btnaceptar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnaceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnaceptar.setMnemonic('a');
        btnaceptar.setText("Aceptar");
        btnaceptar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnaceptar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnaceptar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnaceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnaceptarActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(204, 0, 0));
        jLabel6.setText("Egresos Total: $");

        txtegresos.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtegresos.setForeground(new java.awt.Color(204, 0, 0));
        txtegresos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 204, 51));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("TOTAL: $");

        txttotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txttotal.setForeground(new java.awt.Color(0, 204, 51));
        txttotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        txtdiferencia.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        txtdiferencia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdiferenciaKeyReleased(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 102, 255));
        jLabel2.setText("Ingresos Efectivo $: ");

        txtigresosEfectivo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtigresosEfectivo.setForeground(new java.awt.Color(0, 102, 255));
        txtigresosEfectivo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        txtingresosTarjeta.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtingresosTarjeta.setForeground(new java.awt.Color(0, 153, 153));
        txtingresosTarjeta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 153, 153));
        jLabel5.setText("Ventas Tarjeta $:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 102, 0));
        jLabel8.setText("Ventas CC $:");

        txtingresoscc.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtingresoscc.setForeground(new java.awt.Color(255, 102, 0));
        txtingresoscc.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(204, 204, 0));
        jLabel9.setText("Trasnferencias: $:");

        txttransferencias.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txttransferencias.setForeground(new java.awt.Color(204, 204, 0));
        txttransferencias.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtingresoscc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txttransferencias, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel5)
                            .addGap(19, 19, 19)
                            .addComponent(txtingresosTarjeta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 161, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(50, 50, 50)
                                    .addComponent(jLabel4)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtdiferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtegresos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtapertura, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(18, 18, 18)
                                        .addComponent(txtigresosEfectivo, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGap(0, 0, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtingresosTarjeta, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txttransferencias, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtingresoscc, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtapertura, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtigresosEfectivo, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtegresos, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txttotal, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtdiferencia, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btncancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnaceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(47, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btncancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btncancelarActionPerformed

    private void btnaceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnaceptarActionPerformed
        if ("".equals(txtdiferencia.getText())) {
            JOptionPane.showMessageDialog(null, "No ingreso la diferencia");
        } else {
            ClaseCaja caja = new ClaseCaja();
            System.out.println(Double.valueOf(txtdiferencia.getText()));
            caja.CajaCierre(String.valueOf(idCaja),txtapertura.getText(), txtigresosEfectivo.getText(), txtingresosTarjeta.getText(),txtingresoscc.getText(), txtegresos.getText(),Double.valueOf(txtdiferencia.getText()), Double.valueOf(txttotal.getText()));
            this.dispose();
        }
    }//GEN-LAST:event_btnaceptarActionPerformed

    private void txtdiferenciaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdiferenciaKeyReleased
        
    }//GEN-LAST:event_txtdiferenciaKeyReleased

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnaceptar;
    private javax.swing.JButton btncancelar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel txtapertura;
    private javax.swing.JTextField txtdiferencia;
    private javax.swing.JLabel txtegresos;
    private javax.swing.JLabel txtigresosEfectivo;
    private javax.swing.JLabel txtingresosTarjeta;
    private javax.swing.JLabel txtingresoscc;
    private javax.swing.JLabel txttotal;
    private javax.swing.JLabel txttransferencias;
    // End of variables declaration//GEN-END:variables
}
