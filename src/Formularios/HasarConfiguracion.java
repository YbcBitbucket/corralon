package Formularios;

import Clases.ClaseHasar;
import Clases.ConexionMySQL;
import Clases.ServiciosHasar;
import Clases.fnesNumerico;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

public class HasarConfiguracion extends javax.swing.JDialog {

    public HasarConfiguracion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargardatos();
    }

    void cargardatos() {
        String sql = "SELECT * FROM hasar_configuracion";

        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectHasar = null;
        try {
            SelectHasar = cn.createStatement();
            ResultSet rsHasar = SelectHasar.executeQuery(sql);
            rsHasar.next();

            txtlimitecf.setText(rsHasar.getString("limitecf"));
            txtlimitetf.setText(rsHasar.getString("limitetf"));

            if (rsHasar.getInt("copias") == 48) {
                cbocopias.setSelectedIndex(0);
            }
            if (rsHasar.getInt("copias") == 49) {
                cbocopias.setSelectedIndex(1);
            }
            if (rsHasar.getInt("copias") == 50) {
                cbocopias.setSelectedIndex(2);
            }
            if (rsHasar.getInt("copias") == 51) {
                cbocopias.setSelectedIndex(3);
            }
            if (rsHasar.getInt("copias") == 52) {
                cbocopias.setSelectedIndex(4);
            }

            cbocambio.setSelectedIndex(rsHasar.getInt("cambio"));
            cboleyendas.setSelectedIndex(rsHasar.getInt("leyendas"));

            if (rsHasar.getInt("corte") == 80) {
                cbocorte.setSelectedIndex(0);
            }
            if (rsHasar.getInt("corte") == 70) {
                cbocorte.setSelectedIndex(1);
            }
            if (rsHasar.getInt("corte") == 78) {
                cbocorte.setSelectedIndex(2);
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectHasar != null) {
                    SelectHasar.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtlimitecf = new javax.swing.JTextField();
        btnObtener = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        txtlimitetf = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        cbocopias = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        cbocambio = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        cboleyendas = new javax.swing.JComboBox<>();
        btnConfigurar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        cbocorte = new javax.swing.JComboBox<>();

        jLabel4.setText("jLabel4");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel2.setText("Limite Consumidor Final:");

        txtlimitecf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlimitecfKeyReleased(evt);
            }
        });

        btnObtener.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnObtener.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnObtener.setText("Obtener");
        btnObtener.setMaximumSize(new java.awt.Dimension(120, 50));
        btnObtener.setMinimumSize(new java.awt.Dimension(120, 50));
        btnObtener.setPreferredSize(new java.awt.Dimension(120, 50));
        btnObtener.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnObtenerActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnSalir.setMnemonic('s');
        btnSalir.setText("Salir");
        btnSalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnSalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnSalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        jLabel6.setText("Limite Tiques Fiscal:");

        txtlimitetf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtlimitetfKeyReleased(evt);
            }
        });

        jLabel7.setText("Copias:");

        cbocopias.setMaximumRowCount(10);
        cbocopias.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "NO_COPIAS", "ORIGINAL", "DUPLICADO", "TRIPLICADO", "CUADRUPLICADO" }));

        jLabel8.setText("Cambio:");

        cbocambio.setMaximumRowCount(2);
        cbocambio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Imprimir leyenda “CAMBIO $0.00\"", "No imprimirla" }));

        jLabel9.setText("Leyenda:");

        cboleyendas.setMaximumRowCount(2);
        cboleyendas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Imprimir leyenda opcionales", "No imprimir" }));

        btnConfigurar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnConfigurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Entrar.png"))); // NOI18N
        btnConfigurar.setText(" Configurar");
        btnConfigurar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnConfigurar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnConfigurar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnConfigurar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfigurarActionPerformed(evt);
            }
        });

        jLabel10.setText("Corte:");

        cbocorte.setMaximumRowCount(10);
        cbocorte.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "CORTE_PARCIAL", "CORTE_TOTAL", "NO_CORTE" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnObtener, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnConfigurar, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                        .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel6))
                                .addGap(19, 19, 19)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtlimitetf, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtlimitecf, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(134, 134, 134)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(cboleyendas, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(cbocambio, javax.swing.GroupLayout.PREFERRED_SIZE, 192, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(cbocopias, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(cbocorte, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlimitecf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtlimitetf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(cbocopias, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(cbocambio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(cboleyendas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(cbocorte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 51, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnObtener, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfigurar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnObtenerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnObtenerActionPerformed
        String sql2 = "SELECT * FROM hasar_propiedades";
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectHasar = null;
        try {
            SelectHasar = cn.createStatement();
            ResultSet rsHasar = SelectHasar.executeQuery(sql2);
            rsHasar.last();
            ServiciosHasar hasar = new ServiciosHasar();

            if (!"ERROR".equals(hasar.Comenzar(rsHasar.getInt("transporte"), rsHasar.getInt("puerto"), rsHasar.getInt("baudios"), rsHasar.getString("direccionip")))) {
                hasar.TratarDeCancelarTodo();
                hasar.ObtenerConfiguracion();
                String Datos = "R : " + hasar.Respuesta(0);

                JOptionPane.showMessageDialog(null, Datos);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }

    }//GEN-LAST:event_btnObtenerActionPerformed

    private void txtlimitecfKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlimitecfKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtlimitecf.getText())) {
            txtlimitecf.setText("");
        }
    }//GEN-LAST:event_txtlimitecfKeyReleased

    private void txtlimitetfKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtlimitetfKeyReleased
        fnesNumerico num = new fnesNumerico();
        if (!num.isNumeric(txtlimitetf.getText())) {
            txtlimitetf.setText("");
        }
    }//GEN-LAST:event_txtlimitetfKeyReleased

    private void btnConfigurarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfigurarActionPerformed
        Double limitecf = Double.valueOf(txtlimitecf.getText());
        Double limitetf = Double.valueOf(txtlimitetf.getText());

        int copias = 0;
        if (cbocopias.getSelectedIndex() == 0) {
            copias = 48;
        }
        if (cbocopias.getSelectedIndex() == 1) {
            copias = 49;
        }
        if (cbocopias.getSelectedIndex() == 2) {
            copias = 50;
        }
        if (cbocopias.getSelectedIndex() == 3) {
            copias = 51;
        }
        if (cbocopias.getSelectedIndex() == 4) {
            copias = 52;
        }

        boolean cambio;
        if (cbocambio.getSelectedIndex() == 0) {
            cambio = true;
        } else {
            cambio = false;
        }

        boolean leyendas;
        if (cboleyendas.getSelectedIndex() == 0) {
            leyendas = true;
        } else {
            leyendas = false;
        }

        int corte = 0;
        if (cbocorte.getSelectedIndex() == 0) {
            corte = 80;
        }
        if (cbocorte.getSelectedIndex() == 1) {
            corte = 70;
        }
        if (cbocorte.getSelectedIndex() == 2) {
            corte = 78;
        }

        ClaseHasar hasar=new ClaseHasar();
        try {
            hasar.Configuracion(limitecf, limitetf, copias, cambio, leyendas, corte);
            cargardatos();
        } catch (SQLException ex) {
            //Logger.getLogger(HasarConfiguracion.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, ex);
        }
                
    }//GEN-LAST:event_btnConfigurarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnConfigurar;
    private javax.swing.JButton btnObtener;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox<String> cbocambio;
    private javax.swing.JComboBox<String> cbocopias;
    private javax.swing.JComboBox<String> cbocorte;
    private javax.swing.JComboBox<String> cboleyendas;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txtlimitecf;
    private javax.swing.JTextField txtlimitetf;
    // End of variables declaration//GEN-END:variables
}
