package Formularios;

import Clases.ConexionMySQL;
import Clases.fnAlinear;
import Clases.fnExportar;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

public class Clientes extends javax.swing.JDialog {

    DefaultTableModel model;
    public static int id_cliente;

    public Clientes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargartabla("");
        dobleclick();
    }

    //////////////////////FUNCION CARGAR TABLA //////////////////////
    void cargartabla(String valor) {
        String[] Titulo = {"id", "Razon Social/Nombre", "Resposabilidad IVA", "Tipo Doc", "Documento", "Margen"};
        String[] Registros = new String[8];
        String sql = "SELECT * FROM vista_clientes WHERE CONCAT(IvaDocumento, ' ', nombre)LIKE '%" + valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectClientes=null;
        try {
            SelectClientes = cn.createStatement();
            ResultSet rs = SelectClientes.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = rs.getString(1);
                Registros[1] = rs.getString(2);
                Registros[2] = rs.getString(3);
                Registros[3] = rs.getString(4);
                Registros[4] = rs.getString(5);
                Registros[5] = rs.getString(6);
                model.addRow(Registros);
            }
            tablaclientes.setModel(model);
            tablaclientes.setAutoCreateRowSorter(true);
            fnAlinear alinear = new fnAlinear();
            tablaclientes.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearIzquierda());
            tablaclientes.getColumnModel().getColumn(4).setCellRenderer(alinear.alinearDerecha());
            tablaclientes.getColumnModel().getColumn(5).setCellRenderer(alinear.alinearDerecha());
            tablaclientes.getColumnModel().getColumn(0).setMaxWidth(0);
            tablaclientes.getColumnModel().getColumn(0).setMinWidth(0);
            tablaclientes.getColumnModel().getColumn(0).setPreferredWidth(0);
            tablaclientes.getColumnModel().getColumn(1).setPreferredWidth(250);
            tablaclientes.getColumnModel().getColumn(2).setPreferredWidth(200);
            tablaclientes.getColumnModel().getColumn(3).setPreferredWidth(100);
            tablaclientes.getColumnModel().getColumn(4).setPreferredWidth(150);
            tablaclientes.getColumnModel().getColumn(5).setPreferredWidth(100);
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectClientes != null) {
                    SelectClientes.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION DOBLE CLICK //////////////////////
    void dobleclick() {
        tablaclientes.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    int filasel = tablaclientes.getSelectedRow();
                    if (filasel == -1) {
                        JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
                    } else {
                        id_cliente = Integer.valueOf(tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 0).toString());
                        new ClientesModifica(null, true).setVisible(true);
                        cargartabla("");
                    }
                }
            }
        });
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtbuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaclientes = new javax.swing.JTable();
        buscar = new javax.swing.JLabel();
        btnmodificar = new javax.swing.JButton();
        btnsalir = new javax.swing.JButton();
        btnagregar = new javax.swing.JButton();
        btnexportar = new javax.swing.JButton();
        btndetalle = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        tablaclientes.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaclientesKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(tablaclientes);

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6))
        );

        btnmodificar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnmodificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Modificar.png"))); // NOI18N
        btnmodificar.setText("Modificar");
        btnmodificar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnmodificar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnmodificar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnmodificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmodificarActionPerformed(evt);
            }
        });

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        btnagregar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnagregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Agregar.png"))); // NOI18N
        btnagregar.setText("Agregar");
        btnagregar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnagregar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnagregar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnagregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarActionPerformed(evt);
            }
        });

        btnexportar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnexportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Descargar.png"))); // NOI18N
        btnexportar.setText("Exportar");
        btnexportar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnexportar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnexportar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnexportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexportarActionPerformed(evt);
            }
        });

        btndetalle.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btndetalle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Documento.png"))); // NOI18N
        btndetalle.setText("Detalle");
        btndetalle.setMaximumSize(new java.awt.Dimension(120, 50));
        btndetalle.setMinimumSize(new java.awt.Dimension(120, 50));
        btndetalle.setPreferredSize(new java.awt.Dimension(120, 50));
        btndetalle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndetalleActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btndetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 138, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btndetalle, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnmodificar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnagregar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        String texto = txtbuscar.getText().toUpperCase();
        txtbuscar.setText(texto);
        cargartabla(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnagregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarActionPerformed
        new ClientesAgrega(null, true).setVisible(true);
        cargartabla("");
    }//GEN-LAST:event_btnagregarActionPerformed

    private void btnmodificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmodificarActionPerformed
        int filasel = tablaclientes.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            id_cliente = Integer.valueOf(tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 0).toString());
            new ClientesModifica(null, true).setVisible(true);
            cargartabla("");
        }
    }//GEN-LAST:event_btnmodificarActionPerformed

    private void tablaclientesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaclientesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_TAB) {
            transferFocus();
            evt.consume();
        }
    }//GEN-LAST:event_tablaclientesKeyPressed

    private void btnexportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexportarActionPerformed
        if (this.tablaclientes.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "La tabla está vacía");
            return;
        }
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Guardar Archivo");
        chooser.setMultiSelectionEnabled(false);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            List<JTable> tb = new ArrayList<>();
            List<String> nom = new ArrayList<>();
            tb.add(tablaclientes);
            nom.add("Tabla Clientes");
            String archivo = chooser.getSelectedFile().toString().concat(".xls");
            try {
                Clases.fnExportar e = new fnExportar(new File(archivo), tb, nom);
                if (e.export()) {
                    JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente", "", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_btnexportarActionPerformed

    private void btndetalleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndetalleActionPerformed
        int filasel = tablaclientes.getSelectedRow();
        if (filasel == -1) {
            JOptionPane.showMessageDialog(null, "No se ha seleccionado ninguna Fila");
        } else {
            id_cliente = Integer.valueOf(tablaclientes.getValueAt(tablaclientes.getSelectedRow(), 0).toString());
            new ClientesDetalle(null, true).setVisible(true);
            cargartabla("");
        }
    }//GEN-LAST:event_btndetalleActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnagregar;
    private javax.swing.JButton btndetalle;
    private javax.swing.JButton btnexportar;
    private javax.swing.JButton btnmodificar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel buscar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaclientes;
    private javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables
}
