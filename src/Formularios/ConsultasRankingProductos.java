package Formularios;

import Clases.ConexionMySQL;
import Clases.fnAlinear;
import Clases.fnEditarCeldas;
import Clases.fnExportar;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

public class ConsultasRankingProductos extends javax.swing.JDialog {

    DefaultTableModel model;

    public ConsultasRankingProductos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/icono.png")).getImage());
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        cargartabla("");
        cargardatoscategoria();
        cargardatosmarca();
    }

    //////////////////////FUNCION CARGAR CATEGORIA //////////////////////
    void cargardatoscategoria() {
        String sSQL = "";
        String cat = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectCategoria = null;
        sSQL = "SELECT nombre FROM categorias";
        cbocategorias.removeAllItems();
        cbocategorias.addItem("Categorias");
        try {
            SelectCategoria = cn.createStatement();
            ResultSet rs = SelectCategoria.executeQuery(sSQL);
            while (rs.next()) {
                cat = rs.getString("nombre");
                cbocategorias.addItem(cat);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCategoria != null) {
                    SelectCategoria.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION CARGAR MARCA //////////////////////
    void cargardatosmarca() {
        String sSQL = "";
        String mar = "";
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectMarca = null;
        sSQL = "SELECT nombre FROM marcas";
        cbomarcas.removeAllItems();
        cbomarcas.addItem("Marcas");
        try {
            SelectMarca = cn.createStatement();
            ResultSet rs = SelectMarca.executeQuery(sSQL);
            while (rs.next()) {
                mar = rs.getString("nombre");
                cbomarcas.addItem(mar);
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectMarca != null) {
                    SelectMarca.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION CARGAR TABLA //////////////////////
    void cargartabla(String valor) {
        String[] Titulo = {"Puesto", "Codigo", "Nombre", "Acumulado"};
        String[] Registros = new String[4];
        String sql = "SELECT * FROM vista_producto_ranking "
                + "WHERE CONCAT (codigo, ' ', producto) LIKE " + "'%" + valor + "%'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProductos = null;
        try {
            int puesto = 1;
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = String.valueOf(puesto);
                Registros[1] = rs.getString(1);
                Registros[2] = rs.getString(2);
                Registros[3] = rs.getString(3);
                puesto++;
                model.addRow(Registros);
            }
            tablaproductos.setModel(model);
            tablaproductos.setAutoCreateRowSorter(true);

            /////////////////////////////////////////////////////////////////
            fnAlinear alinear = new fnAlinear();
            tablaproductos.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablaproductos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearCentro());
            ////////////////////////////////////////////////////////////////////
            tablaproductos.getColumnModel().getColumn(0).setPreferredWidth(30);
            tablaproductos.getColumnModel().getColumn(1).setPreferredWidth(70);
            tablaproductos.getColumnModel().getColumn(2).setPreferredWidth(270);
            tablaproductos.getColumnModel().getColumn(3).setPreferredWidth(70);
            tablaproductos.getColumnModel().getColumn(3).setCellRenderer(new fnEditarCeldas());
            ////////////////////////////////////////////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION FILTRAR CATEGORIA//////////////////////
    void filtrarcategoria(String valor) {
        String[] Titulo = {"Puesto", "Codigo", "Nombre", "Acumulado"};
        String[] Registros = new String[4];
        String sql = "SELECT * FROM vista_producto_ranking "
                + "WHERE categoria = '" + valor + "'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProductos = null;
        try {
            int puesto = 1;
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = String.valueOf(puesto);
                Registros[1] = rs.getString(1);
                Registros[2] = rs.getString(2);
                Registros[3] = rs.getString(3);
                puesto++;
                model.addRow(Registros);
            }
            tablaproductos.setModel(model);
            tablaproductos.setAutoCreateRowSorter(true);

            /////////////////////////////////////////////////////////////////
            fnAlinear alinear = new fnAlinear();
            tablaproductos.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablaproductos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearCentro());
            ////////////////////////////////////////////////////////////////////
            tablaproductos.getColumnModel().getColumn(0).setPreferredWidth(30);
            tablaproductos.getColumnModel().getColumn(1).setPreferredWidth(70);
            tablaproductos.getColumnModel().getColumn(2).setPreferredWidth(270);
            tablaproductos.getColumnModel().getColumn(3).setPreferredWidth(70);
            tablaproductos.getColumnModel().getColumn(3).setCellRenderer(new fnEditarCeldas());
            ////////////////////////////////////////////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    //////////////////////FUNCION FILTRAR MARCA//////////////////////
    void filtrarmarca(String valor) {
        String[] Titulo = {"Puesto", "Codigo", "Nombre", "Acumulado"};
        String[] Registros = new String[4];
        String sql = "SELECT * FROM vista_producto_ranking "
                + "WHERE marca = '" + valor + "'";
        model = new DefaultTableModel(null, Titulo) {
            ////Celdas no editables////////
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        ConexionMySQL cc = new ConexionMySQL();
        Connection cn = cc.Conectar();
        Statement SelectProductos = null;
        try {
            int puesto = 1;
            SelectProductos = cn.createStatement();
            ResultSet rs = SelectProductos.executeQuery(sql);
            while (rs.next()) {
                Registros[0] = String.valueOf(puesto);
                Registros[1] = rs.getString(1);
                Registros[2] = rs.getString(2);
                Registros[3] = rs.getString(3);
                puesto++;
                model.addRow(Registros);
            }
            tablaproductos.setModel(model);
            tablaproductos.setAutoCreateRowSorter(true);

            /////////////////////////////////////////////////////////////////
            fnAlinear alinear = new fnAlinear();
            tablaproductos.getColumnModel().getColumn(0).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(1).setCellRenderer(alinear.alinearCentro());
            tablaproductos.getColumnModel().getColumn(2).setCellRenderer(alinear.alinearIzquierda());
            tablaproductos.getColumnModel().getColumn(3).setCellRenderer(alinear.alinearCentro());
            ////////////////////////////////////////////////////////////////////
            tablaproductos.getColumnModel().getColumn(0).setPreferredWidth(30);
            tablaproductos.getColumnModel().getColumn(1).setPreferredWidth(70);
            tablaproductos.getColumnModel().getColumn(2).setPreferredWidth(270);
            tablaproductos.getColumnModel().getColumn(3).setPreferredWidth(70);
            tablaproductos.getColumnModel().getColumn(3).setCellRenderer(new fnEditarCeldas());
            ////////////////////////////////////////////
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectProductos != null) {
                    SelectProductos.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        txtbuscar = new javax.swing.JTextField();
        buscar = new javax.swing.JLabel();
        cbocategorias = new javax.swing.JComboBox();
        cbomarcas = new javax.swing.JComboBox();
        btnsalir = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaproductos = new javax.swing.JTable();
        btnexportar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Buscar", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(102, 102, 102))); // NOI18N
        jPanel1.setOpaque(false);

        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Lupa.png"))); // NOI18N

        cbocategorias.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cbocategorias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbocategoriasActionPerformed(evt);
            }
        });

        cbomarcas.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cbomarcas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbomarcasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(buscar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cbocategorias, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(cbomarcas, javax.swing.GroupLayout.PREFERRED_SIZE, 199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtbuscar, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(buscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbomarcas)
                    .addComponent(cbocategorias, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(10, 10, 10))
        );

        btnsalir.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnsalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Salir.png"))); // NOI18N
        btnsalir.setMnemonic('s');
        btnsalir.setText("Salir");
        btnsalir.setMaximumSize(new java.awt.Dimension(120, 50));
        btnsalir.setMinimumSize(new java.awt.Dimension(120, 50));
        btnsalir.setPreferredSize(new java.awt.Dimension(120, 50));
        btnsalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsalirActionPerformed(evt);
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Productos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 11), new java.awt.Color(153, 153, 153))); // NOI18N

        tablaproductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tablaproductos);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 736, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 425, Short.MAX_VALUE))
        );

        btnexportar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnexportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Iconos/Descargar.png"))); // NOI18N
        btnexportar.setMnemonic('e');
        btnexportar.setText("Exportar");
        btnexportar.setMaximumSize(new java.awt.Dimension(120, 50));
        btnexportar.setMinimumSize(new java.awt.Dimension(120, 50));
        btnexportar.setPreferredSize(new java.awt.Dimension(120, 50));
        btnexportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnexportarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 528, Short.MAX_VALUE)
                        .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnsalir, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnexportar, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        cargartabla(txtbuscar.getText());
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnsalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsalirActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnsalirActionPerformed

    private void btnexportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnexportarActionPerformed
        if (this.tablaproductos.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "La tabla está vacía");
            return;
        }
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Archivos de Excel", "xls");
        chooser.setFileFilter(filter);
        chooser.setDialogTitle("Guardar Archivo");
        chooser.setMultiSelectionEnabled(false);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            List<JTable> tb = new ArrayList<>();
            List<String> nom = new ArrayList<>();
            tb.add(tablaproductos);
            nom.add("Tabla Ranking Productos");
            String archivo = chooser.getSelectedFile().toString().concat(".xls");
            try {
                Clases.fnExportar e = new fnExportar(new File(archivo), tb, nom);
                if (e.export()) {
                    JOptionPane.showMessageDialog(null, "Los datos se guardaron correctamente", "", JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "No se pudo completar la operación");
                JOptionPane.showMessageDialog(null, e);
            }
        }
    }//GEN-LAST:event_btnexportarActionPerformed

    private void cbocategoriasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbocategoriasActionPerformed
        if (!cbocategorias.getSelectedItem().toString().equals("Categorias")) {
            filtrarcategoria(cbocategorias.getSelectedItem().toString());
        } else {
            cargartabla("");
        }
    }//GEN-LAST:event_cbocategoriasActionPerformed

    private void cbomarcasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbomarcasActionPerformed
        if (!cbomarcas.getSelectedItem().toString().equals("Marcas")) {
            filtrarmarca(cbomarcas.getSelectedItem().toString());
        } else {
            cargartabla("");
        }
    }//GEN-LAST:event_cbomarcasActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnexportar;
    private javax.swing.JButton btnsalir;
    private javax.swing.JLabel buscar;
    private javax.swing.JComboBox cbocategorias;
    private javax.swing.JComboBox cbomarcas;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaproductos;
    private javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables
}
