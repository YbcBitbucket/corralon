package Clases;

import Formularios.CajaApertura;
import static Formularios.Login.id_usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

public class ClaseCaja {

    public ClaseCaja() {
    }

    public int iniciocaja() {
        int apertura = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectCaja = null;
        String sql = "SELECT * FROM caja";
        try {
            SelectCaja = cn.createStatement();
            ResultSet rs = SelectCaja.executeQuery(sql);
            rs.last();
            if (rs.getRow() == 0 || rs.getString("descripcion").equals("CIERRE")) {
                int opcion = JOptionPane.showConfirmDialog(null, "Desea Realizar la apertura de Caja", "Mensaje", JOptionPane.YES_NO_OPTION);
                if (opcion == 0) {
                    new CajaApertura(null, true).setVisible(true);
                }
            } else {
                apertura = rs.getInt("idCaja");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de Datos");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (SelectCaja != null) {
                    SelectCaja.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return apertura;
    }

    public int CajaApertura(String total) {
        int apertura = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement InsertCaja = null;
        String sSQL = "INSERT INTO caja(descripcion, total, fecha, hora,id_usuario)"
                + "VALUES(?,?,?,?,?)";
        fnCargarFecha f = new fnCargarFecha();
        try {
            InsertCaja = cn.prepareStatement(sSQL);
            InsertCaja.setString(1, "APERTURA");
            InsertCaja.setString(2, total);
            InsertCaja.setString(3, f.cargarfecha());
            InsertCaja.setString(4, f.cargarHora());
            InsertCaja.setInt(5,id_usuario);
            int n = InsertCaja.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                apertura = 1;
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de Datos");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (InsertCaja != null) {
                    InsertCaja.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return apertura;
    }

    public int CajaCierre(String id_caja,String apertura, String total_ingresos_contado, String total_ingresos_tarjeta,String total_cuenta, String total_egresos, Double diferencia, Double total) {
        int cierre = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement InsertCaja = null;
        fnCargarFecha f = new fnCargarFecha();
        String sSQL = "INSERT INTO caja(descripcion, total, fecha, hora, diferencia,id_usuario)"
                + "VALUES(?,?,?,?,?,?)";

        try {
            //Inserto Caja
            InsertCaja = cn.prepareStatement(sSQL);
            InsertCaja.setString(1, "CIERRE");
            InsertCaja.setDouble(2, total);
            InsertCaja.setString(3, f.cargarfecha());
            InsertCaja.setString(4, f.cargarHora());
            System.out.println("Clases.ClaseCaja.CajaCierre() " + diferencia);
            if (diferencia > 0) {
                InsertCaja.setDouble(5, diferencia);
            } else {
                InsertCaja.setDouble(5, -diferencia);
            }
            InsertCaja.setInt(6,id_usuario);
            int n = InsertCaja.executeUpdate();
            if (n > 0) {
                try {
                    JasperReport reportecaja = (JasperReport) JRLoader.loadObject(getClass().getResource("/Reportes/ControlCaja.jasper"));
                    Map parametrocaja = new HashMap();
                    parametrocaja.put("fecha",  f.cargarfecha());
                    parametrocaja.put("hora",  f.cargarHora());
                    parametrocaja.put("apertura", "$ " + apertura);
                    parametrocaja.put("total_ingresos_contado", "$ " + total_ingresos_contado);
                    parametrocaja.put("total_ingresos_tarjeta", "$ " + total_ingresos_tarjeta);
                    parametrocaja.put("total_cuenta", "$ " + total_cuenta);                    
                    parametrocaja.put("egresos", "$ " + total_egresos);
                    parametrocaja.put("diferencia", "$ " + String.valueOf(diferencia));
                    parametrocaja.put("total", "$ " + String.valueOf(total));
                    parametrocaja.put("observacion", "");

                    JasperPrint jPrint = JasperFillManager.fillReport(reportecaja, parametrocaja, new JREmptyDataSource());
                    //  JasperViewer.viewReport(jPrint, true);
                    //JasperPrint jPrinteg = JasperFillManager.fillReport(reportecaja, parametroseg, new JRBeanCollectionDataSource(ResultadosEgresos));
                    JasperExportManager.exportReportToPdfFile(jPrint, "C:\\Informe_caja\\" + "Caja " + id_caja +" - "+f.cargarfecha() + ".pdf");
                    //JasperPrintManager.printReport(jPrint, false);
                    //JasperPrintManager.printReport(jPrint, false);

                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de Datos");
            JOptionPane.showMessageDialog(null, ex);
        } finally {
            try {
                if (InsertCaja != null) {
                    InsertCaja.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return cierre;
    }

}
