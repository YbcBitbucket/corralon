/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

public class ClaseIngresaPago {
    int id_forma;
    double total;
    double interes;

    public ClaseIngresaPago(int id_forma, double total, double interes) {
        this.id_forma = id_forma;
        this.total = total;
        this.interes = interes;
    }

    public int getId_forma() {
        return id_forma;
    }

    public double getTotal() {
        return total;
    }

    public double getInteres() {
        return interes;
    }

    public void setId_forma(int id_forma) {
        this.id_forma = id_forma;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setInteres(double interes) {
        this.interes = interes;
    }
    
}
