
package Clases;

public class cboCaja {
    private String idcboCaja;
    private String fechacboCaja;
    
    public cboCaja(){}
    
    public cboCaja(String fechacboCaja, String idcboCaja){
        this.fechacboCaja = fechacboCaja;
        this.idcboCaja = idcboCaja;
        
    }

    public String getfechacboCaja() {
        return fechacboCaja;
    }

    public void setfechacboCajaa(String fechacboCaja) {
        this.fechacboCaja = fechacboCaja;
    }
    
    public String getidcboCaja() {
        return idcboCaja;
    }

    public void setidcboCaja(String idcboCaja) {
        this.idcboCaja = idcboCaja;
    }

    
    
    public String toString(){
        return this.fechacboCaja;
    }
    
}