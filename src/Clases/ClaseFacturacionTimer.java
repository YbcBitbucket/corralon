package Clases;

import static Formularios.Login.id_usuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class ClaseFacturacionTimer {

    int randomTimeOutput;
    Random randomTime;
    Timer timerRunAction;
    java.util.TimerTask timerTask;
    java.util.Timer timer;

    public void ClaseFacturacionTimer(final int minimal, final int max) {
        randomTime = new Random();
        randomTimeOutput = (minimal + randomTime.nextInt(max - minimal + 1));

        timerRunAction = new Timer(randomTimeOutput, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                facturar();
                updateTimer(minimal, max);
            }
        });
        timerRunAction.start();
    }

    private void updateTimer(int minimal, int max) {
        timerRunAction.stop();
        randomTimeOutput = (minimal + randomTime.nextInt(max - minimal + 1));
        timerRunAction.setDelay(randomTimeOutput);
        timerRunAction.restart();
    }

    public void stopTimer() {
        timerRunAction.stop();
        
    }

    private void facturar() {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectUsuarios = null;
        String sSQL = "SELECT * FROM vista_facturacion_timer";
        try {
            SelectUsuarios = cn.createStatement();
            ResultSet rs = SelectUsuarios.executeQuery(sSQL);
            // Recorro y me fijo donde coinciden usuario y contraseña
            while (rs.next()) {

                if (rs.getDouble("DIFERENCIA_HOY") >= 0.0) {
                    if (rs.getDouble("FACTURADO_HOY") + rs.getDouble("Total") <= rs.getDouble("topeDiario")) {
                        ClasePedidos pedido = new ClasePedidos();
                        if (pedido.FacturarPedido(rs.getInt("idClientes"), rs.getInt("idPedidos"), rs.getInt("idPuntosdeVentas"),id_usuario)) {
                                JOptionPane.showMessageDialog(null, "La factura se realizo con exito");
                                break;
                            }
                    }
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (SelectUsuarios != null) {
                    SelectUsuarios.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public void FacturarZ(int hora, int minuto) {
        ///Hora Despertador
        timerTask = new java.util.TimerTask() {
            public void run() {
                facturarAntesZ();
                ClaseAfipHasar2G reporte = new ClaseAfipHasar2G();
                reporte.ReporteZ();
                JOptionPane.showMessageDialog(null, "El Reporte se realizo con exito");
            }
        };
        // El despertador suena cada 24h (una vez al dia)
        int tiempoRepeticion = 86400000;
        //int tiempoRepeticion = 5000;
        Date horaDespertar = new Date(System.currentTimeMillis());
        Calendar c = Calendar.getInstance();
        // Si la hora es posterior a las 23 se programa la alarma para el dia siguiente
        if (c.get(Calendar.HOUR_OF_DAY) >= hora) {
            c.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR) + 1);
        }
        c.set(Calendar.HOUR_OF_DAY, hora);
        c.set(Calendar.MINUTE, minuto);
        c.set(Calendar.SECOND, 0);

        horaDespertar = c.getTime();
        System.out.println("hora desperat:" + horaDespertar);

        timer = new java.util.Timer();
        timer.schedule(timerTask, horaDespertar, tiempoRepeticion);
    }

    private void facturarAntesZ() {
        //JOptionPane.showMessageDialog(null, randomTimeOutput);
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectUsuarios = null;
        String sSQL = "SELECT * FROM vista_facturacion_timer";
        int BanderaTope = 0;
        try {
            while (BanderaTope==0) {
                SelectUsuarios = cn.createStatement();
                ResultSet rs = SelectUsuarios.executeQuery(sSQL);
                // Recorro y me fijo donde coinciden usuario y contraseña
                while (rs.next()) {
                    if (rs.getDouble("DIFERENCIA_HOY") >= 0) {
                        if (rs.getDouble("FACTURADO_HOY") + rs.getDouble("Total") <= rs.getDouble("topeDiario")) {
                            ClasePedidos pedido = new ClasePedidos();
                            if (pedido.FacturarPedido(rs.getInt("idClientes"), rs.getInt("idPedidos"), rs.getInt("idPuntosdeVentas"),id_usuario)) {
                                JOptionPane.showMessageDialog(null, "La factura se realizo con exito");
                                break;
                            }

                        }
                    } else {
                        BanderaTope = 1;
                    }
                }
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, e);
        } finally {
            try {
                if (SelectUsuarios != null) {
                    SelectUsuarios.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

}