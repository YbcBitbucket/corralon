package Clases;

public class cboTipoAfip {
    
  
    private int idTipoAfip;
    private String descripcion;
    
    public cboTipoAfip(){}
    
    public cboTipoAfip(int idTipoAfip, String descripcion){
        this.idTipoAfip = idTipoAfip;
        this.descripcion = descripcion;
    }

    public int getidTipoAfip() {
        return idTipoAfip;
    }

    public void setidTipoAfip(int idTipoAfip) {
        this.idTipoAfip = idTipoAfip;
    }

    public String getdescripcion() {
        return descripcion;
    }

    public void setdescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String toString(){
        return this.descripcion;
    }
    
}
