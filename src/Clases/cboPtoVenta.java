package Clases;

public class cboPtoVenta {
    
  
    private int numero;
    private String descripcion;
    
    public cboPtoVenta(){}
    
    public cboPtoVenta(int numero, String descripcion){
        this.numero = numero;
        this.descripcion = descripcion;
    }

    public int getnumero() {
        return numero;
    }

    public void setnumero(int numero) {
        this.numero = numero;
    }

    public String getdescripcion() {
        return descripcion;
    }

    public void setdescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String toString(){
        return this.descripcion;
    }
    
}
