package Clases;

import WSFE_HOMOLOGACION.ArrayOfTributo;
import WebServices.ServidorWsaa;
import WebServices.ServidorWsfev;
import WebServices.ServidorWsfevHomologacion;
import java.util.List;
import javax.swing.JOptionPane;

public class ClaseAfipWSCae {

    public String ObtenerCAE() {
        String CAE = null;
        ClaseAfipEstado Afip = new ClaseAfipEstado();
        //Autorizacion
        ServidorWsaa autorizacion = new ServidorWsaa();
        if (Afip.Estado().equals("PRODUCCION")) {

        }
        if (Afip.Estado().equals("HOMOLOGACION")) {

            //  FeCabReq: La cabecera del comprobante o lote de comprobantes de ingreso está compuesta por los siguientes campos:
            WSFE_HOMOLOGACION.FECAECabRequest CAECab = new WSFE_HOMOLOGACION.FECAECabRequest();

            //  CantReg     Int (4) Cantidad de registros del detalle del comprobante o lote de comprobantes de ingreso S
            CAECab.setCantReg(1);
            //  CbteTipo    Int (3) Tipo de comprobante que se está informando. Si se informa más de un comprobante, todos deben ser del mismo tipo. S
            CAECab.setCbteTipo(1); //FACTURA A
            //  PtoVta      Int (4) Punto de Venta del comprobante que se está informando. Si se informa más de un comprobante, todos deben corresponder al mismo punto de venta. S
            CAECab.setPtoVta(9);

            //  FeDetReq: El detalle del comprobante o lote de comprobantes de ingreso está compuesto por los siguientes campos:
            WSFE_HOMOLOGACION.FECAEDetRequest CAEDet = new WSFE_HOMOLOGACION.FECAEDetRequest();

            //  Concepto    Int(2) Concepto del Comprobante. Valores permitidos: 1 Productos 2 Servicios 3 Productos y Servicios S
            CAEDet.setConcepto(1);
            //  DocTipo     Int (2) Código de documento identificatorio del comprador S
            CAEDet.setDocTipo(80);
            //  DocNro      Long (11) Nro. De identificación del comprador S
            long DocNro = Long.parseLong("20111111112");
            CAEDet.setDocNro(DocNro);
            //  CbteDesde   Long (8) Nro. De comprobante desde Rango 1- 99999999 S
            CAEDet.setCbteDesde(1);
            //  CbteHasta   Long (8) Nro. De comprobante registrado hasta Rango 1- 99999999 S
            CAEDet.setCbteHasta(1);
            //  CbteFch     String (8) Fecha del comprobante (yyyymmdd). Para concepto igual a 1, la fecha de emisión del comprobante puede ser hasta 5 días anteriores o posteriores respecto de la fecha de generación; si se indica Concepto igual a 2 ó 3 puede ser hasta 10 días anteriores o posteriores a la fecha de generación. Si no se envía la fecha del comprobante se asignará la fecha de proceso N
            CAEDet.setCbteFch("20180709");
            //  ImpTotal    Double (13+2) Importe total del comprobante, Debe ser igual a Importe neto no gravado + Importe exento + Importe neto gravado + todos los campos de IVA al XX% + Importe de tributos. S
            CAEDet.setImpTotal(184.05);
            //  ImpTotConc  Double (13+2) Importe neto no gravado. Debe ser menor o igual a Importe total y no puede ser menor a cero. No puede ser mayor al Importe total de la operación ni menor a cero (0). Para comprobantes tipo C debe ser igual a cero (0). Para comprobantes tipo Bienes Usados – Emisor Monotributista este campo corresponde al importe subtotal. S
            CAEDet.setImpTotConc(0);
            //  ImpNeto     Double (13+2) Importe neto gravado. Debe ser menor o igual a Importe total y no puede ser menor a cero. Para comprobantes tipo C este campo corresponde al Importe del Sub Total. Para comprobantes tipo Bienes Usados – Emisor Monotributista no debe informarse o debe ser igual a cero (0). S
            CAEDet.setImpNeto(150);
            //  ImpOpEx     Double (13+2) Importe exento. Debe ser menor o igual a Importe total y no puede ser menor a cero. Para comprobantes tipo C debe ser igual a cero (0). Para comprobantes tipo Bienes Usados – Emisor Monotributista no debe informarse o debe ser igual a cero (0). S
            CAEDet.setImpOpEx(0);
            //  ImpIVA      Double (13+2) Suma de los importes del array de IVA. Para comprobantes tipo C debe ser igual a cero (0). Para comprobantes tipo Bienes Usados – Emisor Monotributista no debe informarse o debe ser igual a cero (0). S 
            CAEDet.setImpIVA(26.25);
            //  ImpTrib     Double (13+2) Suma de los importes del array de tributos S
            CAEDet.setImpTrib(7.8);

            /*DATO OBLIGATORIO PARA SERVICIOS
             //  FchServDesde String (8) Fecha de inicio del abono para el servicio a facturar. Dato obligatorio para concepto 2 o 3 (Servicios / Productos y Servicios). Formato yyyymmdd N
             CAEDet.setFchServDesde("20180709");
             //  FchServHasta String (8) Fecha de fin del abono para el servicio a facturar. Dato obligatorio para concepto 2 o 3 (Servicios / Productos y Servicios). Formato yyyymmdd. FchServHasta no puede ser menor a FchServDesde N 
             CAEDet.setFchServHasta("20180709");
             //  FchVtoPago   String (8) Fecha de vencimiento del pago servicio a facturar. Dato obligatorio para concepto 2 o 3 (Servicios / Productos y Servicios). Formato yyyymmdd. Debe ser igual o posterior a la fecha del comprobante. N
             CAEDet.setFchVtoPago("20180709");
             */
            //  MonId       String (3) Código de moneda del comprobante. Consultar método FEParamGetTiposMonedas para valores posibles S
            CAEDet.setMonId("PES");
            //  MonCotiz    Double (4+6) Cotización de la moneda informada. Para PES, pesos argentinos la misma debe ser 1 S
            CAEDet.setMonCotiz(1);

            //  CbtesAsoc Array Array para informar los comprobantes asociados <CbteAsoc> N
            //  Tributos Array Array para informar los tributos asociados a un comprobante <Tributo>. N
            WSFE_HOMOLOGACION.Tributo Tributo = new WSFE_HOMOLOGACION.Tributo();

            //  Id  Int (2) Código tributo según método FEParamGetTiposTributos S
            short IdTributo = Short.valueOf("99");
            Tributo.setId(IdTributo);
            //  Desc String (80) Descripción del tributo. N 
            Tributo.setDesc("Impuesto Municipal Matanza");
            //  BaseImp Double (13+2) Base imponible para la determinación del tributo S
            Tributo.setBaseImp(150);
            //  Alic Double (3+2) Alícuota S 
            Tributo.setAlic(5.2);
            //  Importe Double (13+2) Importe del tributo S
            Tributo.setImporte(7.8);

            WSFE_HOMOLOGACION.ArrayOfTributo ArrayTributo = new WSFE_HOMOLOGACION.ArrayOfTributo();

            ArrayTributo.getTributo().add(0, Tributo);

            CAEDet.setTributos(ArrayTributo);

            //  IVA Array Array para informar las alícuotas y sus importes asociados a un comprobante <AlicIva>. Para comprobantes tipo C y Bienes Usados – Emisor Monotributista no debe informar el array. N 
            WSFE_HOMOLOGACION.AlicIva alicIva1 = new WSFE_HOMOLOGACION.AlicIva();
            //  Id Int (2) Código de tipo de iva. Consultar método FEParamGetTiposIva S
            alicIva1.setId(5);
            //  BaseImp Double (13+2) Base imponible para la determinación de la alícuota. S
            alicIva1.setBaseImp(100);
            //  Importe Double (13+2) Importe S
            alicIva1.setImporte(21);

            WSFE_HOMOLOGACION.ArrayOfAlicIva ArrayAlicuota = new WSFE_HOMOLOGACION.ArrayOfAlicIva();
            ArrayAlicuota.getAlicIva().add(0, alicIva1);

            WSFE_HOMOLOGACION.AlicIva alicIva2 = new WSFE_HOMOLOGACION.AlicIva();
            alicIva2.setId(4);
            alicIva2.setBaseImp(50);
            alicIva2.setImporte(5.25);

            ArrayAlicuota.getAlicIva().add(1, alicIva2);

            CAEDet.setIva(ArrayAlicuota);

            //  Opcionales Array Array de campos auxiliares. Reservado usos futuros <Opcional>. Adicionales por R.G. N
            //  Compradores Array Array para informar los multiples compradores. N
            WSFE_HOMOLOGACION.ArrayOfFECAEDetRequest ArrayDetReq = new WSFE_HOMOLOGACION.ArrayOfFECAEDetRequest();
            ArrayDetReq.getFECAEDetRequest().add(0, CAEDet);

            WSFE_HOMOLOGACION.FECAERequest CAEreq = new WSFE_HOMOLOGACION.FECAERequest();
            CAEreq.setFeCabReq(CAECab);
            CAEreq.setFeDetReq(ArrayDetReq);

            WSFE_HOMOLOGACION.FECAEResponse ObtenerCAE = ServidorWsfevHomologacion.fecaeSolicitar(autorizacion.ObtenerAutorizacionHomologacion(), CAEreq);
            System.out.println("RESULTADO");
            System.out.println(ObtenerCAE.getFeCabResp().getResultado());
            System.out.println("REPROCESO");
            System.out.println(ObtenerCAE.getFeCabResp().getReproceso());

            List<WSFE_HOMOLOGACION.FECAEDetResponse> lista = ObtenerCAE.getFeDetResp().getFECAEDetResponse();
            for (int i = 0; i < lista.size(); i++) {
                System.out.println("RESULTADO");
                System.out.println(lista.get(i).getResultado());
                System.out.println("CAE");
                System.out.println(lista.get(i).getCAE());
                System.out.println("CAE FEchaVto");
                System.out.println(lista.get(i).getCAEFchVto());
                CAE = lista.get(i).getCAE();
                if (CAE.equals("")) {
                    List<WSFE_HOMOLOGACION.Obs> listaObs = lista.get(i).getObservaciones().getObs();
                    for (int j = 0; j < listaObs.size(); j++) {
                        System.out.println("Mensaje Obs");
                        System.out.println(listaObs.get(i).getMsg());
                        System.out.println("CODIGO Obs");
                        System.out.println(listaObs.get(i).getCode());

                    }
                }

            }

        }

        return CAE;
    }

}
