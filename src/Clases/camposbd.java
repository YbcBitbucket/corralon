package Clases;

public class camposbd {

    String codigo, nombre, precio, bonificacion, iva, cantidad, importe;

    public camposbd(String codigo, String nombre, String precio, String bonificacion, String iva, String cantidad, String importe) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
        this.bonificacion = bonificacion;
        this.iva = iva;
        this.cantidad = cantidad;
        this.importe = importe;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getBonificacion() {
        return bonificacion;
    }

    public void setBonificacion(String bonificacion) {
        this.bonificacion = bonificacion;
    }

    public String getIva() {
        return iva;
    }

    public void setIva(String iva) {
        this.iva = iva;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getimporte() {
        return importe;
    }

    public void setimporte(String importe) {
        this.importe = importe;
    }
}
