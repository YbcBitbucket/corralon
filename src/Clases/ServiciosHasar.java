package Clases;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import javax.swing.JOptionPane;

public class ServiciosHasar {

    StringBuilder pid = new StringBuilder("hasar.fiscal.1");
    ActiveXComponent hasar = new ActiveXComponent(pid.toString());
    Dispatch sControl = hasar.getObject();

    @SuppressWarnings("deprecation")
    public ServiciosHasar() {

    }

    public String Comenzar(int transporte, int puerto, int baudios, String direccionip) {
        String comenzar="";
        try {
            
            Dispatch.put(sControl, "Transporte", transporte);
            if (transporte == 0) {
                Dispatch.put(sControl, "Puerto", puerto);
                Dispatch.put(sControl, "Baudios", baudios);
                Dispatch.call(hasar, "Comenzar");
            } else {
                Dispatch.put(sControl, "Puerto", puerto);
                Dispatch.put(sControl, "DireccionIP", direccionip);
                Dispatch.call(hasar, "Comenzar");
            }
            comenzar="OK";
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
            comenzar="ERROR";
        }
        return comenzar;
    }

    public void AvanzarPapel(int tipodepapel,int cantidadlineas ){
        try {
            Dispatch.call(hasar, "AvanzarPapel", tipodepapel, cantidadlineas);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    public void ObtenerDatosDeInicializacion(){
        try {
            Dispatch.call(hasar, "ObtenerDatosDeInicializacion");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    
    public void TratarDeCancelarTodo() {
        try {
            Dispatch.call(hasar, "TratarDeCancelarTodo");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void DatosCliente(String rsocial, String nrodoc, int tipodoc, int categ, String domic) {
        try {
            Dispatch.call(hasar, "DatosCliente", rsocial, nrodoc, tipodoc, categ, domic);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void AbrirComprobanteFiscal(int tipo) {
        try {
            Dispatch.call(hasar, "AbrirComprobanteFiscal", tipo);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void ImprimirItem(String descrip, double cant, double precio, double alicuota, double impint) {
        try {
            Dispatch.call(hasar, "ImprimirItem", descrip, cant, precio, alicuota, impint);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void Subtotal(boolean impr) {
        try {
            Variant total = null;
            Dispatch.call(hasar, "Subtotal", impr, total);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void ImprimirPago(String descrip, double monto) {
        try {
            Dispatch.call(hasar, "ImprimirPago", descrip, monto);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void CerrarComprobanteFiscal(int copias) {
        try {
            Dispatch.call(hasar, "CerrarComprobanteFiscal", copias);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public boolean VerificarModelo(int modelo) {
        boolean verifica = false;
        try {
            Dispatch.put(sControl, "Modelo", modelo);
            verifica = Dispatch.call(hasar, "VerificarModelo").toBoolean();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return verifica;
    }

    public void AutodetectarControlador() {
        try {
            Dispatch.call(hasar, "AutodetectarControlador");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void AutodetectarModelo() {
        try {
            Dispatch.call(hasar, "AutodetectarModelo");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void PrecioBase(boolean preciobase) {
        try {
            Dispatch.put(sControl, "PrecioBase", preciobase);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void ImpuestoInternoFijo(boolean ImpuestoInternoFijo) {
        try {
            Dispatch.put(sControl, "ImpuestoInternoFijo", ImpuestoInternoFijo);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void DescuentoGeneral(String descrip, Double monto, boolean negat) {
        try {
            Dispatch.call(hasar, "DescuentoGeneral", descrip, monto, negat);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public String Respuesta(long respuesta) {
        String res = null;
        try {
            res = Dispatch.call(sControl, "Respuesta", respuesta).toString();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return res;
    }

    public int UltimoDocumentoFiscalA() {
        int UltimoDocumentoFiscalA = 0;
        try {
            UltimoDocumentoFiscalA = Dispatch.call(sControl, "UltimoDocumentoFiscalA").toInt();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        return UltimoDocumentoFiscalA;
    }

    public void ConfigurarControlador(int param, String valor) {
        try {
            Dispatch.call(sControl, "ConfigurarControlador", param, valor);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void DescuentoUltimoItem(String descrip, Double monto, boolean negat) {
        try {
            Dispatch.call(sControl, "DescuentoUltimoItem", descrip, monto, negat);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    public void ReporteZ(){
        try {
            Dispatch.call(hasar, "ReporteZ");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }

    public void ReporteX(){
        try {
            Dispatch.call(hasar, "ReporteX");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    public void ConfigurarControladorCompleto(boolean imprimir, Double limitecf, Double limitetf, int copias, boolean cambio, boolean leyendas){
        try {
            Dispatch.call(sControl, "ConfigurarControladorCompleto", imprimir, true, limitecf, limitetf, 0.00, copias, cambio, leyendas);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    public void ObtenerConfiguracion(){
        try {
            Dispatch.call(hasar, "ObtenerConfiguracion");
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    public void ConfigurarControladorPorBloque(Double limitecf, Double limitetf, int copias, boolean cambio, boolean leyendas, int corte){
        try {
            Dispatch.call(sControl, "ConfigurarControladorPorBloque", limitecf, limitetf, 0.00, copias, cambio, leyendas, corte);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
    }
    
    
    
}
