package Clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ClaseProductos {

    public ClaseProductos() {
    }

    public int AgregarProducto(String codigo, String nombre, int id_cat, int id_mar, String unidad, int minimo, String descripcion) {
        int producto = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        Statement SelectCodigoProducto = null;
        PreparedStatement AgregarProducto = null;
        //Veo si Ivadocumento no esta cargado en p_cliente
        String sqlcod = "SELECT codigo FROM productos WHERE codigo='" + codigo + "'";
        try {
            SelectCodigoProducto = cn.createStatement();
            ResultSet rscod = SelectCodigoProducto.executeQuery(sqlcod);
            if (!rscod.next()) {
                cn.setAutoCommit(false); //transaction block start
                //Inserto el producto
                String sSQL = "INSERT INTO productos(codigo, nombre, idCategorias, idMarcas"
                        + ", unidad, minimo, descripcion) VALUES(?,?,?,?,?,?,?)";
                AgregarProducto = cn.prepareStatement(sSQL);
                AgregarProducto.setString(1, codigo);
                AgregarProducto.setString(2, nombre);
                AgregarProducto.setInt(3, id_cat);
                AgregarProducto.setDouble(4, id_mar);
                AgregarProducto.setString(5, unidad);
                AgregarProducto.setInt(6, minimo);
                AgregarProducto.setString(7, descripcion);
                int n = AgregarProducto.executeUpdate();
                if (n > 0) {
                    JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
                }
                cn.commit(); //transaction block end
                producto = 1;
            } else {
                JOptionPane.showMessageDialog(null, "El Producto ya esta en la base de datos");
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
            JOptionPane.showMessageDialog(null, ex);
            try {
                cn.rollback();
            } catch (SQLException e) {
                JOptionPane.showMessageDialog(null, e);
            }
        } finally {
            try {
                if (SelectCodigoProducto != null) {
                    SelectCodigoProducto.close();
                }
                if (AgregarProducto != null) {
                    AgregarProducto.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return producto;
    }

    public void ModificarProducto(int idProducto, String codigo, String nombre, int id_cat, int id_mar, String unidad, int minimo, String descripcion) {
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ModificarProducto = null;
        try {
            String sSQL = "UPDATE productos SET codigo=?, nombre=?, idCategorias=?, idMarcas=?, unidad=?, minimo=?,"
                    + " descripcion=? WHERE idProductos=" + idProducto;
            ModificarProducto = cn.prepareStatement(sSQL);
            ModificarProducto.setString(1, codigo);
            ModificarProducto.setString(2, nombre);
            ModificarProducto.setInt(3, id_cat);
            ModificarProducto.setDouble(4, id_mar);
            ModificarProducto.setString(5, unidad);
            ModificarProducto.setInt(6, minimo);
            ModificarProducto.setString(7, descripcion);
            int n = ModificarProducto.executeUpdate();
            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Los Datos se insertaron exitosamente...");
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ModificarProducto != null) {
                    ModificarProducto.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
    }

    public int ModificarMoneda(int idProducto, String moneda) {
        int estado = 0;
        ConexionMySQL mysql = new ConexionMySQL();
        Connection cn = mysql.Conectar();
        PreparedStatement ModificarProducto = null;
        try {
            String sSQL = "UPDATE productos SET moneda=? WHERE idProductos=" + idProducto;
            ModificarProducto = cn.prepareStatement(sSQL);
            ModificarProducto.setString(1, moneda);
            int n = ModificarProducto.executeUpdate();
            if (n > 0) {
                estado = 1;
            }
        } catch (SQLException ex) {
            estado = 0;
            JOptionPane.showMessageDialog(null, ex);
            JOptionPane.showMessageDialog(null, "Error en la base de datos...");
        } finally {
            try {
                if (ModificarProducto != null) {
                    ModificarProducto.close();
                }
                if (cn != null) {
                    cn.close();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        return estado;
    }

}
