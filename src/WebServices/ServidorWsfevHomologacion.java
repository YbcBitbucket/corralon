
package WebServices;

import WSFE_HOMOLOGACION.CbteTipoResponse;
import WSFE_HOMOLOGACION.DummyResponse;
import WSFE_HOMOLOGACION.FEAuthRequest;
import WSFE_HOMOLOGACION.FECAEResponse;
import WSFE_HOMOLOGACION.FEPtoVentaResponse;


public class ServidorWsfevHomologacion {

    public static FECAEResponse fecaeSolicitar(WSFE_HOMOLOGACION.FEAuthRequest auth, WSFE_HOMOLOGACION.FECAERequest CAEreq) {
        WSFE_HOMOLOGACION.Service service = new WSFE_HOMOLOGACION.Service();
        WSFE_HOMOLOGACION.ServiceSoap port = service.getServiceSoap();
        return port.fecaeSolicitar(auth, CAEreq);
    }
    public static DummyResponse feDummy() {
        WSFE_HOMOLOGACION.Service service = new WSFE_HOMOLOGACION.Service();
        WSFE_HOMOLOGACION.ServiceSoap port = service.getServiceSoap();
        return port.feDummy();
    }
    
    public static FEPtoVentaResponse feParamGetPtosVenta(WSFE_HOMOLOGACION.FEAuthRequest auth) {
        WSFE_HOMOLOGACION.Service service = new WSFE_HOMOLOGACION.Service();
        WSFE_HOMOLOGACION.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetPtosVenta(auth);
    }
    
    public static CbteTipoResponse feParamGetTiposCbte(WSFE_HOMOLOGACION.FEAuthRequest auth) {
        WSFE_HOMOLOGACION.Service service = new WSFE_HOMOLOGACION.Service();
        WSFE_HOMOLOGACION.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposCbte(auth);
    }

    
}
